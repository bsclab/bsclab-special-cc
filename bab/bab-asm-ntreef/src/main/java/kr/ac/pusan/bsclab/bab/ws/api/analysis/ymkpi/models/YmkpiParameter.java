package kr.ac.pusan.bsclab.bab.ws.api.analysis.ymkpi.models;

import java.io.Serializable;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.Configuration;

/**
 * Example result class from Test Algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 */
public class YmkpiParameter extends Configuration implements Serializable {
  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;

  public static final String CLASSIFIER_RESOURCE = "event.resource";
  public static final String CLASSIFIER_ORIGINATOR = "event.originator";

  public static final String DELIMETER_PIPE = "|";

  public static final String VALUE_CLASSIFIER_WORKINGTIME = "activity.workingtime";
  public static final String VALUE_CLASSIFIER_WEIGHT = "activity.weight";

  public static final String CAPACITY_CLASSIFIER_CAPACITYTIME = "activity.capacitytime";
  public static final String CAPACITY_CLASSIFIER_CAPACITY = "activity.capacity";

  public static final String KPI_THROUGHPUT = "kpi.throughput";
  public static final String KPI_UTILIZATION = "kpi.utilization";

  private String repositoryURI;

  private String classifier = CLASSIFIER_RESOURCE;

  private String subClassifier = CLASSIFIER_ORIGINATOR;

  private String delimeter = DELIMETER_PIPE;

  private String valueClassifier = VALUE_CLASSIFIER_WORKINGTIME;

  private String capacityClassifier = CAPACITY_CLASSIFIER_CAPACITYTIME;

  private String kpi = KPI_UTILIZATION;

  public String getRepositoryURI() {
    return repositoryURI;
  }

  public void setRepositoryURI(String repositoryURI) {
    this.repositoryURI = repositoryURI;
  }

  public String getClassifier() {
    return classifier;
  }

  public void setClassifier(String classifier) {
    this.classifier = classifier;
  }

  public String getSubClassifier() {
    return subClassifier;
  }

  public void setSubClassifier(String subClassifier) {
    this.subClassifier = subClassifier;
  }

  public String getDelimeter() {
    return delimeter;
  }

  public void setDelimeter(String delimeter) {
    this.delimeter = delimeter;
  }

  public String getValueClassifier() {
    return valueClassifier;
  }

  public void setValueClassifier(String valueClassifier) {
    this.valueClassifier = valueClassifier;
  }

  public String getCapacityClassifier() {
    return capacityClassifier;
  }

  public void setCapacityClassifier(String capacityClassifier) {
    this.capacityClassifier = capacityClassifier;
  }

  public String getKpi() {
    return kpi;
  }

  public void setKpi(String kpi) {
    this.kpi = kpi;
  }

}
