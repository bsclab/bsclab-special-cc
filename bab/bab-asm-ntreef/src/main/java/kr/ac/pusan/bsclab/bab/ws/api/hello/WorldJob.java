package kr.ac.pusan.bsclab.bab.ws.api.hello;

import java.util.ArrayList;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.SparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.AbstractModelJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;
import scala.Tuple2;

public class WorldJob extends AbstractModelJob {

	private static final long serialVersionUID = 1L;

	@Override
	@BabService(
		name = "HelloWorldJob", 
		title = "Hello World",
		requestClass = WorldParameter.class, 
		responseClass = World.class,
		legacyJobExtension = ".world"
	)
	public IJobResult run(String json, IResource res, IExecutor se) throws Exception {

		SparkExecutor.debug("OK MULAI");
		// Ini untuk baca input parameter
		ObjectMapper mapper = new ObjectMapper();

		WorldParameter config = mapper.readValue(json, WorldParameter.class);

		SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);

		long cnt = reader.getCasesRDD().map(new Function<Tuple2<String, ICase>, Integer>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Integer call(Tuple2<String, ICase> v1) throws Exception {
				ICase c = v1._2();
				SparkExecutor.debug("CASE " + c.getId() + ": " + c.getEvents().size());
				return v1._2().getEvents() == null ? 0 : v1._2().getEvents().size();
			}

		}).count();
		SparkExecutor.debug("COUNT CASE " + cnt);

		// Disini inti algoritmanya
		World model = new World();

		ArrayList<String> als = new ArrayList<String>();
		als.add("A");
		als.add("B");
		als.add("C");
		JavaSparkContext jsc = ((SparkExecutor) se).getContext();
		long c = jsc.parallelize(als).count();

		SparkExecutor.debug("HASILNYA: " + c);

		// model.setMessage("Hello " + config.getMessage() + " c");
		// System.out.println(model.getMessage());

		// Ini untuk nulis outputnya
		String outputURI = se.getContextUri(res.getUri());

		RawJobResult result = new RawJobResult("hello.World", outputURI, outputURI, mapper.writeValueAsString(model));

		se.getFileUtil().saveAsTextFile(se, outputURI + ".world", result.getResponse());
		return result;
	}

}
