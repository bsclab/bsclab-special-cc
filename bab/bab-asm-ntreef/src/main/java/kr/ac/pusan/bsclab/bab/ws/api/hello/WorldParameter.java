package kr.ac.pusan.bsclab.bab.ws.api.hello;

import java.io.Serializable;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobConfiguration;

public class WorldParameter extends Configuration implements IJobConfiguration, Serializable {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String message;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
