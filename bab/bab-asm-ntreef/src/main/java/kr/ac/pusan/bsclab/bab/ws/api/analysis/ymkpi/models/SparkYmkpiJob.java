package kr.ac.pusan.bsclab.bab.ws.api.analysis.ymkpi.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;
import scala.Tuple2;

public class SparkYmkpiJob extends YmkpiJob
    implements PairFlatMapFunction<Tuple2<String, ICase>, String, Map<String, Double>>,
    Function2<Map<String, Double>, Map<String, Double>, Map<String, Double>> {

  private static final long serialVersionUID = 1L;

  private SimpleDateFormat sdf = new SimpleDateFormat("yyyy");

  @Override
  public Iterable<Tuple2<String, Map<String, Double>>> call(Tuple2<String, ICase> m)
      throws Exception {
    Map<String, Map<String, Double>> result = new TreeMap<String, Map<String, Double>>();

    ICase c = m._2();
    for (IEvent e : c.getEvents().values()) {
      String a = e.getResource();
      Date t = new Date(e.getTimestamp());
      int i = 1;
      String y = sdf.format(t);
      if (!result.containsKey(y)) {
        result.put(y, new TreeMap<String, Double>());
      }
      if (!result.get(y).containsKey(a)) {
        result.get(y).put(a, 0d);
      }
      result.get(y).put(a, result.get(y).get(a) + i);
    }

    List<Tuple2<String, Map<String, Double>>> mresult =
        new ArrayList<Tuple2<String, Map<String, Double>>>();
    for (String k : result.keySet()) {
      mresult.add(new Tuple2<String, Map<String, Double>>(k, result.get(k)));
    }
    return mresult;
  }


  @Override
  public Map<String, Double> call(Map<String, Double> v1, Map<String, Double> v2) throws Exception {
    Map<String, Double> result = new TreeMap<String, Double>();
    for (String k : v1.keySet()) {
      if (!result.containsKey(k)) {
        result.put(k, 0d);
      }
      result.put(k, result.get(k) + v1.get(k));
    }
    for (String k : v2.keySet()) {
      if (!result.containsKey(k)) {
        result.put(k, 0d);
      }
      result.put(k, result.get(k) + v2.get(k));
    }
    return result;
  }

  @Override
  public IJobResult run(String json, IResource res, IExecutor se) throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    YmkpiParameter config = mapper.readValue(json, YmkpiParameter.class);
    SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);


    SparkYmkpiJob y = new SparkYmkpiJob();
    JavaPairRDD<String, Map<String, Double>> yearlyMr =
        reader.getCasesRDD().flatMapToPair(y).reduceByKey(y);
    SparkYmkpiJob m = new SparkYmkpiJob();
    m.setSdf(new SimpleDateFormat("yyyy-MM"));
    JavaPairRDD<String, Map<String, Double>> monthlyMr =
        reader.getCasesRDD().flatMapToPair(m).reduceByKey(m);

    Ymkpi model = new Ymkpi();

    model.setParameter(config);
    model.setYearly(yearlyMr.collectAsMap());
    model.setMonthly(monthlyMr.collectAsMap());

    String outputURI = se.getContextUri(res.getUri());

    RawJobResult result =
        new RawJobResult("ym.KPI", outputURI, outputURI, mapper.writeValueAsString(model));

    se.getFileUtil().saveAsTextFile(se, outputURI + ".ymkpi", result.getResponse());
    return result;
  }


  public SimpleDateFormat getSdf() {
    return sdf;
  }


  public void setSdf(SimpleDateFormat sdf) {
    this.sdf = sdf;
  }

}
