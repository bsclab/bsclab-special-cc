package kr.ac.pusan.bsclab.bab.ws.api.hello2;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.AbstractModelJob;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

public class WorldJob2 extends AbstractModelJob {

	private static final long serialVersionUID = 1L;

	@Override
	public IJobResult run(String json, IResource res, IExecutor se) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		WorldParameter2 config = mapper.readValue(json, WorldParameter2.class);
		
		World2 model = new World2();
		
		model.setMessage("Hello " +config.getMessage());
		String outputURI = se.getContextUri(res.getUri());
		RawJobResult result = new RawJobResult("hello.World2", outputURI, outputURI, mapper.writeValueAsString(model));
		se.getFileUtil().saveAsTextFile(se, outputURI + ".kamaljson", result.getResponse());
		
		return result;
	}

	
	
	
}
