package kr.ac.pusan.bsclab.bab.ws.api.hello2;

public class WorldParameter2 {

	private String message;
	private String repositoryURI;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getRepositoryURI() {
		return repositoryURI;
	}
	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}
}
