package kr.ac.pusan.bsclab.bab.ws.api.hello2;

public class World2 {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
