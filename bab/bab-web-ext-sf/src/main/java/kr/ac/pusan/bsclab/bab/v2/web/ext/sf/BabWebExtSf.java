package kr.ac.pusan.bsclab.bab.v2.web.ext.sf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import kr.ac.pusan.bsclab.bab.v2.web.BabWebCommon;

@ComponentScan(basePackages = "kr.ac.pusan.bsclab.bab.v2.web")
@EnableScheduling
@SpringBootApplication
public class BabWebExtSf extends BabWebCommon {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(BabWebExtSf.class, args);
	}
}
