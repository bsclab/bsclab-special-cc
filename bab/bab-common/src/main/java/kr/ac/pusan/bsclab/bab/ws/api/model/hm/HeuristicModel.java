/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.util.LinkedHashMap;
import java.util.Map;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

/**
 * Heuristic model, result of heuristic miner algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class HeuristicModel extends Result {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Result Id
	 */
	private String id;

	/**
	 * Node name and its mined properties (freq, duration, etc)
	 */
	private Map<String, Node> nodes = new LinkedHashMap<String, Node>();

	/**
	 * Arc / transition name and its mined properties (freq, duration,
	 * dependency, etc)
	 */
	private Map<String, Arc> arcs = new LinkedHashMap<String, Arc>();

	/**
	 * BAB resource id
	 */

	public HeuristicModel() {

	}

	public HeuristicModel(String outputURI) {

	}

	public Node getOrAddNode(String label) {
		Node n = nodes.get(label);
		if (n == null) {
			n = new Node();
			n.setLabel(label);
			nodes.put(label, n);
		}
		return n;
	}

	public Arc getOrAddArc(String source, String target) {
		Arc n = arcs.get(source + "|" + target);
		if (n == null) {
			n = new Arc();
			n.setSource(source);
			n.setTarget(target);
			arcs.put(source + "|" + target, n);
		}
		return n;
	}

	public Arc getOrAddArc(String source, String target, double frequency, double dependency) {
		Arc n = getOrAddArc(source, target);
		n.getFrequency().setAbsolute((int) frequency);
		n.setDependency(dependency);
		return n;
	}

	public Map<String, Node> getNodes() {
		return nodes;
	}

	public void setNodes(Map<String, Node> nodes) {
		this.nodes = nodes;
	}

	public Map<String, Arc> getArcs() {
		return arcs;
	}

	public void setArcs(Map<String, Arc> arcs) {
		this.arcs = arcs;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
