/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.common.views;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import kr.ac.pusan.bsclab.bab.v2.common.data.matrix.models.Matrix2D;

public class Matrix2DRenderer<X, Y, V> extends AbstractRenderer<Matrix2D<X, Y, V>, String> {

	@Override
	public String render(Matrix2D<X, Y, V> m) {
		if (renderer != null) {
			return renderer.render(m);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<table>").append(eol);
		Set<Y> headers;
		if (m.isSorted()) {
			headers = new TreeSet<Y>();
		} else {
			headers = new LinkedHashSet<Y>();
		}
		for (X x : m.getMatrix().keySet()) {
			for (Y y : m.get(x).keySet()) {
				headers.add(y);
			}
		}
		sb.append(tab).append("<thead>").append(eol);
		sb.append(tab).append(tab).append("<tr>").append(eol);
		sb.append(tab).append(tab).append(tab).append("<th>#</th>").append(eol);
		for (Y y : headers) {
			sb.append(tab).append(tab).append(tab).append("<th>").append(y).append("</th>").append(eol);
		}
		sb.append(tab).append(tab).append("</tr>").append(eol);
		sb.append(tab).append("</thead>").append(eol);
		sb.append(tab).append("<tbody>").append(eol);
		for (X x : m.getMatrix().keySet()) {
			sb.append(tab).append(tab).append("<tr>").append(eol);
			sb.append(tab).append(tab).append(tab).append("<td>").append(x).append("</td>").append(eol);
			for (Y y : headers) {
				V v = m.get(x, y);
				if (v == null) {
					sb.append(tab).append(tab).append(tab).append("<td>-</td>").append(eol);
				} else {
					sb.append(tab).append(tab).append(tab).append("<td>").append(v).append("</td>").append(eol);
				}
			}
			sb.append(tab).append(tab).append("</tr>").append(eol);
		}
		sb.append(tab).append("<tbody>").append(eol);
		sb.append("</table>");
		return sb.toString();
	}

}
