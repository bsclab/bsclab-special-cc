/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.dl;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.Configuration;

/**
 * Delta analysis job configuration
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class DeltaAnalysisJobConfiguration extends Configuration implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	public static final String IS_REPOSITORY = "brepo";
	public static final String IS_HEURISTIC_MODEL = "hmodel";
	public static final String IS_HBPMN_MODEL = "hbmodel";

	/**
	 * Expected repository URI
	 */
	private String repositoryURI;

	/**
	 * Observed repository URI
	 */
	private String compareToURI;

	/**
	 * Expected repository extension
	 */
	private String repositoryExtension = "brepo";

	/**
	 * Observed repository extension
	 */
	private String compareToExtension = "brepo";

	/**
	 * Mapping activity between two event log
	 */
	private Map<String, String> map = new LinkedHashMap<String, String>();

	/**
	 * Expected and observed label
	 */
	private Label label;

	/**
	 * Start time threshold
	 */
	private Double deltaStartTimeThreshold = 3d;

	/**
	 * Processing time (duration) threshold
	 */
	private Double deltaProcessingThreshold = 3d;

	/**
	 * Use trace level mapping if possible
	 */
	private int traceLevelMapping = 0;

	private int compareLogToLog = 0;

	public int getCompareLogToLog() {
		return compareLogToLog;
	}

	public void setCompareLogToLog(int compareLogToLog) {
		this.compareLogToLog = compareLogToLog;
	}

	public boolean isRepository() {
		return getRepositoryExtension().compareTo(IS_REPOSITORY) == 0;
	}

	public boolean isCompareToRepository() {
		return getCompareToExtension().compareTo(IS_REPOSITORY) == 0;
	}

	public String getRepositoryExtension() {
		return repositoryExtension;
	}

	public void setRepositoryExtension(String repositoryExtension) {
		this.repositoryExtension = repositoryExtension;
	}

	public String getCompareToExtension() {
		return compareToExtension;
	}

	public void setCompareToExtension(String compareToExtension) {
		this.compareToExtension = compareToExtension;
	}

	public int getTraceLevelMapping() {
		return traceLevelMapping;
	}

	public void setTraceLevelMapping(int traceLevelMapping) {
		this.traceLevelMapping = traceLevelMapping;
	}

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}

	public String getCompareToURI() {
		return compareToURI;
	}

	public void setCompareToURI(String compareToURI) {
		this.compareToURI = compareToURI;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public Double getDeltaProcessingThreshold() {
		return deltaProcessingThreshold;
	}

	public void setDeltaProcessingThreshold(Double deltaProcessingThreshold) {
		this.deltaProcessingThreshold = deltaProcessingThreshold;
	}

	public Double getDeltaStartTimeThreshold() {
		return deltaStartTimeThreshold;
	}

	public void setDeltaStartTimeThreshold(Double deltaStartTimeThreshold) {
		this.deltaStartTimeThreshold = deltaStartTimeThreshold;
	}
}
