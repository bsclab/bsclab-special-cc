/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.common.data.matrix.models;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Matrix2D<X, Y, V> implements IMatrix2D<X, Y, V> {

	protected final boolean sort;
	protected Map<X, Map<Y, V>> matrix;

	public Matrix2D() {
		sort = false;
	}

	public Matrix2D(boolean s) {
		sort = s;
	}

	@Override
	public void setMatrix(Map<X, Map<Y, V>> m) {
		getMatrix().clear();
		getMatrix().putAll(m);
	}

	@Override
	public Map<X, Map<Y, V>> getMatrix() {
		if (matrix == null) {
			if (isSorted()) {
				matrix = new TreeMap<X, Map<Y, V>>();
			} else {
				matrix = new LinkedHashMap<X, Map<Y, V>>();
			}
		}
		return matrix;
	}

	@Override
	public void get(X x, Map<Y, V> s) {
		getMatrix().put(x, s);
	}

	@Override
	public Map<Y, V> get(X x) {
		return getMatrix().get(x);
	}

	@Override
	public void set(X x, Y y, V v) {
		if (!getMatrix().containsKey(x)) {
			if (isSorted()) {
				getMatrix().put(x, new TreeMap<Y, V>());
			} else {
				getMatrix().put(x, new LinkedHashMap<Y, V>());
			}
		}
		getMatrix().get(x).put(y, v);
	}

	@Override
	public V get(X x, Y y) {
		if (getMatrix().containsKey(x) && getMatrix().get(x).containsKey(y)) {
			return getMatrix().get(x).get(y);
		}
		return null;
	}

	@Override
	public IMatrix2D<Y, X, V> transpose() {
		Matrix2D<Y, X, V> t = new Matrix2D<Y, X, V>(isSorted());
		for (X x : getMatrix().keySet()) {
			for (Y y : getMatrix().get(x).keySet()) {
				V v = getMatrix().get(x).get(y);
				t.set(y, x, v);
			}
		}
		return t;
	}

	@Override
	public boolean isSorted() {
		return sort;
	}
}
