/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@XmlRootElement(name = "task")
@XmlSeeAlso({ CallActivity.class })
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = CallActivity.class, name = CallActivity.TYPE) })
public class Task extends Element {

	public static final String TYPE = "task";

	public Task() {
		super();
		setType(TYPE);
	}

	public Task(String id, String name) {
		super(id, name);
		setType(TYPE);
	}

	private int completionQuantity = 1;
	private int startQuantity = 1;
	private boolean isForCompensation = false;

	public int getCompletionQuantity() {
		return completionQuantity;
	}

	@XmlAttribute(name = "completionQuantity")
	public void setCompletionQuantity(int completionQuantity) {
		this.completionQuantity = completionQuantity;
	}

	public int getStartQuantity() {
		return startQuantity;
	}

	@XmlAttribute(name = "startQuantity")
	public void setStartQuantity(int startQuantity) {
		this.startQuantity = startQuantity;
	}

	public boolean isForCompensation() {
		return isForCompensation;
	}

	@XmlAttribute(name = "forCompensation")
	public void setForCompensation(boolean isForCompensation) {
		this.isForCompensation = isForCompensation;
	}

	private ArrayList<String> incoming = new ArrayList<String>();
	private ArrayList<String> outgoing = new ArrayList<String>();

	public ArrayList<String> getIncoming() {
		return incoming;
	}

	public ArrayList<String> getOutgoing() {
		return outgoing;
	}

	@XmlElement(name = "incoming")
	public void setIncoming(ArrayList<String> incoming) {
		this.incoming = incoming;
	}

	@XmlElement(name = "outgoing")
	public void setOutgoing(ArrayList<String> outgoing) {
		this.outgoing = outgoing;
	}

}
