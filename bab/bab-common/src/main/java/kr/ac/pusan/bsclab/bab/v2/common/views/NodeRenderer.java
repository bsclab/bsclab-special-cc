/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.common.views;

import kr.ac.pusan.bsclab.bab.v2.common.process.graph.models.Arc;
import kr.ac.pusan.bsclab.bab.v2.common.process.graph.models.Node;

public class NodeRenderer extends AbstractRenderer<Node, String> {

	protected static final String NODE_PREFIX = "N_";
	protected static final String ARC_PREFIX = "A_";

	@Override
	public String render(Node i) {
		if (renderer != null) {
			return renderer.render(i);
		}
		StringBuilder dot = new StringBuilder();
		dot.append("digraph G {")
			.append(eol)
			.append(tab)
			.append("node[shape=\"rect\"]")
			.append(eol)
			.append(tab)
			.append("rankdir=LR;")
			.append(eol);
		dot.append(renderRecursive(i, tab, 0));
		dot.append("}")
			.append(eol);
		return dot.toString();
	}
	
	protected String renderRecursive(Node i, String t, int l) {
		StringBuilder dot = new StringBuilder();
		int h = i.getHeight();
		if (h > 1 && l > 0) {
			dot.append(t)
				.append("subgraph ")
				.append(NODE_PREFIX)
				.append(i.getId())
				.append(" {")
				.append(eol)
				.append(t)
				.append(tab)
				.append("color=black;")
				.append(eol);
		}
		if (h == 1) {
			dot.append(t)
				.append(NODE_PREFIX)
				.append(i.getId())
				.append(" [")
				.append("label=\"")
				.append(i.getLabel());
			for (String k : i.getAttributes().keySet()) {
				Object v = i.getAttributes().get(k);
				dot.append("\\n")
					.append(k)
					.append("=")
					.append(v);
			}
			dot.append("\"")
				.append(",shape=\"")
				.append(i.getType().getShape())
				.append("\"")
				.append("];")
				.append(eol);
		} else {
			for (Node n : i.getNodes().values()) {
				dot.append(renderRecursive(n, t + tab, l + 1));
			}
		}
		for (Arc a : i.getArcs().values()) {
			String dir = " -> ";
			dot.append(t)
				.append(tab)
				.append(NODE_PREFIX)
				.append(a.getSource().getId())
				.append(dir)
				.append(NODE_PREFIX)
				.append(a.getDestination().getId())
				.append(" [")
				.append("label=\"")
				.append(a.getLabel());
			for (String k : a.getAttributes().keySet()) {
				Object v = a.getAttributes().get(k);
				dot.append("\\n")
					.append(k)
					.append("=")
					.append(v);
			}
			dot.append("\"")
				.append(",dir=\"")
				.append(a.getType().getShape())
				.append("\"")
//				.append(",lhead=\"")
//				.append(NODE_PREFIX)
//				.append(a.getSource().getId())
//				.append("\"")
//				.append(",ltail=\"")
//				.append(NODE_PREFIX)
//				.append(a.getDestination().getId())
//				.append("\"")
				.append("];")
				.append(eol);
		}
		if (h > 1 && l > 0) {
			dot.append(t)
				.append("}")
				.append(eol);
		}
		return dot.toString();
	}

}
