package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

public class Arc {
	public Arc() {

	}

	private String source;
	private String target;
	private ArcFrequency frequency = new ArcFrequency();
	private Duration duration = new Duration();
	private double dependency;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public ArcFrequency getFrequency() {
		return frequency;
	}

	public void setFrequency(ArcFrequency frequency) {
		this.frequency = frequency;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	public double getDependency() {
		return dependency;
	}

	public void setDependency(double dependency) {
		this.dependency = dependency;
	}
}
