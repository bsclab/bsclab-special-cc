package kr.ac.pusan.bsclab.bab.ws.api.analysis.kpi.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.Configuration;
import kr.ac.pusan.bsclab.bab.ws.model.BEvent;

/**
 * Example configuration class from Test Algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class KpiParameters extends Configuration implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	private String repositoryURI;
	private List<KpiParameter> kpis = new ArrayList<KpiParameter>();

	public List<KpiParameter> getKpis() {
		return kpis;
	}

	public void setKpis(List<KpiParameter> kpis) {
		this.kpis = kpis;
	}

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}

	public KpiParameters() {
		KpiParameter k = new KpiParameter();

		k.setGroup("Performance");
		k.setName("Activity Count");
		k.setType(KpiParameter.KPI_TYPE_CAT_ORDINAL);
		k.getClassifiers().add(BEvent.DIM_EVENT_ACTIVITY);
		k.getAggregation().add(KpiParameter.KPI_AGGREGATION_COUNT);
		k.getAggregation().add(KpiParameter.KPI_AGGREGATION_COUNT_DISTINCT);
		k.setTarget(KpiParameter.KPI_TARGET_ACTIVITY);

		k.setGroup("Performance");
		k.setName("Transition Count");
		k.setType(KpiParameter.KPI_TYPE_CAT_ORDINAL);
		k.getClassifiers().add(BEvent.DIM_EVENT_ACTIVITY);
		k.getAggregation().add(KpiParameter.KPI_AGGREGATION_COUNT);
		k.getAggregation().add(KpiParameter.KPI_AGGREGATION_COUNT_DISTINCT);
		k.setTarget(KpiParameter.KPI_TARGET_TRANSITION);

		kpis.add(k);
	}
}
