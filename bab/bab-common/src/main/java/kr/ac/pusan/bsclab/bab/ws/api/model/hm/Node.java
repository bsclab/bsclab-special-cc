package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

public class Node {
	public Node() {

	}

	private String label;
	private Frequency frequency = new Frequency();
	private Duration duration = new Duration();

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Frequency getFrequency() {
		return frequency;
	}

	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	public Duration getDuration() {
		return duration;
	}

	public void setDuration(Duration duration) {
		this.duration = duration;
	}
}
