package kr.ac.pusan.bsclab.bab.ws.api.repository.ls;

import java.util.Map;
import java.util.TreeMap;
import kr.ac.pusan.bsclab.bab.ws.controller.DateUtil;

public class DCase {
	private String caseId;
	private int noOfEvents;
	private long started;
	private long finished;
	private long duration;
	private Map<String, String> noOfAttributes;

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public int getNoOfEvents() {
		return noOfEvents;
	}

	public void setNoOfEvents(int noOfEvents) {
		this.noOfEvents = noOfEvents;
	}

	public long getStarted() {
		return started;
	}

	public void setStarted(long started) {
		this.started = started;
	}

	public long getFinished() {
		return finished;
	}

	public void setFinished(long finished) {
		this.finished = finished;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getStartedStr() {
		return DateUtil.getInstance().toDateTimeString(started);
	}

	public String getFinishedStr() {
		return DateUtil.getInstance().toDateTimeString(finished);
	}

	public String getDurationStr() {
		return DateUtil.getInstance().toDurationString(duration);
	}

	public Map<String, String> getNoOfAttributes() {
		if (noOfAttributes == null) {
			noOfAttributes = new TreeMap<String, String>();
		}
		return noOfAttributes;
	}

	public void setNoOfAttributes(Map<String, String> noOfAttributes) {
		this.noOfAttributes = noOfAttributes;
	}
}
