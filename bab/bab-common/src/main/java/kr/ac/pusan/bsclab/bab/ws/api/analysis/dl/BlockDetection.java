package kr.ac.pusan.bsclab.bab.ws.api.analysis.dl;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.Arc;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.Node;

public class BlockDetection implements Serializable {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	final HeuristicModel model;
	final Model result;
	final Map<String, Integer> nodes;
	final int[][] arcs;

	public BlockDetection(HeuristicModel m, Model r) {
		model = m;
		result = r;
		nodes = new TreeMap<String, Integer>();
		int i = 0;
		for (Node n : m.getNodes().values()) {
			nodes.put(n.getLabel(), i++);
		}
		arcs = new int[nodes.size()][nodes.size()];
		for (Arc a : m.getArcs().values()) {
			arcs[nodes.get(a.getSource())][nodes.get(a.getTarget())] = 1;
		}
	}

	public void run() {
		int serial = 0;
		int paralel = 0;
		double dserial = 0d;
		double dparalel = 0d;
		for (int i = 0; i < arcs.length; i++) {
			int k = 0;
			for (int j = 0; j < arcs.length; j++) {
				if (arcs[i][j] == 1)
					k++;
			}
			if (k > 1) {
				paralel++;
			} else if (k == 1) {
				serial++;
			}
		}
		result.setNoOfSerialBlock(serial);
		result.setNoOfParalelBlock(paralel);
		dserial = serial;
		dparalel = paralel;
		double divider = dserial + dparalel + 1;
		result.setSerialization(dserial / divider);
		result.setParalelization(dparalel / divider);
	}

}
