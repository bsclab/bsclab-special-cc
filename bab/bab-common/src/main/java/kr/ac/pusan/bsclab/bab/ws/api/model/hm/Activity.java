package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

public class Activity implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private Dependency l1;
	private Map<Integer, Dependency> input;
	private Map<Integer, Dependency> output;
	private Map<Integer, Dependency> inputL2;
	private Map<Integer, Dependency> outputL2;

	public Activity() {
	}

	public Map<Integer, Dependency> getInputL2() {
		if (inputL2 == null) {
			inputL2 = new LinkedHashMap<Integer, Dependency>();
		}
		return inputL2;
	}

	public void setInputL2(Map<Integer, Dependency> inputL2) {
		this.inputL2 = inputL2;
	}

	public Map<Integer, Dependency> getOutputL2() {
		if (outputL2 == null) {
			outputL2 = new LinkedHashMap<Integer, Dependency>();
		}
		return outputL2;
	}

	public void setOutputL2(Map<Integer, Dependency> outputL2) {
		this.outputL2 = outputL2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Dependency getL1() {
		return l1;
	}

	public void setL1(Dependency l1) {
		this.l1 = l1;
	}

	public Map<Integer, Dependency> getInput() {
		if (input == null) {
			input = new LinkedHashMap<Integer, Dependency>();
		}
		return input;
	}

	public void setInput(Map<Integer, Dependency> input) {
		this.input = input;
	}

	public Map<Integer, Dependency> getOutput() {
		if (output == null) {
			output = new LinkedHashMap<Integer, Dependency>();
		}
		return output;
	}

	public void setOutput(Map<Integer, Dependency> output) {
		this.output = output;
	}

}
