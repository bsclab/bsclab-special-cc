/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.legacy.factories;

import java.util.LinkedHashMap;
import java.util.Map;

import kr.ac.pusan.bsclab.bab.v2.common.process.graph.models.Arc;
import kr.ac.pusan.bsclab.bab.v2.common.process.graph.models.Node;
import kr.ac.pusan.bsclab.bab.v2.common.process.graph.models.NodeType;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.SocialNetworkAnalysis;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.TimeGapAnalysisJobModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.Transition;
import kr.ac.pusan.bsclab.bab.ws.api.model.fm.FuzzyModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Element;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Event;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Gateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.SequenceFlow;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Task;

public class NodeFactory {

	public static Node create(Object o) {
		if (o instanceof HeuristicModel) {
			return createFromHeuristicsModel((HeuristicModel) o);
		}
		if (o instanceof FuzzyModel) {
			return createFromFuzzyModel((FuzzyModel) o);
		}
		if (o instanceof Model) {
			return createFromBpmnModel((Model) o);
		}
		if (o instanceof SocialNetworkAnalysis) {
			return createFromSocialNetwork((SocialNetworkAnalysis) o);
		}
		if (o instanceof TimeGapAnalysisJobModel) {
			return createFromTimegapModel((TimeGapAnalysisJobModel) o);
		}
		return null;
	}

	public static Node createFromHeuristicsModel(HeuristicModel hm) {
		Node p = new Node(hm.getId(), hm.getUri(), NodeType.PROCESS);
		p.setUri(hm.getUri());

		Map<String, Node> nmap = new LinkedHashMap<String, Node>();

		for (String i : hm.getNodes().keySet()) {
			kr.ac.pusan.bsclab.bab.ws.api.model.hm.Node hn = hm.getNodes().get(i);
			Node n = new Node(i, hn.getLabel());
			n.getAttributes().put("nevents", hn.getFrequency().getAbsolute());
			n.getAttributes().put("ncases", hn.getFrequency().getCases());
			p.getNodes().put(n.getId(), n);
			nmap.put(i, n);
		}

		for (String i : hm.getArcs().keySet()) {
			kr.ac.pusan.bsclab.bab.ws.api.model.hm.Arc ha = hm.getArcs().get(i);
			Arc a = new Arc(i, i, nmap.get(ha.getSource()), nmap.get(ha.getTarget()));
			a.getAttributes().put("ntrans", ha.getFrequency().getAbsolute());
			a.getAttributes().put("dep", ha.getDependency());
			p.getArcs().put(a.getId(), a);
		}

		return p;
	}

	public static Node createFromFuzzyModel(FuzzyModel fm) {
		Node p = new Node(fm.getId(), fm.getUri(), NodeType.PROCESS);
		p.setUri(fm.getUri());

		Map<String, Node> nmap = new LinkedHashMap<String, Node>();

		for (String i : fm.getNodes().keySet()) {
			kr.ac.pusan.bsclab.bab.ws.api.model.fm.Node hn = fm.getNodes().get(i);
			Node n = new Node(i, hn.getLabel());
			n.getAttributes().put("nevents", hn.getFrequency().getAbsolute());
			n.getAttributes().put("sig", hn.getSignificance());
			p.getNodes().put(n.getId(), n);
			nmap.put(i, n);
		}

		for (String i : fm.getArcs().keySet()) {
			kr.ac.pusan.bsclab.bab.ws.api.model.fm.Arc ha = fm.getArcs().get(i);
			Arc a = new Arc(i, i, nmap.get(ha.getSource()), nmap.get(ha.getTarget()));
			a.getAttributes().put("ntrans", ha.getFrequency().getAbsolute());
			a.getAttributes().put("sig", ha.getSignificance());
			p.getArcs().put(a.getId(), a);
		}

		return p;
	}

	public static Node createFromBpmnModel(Model bm) {
		Node p = new Node(bm.getId(), bm.getUri(), NodeType.PROCESS);
		p.setUri(bm.getUri());

		Map<String, Node> nmap = new LinkedHashMap<String, Node>();

		for (Element e : bm.getProcess().getElements()) {
			if (!(e instanceof SequenceFlow)) {
				Node n = new Node(e.getId(), e.getName());
				if (e instanceof Event) {
					n.setType(NodeType.EVENT);
				}
				if (e instanceof Task) {
					n.setType(NodeType.ACTIVITY);
				}
				if (e instanceof Gateway) {
					n.setType(NodeType.GATEWAY);
				}
				p.getNodes().put(n.getId(), n);
				nmap.put(e.getId(), n);
			}
		}
		for (Element e : bm.getProcess().getElements()) {
			if (e instanceof SequenceFlow) {
				Arc a = new Arc(e.getId(), e.getName(), nmap.get(((SequenceFlow) e).getSourceRef()),
						nmap.get(((SequenceFlow) e).getTargetRef()));
				p.getArcs().put(a.getId(), a);
			}
		}

		return p;
	}

	public static Node createFromSocialNetwork(SocialNetworkAnalysis sna) {
		Node p = new Node(sna.getId(), sna.getUri(), NodeType.PROCESS);
		p.setUri(sna.getUri());

		Map<String, Node> nmap = new LinkedHashMap<String, Node>();

		for (String i : sna.getNodes().keySet()) {
			kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.Node hn = sna.getNodes().get(i);
			Node n = new Node(i, hn.getLabel());
			n.getAttributes().put("nevents", hn.getFrequency().getAbsolute());
			n.getAttributes().put("ncases", hn.getFrequency().getCases());
			p.getNodes().put(n.getId(), n);
			nmap.put(i, n);
		}

		for (String i : sna.getArcs().keySet()) {
			kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.Arc ha = sna.getArcs().get(i);
			if (ha.getFrequency().getAbsolute() == 0 && ha.getFrequency().getRelative() == 0)
				continue;
			Arc a = new Arc(i, i, nmap.get(ha.getSource()), nmap.get(ha.getTarget()));
			a.getAttributes().put("ntrans", ha.getFrequency().getAbsolute());
			a.getAttributes().put("nstr", ha.getFrequency().getRelative());
			p.getArcs().put(a.getId(), a);
		}

		return p;
	}

	public static Node createFromTimegapModel(TimeGapAnalysisJobModel tm) {
		Node p = new Node(tm.getUri(), tm.getUri(), NodeType.PROCESS);
		p.setUri(tm.getUri());

		Map<String, Node> nmap = new LinkedHashMap<String, Node>();

		for (String i : tm.getNodes().keySet()) {
			Node n = new Node(i, i);
			p.getNodes().put(n.getId(), n);
			nmap.put(i, n);
		}

		for (String i : tm.getTransitions().keySet()) {
			Transition t = tm.getTransitions().get(i);
			Arc a = new Arc(i, i, nmap.get(t.getSource()), nmap.get(t.getTarget()));
			a.getAttributes().put("csuc", t.getNoOfSuccessiveCases());
			a.getAttributes().put("msuc", t.getMeanSuccessive());
			a.getAttributes().put("cnsuc", t.getNoOfNonSuccessiveCases());
			a.getAttributes().put("mnsuc", t.getMeanNonSuccessive());
			p.getArcs().put(a.getId(), a);
		}

		return p;
	}
}
