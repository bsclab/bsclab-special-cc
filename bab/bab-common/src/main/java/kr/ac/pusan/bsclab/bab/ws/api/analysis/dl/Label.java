package kr.ac.pusan.bsclab.bab.ws.api.analysis.dl;

import java.io.Serializable;

public class Label implements Serializable {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private String source;
	private String target;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
}
