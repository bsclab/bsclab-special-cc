/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.model;

import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;

public class TimeGapEntry implements Comparable<TimeGapEntry> {
	/**
	 * contains boolean value of successive or non successive entry
	 */
	private boolean successive;

	/**
	 * contains object value of start activity
	 */
	private IEvent startActivity;

	/**
	 * contains object value of end activity
	 */
	private IEvent endActivity;

	/**
	 * contains time gap between start and end activity
	 */
	private long gap;

	public IEvent getStartActivity() {
		return startActivity;
	}

	public void setStartActivity(IEvent startActivity) {
		this.startActivity = startActivity;
	}

	public IEvent getEndActivity() {
		return endActivity;
	}

	public void setEndActivity(IEvent endActivity) {
		this.endActivity = endActivity;
	}

	public long getGap() {
		gap = endActivity.getTimestamp() - startActivity.getTimestamp();
		return gap;
	}

	public void setGap(long gap) {
		this.gap = gap;
	}

	public String getStrGap() {
		int seconds = (int) (getGap() / 1000) % 60;
		int minutes = (int) ((getGap() / (1000 * 60)) % 60);
		int hours = (int) ((getGap() / (1000 * 60 * 60)) % 24);
		int days = (int) (getGap() / (1000 * 60 * 60 * 24));

		String day = "";
		if (days > 0) {
			day = String.format("%02d", days) + "D;";
		}

		return day + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":"
				+ String.format("%02d", seconds);
	}

	public boolean isSuccessive() {
		return successive;
	}

	public void setSuccessive(boolean successive) {
		this.successive = successive;
	}

	@Override
	public int compareTo(TimeGapEntry o) {
		if (startActivity.getId().equalsIgnoreCase(o.getStartActivity().getId())
				&& startActivity.getType().equalsIgnoreCase(o.getStartActivity().getType())
				&& endActivity.getId().equalsIgnoreCase(o.getEndActivity().getId())
				&& endActivity.getType().equalsIgnoreCase(o.getEndActivity().getType()))
			return 0;
		return -1;
	}

	public String toString() {
		return getStrGap();
	}

}
