package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg;

public class Case {

	/**
	 * contains string value of process instance (case) ID
	 */
	public String label;

	/**
	 * contains string value of time gap
	 */
	public String gap;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getGap() {
		return gap;
	}

	public void setGap(String gap) {
		this.gap = gap;
	}

}
