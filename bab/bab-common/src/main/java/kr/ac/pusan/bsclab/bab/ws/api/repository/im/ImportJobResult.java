/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.im;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import kr.ac.pusan.bsclab.bab.ws.api.Result;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;

/**
 * Immediate repository file as a result for import repository job
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class ImportJobResult extends Result implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String description;
	private String uri;
	private List<IRepository> repositories;

	private String repositoryURI = "";
	private String rawPath = "";
	private String originalFormat = "";
	private Map<String, Map<String, Integer>> dimensions = new LinkedHashMap<String, Map<String, Integer>>();

	public Map<String, Integer> getOrAddDimension(String dimension) {
		if (!dimensions.containsKey(dimension)) {
			dimensions.put(dimension, new LinkedHashMap<String, Integer>());
		}
		return dimensions.get(dimension);
	}

	public Integer increaseDimensionalStateFrequency(String dimension, String state) {
		Map<String, Integer> states = getOrAddDimension(dimension);
		if (states.size() >= 50)
			return 0;
		if (!states.containsKey(state)) {
			states.put(state, 0);
		}
		states.put(state, states.get(state) + 1);
		return states.get(state);
	}

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}

	public Map<String, Map<String, Integer>> getDimensions() {
		return dimensions;
	}

	public void setDimensions(Map<String, Map<String, Integer>> dimensions) {
		this.dimensions = dimensions;
	}

	public void debugDimensionalStates() {
		for (String dimension : getDimensions().keySet()) {
			Map<String, Integer> dimensionalStates = getDimensions().get(dimension);
			System.err.println("Dimension [" + dimension + "] : ");
			for (String state : dimensionalStates.keySet()) {
				int frequency = dimensionalStates.get(state);
				System.err.println("- " + state + " : " + frequency);
			}
		}
	}

	public String getRawPath() {
		return rawPath;
	}

	public void setRawPath(String rawPath) {
		this.rawPath = rawPath;
	}

	public String getOriginalFormat() {
		return originalFormat;
	}

	public void setOriginalFormat(String originalFormat) {
		this.originalFormat = originalFormat;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public List<IRepository> getRepositories() {
		if (repositories == null) {
			repositories = new ArrayList<IRepository>();
		}
		return repositories;
	}

	public void setRepositories(List<IRepository> repositories) {
		this.repositories = repositories;
	}

}
