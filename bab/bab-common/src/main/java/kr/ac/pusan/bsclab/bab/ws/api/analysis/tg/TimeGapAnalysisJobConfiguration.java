/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.Configuration;

/**
 * Time gap analysis job configuration
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class TimeGapAnalysisJobConfiguration extends Configuration implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Repository URI
	 */
	private String repositoryURI;

	/**
	 * Boolean value for successive (direct) only transition
	 */
	private boolean successiveOnly;

	/**
	 * Selected process instances for multi dimensional time gap analysis (MTGA)
	 * - deprecated
	 */
	private Map<String, String> selectedPIs = new HashMap<String, String>();

	/**
	 * Start activity with concatenation value of activity name and event type,
	 * i.e. 'Activity Name (Event type)'
	 */
	private String startActivity;

	/**
	 * End activity with concatenation value of activity name and event type,
	 * i.e. 'Activity Name (Event type)'
	 */
	private String endActivity;

	/**
	 * List of process instances
	 */
	private List<String> caseIds;

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}

	public boolean isSuccessiveOnly() {
		return successiveOnly;
	}

	public void setSuccessiveOnly(boolean successiveOnly) {
		this.successiveOnly = successiveOnly;
	}

	public Map<String, String> getSelectedPIs() {
		return selectedPIs;
	}

	public void setSelectedPIs(Map<String, String> selectedPIs) {
		this.selectedPIs = selectedPIs;
	}

	public String getStartActivity() {
		return startActivity;
	}

	public void setStartActivity(String startActivity) {
		this.startActivity = startActivity;
	}

	public String getEndActivity() {
		return endActivity;
	}

	public void setEndActivity(String endActivity) {
		this.endActivity = endActivity;
	}

	public List<String> getCaseIds() {
		return caseIds;
	}

	public void setCaseIds(List<String> caseIds) {
		this.caseIds = caseIds;
	}

}
