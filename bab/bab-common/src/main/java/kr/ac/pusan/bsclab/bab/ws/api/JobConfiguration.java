package kr.ac.pusan.bsclab.bab.ws.api;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JobConfiguration {

	protected String jobId;
	protected String jobClass;
	protected String workspaceId;
	protected String datasetId;
	protected String repositoryId;
	protected String path;
	protected String ext;
	protected String jobPath;
	protected String resourcePath;
	protected String resultDir;
	protected String resultPath;
	protected String configuration;
	protected Timestamp sdt;
	protected Timestamp edt;
	protected Map<String, String> configurations;
	protected List<String[]> jobs = new ArrayList<String[]>();

	public String getJobId() {
		return jobId;
	}

	public String getJobClass() {
		return jobClass;
	}

	public String getWorkspaceId() {
		return workspaceId;
	}

	public String getDatasetId() {
		return datasetId;
	}

	public String getRepositoryId() {
		return repositoryId;
	}

	public String getPath() {
		return path;
	}

	public String getExt() {
		return ext;
	}

	public String getJobPath() {
		return jobPath;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public String getResultDir() {
		return resultDir;
	}

	public String getResultPath() {
		return resultPath;
	}

	public String getConfiguration() {
		return configuration;
	}

	public Timestamp getSdt() {
		return sdt;
	}

	public Timestamp getEdt() {
		return edt;
	}

	public Map<String, String> getConfigurations() {
		return configurations;
	}

	public List<String[]> getJobs() {
		return jobs;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public void setWorkspaceId(String workspaceId) {
		this.workspaceId = workspaceId;
	}

	public void setDatasetId(String datasetId) {
		this.datasetId = datasetId;
	}

	public void setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public void setJobPath(String jobPath) {
		this.jobPath = jobPath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public void setResultDir(String resultDir) {
		this.resultDir = resultDir;
	}

	public void setResultPath(String resultPath) {
		this.resultPath = resultPath;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	public void setSdt(Timestamp sdt) {
		this.sdt = sdt;
	}

	public void setEdt(Timestamp edt) {
		this.edt = edt;
	}

	public void setConfigurations(Map<String, String> configurations) {
		this.configurations = configurations;
	}

	public void setJobs(List<String[]> jobs) {
		this.jobs = jobs;
	}

}
