package kr.ac.pusan.bsclab.bab.ws.api.repository.ls;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Dimension {
	private String dimension = "";
	private Map<String, Integer> frequency;
	private Map<String, Double> medianDuration;
	private Map<String, Double> meanDuration;
	private Map<String, Double> durationRange;
	private Map<String, Double> aggregateDuration;
	private Map<String, State> states = new LinkedHashMap<String, State>();

	public Map<String, Integer> getFrequency() {
		if (frequency == null) {
			frequency = new TreeMap<String, Integer>();
			for (String state : states.keySet()) {
				frequency.put(state, states.get(state).getFrequency());
			}
		}
		return frequency;
	}

	public Map<String, Double> getMedianDuration() {
		if (medianDuration == null) {
			medianDuration = new TreeMap<String, Double>();
			for (String state : states.keySet()) {
				medianDuration.put(state, states.get(state).getDurationRange());
			}
		}
		return medianDuration;
	}

	public Map<String, Double> getMeanDuration() {
		if (meanDuration == null) {
			meanDuration = new TreeMap<String, Double>();
			for (String state : states.keySet()) {
				meanDuration.put(state, states.get(state).getMeanDuration());
			}
		}
		return meanDuration;
	}

	public Map<String, Double> getDurationRange() {
		if (durationRange == null) {
			durationRange = new TreeMap<String, Double>();
			for (String state : states.keySet()) {
				durationRange.put(state, states.get(state).getDurationRange());
			}
		}
		return durationRange;
	}

	public Map<String, Double> getAggregateDuration() {
		if (aggregateDuration == null) {
			aggregateDuration = new TreeMap<String, Double>();
			for (String state : states.keySet()) {
				aggregateDuration.put(state, states.get(state).getSumDuration());
			}
		}
		return aggregateDuration;
	}

	public void setMedianDuration(Map<String, Double> medianDuration) {
		this.medianDuration = medianDuration;
	}

	public void setMeanDuration(Map<String, Double> meanDuration) {
		this.meanDuration = meanDuration;
	}

	public void setDurationRange(Map<String, Double> durationRange) {
		this.durationRange = durationRange;
	}

	public void setAggregateDuration(Map<String, Double> aggregateDuration) {
		this.aggregateDuration = aggregateDuration;
	}

	public State getState(String state) {
		if (states.containsKey(state)) {
			return states.get(state);
		}
		State ns = new State();
		ns.setLabel(state);
		states.put(state, ns);
		return ns;
	}

	public String getDimension() {
		return dimension;
	}

	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	public Map<String, State> getStates() {
		return states;
	}

	public void setStates(Map<String, State> states) {
		this.states = states;
	}

	public void calculateRelativeFrequency(int totalEvents) {
		for (State s : states.values()) {
			if (totalEvents != 0)
				s.setRelativeFrequency(s.getFrequency() / totalEvents);
		}

	}

	public void setFrequency(Map<String, Integer> frequencies) {
		this.frequency = frequencies;
	}

}
