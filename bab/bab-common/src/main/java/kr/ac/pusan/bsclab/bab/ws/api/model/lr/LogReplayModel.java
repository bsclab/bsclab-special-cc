/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com), Dzulfikar Adi Putra (dzulfikar.adiputra@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.lr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

/**
 * Log Replay model as result from Log Replay algorithm
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class LogReplayModel extends Result implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Number of cases
	 */
	private int casesNumber;

	/**
	 * Number of events
	 */
	private int eventsNumber;

	/**
	 * Event offset
	 */
	private int offset;

	/**
	 * Start time
	 */
	private long startTime;

	/**
	 * End time
	 */
	private long endTime;

	/**
	 * Duration
	 */
	private long duration;

	/**
	 * Part request URI
	 */
	private String requestURI;

	/**
	 * Partitions information
	 */
	private List<EventPartition> eventPartitions = new ArrayList<EventPartition>();

	/**
	 * Event count for given timestamp
	 */
	private Map<Long, Integer> eventCount = new TreeMap<Long, Integer>();

	public LogReplayModel() {
	}

	public Map<Long, Integer> getEventCount() {
		return eventCount;
	}

	public void setEventCount(Map<Long, Integer> eventCount) {
		this.eventCount = eventCount;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public LogReplayModel addPartition(EventPartition event) {
		eventPartitions.addAll(Arrays.asList(event));
		return this;
	}

	public List<EventPartition> getEventPartitions() {
		return eventPartitions;
	}

	public void setEventPartitions(List<EventPartition> eventPartitions) {
		this.eventPartitions = eventPartitions;
	}

	public int getCasesNumber() {
		return casesNumber;
	}

	public void setCasesNumber(int casesNumber) {
		this.casesNumber = casesNumber;
	}

	public int getEventsNumber() {
		return eventsNumber;
	}

	public void setEventsNumber(int eventsNumber) {
		this.eventsNumber = eventsNumber;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getRequestURI() {
		return requestURI;
	}

	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}
}
