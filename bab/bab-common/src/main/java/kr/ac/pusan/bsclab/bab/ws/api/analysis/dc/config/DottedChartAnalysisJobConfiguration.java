/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.config;

import java.io.Serializable;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.Configuration;

public class DottedChartAnalysisJobConfiguration extends Configuration implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Repository URI
	 */
	private String repositoryURI;

	/**
	 * Component dimension
	 */
	private String component = "CASE";

	/**
	 * Shape dimension
	 */
	private String shape = "TYPE";

	/**
	 * Color dimension
	 */
	private String color = "ACTIVITY";

	/**
	 * Time dimension format
	 */
	private String timeFormat = "yyyyMMddHHmmss";

	/**
	 * Number of time split
	 */
	private int timeSplit = 10;

	/**
	 * Use relative time to the first event
	 */
	private boolean timeRelative = false;

	public DottedChartAnalysisJobConfiguration() {

	}

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	public String getShape() {
		return shape;
	}

	public void setShape(String shape) {
		this.shape = shape;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTimeFormat() {
		return timeFormat;
	}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	public int getTimeSplit() {
		return timeSplit;
	}

	public void setTimeSplit(int timeSplit) {
		this.timeSplit = timeSplit;
	}

	public boolean isTimeRelative() {
		return timeRelative;
	}

	public void setTimeRelative(boolean timeRelative) {
		this.timeRelative = timeRelative;
	}

}
