/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy
 * (wanprabu@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class TaskMatrixModel implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private Set<HeatCasesModel> nodes = new TreeSet<HeatCasesModel>();
	private List<Link> links = new ArrayList<Link>();
	private int minFreq;
	private int maxFreq;
	private double avgFreq;

	public TaskMatrixModel addNode(HeatCasesModel e) {
		nodes.add((e));
		return this;
	}

	public TaskMatrixModel addLink(Link e) {
		links.addAll(Arrays.asList(e));
		return this;
	}

	public Set<HeatCasesModel> getNodes() {
		return nodes;
	}

	public void setNodes(Set<HeatCasesModel> nodes) {
		this.nodes = nodes;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public int getMinFreq() {
		return minFreq;
	}

	public void setMinFreq(int minFreq) {
		this.minFreq = minFreq;
	}

	public int getMaxFreq() {
		return maxFreq;
	}

	public void setMaxFreq(int maxFreq) {
		this.maxFreq = maxFreq;
	}

	public double getAvgFreq() {
		return avgFreq;
	}

	public void setAvgFreq(double avgFreq) {
		this.avgFreq = avgFreq;
	}
}
