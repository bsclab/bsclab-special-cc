/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.legacy.factories;

import kr.ac.pusan.bsclab.bab.v2.common.data.matrix.models.Matrix2D;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models.Data;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models.DottedChartModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.SocialNetworkAnalysis;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.TimeGapAnalysisJobModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.Transition;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.HeatMapModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.Link;
import kr.ac.pusan.bsclab.bab.ws.api.model.fm.FuzzyModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.Event;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.PerformanceChartAnalysis;

public class Matrix2DFactory {

	public static final String SEP = " | ";

	public static Matrix2D<String, String, String> create(Object o) {
		if (o instanceof HeuristicModel) {
			return createFromHeuristicsModel((HeuristicModel) o);
		}
		if (o instanceof FuzzyModel) {
			return createFromFuzzyModel((FuzzyModel) o);
		}
		if (o instanceof DottedChartModel) {
			return createFromDottedChartModel((DottedChartModel) o);
		}
		if (o instanceof HeatMapModel) {
			return createFromHeatMapModel((HeatMapModel) o);
		}
		if (o instanceof SocialNetworkAnalysis) {
			return createFromSocialNetwork((SocialNetworkAnalysis) o);
		}
		if (o instanceof PerformanceChartAnalysis) {
			return createFromPerformanceChart((PerformanceChartAnalysis) o);
		}
		if (o instanceof TimeGapAnalysisJobModel) {
			return createFromTimegapModel((TimeGapAnalysisJobModel) o);
		}
		return null;
	}

	public static Matrix2D<String, String, String> createFromHeuristicsModel(HeuristicModel hm) {
		Matrix2D<String, String, String> m = new Matrix2D<String, String, String>(true);
		for (kr.ac.pusan.bsclab.bab.ws.api.model.hm.Arc i : hm.getArcs().values()) {
			m.set(i.getSource(), i.getTarget(), i.getFrequency().getAbsolute() + SEP + i.getDependency());
		}
		return m;
	}

	public static Matrix2D<String, String, String> createFromFuzzyModel(FuzzyModel fm) {
		Matrix2D<String, String, String> m = new Matrix2D<String, String, String>(true);
		for (kr.ac.pusan.bsclab.bab.ws.api.model.fm.Arc i : fm.getArcs().values()) {
			m.set(i.getSource(), i.getTarget(), i.getFrequency().getAbsolute() + SEP + i.getSignificance());
		}
		return m;
	}

	public static Matrix2D<String, String, String> createFromDottedChartModel(DottedChartModel dm) {
		Matrix2D<String, String, String> m = new Matrix2D<String, String, String>();
		for (String r : dm.getData().keySet()) {
			for (Long t : dm.getData().get(r).keySet()) {
				Data d = dm.getData().get(r).get(t);
				m.set(r, String.valueOf(t), d.getName() + SEP + d.getType());
			}
		}
		return m;
	}

	public static Matrix2D<String, String, String> createFromHeatMapModel(HeatMapModel hm) {
		Matrix2D<String, String, String> m = new Matrix2D<String, String, String>(true);
		for (Link l : hm.getHeatList()) {
			m.set(l.getSource(), l.getTarget(), String.valueOf(l.getValue()));
		}
		return m;
	}

	public static Matrix2D<String, String, String> createFromSocialNetwork(SocialNetworkAnalysis sm) {
		Matrix2D<String, String, String> m = new Matrix2D<String, String, String>(true);
		for (String i : sm.getArcs().keySet()) {
			kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.Arc ha = sm.getArcs().get(i);
			m.set(ha.getSource(), ha.getTarget(), String.valueOf(ha.getFrequency().getAbsolute()) + SEP
					+ String.valueOf(ha.getFrequency().getRelative()));
		}
		return m;
	}

	public static Matrix2D<String, String, String> createFromPerformanceChart(PerformanceChartAnalysis pm) {
		Matrix2D<String, String, String> m = new Matrix2D<String, String, String>();
		for (Event e : pm.getMatrix().values()) {
			m.set(e.getLabel(), e.getX(), String.valueOf(e.getY()) + SEP + e.getZ());
		}
		return m;
	}

	public static Matrix2D<String, String, String> createFromTimegapModel(TimeGapAnalysisJobModel tm) {
		Matrix2D<String, String, String> m = new Matrix2D<String, String, String>(true);
		for (Transition t : tm.getTransitions().values()) {
			m.set(t.getSource(), t.getTarget(), t.getNoOfSuccessiveCases() + SEP + t.getMeanSuccessive() + SEP
					+ t.getNoOfNonSuccessiveCases() + SEP + t.getMeanNonSuccessive());
		}
		return m;
	}
}
