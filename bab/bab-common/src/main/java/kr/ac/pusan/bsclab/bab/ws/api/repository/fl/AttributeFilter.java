package kr.ac.pusan.bsclab.bab.ws.api.repository.fl;

public abstract class AttributeFilter<I, O> extends RepositoryFilter<I, O> {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
public static final String NAME = "VARIATION";

  @Override
  public String getName() {
    return NAME;
  }

  public AttributeFilter() {
    getOptions().put("min", String.valueOf(Long.MIN_VALUE));
    getOptions().put("max", String.valueOf(Long.MAX_VALUE));
  }

  public long getMin() {
    return Long.parseLong(getOptions().get("min"));
  }

  public void setMinValue(long min) {
    getOptions().put("min", String.valueOf(min));
  }

  public long getMax() {
    return Long.parseLong(getOptions().get("max"));
  }

  public void setMaxValue(long max) {
    getOptions().put("min", String.valueOf(max));
  }

}
