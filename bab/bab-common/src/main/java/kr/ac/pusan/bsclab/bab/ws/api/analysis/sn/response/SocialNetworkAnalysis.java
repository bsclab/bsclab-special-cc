/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import kr.ac.pusan.bsclab.bab.ws.api.Result;

/**
 * Social network analysis result
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SocialNetworkAnalysis extends Result implements Serializable {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Result Id
	 */
	private String id = "";

	/**
	 * List of nodes and its properties (frequency)
	 */
	private Map<String, Node> nodes = new LinkedHashMap<String, Node>();

	/**
	 * List of arcs and its properties (frequency)
	 */
	private Map<String, Arc> arcs = new LinkedHashMap<String, Arc>();

	/**
	 * List of originators
	 */
	private List<String> originators = new LinkedList<String>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Node> getNodes() {
		return nodes;
	}

	public void setNodes(Map<String, Node> nodes) {
		this.nodes = nodes;
	}

	public Map<String, Arc> getArcs() {
		return arcs;
	}

	public void setArcs(Map<String, Arc> arcs) {
		this.arcs = arcs;
	}

	public List<String> getOriginators() {
		return originators;
	}

	public void setOriginators(List<String> originators) {
		this.originators = originators;
	}

}
