package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg;

import java.util.HashMap;
import java.util.Map;

public class Transition {
	/**
	 * contains start activity with concatenation value of activity name and
	 * event type, i.e. 'Activity Name (Event type)'
	 */
	public String source;

	/**
	 * contains end activity with concatenation value of activity name and event
	 * type, i.e. 'Activity Name (Event type)'
	 */
	public String target;

	/**
	 * contains a map of string case id and an case objects for a list of case
	 * if for successive (direct) transition
	 */
	public Map<String, Case> caseIDSuccessive = new HashMap<String, Case>();

	/**
	 * contains a map of string case id and an case objects for a list of case
	 * if for non successive (indirect) transition
	 */
	public Map<String, Case> caseIDNonSuccessive = new HashMap<String, Case>();

	/**
	 * contains count of successive case
	 */
	public int noOfSuccessiveCases;

	/**
	 * contains count of non successive case
	 */
	public int noOfNonSuccessiveCases;

	/**
	 * contains a string format of mean for successive case
	 */
	public String meanSuccessive;

	/**
	 * contains a string format of standard deviation for successive case
	 */
	public String stdDevSuccessive;

	/**
	 * contains a string format of mean for non successive case
	 */
	public String meanNonSuccessive;

	/**
	 * contains a string format of standard deviation for non successive case
	 */
	public String stdDevNonSuccessive;

	/**
	 * contains a mean value for all cases
	 */
	public int meanRaw;

	public Case setOrGetSuccessive(String caseID) {
		Case c = caseIDSuccessive.get(caseID);
		if (c == null) {
			c = new Case();
			c.setLabel(caseID);
		}

		caseIDSuccessive.put(caseID, c);
		return c;
	}

	public Case setOrGetNonSuccessive(String caseID) {
		Case c = caseIDNonSuccessive.get(caseID);
		if (c == null) {
			c = new Case();
			c.setLabel(caseID);
		}

		caseIDNonSuccessive.put(caseID, c);
		return c;
	}

	public String getMeanSuccessive() {
		return meanSuccessive;
	}

	public void setMeanSuccessive(String meanSuccessive) {
		this.meanSuccessive = meanSuccessive;
	}

	public String getStdDevSuccessive() {
		return stdDevSuccessive;
	}

	public void setStdDevSuccessive(String stdDevSuccessive) {
		this.stdDevSuccessive = stdDevSuccessive;
	}

	public String getMeanNonSuccessive() {
		return meanNonSuccessive;
	}

	public void setMeanNonSuccessive(String meanNonSuccessive) {
		this.meanNonSuccessive = meanNonSuccessive;
	}

	public String getStdDevNonSuccessive() {
		return stdDevNonSuccessive;
	}

	public void setStdDevNonSuccessive(String stdDevNonSuccessive) {
		this.stdDevNonSuccessive = stdDevNonSuccessive;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String startActivity) {
		this.source = startActivity;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String endActivity) {
		this.target = endActivity;
	}

	public Map<String, Case> getCaseIDSuccessive() {
		return caseIDSuccessive;
	}

	public void setCaseIDSuccessive(Map<String, Case> caseIDSuccessive) {
		this.caseIDSuccessive = caseIDSuccessive;
	}

	public Map<String, Case> getCaseIDNonSuccessive() {
		return caseIDNonSuccessive;
	}

	public void setCaseIDNonSuccessive(Map<String, Case> caseIDNonSuccessive) {
		this.caseIDNonSuccessive = caseIDNonSuccessive;
	}

	public int getNoOfSuccessiveCases() {
		// noOfSuccessiveCases = caseIDSuccessive.size();
		return noOfSuccessiveCases;
	}

	public void setNoOfSuccessiveCases(int noOfSuccessiveCases) {
		this.noOfSuccessiveCases = noOfSuccessiveCases;
	}

	public int getNoOfNonSuccessiveCases() {
		// noOfNonSuccessiveCases = caseIDNonSuccessive.size() +
		// caseIDSuccessive.size();
		return noOfNonSuccessiveCases;
	}

	public void setNoOfNonSuccessiveCases(int noOfNonSuccessiveCases) {
		this.noOfNonSuccessiveCases = noOfNonSuccessiveCases;
	}

	public int getMeanRaw() {
		return meanRaw;
	}

	public void setMeanRaw(int meanRaw) {
		this.meanRaw = meanRaw;
	}

}
