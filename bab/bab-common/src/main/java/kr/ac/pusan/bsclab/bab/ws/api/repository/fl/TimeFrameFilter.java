package kr.ac.pusan.bsclab.bab.ws.api.repository.fl;

public abstract class TimeFrameFilter<I, O> extends RepositoryFilter<I, O> {
  private static final long serialVersionUID = 1L;

  public static final String NAME = "TIMEFRAME";

  @Override
  public String getName() {
    return NAME;
  }

  public static final String KEEP_CASES_CONTAINED = "CONTAINED";
  public static final String KEEP_CASES_INTERSECT = "INTERSECT";
  public static final String KEEP_CASES_STARTED = "STARTED";
  public static final String KEEP_CASES_COMPLETED = "COMPLETED";
  public static final String KEEP_CASES_TRIM = "TRIM";

  public TimeFrameFilter() {
    getOptions().put("rule", KEEP_CASES_CONTAINED);
    getOptions().put("min", String.valueOf(Long.MIN_VALUE));
    getOptions().put("max", String.valueOf(Long.MAX_VALUE));
  }

  public String getRule() {
    return getOptions().get("rule");
  }

  public void setRule(String rule) {
    getOptions().put("rule", rule);
  }

  public long getMin() {
    return Long.parseLong(getOptions().get("min"));
  }

  public void setMinValue(long min) {
    getOptions().put("min", String.valueOf(min));
  }

  public long getMax() {
    return Long.parseLong(getOptions().get("max"));
  }

  public void setMaxValue(long max) {
    getOptions().put("max", String.valueOf(max));
  }


}
