/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.common.process.graph.models;

import java.util.LinkedHashMap;
import java.util.Map;

import kr.ac.pusan.bsclab.bab.v2.common.models.AttributableResource;

public class Node extends AttributableResource implements INode<Node, Node, Arc> {

	protected String id;
	protected String label;
	protected NodeType type;
	protected Map<String, Node> nodes;
	protected Map<String, Arc> arcs;
	protected Map<String, Node> allNodes;
	protected Map<String, Arc> allArcs;

	public Node() {
		setType(NodeType.ACTIVITY);
	}

	public Node(String i, String l) {
		set(i, l, NodeType.ACTIVITY);
	}

	public Node(String i, String l, NodeType t) {
		set(i, l, t);
	}

	@Override
	public void setId(String i) {
		if (i != null) {
			id = i.replaceAll("[^A-Za-z0-9]", "");
		}
	}

	@Override
	public void setLabel(String l) {
		label = l;
	}

	@Override
	public void setType(NodeType t) {
		type = t;
	}

	@Override
	public void setNodes(Map<String, Node> n) {
		getNodes().clear();
		getNodes().putAll(n);
	}

	@Override
	public void setArcs(Map<String, Arc> a) {
		getArcs().clear();
		getArcs().putAll(a);
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public NodeType getType() {
		return type;
	}

	@Override
	public Map<String, Node> getNodes() {
		if (nodes == null) {
			nodes = new LinkedHashMap<String, Node>();
		}
		return nodes;
	}

	@Override
	public Map<String, Arc> getArcs() {
		if (arcs == null) {
			arcs = new LinkedHashMap<String, Arc>();
		}
		return arcs;
	}

	@Override
	public Map<String, Node> getAllNodesTransversal() {
		return getAllNodesTransversal(false);
	}

	@Override
	public Map<String, Arc> getAllArcsTransversal() {
		return getAllArcsTransversal(false);
	}

	@Override
	public Map<String, Node> getAllNodesTransversal(boolean r) {
		if (allNodes == null || r) {
			allNodes = new LinkedHashMap<String, Node>();
			for (String i : getNodes().keySet()) {
				Node n = getNodes().get(i);
				allNodes.put(i, n);
			}
			for (Node n : getNodes().values()) {
				allNodes.putAll(n.getAllNodesTransversal(r));
			}
		}
		return allNodes;
	}

	@Override
	public Map<String, Arc> getAllArcsTransversal(boolean r) {
		if (allArcs == null || r) {
			allArcs = new LinkedHashMap<String, Arc>();
			for (String i : getArcs().keySet()) {
				Arc a = getArcs().get(i);
				allArcs.put(getId() + "_" + i, a);
			}
			for (Node n : getNodes().values()) {
				allArcs.putAll(n.getAllArcsTransversal(r));
			}
		}
		return allArcs;
	}

	@Override
	public void set(String i, String l) {
		setId(i);
		setLabel(l);
	}

	@Override
	public void set(String i, String l, NodeType t) {
		setId(i);
		setLabel(l);
		setType(t);
	}

	@Override
	public int getHeight() {
		int max = 0;
		for (Node n : getNodes().values()) {
			int h = n.getHeight();
			if (max < h) {
				max = h;
			}
		}
		return 1 + max;
	}

}
