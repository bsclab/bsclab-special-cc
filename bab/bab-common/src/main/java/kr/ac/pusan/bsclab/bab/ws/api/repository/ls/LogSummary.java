/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.ls;

import java.util.*;

import kr.ac.pusan.bsclab.bab.ws.api.Result;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;

public class LogSummary extends Result {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Case caseTab = new Case();
	private BRepository repository;
	private Timeline timelineTab = new Timeline();
	private Dimension activityTab = new Dimension();
	private Dimension originatorTab = new Dimension();
	private Dimension resourceTab = new Dimension();
	private Map<String, Dimension> otherTab = new LinkedHashMap<String, Dimension>();

	public LogSummary() {
		activityTab.setDimension(IEvent.MAP_EVENT_ACTIVITY);
		originatorTab.setDimension(IEvent.MAP_EVENT_ORIGINATOR);
		resourceTab.setDimension(IEvent.MAP_EVENT_RESOURCE);
	}

	public BRepository getRepository() {
		return repository;
	}

	public void setRepository(BRepository repository) {
		this.repository = repository;
	}

	public Dimension createDimension() {
		return new Dimension();
	}

	public Timeline createTimeline() {
		return new Timeline();
	}

	public Case getCaseTab() {
		return caseTab;
	}

	public void setCaseTab(Case caseTab) {
		this.caseTab = caseTab;
	}

	public Timeline getTimelineTab() {
		return timelineTab;
	}

	public void setTimelineTab(Timeline timelineTab) {
		this.timelineTab = timelineTab;
	}

	public Dimension getActivityTab() {
		return activityTab;
	}

	public void setActivityTab(Dimension activityTab) {
		this.activityTab = activityTab;
	}

	public Dimension getOriginatorTab() {
		return originatorTab;
	}

	public void setOriginatorTab(Dimension originatorTab) {
		this.originatorTab = originatorTab;
	}

	public Dimension getResourceTab() {
		return resourceTab;
	}

	public void setResourceTab(Dimension resourceTab) {
		this.resourceTab = resourceTab;
	}

	public Map<String, Dimension> getOtherTab() {
		return otherTab;
	}

	public void setOtherTab(Map<String, Dimension> otherTab) {
		this.otherTab = otherTab;
	}

	public void calculateRelativeFrequency(int totalEvents) {
		List<Dimension> dimensions = new ArrayList<Dimension>();
		for (Dimension dimension : dimensions) {
			dimension.calculateRelativeFrequency(totalEvents);
		}
	}

}
