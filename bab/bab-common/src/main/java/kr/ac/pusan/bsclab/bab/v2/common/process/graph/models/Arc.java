/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.common.process.graph.models;

import kr.ac.pusan.bsclab.bab.v2.common.models.AttributableResource;

public class Arc extends AttributableResource implements IArc<Node, Node, Arc> {

	protected String id;
	protected String label;
	protected ArcType type;
	protected Node source;
	protected Node destination;

	public Arc() {
		setType(ArcType.FORWARD);
	}

	public Arc(String i, String l) {
		set(i, l, ArcType.FORWARD);
	}

	public Arc(String i, String l, ArcType t) {
		set(i, l, t);
	}

	public Arc(Node s, Node d) {
		set(s, d, ArcType.FORWARD);
	}

	public Arc(Node s, Node d, ArcType t) {
		set(s, d, t);
	}

	public Arc(String i, String l, Node s, Node d) {
		set(i, l, s, d, ArcType.FORWARD);
	}

	public Arc(String i, String l, Node s, Node d, ArcType t) {
		set(i, l, s, d, t);
	}

	@Override
	public void setId(String i) {
		if (i != null) {
			id = i.replaceAll("[^A-Za-z0-9]", "");
		}
	}

	@Override
	public void setLabel(String l) {
		label = l;
	}

	@Override
	public void setSource(Node s) {
		source = s;
	}

	@Override
	public void setDestination(Node d) {
		destination = d;
	}

	@Override
	public void setType(ArcType t) {
		type = t;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getLabel() {
		return label;
	}

	@Override
	public Node getSource() {
		return source;
	}

	@Override
	public Node getDestination() {
		return destination;
	}

	@Override
	public ArcType getType() {
		return type;
	}

	@Override
	public void set(String i, String l) {
		setId(i);
		setLabel(l);
	}

	@Override
	public void set(String i, String l, ArcType t) {
		setId(i);
		setLabel(l);
		setType(t);
	}

	@Override
	public void set(Node s, Node d) {
		setSource(s);
		setDestination(d);
	}

	@Override
	public void set(Node s, Node d, ArcType t) {
		setSource(s);
		setDestination(d);
		setType(t);
	}

	@Override
	public void set(String i, String l, Node s, Node d) {
		setId(i);
		setLabel(l);
		setSource(s);
		setDestination(d);
	}

	@Override
	public void set(String i, String l, Node s, Node d, ArcType t) {
		setId(i);
		setLabel(l);
		setSource(s);
		setDestination(d);
		setType(t);
	}

}
