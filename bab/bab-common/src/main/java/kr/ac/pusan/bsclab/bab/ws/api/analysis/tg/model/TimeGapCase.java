/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;

public class TimeGapCase {

	/**
	 * contains a standard deviation for successive case
	 */
	private double stdDevSuccessive;

	/**
	 * contains standard deviation for non successive case
	 */
	private double stdDevNotSuccessive;

	/**
	 * contains mean for successive case
	 */
	private double meanSuccessive;

	/**
	 * contains mean for non successive case
	 */
	private double meanNotSuccessive;

	/**
	 * contains start activity, i.e. 'Activity Name'
	 */
	private String startActivity;

	/**
	 * contains start activity type, i.e. 'complete'
	 */
	private String startActivityType;

	/**
	 * contains end activity, i.e. 'Activity Name'
	 */
	private String endActivity;

	/**
	 * contains end activity type, i.e. 'complete'
	 */
	private String endActivityType;

	/**
	 * contains counts of successive cases
	 */
	private int noOfCasesSuccessive;

	/**
	 * contains counts of non successive cases
	 */
	private int noOfCasesNotSuccessive;

	/**
	 * contains map of case IDs (key) and a list of time gap entry (value)for
	 * successive cases
	 */
	private Map<String, List<TimeGapEntry>> caseIDSuccessive = new HashMap<String, List<TimeGapEntry>>();

	/**
	 * contains map of case IDs (key) and a list of time gap entry (value)for
	 * non successive cases
	 */
	private Map<String, List<TimeGapEntry>> caseIDNonSuccessive = new HashMap<String, List<TimeGapEntry>>();

	public double getStdDevSuccessive() {
		return stdDevSuccessive;
	}

	public void setStdDevSuccessive(double stdDevSuccessive) {
		this.stdDevSuccessive = stdDevSuccessive;
	}

	public double getStdDevNotSuccessive() {
		return stdDevNotSuccessive;
	}

	public void setStdDevNotSuccessive(double stdDevNotSuccessive) {
		this.stdDevNotSuccessive = stdDevNotSuccessive;
	}

	public double getMeanSuccessive() {
		return meanSuccessive;
	}

	public void setMeanSuccessive(double meanSuccessive) {
		this.meanSuccessive = meanSuccessive;
	}

	public double getMeanNotSuccessive() {
		return meanNotSuccessive;
	}

	public void setMeanNotSuccessive(double meanNotSuccessive) {
		this.meanNotSuccessive = meanNotSuccessive;
	}

	public String getStartActivity() {
		return startActivity;
	}

	public void setStartActivity(String startActivity) {
		this.startActivity = startActivity;
	}

	public String getStartActivityType() {
		return startActivityType;
	}

	public void setStartActivityType(String startActivityType) {
		this.startActivityType = startActivityType;
	}

	public String getEndActivity() {
		return endActivity;
	}

	public void setEndActivity(String endActivity) {
		this.endActivity = endActivity;
	}

	public String getEndActivityType() {
		return endActivityType;
	}

	public void setEndActivityType(String endActivityType) {
		this.endActivityType = endActivityType;
	}

	public int getNoOfCasesSuccessive() {
		return noOfCasesSuccessive;
	}

	public void setNoOfCasesSuccessive(int noOfCasesSuccessive) {
		this.noOfCasesSuccessive = noOfCasesSuccessive;
	}

	public int getNoOfCasesNotSuccessive() {
		return noOfCasesNotSuccessive;
	}

	public void setNoOfCasesNotSuccessive(int noOfCasesNotSuccessive) {
		this.noOfCasesNotSuccessive = noOfCasesNotSuccessive;
	}

	public Map<String, List<TimeGapEntry>> getCaseIDSuccessive() {
		return caseIDSuccessive;
	}

	public void setCaseIDSuccessive(Map<String, List<TimeGapEntry>> caseIDSuccessive) {
		this.caseIDSuccessive = caseIDSuccessive;
	}

	public Map<String, List<TimeGapEntry>> getCaseIDNonSuccessive() {
		return caseIDNonSuccessive;
	}

	public void setCaseIDNonSuccessive(Map<String, List<TimeGapEntry>> caseIDNonSuccessive) {
		this.caseIDNonSuccessive = caseIDNonSuccessive;
	}

	public void add(String cID, IEvent e1, IEvent e2, boolean successive) {
		List<TimeGapEntry> entry = null;
		if (successive == true) {
			entry = caseIDSuccessive.get(cID);
		} else {
			entry = caseIDNonSuccessive.get(cID);
		}

		TimeGapEntry tge = null;
		if (entry == null) {
			entry = new ArrayList<TimeGapEntry>();
		}

		tge = new TimeGapEntry();
		tge.setStartActivity(e1);
		tge.setEndActivity(e2);
		tge.setSuccessive(successive);
		tge.setGap(e2.getTimestamp() - e1.getTimestamp());

		if (!contain(entry, tge)) {
			entry.add(tge);
		}

		if (successive == true) {
			caseIDSuccessive.put(cID, entry);
		} else {
			caseIDNonSuccessive.put(cID, entry);
		}
	}

	private boolean contain(List<TimeGapEntry> entry, TimeGapEntry tge) {
		for (TimeGapEntry e : entry) {
			if (e.compareTo(tge) == 0) {
				return true;
			}
		}
		return false;
	}

	public int size(boolean successive) {
		if (successive == true) {
			return caseIDSuccessive.size();
		} else {
			return caseIDNonSuccessive.size();
		}

	}

	public String getStrGap(double gap) {
		int seconds = (int) (gap / 1000) % 60;
		int minutes = (int) ((gap / (1000 * 60)) % 60);
		int hours = (int) ((gap / (1000 * 60 * 60)) % 24);
		int days = (int) (gap / (1000 * 60 * 60 * 24));

		String day = "";
		if (days > 0) {
			day = String.format("%02d", days) + "D;";
		}

		return day + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":"
				+ String.format("%02d", seconds);
	}

	public void calculate(boolean isSuccessive) {

		Map<String, List<TimeGapEntry>> temp = null;

		if (isSuccessive == true) {
			temp = caseIDSuccessive;
			// System.err.println("line 200: "+startActivity+"
			// "+startActivityType+"=>"+endActivity+"
			// "+endActivityType+" "+isSuccessive+" "+caseIDSuccessive.size());
		} else {
			temp = caseIDNonSuccessive;
			// System.err.println("line 200: "+startActivity+"
			// "+startActivityType+"=>"+endActivity+"
			// "+endActivityType+" "+isSuccessive+"
			// "+caseIDNonSuccessive.size());
		}

		List<Double> duration = new ArrayList<Double>();

		for (String caseID : temp.keySet()) {
			for (TimeGapEntry tge : temp.get(caseID)) {
				duration.add((double) tge.getGap());
			}
		}

		if (isSuccessive == true) {
			setMeanSuccessive(calculateMean(duration));
			setStdDevSuccessive(temp.size() > 0 ? calculateStdDev(duration) : 0);
			setNoOfCasesSuccessive(temp.size());
		} else {
			setMeanNotSuccessive(calculateMean(duration));
			setStdDevNotSuccessive(temp.size() > 0 ? calculateStdDev(duration) : 0);
			setNoOfCasesNotSuccessive(temp.size());
		}

		// System.err.println("line 231: "+startActivity+"
		// "+startActivityType+"=>"+endActivity+"
		// "+endActivityType+" "+isSuccessive+" "+temp.size());
	}

	private double calculateMean(List<Double> duration) {
		double sum = 0;
		for (Double d : duration) {
			sum += d;
		}
		/*
		 * System.out.println("Sum " + sum + "; " + getDurations().size() + "; "
		 * + sum / getDurations().size());
		 */
		return (sum / (double) duration.size());
	}

	private double getVariance(List<Double> duration) {
		double mean = calculateMean(duration);
		double temp = 0;
		for (Double d : duration) {
			temp += Math.pow(mean - d, 2);
		}
		return (temp / (double) duration.size());
	}

	private double calculateStdDev(List<Double> duration) {
		return Math.sqrt(getVariance(duration));
	}

	public String getGap(String caseKey, boolean successive) {
		Map<String, List<TimeGapEntry>> temp = null;
		if (successive == true) {
			temp = caseIDSuccessive;
		} else {
			temp = caseIDNonSuccessive;
		}

		double count = 0.0;
		double sum = 0.0;
		for (TimeGapEntry tge : temp.get(caseKey)) {
			count++;
			sum = sum + (double) tge.getGap();
		}

		return getStrGap(count == 0 ? 0.0 : (sum / count));
	}

	public String toString() {
		return "\t Successive ==> " + caseIDSuccessive.toString() + "\t NonSuccessive ==> " + caseIDNonSuccessive;
	}
}
