package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

public class Frequency {
	private int absolute = 0;
	private double relative = 0;
	private int cases = 0;
	private int maxRepetition = 0;

	public int getAbsolute() {
		return absolute;
	}

	public void setAbsolute(int absolute) {
		this.absolute = absolute;
	}

	public double getRelative() {
		return relative;
	}

	public void setRelative(double relative) {
		this.relative = relative;
	}

	public int getCases() {
		return cases;
	}

	public void setCases(int cases) {
		this.cases = cases;
	}

	public int getMaxRepetition() {
		return maxRepetition;
	}

	public void setMaxRepetition(int maxRepetition) {
		this.maxRepetition = maxRepetition;
	}

	public void increaseAbsolute() {
		absolute++;
	}

	public void calculateRelative(double denominator) {
		if (denominator == 0) {
			relative = 0;
		} else {
			relative = relative / denominator;
		}
	}
}
