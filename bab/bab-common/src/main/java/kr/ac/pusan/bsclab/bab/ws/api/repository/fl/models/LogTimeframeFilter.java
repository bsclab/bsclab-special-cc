/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.fl.models;

import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;

/**
 * 
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class LogTimeframeFilter extends Filter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected String name = "filter.log.timeframe";

	public static String ARG_TIME_START = "time.start";
	public static String ARG_TIME_END = "time.end";
	public static String ARG_CASE_RULE = "case.rule";
	public static String ARGV_CASE_RULE_CONTAINED = "contained";
	public static String ARGV_CASE_RULE_INTERSECT = "intersect";
	public static String ARGV_CASE_RULE_STARTED = "started";
	public static String ARGV_CASE_RULE_COMPLETED = "completed";
	public static String ARGV_CASE_RULE_TRIM = "trim";

	public boolean isValid() {
		if (!getParameters().containsKey(ARG_TIME_START))
			return false;
		if (!getParameters().containsKey(ARG_TIME_END))
			return false;
		if (!getParameters().containsKey(ARG_CASE_RULE))
			return false;
		return true;
	}

	public Iterable<Object> filterObjects(Iterable<Object> data) {
		return data;
	}

	public Iterable<IRepository> filterRepository(Iterable<IRepository> repository) {
		return repository;
	}

	public Iterable<ICase> filterCases(Iterable<ICase> cases) {
		return cases;
	}

	public Iterable<IEvent> filterEvents(Iterable<IEvent> events) {
		return events;
	}

}
