package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

public class Duration {
	private long total = 0;
	private long min = Long.MAX_VALUE;
	private long max = Long.MIN_VALUE;
	private double mean = 0;
	private long median = 0;

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public long getMin() {
		return min;
	}

	public void setMin(long min) {
		this.min = min;
	}

	public long getMax() {
		return max;
	}

	public void setMax(long max) {
		this.max = max;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public long getMedian() {
		return median;
	}

	public void setMedian(long median) {
		this.median = median;
	}
}
