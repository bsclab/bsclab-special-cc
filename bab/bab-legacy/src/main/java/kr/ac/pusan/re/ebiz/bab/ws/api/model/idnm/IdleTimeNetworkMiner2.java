/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.idnm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.linalg.distributed.IndexedRowMatrix;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.stat.MultivariateStatisticalSummary;
import org.apache.spark.mllib.stat.Statistics;
import org.apache.spark.mllib.stat.test.ChiSqTestResult;
import org.netlib.util.doubleW;

import scala.Tuple2;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.AbstractAnalysisJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.IRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class IdleTimeNetworkMiner2 extends AbstractAnalysisJob {
	private static final long serialVersionUID = 1L;


	public IdleTimeNetwork run(IdleTimeRepositoryReader reader) {
		reader.getClusteredCases();

		// 0. Configuration
		final double divider = 1000 * 60 * 60 * 24; // days

		// 1. Get Logical Start Time for Each Node and Duration for Each Node
		Map<String, Double[]> nodeLogicalStartTimes = reader
				.getCasesRDD()
				.flatMapToPair(
						new PairFlatMapFunction<Tuple2<String, ICase>, String, Vector>() {
							private static final long serialVersionUID = 1L;

						@Override
							public Iterable<Tuple2<String, Vector>> call(
									Tuple2<String, ICase> arg0)
									throws Exception {
								Map<String, List<Double>> result = new HashMap<String, List<Double>>();
								for (IEvent e : arg0._2().getEvents().values()) {
									String name = e.getLabel() + "||"
											+ e.getType();
									if (!result.containsKey(name))
										result.put(name,
												new ArrayList<Double>());
									double timestamp = (double) e
											.getTimestamp() / divider;
									// Every timestamp = [timestamp, 1(count),
									// 0(variance)]
									result.get(name).add(timestamp);
									result.get(name).add(1d);
									result.get(name).add(0d); // For Average
									result.get(name).add(0d); // For Variance
								}
								List<Tuple2<String, Vector>> startTimes = new ArrayList<Tuple2<String, Vector>>();
								for (String s : result.keySet()) {
									startTimes.add(new Tuple2<String, Vector>(
											s,
											Vectors.dense(ArrayUtils
													.toPrimitive(result
															.get(s)
															.toArray(
																	new Double[0])))));
								}
								return startTimes;
							}
						})
				.groupByKey()
				.mapToPair(
						new PairFunction<Tuple2<String, Iterable<Vector>>, String, Vector>() {
							private static final long serialVersionUID = 1L;


							@Override
							public Tuple2<String, Vector> call(
									Tuple2<String, Iterable<Vector>> arg0)
									throws Exception {
								int size = 0;
								for (Vector v : arg0._2()) {
									if (v.size() > size)
										size = v.size();
								}
								double[] result = new double[size];
								for (Vector v : arg0._2()) {
									double[] va = v.toArray();
									for (int i = 0; i < va.length; i++) {
										result[i] += va[i];
									}
								}
								// Calculate Average
								for (int i = 0; i < result.length; i += 4) {
									result[i + 2] = (result[i + 1] == 0) ? 0
											: result[i] / result[i + 1];
								}
								// Calculate Variance
								for (Vector v : arg0._2()) {
									double[] va = v.toArray();
									for (int i = 0; i < va.length; i += 4) {
										result[i + 3] += (result[i] - result[i + 2])
												* (result[i] - result[i + 2]);
									}
								}
								for (int i = 0; i < result.length; i += 4) {
									result[i + 3] = (result[i + 1] == 0) ? 0
											: result[i + 3] / result[i + 1];
								}
								return new Tuple2<String, Vector>(arg0._1(),
										Vectors.dense(result));
							}
						})
				.sortByKey()
				.mapToPair(
						new PairFunction<Tuple2<String, Vector>, String, Double[]>() {
							private static final long serialVersionUID = 1L;


							@Override
							public Tuple2<String, Double[]> call(
									Tuple2<String, Vector> arg0)
									throws Exception {
								return new Tuple2<String, Double[]>(arg0._1(),
										ArrayUtils
												.toObject(arg0._2().toArray()));
							}
						}).collectAsMap();

		// 2. Get Logical Processing Time for Each Node and Duration for Each
		// Node
		Map<String, Double[]> nodeLogicalProcessingTimes = reader
				.getCasesRDD()
				.flatMapToPair(
						new PairFlatMapFunction<Tuple2<String, ICase>, String, Vector>() {
							private static final long serialVersionUID = 1L;

					@Override
							public Iterable<Tuple2<String, Vector>> call(
									Tuple2<String, ICase> arg0)
									throws Exception {
								Map<String, List<Double>> result = new HashMap<String, List<Double>>();

								Map<Double, List<IEvent>> sortedEvent = new TreeMap<Double, List<IEvent>>();
								for (IEvent e : arg0._2().getEvents().values()) {
									double timestamp = e.getTimestamp()
											/ divider;
									if (!sortedEvent.containsKey(timestamp))
										sortedEvent.put((double) timestamp,
												new ArrayList<IEvent>());
									sortedEvent.get(timestamp).add(e);
								}

								List<IEvent> pes = null;
								double pt = 0d;
								for (Double t : sortedEvent.keySet()) {
									if (pes == null) {
										pt = t;
										pes = sortedEvent.get(t);
										continue;
									}
									for (IEvent pe : pes) {
										for (IEvent e : sortedEvent.get(t)) {
											String name = pe.getLabel() + "||"
													+ pe.getType();
											if (!result.containsKey(name))
												result.put(name,
														new ArrayList<Double>());
											result.get(name).add(t - pt);
											result.get(name).add(1d);
											result.get(name).add(0d); // For
																		// Average
											result.get(name).add(0d); // For
																		// Variance
										}
									}
									pt = t;
									pes = sortedEvent.get(t);
								}

								List<Tuple2<String, Vector>> startTimes = new ArrayList<Tuple2<String, Vector>>();
								for (String s : result.keySet()) {
									startTimes.add(new Tuple2<String, Vector>(
											s,
											Vectors.dense(ArrayUtils
													.toPrimitive(result
															.get(s)
															.toArray(
																	new Double[0])))));
								}
								return startTimes;
							}
						})
				.groupByKey()
				.mapToPair(
						new PairFunction<Tuple2<String, Iterable<Vector>>, String, Vector>() {
							private static final long serialVersionUID = 1L;


							@Override
							public Tuple2<String, Vector> call(
									Tuple2<String, Iterable<Vector>> arg0)
									throws Exception {
								int size = 0;
								for (Vector v : arg0._2()) {
									if (v.size() > size)
										size = v.size();
								}
								double[] result = new double[size];
								for (Vector v : arg0._2()) {
									double[] va = v.toArray();
									for (int i = 0; i < va.length; i++) {
										result[i] += va[i];
									}
								}
								// Calculate Average
								for (int i = 0; i < result.length; i += 4) {
									result[i + 2] = (result[i + 1] == 0) ? 0
											: result[i] / result[i + 1];
								}
								// Calculate Variance
								for (Vector v : arg0._2()) {
									double[] va = v.toArray();
									for (int i = 0; i < va.length; i += 4) {
										result[i + 3] += (result[i] - result[i + 2])
												* (result[i] - result[i + 2]);
									}
								}
								for (int i = 0; i < result.length; i += 4) {
									result[i + 3] = (result[i + 1] == 0) ? 0
											: result[i + 3] / result[i + 1];
								}
								return new Tuple2<String, Vector>(arg0._1(),
										Vectors.dense(result));
							}
						})
				.sortByKey()
				.mapToPair(
						new PairFunction<Tuple2<String, Vector>, String, Double[]>() {
							private static final long serialVersionUID = 1L;


							@Override
							public Tuple2<String, Double[]> call(
									Tuple2<String, Vector> arg0)
									throws Exception {
								return new Tuple2<String, Double[]>(arg0._1(),
										ArrayUtils
												.toObject(arg0._2().toArray()));
							}
						}).collectAsMap();

		// 3. Count Enabled Transition
		Map<String, Double[]> arcLogicalTransitionTimes = reader
				.getCasesRDD()
				.flatMapToPair(
						new PairFlatMapFunction<Tuple2<String, ICase>, String, Vector>() {
							private static final long serialVersionUID = 1L;

					@Override
							public Iterable<Tuple2<String, Vector>> call(
									Tuple2<String, ICase> arg0)
									throws Exception {
								Map<String, List<Double>> result = new HashMap<String, List<Double>>();

								Map<Double, List<IEvent>> sortedEvent = new TreeMap<Double, List<IEvent>>();
								for (IEvent e : arg0._2().getEvents().values()) {
									double timestamp = e.getTimestamp();
									if (!sortedEvent.containsKey(timestamp))
										sortedEvent.put((double) timestamp,
												new ArrayList<IEvent>());
									sortedEvent.get(timestamp).add(e);
								}

								List<IEvent> pes = null;
								double pt = 0d;
								for (Double t : sortedEvent.keySet()) {
									if (pes == null) {
										pt = t;
										pes = sortedEvent.get(t);
										continue;
									}
									for (IEvent pe : pes) {
										for (IEvent e : sortedEvent.get(t)) {
											String name = pe.getLabel() + "||"
													+ pe.getType() + "||"
													+ e.getLabel() + "||"
													+ e.getType();
											if (!result.containsKey(name))
												result.put(name,
														new ArrayList<Double>());
											result.get(name).add(t - pt);
											result.get(name).add(1d);
											result.get(name).add(0d); // For
																		// Average
											result.get(name).add(0d); // For
																		// Variance
										}
									}
									pt = t;
									pes = sortedEvent.get(t);
								}

								List<Tuple2<String, Vector>> startTimes = new ArrayList<Tuple2<String, Vector>>();
								for (String s : result.keySet()) {
									startTimes.add(new Tuple2<String, Vector>(
											s,
											Vectors.dense(ArrayUtils
													.toPrimitive(result
															.get(s)
															.toArray(
																	new Double[0])))));
								}
								return startTimes;
							}
						})
				.groupByKey()
				.mapToPair(
						new PairFunction<Tuple2<String, Iterable<Vector>>, String, Vector>() {
							private static final long serialVersionUID = 1L;


							@Override
							public Tuple2<String, Vector> call(
									Tuple2<String, Iterable<Vector>> arg0)
									throws Exception {
								int size = 0;
								for (Vector v : arg0._2()) {
									if (v.size() > size)
										size = v.size();
								}
								double[] result = new double[size];
								for (Vector v : arg0._2()) {
									double[] va = v.toArray();
									for (int i = 0; i < va.length; i++) {
										result[i] += va[i];
									}
								}
								// Calculate Average
								for (int i = 0; i < result.length; i += 4) {
									result[i + 2] = (result[i + 1] == 0) ? 0
											: result[i] / result[i + 1];
								}
								// Calculate Variance
								for (Vector v : arg0._2()) {
									double[] va = v.toArray();
									for (int i = 0; i < va.length; i += 4) {
										result[i + 3] += (result[i] - result[i + 2])
												* (result[i] - result[i + 2]);
									}
								}
								for (int i = 0; i < result.length; i += 4) {
									result[i + 3] = (result[i + 1] == 0) ? 0
											: result[i + 3] / result[i + 1];
								}
								return new Tuple2<String, Vector>(arg0._1(),
										Vectors.dense(result));
							}
						})
				.sortByKey()
				.mapToPair(
						new PairFunction<Tuple2<String, Vector>, String, Double[]>() {
							private static final long serialVersionUID = 1L;


							@Override
							public Tuple2<String, Double[]> call(
									Tuple2<String, Vector> arg0)
									throws Exception {
								return new Tuple2<String, Double[]>(arg0._1(),
										ArrayUtils
												.toObject(arg0._2().toArray()));
							}
						}).collectAsMap();

		Map<String, Map<Double, Double[]>> logicalTimeline = new TreeMap<String, Map<Double, Double[]>>();
		double logicalCoverageTime = 0d;
		for (String node : nodeLogicalStartTimes.keySet()) {
			Double[] dd = nodeLogicalStartTimes.get(node);
			Double[] dp = nodeLogicalProcessingTimes.containsKey(node) ? nodeLogicalProcessingTimes
					.get(node) : new Double[0];
			for (int i = 0; i < dd.length; i += 4) {
				double startTime = Math.floor(dd[i + 2]);
				double startTimeVariance = Math.floor(dd[i + 3]);
				double processTime = Math.floor((i + 2 < dp.length) ? dp[i + 2]
						: 0d);
				double processTimeVariance = Math
						.floor((i + 3 < dp.length) ? dp[i + 3] : 0d);
				if (!logicalTimeline.containsKey(node))
					logicalTimeline.put(node, new TreeMap<Double, Double[]>());
				logicalTimeline.get(node).put(
						startTime,
						new Double[] { dd[i + 1], startTime, startTimeVariance,
								processTime, processTimeVariance });
				double coverageTime = startTime + processTime
						+ processTimeVariance;
				if (coverageTime > logicalCoverageTime)
					logicalCoverageTime = coverageTime;
			}
		}

		IdleTimeNetwork result = new IdleTimeNetwork();
		result.setLogicalStartTime(nodeLogicalStartTimes);
		result.setLogicalProcessingTime(nodeLogicalProcessingTimes);
		result.setLogicalTimeline(logicalTimeline);
		result.setLogicalCoverageTime(logicalCoverageTime);
		result.setLogicalTransitionTime(arcLogicalTransitionTimes);
		result.setCountCoverageTime(logicalTimeline.size());
		return result;
	}

	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			JSONSerializer serializer = new JSONSerializer();
			IdleTimeNetworkConfiguration config = new JSONDeserializer<IdleTimeNetworkConfiguration>()
					.deserialize(json, IdleTimeNetworkConfiguration.class);
			String outputURI = se.getHdfsURI(res.getUri());
			IdleTimeRepositoryReader reader = new IdleTimeRepositoryReader(se,
					config.getRepositoryURI());
			RawJobResult jobResult = new RawJobResult("model.IdleTimeNetwork",
					outputURI, outputURI, serializer.exclude("*.class")
							.deepSerialize(run(reader)));
			HdfsUtil.saveAsTextFile(se, outputURI + ".idmodel",
					jobResult.getResponse());
			return jobResult;
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}
		return null;
	}

}
