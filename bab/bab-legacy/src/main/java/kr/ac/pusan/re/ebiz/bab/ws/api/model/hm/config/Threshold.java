/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.config;

import java.io.Serializable;

public class Threshold implements Serializable {
	private static final long serialVersionUID = 1L;

	public Threshold() {

	}

	private double dependency = 0.9;
	private double relativeToBest = 0.05;
	private double l1Loop = 0.9;
	private double l2Loop = 0.9;
	private double longDistance = 0.9;
	private double and = 0.1;
	private int dependencyDivisor = 1;

	public double getRelativeToBest() {
		return relativeToBest;
	}

	public void setRelativeToBest(double relativeToBest) {
		this.relativeToBest = relativeToBest;
	}

	public double getL1Loop() {
		return l1Loop;
	}

	public void setL1Loop(double l1Loop) {
		this.l1Loop = l1Loop;
	}

	public double getL2Loop() {
		return l2Loop;
	}

	public void setL2Loop(double l2Loop) {
		this.l2Loop = l2Loop;
	}

	public double getLongDistance() {
		return longDistance;
	}

	public void setLongDistance(double longDistance) {
		this.longDistance = longDistance;
	}

	public double getAnd() {
		return and;
	}

	public void setAnd(double and) {
		this.and = and;
	}

	public int getDependencyDivisor() {
		return dependencyDivisor;
	}

	public void setDependencyDivisor(int dependencyDivisor) {
		this.dependencyDivisor = dependencyDivisor;
	}

	public double getDependency() {
		return dependency;
	}

	public void setDependency(double dependency) {
		this.dependency = dependency;
	}
}
