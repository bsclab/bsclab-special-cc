/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model;

import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.binary.RelationProximityMetric;

public class AggregationMetric extends AbstractMetric {
	private static final long serialVersionUID = 1L;


	private int aggregationCount = 0;
	
	@Override
	public void aggregate(Metric metric) {
		aggregationCount++;
		for (String s : metric.getLabels()) {
			measured.put(s, getSignificance(s) + metric.getSignificance(s));
		}
	}

	public void completeAggregation() {
		for (String s : getLabels()) {
			measured.put(s, getSignificance(s) / aggregationCount);
		}
	}
	

	@Override
	public Metric cloneMetric() {
		Metric result = new AggregationMetric();
		result.aggregate(this);
		return result;
	}
}
