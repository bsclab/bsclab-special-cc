/*
 * 
 * Copyright © 2013-2015 Park Chanho (cksgh4178@naver.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import kr.ac.pusan.re.ebiz.bab.ws.controller.DateUtil;

public class PerformanceChartAnalysis {
	private String id = "Try";
	private String series;
	private Map<Integer, Event> matrix = new HashMap<Integer, Event>();
	private List<String> labels = new ArrayList<String>();

	public class Event {
		private String label;
		private String x;
		private double y;
		private String z;
		private String pass;

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public String getX() {
			return x;
		}

		// public String getXString() {
		// return DateUtil.toDateTimeString(x);
		// }
		public void setX(String x) {
			this.x = x;
		}

		public double getY() {
			return y;
		}

		public void setY(double y) {
			this.y = y;
		}

		public String getZ() {
			return z;
		}

		public void setZ(String z) {
			this.z = z;
		}

		public void setPass(String pass) {
			this.pass = pass;
		}

		public String getPass() {
			return pass;
		}
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public Event getevent(int index) {
		Event e = new Event();
		matrix.put(index, e);
		return e;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<Integer, Event> getMatrix() {
		return matrix;
	}

	public void setMatrix(Map<Integer, Event> matrix) {
		this.matrix = matrix;
	}

	public List<String> getlabels() {
		return labels;
	}

	public void setSeries(List<String> labels) {
		this.labels = labels;
	}
}
