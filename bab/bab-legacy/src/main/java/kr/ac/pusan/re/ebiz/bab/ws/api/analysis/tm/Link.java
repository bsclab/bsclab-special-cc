/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy (wanprabu@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tm;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class Link implements Serializable{
	private String source;
	private String target;
	private Set<String> cases = new LinkedHashSet<String>();
	private boolean empty = false;
	private long value = 0;
	private int casesNumber = 0;
	
	public Link(String source, String target) {
		//super();
		this.source = source;
		this.target = target;
	}
	public int getCasesNumber() {
		return casesNumber;
	}
	public void setCasesNumber(int casesNumber) {
		this.casesNumber = casesNumber;
	}
	public boolean isEmpty() {
		return empty;
	}
	public void setEmpty(boolean empty) {
		this.empty = empty;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	
	public long getValue() {
		return value;
	}
	public void setValue(long value) {
		this.value = value;
	}
	public Set<String> getCases() {
		return cases;
	}
	public void setCases(Set<String> cases) {
		this.cases = cases;
	}
	public Link addCase(String caseid){
		cases.addAll(Arrays.asList(caseid));
		return this;
	}
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if (obj != null && obj instanceof Link){
			return (this.source.equals(((Link) obj).source) &&
					this.target.equals(((Link) obj).target));
		}else
			return false;
	}
	
}