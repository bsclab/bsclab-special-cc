/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimeGapAnalysisJobConfiguration implements Serializable{
	private static final long serialVersionUID = 1L;

	private String repositoryURI;
	private boolean successiveOnly;
	private Map<String, String> selectedPIs = new HashMap<String, String>();
	private String startActivity; //concatenate of 'Activity Name (Event type)'
	private String endActivity; //concatenate of 'Activity Name (Event type)'
	private List<String> caseIds;
	
	public String getRepositoryURI() {
		return repositoryURI;
	}
	
	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}
	
	public boolean isSuccessiveOnly() {
		return successiveOnly;
	}
	
	public void setSuccessiveOnly(boolean successiveOnly) {
		this.successiveOnly = successiveOnly;
	}
	
	public Map<String, String> getSelectedPIs() {
		return selectedPIs;
	}
	
	public void setSelectedPIs(Map<String, String> selectedPIs) {
		this.selectedPIs = selectedPIs;
	}

	public String getStartActivity() {
		return startActivity;
	}

	public void setStartActivity(String startActivity) {
		this.startActivity = startActivity;
	}

	public String getEndActivity() {
		return endActivity;
	}

	public void setEndActivity(String endActivity) {
		this.endActivity = endActivity;
	}

	public List<String> getCaseIds() {
		return caseIds;
	}

	public void setCaseIds(List<String> caseIds) {
		this.caseIds = caseIds;
	}
	
	
	
}
