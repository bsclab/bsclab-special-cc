/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy (wanprabu@gmail.com), Dzulfikar Adi Putra (dzulfikar.adiputra@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.lr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EventListModel implements Serializable{
	private static final long serialVersionUID = 1L;
	private int part;
	private int eventsNumber;
	private long start;
	private long complete;
	private List<LEvent> listOfEvent = new ArrayList<LEvent>();

	public EventListModel() {
	}

	public int getPart() {
		return part;
	}

	public void setPart(int part) {
		this.part = part;
	}

	public int getEventsNumber() {
		return eventsNumber;
	}

	public void setEventsNumber(int eventsNumber) {
		this.eventsNumber = eventsNumber;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getComplete() {
		return complete;
	}

	public void setComplete(long complete) {
		this.complete = complete;
	}

	public EventListModel addLEvent(LEvent event){
		listOfEvent.addAll(Arrays.asList(event));
		return this;
	}

	public List<LEvent> getListOfEvent() {
		return listOfEvent;
	}

	public void setListOfEvent(List<LEvent> listOfEvent) {
		this.listOfEvent = listOfEvent;
	}
}
