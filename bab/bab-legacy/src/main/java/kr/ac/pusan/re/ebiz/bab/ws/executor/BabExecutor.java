/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.executor;

import java.util.Map;
import java.util.TreeMap;

import org.apache.spark.api.java.JavaSparkContext;

import kr.ac.pusan.re.ebiz.bab.ws.api.AbstractJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.ar.AprioriARMinerJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.dc.DottedChartAnalysisJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.PerformanceChartAnalysisJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg.TimeGapAnalysisJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tm.TaskMatrixAnalysisJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.FuzzyMinerJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.HeuristicMinerJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.HeuristicMinerJob2;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.idnm.IdleTimeNetworkMiner;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.lr.LogReplayJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.ex.hmodel.bpmn.HNetToBPMNMinerJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.bpmn.hmodel.HeuristicFromApromoreBpmnImportJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.csv.brepo.CsvImportJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.mxml.brepo.MxmlGzImportJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.mxml.brepo.MxmlImportJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.ls.LogSummaryJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.map.IntermediateMappingJob;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawResource;

public class BabExecutor {
	public static void main(String[] args) throws Exception {
		
		long startup = System.currentTimeMillis();
		if (args.length < 6) return;

		Map<String, Class<?>> jobClass = new TreeMap<String, Class<?>>();
		jobClass.put("RepositoryLogSummary", LogSummaryJob.class);
		jobClass.put("RepositoryMxmlGzImportJob", MxmlGzImportJob.class);
		jobClass.put("RepositoryMxmlImportJob", MxmlImportJob.class);
		jobClass.put("RepositoryCsvImportJob", CsvImportJob.class);
		jobClass.put("RepositoryMappingJob", IntermediateMappingJob.class);
		jobClass.put("RepositoryHeuristicFromApromoreBpmnImportJob", HeuristicFromApromoreBpmnImportJob.class);
		jobClass.put("ModelHeuristicJob", HeuristicMinerJob.class);
		jobClass.put("ModelFuzzyJob", FuzzyMinerJob.class);
		jobClass.put("ModelLogReplayJob", LogReplayJob.class);
		jobClass.put("HNetToBPMNMinerJob", HNetToBPMNMinerJob.class);
		jobClass.put("AnalysisDottedChartJob", DottedChartAnalysisJob.class);
		jobClass.put("AnalysisAssociationRuleJob", AprioriARMinerJob.class);
		jobClass.put("AnalysisPerformanceChartJob", PerformanceChartAnalysisJob.class);
		jobClass.put("AnalysisTimeGapJob", TimeGapAnalysisJob.class);
		jobClass.put("AnalysisTaskMatrixJob", TaskMatrixAnalysisJob.class);
		
		jobClass.put("ModelHeuristicJob2", HeuristicMinerJob2.class);
		jobClass.put("ModelIdleTimeNetworkMinerJob", IdleTimeNetworkMiner.class);


		System.out.println("BAB UNIT TEST START");
		String jobName = args[0];
		String workspaceName = args[1];
		String repositoryName = args[2];
		String outputName = args[3];
		String hdfs = args[4];
		String spark = args[5];
		System.out.println("ARGS: " + jobName + " " + workspaceName + " " + repositoryName + " " + outputName + " " + hdfs + " " + spark);
		
		if (jobClass.containsKey(jobName)) {
			SparkExecutor se = new SparkExecutor(); 
			se.setHdfsURI(hdfs);
			se.startup(spark);
			//se.startup("yarn-cluster");
			if (args.length > 6) se.setExecutorCore(Integer.valueOf(args[6]));
			if (args.length > 7) se.setExecutorMemory(Integer.valueOf(args[7]));
			se.setAppName("BAB:" + jobName + "@" + workspaceName + "_" + repositoryName + "");
			JavaSparkContext jsc = se.getContext();
			jsc.addJar(hdfs + "/bab/lib/flexjson-2.1.jar");
			jsc.addJar(hdfs + "/bab/lib/bab-legacy-1.0.0-RELEASE.jar");
			String jsonUri =  se.getHdfsURI("/bab/workspaces/" + workspaceName + "/jobs/" + repositoryName + "." + outputName);
			String request = HdfsUtil.loadTextFile(se, jsonUri);
			System.out.println("JSON: " + jsonUri + " = " + request);
			IResource res = new RawResource("resource.raw.Mxml", null, "/bab/workspaces/" + workspaceName + "/" + repositoryName);
			AbstractJob job;
			try {
				job = (AbstractJob) jobClass.get(jobName).getConstructors()[0].newInstance(new Object[0]);
				IJobResult result = job.run(request, res, se);
				System.out.println(result.getResponse());
			} catch (Exception e) {
			}
			se.shutdown();
			System.out.println("BAB UNIT TEST END @ " + (System.currentTimeMillis() - startup) + " ms");
		}
		
	}
}
