/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.mxml.brepo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


//import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
//import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import scala.Tuple2;

import com.google.common.base.Strings;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.transformer.IterableTransformer;
import flexjson.transformer.MapTransformer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.ImportJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.ImportJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.BCase;
import kr.ac.pusan.re.ebiz.bab.ws.model.BEvent;
import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class MxmlImportJobOld extends ImportJob {
	private static final long serialVersionUID = 1L;


	private SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	//private Map<String, BCase> bcases = new LinkedHashMap<String, BCase>();

	private int eventCounts = 0;
	private Map<String, Integer> cases = new LinkedHashMap<String, Integer>();
	private Map<String, Integer> activities = new LinkedHashMap<String, Integer>();
	private Map<String, Integer> activityTypes = new LinkedHashMap<String, Integer>();
	private Map<String, Integer> originators = new LinkedHashMap<String, Integer>();
	private Map<String, Integer> resources = new LinkedHashMap<String, Integer>();
	private Map<String, Integer> resourceClasses = new LinkedHashMap<String, Integer>();

	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			ImportJobConfiguration config = new JSONDeserializer<ImportJobConfiguration>()
					.deserialize(json, ImportJobConfiguration.class);
			String outputURI = se.getHdfsURI(config.getRepositoryURI());

			String[] repos = config.getRepositoryURI().split("/");
			String repositoryId = "/bab/workspaces/tmp/"
					+ repos[repos.length - 1] + ".mxml";
			// InputStreamReader isr = new InputStreamReader(new
			// GZIPInputStream(fs.open(new Path(se.getHdfsURI(repositoryId)))),
			// "UTF-8");
			Reader isr = new InputStreamReader(fs.open(new Path(se
					.getHdfsURI(repositoryId))), "UTF-8");
			final JSONSerializer serializer = new JSONSerializer().transform(new IterableTransformer(), ArrayList.class)
					.transform(new MapTransformer(), LinkedHashMap.class)
					.exclude("*.class");
			final List<String> jbcases = new ArrayList<String>();

			InputSource is = new InputSource(isr);
			is.setEncoding("UTF-8");
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(is, new DefaultHandler() {

				private boolean extractCase = false;
				private boolean extractEvent = false;
				private boolean extractEventId = false;
				private boolean extractEventType = false;
				private boolean extractTimestamp = false;
				private boolean extractOriginator = false;
				private boolean extractData = false;
				private boolean extractAttribute = false;

				private String caseId;
				private String activity;
				private String originator;
				private long timestamp;
				private String resource;
				private String eventType;
				private String attribute;
				private Map<String, Object> data;
				private BCase bcase;

				@Override
				public void startElement(String uri, String localName,
						String qName, Attributes attributes)
						throws SAXException {
					String temp = null;
					if (qName.equalsIgnoreCase("ProcessInstance")) {
						temp = attributes.getValue("id");
						if (temp != null) {
							caseId = temp;
//							if (!bcases.containsKey(caseId))
//							bcases.put(caseId, new BCase(caseId, ""));
						//bcase = bcases.get(caseId);
						if (bcase == null)
							bcase = new BCase(caseId, "");
							extractCase = true;
						}
					} else if (qName.equalsIgnoreCase("AuditTrailEntry")) {
						extractEvent = true;
					} else if (qName.equalsIgnoreCase("WorkflowModelElement")) {
						extractEventId = true;
					} else if (qName.equalsIgnoreCase("EventType")) {
						extractEventType = true;
					} else if (qName.equalsIgnoreCase("Timestamp")) {
						extractTimestamp = true;
					} else if (qName.equalsIgnoreCase("Originator")) {
						extractOriginator = true;
					} else if (qName.equalsIgnoreCase("Data")) {
						data = new LinkedHashMap<String, Object>();
						extractData = true;
					} else if (qName.equalsIgnoreCase("Attribute")) {
						temp = attributes.getValue("name");
						if (temp != null) {
							attribute = temp;
							extractAttribute = true;
						}
					}
				}

				@Override
				public void characters(char[] ch, int start, int length)
						throws SAXException {
					String str = "null";
					if (ch != null) {
						str = new String(ch, start, length);
					}
					if (str == null) {
						str = "null";
					}
					if (extractCase) {
						if (extractEvent) {
							if (extractEventId) {
								activity = str;
							} else if (extractEventType) {
								eventType = str;
							} else if (extractTimestamp) {
								Date d;
								try {
									d = dateFormat.parse(str);
									timestamp = d.getTime();
									toString();
								} catch (ParseException e) {
									timestamp = 0;
								}
							} else if (extractOriginator) {
								originator = str;
							} else if (extractData && extractAttribute) {
								data.put(attribute, str);
							}
						}
					}
				}

				@Override
				public void endElement(String uri, String localName,
						String qName) throws SAXException {
					if (qName.equalsIgnoreCase("ProcessInstance")) {
						if (bcase != null) {
							//bcases.put(bcase.getId(), bcase);
							cases.put(bcase.getId(), bcase.getEvents().size());
							jbcases.add(serializer.serialize(bcase));
							bcase = null;
						}
						extractCase = false;
					} else if (qName.equalsIgnoreCase("AuditTrailEntry")) {
						if (bcase != null && activity != null) {
							if (resource == null)
								resource = "null";
							if (data != null) {
								for (String d : data.keySet()) {
									if (d == null)
										d = "null";
									if (d.compareToIgnoreCase("RESOURCE") == 0) {
										resource = (String) data.get(d);
										continue;
									}
									if (!resourceClasses.containsKey(d))
										resourceClasses.put(d, 0);
									resourceClasses.put(d,
											resourceClasses.get(d) + 1);
								}
							}
							BEvent e = new BEvent(Strings.padStart(
									String.valueOf(bcase.getEvents().size()),
									10, '0'), "", activity, eventType,
									originator, timestamp, resource);
							if (data != null) {
								e.setAttributes(data);
							}
							bcase.getEvents().put(e.getId(), e);
							eventCounts = eventCounts + 1;
							if (!activities.containsKey(activity))
								activities.put(activity, 0);
							activities.put(activity,
									activities.get(activity) + 1);
							if (!activityTypes.containsKey(eventType))
								activityTypes.put(eventType, 0);
							activityTypes.put(eventType,
									activityTypes.get(eventType) + 1);
							if (originator == null)
								originator = "null";
							if (!originators.containsKey(originator))
								originators.put(originator, 0);
							originators.put(originator,
									originators.get(originator) + 1);
							if (!resources.containsKey(resource))
								resources.put(resource, 0);
							resources.put(resource, resources.get(resource) + 1);
							activity = null;
							originator = null;
							timestamp = 0;
							eventType = null;
							resource = null;
							attribute = null;
							data = null;
						}
						extractEvent = false;
					} else if (qName.equalsIgnoreCase("WorkflowModelElement")) {
						extractEventId = false;
					} else if (qName.equalsIgnoreCase("EventType")) {
						extractEventType = false;
					} else if (qName.equalsIgnoreCase("Timestamp")) {
						extractTimestamp = false;
					} else if (qName.equalsIgnoreCase("Originator")) {
						extractOriginator = false;
					} else if (qName.equalsIgnoreCase("Data")) {
						if (bcase != null && !extractEvent) {
							for (String d : data.keySet()) {
								if (d == null)
									d = "null";
								bcase.getAttributes().put(d, data.get(d));
							}
							data = new LinkedHashMap<String, Object>();
						}
						extractData = false;
					} else if (qName.equalsIgnoreCase("Attribute")) {
						extractAttribute = false;
					}
				}
			});

			Runtime runtime = Runtime.getRuntime();
			long memBefore = runtime.totalMemory() - runtime.freeMemory();

			long memAfter = runtime.totalMemory() - runtime.freeMemory();
			int frameSize = Integer.parseInt(se.getConfiguration().get(
					"spark.akka.frameSize"));
			int blockSize = 64;
			int partitions = ((int) ((memAfter - memBefore) / 1000000))
					/ blockSize * (frameSize / blockSize);
			if (partitions < 2)
				partitions = 2;
			partitions = 16;

			JavaRDD<String> s = sc.parallelize(jbcases, partitions);
			s.saveAsTextFile(outputURI + ".trepo");

//			List<ICase> x = new ArrayList<ICase>(bcases.values());
//			JavaRDD<ICase> s2 = sc.parallelize(x, partitions);
//			s2.saveAsObjectFile(outputURI + ".trepoo");

			BRepository repository = new BRepository("BRepository@"
					+ System.currentTimeMillis(), res.getUri(),
					config.getRepositoryURI(), System.currentTimeMillis(),
					cases.size(), eventCounts, activities.size(),
					activityTypes.size(), originators.size(), resources.size());
			toString();

			repository.getCases().putAll(cases);
			repository.getActivities().putAll(activities);
			repository.getActivityTypes().putAll(activityTypes);
			repository.getOriginators().putAll(originators);
			repository.getResources().putAll(resources);

			RawJobResult result = new RawJobResult("repository.Repository",
					outputURI, outputURI, serializer.exclude("*.class")
							.serialize(repository));
			HdfsUtil.saveAsTextFile(se, outputURI + ".brepo",
					result.getResponse());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
