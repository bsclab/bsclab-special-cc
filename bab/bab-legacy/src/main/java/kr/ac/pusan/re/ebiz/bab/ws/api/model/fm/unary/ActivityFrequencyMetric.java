/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.unary;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.AbstractMetric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.Metric;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.fm.model.UnaryMetric;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import scala.Tuple2;

public class ActivityFrequencyMetric extends AbstractMetric implements UnaryMetric {
	private static final long serialVersionUID = 1L;


	@Override
	public double measure(IEvent event, int index) {
		double result = 0;
		String label = event.getLabel() + " (" + event.getType() + ")";
		measured.put(label, getSignificance(label) + 1);
		return result;
	}

	@Override
	public Metric cloneMetric() {
		Metric result = new ActivityFrequencyMetric();
		result.aggregate(this);
		return result;
	}

}
