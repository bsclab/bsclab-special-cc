/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg;

import java.util.ArrayList;

import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaSparkContext;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class TimeGapAnalysisJob extends ATimeGapAnalysisJob{
	private static final long serialVersionUID = 1L;


	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		// TODO Auto-generated method stub
		
		try{
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			JSONSerializer serializer = new JSONSerializer();
			TimeGapAnalysisJobConfiguration config = new JSONDeserializer<TimeGapAnalysisJobConfiguration>()
					.use("caseIds", ArrayList.class)
					.deserialize(json, TimeGapAnalysisJobConfiguration.class);
			String outputURI = se.getHdfsURI(res.getUri());
			
			SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI());
			IRepository repository = reader.getRepository();
			
			ATimeGapAnalysisJob tgJob = null;
			IJobResult result = new RawJobResult("a","a","a","{}");
			if(config.getCaseIds() == null ){
				System.err.println("no parameter");
				tgJob = new TimeGapAnalysisJob1();
				result = tgJob.run(json, res, se);
			}else {
				System.err.println("with parameter");
				tgJob = new TimeGapAnalysisJob2();
				result = tgJob.run(json, res, se);
			}
			
			//System.out.println(config.getSelected_events());
			
			return result;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

}
