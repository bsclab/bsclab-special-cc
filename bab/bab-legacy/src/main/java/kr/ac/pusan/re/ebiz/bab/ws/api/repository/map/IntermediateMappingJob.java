/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.repository.map;

import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.DoubleFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.common.base.Strings;

import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.ImportJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.BCase;
import kr.ac.pusan.re.ebiz.bab.ws.model.BEvent;
import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class IntermediateMappingJob extends MappingJob {

	public static final String DELIMETER_PARAM = ",";
	public static final String DELIMETER_LABEL = "+";
	public static final String FIELD_CASE = "CASE";
	public static final String FIELD_ACTIVITY = "ACTIVITY";
	public static final String FIELD_TYPE = "TYPE";
	public static final String FIELD_ORIGINATOR = "ORIGINATOR";
	public static final String FIELD_TIMESTAMP = "TIMESTAMP";
	public static final String FIELD_RESOURCE = "RESOURCE";
	public static final String FIELD_ATTRIBUTE = "ATTRIBUTE";
	public static final String SCOPE_CASE = "CASE";
	public static final String SCOPE_EVENT = "EVENT";
	public static final String EVENT_TYPE_DEFAULT = "complete";
	public static final String CASE_ID_VOID = "---VOID---";
	public static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

	public static String buildData(Map<String, String> builders, Map<String, String> eventData, String scope) {
		StringBuilder builder = new StringBuilder();
		for (String dimension : builders.keySet()) {
			String value = builders.get(dimension);
			if (value.compareTo(scope) == 0) {
				String state = (eventData.containsKey(dimension)) ? eventData.get(dimension) : ""; 
				if (builder.length() > 0) builder.append(DELIMETER_LABEL);
				builder.append(state);
			}
		}
		return builder.length() > 0 ? builder.toString() : null;
	}
		
	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			ObjectMapper jsonMapper = new ObjectMapper();
			final MappingJobConfiguration config = jsonMapper.readValue(json, MappingJobConfiguration.class);
			final Map<String, SimpleDateFormat> eventTimestamps = new LinkedHashMap<String, SimpleDateFormat>();
			if (config.getMapping().containsKey(FIELD_TIMESTAMP)) {
				Map<String, String> timestamps = config.getMapping().get(FIELD_TIMESTAMP);
				for (String dimension : timestamps.keySet()) {
					String[] value = timestamps.get(dimension).split(DELIMETER_PARAM);
					if (value.length > 0 && value[0].compareTo(SCOPE_EVENT) == 0) {
						String timestampString = dimension + DELIMETER_PARAM + (value.length > 2 ? value[2] : EVENT_TYPE_DEFAULT);
						SimpleDateFormat timestampFormatter = new SimpleDateFormat(value.length > 1 ? value[1] : DATE_DEFAULT_FORMAT); 
						eventTimestamps.put(timestampString, timestampFormatter);
					}
				}
			}
			final boolean useTypeField = eventTimestamps.size() == 1; 
			
			System.err.println(config.getFilter().keySet());
			
			if (eventTimestamps.size() > 0) {
				String repositoryURI = se.getHdfsURI(config.getRepositoryURI());
				String outputURI = se.getHdfsURI(config.getRepositoryURI() + "_" + config.getName().replaceAll("[^A-Za-z0-9]", ""));
				
				final ImportJobResult repositoryMetadata = jsonMapper.readValue(new InputStreamReader(fs.open(new Path(repositoryURI + ".mrepo")), "UTF-8"), ImportJobResult.class);
				JavaPairRDD<String, BCase> casesRDD = sc.textFile(repositoryURI + ".irepo")
				.map(new Function<String, Map<String, String>>() {
					
					ObjectMapper jsonMapper = new ObjectMapper();
					TypeFactory t = TypeFactory.defaultInstance();
					MapType classType = t.constructMapType(LinkedHashMap.class, String.class, String.class);

					@Override
					public Map<String, String> call(String dataJson)
							throws Exception {
						Map<String, String> eventData =  jsonMapper.readValue(dataJson, classType);
						return eventData;
					}
				})
				.filter(new Function<Map<String,String>, Boolean>() {
					
					@Override
					public Boolean call(Map<String, String> eventData) throws Exception {
						for (String dimension : eventData.keySet()) {
							String state = eventData.get(dimension);
							if (config.getFilter().containsKey(dimension) && !config.getFilter().get(dimension).containsKey(state)) {
								System.err.println(config.getFilter().keySet());
								return false;
							}
						}
						return true;
					}
				})
				.mapToPair(new PairFunction<Map<String, String>, String, BCase>() {
	
					@Override
					public Tuple2<String, BCase> call(Map<String, String> eventData) throws Exception {
	//					public static final String FIELD_TYPE = "TYPE";
	//					public static final String FIELD_TIMESTAMP = "TIMESTAMP";
	//					public static final String FIELD_ATTRIBUTE = "ATTRIBUTE";
	
						String caseId = config.getMapping().containsKey(FIELD_CASE) ? buildData(config.getMapping().get(FIELD_CASE), eventData, SCOPE_CASE) : null;
						String activity = config.getMapping().containsKey(FIELD_ACTIVITY) ? buildData(config.getMapping().get(FIELD_ACTIVITY), eventData, SCOPE_EVENT) : null;
						String type = config.getMapping().containsKey(FIELD_TYPE) ? buildData(config.getMapping().get(FIELD_TYPE), eventData, SCOPE_EVENT) : null;
						String originator = config.getMapping().containsKey(FIELD_ORIGINATOR) ? buildData(config.getMapping().get(FIELD_ORIGINATOR), eventData, SCOPE_EVENT) : null;
						String resource = config.getMapping().containsKey(FIELD_RESOURCE) ? buildData(config.getMapping().get(FIELD_RESOURCE), eventData, SCOPE_EVENT) : null;
						BCase bcase = new BCase(caseId, caseId);
						if (type == null) type = EVENT_TYPE_DEFAULT;
						if (caseId != null && activity != null) {
							for (String timestampField : eventTimestamps.keySet()) {
								SimpleDateFormat formatter = eventTimestamps.get(timestampField);
								String[] timestampStrings = timestampField.split(DELIMETER_PARAM);
								type = useTypeField ? type : timestampStrings[1];
								if (eventData.containsKey(timestampStrings[0])) {
									try {
										long timestamp = formatter.parse(eventData.get(timestampStrings[0])).getTime();
										BEvent bevent = new BEvent(Strings.padStart(String.valueOf(bcase.getEvents().size()), 10, '0'), caseId, activity, type, originator, timestamp, resource);
										if (config.getMapping().containsKey(FIELD_ATTRIBUTE)) {
											Map<String, String> attributes = config.getMapping().get(FIELD_ATTRIBUTE);
											for (String attribute : attributes.keySet()) {
												String value = attributes.get(attribute);
												attribute = attribute.replace(".", "");
												if (value.compareTo(SCOPE_EVENT) == 0) {
													String state = (eventData.containsKey(attribute)) ? eventData.get(attribute) : "";
													bevent.getAttributes().put(attribute, state);
												}
											}
										}
										bcase.getEvents().put(bevent.getId(), bevent);
									} catch (Exception ex) {
										
									}
								}
							}
						}
						return new Tuple2<String, BCase>(bcase.getEvents().size() > 0 ? caseId : CASE_ID_VOID, bcase);
					}
					
				})
				.filter(new Function<Tuple2<String,BCase>, Boolean>() {
					@Override
					public Boolean call(Tuple2<String, BCase> arg0) throws Exception {
						return arg0._1().compareTo(CASE_ID_VOID) != 0;
					}
				})
				.reduceByKey(new Function2<BCase, BCase, BCase>() {
	
					@Override
					public BCase call(BCase arg0, BCase arg1) throws Exception {
						BCase bcase = null;
						if (arg0 != null) {
							bcase = new BCase(arg0.getId(), arg0.getUri());
							for (IEvent e : arg0.getEvents().values()) {
								((BEvent) e).setId(Strings.padStart(String.valueOf(bcase.getEvents().size()), 10, '0'));
								bcase.getEvents().put(e.getId(), e);
							}
						}
						if (arg1 != null) {
							if (bcase == null) bcase = new BCase(arg1.getId(), arg1.getUri());
							for (IEvent e : arg1.getEvents().values()) {
								((BEvent) e).setId(Strings.padStart(String.valueOf(bcase.getEvents().size()), 10, '0'));
								bcase.getEvents().put(e.getId(), e);
							}
						}
						return bcase;
					}
					
				})
				.mapToPair(new PairFunction<Tuple2<String,BCase>, String, BCase>() {

					@Override
					public Tuple2<String, BCase> call(Tuple2<String, BCase> arg0)
							throws Exception {
						BCase bcase = arg0._2();
						Map<Long, List<IEvent>> sorted = new TreeMap<Long, List<IEvent>>();
						for (IEvent e : bcase.getEvents().values()) {
							long timestamp = e.getTimestamp();
							if (!sorted.containsKey(timestamp)) sorted.put(timestamp, new ArrayList<IEvent>());
							sorted.get(timestamp).add(e);
						}
						bcase.getEvents().clear();
						for (Long t : sorted.keySet()) {
							for (IEvent e : sorted.get(t)) {
								bcase.getEvents().put(Strings.padStart(String.valueOf(bcase.getEvents().size()), 10, '0'), e);
							}
						}
						return new Tuple2<String, BCase>(arg0._1(), bcase);
					}
				});
				
				if (casesRDD.count() < 1) {
					System.out.println("NO LINE !!!");
					return null;
				}
				
				casesRDD.map(new Function<Tuple2<String,BCase>, String>() {
					
					ObjectMapper jsonMapper = new ObjectMapper();
					
					@Override
					public String call(Tuple2<String, BCase> arg0)
							throws Exception {
						return jsonMapper.writeValueAsString(arg0._2());
					}
				})
				.saveAsTextFile(outputURI + ".trepo");
				
				Function2<Integer, Integer, Integer> integerReducer = new Function2<Integer, Integer, Integer>() {
					@Override
					public Integer call(Integer arg0, Integer arg1)
							throws Exception {
						return arg0 + arg1;
					}
				};
				
				Map<String, Integer> cases = casesRDD
					.mapToPair(new PairFunction<Tuple2<String,BCase>, String, Integer>() {
						@Override
						public Tuple2<String, Integer> call(
								Tuple2<String, BCase> arg0) throws Exception {
							return new Tuple2<String, Integer>(arg0._1(), 1);
						}
					})
					.reduceByKey(integerReducer)
					.collectAsMap();
				
				double eventCounts = casesRDD
					.mapToDouble(new DoubleFunction<Tuple2<String,BCase>>() {
						
						@Override
						public double call(Tuple2<String, BCase> arg0) throws Exception {
							return arg0._2().getEvents().size();
						}
					})
					.sum();
				Map<String, Integer> activities = casesRDD
					.flatMapToPair(new PairFlatMapFunction<Tuple2<String,BCase>, String, Integer>() {
						@Override
						public Iterable<Tuple2<String, Integer>> call(
								Tuple2<String, BCase> arg0) throws Exception {
							Map<String, Integer> r = new LinkedHashMap<String, Integer>();
							for (IEvent e : arg0._2().getEvents().values()) {
								String k = e.getLabel();
								if (!r.containsKey(k)) r.put(k, 0);
								r.put(k, r.get(k) + 1);
							}
							List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String,Integer>>();
							for (String k : r.keySet()) {
								Integer f = r.get(k);
								result.add(new Tuple2<String, Integer>(k, f));
							}
							return result;
						}
					})
					.reduceByKey(integerReducer)
					.collectAsMap();
				Map<String, Integer> activityTypes = casesRDD
					.flatMapToPair(new PairFlatMapFunction<Tuple2<String,BCase>, String, Integer>() {
						@Override
						public Iterable<Tuple2<String, Integer>> call(
								Tuple2<String, BCase> arg0) throws Exception {
							Map<String, Integer> r = new LinkedHashMap<String, Integer>();
							for (IEvent e : arg0._2().getEvents().values()) {
								String k = e.getType();
								if (!r.containsKey(k)) r.put(k, 0);
								r.put(k, r.get(k) + 1);
							}
							List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String,Integer>>();
							for (String k : r.keySet()) {
								Integer f = r.get(k);
								result.add(new Tuple2<String, Integer>(k, f));
							}
							return result;
						}
					})
					.reduceByKey(integerReducer)
					.collectAsMap();
				Map<String, Integer> originators = casesRDD
					.flatMapToPair(new PairFlatMapFunction<Tuple2<String,BCase>, String, Integer>() {
						@Override
						public Iterable<Tuple2<String, Integer>> call(
								Tuple2<String, BCase> arg0) throws Exception {
							Map<String, Integer> r = new LinkedHashMap<String, Integer>();
							for (IEvent e : arg0._2().getEvents().values()) {
								String k = e.getOriginator();
								if (!r.containsKey(k)) r.put(k, 0);
								r.put(k, r.get(k) + 1);
							}
							List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String,Integer>>();
							for (String k : r.keySet()) {
								Integer f = r.get(k);
								result.add(new Tuple2<String, Integer>(k, f));
							}
							return result;
						}
					})
					.reduceByKey(integerReducer)
					.collectAsMap();
				Map<String, Integer> resources = casesRDD
					.flatMapToPair(new PairFlatMapFunction<Tuple2<String,BCase>, String, Integer>() {
						@Override
						public Iterable<Tuple2<String, Integer>> call(
								Tuple2<String, BCase> arg0) throws Exception {
							Map<String, Integer> r = new LinkedHashMap<String, Integer>();
							for (IEvent e : arg0._2().getEvents().values()) {
								String k = e.getResource();
								if (!r.containsKey(k)) r.put(k, 0);
								r.put(k, r.get(k) + 1);
							}
							List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String,Integer>>();
							for (String k : r.keySet()) {
								Integer f = r.get(k);
								result.add(new Tuple2<String, Integer>(k, f));
							}
							return result;
						}
					})
					.reduceByKey(integerReducer)
					.collectAsMap();
				
				BRepository repository = new BRepository("BRepository@"
					+ System.currentTimeMillis(), res.getUri(),
					config.getRepositoryURI(), System.currentTimeMillis(),
					cases.size(), (int) eventCounts, activities.size(),
					activityTypes.size(), originators.size(), resources.size());
				toString();
				
				repository.setName(config.getName());
				repository.setDescription(config.getDescription());
				repository.setMapping(config.getMapping());
				
				repository.getCases().putAll(cases);
				repository.getActivities().putAll(activities);
				repository.getActivityTypes().putAll(activityTypes);
				repository.getOriginators().putAll(originators);
				repository.getResources().putAll(resources);				
				
				String repositoryJson = jsonMapper.writeValueAsString(repository);
				RawJobResult result = new RawJobResult("repository.Repository", outputURI, outputURI, repositoryJson);
				HdfsUtil.saveAsTextFile(se, outputURI + ".brepo", result.getResponse());
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
