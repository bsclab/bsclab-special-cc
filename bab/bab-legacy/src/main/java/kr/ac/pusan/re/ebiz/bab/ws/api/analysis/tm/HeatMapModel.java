/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id), Wahyu Andy (wanprabu@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import kr.ac.pusan.re.ebiz.bab.ws.api.model.lr.LogReplayModel;

public class HeatMapModel implements Serializable{
	private List<Integer> hcrow = new ArrayList<Integer>();
	private List<Integer> hccol = new ArrayList<Integer>();
	private List<String> rowLabel = new ArrayList<String>();
	private List<String> colLabel = new ArrayList<String>();
	private List<Link> heatList = new ArrayList<Link>();
	private Stats statistics = new Stats();
	private String unit;
	private String casesUrl;
		
	public String getCasesUrl() {
		return casesUrl;
	}
	public void setCasesUrl(String casesUrl) {
		this.casesUrl = casesUrl;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Stats getStatistics() {
		return statistics;
	}
	public void setStatistics(Stats statistics) {
		this.statistics = statistics;
	}
	public List<Link> getHeatList() {
		return heatList;
	}
	public void setHeatList(List<Link> heatList) {
		this.heatList = heatList;
	}
	public List<Integer> getHcrow() {
		return hcrow;
	}
	public void setHcrow(List<Integer> hcrow) {
		this.hcrow = hcrow;
	}
	public List<Integer> getHccol() {
		return hccol;
	}
	public void setHccol(List<Integer> hccol) {
		this.hccol = hccol;
	}
	public List<String> getRowLabel() {
		return rowLabel;
	}
	public void setRowLabel(List<String> rowLabel) {
		this.rowLabel = rowLabel;
	}
	public List<String> getColLabel() {
		return colLabel;
	}
	public void setColLabel(List<String> colLabel) {
		this.colLabel = colLabel;
	}
	
	public HeatMapModel addHcrow(Integer e){
		hcrow.addAll(Arrays.asList(e));
		return this;		
	}
	
	public HeatMapModel addHccol(Integer e){
		hccol.addAll(Arrays.asList(e));
		return this;		
	}
	
	public HeatMapModel addRowLabel(String e){
		rowLabel.addAll(Arrays.asList(e));
		return this;		
	}
	
	public HeatMapModel addColLabel(String e){
		colLabel.addAll(Arrays.asList(e));
		return this;		
	}
	
	public HeatMapModel addHeatList(Link e){
		heatList.addAll(Arrays.asList(e));
		return this;		
	}
	
	class Stats implements Serializable{
		private double total = 0;
		private double count = 0;
		private long min = Long.MAX_VALUE;
		private long max = Long.MIN_VALUE;
		private double mean = 0;
		private long median = 0;
		
		public double getCount() {
			return count;
		}
		public void setCount(double count) {
			this.count = count;
		}
		public double getTotal() {
			return total;
		}
		public void setTotal(double total) {
			this.total = total;
		}
		public long getMin() {
			return min;
		}
		public void setMin(long min) {
			this.min = min;
		}
		public long getMax() {
			return max;
		}
		public void setMax(long max) {
			this.max = max;
		}
		public double getMean() {
			return mean;
		}
		public void setMean(double mean) {
			this.mean = mean;
		}
		public long getMedian() {
			return median;
		}
		public void setMedian(long median) {
			this.median = median;
		}
	}
}
