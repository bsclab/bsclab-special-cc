//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.model;

import java.util.Map;
import java.util.TreeMap;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;

public class BCase2 extends BCase {

	private static final long serialVersionUID = 1L;
	
	private Map<Long, Map<String, IEvent>> events2;
	
	public BCase2() {
		super();
	}

	public BCase2(String id, String uri) {
		super(id, uri);
	}
	
	public Map<Long, Map<String, IEvent>> getEvents2() {
		if (events2 == null) {
			events2 = new TreeMap<Long, Map<String, IEvent>>();
			for (String ek : getEvents().keySet()) {
				IEvent e = getEvents().get(ek);
				long t = e.getTimestamp();
				if (!events2.containsKey(t)) events2.put(t, new TreeMap<String, IEvent>());
				events2.get(t).put(ek, e);
			}
		}
		return events2;
	}
	
}
