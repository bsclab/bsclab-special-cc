/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api;

import java.io.IOException;
import java.io.Serializable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

public class SparkExecutor implements ISparkExecutor, Serializable {

	private static final long serialVersionUID = 1L;
	
	private static SparkConf configuration;
	private static JavaSparkContext context;
	private static Configuration hdfsConfiguration;
	private String hdfsUri;

	public SparkConf getConfiguration() {
		return configuration;
	}

	public void setAppName(String appName) {
		if (configuration == null)
			return;
		configuration.setAppName(appName);
	}

	public void setExecutorCore(int noOfCore) {
		if (configuration == null)
			return;
		configuration.set("spark.cores.max", String.valueOf(noOfCore));
	}

	public void setExecutorMemory(int memSizeInMb) {
		if (configuration == null)
			return;
		configuration.set("spark.executor.memory", memSizeInMb + "M");
	}

	public void setHdfsURI(String hdfsURI) {
		if (hdfsConfiguration == null) {
			hdfsConfiguration = new Configuration();
		}
		hdfsConfiguration.set("fs.defaultFS", hdfsURI);
		hdfsUri = hdfsURI;
	}

	public String getHdfsURI() {
		if (hdfsConfiguration == null)
			return null;
		return hdfsConfiguration.get("fs.defaultFS");
	}

	public String getHdfsURI(String path) {
		if (hdfsConfiguration == null)
			return null;
		return hdfsConfiguration.get("fs.defaultFS") + path;
	}

	public Configuration getHdfsConfiguration() {
		return hdfsConfiguration;
	}

	public FileSystem getHdfsFileSystem() {
		try {
			FileSystem fs = FileSystem.get(hdfsConfiguration);
			return fs;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void startup(String sparkMasterURI) {
		if (configuration == null) {
			configuration = new SparkConf();
			configuration.setMaster(sparkMasterURI);
			configuration.set("spark.eventLog.enabled", "true");
			configuration.set("spark.eventLog.dir", hdfsUri + "/user/spark/applicationHistory");
			configuration.set("spark.rdd.compress", "true");
			configuration.set("spark.akka.frameSize", "128");
			configuration.set("spark.storage.memoryFraction", "0.2");
		}
	}

	@Override
	public JavaSparkContext getContext() {
		if (configuration != null && context == null) {
			context = new JavaSparkContext(configuration);
		}
		return context;
	}

	@Override
	public void shutdown() {
		if (context != null) {
			context.stop();
		}
	}

}
