/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.model;

import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;

public class RawJobResult implements IJobResult {
	private static final long serialVersionUID = 1L;
	
	private String resourceClass = "JobResult";
	private String id;
	private String uri;
	private String response;
	private Map<String, Object> attributes;

	public RawJobResult(String resourceClass, String id, String uri,
			String response) {
		this.resourceClass = resourceClass;
		this.uri = uri;
		this.response = response;
		if (id == null)
			this.id = this.resourceClass + "@"
					+ String.valueOf(System.nanoTime());
		else
			this.id = id;
	}

	@Override
	public String getResourceClass() {
		return resourceClass;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public Map<String, Object> getAttributes() {
		if (attributes == null) {
			attributes = new TreeMap<String, Object>();
		}
		return attributes;
	}

	@Override
	public String getResponse() {
		return response;
	}

}
