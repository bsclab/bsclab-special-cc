//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.model;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;

public class BRepository implements IRepository {
	
	private static final long serialVersionUID = 1L;
	
	private String repositoryURI;
	private String name;
	private String description;
	private Map<String, Map<String, String>> mapping = new TreeMap<String, Map<String,String>>();
	
	private String id;
	private String uri;
	private Map<String, Object> attributes;

	private String originalName;
	private long createdDate;

	private Map<String, Integer> cases;
	private Map<String, Integer> activities;
	private Map<String, Integer> activityTypes;
	private Map<String, Integer> originators;
	private Map<String, Integer> resources;

	private int noOfCases;
	private int noOfEvents;
	private int noOfActivities;
	private int noOfActivityTypes;
	private int noOfOriginators;
	private int noOfResourceClasses;

	public BRepository() {

	}

	public BRepository(String id, String uri, String originalName,
			long createdDate, int noOfCases, int noOfEvents,
			int noOfActivities, int noOfActivityTypes, int noOfOriginator,
			int noOfResourceClasses) {
		this.id = id;
		this.uri = uri;
		this.originalName = originalName;
		this.createdDate = createdDate;
		this.noOfCases = noOfCases;
		this.noOfEvents = noOfEvents;
		this.noOfActivities = noOfActivities;
		this.noOfActivityTypes = noOfActivityTypes;
		this.noOfOriginators = noOfOriginator;
		this.noOfResourceClasses = noOfResourceClasses;
	}

	@Override
	public Map<String, Integer> getCases() {
		if (cases == null) {
			cases = new LinkedHashMap<String, Integer>();
		}
		return cases;
	}

	@Override
	public Map<String, Integer> getActivities() {
		if (activities == null) {
			activities = new LinkedHashMap<String, Integer>();
		}
		return activities;
	}

	@Override
	public Map<String, Integer> getActivityTypes() {
		if (activityTypes == null) {
			activityTypes = new LinkedHashMap<String, Integer>();
		}
		return activityTypes;
	}

	@Override
	public Map<String, Integer> getOriginators() {
		if (originators == null) {
			originators = new LinkedHashMap<String, Integer>();
		}
		return originators;
	}

	@Override
	public Map<String, Integer> getResources() {
		if (resources == null) {
			resources = new LinkedHashMap<String, Integer>();
		}
		return resources;
	}

	@Override
	public Map<String, Object> getAttributes() {
		if (attributes == null) {
			attributes = new TreeMap<String, Object>();
		}
		return attributes;
	}

	@Override
	public String getResourceClass() {
		return "repository.BRepository";
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public String getOriginalName() {
		return originalName;
	}

	@Override
	public long getCreatedDate() {
		return createdDate;
	}

	@Override
	public int getNoOfCases() {
		return noOfCases;
	}

	@Override
	public int getNoOfEvents() {
		return noOfEvents;
	}

	@Override
	public int getNoOfActivities() {
		return noOfActivities;
	}

	@Override
	public int getNoOfActivityTypes() {
		return noOfActivityTypes;
	}

	@Override
	public int getNoOfOriginators() {
		return noOfOriginators;
	}

	@Override
	public int getNoOfResourceClasses() {
		return noOfResourceClasses;
	}

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, Map<String, String>> getMapping() {
		return mapping;
	}

	public void setMapping(Map<String, Map<String, String>> mapping) {
		this.mapping = mapping;
	}
}
