/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.csv.brepo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;








//import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
//import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import scala.Tuple2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.transformer.IterableTransformer;
import flexjson.transformer.MapTransformer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.ImportJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.ImportJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.im.ImportJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.map.MappingJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.BCase;
import kr.ac.pusan.re.ebiz.bab.ws.model.BEvent;
import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class CsvImportJob extends ImportJob {
	private static final long serialVersionUID = 1L;


	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			ObjectMapper jsonMapper = new ObjectMapper();
			ImportJobConfiguration config = jsonMapper.readValue(json, ImportJobConfiguration.class);
			String outputURI = se.getHdfsURI(config.getRepositoryURI());
			int bufferSize = 0;
			
			BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(new Path(se.getHdfsURI(config.getRawPath()))), "UTF-8"));
			String delimeter = ",";
			
			String metadataLine = br.readLine();
			if (metadataLine != null) {
				ImportJobResult result = new ImportJobResult();
				result.setOriginalFormat("CSV");
				result.setRawPath(config.getRawPath());
				result.setRepositoryURI(config.getRepositoryURI());
				
				//JavaRDD<String> dataRDD = sc.parallelize(new ArrayList<String>());
				int partNumber = 0;
				List<String> columns = new ArrayList<String>();
				List<String> dataStringBuffer = new ArrayList<String>();
				StringTokenizer csvTokenizer = new StringTokenizer(metadataLine, delimeter);
				while (csvTokenizer.hasMoreElements()) {
					String column = csvTokenizer.nextToken();
					columns.add(column);
					result.getOrAddDimension(column);
				}
				String dataLine;
				while ((dataLine = br.readLine()) != null) {
					csvTokenizer = new StringTokenizer(dataLine, delimeter);
					int i = 0;
					Map<String, String> data = new TreeMap<String, String>();
					while (csvTokenizer.hasMoreElements() && i < columns.size()) {
						String column = columns.get(i);
						String value = csvTokenizer.nextToken();
						data.put(column, value);
						result.increaseDimensionalStateFrequency(column, value);
						i++;
					}
					if (i < columns.size()) {
						for (int j = i; j < columns.size(); j++) {
							data.put(columns.get(j), null);
						}
					}
					String dataJson = jsonMapper.writeValueAsString(data);
					dataStringBuffer.add(dataJson);
					bufferSize += dataJson.length();
					if (bufferSize >= HDFS_BLOCK_SIZE) {
						StringBuilder sb = new StringBuilder();
						for (String l : dataStringBuffer) {
							sb.append(l).append("\n");
						}
						HdfsUtil.saveAsTextFile(se, outputURI + ".irepo/part-" + Strings.padStart(String.valueOf(partNumber), 5, '0'), sb.toString());
						
						//dataRDD = dataRDD.union(sc.parallelize(dataStringBuffer));
						dataStringBuffer = new ArrayList<String>();
						bufferSize = 0;
						partNumber++;
					}
				}
				if (dataStringBuffer.size() > 0) {
					StringBuilder sb = new StringBuilder();
					for (String l : dataStringBuffer) {
						sb.append(l).append("\n");
					}
					HdfsUtil.saveAsTextFile(se, outputURI + ".irepo/part-" + Strings.padStart(String.valueOf(partNumber), 5, '0'), sb.toString());
					//dataRDD = dataRDD.union(sc.parallelize(dataStringBuffer));
				}
				//dataRDD.saveAsTextFile(outputURI + ".irepo");

				String metadataJson = jsonMapper.writeValueAsString(result);
				RawJobResult response = new RawJobResult("repository.Repository", outputURI, outputURI, metadataJson); 
				HdfsUtil.saveAsTextFile(se, outputURI + ".mrepo", response.getResponse());
				return response;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
