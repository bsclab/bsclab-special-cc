/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaSparkContext;

import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.AbstractAnalysisJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg.TimeGapAnalysisJobModel.Case;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg.TimeGapAnalysisJobModel.Transition;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg.model.TimeGapEntry;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.tg.model.TimeGapCase;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class TimeGapAnalysisJob1 extends ATimeGapAnalysisJob{
	private static final long serialVersionUID = 1L;


	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		// TODO Auto-generated method stub
		try{
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			JSONSerializer serializer = new JSONSerializer();
			TimeGapAnalysisJobConfiguration config = new JSONDeserializer<TimeGapAnalysisJobConfiguration>()
					.use("caseIds", ArrayList.class)
					.deserialize(json, TimeGapAnalysisJobConfiguration.class);
			String outputURI = se.getHdfsURI(res.getUri());
			
			SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI());
			IRepository repository = reader.getRepository();
			
			/**
			 * get the config value
			 */
			String startActivity = "";
			String startActivityType = "";
			String endActivity = "";
			String endActivityType = ""; 
					
			if(config.getStartActivity() != null && config.getEndActivity() != null){
				String[] temp = config.getStartActivity().split("\\(");
				startActivity = temp[0].trim();
				String[] startActivityTypes = temp[1].split("\\)");
				startActivityType = startActivityTypes[0];
				
				String[] temp2 = config.getEndActivity().split("\\(");
				endActivity = temp2[0].trim();
				String[] endActivityTypes = temp2[1].split("\\)");
				endActivityType = endActivityTypes[0];
			}
			
			/**
			 * scan repository to find the A-B transition's time gap
			 */
			
			Map<String, TimeGapCase> entries = new HashMap<String, TimeGapCase>(); //key=> 'activity (type) | activity (type)' 
			
			for(String iCase : reader.getCases().keySet()){
				ICase c = reader.getCases().get(iCase);
				IEvent e1 = null;
				IEvent e2 = null;
				
				int i = 0;
				for(String e_k : c.getEvents().keySet()){
					e1 = c.getEvents().get(e_k);
					int j = 0;
					for(String e_k2 : c.getEvents().keySet()){
						if(i<j){
							e2 = c.getEvents().get(e_k2);
							
							String e1Label = e1.getLabel()+" ("+e1.getType()+")";
							String e2Label = e2.getLabel()+" ("+e2.getType()+")";
							
							TimeGapCase caseKey = entries.get(e1Label+"|"+e2Label);
							if(caseKey == null){
								caseKey = new TimeGapCase();
								caseKey.setStartActivity(e1.getLabel());
								caseKey.setStartActivityType(e1.getType());
								caseKey.setEndActivity(e2.getLabel());
								caseKey.setEndActivityType(e2.getType());
							}
							
							if(j-i == 1){ //this means e1 and e2 is successive
								caseKey.add(c.getId(), e1, e2, true);
							}
							caseKey.add(c.getId(), e1, e2, false);
							
							entries.put(e1Label+"|"+e2Label, caseKey);
							//System.err.println()
						}
						j++;
					}
					i++;
				}
			}
			
			/**
			 * Populate the model
			 */
			TimeGapAnalysisJobModel model = new TimeGapAnalysisJobModel(); 
			model.setRepositoryURI(config.getRepositoryURI());
			NumberFormat nf = new DecimalFormat("#0.00");
			int max = 0, min = 0;
			for(String key : entries.keySet()){
				TimeGapCase entry = entries.get(key);
				startActivity = entry.getStartActivity() + " ("+entry.getStartActivityType() + ")";
				endActivity = entry.getEndActivity() + " ("+entry.getEndActivityType() + ")";
				
				entry.calculate(true); //calculate successive only desc statistics
				entry.calculate(false); //calculate non successive only desc statistics
				
				Transition t = model.setOrGetTransition(startActivity, endActivity);
				t.setMeanSuccessive(entry.getStrGap(entry.getMeanSuccessive()));
				t.setMeanNonSuccessive(entry.getStrGap(entry.getMeanNotSuccessive()));
				t.setStdDevSuccessive(nf.format(entry.getStdDevSuccessive()));
				t.setStdDevNonSuccessive(nf.format(entry.getStdDevNotSuccessive()));
				t.setNoOfSuccessiveCases(entry.getNoOfCasesSuccessive());
				t.setNoOfNonSuccessiveCases(entry.getNoOfCasesNotSuccessive());
				t.setMeanRaw((int) entry.getMeanNotSuccessive());
				
				for(String caseKey : entry.getCaseIDSuccessive().keySet()){
					Case c = t.setOrGetSuccessive(caseKey);
					c.setGap(entry.getGap(caseKey, true));
				}
				
				
				for(String caseKey : entry.getCaseIDNonSuccessive().keySet()){
					Case c = t.setOrGetNonSuccessive(caseKey);
					c.setGap(entry.getGap(caseKey, false));
				}
				
				if(max < entry.getMeanNotSuccessive()){
					max = (int) Math.round(entry.getMeanNotSuccessive());
				}
				
				if(min > entry.getMeanNotSuccessive()){
					min = (int) Math.round(entry.getMeanNotSuccessive());
				}
			}
			
			model.setMaxGap(max);
			model.setMinGap(min);
			/**
			 * Return result
			 */
			//System.err.println("masuk sini");
			IJobResult result = new RawJobResult("analysis.TimeGap", outputURI, outputURI, serializer.exclude("*.class").deepSerialize(model));
			HdfsUtil.saveAsTextFile(se, outputURI + ".tgans", result.getResponse());
			return result;
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
}
