/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.hm;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;







import com.fasterxml.jackson.databind.ObjectMapper;

//import flexjson.JSONDeserializer;
//import flexjson.JSONSerializer;
import scala.Tuple2;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.AbstractModelJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.BCase;
import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawResource;

public class HeuristicMinerJob extends AbstractModelJob {
	private static final long serialVersionUID = 1L;


	private HeuristicMinerJobConfiguration config;
	private HeuristicModel model;
	
	public HeuristicMinerJobConfiguration getConfig() {
		return config;
	}
	
	public HeuristicModel getModel() {
		return model;
	}
	
	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		try {
			JavaSparkContext sc = se.getContext();
			FileSystem fs = se.getHdfsFileSystem();
			ObjectMapper mapper = new ObjectMapper();
			//JSONSerializer serializer = new JSONSerializer();
			//config = new JSONDeserializer<HeuristicMinerJobConfiguration>()
				//	.deserialize(json, HeuristicMinerJobConfiguration.class);
			config = mapper.readValue(json, HeuristicMinerJobConfiguration.class);
			String outputURI = se.getHdfsURI(res.getUri());

			SparkRepositoryReader reader = new SparkRepositoryReader(se,
					config.getRepositoryURI());
			IRepository repository = reader.getRepository();

			// Step 1. Count arcs
			Map<String, ArcCount> arcs = 
					reader.getCasesRDD()
			// 1.1. Calculate arc frequency
			.flatMapToPair(new PairFlatMapFunction<Tuple2<String,ICase>, String, ArcCount>() {
				private static final long serialVersionUID = 1L;


				@Override
				public Iterable<Tuple2<String, ArcCount>> call(
						Tuple2<String, ICase> trace) throws Exception {
					Map<String, Tuple2<String, ArcCount>> counts = new LinkedHashMap<String, Tuple2<String,ArcCount>>();
					Iterable<IEvent> events = trace._2().getEvents().values();
					IEvent prev = null;
					for (IEvent event : events) {
						String from;
						if (prev == null) {
							from = ArcCount.NULLSTR;
						} else {
							from = prev.getLabel() + " (" + prev.getType() + ")";
						}
						String to	= event.getLabel() + " (" + event.getType() + ")";
						String transition = from + "=>" + to;
						if (!counts.containsKey(transition)) counts.put(transition, new Tuple2<String, ArcCount>(transition, new ArcCount(from, to)));
						ArcCount arc = counts.get(transition)._2();
						if (prev == null) {
							arc.increase(0);
						} else {
							arc.increase(event.getTimestamp() - prev.getTimestamp());
						}
						prev = event;
					}
					if (prev != null) {
						String from = prev.getLabel() + " (" + prev.getType() + ")";
						String to = ArcCount.NULLSTR;
						String transition = from + "=>" + to;
						if (!counts.containsKey(transition)) counts.put(transition, new Tuple2<String, ArcCount>(transition, new ArcCount(from, to)));
						ArcCount arc = counts.get(transition)._2();
						arc.increase(0);
					}
					return counts.values();
				}
			}).reduceByKey(new Function2<HeuristicMinerJob.ArcCount, HeuristicMinerJob.ArcCount, HeuristicMinerJob.ArcCount>() {
				private static final long serialVersionUID = 1L;


				@Override
				public ArcCount call(ArcCount arg0, ArcCount arg1) throws Exception {
					ArcCount reduced = new ArcCount(arg0.getFrom(), arg0.getTo());
					reduced.add(arg0);
					reduced.add(arg1);
					return reduced;
				}
			})
			.collectAsMap();
			
			// Step 2. CalculateDependency
			// 2.1. Calculate Dependency and Build Nodes
			Map<String, Integer> nodes = new LinkedHashMap<String, Integer>();
			String node;
			double ab, ba;
			for (ArcCount arc : arcs.values()) {
				if (arc.isSelfLoop()) {
					ab = arc.getFrequency();
					arc.setDependency(ab / (ab + 1));
				} else {
					ab = arc.getFrequency();
					String rtrans = arc.getTo() + "=>" + arc.getFrom();
					ba = arcs.containsKey(rtrans) ? arcs.get(rtrans).getFrequency() : 0d;
					arc.setDependency((ab - ba) / (ab + ba + 1));
				}
				node = arc.getFrom();
				if (!nodes.containsKey(node)) nodes.put(node, 0);
				nodes.put(node, nodes.get(node) + arc.getFrequency());
				node = arc.getTo();
				if (!nodes.containsKey(node)) nodes.put(node, 0);
				nodes.put(node, nodes.get(node) + arc.getFrequency());
			}
			// 2.2. Prune arcs
			double positiveObservations = config.getPositiveObservation();
			double minimumThreshold = config.getThreshold().getDependency();
			boolean useAllConnectedHeuristics = config.getOption().isAllConnected();
			boolean accepted;
			model = new HeuristicModel(outputURI);
			HeuristicModel.Node modelNode;
			HeuristicModel.Arc modelArc;
			for (ArcCount arc : arcs.values()) {
				accepted = arc.getFrequency() >= positiveObservations
						&& arc.getDependency() > 0
						&& arc.getDependency() >= minimumThreshold
						&& !arc.getFrom().equals(ArcCount.NULLSTR)
						&& !arc.getTo().equals(ArcCount.NULLSTR)
						;
				node = arc.getFrom();
				model.getOrAddNode(node).getFrequency().setAbsolute(nodes.get(node));
				node = arc.getTo();
				model.getOrAddNode(node).getFrequency().setAbsolute(nodes.get(node));
				if (!accepted) {
					node = arc.getFrom();
					node = arc.getTo();
				} else {
					modelArc = model.getOrAddArc(arc.getFrom(), arc.getTo());
					modelArc.getFrequency().setAbsolute(arc.getFrequency());
					modelArc.setDependency(arc.getDependency());
				}
			}
			model.getNodes().remove(ArcCount.NULLSTR);
						
			RawJobResult
			result = new RawJobResult("model.Heuristic",
					outputURI, outputURI, mapper.writeValueAsString(model));
			HdfsUtil.saveAsTextFile(se, outputURI + ".hmodel",
					result.getResponse());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	class ArcCount implements Serializable {
		private static final long serialVersionUID = 1L;

		public static final String NULLSTR = "null (null)";
		private final String from;
		private final String to;
		private int frequency;
		private long duration;
		private double dependency;
		
		public ArcCount() {
			from = NULLSTR;
			to = NULLSTR;
		}

		public double getDependency() {
			return dependency;
		}

		public ArcCount(String from, String to) {
			this.from = from;
			this.to = to;
		}
		
		public ArcCount(ArcCount copy) {
			this.from = copy.getFrom();
			this.to = copy.getTo();
			add(copy);
		}
		
		public int getFrequency() {
			return frequency;
		}

		public long getDuration() {
			return duration;
		}

		public String getFrom() {
			return from;
		}

		public String getTo() {
			return to;
		}
		
		public boolean isStartNode() {
			return from.equalsIgnoreCase(NULLSTR);
		}
		
		public boolean isEndNode() {
			return to.equalsIgnoreCase(NULLSTR);
		}
		
		public boolean isSelfLoop() {
			return from.equalsIgnoreCase(to);
		}

		public void increase(long duration) {
			frequency++;
			this.duration += duration;
		}
		
		public void add(ArcCount add) {
			this.frequency += add.getFrequency();
			this.duration += add.getDuration();
		}

		public void setDependency(double dependency) {
			this.dependency = dependency;
		}
	}
}
