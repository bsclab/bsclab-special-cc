/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.idnm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
//import org.apache.spark.storage.StorageLevel;

import com.google.common.base.Strings;

import scala.Tuple2;
import flexjson.ClassLocator;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.ObjectBinder;
import flexjson.ObjectFactory;
import flexjson.factories.ClassLocatorObjectFactory;
import flexjson.factories.MapObjectFactory;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.IRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.BCase;
import kr.ac.pusan.re.ebiz.bab.ws.model.BEvent;
import kr.ac.pusan.re.ebiz.bab.ws.model.BRepository;

public class IdleTimeRepositoryReader implements IRepositoryReader,
		Serializable {
	private static final long serialVersionUID = 1L;


	private SparkExecutor executor;
	private IRepository repository;
	private JavaPairRDD<String, ICase> casesRDD;
	private Map<String, ICase> cases;
	private String repositoryURI;

	public IdleTimeRepositoryReader(SparkExecutor se, String repositoryURI) {
		this.executor = se;
		try {
			this.repositoryURI = repositoryURI;
			ClassLocatorObjectFactory objectFactory = new ClassLocatorObjectFactory(
					new ClassLocator() {
						@Override
						public Class<?> locate(ObjectBinder arg0,
								flexjson.Path arg1)
								throws ClassNotFoundException {
							return String.class;
						}
					});
			String repo = HdfsUtil.loadTextFile(se,
					se.getHdfsURI(repositoryURI + ".brepo"));
			this.repository = new JSONDeserializer<BRepository>()
					.use(null, BRepository.class).use("cases", TreeMap.class)
					.use("cases.values", Integer.class)
					.use("activities", TreeMap.class)
					.use("activities.values", Integer.class)
					.use("activityTypes", TreeMap.class)
					.use("activityTypes.values", Integer.class)
					.use("originators", TreeMap.class)
					.use("originators.values", Integer.class)
					.use("resources", TreeMap.class)
					.use("resources.values", Integer.class)
					.use("attributes", TreeMap.class)
					.use("attributes.values", objectFactory)
					.deserialize(repo, BRepository.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IRepository getRepository() {
		return repository;
	}

	@Override
	public JavaPairRDD<String, ICase> getCasesRDD() {

		if (casesRDD == null) {
			JavaSparkContext jsc = executor.getContext();
			JavaRDD<String> caseStrRdd = jsc.textFile(executor
					.getHdfsURI(repositoryURI + ".trepo"));
			casesRDD = caseStrRdd
					.mapToPair(new PairFunction<String, String, ICase>() {
						private static final long serialVersionUID = 1L;

						@Override
						public Tuple2<String, ICase> call(String caseJson)
								throws Exception {
							ClassLocatorObjectFactory objectFactory = new ClassLocatorObjectFactory(
									new ClassLocator() {
										@Override
										public Class locate(ObjectBinder arg0,
												flexjson.Path arg1)
												throws ClassNotFoundException {
											return String.class;
										}
									});
							BCase bcase = new JSONDeserializer<BCase>()
									.use(null, BCase.class)
									.use("events", TreeMap.class)
									.use("events.values", BEvent.class)
									.use("attributes", TreeMap.class)
									.use("attributes.values", objectFactory)
									.use("events.values.attributes",
											TreeMap.class)
									.use("events.values.attributes.values",
											objectFactory)
									.deserialize(caseJson);
							if (bcase.getEvents().size() > 0) {
								IEvent[] events = bcase.getEvents().values()
										.toArray(new IEvent[0]);
								Map<Long, List<IEvent>> sortedEvents = new TreeMap<Long, List<IEvent>>();
								for (IEvent e : events) {
									if (!sortedEvents.containsKey(e
											.getTimestamp()))
										sortedEvents.put(e.getTimestamp(),
												new ArrayList<IEvent>());
									sortedEvents.get(e.getTimestamp()).add(e);
								}
								Long[] timestamps = sortedEvents.keySet()
										.toArray(new Long[0]);
								Long caseStart = timestamps[0];
								Long caseEnd = timestamps[timestamps.length - 1];
								bcase.getAttributes().put("caseStart",
										caseStart);
								bcase.getAttributes().put("caseEnd", caseEnd);
								bcase.getEvents().clear();
								for (Long t : sortedEvents.keySet()) {
									for (IEvent e : sortedEvents.get(t)) {
										String eid = Strings.padStart(String
												.valueOf(bcase.getEvents()
														.size()), 10, '0');
										((BEvent) e).setId(eid);
										// ((BEvent)
										// e).setTimestamp(e.getTimestamp() -
										// caseStart);
										bcase.getEvents().put(eid, e);
									}
								}
								IEvent se = events[0];
								IEvent ee = events[events.length - 1];
								BEvent ase = new BEvent("-1", se.getUri(),
										"Start", "complete",
										se.getOriginator(), se.getTimestamp(),
										se.getResource());
								BEvent aee = new BEvent(String
										.valueOf(events.length), ee.getUri(),
										"End", "complete", ee.getOriginator(),
										ee.getTimestamp(), ee.getResource());
								bcase.getEvents().clear();
								if (se.getLabel().compareToIgnoreCase("start") != 0) {
									bcase.getEvents().put(ase.getId(), ase);
								}
								for (IEvent e : events) {
									bcase.getEvents().put(e.getId(), e);
								}
								if (ee.getLabel().compareToIgnoreCase("end") != 0) {
									bcase.getEvents().put(aee.getId(), aee);
								}
							}
							return new Tuple2<String, ICase>(bcase.getId(),
									bcase);
						}

					});
		}
		return casesRDD;
	}

	@Override
	public Map<String, ICase> getCases() {
		if (cases == null) {
			cases = getCasesRDD().collectAsMap();
		}
		return cases;
	}

	public Map<String, ICase> getClusteredCases() {

		System.err
				.println(getCasesRDD()
				// 1. Map to Pair -> Key: Trace Signature, Value: ICase
						.mapToPair(
								new PairFunction<Tuple2<String, ICase>, String, ICase>() {
									private static final long serialVersionUID = 1L;


									@Override
									public Tuple2<String, ICase> call(
											Tuple2<String, ICase> arg0)
											throws Exception {
										Map<Long, Set<String>> traces = new TreeMap<Long, Set<String>>();
										for (IEvent e : arg0._2().getEvents()
												.values()) {
											if (!traces.containsKey(e
													.getTimestamp()))
												traces.put(e.getTimestamp(),
														new TreeSet<String>());
											traces.get(e.getTimestamp())
													.add(e.getLabel() + " ("
															+ e.getType() + ")");
										}
										List<Set<String>> signature = new ArrayList<Set<String>>();
										for (Set<String> s : traces.values())
											signature.add(s);
										return new Tuple2<String, ICase>(
												DigestUtils.md5Hex(signature
														.toString()), arg0._2());
									}
								})
						// 2. Group by Key
						.groupByKey().keys().collect());

		return null;
	}

}
