//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.model;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;

public class BCase implements ICase, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5855461943176058521L;

	public String id;
	public String uri;
	public Map<String, Object> attributes;
	public Map<String, IEvent> events;

	public BCase() {

	}

	public BCase(String id, String uri) {
		this.id = id;
		this.uri = uri;
	}

	@Override
	public Map<String, Object> getAttributes() {
		if (attributes == null) {
			attributes = new TreeMap<String, Object>();
		}
		return attributes;
	}

	@Override
	public String getResourceClass() {
		return "repository.BCase";
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public Map<String, IEvent> getEvents() {
		if (events == null) {
			events = new TreeMap<String, IEvent>();
		}
		return events;
	}
}
