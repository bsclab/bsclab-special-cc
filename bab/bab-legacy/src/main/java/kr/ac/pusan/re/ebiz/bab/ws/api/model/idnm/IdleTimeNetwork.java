/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.idnm;

import java.io.Serializable;
import java.util.Map;

import org.apache.spark.mllib.linalg.Vector;

public class IdleTimeNetwork implements Serializable {
	private static final long serialVersionUID = 1L;


	private Map<String, Double[]> logicalStartTime;
	private Map<String, Double[]> logicalProcessingTime;
	private Map<String, Double[]> logicalTransitionTime;
	private Map<String, Map<Double, Double[]>> logicalTimeline;
	private double logicalCoverageTime;
	private int countCoverageTime;

	public Map<String, Double[]> getLogicalStartTime() {
		return logicalStartTime;
	}

	public Map<String, Double[]> getLogicalProcessingTime() {
		return logicalProcessingTime;
	}

	public Map<String, Map<Double, Double[]>> getLogicalTimeline() {
		return logicalTimeline;
	}

	public void setLogicalStartTime(Map<String, Double[]> logicalStartTime) {
		this.logicalStartTime = logicalStartTime;
	}

	public void setLogicalProcessingTime(
			Map<String, Double[]> logicalProcessingTime) {
		this.logicalProcessingTime = logicalProcessingTime;
	}

	public void setLogicalTimeline(
			Map<String, Map<Double, Double[]>> logicalTimeline) {
		this.logicalTimeline = logicalTimeline;
	}

	public double getLogicalCoverageTime() {
		return logicalCoverageTime;
	}

	public int getCountCoverageTime() {
		return countCoverageTime;
	}

	public void setLogicalCoverageTime(double logicalCoverageTime) {
		this.logicalCoverageTime = logicalCoverageTime;
	}

	public void setCountCoverageTime(int countCoverageTime) {
		this.countCoverageTime = countCoverageTime;
	}

	public Map<String, Double[]> getLogicalTransitionTime() {
		return logicalTransitionTime;
	}

	public void setLogicalTransitionTime(
			Map<String, Double[]> logicalTransitionTime) {
		this.logicalTransitionTime = logicalTransitionTime;
	}

}
