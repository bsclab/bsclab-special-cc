/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.model.fm;

public class FuzzyMinerJobConfiguration {
	private String repositoryURI;
	private Node node;
	private Edge edge;
	private Concurrency concurrency;
	
	class Node {
		private double cutoff;

		public double getCutoff() {
			return cutoff;
		}

		public void setCutoff(double cutoff) {
			this.cutoff = cutoff;
		}
	}
	
	class Edge {
		private double cutoff;
		private double utility;
		private String transformer;
		private boolean ignoreSelfLoop;
		private boolean inAbsolute;
		public double getCutoff() {
			return cutoff;
		}
		public void setCutoff(double cutoff) {
			this.cutoff = cutoff;
		}
		public double getUtility() {
			return utility;
		}
		public void setUtility(double utility) {
			this.utility = utility;
		}
		public String getTransformer() {
			return transformer;
		}
		public void setTransformer(String transformer) {
			this.transformer = transformer;
		}
		public boolean isIgnoreSelfLoop() {
			return ignoreSelfLoop;
		}
		public void setIgnoreSelfLoop(boolean ignoreSelfLoop) {
			this.ignoreSelfLoop = ignoreSelfLoop;
		}
		public boolean isInAbsolute() {
			return inAbsolute;
		}
		public void setInAbsolute(boolean inAbsolute) {
			this.inAbsolute = inAbsolute;
		}
	}
	
	class Concurrency {
		private double perserve;
		private double ratio;
		public double getPerserve() {
			return perserve;
		}
		public void setPerserve(double perserve) {
			this.perserve = perserve;
		}
		public double getRatio() {
			return ratio;
		}
		public void setRatio(double ratio) {
			this.ratio = ratio;
		}
	}

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

	public Edge getEdge() {
		return edge;
	}

	public void setEdge(Edge edge) {
		this.edge = edge;
	}

	public Concurrency getConcurrency() {
		return concurrency;
	}

	public void setConcurrency(Concurrency concurrency) {
		this.concurrency = concurrency;
	}

	public String getRepositoryURI() {
		return repositoryURI;
	}

	public void setRepositoryURI(String repositoryURI) {
		this.repositoryURI = repositoryURI;
	}
}
