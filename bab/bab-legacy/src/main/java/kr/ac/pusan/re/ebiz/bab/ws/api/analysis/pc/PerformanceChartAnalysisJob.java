/*
 * 
 * Copyright © 2013-2015 Park Chanho (cksgh4178@naver.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc;

import java.util.*;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFlatMapFunction;

import scala.Tuple2;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import kr.ac.pusan.re.ebiz.bab.ws.api.SparkExecutor;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.AbstractAnalysisJob;
//<<<<<<< .mine
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.PerformanceChartAnalysis;

//import kr.ac.pusan.re.ebiz.bab.ws.api.model.hm.HeuristicMinerJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
//=======
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.PerformanceChartAnalysis;
import kr.ac.pusan.re.ebiz.bab.ws.api.repository.SparkRepositoryReader;
//>>>>>>> .r162
import kr.ac.pusan.re.ebiz.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IEvent;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;
import kr.ac.pusan.re.ebiz.bab.ws.controller.DateUtil;
import kr.ac.pusan.re.ebiz.bab.ws.controller.HdfsUtil;
import kr.ac.pusan.re.ebiz.bab.ws.model.RawJobResult;

public class PerformanceChartAnalysisJob extends AbstractAnalysisJob {
	private static final long serialVersionUID = 1L;

	@Override
	public IJobResult run(String json, IResource res, SparkExecutor se) {
		// <<<<<<< .mine
		JavaSparkContext sc = se.getContext();
		FileSystem fs = se.getHdfsFileSystem();
		JSONSerializer serializer = new JSONSerializer();
		PerformanceChartAnalysisJobConfiguration config = new JSONDeserializer<PerformanceChartAnalysisJobConfiguration>()
				.deserialize(json,
						PerformanceChartAnalysisJobConfiguration.class);
		String outputURI = se.getHdfsURI(res.getUri());
		SparkRepositoryReader reader = new SparkRepositoryReader(se,
				config.getRepositoryURI());

		Map<String, ICase> Input = reader.getCases();
		Iterator<ICase> it = Input.values().iterator();

		AbstractPerformance taskT = new AbstractPerformance();
		AbstractPerformance originatorT = new AbstractPerformance();
		PerformanceChartAnalysis analysis = new PerformanceChartAnalysis();

		if (config.getSeries().equalsIgnoreCase("Task"))
			analysis.setSeries("Task");
		else if (config.getSeries().equalsIgnoreCase("Originator"))
			analysis.setSeries("Originator");
		int index = 0;
		int i = 0;
		long timeunit;
		if (config.getUnit().equalsIgnoreCase("seconds")) {
			timeunit = 1000;
		} else if (config.getUnit().equalsIgnoreCase("minutes")) {
			timeunit = 60000;
		} else if (config.getUnit().equalsIgnoreCase("hours")) {
			timeunit = 3600000L;
		} else if (config.getUnit().equalsIgnoreCase("days")) {
			timeunit = 86400000L;
		} else if (config.getUnit().equalsIgnoreCase("weeks")) {
			timeunit = 604800000L;
		} else if (config.getUnit().equalsIgnoreCase("months")) {
			timeunit = 2592000000L;
		} else {
			timeunit = 31536000000L;
		}
		String y = config.getY();
		while (it.hasNext()) {
			ICase pi = it.next();
			Iterator<IEvent> events = pi.getEvents().values().iterator();
			long waitingInstance = 0; // waiting for instance
			long workingInstance = 0; // working for instance
			ArrayList<IEvent> durationReferenceList = new ArrayList<IEvent>();
			ArrayList<IEvent> scheduleReferenceList = new ArrayList<IEvent>();
			IEvent lastEvent = null;
			IEvent startEvent = null;
			IEvent endEvent = null;
			IEvent event = null;
			while (events.hasNext()) {
				event = events.next();
				PerformanceChartAnalysis.Event e = analysis.getevent(index);
				e.setX(String.valueOf(event.getTimestamp() / timeunit));
				if (event.getTimestamp() != null) {
					if (lastEvent == null)
						lastEvent = event;
					if (startEvent == null) {
						startEvent = event;
						endEvent = event;
					}
					if (endEvent.getTimestamp() < (event.getTimestamp())) {
						endEvent = event;
					}
					String originator = event.getOriginator();
					String task = event.getLabel();
					if (event.getType().equals("schedule")) {
						scheduleReferenceList.add(event);
					} else if (event.getType().equals("start")) {
						durationReferenceList.add(event);
						IEvent eventRef = null;
						for (IEvent event1 : scheduleReferenceList) {
							if (event1.getLabel().equalsIgnoreCase(task)) {
								scheduleReferenceList.remove(event1);
								eventRef = event1;
								break;
							}
						}
						if (eventRef == null)
							eventRef = lastEvent;
						if (eventRef != null) {
							double diff = calculationdiff(eventRef, event);
							if (config.getSeries().equals("Task")) {
								taskT.addwaiting(task, diff);
								e.setLabel(task);
								if (y.equalsIgnoreCase("Minimum"))
									e.setY((taskT.waiting.get(task).getMin())
											/ timeunit);
								else if (y.equalsIgnoreCase("Average"))
									e.setY((taskT.waiting.get(task).getMean())
											/ timeunit);
								else if (y.equalsIgnoreCase("Maximum"))
									e.setY((taskT.waiting.get(task).getMax())
											/ timeunit);
								else if (y.equalsIgnoreCase("Sum"))
									e.setY((taskT.waiting.get(task).getSum())
											/ timeunit);
								else if (y.equalsIgnoreCase("Std.Deviation"))
									e.setY((taskT.waiting.get(task)
											.getStandardDeviation()) / timeunit);
							}
							if (config.getSeries().equals("Originator")) {
								originatorT.addwaiting(originator, diff);
								e.setLabel(originator);
								if (y.equalsIgnoreCase("Minimum"))
									e.setY((originatorT.waiting.get(originator)
											.getMin()) / timeunit);
								else if (y.equalsIgnoreCase("Average"))
									e.setY((originatorT.waiting.get(originator)
											.getMean()) / timeunit);
								else if (y.equalsIgnoreCase("Maximum"))
									e.setY(originatorT.waiting.get(originator)
											.getMax() / timeunit);
								else if (y.equalsIgnoreCase("Sum"))
									e.setY((originatorT.waiting.get(originator)
											.getSum()) / timeunit);
								else if (y.equalsIgnoreCase("Std.Deviation"))
									e.setY((originatorT.waiting.get(originator)
											.getStandardDeviation()) / timeunit);
							}
							waitingInstance += diff;
						}
					} else if (event.getType().equals("complete")) {
						IEvent eventRef = null;
						for (IEvent event1 : durationReferenceList) {
							if (event1.getLabel().equals(task)) {
								durationReferenceList.remove(event1);
								eventRef = event1;
								break;
							}
						}
						// for waiting Time
						if (eventRef == null)
							eventRef = lastEvent;
						double diff = calculationdiff(eventRef, event);
						workingInstance += diff;
						if (config.getSeries().equals("Task")) {
							taskT.addworking(task, diff);
							e.setLabel(task);
							if (y.equalsIgnoreCase("Minimum"))
								e.setY((taskT.working.get(task).getMin())
										/ timeunit);
							else if (y.equalsIgnoreCase("Average"))
								e.setY((taskT.working.get(task).getMean())
										/ timeunit);
							else if (y.equalsIgnoreCase("Maximum"))
								e.setY((taskT.working.get(task).getMax())
										/ timeunit);
							else if (y.equalsIgnoreCase("Sum"))
								e.setY((taskT.working.get(task).getSum())
										/ timeunit);
							else if (y.equalsIgnoreCase("Std.Deviation"))
								e.setY((taskT.working.get(task)
										.getStandardDeviation()) / timeunit);
						}
						if (config.getSeries().equals("Originator")) {
							originatorT.addworking(originator, diff);
							e.setLabel(originator);
							if (y.equalsIgnoreCase("Minimum"))
								e.setY((originatorT.working.get(originator)
										.getMin()) / timeunit);
							else if (y.equalsIgnoreCase("Average"))
								e.setY((originatorT.working.get(originator)
										.getMean()) / timeunit);
							else if (y.equalsIgnoreCase("Maximum"))
								e.setY(originatorT.working.get(originator)
										.getMax() / timeunit);
							else if (y.equalsIgnoreCase("Sum"))
								e.setY((originatorT.working.get(originator)
										.getSum()) / timeunit);
							else if (y.equalsIgnoreCase("Std.Deviation"))
								e.setY((originatorT.working.get(originator)
										.getStandardDeviation()) / timeunit);
						}
						lastEvent = event;
					}
				}
				analysis.getMatrix().put(index, e);
				index++;
			}

		}
		RawJobResult result = new RawJobResult("analysis.PerformanceChart",
				outputURI, outputURI, serializer.include("matrix")
						.include("series").exclude("*.class")
						.serialize(analysis));
		HdfsUtil.saveAsTextFile(se, outputURI + ".pfans", result.getResponse());
		return result;
		// >>>>>>> .r162*/
	}

	private double calculationdiff(IEvent startevnet, IEvent endevent) {
		return endevent.getTimestamp() - startevnet.getTimestamp();
	}

	class AbstractPerformance {
		private HashMap<String, DescriptiveStatistics> working = new HashMap<String, DescriptiveStatistics>();
		private HashMap<String, DescriptiveStatistics> waiting = new HashMap<String, DescriptiveStatistics>();

		public void addworking(String item, double n) {
			if (working.containsKey(item))
				working.get(item).addValue(n);
			else {
				DescriptiveStatistics ds = new DescriptiveStatistics();
				ds.addValue(n);
				working.put(item, ds);
			}
		}

		public void addwaiting(String item, double n) {
			if (waiting.containsKey(item))
				waiting.get(item).addValue(n);
			else {
				DescriptiveStatistics ds = new DescriptiveStatistics();
				ds.addValue(n);
				waiting.put(item, ds);
			}
		}

		public HashMap<String, DescriptiveStatistics> getworking() {
			return working;
		}

		public HashMap<String, DescriptiveStatistics> getwaiting() {
			return waiting;
		}
	}
}
