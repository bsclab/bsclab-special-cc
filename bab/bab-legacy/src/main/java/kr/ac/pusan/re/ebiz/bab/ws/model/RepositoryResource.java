/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of 
 * the GNU General Public License as published by the Free Software Foundation; 
 * either version 3 of the License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.re.ebiz.bab.ws.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.api.java.JavaPairRDD;

import scala.Tuple2;

import kr.ac.pusan.re.ebiz.bab.ws.api.repository.IRepositoryReader;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.ICase;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IRepository;
import kr.ac.pusan.re.ebiz.bab.ws.base.model.IResource;

public class RepositoryResource implements IResource, IRepositoryReader {

	private String id;
	private final String uri;
	private IRepositoryReader reader;
	private IRepository repository;
	private JavaPairRDD<String, ICase> casesRdd;
	private Map<String, ICase> cases;

	public RepositoryResource(String uri, IRepositoryReader reader) {
		this.uri = uri;
		this.reader = reader;
	}

	public RepositoryResource(String uri, IRepository repository,
			JavaPairRDD<String, ICase> casesRdd) {
		this.uri = uri;
		this.repository = repository;
		this.casesRdd = casesRdd;
		cases = new LinkedHashMap<String, ICase>();
		List<Tuple2<String, ICase>> bcases = casesRdd.collect();
		for (Tuple2<String, ICase> bcase : bcases) {
			cases.put(bcase._1(), bcase._2());
		}
	}

	public IRepositoryReader getRepositoryReader() {
		return reader;
	}

	@Override
	public IRepository getRepository() {
		return reader == null ? repository : reader.getRepository();
	}

	@Override
	public JavaPairRDD<String, ICase> getCasesRDD() {
		return reader == null ? casesRdd : reader.getCasesRDD();
	}

	@Override
	public Map<String, ICase> getCases() {
		return reader == null ? cases : reader.getCases();
	}

	@Override
	public String getResourceClass() {
		return "IRepository Resource";
	}

	@Override
	public String getId() {
		if (id == null)
			id = "IRepositoryResource@" + String.valueOf(System.nanoTime());
		return id;
	}

	@Override
	public String getUri() {
		return uri;
	}
}
