<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
$ver = 'v1.0';
$api = $this->context->api;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
	<div class="wrap">
		<?php
				NavBar::begin([
						'brandLabel' => 'BAB Web Service',
						'brandUrl' => Yii::$app->homeUrl,
						'options' => [
								'class' => 'navbar-inverse navbar-fixed-top',
						],
				]);
				echo Nav::widget([
						'options' => ['class' => 'navbar-nav navbar-right'],
						'items' => [
								['label' => 'Repository API', 'url' => ['/' . $ver . '/doc/repository']],
								['label' => 'Modelling API', 'url' => ['/' . $ver . '/doc/model']],
								['label' => 'Analysis API', 'url' => ['/' . $ver . '/doc/analysis']],
								['label' => 'About', 'url' => ['/doc/about']],
						],
				]);
				NavBar::end();
		?>
		<div class="container">
				<?= Breadcrumbs::widget([
						'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				]) ?>
			<div class="site-index">
				<div class="body-content">
					<div class="row">
						<div class="col-lg-9">
							<?= $content ?>
						</div>
						<div class="col-lg-3">
							<div class="panel-group" id="accordion">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
												Repository API
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse <?=($api=='repository')?'in':''?>">
										<div class="panel-body">
											<ul class="nav nav-pills nav-stacked">
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/repository/workspacecreation')?>">Workspace Creation</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/repository/workspace')?>">Workspace List</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/repository/import')?>">Event Repository Import</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/repository/list')?>">Repository List</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/repository/view')?>">Repository Statistics</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/repository/summary')?>">Repository Summary</a></li>
												<!--<li><a href="<?=Url::toRoute('/' . $ver . '/doc/repository/export')?>">Event Repository Export</a></li>-->
											</ul>
										</div>
									</div>
								</div>
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
												Modelling API
											</a>
										</h4>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse <?=($api=='model')?'in':''?>">
										<div class="panel-body">
											<ul class="nav nav-pills nav-stacked">
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/model/heuristic')?>">Heuristic Model</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/model/proximity')?>">Proximity Model</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/model/fuzzy')?>">Fuzzy Model</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/model/socialnetwork')?>">Social Network Model</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/model/bayesiannetwork')?>">Bayesian Network Model</a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel panel-primary">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
												Analysis API
											</a>
										</h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse <?=($api=='analysis')?'in':''?>">
										<div class="panel-body">
											<ul class="nav nav-pills nav-stacked">
												<li class="active">Performance Analysis</li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/taskmatrix')?>">Task Matrix</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/dottedchart')?>">Dotted Chart</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/performancechart')?>">Performance Chart</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/timegap')?>">Timegap Analysis</a></li>
												<li class="active">Conformance Checking</li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/conformance')?>">Conformance Checking</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/delta')?>">Delta Analysis</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/associationrule')?>">Association Rules</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/ltlchecker')?>">Linear Temporal Logic Checker</a></li>
												<li class="active">Clustering</li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/kmeansclustering')?>">K-Mean Clustering</a></li>
												<li><a href="<?=Url::toRoute('/' . $ver . '/doc/analysis/hierarchicalclustering')?>">Hierarchical Clustering</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<p class="pull-left">&copy; 2013 - <?= date('Y') ?> <a href="http://e-biz.re.pusan.ac.kr/">BSC Laboratory, Industrial Engineering, Pusan National University</a></p>
			<p class="pull-right"><?= Yii::powered() ?></p>
		</div>
	</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
