<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';

?>
<article>
	<section>
		<h1>Workscape List API Reference</h1>
		<p>Get workspace list for selected users</p>
	</section>
	<section>
		<h2>Description</h2>
		<p>Get workspace list for selected users</p>
		<footer>
			<h3>Refference</h3>
			<ul>
				<li>-</li>
			</ul>
		</footer>
	</section>
	<section>
		<h2>Request</h2>
<pre>GET /api/repository/workspace/[userId]
</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>userId</code></td>
			<td><code>String</code></td>
			<td>User id</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	<section>
		<h2>Response</h2>
<pre>{ 
	"userId": "[userId]",
	"workspaces": {
		"[workspaceId]": {
			"workspaceId": "[workspaceId]",
			"name": "[name]",
			"description: "[description]",
			"fields": {
				"[fieldName]":"[mappingScheme]"
			},
			"noOfRepositories": [repositoryCount]
		}
	}
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>userId</code></td>
			<td><code>String</code></td>
			<td>User id</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>workspaces</code></td>
			<td><code>String</code></td>
			<td>List of workspace for selected user</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>workspaceId</code></td>
			<td><code>String</code></td>
			<td>New candidate workspace Id for data mapping scheme</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>name</code></td>
			<td><code>String</code></td>
			<td>Workspace name</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>description</code></td>
			<td><code>String</code></td>
			<td>Workspace description</td>
			<td>Optional</td>
		</tr>
		<tr>
			<td><code>fields</code></td>
			<td><code>String[String]</code></td>
			<td>List of field name and mapping recommendation for event log :
			<br />* Key : Field Name
			<br />* Value : Mapping Scheme :
			<br />&nbsp;&nbsp;&nbsp;- event.caseId
			<br />&nbsp;&nbsp;&nbsp;- event.activity
			<br />&nbsp;&nbsp;&nbsp;- event.type
			<br />&nbsp;&nbsp;&nbsp;- event.timestamp.start:[format]
			<br />&nbsp;&nbsp;&nbsp;- event.timestamp.complete:[format]
			<br />&nbsp;&nbsp;&nbsp;- event.originator
			<br />&nbsp;&nbsp;&nbsp;- event.resource			
			</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	</section>
	<section>
		<h2>Exception</h2>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Exception</th>
			<th>Description</th>
		</tr>
		</thead>
		<tbody>
		<!--
		<tr>
			<td><code>UnsupportedFileFormat</code></td>
			<td>File format is not supported. Supported file format : .MXML, .MXML.GZ, .CSV, .XLS, .XLSX, .XES</td>
		</tr>
		-->
		</tbody>
		</table>
	</section>
</article>