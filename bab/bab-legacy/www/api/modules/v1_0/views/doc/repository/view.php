<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';

?>
<article>
	<section>
		<h1>Event Repository Statistic API Reference</h1>
		<p>Get event repository simple data statistics</p>
	</section>
	<section>
		<h2>Description</h2>
		<p>Get event repository simple data statistics</p>
		<footer>
			<h3>Refference</h3>
			<ul>
				<li>-</li>
			</ul>
		</footer>
	</section>
	<section>
		<h2>Request</h2>
<pre>POST /api/repository/view/[repositoryId]
{
	"file": "[@SampleEventLogFile]"
}
</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>repositoryId</code></td>
			<td><code>String</code></td>
			<td>Repository Id</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	<section>
		<h2>Response</h2>
<pre>{ 
	"id": "[repositoryId]",
	"originalName": "[originalName]",
	"createdDate": [createdDate],
	"noOfCases": [noOfCases],
	"noOfEvents": [noOfEvents],
	"noOfActivities": [noOfActivities],
	"noOfActivityTypes": [noOfActivityTypes],
	"noOfOriginators": [noOfOriginators],
	"noOfAttributes": [noOfAttributes],
	"cases": {
		"[caseId]": [noOfEvents]
	}
	"activities": {
		"[activity]": [noOfOccurrences]
	},
	"activityTypes": {
		"[activityType]": [noOfOccurrences]
	},
	"originators": {
		"[originator]": [noOfOccurrences]
	},
	"attributes": [
		"[attribute]": [noOfStates]
	]
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>id</code></td>
			<td><code>String</code></td>
			<td>Repository Id</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>originalName</code></td>
			<td><code>String</code></td>
			<td>Repository original file name and upload time</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>createdDate</code></td>
			<td><code>String</code></td>
			<td>Created data or processing date on system</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>noOfCases</code></td>
			<td><code>String</code></td>
			<td>No of cases</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>noOfEvents</code></td>
			<td><code>String</code></td>
			<td>No of events</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>noOfActivities</code></td>
			<td><code>String</code></td>
			<td>No of activity label</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>noOfActivityTypes</code></td>
			<td><code>String</code></td>
			<td>No of activity type labels</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>noOfOriginators</code></td>
			<td><code>String</code></td>
			<td>No of originator labels</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>noOfAttributes</code></td>
			<td><code>String</code></td>
			<td>No of attributes label</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>caseId</code></td>
			<td><code>String</code></td>
			<td>Case Id</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>noOfEvents</code></td>
			<td><code>String</code></td>
			<td>No of events on case</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>activity</code></td>
			<td><code>String</code></td>
			<td>Activity label</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>activityType</code></td>
			<td><code>String</code></td>
			<td>Activity type label</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>originator</code></td>
			<td><code>String</code></td>
			<td>Originator type label</td>
			<td>Yes</td>
		</tr>		
		<tr>
			<td><code>attribute</code></td>
			<td><code>String</code></td>
			<td>Other event attribute label</td>
			<td>Optional</td>
		</tr>
		<tr>
			<td><code>noOfOccurrences</code></td>
			<td><code>String</code></td>
			<td>No of occurrences for corresponding label</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>noOfStates</code></td>
			<td><code>String</code></td>
			<td>No of distinct values for corresponding label</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	</section>
	<section>
		<h2>Exception</h2>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Exception</th>
			<th>Description</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>UnsupportedFileFormat</code></td>
			<td>File format is not supported. Supported file format : .MXML, .MXML.GZ, .CSV, .XLS, .XLSX, .XES</td>
		</tr>
		</tbody>
		</table>
	</section>
</article>