<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to</h1>

        <p class="lead">Best Analytics of Big Data Web Services

        <p class="btn btn-lg btn-success" href="<?= Yii::$app->urlManager->createAbsoluteUrl('/v1.0/api'); ?>"><?= Yii::$app->urlManager->createAbsoluteUrl('/v1.0/api'); ?></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Repository API</h2>

                <p>API Collection for Event Log Preprocessing creating Event Repository.</p>

                <p><a class="btn btn-default" href="<?= Url::toRoute('/v1.0/doc/repository'); ?>">Read More &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Modelling API</h2>

                <p>API Collection for rediscover Business Process Model from Event Repository.</p>

                <p><a class="btn btn-default" href="<?= Url::toRoute('/v1.0/doc/model'); ?>">Read More &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Analysis API</h2>

                <p>API Collection for Business Process Analysis based on Process Mining and Data Mining.</p>

                <p><a class="btn btn-default" href="<?= Url::toRoute('/v1.0/doc/analysis'); ?>">Read More &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
