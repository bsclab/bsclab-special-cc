<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';

?>
<article>
	<section>
		<h1>Event Repository Import API Reference</h1>
		<p>Import new event log into user workspace</p>
	</section>
	<section>
		<h2>Description</h2>
		<p>Import new event log into user workspace, new event log should have same mapping scheme with workspace</p>
		<footer>
			<h3>Refference</h3>
			<ul>
				<li>-</li>
			</ul>
		</footer>
	</section>
	<section>
		<h2>Request</h2>
<pre>POST /api/repository/workspace/import/[workspaceId]
{
	"file": "[@SampleEventLogFile]"
}
</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>file</code></td>
			<td><code>File</code></td>
			<td>Sample event log for event repository data mapping format. 
			Other event log stored in same workspace should contains same data mapping scheme or it will be rejected by system</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	<section>
		<h2>Response</h2>
<pre>{ 
	
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	</section>
	<section>
		<h2>Exception</h2>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Exception</th>
			<th>Description</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>UnsupportedFileFormat</code></td>
			<td>File format is not supported. Supported file format : .MXML, .MXML.GZ, .CSV, .XLS, .XLSX, .XES</td>
		</tr>
		</tbody>
		</table>
	</section>
</article>