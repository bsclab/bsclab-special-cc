<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';

?>
<article>
	<section>
		<h1> API Referrence</h1>
		<p></p>
	</section>
	<section>
		<h2>Description</h2>
		<p>
		</p>
		<footer>
			<h3>Refference</h3>
			<ul>
				<li>-</li>
			</ul>
		</footer>
	</section>
	<section>
		<h2>Request</h2>
<pre>GET /api/
{
}
</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	<section>
		<h2>Response</h2>
<pre>{ 
	
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code></code></td>
			<td><code>String</code></td>
			<td></td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	</section>
	<section>
		<h2>Exception</h2>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Exception</th>
			<th>Description</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code></code></td>
			<td></td>
		</tr>
		</tbody>
		</table>
	</section>
</article>