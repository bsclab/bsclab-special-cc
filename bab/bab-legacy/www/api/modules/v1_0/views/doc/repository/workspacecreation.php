<?php

use yii\helpers\Url;

/**
 * @var yii\web\View $this
 */
$this->title = 'BAB Web Services';

?>
<article>
	<section>
		<h1>Workscape Creation API Reference</h1>
		<p>Create new workspace for event repository storage</p>
	</section>
	<section>
		<h2>Description</h2>
		<p>Workspace should be created before storing event log with same data mapping structure and format<br />
		This API require two request part : <br />
			<ol>
				<li>Upload sample event log</li>
				<li>Register data mapping scheme</li>
			</ol>
		</p>
		<footer>
			<h3>Refference</h3>
			<ul>
				<li>-</li>
			</ul>
		</footer>
	</section>
	<section>
		<h2>Request &amp; Response</h2>
		<h3>1. Upload sample event log</h3>
<pre>POST /api/repository/workspace 
{
	"file": "[@SampleEventLogFile]"
}</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>file</code></td>
			<td><code>File</code></td>
			<td>Sample event log for event repository data mapping format. 
			Other event log stored in same workspace should contains same data mapping scheme or it will be rejected by system</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
		<p>This part will return candidate workspace Id for data mapping scheme as follows : </p>
<pre>{ 
	"workspaceId": "[workspaceId]",
	"fields": {
		"[fieldName]":"[mappingRecommendation]"
	}
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>workspaceId</code></td>
			<td><code>String</code></td>
			<td>New candidate workspace Id for data mapping scheme</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>fields</code></td>
			<td><code>String[String]</code></td>
			<td>List of field name and mapping recommendation for event log :
			<br />* Key : Field Name
			<br />* Value : Mapping Scheme :
			<br />&nbsp;&nbsp;&nbsp;- event.caseId
			<br />&nbsp;&nbsp;&nbsp;- event.activity
			<br />&nbsp;&nbsp;&nbsp;- event.type
			<br />&nbsp;&nbsp;&nbsp;- event.timestamp.start:[format]
			<br />&nbsp;&nbsp;&nbsp;- event.timestamp.complete:[format]
			<br />&nbsp;&nbsp;&nbsp;- event.originator
			<br />&nbsp;&nbsp;&nbsp;- event.resource			
			</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
		<h3>2. Register data mapping scheme</h3>
<pre>POST /api/repository/workspace/map/[workspaceId]
{
	"name": "[Workspace Name]",
	"description": "[Workspace Description]",
	"map": {
		"ProcessInstance.Id": "event.caseId",
		"AuditTrailEntry.WorkflowModelElement": "event.activity",
		"AuditTrailEntry.Type": "event.type",
		"AuditTrailEntry.Timestamp": "event.timestamp.complete:YYYY-MM-DD",
		"AuditTrailEntry.Originator": "event.originator",
		"AuditTrailEntry.Resource": "event.resource"
	},
}</pre>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Required</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>name</code></td>
			<td><code>String</code></td>
			<td>Workspace name</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>description</code></td>
			<td><code>String</code></td>
			<td>Workspace description</td>
			<td>Optional</td>
		</tr>
		<tr>
			<td><code>map</code></td>
			<td><code>String[String]</code></td>
			<td>Field mapping for event log :
			<br />* Key : Field Name
			<br />* Value : Mapping Scheme :
			<br />&nbsp;&nbsp;&nbsp;- event.caseId
			<br />&nbsp;&nbsp;&nbsp;- event.activity
			<br />&nbsp;&nbsp;&nbsp;- event.type
			<br />&nbsp;&nbsp;&nbsp;- event.timestamp.start:[format]
			<br />&nbsp;&nbsp;&nbsp;- event.timestamp.complete:[format]
			<br />&nbsp;&nbsp;&nbsp;- event.originator
			<br />&nbsp;&nbsp;&nbsp;- event.resource
			</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
		<p>This part will return fixed workspace id and it's properties : </p>
<pre>{ 
	"workspaceId": "[workspaceId]",
	"name": "[name]",
	"description: "[description]",
	"fields": {
		"[fieldName]":"[mappingScheme]"
	}
}</pre>
	<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Name</th>
			<th width="15%">Type</th>
			<th>Description</th>
			<th width="15%">Always</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>workspaceId</code></td>
			<td><code>String</code></td>
			<td>New candidate workspace Id for data mapping scheme</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>name</code></td>
			<td><code>String</code></td>
			<td>Workspace name</td>
			<td>Yes</td>
		</tr>
		<tr>
			<td><code>description</code></td>
			<td><code>String</code></td>
			<td>Workspace description</td>
			<td>Optional</td>
		</tr>
		<tr>
			<td><code>fields</code></td>
			<td><code>String[String]</code></td>
			<td>List of field name and mapping recommendation for event log :
			<br />* Key : Field Name
			<br />* Value : Mapping Scheme :
			<br />&nbsp;&nbsp;&nbsp;- event.caseId
			<br />&nbsp;&nbsp;&nbsp;- event.activity
			<br />&nbsp;&nbsp;&nbsp;- event.type
			<br />&nbsp;&nbsp;&nbsp;- event.timestamp.start:[format]
			<br />&nbsp;&nbsp;&nbsp;- event.timestamp.complete:[format]
			<br />&nbsp;&nbsp;&nbsp;- event.originator
			<br />&nbsp;&nbsp;&nbsp;- event.resource			
			</td>
			<td>Yes</td>
		</tr>
		</tbody>
		</table>
	</section>
	<section>
		<h2>Exception</h2>
		<table class="table table-bordered">
		<thead>
		<tr>
			<th width="25%">Exception</th>
			<th>Description</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td><code>UnsupportedFileFormat</code></td>
			<td>File format is not supported. Supported file format : .MXML, .MXML.GZ, .CSV, .XLS, .XLSX, .XES</td>
		</tr>
		</tbody>
		</table>
	</section>
</article>