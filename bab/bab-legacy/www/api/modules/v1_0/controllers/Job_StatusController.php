<?php

namespace app\modules\v1_0\controllers;

use app\components\ServiceController;

class Job_StatusController extends ServiceController {

	public function actionRead($resource) {
		$html = file_get_contents($this->params['spark']['master']);
		$response = array();
		$find = strpos($html, base64_decode($resource));
		if ($find !== FALSE) {
			$revbefore = strrev(substr($html, 0, $find));
			$before = strrev(substr($revbefore, 0, strpos($revbefore, '>rt<')));
			$after = substr($html, $find, strpos($html, '</tr>', $find) - $find);
			$rdatas = explode('</td><td', str_replace("\n", '', str_replace(' ', '', $before . $after)));
			$datas = array();
			foreach ($rdatas as $rdata) {
				$data = strip_tags($rdata);
				$tag = strpos($data, '>');
				if ($tag !== FALSE) {
					$data = substr($data, $tag + 1);
				}
				$datas[] = $data;
			}
			$response['ID'] = $datas[0];
			$response['Name'] = $datas[1];
			$response['Core'] = $datas[2];
			$response['Memory'] = $datas[3];
			$response['Time'] = $datas[4];
			$response['User'] = $datas[5];
			$response['State'] = $datas[6];
			$response['Duration'] = $datas[7];
		}
		return $response;
	}
}
