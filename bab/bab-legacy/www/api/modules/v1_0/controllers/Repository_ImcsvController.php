<?php

namespace app\modules\v1_0\controllers;

use app\components\ServiceController;
use yii\web\UploadedFile;

class Repository_ImcsvController extends ServiceController {

	public function actionRead($resource) {
		list($workspaceId, $repositoryId) = explode('_', $resource, 2);
		return $this->runBabJob($resource, 'RepositoryCsvImportJob', 'mrepo', array(
			'rawPath' => $this->hdfsHome . '/tmp/' . $repositoryId . '.csv',
		));
	}
	
}