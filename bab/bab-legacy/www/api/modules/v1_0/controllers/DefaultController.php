<?php

namespace app\modules\v1_0\controllers;

use app\components\Controller;

class DefaultController extends Controller {

	public function actionIndex() {
		$this->redirect('v1.0/doc');
	}
}
