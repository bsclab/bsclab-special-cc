<?php

namespace app\modules\v1_0\controllers;

use app\components\ServiceController;
use yii\web\UploadedFile;

class Repository_WorkspaceController extends ServiceController {

	public function actionIndex() {
		$dir = $this->hdfsExecute($this->hdfsHome . '/?op=LISTSTATUS');
		$files = array();
		if (isset($dir['FileStatuses']['FileStatus'])) {
			foreach ($dir['FileStatuses']['FileStatus'] as $file) {
				if ($file['type'] == 'DIRECTORY') {
					$workspace = $file['pathSuffix'];
					if ($workspace != 'tmp') {
						$files[$file['pathSuffix']] = array(
							 'workspaceId' => $workspace,
						);
					}
				} 
			}
		}
		rsort($files);
		$response = array(
			'workspaces'  => $files,
		);
		return $response;
	}
	
	public function actionRead($resource, $ext = 'brepo') {
		$dir = $this->hdfsExecute($this->hdfsHome . '/' . $resource . '?op=LISTSTATUS');
		$files = array();
		$len = (1 + strlen($ext));
		if (isset($dir['FileStatuses']['FileStatus'])) {
			foreach ($dir['FileStatuses']['FileStatus'] as $file) {
				if ($file['type'] == 'FILE') {
					
					if (substr($file['pathSuffix'], - $len) == '.' . $ext) {
						$repository = substr($file['pathSuffix'], 0, strlen($file['pathSuffix']) - $len);
						$files[$file['pathSuffix']]['repositoryId'] = $resource . '_' . $repository;
						//$files[$file['pathSuffix']]['summary'] = json_decode($this->hdfsLoad($this->hdfsHome . '/' . $resource . '/' . $repository . '.brepo?op=OPEN'), true);
					}
				} 
			}
		} else {
			return array('exception' => 'Workspace ' . $resource . ' not found.');
		}
		rsort($files);
		$response = array(
			'repositories'  => $files,
		);
		return $response;
	}

	public function actionDataset($resource) {
		return $this->actionRead($resource, 'mrepo');
	}
	
	public function actionRepository($resource, $ext = 'brepo') {
		list($workspaceId, $datasetId) = explode('_', $resource, 2);
		$dir = $this->hdfsExecute($this->hdfsHome . '/' . $workspaceId . '?op=LISTSTATUS');
		$files = array();
		$len = (1 + strlen($ext));
		$plen = strlen($datasetId);
		if (isset($dir['FileStatuses']['FileStatus'])) {
			foreach ($dir['FileStatuses']['FileStatus'] as $file) {
				if ($file['type'] == 'FILE') {
					
					if (substr($file['pathSuffix'], - $len) == '.' . $ext && substr($file['pathSuffix'], 0, $plen) == $datasetId) {
						$repository = substr($file['pathSuffix'], 0, strlen($file['pathSuffix']) - $len);
						$files[$file['pathSuffix']]['repositoryId'] = $workspaceId . '_' . $repository;
						//$files[$file['pathSuffix']]['summary'] = json_decode($this->hdfsLoad($this->hdfsHome . '/' . $resource . '/' . $repository . '.brepo?op=OPEN'), true);
					}
				} 
			}
		} else {
			return array('exception' => 'Workspace ' . $resource . ' not found.');
		}
		rsort($files);
		$response = array(
			'repositories'  => $files,
		);
		return $response;
	}

	public function actionCreate()	{
		$repository = UploadedFile::getInstanceByName('file');
		if ($repository != null && $repository->tempName != '') {
			$name = $repository->name;
			if (substr($name, strlen($name) - 5) == '.mxml') {
				$response = array();
				$import = str_replace(' ', '', substr($name, 0, strlen($name) - 5)) . '' . date('YmdHis', time());
				$this->hdfsSave($this->hdfsHome . '/tmp/' . $import . '.mxml?op=CREATE&overwrite=true', $repository->tempName);
				$this->executorRun('MxmlImportJob', $import . ' ' . $import);
				$response['workspaceId'] = $import;
				$response['repositoryId'] =  $response['workspaceId'] . '_' . $import;
				return $response;
			} else {
				return array('exception' => 'Not supported file format.');
			}
		}
		return array('exception' => 'Illegal argument, please specify POST [file=] parameters.');
	}

	public function actionImport($resource)	{
		$repository = UploadedFile::getInstanceByName('file');
		if ($repository != null && $repository->tempName != '') {
			$name = str_replace('-', '', str_replace('_', '', $repository->name));
			if (substr($name, strlen($name) - 5) == '.mxml') {
				$response = array();
				$import = str_replace(' ', '', substr($name, 0, strlen($name) - 5)) . '' . date('YmdHis', time());
				$this->hdfsSave($this->hdfsHome . '/tmp/' . $import . '.mxml?op=CREATE&overwrite=true', $repository->tempName);
				//$this->executorRun('MxmlImportJob', $resource . ' ' . $import);
				$response['workspaceId'] = $resource;
				$response['repositoryId'] = $response['workspaceId'] . '_' . $import;
				$response['ext'] = 'mxml';
				return $response;
			} else if (substr($name, strlen($name) - 8) == '.mxml.gz') {
				$response = array();
				$import = str_replace(' ', '', substr($name, 0, strlen($name) - 8)) . '' . date('YmdHis', time());
				$this->hdfsSave($this->hdfsHome . '/tmp/' . $import . '.mxml.gz?op=CREATE&overwrite=true', $repository->tempName);
				//$this->executorRun('MxmlGzImportJob', $resource . ' ' . $import);
				$response['workspaceId'] = $resource;
				$response['repositoryId'] = $response['workspaceId'] . '_' . $import;
				$response['ext'] = 'mxmlgz';
				//$this->runBabJob($response['repositoryId'], 'MxmlGzImportJob', 'brepo');
				return $response;
			} else if (substr($name, strlen($name) - 5) == '.bpmn') {
				$response = array();
				$import = str_replace(' ', '', substr($name, 0, strlen($name) - 5)) . '' . date('YmdHis', time());
				$this->hdfsSave($this->hdfsHome . '/tmp/' . $import . '.bpmn?op=CREATE&overwrite=true', $repository->tempName);
				//$this->executorRun('MxmlGzImportJob', $resource . ' ' . $import);
				$response['workspaceId'] = $resource;
				$response['repositoryId'] = $response['workspaceId'] . '_' . $import;
				$response['ext'] = 'bpmn';
				//$this->runBabJob($response['repositoryId'], 'MxmlGzImportJob', 'brepo');
				return $response;
			} else if (substr($name, strlen($name) - 4) == '.csv') {
				$response = array();
				$import = str_replace(' ', '', substr($name, 0, strlen($name) - 4)) . '' . date('YmdHis', time());
				$this->hdfsSave($this->hdfsHome . '/tmp/' . $import . '.csv?op=CREATE&overwrite=true', $repository->tempName);
				$response['workspaceId'] = $resource;
				$response['repositoryId'] = $response['workspaceId'] . '_' . $import;
				$response['ext'] = 'csv';
				//$_POST = array(
					//'rawPath' => 
				//);
				//$this->runBabJob($response['workspaceId'] . '_' . $import, 'CsvImportJob', 'mrepo');
				
				return $response;
			} else {
				return array('exception' => 'Unsupported file format.');
			}
		}
		return array('exception' => 'Illegal argument, please specify POST [file=] parameters.');
	}
	
	public function actionDelete($resource) {
		list($workspaceId, $repositoryId) = explode('_', $resource, 2);

		$dir = $this->hdfsExecute($this->hdfsHome . '/' . $workspaceId . '?op=LISTSTATUS');
		$len = strlen($repositoryId);
		foreach ($dir['FileStatuses']['FileStatus'] as $file) {
			if (substr($file['pathSuffix'], 0, $len) == $repositoryId) {
				$this->hdfsExecute($this->hdfsHome . '/' . $workspaceId . '/' . $file['pathSuffix'] . '?op=RENAME&destination=/bab/workspaces/' . $workspaceId . '/' . $file['pathSuffix']  . '.deleted', 'PUT');
			};
		}
		$response = array(
			'deleted'  => $resource,
		);
		return $response;
	}
}