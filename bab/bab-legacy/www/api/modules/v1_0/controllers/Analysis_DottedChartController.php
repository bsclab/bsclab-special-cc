<?php
 
namespace app\modules\v1_0\controllers;

use app\components\ServiceController;
use yii\web\UploadedFile;

class Analysis_DottedChartController extends ServiceController {

	public function actionRead($resource) {
		return $this->runBabJob($resource, 'AnalysisDottedChartJob', 'dcans');
	}
	
	public function actionRead2($resource) {
		list($workspaceId, $repositoryId) = explode('_', $resource, 2);
		$path = '/' . $workspaceId . '/' . $repositoryId . '.dcans';
		$response = array();
		if ($this->hdfsFileExists($path)) {
			return json_decode($this->hdfsLoad($this->hdfsHome . $path . '?op=OPEN'), true);
		} else {
			$this->executorRun('AnalysisDottedChartJob', $workspaceId . ' ' . $repositoryId);
			$response['returnUri'] = $resource;
		}; 
		return $response;
	}

}