<?php

if (isset($_GET['cmd'])) {
	echo 'Ideas PHP Service Gateway, running : ' . $_GET['cmd'] . ' ... ';
	$con = ssh2_connect('localhost', 22);
	ssh2_auth_password($con, 'ideas', 'indonesian');
	ssh2_exec($con, 'java -jar /var/bab/' . $_GET['cmd'] . '.jar ' . base64_decode($_GET['args']) . ' > /dev/null &');
	ssh2_exec($con, 'exit');
	unset($con);
	echo 'Done';
} else {
	echo 'Ideas PHP Service Gateway, please specify POST cmd command.';
}
