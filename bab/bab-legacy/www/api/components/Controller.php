<?php

namespace app\components;

use Yii;
use yii\web\Controller as YiiController;

class Controller extends YiiController {
	public function ver() {
		return Yii::$app->params['latestVersion'];
	}
}