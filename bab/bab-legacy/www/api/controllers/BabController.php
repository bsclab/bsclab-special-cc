<?php

namespace app\controllers;

use app\components\Controller;

class BabController extends Controller {
    public function actionIndex() {
		return $this->redirect('/bab/web/index.php/home');
        /*/
				return $this->render('index', [
					'ver' => $this->ver(),
				]);
        //*/
    }

    public function actionAbout() {
        return $this->render('about');
    }
}
