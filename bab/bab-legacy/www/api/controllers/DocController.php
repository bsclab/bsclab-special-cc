<?php

namespace app\controllers;

use app\components\Controller;

class DocController extends Controller {
    public function actionIndex() {
			$this->redirect($this->ver() . '/doc');
    }

    public function actionRepository() {
			$this->redirect($this->ver() . '/doc/repository');
    }

    public function actionModel() {
			$this->redirect($this->ver() . '/doc/model');
    }

    public function actionAnalysis() {
			$this->redirect($this->ver() . '/doc/analysis');
    }
}
