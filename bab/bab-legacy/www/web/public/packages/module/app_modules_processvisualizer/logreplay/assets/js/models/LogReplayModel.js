// Filename: models/EventModel
var LogReplayModel = Backbone.RelationalModel.extend({
    defaults: {
      state: 'stop-initial',
      simulation: 0,
      real:0,
      current:0,
      start: 0,
      finish:0,
      duration:0,
      timeScaling:0,
      totalEvent:0,
      parts:[],	// partition 
      jsonEventsHeader: null, 
      jsonProcessModel: null, 
      baseUrl: null,
      isShowTokenCaseId   : false, 
      isShowTokenDefaultColor: false,
      numberTokenRunning: 0,
      numberTokenFinish: 0,
      eventsProjected: [],
      eventsProjectedCount: 0,
      eventsNotProjected: [],
      eventsNotProjectedCount: 0,
      isHideTokenUnselectedAttributes: false,
      syncIndicator: true,
      enableGlobalKPI: false,
      enableArcNodeKPI: false,
      enableTokenFadeOut: true
    },

    relations:[
      {
        type: Backbone.HasOne,
        key: 'currentPartition',
        relatedModel: 'PartitionModel'
      }
    ], 

    initialize: function(){
        var format = d3.time.format("%Y-%m-%d");
        this.set({ 
            start: this.get('start')/1000,
            finish: this.get('finish')/1000
        });
        
        startDate = format(new Date(this.get('start')*1000)); // returns a string
        finishDate = format(new Date(this.get('finish')*1000));
        initialData = [ 
          { date: new Date(startDate), value: 0 }, 
          { date: new Date(finishDate), value: 1 }
        ];
        this.set({
            eventsProjected: initialData,
            eventsNotProjected: _.extend([], initialData)
        });
        this.set({ duration: (this.get('finish')-this.get('start')) });
      
        // set max speed
        this.set({ maxSpeed : this.get('duration')/(60*5) }); //because default speed value is 1000
      
        this.setTheRealTime();
      
        // this.on('change:current', this.setRealTime, this);
        this.on('change:current', this.setTheRealTime, this);
    },

    getVariableOptions: function(varName){
      filtered = _.findWhere(this.get('variables'), {name: varName, type:'array'});
      return filtered.options;
    },

    setTheRealTime: function(){
      this.set({ real: this.get('current')+this.get('start') });

      if(this.get('current') >= this.get('duration')){
        this.set({ current: this.get('duration') });
      }
    }, 

    setRealTime: function(){
      // console.log('set Real Time!')
      var result = (this.get('current') + this.get('start')).toString();
    
      /* set real time and simulation time */
      this.set({'real': result.substr(0,result.length-1)+0});  // to set last increment time always 0
      this.set({'simulation': this.get('current')});

      /* if increment more than maximum value, then set to maximum value */
      if(this.get('simulation') >= this.get('duration')){
        this.set({'simulation' : this.get('duration')});
      }
    }

  });