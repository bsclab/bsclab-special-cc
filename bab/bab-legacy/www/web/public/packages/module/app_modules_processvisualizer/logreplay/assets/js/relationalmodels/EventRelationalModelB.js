// Filename: models/EventModel
var EventRelationalModelB = Backbone.RelationalModel.extend({
    defaults: {
      id: 0,
      selector: 'the-id',
      d3selector: null,
      caseId: 0,
      name: "Harry Potter",
      startTimeline:0,
      start: 0,
      complete: 0,
      duration: 0, 
      offset: 0, 
      state: 'stop-initial',
      isShowTokenDefaultColor: false,
      isShowTokenCaseId: true
    }, 
     /* initialize selector here */
    initialize: function(){
      this.set({id: 'event-'+this.get('id')});
      this.set({ duration: this.setDuration() });
      this.set({ offset: this.setOffset() });
      this.set({ selector: '#'+this.get('id') });
    },
    relations: [
      {
        type: Backbone.HasOne, 
        key: 'arc', 
        relatedModel: 'ArcRelationalModelB', 
      },
    ], 

    setD3Selector: function(){
      return d3.select(this.get('selector'));
    }, 

    setDuration: function(){
      return this.get('complete')-this.get('start');
    }, 

    setOffset: function(){
      return this.get('start')-this.get('startTimeline');
    },

    // setId: function(){
    //   return S('case '+this.get('start')+' '+this.get('completed')+' '+this.get('caseId')).slugify().s ;
    // }

  });