ProjectedChartView = Backbone.View.extend({
	initialize: function(options){
		this.options = options || {};
		// this.model.on('change:eventsProjectedCount', this.updateEventsProjected, this);
		// this.model.on('change:eventsNotProjectedCount', this.updateEventsNotProjected, this);
		this.model.on('change:eventsProjectedCount', this.updateEventsProjectedCombined, this);

		this.$projectedCombinedBarChartEl = this.$el.find('#projected-events-barchart-combined');
		this.projectedCombinedBarChartTemplate = Handlebars.compile($('#progressbar-stacked-template').html());

		this.eventsProjectedParam = {
		    title: "Projected Chart",
		    description: "Events projected into process model.",
		    data: this.model.get('eventsProjected'),
		    width: this.$el[0].clientWidth,
		    height: 175,
		    left:0,
		    right:0,
		    interpolate: 'linear',
		    // xax_count: 4,
		    target: '#projected-events-chart',
		    missing_is_zero: false,
		    x_accessor: 'date',
		    y_accessor: 'value'
		};

		this.eventsNotProjectedParam = {
			title: "Not Projected Chart",
		    description: "Events projected into process model.",
		    data: this.model.get('eventsNotProjected'),
		    width: this.$el[0].clientWidth,
		    height: 175,
		    left:0,
		    right:0,
		    interpolate: 'linear',
		    // xax_count: 4,
		    target: '#notprojected-events-chart',
		    missing_is_zero: false,
		    x_accessor: 'date',
		    y_accessor: 'value'
		},

		this.eventsProjectedCombinedParam = {
			title: "Projected and Not Projected Tokens",
		    description: "Events projected into process model.",
		    data: [this.model.get('eventsProjected'), this.model.get('eventsNotProjected')],
		    width: this.$el[0].clientWidth,
		    // full_width: true,
		    height: 175,
		    left:0,
		    right:0,
		    interpolate: 'linear',
		    // xax_count: 4,
		    target: '#projected-events-linechart-combined',
		    missing_is_hidden: false,
		    x_accessor: 'date',
		    y_accessor: 'value',
		    aggregate_rollover: true,
		    legend: ['Projected', 'Not Projected'],
		    legend_target: '#projected-events-linechart-combined-legend',
		    custom_line_color_map: ['projected', 'notprojected']
		}
		this.render();
	},
	render: function(){
		MG.data_graphic(this.eventsProjectedCombinedParam);
		this.$projectedCombinedBarChartEl.html(this.projectedCombinedBarChartTemplate({
			value1: numeral(this.model.get('eventsProjectedCount')).format('0,0'),
			value2: numeral(this.model.get('eventsNotProjectedCount')).format('0,0'),
			value1Percent: 100*this.model.get('eventsProjectedCount') / ( this.model.get('eventsProjectedCount')+this.model.get('eventsNotProjectedCount') ),
			value2Percent: 100*this.model.get('eventsNotProjectedCount') / ( this.model.get('eventsProjectedCount')+this.model.get('eventsNotProjectedCount') )
		}));
	},
	
	updateEventsProjected: function(){
		console.log('eventsProjected updated');
		this.eventsProjectedParam.data = this.model.get('eventsProjected');
		MG.data_graphic(this.eventsProjectedParam);
	},

	updateEventsNotProjected: function(){
		console.log('eventsNotProjected updated');
		this.eventsNotProjectedParam.data = this.model.get('eventsNotProjected');
		MG.data_graphic(this.eventsNotProjectedParam);
	},

	updateEventsProjectedCombined: function(){
		console.log('eventsProjected Combined updated');
		// this.eventsProjectedCombinedParam.missing_is_hidden = true;
		this.eventsProjectedCombinedParam.data = [this.model.get('eventsProjected'), this.model.get('eventsNotProjected')];
		this.render();		
	}, 

	resizeProjectedCombined: function(){
		this.eventsProjectedCombinedParam.width = this.$el[0].clientWidth;
		this.render();
	}
});