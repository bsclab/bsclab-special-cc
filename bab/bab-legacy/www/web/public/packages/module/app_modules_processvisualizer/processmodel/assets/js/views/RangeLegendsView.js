var RangeLegendsView = Backbone.View.extend({


    initialize: function(){
        this.template= Handlebars.compile($('#range-template').html()),
        this.render();
    },

    render: function(){
        this.$el.html(this.template(this.model.attributes));
        return this;
    }, 
});