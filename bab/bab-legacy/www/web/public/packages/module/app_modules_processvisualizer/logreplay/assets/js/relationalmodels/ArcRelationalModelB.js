// Filename: models/EventModel
var ArcRelationalModelB = Backbone.RelationalModel.extend({
    defaults: {
    	id: 'the-id',
    	selector: 'the-id',
    	d3selector: null,
      	name: 'The Name', 
      	frequency: 0, 
      	dependency: 0, 
      	pathLength: 0,
      	path: null,
    },
     /* initialize selector here */
    initialize: function(){
    	this.set({ selector: '#'+this.get('id') });
    	this.set({ d3selector: d3.select('#'+this.get('id')) });
		  this.set({ path: this.setPath() });
    },

    relations:[
    	{
	    	type: Backbone.HasOne,
			key: 'nodeSource',
			relatedModel: 'NodeRelationalModelB'
	    },
	    {
	    	type: Backbone.HasOne,
			key: 'nodeTarget',
			relatedModel: 'NodeRelationalModelB',
	    }, 
	], 

	setPath: function(){
		return document.querySelector(this.get('selector')+' path');
	} 

  });