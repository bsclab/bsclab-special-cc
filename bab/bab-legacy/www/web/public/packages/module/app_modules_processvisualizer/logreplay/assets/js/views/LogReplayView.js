LogReplayView = Backbone.View.extend({
	events: {
		'click button#button_play': 'buttonPlayPress',
		'click button#button_stop': 'buttonStopPress',
		'click button#button_repeat': 'buttonRepeatPress',
	},

	initialize: function(options){
		this.options = options || {};

		this.timeline = new TimelineLite();
		this.partitionCollection = new PartitionCollection();
		this.eventCollection = new EventCollection();
		
		// this.nodeKPICollection = new NodeKPICollection();
		// this.arcKPICollection = new ArcKPICollection();

		// var filtered = new FilteredCollection(this.eventCollection);
		// this.eventCollectionFinish = filtered.filterBy({state: 'finish'});
		// this.eventCollectionRunning = filtered.filterBy({state: 'running'});

		this.$sliderTimelineSlidingEl = $('.slider_timeline .sliding');
		this.$sliderSpeedSlidingEl = $(".slider_speed .sliding");
		this.$sliderSpeedInfoEl = $(".slider_speed .info");
		this.$modalEl = $('#myModal1');

		this.nodeViewCollection = [];
		this.arcViewCollection = [];
		// this.initializeExtendedProcessModel();
		this.render();
		this.initializeSlider();
		this.animationMode();

		this.prepareAnimation();
		this.initializeAnimation();
		this.performanceMonitoring();
	},

	render: function(){
		$('#animation-starttime span').html(moment(this.model.get('start')*1000).format('ddd, MMM Do YYYY HH:mm:ss'));
		$('#animation-finishtime span').html(moment(this.model.get('finish')*1000).format('ddd, MMM Do YYYY HH:mm:ss'));

		this.$modalEl.modal({
		  keyboard: false,
		  backdrop: 'static'
		});

		TheModel = Backbone.Model.extend();

		this.gaugeIndicator = new GaugeIndicatorView({
            el: '#gauge-indicator',
            collection: this.eventCollection,
            model: new TheModel({
                totalEvent: this.model.get('totalEvent')
            })
        });

        d3.selectAll('.disable-in-page').classed('disabled', true);
	},

	/* extend current process model view (Node and Arc) so will be able to handle showing indicator */
	initializeExtendedProcessModel: function(){
		var self = this;		
		
		this.options.nodeCollection.forEach(function(value, key){
			var nodeKPIView = new NodeKPIView({
				el: value.get('selector')+' g',
				model: value,
				collection: self.nodeKPICollection
			})
			self.nodeViewCollection.push(nodeKPIView);
		});

		this.options.arcCollection.forEach(function(value, key){
			var arcKPIView = new ArcKPIView({
				el: value.get('labelSelector'),
				model: value,
				collection: self.arcKPICollection
			})
			self.arcViewCollection.push(arcKPIView);
		});
		// console.log(self.nodeViewCollection);
		// console.log(self.arcViewCollection);
	},

	initializeSlider: function(){
		var self = this;
		var start = this.model.get('start');

		this.$sliderTimelineSlidingEl.slider({
			range: false,
			min: 0,
			max: this.model.get('duration'),
			value: this.model.get('current'),
			step: this.model.get('stepTimelineSlider'),
			// formater: function(value){
			// 	return moment((self.model.get('current')+self.model.get('start'))*1000).format('ddd, MMM Do YYYY HH:mm:ss');
			// }
		})
		.on('slide', function(event, ui){
			if(self.model.get('state')=='play'){
				self.model.set({state: 'pause' });
				self.changeButtonColor();
				self.model.set({ 
					enableGlobalKPI: false,
					enableArcNodeKPI: false
				});
			}
			self.timeline.pause();
			// self.model.set({current: ev.value });
			self.timeline.progress(ui.value/self.model.get('duration'));
		})
		.on('slidestop', function(event, ui){
			if(self.model.get('state')=='pause'){
				self.model.set({state: 'play'});
				self.changeButtonColor();
				// self.model.set({ 
				// 	enableGlobalKPI: true,
				// 	enableArcNodeKPI: true
				// });
			}
			// self.model.set({current: ev.value });
			self.timeline.progress(ui.value/self.model.get('duration'));
			self.timeline.play();
		});

		this.$sliderSpeedSlidingEl.slider({
			range: false, 
			min: this.model.get('minSpeed'),
			max: this.model.get('maxSpeed'),
			value: this.model.get('currentSpeed'),
			step: this.model.get('stepSpeedSlider'),
			// formater: function(value){
			// 	return numeral(value / self.model.get('minSpeed')).format('0,0')+'x';
			// }
		})
		.on('slide', function(event, ui){
			self.model.set({ currentSpeed : ui.value });
			self.timeline.timeScale(self.model.get('currentSpeed'));
			self.$sliderSpeedInfoEl.text(numeral(self.model.get('currentSpeed')/self.model.get('minSpeed')).format('0,0')+'x faster');
		})
		.on('slidestop', function(event, ui){
			self.model.set({ currentSpeed : ui.value });
			self.timeline.timeScale(self.model.get('currentSpeed'));
			self.$sliderSpeedInfoEl.text(numeral(self.model.get('currentSpeed')/self.model.get('minSpeed')).format('0,0')+'x faster');
		});

		this.$sliderSpeedInfoEl.text(numeral(self.model.get('currentSpeed')/self.model.get('minSpeed')).format('0,0')+'x faster');
	},

	prepareAnimation: function(){
		var newTween = TweenLite.to('.timeline-master', this.model.get('duration'), {
			width: '100%',
		});
		newTween.eventCallback('onUpdate', this.updateSlider, ["{self}", this]);

		this.timeline.add(newTween);
		this.timeline.timeScale(this.model.get('currentSpeed'));
		this.timeline.pause();
	},

	performanceMonitoring: function(){
		var stats = new Stats();
		stats.setMode(0); // 0: fps, 1: ms, 2: mb

		// align top-left
		stats.domElement.style.position = 'absolute';
		stats.domElement.style.right = '15px';
		stats.domElement.style.bottom = '7px';

		this.$el.find('#content-graph').append(stats.domElement);

		var update = function () {
		    stats.begin();
		    // monitored code goes here
		    stats.end();
		    requestAnimationFrame( update );
		};

		requestAnimationFrame( update );
	},

	initializeAnimation: function(){
		var self = this;
		var jsonEventsHeader = this.model.get('jsonEventsHeader');

		// event partitions should be defined
		if(!_.isUndefined(jsonEventsHeader.eventPartitions)){

			jsonEventsHeader.eventPartitions.forEach(function(value, key){
				// initialize partition model
				var partitionModel = new PartitionModel({
					part: key,
					start: value.start,
					nextPartitionStart: 0,
					isLastPartition: false,
					startDateFormatted: moment(value.start).format('MMM Do YYYY'),
					startTimeFormatted: moment(value.start).format('HH:mm'),
					complete: value.complete,
					completeDateFormatted: moment(value.complete).format('MMM Do YYYY'),
					completeTimeFormatted: moment(value.complete).format('HH:mm'),
					url: value.url,
					state: 'NA',
					loadingTime: 0,
					startMaster: self.model.get('start'),
					eventsProjected:0,
					eventsNotProjected:0
				});		
				// initialize partition view
				var rowView = new TableRowPartitionView({
			    	model: partitionModel,
			    	id: 'table-row-part-'+partitionModel.get('key'),
			    	tagName: 'tr'
			    });
			    $('table#pm-table tbody').append(rowView.render().el);
			    
				self.partitionCollection.add(partitionModel);
			});

			this.partitionCollection.forEach(function(value, key){
				// find next partition model, then set nextStart timestamp value to reduce complexity in checking next partition when animation is running
				if(key < jsonEventsHeader.eventPartitions.length-1){
					nextPartition = self.partitionCollection.findWhere({part:value.get('part')+1});
					// console.log(nextPartition);console.log(key+1);
					if(!_.isUndefined(nextPartition)){
						value.set({nextPartitionStart: nextPartition.start});
					}
				}
				else{
					value.set({ isLastPartition: true });
				}
			})
			console.log('partition collection');
			// console.log(this.partitionCollection);
			//initialize token animation only for first partition
			var firstPartitionModel = _.first(this.partitionCollection.models);
			self.model.set({ currentPartition: firstPartitionModel });
			self.initializeLocalTimeline(firstPartitionModel, self);
		}
	},

	initializeLocalTimeline: function(partitionModel, self){
		self.$modalEl.modal('show');
		// console.log(self.partitionCollection);
		console.log('initialize new local timeline { part:'+partitionModel.get('part')+', start:'+partitionModel.get('start')+', complete:'+partitionModel.get('complete')+', offset:'+partitionModel.get('offset')+'}');
		console.log(self.model.get('current')+self.model.get('start'));
		
		var url = self.model.get('baseUrl') + partitionModel.get('url');
		var eventCount = 0;
		start = new Date();

		console.log(url);
		$.ajax({
			dataType: 'json',
			async	: true,
			url		: url,
			success	: function(res){
				partitionModel.set({ 
					initialized: false,
					state: 'ST',
					eventJSON: res.listOfEvent
				});
			},
			complete: function(res){
				format = d3.time.format("%Y-%m-%d");
				index = 0;
				eventsProjected = 0;
				eventsNotProjected = 0;
				partitionModel.set({ state: 'IN' });

				// create local timeline and add to master timeline
				var localTimeline = new TimelineLite();
				self.timeline.add(localTimeline, partitionModel.get('offset'));				

				// console.log('new timeline '+partitionModel.get('part')+' { start:'+partitionModel.get('start')+ ' , offset:'+partitionModel.get('offset')+' }');

				partitionModel.get('eventJSON').forEach(function(value, key){
					var arcObj = self.options.arcCollection.findWhere({label: value.source+'|'+value.target}); 

					// only create token model which node source and node target is available in process model. and token which complete time is >= start time
					if( arcObj != undefined && value.complete >= value.start ){

						attributes = [];
						self.model.get('tokenAttributes').forEach(function(val, key){
							if(val.type=='number'){
								//random value between 1-10
								randomValue = Math.floor(Math.random()*(10-1))+1;
								attributes.push({name: val.name, type:'number', value: randomValue });						
							}
							else if(val.type=='array'){
								randomValue = Math.floor(Math.random()*(val.options.length-1));
								attributes.push({name: val.name, type:'string', value: val.options[randomValue] });
							}
						})

						var eventModel = new EventRelationalModel({
							tokenId: 'event-'+partitionModel.get('part')+'-'+key,
							caseId: value.caseId, 
							startTimeline: partitionModel.get('start'),
							start: value.start,
							complete: value.complete,
							arc: arcObj,
							source: value.source,
							target: value.target,
							isShowTokenDefaultColor: self.model.get('isShowTokenDefaultColor'),
							isShowTokenCaseId: self.model.get('isShowTokenCaseId'),
							rendered: false,
							attributes: attributes
						});

						self.eventCollection.add(eventModel);

						tokenView = new TokenView({
							model: eventModel,
							tooltipEl: '#tooltip-wrapper'
						});
						// console.log(eventModel.get('caseId')+'-'+eventModel.get('source')+'-'+eventModel.get('target'));

						
						tokenTween = TweenLite.to('#'+eventModel.get('tokenId'), eventModel.get('duration'));
						tokenTween.eventCallback('onUpdate', tokenView.movingInPath, ['{self}', tokenView]);
						localTimeline.add(tokenTween, eventModel.get('offset'));
						
						
						eventModel.bind('change:state', function(model){
							// binding to token view
							

							// binding to gauge view 
							self.gaugeIndicator.updateIndicator(model, self.model);

							// binding to node source view 
							nodeSourceViewFiltered = _.find(self.options.nodeViews, function(value, key){
								return value.model.get('label') == model.get('source');
							});
							// console.log(model.get('caseId')+'-'+model.get('source')+'-'+model.get('target')+'-'+nodeSourceViewFiltered.model.get('label'));
							nodeSourceViewFiltered.changeTokensCreated(self.model, model, self.eventCollection);

							// binding to node target view 
							nodeTargetViewFiltered = _.find(self.options.nodeViews, function(value, key){
								return value.model.get('label') == model.get('target');
							});
							nodeTargetViewFiltered.changeTokensFinished(self.model, model, self.eventCollection);

							// binding to arc view 
							arcViewFiltered = _.find(self.options.arcViews, function(value, key){
								return value.model.get('nodeSource').get('label') == model.get('source') && value.model.get('nodeTarget').get('label') == model.get('target');
							});
							arcViewFiltered.changeTokensRunning(self.model, model, self.eventCollection);
							// arcViewFiltered.changeTotalDuration(self.model, model, self.eventCollection);
						});
						// partitionModel.get('events').push(eventModel);

						// use date object because if parsing to string using momentjs too slow
						startDate = format(new Date(value.start));
						startDate = new Date(startDate);
						filteredDate = _.find(self.model.get('eventsProjected'), function(value){
							return _.isEqual(value.date, startDate);
						});
						if(_.isObject(filteredDate)){
							filteredDate.value+=1;
						}
						else{
							self.model.get('eventsProjected').push({
								date: startDate,
								value: 1
							});
						}

						eventsProjected++;
					}
					else{
						startDate = format(new Date(value.start));
						startDate = new Date(startDate);
						filteredDate = _.find(self.model.get('eventsNotProjected'), function(value){
							return _.isEqual(value.date, startDate);
						});
						if(_.isObject(filteredDate)){
							filteredDate.value+=1;
						}
						else{
							self.model.get('eventsNotProjected').push({
								date: startDate,
								value: 1
							});
						}
						eventsNotProjected++;
					}
				});
				
				end = new Date();
				duration = moment.duration(end-start);
				loadingTime = duration.hours()+':'+duration.minutes()+':'+duration.seconds();
				
				partitionModel.set({ 
					state: 'RD', 
					loadingTime: loadingTime,
					timeline: localTimeline,
					eventJSON: [],
					eventsProjected: eventsProjected,
					eventsNotProjected: eventsNotProjected,
					eventsProjectedPercent: 100*eventsProjected/(eventsProjected+eventsNotProjected),
					eventsNotProjectedPercent: 100*eventsNotProjected/(eventsProjected+eventsNotProjected),
				});

				// self.model.get('eventsProjected').push(lastEventsProjected);
				// self.model.get('eventsNotProjected').push(lastEventsNotProjected);
				self.model.set({
					eventsProjectedCount : self.model.get('eventsProjectedCount')+eventsProjected,
					eventsNotProjectedCount : self.model.get('eventsNotProjectedCount')+eventsNotProjected
				});

				self.$modalEl.modal('hide');

				console.log('initialize local timeline '+partitionModel.get('part')+' finish in '+loadingTime);
				// console.log(self.model.get('eventsProjected'));
				// console.log(self.model.get('eventsNotProjected'));

			}
		});
	},

	removeLocalTimeline: function(partitionModel, self){
		self.$modalEl.modal('show');
		
		console.log(self.timeline.getChildren());
		console.log(partitionModel.get('events').length);
		
		partitionModel.get('timeline').kill();
		console.log(self.timeline.getChildren());
		
		partitionModel.get('events').models.forEach(function(value, key){
			// console.log(value.get('tokenId'));
			value.set({destroy: true});
		});
		partitionModel.set({ 
			state: 'NA', 
			loadingTime: 0,
			eventsProjected:0,
			eventsNotProjected:0
		});
		self.$modalEl.modal('hide');
	},

	updateSlider: function(tween, self){
		time = tween.time().toString().split(".");
		self.model.set({'current': tween.time()});
		self.$sliderTimelineSlidingEl.slider('value', Number(time[0]));

		// filter current partition model which (start < real time < complete) and not yet initialized
		// filtered = self.partitionCollection.find(function(model){
		//     return self.model.get('real') >= model.get('start') && self.model.get('real') <= model.get('complete') && model.get('state')=='NA';
		// });
		nextPartition = self.partitionCollection.chain()
					.filter(function(model){
					    return (self.model.get('current')+self.model.get('start')) >= model.get('start');
					})
					.max(function(model){
						return model.get('start');
					})
					.value();

		/*if((self.model.get('current')+self.model.get('start')) < self.model.get('currentPartition').get('start')){
			self.timeline.pause();
			currentPartition = self.partitionCollection.findWhere({ part: self.model.get('currentPartition').get('part')-1 });
			self.model.set({ currentPartition: currentPartition });
			if(self.model.get('currentPartition').get('part')-1 >= 0){
				prevPartition = self.partitionCollection.findWhere({part: self.model.get('currentPartition').get('part')-1 });
				if(prevPartition.get('state')=='NA'){
					prevPartition.set({ state: 'DL' });
		 			if(prevPartition.get('state') == 'DL'){
						self.initializeLocalTimeline(prevPartition, self);
					}
				}
			}
			if(self.model.get('currentPartition').get('part')+2 <= self.partitionCollection.length){
				nextNextPartition = self.partitionCollection.findWhere({part: self.model.get('currentPartition').get('part')+2 });
				if(_.isObject(nextNextPartition)){
					if(nextNextPartition.get('state')!='NA'){
						self.removeLocalTimeline(nextNextPartition, self);
					}									
				}
			}
			self.timeline.resume();
		}*/

		if(_.isObject(nextPartition)){			
			self.timeline.pause();
			// to prevent multiple initialization because of timeline slider moving very fast.
			if(nextPartition.get('state')=='NA'){
		 		nextPartition.set({ state: 'DL' });
		 		if(nextPartition.get('state') == 'DL'){
					self.initializeLocalTimeline(nextPartition, self);
					self.model.set({ currentPartition: nextPartition });
		 		}
			}			
			
			// if next partition index n more than 3, then find n-3 partition
			// if n-2 partition state is not "NA" then remove it
			/*if(nextPartition.get('part') >=3){
				prevPrevPartition = self.partitionCollection.findWhere({part: nextPartition.get('part')-3})
				if(_.isObject(prevPrevPartition)){
					if(prevPrevPartition.get('state')!='NA'){
						self.removeLocalTimeline(prevPrevPartition, self);
					}
				}
			}*/
			self.timeline.resume();
		}
	},

	buttonPlayPress: function(){
		switch(this.model.get('state')){
			case 'ready':
			case 'stop':
				this.model.set({ state : 'play' });
				this.timeline.play();
				break;
			case 'pause':
				this.model.set({ state : 'play' });
				this.timeline.resume();
				break;
			case 'play':
			case 'resume':
				this.model.set({ state : 'pause' });
				this.timeline.pause();
				break;
		}
		this.changeButtonColor();
		this.animationMode();
	},

	buttonRepeatPress: function(){
		this.buttonStopPress();

		// reset all state of event collection
		this.eventCollection.forEach(function(model){
			model.set({state: 'stop-initial'});
		});

		this.model.set({ state: 'play'});
		this.timeline.restart();
	},

	buttonStopPress: function(){
		// set all children timeline progress to 0
		this.model.set({ state: 'stop'});
	    var children = this.timeline.getChildren(false, false, true);
	    children.forEach(function(localTimeline, key){
	      localTimeline.progress(0);
	    });
	    this.timeline.progress(0);
	    this.timeline.pause();
	},

	changeButtonColor: function(){
		switch(this.model.get('state')){
			case 'play':
			case 'resume':
			  	var button = d3.select("#button_play").classed('btn-success', true); 
			  	button.select("i").attr('class', "fa fa-pause"); 
			  	d3.select('#button_stop').classed('disabled', false);
  				d3.select('#button_repeat').classed('disabled', false);
			  	break;
			case 'pause':
				var button = d3.select("#button_play").classed('btn-success', false); 
			  	button.select("i").attr('class', "fa fa-play"); 
			  	break;
			case 'stop':
			  	var button = d3.select("#button_play").classed('btn-success', false);
			  	button.select('i').attr('class', "fa fa-play");
			  	break;
		}
	},

	animationMode: function(){
		switch(this.model.get('state')){
			case 'play':
			case 'pause':
				$('.slider_timeline .sliding').slider( "enable" );
				$('.slider_speed .sliding').slider( "enable" );
				d3.selectAll('.edgePath').classed('animation', true);
				d3.selectAll('.edgeLabel ul').classed('hidden', true);
				d3.select('.slide-panel.slide-side').classed('hidden', true);
				break;
			case 'ready':
			case 'stop':
				$('.slider_timeline .sliding').slider( "disable" );
				$('.slider_speed .sliding').slider( "disable" );
				d3.selectAll('.edgePath').classed('animation', false);	
				d3.selectAll('.edgeLabel ul').classed('hidden', false);
				d3.select('.slide-panel.slide-side').classed('hidden', false);
				break;
		}
	}


});