// Filename: view/CircleViewB
var TokenView = Backbone.View.extend({
	
	initialize: function(options){
		this.options = options || {};
		_.bindAll(this, 'render'); // remember: every function that uses 'this
		this.render();
		
		this.$el = $('#'+this.model.get('tokenId'));
		this.tooltipTemplate = Handlebars.compile($('#tooltip-token-template').html());

		this.model.on('change:state', this.showHidden, this);
		this.model.on('change:isShowTokenDefaultColor', this.defaultColor, this);
		this.model.on('change:isShowTokenCaseId', this.showCaseId, this);
		this.model.on('change:destroy', this.destroyView, this);
		
		this.showCaseId();
		this.defaultColor();
		
		this.initializeTooltip();

		this.path = d3.select('#'+this.model.get('arc').cid+' path').node();
	},

	destroyView: function(){
		this.remove();
		this.model.destroy();
	},

	render: function(){
		var self = this;

		this.d3Selector = d3.select('svg g g.events').append('g')
			.attr('id', this.model.get('tokenId'))
			.attr('class', 'walker hidden')
			.classed('case-'+this.model.get('caseId'), true);

		_.where(this.model.get('attributes'), {type:'string'}).forEach(function(val, key){
			self.d3Selector.classed('attr-'+S(val.name).slugify().s+'-'+S(val.value).slugify().s, true);
		});

		this.textD3Selector = this.d3Selector.append('text')
			.attr('class', 'label')
			.text(this.model.get('caseId'))
			.attr('transform', 'translate(10,-10)');
		
		this.tokenD3Selector = this.d3Selector.append('circle')
			.attr('cx',0) 
			.attr('cy',0) 
			.attr('r', 10) 
			.attr('transform', 'translate(0,0)');
	}, 

	initializeTooltip: function(){
		var self = this;
		this.$el.qtip({
			show: "click",
	        hide: "unfocus",
	        style: "qtip-light",
	        position: {
	            my: 'center left',  // Position my top left...
	            at: 'center right', // at the bottom right of...
	        },
	        events:{
	          show: function(event, api){
	          	/* if node clicked, then will add attribute aria-describedby */
	            var theId = $(this).attr('id');
	            d3.select("[aria-describedby='"+theId+"']").classed('show', true);
	          },
	          hide: function(event, api){
	            var theId = $(this).attr('id');
	            d3.select("[aria-describedby='"+theId+"']").classed('show', false);
	          }
	        }, 
	        content:{
	        	title: 'Token Information',
	        	text: function(){
		            data = {
		            	tokenId: self.model.get('tokenId'),
		            	caseId: self.model.get('caseId'),
		            	start: moment(self.model.get('start')*1000).format('ddd, MMM Do YYYY HH:mm:ss'),
		            	complete: moment(self.model.get('complete')*1000).format('ddd, MMM Do YYYY HH:mm:ss'),
		            	nodeSource: self.model.get('arc').get('nodeSource').get('label'),
		            	nodeTarget: self.model.get('arc').get('nodeTarget').get('label')
		            }
		            console.log(data);
		            return self.tooltipTemplate(data);
	        	}
	        }
		});
	},

	defaultColor: function(){
		if(this.model.get('isShowTokenDefaultColor')){
			this.d3Selector.classed('walker-default', true);
		}
		else{
			this.d3Selector.classed('walker-default', false);
		}
	},

	showCaseId: function(){		
		if(this.model.get('isShowTokenCaseId')){
			this.textD3Selector.classed('hidden', false);
		}
		else{
			this.textD3Selector.classed('hidden', true);
		}
	},

	showHidden: function(){
		var self = this;
		if(this.model.get('state')=='stop-initial'){
			this.d3Selector.classed('token-finish hidden', false);
		}
		else if(this.model.get('state')=='finish'){
			this.d3Selector.classed('token-finish', true);
	        // self.d3Selector.classed('hidden', true);
			
	        if(this.model.get('isEnableTokenFadeOut')){
			this.tokenD3Selector.transition()
	            .duration(250)
	            .attr("r",30)
	            .style("opacity", 0);

				setTimeout(function (){
		        	self.tokenD3Selector.attr("r", 10).style("opacity", 1);
		        	self.d3Selector.classed('hidden', true);
			    }, 500);
	        }
	        else{
	        	this.d3Selector.classed('hidden', true);
	        }
		}
		else if(this.model.get('state')=='running'){
			this.d3Selector.classed('token-finish hidden', false);
		}
	},

	movingInPath: function(tween, tokenView){
		offsetLength = tokenView.path.getTotalLength() * tween.progress();
		point = tokenView.path.getPointAtLength(offsetLength);

       tokenView.$el.attr('transform',function(){
          	return 'translate('+[point.x, point.y]+')'; 
        });
		
		
	  	/* if running then set state to "run"
	  	   if finish then set state to "finish" 
	  	   if running then set state to "run" 
	  	 */
	  	if(tween.progress()>0 && tween.progress()<1){
	  		if(tokenView.model.get('state')!='running'){
	  			tokenView.model.set({state:'running' });
	  		}
	  	}
	  	else if(tween.progress()==1){
	  		if(tokenView.model.get('state')!='finish'){
	  			tokenView.model.set({state:'finish' });
	  		}
	  	}
	  	else if(isNaN(tween.progress()) || tween.progress()==0 ){
	  		if(tokenView.model.get('state')!='stop-initial'){
	  			tokenView.model.set({state:'stop-initial' });
	  		}
	  	}
	}
});