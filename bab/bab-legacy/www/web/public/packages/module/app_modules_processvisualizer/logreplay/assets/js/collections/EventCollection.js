// Filename: collections/eventlist
var EventCollection = Backbone.Collection.extend({
    model: EventRelationalModel, 
    countEventFinish: function(){
    	return this.where({ state: 'finish' }).length;
    },
    countEventRunning: function(){
    	return this.where({ state: 'running' }).length;
    },
});