var JustGaugeView = Backbone.View.extend({
	initialize: function(options){
		var self = this;
		this.options = options || {};
		this.render();
	}, 
	render: function(){
		this.gaugeChart = new JustGage({
		    id: this.$el.selector.substring(1),
		    value: this.model.get('min'),
		    min: this.model.get('min'),
		    max: this.model.get('max'),
		    title: this.model.get('title'),
		    label: this.model.get('label'),
		    formatNumber: true,
	  	});
	},

	countTokenFinish: function(evModel, lrModel){
		if(evModel.get('state')=='finish'){
			filtered = this.collection.where({ state: 'finish' });
			this.gaugeChart.refresh(filtered.length);
			// lrModel.set({ numberTokenFinish: filtered.length });
			// this.gaugeChart.refresh(this.collection.countEventFinish());
			// this.gaugeChart.refresh(this.options.filteredCollection.length);
			// filtered = Lazy(this.collection.models).filter(function(val){
			// 	return val.get('state') == 'finish';
			// }).toArray();
			// console.log(filtered);
			// this.gaugeChart.refresh(filtered.length);
		}
		// if(evModel.get('state')=='finish' && evModel.get('prevState')=='running'){
		// 	lrModel.set({ 
		// 		numberTokenFinish: lrModel.get('numberTokenFinish')+1,
		// 		numberTokenRunning: lrModel.get('numberTokenRunning')-1, 
		// 	});
		// }
		// this.gaugeChart.refresh(lrModel.get('numberTokenFinish'));
	},
	
	countTokenRunning: function(evModel, lrModel){
		if(evModel.get('state')=='running'){
			filtered = this.collection.where({ state: 'running' });
			this.gaugeChart.refresh(filtered.length);
			// lrModel.set({ numberTokenRunning: filtered.length });
			// this.gaugeChart.refresh(this.collection.countEventRunning());
			// this.gaugeChart.refresh(this.options.filteredCollection.length);
			// filtered = Lazy(this.collection.models).filter(function(val){
			// 	return val.get('state') == 'running';
			// }).toArray();
			// this.gaugeChart.refresh(filtered.length);
		}
		// if(evModel.get('state')=='running' && evModel.get('prevState')=='finish'){
			// lrModel.set({ 
			// 	numberTokenFinish: lrModel.get('numberTokenFinish')-1,
			// 	numberTokenRunning: lrModel.get('numberTokenRunning')+1,
			// });
		// }
		// else if(evModel.get('state')=='running' && evModel.get('prevState')=='stop-initial'){
			// lrModel.set({ numberTokenRunning: lrModel.get('numberTokenRunning')+1, });
		// }
		// this.gaugeChart.refresh(lrModel.get('numberTokenRunning'));
	},

	showCurrentTimeline: function(args){
		this.gaugeChart.refresh(this.model.get('progress'));
	},
});