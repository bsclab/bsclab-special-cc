var ArcModel = Backbone.RelationalModel.extend({
    defaults: {
        pathLength: 0,
        isFound: false,
        isDifferent: false,
        isClustered: false,
        attribute: {},        
        destroy: false
    },
     /* initialize selector here */
    initialize: function(){
        // add additional attribute if 
        if(this.get('isLogReplay')){
            this.set({
                tokensRunning: 0,
                totalDuration: 0           
            })
        }
    },

    relations:[
        {
            type: Backbone.HasOne,
            key: 'nodeSource',
            relatedModel: 'NodeModel',
        },
        {
            type: Backbone.HasOne,
            key: 'nodeTarget',
            relatedModel: 'NodeModel',
        }, 
    ], 

    getClassRange: function(classRange, number){
        return _.find(classRange, function(value){
            return number >= value.min && number <= value.max;
        });
    },

    setClass: function(processModelType, frequencyRange, dependencyRange){
        var cssClass = undefined ;
        if(this.get('artificial')){
            cssClass = 'edgePath-artificial';
        }
        else{
            switch(processModelType){
                case 'heuristic':
                    var objFreq = this.getClassRange(frequencyRange, this.get('attribute').frequency.absolute);
                    var objDep = this.getClassRange(dependencyRange, this.get('attribute').dependency);
                    cssClass = objFreq.class+' '+objDep.class;
                    break;
                case 'fuzzy':
                    var objFreq = this.getClassRange(frequencyRange, this.get('attribute').frequency.absolute);
                    var objDep = this.getClassRange(dependencyRange, this.get('attribute').significance);
                    cssClass = objFreq.class+' '+objDep.class;
                    break;
                case 'mtga':
                    cssClass = 'color-' + this.get('attribute').color;
                    break;
                case 'delta':
                    cssClass = 'edgePath-delta';
                case 'bayesian':
                case 'timegap':
                    cssClass = ''
                    break;                
            }

            if(this.get('isLogReplay')){
                cssClass += ' edgePath-logreplay';
            }
        }

        if(this.get('isDifferent')){
            cssClass += ' different';
        } 
        this.set({ class: cssClass });
    },

    setLabelData: function(processModelType){
        var data = {
                isLogReplay: this.get('isLogReplay'),
                attributes: []
            }
        if(!this.get('artificial')){
            switch(processModelType){
                case 'fuzzy':
                    data.attributes = [
                            { key: 'frequency', value: numeral(this.get('attribute').frequency.absolute).format('0,0') },
                            { key: 'significance', value: numeral(this.get('attribute').significance).format('0,0.000') }
                        ];
                    break;
                case 'heuristic':
                case 'delta':
                    data.attributes = [
                            { key: 'frequency', value: numeral(this.get('attribute').frequency.absolute).format('0,0') },
                            { key: 'dependency', value: numeral(this.get('attribute').dependency).format('0,0.000') }
                        ];
                    break;
                case 'mtga':
                    data.attributes =  [ 
                            {key:'gap', value: this.get('attribute').gap} 
                        ];
                    break;
                case 'timegap':
                    data = {
                        isLogReplay: this.get('isLogReplay'),
                        attributes: [ 
                            { key: 'meannonsuccessive', value: this.get('attribute').meanNonSuccessive} 
                        ] 
                    }
                    break;
                case 'bayesian':
                default:
                    data = {
                        isLogReplay: this.get('isLogReplay'),
                        attributes: []
                    }
            }
            this.set({ labelData: data })
        }
    }
});