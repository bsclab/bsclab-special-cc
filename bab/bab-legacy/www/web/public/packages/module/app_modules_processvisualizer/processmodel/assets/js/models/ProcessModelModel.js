ProcessModelModel = Backbone.RelationalModel.extend({
	defaults: {
		type: 'heuristic',
		isLogReplay: false,
		showArtificial: true, 
		dagreParam: {
			nodesep: 70,
		    ranksep: 50,
		    rankdir: "TB",
		    marginx: 20,
		    marginy: 20
		}
	},

	initialize: function(){
		//binding if model properties change 
        this.on('change:jsonProcessModel', this.setNodesAndArcs, this);
        this.setNodesAndArcs();
	},

	setNodesAndArcs: function(){
		if(!_.isUndefined(this.get('jsonProcessModel'))){
			this.set({ nodes : this.get('jsonProcessModel').nodes });
			
			switch(this.get('type')){
				case 'timegap':
					this.set({ arcs : this.get('jsonProcessModel').transitions });
					break;
				default: 
					this.set({ arcs : this.get('jsonProcessModel').arcs });
					break;
			}

			this.set({jsonProcessModel: undefined });
		}
	}
})