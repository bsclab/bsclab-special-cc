var ArcView = Backbone.View.extend({    
    initialize: function(options){
        //for passing parameter to a view http://stackoverflow.com/questions/7803138/backbone-js-how-to-pass-parameters-to-a-view
        this.options = options || {};
        _.bindAll(this, 'render');

        //binding if model properties change 
        this.model.on('change:isFound', this.isFound, this);
        this.model.on('change:isShow', this.isShow, this);
        this.model.on('change:destroy', this.remove, this);

        this.tooltipTemplate = Handlebars.compile($('#tooltip-template').html());

        this.$el = $('#'+this.model.cid);
        this.$pathEl = this.$el.find('path');
        this.$labelEl = $('#label-'+this.model.cid);

        this.pathSVG = d3.select('#'+this.model.cid+' path');
        this.elLunar = this.$el[0];
        
        this.updateRender();

        if(this.model.get('isLogReplay')){
            this.initializeIndicator();
        }
    },

    events: {
        'click' : 'setNodeSourceAndDestination'
    },

    setNodeSourceAndDestination: function(){
        console.log('setNodeSourceAndDestination');
        $('#select-node-source').val(this.model.get('nodeSource').get('label'));
        $('#select-node-target').val(this.model.get('nodeTarget').get('label'));
    },

    initializeIndicator: function(){
        this.$indicator1 = this.$labelEl.find(".indicator-1");
        this.$indicator2 = this.$labelEl.find(".indicator-2");
        this.$indicator1.html(this.model.get('tokensRunning'));
        this.$indicator2.html(BabHelper.printDuration(this.model.get('totalDuration')));
        this.model.on('change:tokensRunning', this.showTokensRunning, this);
        this.model.on('change:tokensRunning', this.arcColorChange, this);
        this.model.on('change:totalDuration', this.showTotalDuration, this);
    },

    updateRender: function(){
        var self = this;
        // check if the arc is repeat
        if(this.model.get('nodeSource').get('label') == this.model.get('nodeTarget').get('label')){
            this.nodeSVG = d3.select('#'+this.model.get('nodeSource').cid);
            source = this.getNodeSourcePosition(this.model.get('nodeSource'));
            
            // recWidth = $('#'+this.model.get('nodeSource').cid+' '+this.model.get('nodeSource').get('shape')).attr("width")/1.8;
            arcSetting = {radius:50, degStart:130, degEnd:440};
            this.model.set({ class: this.model.get('class')+ ' edgePath-repeat'});
            // console.log(source); console.log(recWidth);
            this.pathSVG.attr("data-x", source.x);
            // this.pathSVG.attr("d", this.describeArc(source.x-recWidth, source.y, arcSetting.radius, arcSetting.degStart, arcSetting.degEnd));
            this.pathSVG.attr("d", this.describeArc(source.x-this.nodeSVG.node().getBoundingClientRect().width, source.y, arcSetting.radius, arcSetting.degStart, arcSetting.degEnd));
        }

        lunar.addClass(this.elLunar, this.model.get('class'));

        var pos = this.pathSVG.node().getPointAtLength(this.pathSVG.node().getTotalLength()/2);     // find position on 50% of path length
        
        this.$labelEl.parent().attr('transform', "translate ("+pos.x+","+pos.y+")");

        
        this.$pathEl.qtip({
            content: {
                title: '<h5>'+self.model.get('nodeSource').get('label')+' <i class="fa fa-arrow-right"></i> '+self.model.get('nodeTarget').get('label')+'</h5>',
                text: ''
            },
            position: {
                my: 'center left',
                at: 'center right',
                target: self.$labelEl       
            },
            style: {
                classes: 'qtip-dark qtip-shadow arcinfo'
            }
        });

        this.$el.qtip({
            hide: {
                event: "unfocus"
            },
            show:{
                event:'click',
                solo: true
            },
            position: {
                my: 'center right',
                at: 'center left',
                target: self.$labelEl 
            },
            style: {
                classes: 'qtip-light qtip-shadow arcinfo'
            },
            content: {
                title: '<h5>'+self.model.get('nodeSource').get('label')+' <i class="fa fa-arrow-right"></i> '+self.model.get('nodeTarget').get('label')+'</h5>',
                text: function(){
                    obj = { 
                        isLogReplay: false, 
                        data: [],
                        logReplayData: []
                    };

                    if(!self.model.get('artificial')){
                        if(self.model.get('isLogReplay')){
                            obj.isLogReplay = true;
                            obj.logReplayData = [
                                {
                                    label: 'Log Replay',
                                    data: [
                                        { key: 'tokensRunning', label: 'Tokens Running', value : numeral(self.model.get('tokensRunning')).format('0,0')},
                                        { key: 'totalDuration', label: 'Total Duration', value : BabHelper.printDuration(self.model.get('totalDuration'))}
                                    ]
                                }
                            ];
                        }

                        switch(self.model.get('type')){
                            case 'timegap':
                                obj.data = []
                                break;
                            case 'heuristic':
                                obj.data = [
                                    {
                                        label: 'Dependency',
                                        data: [
                                            { key: 'dependency', label: 'Dependency', value: numeral(self.model.get('attribute').dependency).format('0,0.000') }
                                        ]
                                    },
                                    // {
                                    //     label: 'Duration',
                                    //     data: [
                                    //         { key: 'min', label: 'Min', value : self.model.get('attribute').duration.min},
                                    //         { key: 'max', label: 'Max', value : self.model.get('attribute').duration.max},
                                    //         { key: 'median', label: 'Median', value : self.model.get('attribute').duration.median},
                                    //         { key: 'mean', label: 'Mean', value : self.model.get('attribute').duration.mean},
                                    //         { key: 'total', label: 'Total', value : self.model.get('attribute').duration.total}
                                    //     ]
                                        
                                    // },
                                    {
                                        label: 'Frequency',
                                        data: [
                                            { key: 'absolute', label: 'Absolute', value : numeral(self.model.get('attribute').frequency.absolute).format('0,0') },
                                            { key: 'cases', label: 'Cases', value : numeral(self.model.get('attribute').frequency.cases).format('0,0') },
                                            { key: 'maxRepetition', label: 'Max Repetition', value : numeral(self.model.get('attribute').frequency.maxRepetition).format('0,0') },
                                            { key: 'relative', label: 'Relative', value : numeral(self.model.get('attribute').frequency.relative).format('0,0')}
                                        ]
                                    }
                                ]
                                break;
                            case 'fuzzy':
                                obj.data = [
                                    {
                                        label: 'Significance',
                                        data: [
                                            { key: 'significance', label: 'Significance', value: numeral(self.model.get('attribute').significance).format('0,0.000') }
                                        ]
                                    },
                                    // {
                                    //     label: 'Duration',
                                    //     data: [
                                    //         { key: 'min', label: 'Min', value : self.model.get('attribute').duration.min},
                                    //         { key: 'max', label: 'Max', value : self.model.get('attribute').duration.max},
                                    //         { key: 'median', label: 'Median', value : self.model.get('attribute').duration.median},
                                    //         { key: 'mean', label: 'Mean', value : self.model.get('attribute').duration.mean},
                                    //         { key: 'total', label: 'Total', value : self.model.get('attribute').duration.total}
                                    //     ]
                                        
                                    // },
                                    {
                                        label: 'Frequency',
                                        data: [
                                            { key: 'absolute', label: 'Absolute', value : numeral(self.model.get('attribute').frequency.absolute).format('0,0') },
                                            { key: 'cases', label: 'Cases', value : numeral(self.model.get('attribute').frequency.cases).format('0,0') },
                                            { key: 'maxRepetition', label: 'Max Repetition', value : numeral(self.model.get('attribute').frequency.maxRepetition).format('0,0') },
                                            { key: 'relative', label: 'Relative', value : numeral(self.model.get('attribute').frequency.relative).format('0,0')}
                                        ]
                                    }
                                ]
                                break;
                        }

                    }

                    return self.tooltipTemplate(obj);
                }
            }
        })
    },

    getNodeSourcePosition: function(nodeModel){
        // nodeSVG = d3.select('#'+nodeModel.cid);
        var strPos = this.nodeSVG.attr('transform').match(/\(([^)]+)\)/)[1];
        var thePos = strPos.split(",");
        var result = {
            x : parseInt(thePos[0])+ (Math.round(this.nodeSVG.node().getBoundingClientRect().width)/2),
            y : parseInt(thePos[1])
        };

        // if(nodeModel.get('shape')=='circle'){
            // get width of actual svg http://stackoverflow.com/questions/11702952/how-to-get-the-width-of-an-svgg-element
            // result.x -= Math.round(this.nodeSVG.node().getBoundingClientRect().width);
            // console.log(result);
        // }
        
        return result;
    }, 

    describeArc : function(x, y, radius, startAngle, endAngle) {
        var start = this.polarToCartesian(x, y, radius, endAngle);
        var end = this.polarToCartesian(x, y, radius, startAngle);

        var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

        var d = [ "M", start.x, start.y, "A", radius, radius, 0, arcSweep, 0,
                end.x, end.y ].join(" ");
        return d;
    },

    polarToCartesian : function(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

        return {
            x : centerX + (radius * Math.cos(angleInRadians)),
            y : centerY + (radius * Math.sin(angleInRadians))
        };
    },

    isFound: function(){
        if(this.model.get('isFound')){
            lunar.addClass(this.elLunar, 'found');
        }
        else{
            lunar.removeClass(this.elLunar, 'found');
        }
    }, 

    isShow: function(){
        if(this.model.get('isShow')){
            lunar.removeClass(this.elLunar, 'hidden');
        }
        else{
            lunar.addClass(this.elLunar, 'hidden');
        }
    },

    changeTokensRunning: function(logreplayModel, eventModel, eventCollection){
        if(logreplayModel.get('enableArcKPI')){
            filtered = eventCollection.filter(function(value, key){
                return eventModel.get('start') <= logreplayModel.get('real') 
                && eventModel.get('source') == value.get('arc').get('nodeSource').get('label') 
                && eventModel.get('target') == value.get('arc').get('nodeTarget').get('label') 
                && value.get('state')=='running';
            });

            duration = 0;
            filtered.forEach(function(value, key){
                duration+=value.get('duration');
            })
            this.model.set({ 
                tokensRunning: filtered.length,
                totalDuration: duration
            });
        }
        else{
            this.model.set({ 
                tokensRunning: 0,
                totalDuration: 0
            })
        }
    },

    changeTotalDuration: function(logreplayModel, eventModel, eventCollection){
        // calculated only if syncIndicator is true
        // calculated only if token duration is not yet added to arc duration
        if(logreplayModel.get('enableArcKPI')){
            if(!eventModel.get('isDurationCalculated')){
                eventModel.set({ isDurationCalculated: true });
                this.model.set({ totalDuration: this.model.get('totalDuration') + eventModel.get('duration') });
            }
        }
    },

    showTokensRunning: function(){
        this.$indicator1.html(this.model.get('tokensRunning'));
    },

    showTotalDuration: function(){
        this.$indicator2.html(BabHelper.printDuration(this.model.get('totalDuration')));
    },

    arcColorChange: function(){
        if(this.model.get('tokensRunning')==0){
            lunar.removeClass(this.elLunar, 'density-low');
            lunar.removeClass(this.elLunar, 'density-medium');
            lunar.removeClass(this.elLunar, 'density-high');
        }
        else if(this.model.get('tokensRunning')>0 && this.model.get('tokensRunning')<=5){
            lunar.addClass(this.elLunar, 'density-low');
            lunar.removeClass(this.elLunar, 'density-medium');
            lunar.removeClass(this.elLunar, 'density-high');
        }
        else if(this.model.get('tokensRunning')>5 && this.model.get('tokensRunning')<=10){
            lunar.addClass(this.elLunar, 'density-medium');
            lunar.removeClass(this.elLunar, 'density-low');
            lunar.removeClass(this.elLunar, 'density-high');
        }
        else if(this.model.get('tokensRunning')>10){
            lunar.addClass(this.elLunar, 'density-high');
            lunar.removeClass(this.elLunar, 'density-low');
            lunar.removeClass(this.elLunar, 'density-medium');
        }
    }
});