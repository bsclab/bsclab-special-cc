// Filename: view/CircleViewB
var StatisticsAnimationView = Backbone.View.extend({
	el:$('#table-animation tbody'), 
	initialize: function(options){
		this.options = options || {};
		this.render();	
	}, 	
	render: function(){
		console.log(this.collection);
		d3.select('#table-animation tbody .events-all').text(this.collection.length);
		d3.select('#table-animation tbody .events-running').text(this.collection.where({status: "run"}).length);
		d3.select('#table-animation tbody .events-running-percent').text('0%');
		d3.select('#table-animation tbody .animation-completion').text('0%');

		this.chartEventsRunning =  this.createNewChart({
			bindto: '#chart-indicator .events-running .gauge-chart',
			gauge:{
				max: this.model.get('totalEvent'),
				label: {
					format: function(value, ratio){
						return numeral(value).format('0,0');
					}
				}
			}
		});

		this.chartAnimationCompletion = this.createNewChart({
			bindto: '#chart-indicator .animation-completion .gauge-chart',
			gauge: {
				max: 100,
				label:{
					format: function(value, ratio){
						return numeral(ratio).format('0.0%');
					}
				}
			}
		});
	}, 

	createNewChart: function(args){
		var number = math.ceil(args.gauge.max/4);
		var rangeNumber = _.range(number, args.gauge.max+number, number);
		// console.log('rangeNumber');
		// console.log(rangeNumber);
		var chart = c3.generate({
			bindto: args.bindto,
		    data: {
		        columns: [
		            ['data', 0]
		        ],
		        type: 'gauge',
		        onclick: function (d, i) { console.log("onclick", d, i); },
		        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
		        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
		    },
		    gauge: {
		    	max: args.gauge.max,
		    	label: {
		    		format: args.gauge.label.format,
		           	show: true 
		    	},
		    	expand: true
		    },
		    color: {
		        pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
		        threshold: {
		            values: rangeNumber
		        }
		    },
		    size: {
		        height: 200
		    }
		});
		return chart;
	}, 

	showCurrentTimeline: function(args){
		// d3.select('#table-animation tbody .animation-completion').text(numeral(this.model.get('progress')).format('0%'));
		this.chartAnimationCompletion.load({
	        columns: [['data', this.model.get('progress')*100]]
	    });
	},

	showCurrentEvents: function(args){
		var eventRunning = this.model.get('totalEvent')-(this.collection.where({state: 'stop-initial'}).length);
		// console.log(args.collection.where({state:'running'}).length);
		// console.log(this.collection.where({state:'running'}).length);
		// console.log('heyhey table animation show!');
		// d3.select('#table-animation tbody .events-running').text(eventTotal-eventInitial);
		// d3.select('#table-animation tbody .events-running-percent').text(numeral(eventTotal-eventInitial/eventTotal).format('0%'));

		this.chartEventsRunning.load({
	        columns: [['data', eventRunning]]
	    });
	}

});