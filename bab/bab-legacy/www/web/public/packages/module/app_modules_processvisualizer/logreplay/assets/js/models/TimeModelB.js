// Filename: models/EventModel
var TimeModelB = Backbone.Model.extend({
    defaults: {
      state: 'stop-initial',
      progress:0,
      simulation: 0,
      real:0,
      current:0,
      start: 0,
      finish:0,
      timeScaling:100000,
      duration:0,
      totalEvent:0,
      parts:[]	// partition 
    }, 

    initialize: function(){
      this.set({start: this.get('start')});
      this.set({finish: this.get('finish')});
      this.set({duration: this.setDuration()});

      this.bind('change', this.setRealTime);
    },

    setDuration: function(){
      return (this.get('finish')-this.get('start'));
    },  

    setTimeToSecond: function(theTime){
      return theTime;
    }, 

    setTimeToMiliSecond: function(theTime){
      return theTime;
    },

    setRealTime: function(){
      // console.log('set Real Time!')
      var result = (this.get('current') + this.get('start')).toString();
    
      /* set real time and simulation time */
      this.set({'real': result.substr(0,result.length-1)+0});  // to set last increment time always 0
      this.set({'simulation': this.get('current')});

      /* if increment more than maximum value, then set to maximum value */
      if(this.get('simulation') >= this.get('duration')){
        this.set({'simulation' : this.get('duration')});
      }
    }

  });