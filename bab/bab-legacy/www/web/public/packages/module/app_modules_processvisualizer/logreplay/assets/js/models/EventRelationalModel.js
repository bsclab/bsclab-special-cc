// Filename: models/EventModel
var EventRelationalModel = Backbone.RelationalModel.extend({
    defaults: {
      caseId: 0,
      startTimeline:0,
      start: 0,
      complete: 0,
      duration: 0, 
      offset: 0, 
      state: 'stop-initial',
      isShowTokenDefaultColor: false,
      isShowTokenCaseId: true,
      isDurationCalulated: false,
      isEnableTokenFadeOut: true,
      syncIndicator: true,
      projected: false,
      destroy: false
    }, 
     /* initialize selector here */
    initialize: function(){
      this.set({ 
          start: this.get('start')/1000,
          complete: this.get('complete')/1000
      });
      this.set({ duration: this.get('complete')-this.get('start') });
      this.set({ offset: this.get('start')-this.get('startTimeline') });
    },
    relations: [
      {
        type: Backbone.HasOne, 
        key: 'arc', 
        relatedModel: 'ArcModel', 
      },
    ]

  });