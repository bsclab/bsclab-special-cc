// Filename: models/EventModel
var NodeRelationalModelB = Backbone.RelationalModel.extend({
    defaults: {
    	id: 'the-id',
    	selector: '#the-id',
    	d3selector: null,
      	name: 'The Name', 
      	frequency: 0, 
      	dependency: 0
    },

    /* initialize selector here */
    initialize: function(){
    	this.set({ selector: '#'+this.get('id') });
    	this.set({ d3selector: d3.select(this.get('selector')) });
    },
    relations:[
    	{
	    	type: Backbone.HasMany,
			key: 'currentEvents',
			relatedModel: 'EventRelationalModelB',
			collectionType: 'EventCollectionB',
			reverseRelation: {
				key: 'currentNode',
				includeInJSON: 'id'
				// 'relatedModel' is automatically set to 'Zoo'; the 'relationType' to 'HasOne'.
			}
	    }, 
	]
  });