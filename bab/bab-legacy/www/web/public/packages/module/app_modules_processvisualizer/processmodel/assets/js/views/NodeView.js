NodeView = Backbone.View.extend({
    events: {
    }, 

    initialize: function(options){
        //for passing parameter to a view http://stackoverflow.com/questions/7803138/backbone-js-how-to-pass-parameters-to-a-view
        this.options = options || {};
        
        _.bindAll(this, 'render');

        //binding if model properties change 
        this.model.on('change:isFound', this.isFound, this);
        this.model.on('change:isShow', this.isShow, this);
        this.model.on('change:destroy', this.remove, this);

        this.tooltipTemplate = Handlebars.compile($('#tooltip-template').html());

        this.$el = $('#'+this.model.cid);
        this.$shapeEl = this.$el.find(this.model.get('shape'));

        this.elLunar = this.$el[0];
        
        this.updateRender();

        if(this.model.get('isLogReplay')){
            this.initializeIndicator();
        }
    },

    initializeIndicator: function(){
        this.$indicator1 = this.$el.find(".indicator-1");
        this.$indicator2 = this.$el.find(".indicator-2");
        this.$indicator1.html(this.model.get('tokensCreated'));
        this.$indicator2.html(this.model.get('tokensFinished'));
        this.model.on('change:tokensCreated', this.showTokensCreated, this);
        this.model.on('change:tokensFinished', this.showTokensFinished, this);
    },

    updateRender: function(){
        var self = this; 
        lunar.addClass(this.elLunar, this.model.get('class'));

        this.$el.attr('title', this.model.get('label'));
        this.$shapeEl.qtip({
            show:{
                event:'mouseover',
                solo: true
            },
            position: {
                my: 'bottom center',
                at: 'top center'                
            },
            style: {
                classes: 'qtip-green qtip-shadow'
            },
            content: {
                title: '<h5>'+self.model.get('label')+'</h5>',
                text: ''
            },
        });
        
        this.$el.qtip({
            hide: {
                event: "unfocus"
            },
            show:{
                event:'click',
                solo: true
            },
            position: {
                my: 'center left',
                at: 'center right'
            },
            style: {
                classes: 'qtip-light qtip-shadow nodeinfo'
            },
            content: {
                title: '<h5>'+self.model.get('label')+'</h5>',
                text: function(){

                    obj = { 
                        isLogReplay: false, 
                        type: self.model.get('type'),
                        isNode: true,
                        data: [],
                        logReplayData: []
                    };

                    if(!self.model.get('artificial')){
                        if(self.model.get('isLogReplay')){
                            obj.isLogReplay = true;
                            obj.logReplayData = [
                                {
                                    label: 'Log Replay',
                                    data: [
                                        { key: 'tokenscreated', label: 'Tokens Created', value : numeral(self.model.get('tokensCreated')).format('0,0')},
                                        { key: 'tokensfinished', label: 'Tokens Finished', value : numeral(self.model.get('tokensFinished')).format('0,0')}
                                    ]
                                }
                            ];
                        }

                        switch(self.model.get('type')){
                            case 'heuristic':
                            case 'fuzzy':
                                obj.data = [
                                    // {
                                    //     label: 'Duration',
                                    //     data: [
                                    //         { key: 'min', label: 'Min', value : self.model.get('attribute').duration.min},
                                    //         { key: 'max', label: 'Max', value : self.model.get('attribute').duration.max},
                                    //         { key: 'mean', label: 'Mean', value : self.model.get('attribute').duration.mean},
                                    //         { key: 'median', label: 'Median', value : self.model.get('attribute').duration.median},
                                    //         { key: 'total', label: 'Total', value : self.model.get('attribute').duration.total}
                                    //     ]
                                    // },
                                    {
                                        label: 'Frequency',
                                        data: [
                                            { key: 'absolute', label: 'Absolute', value : numeral(self.model.get('attribute').frequency.absolute).format('0,0') },
                                            { key: 'casesAsStartEvent', label: 'Cases as Start Event', value : numeral(self.model.get('attribute').frequency.cases.asStartEvent).format('0,0') },
                                            { key: 'casesAsEndEvent', label: 'Cases as End Event', value : numeral(self.model.get('attribute').frequency.cases.asEndEvent).format('0,0') },
                                            { key: 'casesInclude', label: 'Cases Include', value : numeral(self.model.get('attribute').frequency.cases.include).format('0,0') },
                                            { key: 'maxRepetition', label: 'Max Repetition', value : numeral(self.model.get('attribute').frequency.maxRepetition).format('0,0') },
                                            { key: 'relative', label: 'Relative', value : numeral(self.model.get('attribute').frequency.relative).format('0,0') }
                                        ]
                                    }
                                ]
                                break;
                            case 'bayesian':
                                posterior = self.model.get('attribute').posteriorProbability;
                                data = [];
                                for(var key in posterior){
                                    data.push({ key: key, label: key, value: posterior[key] });
                                }
                                obj.data = [
                                    {
                                        label: 'Posterior Probability',
                                        data: data
                                    }
                                ]
                                break;
                            case 'timegap':
                            default : 
                                break;
                        }                        
                    }

                    return self.tooltipTemplate(obj)
                }
            }
        })

    },

    isFound: function(){
        if(this.model.get('isFound')){
            lunar.addClass(this.elLunar, 'found');
        }
        else{
            lunar.removeClass(this.elLunar, 'found');
        }
    },

    isShow: function(){
        if(this.model.get('isShow')){
            lunar.removeClass(this.elLunar, 'hidden');
        }
        else{
            lunar.addClass(this.elLunar, 'hidden');
        }
    },

    /* show number of tokens generated by a node */
    changeTokensCreated: function(logreplayModel, eventModel, eventCollection){
        // if(logreplayModel.get('syncIndicator')){
            // if(eventModel.get('state') == 'running' && eventModel.previous('state')!='running'){
            //     this.model.set({ tokensCreated: this.model.get('tokensCreated')+1 })
            // }
            // else if(eventModel.get('state') == 'stop-initial' && eventModel.previous('state')!='stop-initial'){
            //     this.model.set({ tokensCreated: this.model.get('tokensCreated')-1 })
            // }
            // console.log('tokensCreated in '+this.model.get('label')+' change! to '+this.model.get('tokensCreated'));
            // if(eventModel.get('state') != 'stop-initial'){
            //     this.model.set({ tokensCreated: this.model.get('tokensCreated')+1 })
            // }
        // }
        if(logreplayModel.get('enableNodeKPI')){
            filtered = eventCollection.filter(function(value, key){
                return eventModel.get('start') <= logreplayModel.get('real') && eventModel.get('source') == value.get('source') && value.get('state')=='stop-initial';
            });
            this.model.set({ tokensCreated: filtered.length })            
        }
    },

    /* show number of tokens finish in a node */
    changeTokensFinished: function(logreplayModel, eventModel, eventCollection){
        // if(logreplayModel.get('syncIndicator')){
            // if(eventModel.get('state') == 'finish' && eventModel.previous('state')=='running'){
            //     this.model.set({ tokensFinished: this.model.get('tokensFinished')+1 })
            // }
            // else if(eventModel.get('state') == 'running' && eventModel.previous('state')=='finish'){
            //     this.model.set({ tokensFinished: this.model.get('tokensFinished')-1 })
            // }
        // }
        if(logreplayModel.get('enableNodeKPI')){
            filtered = eventCollection.filter(function(value, key){
                return eventModel.get('complete') <= logreplayModel.get('real') && eventModel.get('target') == value.get('target') && value.get('state')=='finish';
            });
            this.model.set({ tokensFinished: filtered.length });
        }
    },

    showTokensCreated: function(){
        this.$indicator1.html(this.model.get('tokensCreated'));
    },

    showTokensFinished: function(){
        this.$indicator2.html(this.model.get('tokensFinished'));
    }
}); 