var selectAdditionalAttribute = function(type){
    var additionalAttr = {
        heuristic : {
            name: 'heuristic',
            attribute: 'dependency',
            class: 'edgePath-dep-',
            attributes: {
                node: ['duration', 'frequency'],
                arc: ['dependency', 'duration', 'frequency' ]
            }
        },
        fuzzy : {
            name: 'fuzzy',
            attribute: 'significance',
            class: 'edgePath-dep-',
            attributes: {
                node: ['duration', 'frequency'],
                arc: ['significance', 'duration', 'frequency' ]
            }
        },
        timegap: {
            name: 'timegap',
            attribute: 'meanNonSuccessive',
            class: 'edgePath-dep-', 
            attributes: {
                node: [],
                arc: []
            }
        },
        mtga: {
            name: 'mtga',
            attribute: 'gap',
            class: 'edgePath-dep-', 
            attributes: {
                node: ['caseID', 'dimension', 'eventName', 'timestamp', 'color'],
                arc: ['gap', 'color']
            }
        },
        delta: {
            name: 'delta',
            attribute: 'dependency',
            class: 'edgePath-dep-',
            attributes: {
                node: [],
                arc: []
            }
        },
        bayesian: {
            name: 'bayesian', 
            attribute: 'posteriorProbability', 
            class: 'edgePath-dep-',
            attributes: {
                node: ['posteriorProbability'],
                arc: []
            }
        }
    }
    return additionalAttr[type];            
}

var Model = Backbone.RelationalModel.extend({});

var ArcModel = Backbone.RelationalModel.extend({
    defaults: {
        theId: 'the-id',
        labelId: 'the-id',
        selector: 'the-id',
        labelSelector: 'the-id',
        d3selector: null,
        name: 'The Name',
        pathLength: 0,
        path: null,
        isFound: false,
        isDifferent: false,
        isClustered: false,
        attribute: {}
    },
     /* initialize selector here */
    initialize: function(){
        // this.set({ theId: this.get('theId')+this.cid });
        // this.set({ selector: '#'+this.get('theId') });
        // this.set({ labelId: 'label_'+this.get('theId') });
        // this.set({ labelSelector: '#label_'+this.get('theId') });
        this.set({ path: document.querySelector(this.get('selector')+' path') });
    },

    relations:[
        {
            type: Backbone.HasOne,
            key: 'nodeSource',
            relatedModel: 'NodeModel',
        },
        {
            type: Backbone.HasOne,
            key: 'nodeTarget',
            relatedModel: 'NodeModel',
        }, 
    ], 

    setPath: function(){
        return ;
    } 
});

var NodeModel = Backbone.RelationalModel.extend({
    defaults: {
        theId: 'the-id',
        selector: '#the-id',
        label: 'The Name',
        shape: 'rect',
        isFound: false,
        isDifferent: false,
        isClustered: false,
        attribute: {},
    },

    relations:[
        {
            type: Backbone.HasMany,
            key: 'nextNodes',
            relatedModel: 'NodeModel',
            collectionType: 'NodeCollection',
        },
        {
            type: Backbone.HasMany,
            key: 'prevNodes',
            relatedModel: 'NodeModel',
            collectionType: 'NodeCollection',
        }, 
    ], 

    /* initialize selector here */
    initialize: function(){
        // this.set({ theId: this.get('theId')+this.cid });
        // this.set({ selector: '#'+this.get('theId') });
    },
});

var ArcCollection = Backbone.Collection.extend({
    model: ArcModel
});

var NodeCollection = Backbone.Collection.extend({
    model: NodeModel
});

var NodeView = Backbone.View.extend({
    // tagName : 'g',
    el: 'g.vertex',

    events: {
    }, 

    initialize: function(options){
        //for passing parameter to a view http://stackoverflow.com/questions/7803138/backbone-js-how-to-pass-parameters-to-a-view
        this.options = options || {};
        this.setShape();

        _.bindAll(this, 'render');

        //binding if model properties change 
        this.model.on('change:isFound', this.isFound, this);
        this.model.on('change:isShow', this.isShow, this);
        this.render();
    }, 

    render: function(){
        var _this = this;
        var html = '<ul data-id="'+this.model.get('theId')+'" class="node-label list-unstyled">';
            html += "<li class=status></li>";
            if(this.model.get('type') != 'mtga'){
                html += "<li class=name>"+this.model.get('label')+"</li>";    
            }            
            
            var label ; 

            if(!this.model.get('artificial')){
                switch(this.model.get('type')){
                    case 'mtga':
                        var temp = this.model.get('attribute').dimension.split('|');
                        var string = "";
                        if(temp.length > 1){
                            for( var i = 0; i < temp.length - 1; i++){
                                string = string + temp[i] + "</br>";
                            }
                        }else{
                            string = temp[0] + "</br>";
                        }

                        html += '<li class="name"><h5>'
                                + this.model.get('attribute').caseID
                                + '</h5></li>'
                                +'<li class="node-attr">'                           
                                + this.model.get('attribute').eventName + "</br>" 
                                + string 
                                + this.model.get('attribute').timestamp + "</br>"
                                + '</li>';
                        break;
                    case 'timegap':
                        // do nothing
                        break;
                    case 'bayesian': 
                        var posterior = this.model.get('attribute').posteriorProbability;
                        html+='<li>______</li>';
                        for(var key in posterior){
                            html+='<li><small><em>'+key+':'+posterior[key]+'</em></small></li>';
                        }
                        break;
                    case 'fuzzy':
                        html += "<li class=frequency>"+numeral(this.model.get('attribute').significance).format('0,0.0')+"</li>";
                        break;
                    default:
                        html += "<li class=frequency>"+numeral(this.model.get('attribute').frequency.absolute).format('0,0')+"</li>";
                        break;
                }
            }


            html += '</ul>';

            if(this.model.get('isLogReplay')){
                html+= $('#node-indicator-template').html();
            }
        
        this.options.g.setNode(
            this.model.get('theId'), 
            { 
                id: this.model.get('theId'),
                labelType: "html",
                label: html,
                rx: this.model.get('rx'),
                ry: this.model.get('ry'),
                padding: this.model.get('padding'),
                shape: this.model.get('shape'),
                // class: this.model.get('class')
            }
        );
    },

    setShape: function(){
        if(S(this.model.get('theId')).contains('cluster') || this.model.get('shape')=='circle'){
            this.model.set({
                shape: 'circle',
                rx: 10,
                ry: 10, 
                padding: 10
            });
        }
        else{
            this.model.set({
                shape: 'rect',
                rx: 10,
                ry: 10, 
                padding: 10
            });
        }
    },

    isFound: function(){
        if(this.model.get('isFound')){
            d3.select(this.model.get('selector')).classed('found', true);
        }
        else{
            d3.select(this.model.get('selector')).classed('found', false);
        }
    },

    isShow: function(){
        if(this.model.get('isShow')){
            d3.select(this.model.get('selector')).classed('hidden', false);
        }
        else{
            d3.select(this.model.get('selector')).classed('hidden', true);
        }
    },

    showName: function(){
    
    }
});       

var ArcView = Backbone.View.extend({    
    initialize: function(options){
        //for passing parameter to a view http://stackoverflow.com/questions/7803138/backbone-js-how-to-pass-parameters-to-a-view
        this.options = options || {};
        _.bindAll(this, 'render');
        this.render();

        //binding if model properties change 
        this.model.on('change:isFound', this.isFound, this);
        this.model.on('change:isShow', this.isShow, this);
    }, 

    render: function(){
        var label = '';
        if(!this.model.get('artificial')){
            switch(this.model.get('type')){
                case 'mtga':
                    label = this.model.get('attribute').gap;
                    break;
                case 'timegap':
                    label = this.model.get('attribute').meanNonSuccessive;
                    break;
                case 'bayesian': 
                    label = '';
                    break;
                case 'fuzzy':
                    label = '<ul>';
                    label += '<li>'+numeral(this.model.get('attribute').frequency.absolute).format('0,0')+'</li>';
                    label += '<li>'+numeral(this.model.get('attribute').significance).format('0,0.000')+'</li>';
                    label += '</ul>';
                    break;
                default:
                    label = '<ul>';
                    label += '<li>'+numeral(this.model.get('attribute').frequency.absolute).format('0,0')+'</li>';
                    label += '<li>'+numeral(this.model.get('attribute').dependency).format('0,0.000')+'</li>';
                    label += '</ul>';
                    break;
            }

            if(this.model.get('isLogReplay')){
                label+= $('#arc-indicator-template').html();
            }
        }
        
        this.options.g.setEdge(
            this.model.get('nodeSource').get('theId'),
            this.model.get('nodeTarget').get('theId'), 
            { 
                id: this.model.get('theId'),
                labelType: "html",
                label: '<div>'+label+'</div>', 
                lineInterpolate: 'basis', 
                labelpos: 'c',
                labelId: this.model.get('labelId'),
            }
        );            
    },

    isFound: function(){
        if(this.model.get('isFound')){
            d3.select(this.model.get('selector')).classed('found', true);
        }
        else{
            d3.select(this.model.get('selector')).classed('found', false);
        }
    }, 

    isShow: function(){
        if(this.model.get('isShow')){
            d3.select(this.model.get('selector')).classed('hidden', false);
        }
        else{
            d3.select(this.model.get('selector')).classed('hidden', true);
        }
    },

}); 

var RangeLegendsView = Backbone.View.extend({
    
    initialize: function(){
        this.render();
    },

    render: function(){
        
        d3.select(this.el).append('p').text(this.model.get('title'));

        d3.select(this.el).selectAll('div').data(this.model.get('arrRange').reverse()).enter().append("div")
                .attr("class", function(d) {
                    return d.theClass;
                }).classed({
                    "legends-box" : true
                }).text(function(d) {
                    return numeral(d.max).format('0,0');
                });
    }, 
});

var ProcessModelView = Backbone.View.extend({
        
    initialize: function(){
        
        // initiate arc and node collection
        this.nodeCollection = new NodeCollection();
        this.arcCollection = new ArcCollection();
        this.arcsFrequencyRange = [];
        this.arcsDependencyRange = [];
        this.nodesFrequencyRange = [];
        this.nodeViews = [];
        this.arcViews = [];

        this.d3Selector = d3.select(this.el);
       
        this.initializeDigraph();
        this.initializeModels();
        
        this.loadModelsToDigraph();
        this.renderGraph();

        this.initializeToolbox();

        if(this.nodesFrequencyRange.length>0){
            this.renderRangeLegends();
        }
    },

    events:{
        'click #button-graph-fit-window' : 'graphScaleToFit',
        'click #button-graph-fit-actual' : 'graphScaleToActual',

        'click #button-graph-font-inc' : 'fontIncrease',
        'click #button-graph-font-dec' : 'fontDecrease',
        
        'click #button-graph-arctype-rounded' : 'changeArcTypeTo',
        'click #button-graph-arctype-linear' : 'changeArcTypeTo',
        'click #button-graph-arctype-linearsmooth' : 'changeArcTypeTo',
        'click #button-graph-arctype-rectangular' : 'changeArcTypeTo',
        
        'click #button-graph-arcdirection-tb' : 'changeArcDirectionTo',
        'click #button-graph-arcdirection-lr' : 'changeArcDirectionTo',

        'keyup #node-name' : 'searchNodeName',
        'keyup #node-frequency' : 'searchNodeFrequency',
        'keyup #arc-frequency' : 'searchArcFrequency',
        'keyup #arc-dependency' : 'searchArcDependency',
        'keyup #arc-significance' : 'searchArcSignificance',

        'click #button-graph-showartificial' : 'showArtificial'
    },

    renderRangeLegends: function(){
        var rangeModel = new Model({ 
                title: 'Node Range',
                arrRange: this.nodesFrequencyRange,
            });

        var rangeView = new RangeLegendsView({
            el: '#range-nodes',
            model: rangeModel
        });

        var rangeModel = new Model({ 
                title: 'Arc Range',
                arrRange: this.arcsFrequencyRange,
            });

        var rangeView = new RangeLegendsView({
            el: '#range-arcs',
            model: rangeModel
        });
    },

    initializeTooltip: function(){
        var _this = this;
        $('g.vertex ul').qtip({
            content: {
                text: function(event, api){
                    var id = $(this).attr('data-id');
                    var model = _this.nodeCollection.findWhere({theId:id});
                    return '<h4>'+model.get('label')+'</h4>';
                }
            },
            show:{
                event:'mouseenter'
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            },
            style: {
                classes: 'qtip-green qtip-shadow'
            },                
        });

        $('g.vertex').qtip({
            hide: "unfocus",
            show:{
                event:'click',
                solo: true
            },
            position: {
                my: 'center left',
                at: 'center right'
            },
            style: {
                classes: 'qtip-light qtip-shadow'
            },   
            content: {
                title: function(event, api){
                    var id = $(this).attr('id');
                    var model = _this.nodeCollection.findWhere({theId:id});
                    return '<h5>'+model.get('label')+'</h5>';
                },
                text: function(event, api){
                    // var id = $(this).attr('data-id');
                    var id = $(this).attr('id');
                    var model = _this.nodeCollection.findWhere({theId:id});

                    var node = selectAdditionalAttribute.call(model, model.get('type'));
                    var html = '<table class="table table-hover">';
                    node.attributes.node.forEach(function(value, key){
                        html += '<tr><th class="text-center" colspan="2">'+value+'</th></tr>';
                        var attr = model.get('attribute')[value];

                        var pairs = _.pairs(model.get('attribute')[value]);

                        pairs.forEach(function(value, key){

                            if(_.isObject(value[1])){
                                var pairs2 = _.pairs(value[1]);
                                pairs2.forEach(function(value2, key2){
                                    html += '<tr>'+
                                            '<td>'+value[0] +'-'+value2[0]+'</td>'+
                                            '<td>'+value2[1]+'</td>'+
                                        '</tr>';    
                                }) 
                            }
                            else{
                                html += '<tr>'+
                                            '<td>'+value[0]+'</td>'+
                                            '<td>'+value[1]+'</td>'+
                                        '</tr>';
                            }
                        });
                    });
                    html += '</html>';

                    return html;
                }
            },                             
        });

        $('g.edgePath path').qtip({
            content: {
                text: function(event, api){
                    var id = $(this).attr('data-id');
                    var model = _this.arcCollection.findWhere({theId:id});
                    return '<h4>'+model.get('label')+'</h4>';
                }
            },
            show:{
                event:'mouseenter'
            },
            position: {
                my: 'bottom left',
                at: 'center center'
            },
            style: {
                classes: 'qtip-dark qtip-shadow'
            }
        });

        $('g.edgePath').qtip({
            hide: "unfocus",
            show:{
                event:'click',
                solo: true
            },
            position: {
                my: 'bottom left',
                at: 'bottom center'
            },
            style: {
                classes: 'qtip-light qtip-shadow'
            },
            content:{
                title: function(event, api){
                    var id = $(this).attr('id');
                    var model = _this.arcCollection.findWhere({theId:id});
                    return '<h5>'+model.get('label')+'</h5>';
                },
                text: function(event, api){
                    var id = $(this).attr('id');
                    var model = _this.arcCollection.findWhere({theId:id});
                    var arc = selectAdditionalAttribute.call(model, model.get('type'));
                    
                    var html = '<table class="table table-hover">';
                    arc.attributes.arc.forEach(function(value, key){
                        html += '<tr><th class="text-center" colspan="2">'+value+'</th></tr>';
                        
                        var pairs = _.pairs(model.get('attribute')[value]);
                        if(pairs.length == 0){
                            html += '<tr>'+
                                        '<td>'+value+'</td>'+
                                        '<td>'+model.get('attribute')[value]+'</td>'+
                                    '</tr>';

                        }
                        else{
                            pairs.forEach(function(value, key){
                                if(_.isObject(value[1])){
                                    var pairs2 = _.pairs(value[1]);
                                    pairs2.forEach(function(value2, key2){
                                        html += '<tr>'+
                                                '<td>'+value[0] +'-'+value2[0]+'</td>'+
                                                '<td>'+value2[1]+'</td>'+
                                            '</tr>';    
                                    }) 
                                }
                                else{
                                    html += '<tr>'+
                                                '<td>'+value[0]+'</td>'+
                                                '<td>'+value[1]+'</td>'+
                                            '</tr>';
                                }
                            });                                
                        }
                    });
                    html += '</table>';
                    if(model.get('type') == 'timegap'){
                        html += '<div align="center"><button id="analyze">' +
                                    'Analyze'
                                '</button></div>';    
                    }
                    html += '</html>';
                    
                    return html;
                }
            },
        });
    },

    initializeToolbox: function(){
        
    }, 

    initializeDigraph: function(){
        this.g = new dagreD3.graphlib.Graph()
                    .setGraph({})
                    .setDefaultEdgeLabel(function() { return {}; });

        this.svg = this.d3Selector.append('svg'),
        this.inner = this.svg.append('g');

        // Create the renderer
        this.renderer = new dagreD3.render();
    },

    emptyDigraph: function(){
        var _this = this;
        
        this.g.nodes().forEach(function(value, key) {
            _this.g.removeNode(value);
        });
        
        this.g.edges().forEach(function(value, key) {
            _this.g.removeEdge(value.w, value.w);
        });
        
        if(_.isEmpty(this.nodeViews ==  false)){
            this.nodeViews.forEach(function(view, key){
                _this.nodeViews.pop();
                view.remove();
            })
        }

        if(_.isEmpty(this.arcViews ==  false)){
            this.arcViews.forEach(function(view, key){
                _this.arcViews.pop();
                view.remove();
            })
        }
    },

    initializeModels: function(){
        // remove current collection
        if(_.isEmpty(this.nodeCollection.models) ==  false){
            this.nodeCollection.remove(this.nodeCollection.models);
        }
        if(_.isEmpty(this.arcCollection.models) == false){
            this.arcCollection.remove(this.arcCollection.models);
        }

        var listNodes, listArcs;

        listNodes = this.model.get('jsonProcessModel').nodes;
        
        switch(this.model.get('setting').type){
            case 'timegap':
                listArcs = this.model.get('jsonProcessModel').transitions;
                break;
            default: 
                listArcs = this.model.get('jsonProcessModel').arcs;
                break;
        }

        for (var row in listNodes){
            theNode = listNodes[row];
            
            nodeModel = new NodeModel({
                theId: 'node-'+S(theNode.label).slugify().s,
                selector: this.$el.selector+' #node-'+S(theNode.label).slugify().s,
                label: theNode.label, 
                artificial: false,
                attribute: theNode,
                type: this.model.get('setting').type, 
                isLogReplay: this.model.get('setting').isLogReplay
            });

            if(this.model.get('setting').type === 'delta'){
                if(this.model.get('jsonProcessModelDifferences').nodes[nodeModel.get('label')] ==1){
                    nodeModel.set({ isDifferent: true });   
                }
            }

            this.nodeCollection.push(nodeModel);
        }

        for (var row in listArcs){
            var theArc = listArcs[row];

            arcModel = new ArcModel({
                theId: 'arc-'+S(row).slugify().s, 
                label: row,
                selector: this.$el.selector+' #arc-'+S(row).slugify().s,
                labelId: 'label-arc-'+S(row).slugify().s,
                labelSelector: '#label-arc-'+S(row).slugify().s,
                artificial: false,
                attribute: theArc,
                type: this.model.get('setting').type,
                isLogReplay: this.model.get('setting').isLogReplay               
            });               
            arcModel.set({
                nodeSource: this.nodeCollection.findWhere({theId: 'node-'+S(theArc.source).slugify().s }),
                nodeTarget: this.nodeCollection.findWhere({theId: 'node-'+S(theArc.target).slugify().s }),
            });

            if(this.model.get('setting').type === 'delta'){
                if(this.model.get('jsonProcessModelDifferences').arcs[arcModel.get('label')] ==1){
                    arcModel.set({ isDifferent: true });
                }
            }

            arcModel.get('nodeSource').get('nextNodes').push(arcModel.get('nodeTarget'));
            arcModel.get('nodeTarget').get('prevNodes').push(arcModel.get('nodeSource'));
            // newModel.get('nodeSource').set({ nextNode: nextNode });
            this.arcCollection.push(arcModel);
        }
        
        this.createArtificial();

    },

    loadModelsToDigraph: function(){
        var _this = this;

        this.emptyDigraph();
        var otherAttr = selectAdditionalAttribute.call(this, this.model.get('setting').type);
        var nodeFreqClass = this.makeArray(5, 'vertex-freq-', ''); 
        var arcFreqClass = this.makeArray(5, 'edgePath-freq-', '');
        var arcDepClass = this.makeArray(5, otherAttr.class, '');
        
        //only load to digraph if node collection is not empty
        if(_.isEmpty(this.nodeCollection.models) == false){
            
            switch(this.model.get('setting').type){
                case 'timegap':
                case 'mtga':
                case 'bayesian':
                case 'delta':
                    // do nothing
                    break;
                default:
                    this.nodesFrequencyRange = this.setClassRange(this.nodeCollection.models, 'frequency.absolute', nodeFreqClass);
                    break;
            }
            
            this.nodeCollection.forEach(function(nodeModel, key){
                if(nodeModel.get('artificial')){
                    nodeModel.set({class:'vertex enter vertex-artificial'});
                }
                else{
                    switch(_this.model.get('setting').type){
                        case 'timegap':
                            nodeModel.set({class:'vertex enter vertex-timegap'});
                            break;
                        case 'mtga':
                            nodeModel.set({class:'vertex enter color-'+nodeModel.get('attribute').color});
                            break;
                        case 'bayesian':
                            nodeModel.set({class:'vertex enter vertex-bayesian'});
                            break;
                        case 'delta':
                            nodeModel.set({class:'vertex enter vertex-delta'});
                            break;
						case 'fuzzy':
                            var obj = _this.getClassRange(_this.nodesFrequencyRange, nodeModel.get('attribute').frequency.absolute);
							if (nodeModel.get('label').substring(0, 8) == 'Cluster ') {
								nodeModel.set({class:'vertex vertex-cluster enter '+obj[0].theClass});
							} 
                            else {
								nodeModel.set({class:'vertex enter '+obj[0].theClass});
							}
							break;
                        default: 
                            var obj = _this.getClassRange(_this.nodesFrequencyRange, nodeModel.get('attribute').frequency.absolute);
                            nodeModel.set({class:'vertex enter '+obj[0].theClass});
                            break;
                    }
                }
                if(nodeModel.get('isDifferent')){
                    nodeModel.set({class:nodeModel.get('class')+' found'});
                }                    

                var newView = new NodeView({
                    model: nodeModel, 
                    g: _this.g
                });
                _this.nodeViews.push(newView);
            });
        }            

        //only load to digraph if arc collection is not empty
        if(_.isEmpty(this.arcCollection.models) ==false){

            // var arcsDependencyRange, arcsFrequencyRange; 
            switch(this.model.get('setting').type){
                case 'timegap':
                case 'mtga':
                case 'bayesian':
                    // do nothing
                    break;
                default:
                    this.arcsFrequencyRange = this.setClassRange(this.arcCollection.models, 'frequency.absolute', arcFreqClass);
                    this.arcsDependencyRange = this.setClassRange(this.arcCollection.models, otherAttr.attribute, arcDepClass);
                    break;
            }
            
            this.arcCollection.forEach(function(arcModel, key){
                // set class of each model
                if(arcModel.get('artificial')){
                    arcModel.set({class:'edgePath enter edgePath-artificial'});
                }
                else{
                    switch(_this.model.get('setting').type){                        
                        case 'mtga':
                            arcModel.set({class:'edgePath enter color-' + arcModel.get('attribute').color});
                            break;
                        case 'timegap':
                        case 'bayesian':
                            arcModel.set({class:'edgePath enter '});
                            break;
                        default:
                            var objFreq = _this.getClassRange(_this.arcsFrequencyRange, arcModel.get('attribute').frequency.absolute);
                            var objDep = _this.getClassRange(_this.arcsDependencyRange, arcModel.get('attribute')[otherAttr.attribute]);
                            arcModel.set({class:'edgePath enter '+objFreq[0].theClass+' '+objDep[0].theClass});
                            break;
                    }                        
                }

                if(arcModel.get('isDifferent')){
                    arcModel.set({class:arcModel.get('class')+' found'});
                } 
                
                var newView = new ArcView({
                    model: arcModel, 
                    g: _this.g
                }); 
                _this.arcViews.push(newView);
            });
        }
    },

    deleteAllGraphElement : function() {            
        this.d3Selector.select('svg g.edgePaths').selectAll("*").remove();
        this.d3Selector.select("svg g.edgeLabels").selectAll("*").remove();
        this.d3Selector.select("svg g.nodes").selectAll("*").remove();
        this.d3Selector.select("svg defs").selectAll("*").remove();
    },

    modifyGraph: function(){
        var _this = this;

        if(this.model.get('setting').isLogReplay){
            d3.select('svg g.output').append('g').attr('class', 'events');
        }

        // set the class for node 
        this.nodeCollection.forEach(function(nodeModel, key){
            d3.select(nodeModel.get('selector'))
                .attr('class', nodeModel.get('class'))
                .attr('data-name', nodeModel.get('label'));
        });

        // set the class for arc 
        this.arcCollection.forEach(function(arcModel, key){
            d3.select(arcModel.get('selector')).attr('class', arcModel.get('class'));

            var d3Arc = d3.select(arcModel.get('selector'));
            var d3ArcLabel = d3.select(arcModel.get('labelSelector'));
            d3Arc.select("path").attr("data-id", arcModel.get('theId'));

            if(arcModel.get('nodeSource').get('label') == arcModel.get('nodeTarget').get('label')){
                var d3NodeSource = d3.select(arcModel.get('nodeSource').get('selector'));

                var source = _this.getNodePosition(arcModel.get('nodeSource'));
                var shape = arcModel.get('nodeSource').get('shape');
                var recWidth = d3NodeSource.select(shape).attr("width")/1.8;
                var arcSetting = {radius:50, degStart:130, degEnd:440};
                
                d3ArcLabel.classed({"edgePath-repeat":true});
                d3Arc.classed({"edgePath-repeat":true});
                d3Arc.select("path").attr("data-x", source.x);
                d3Arc.select("path").attr("d", _this.describeArc(source.x-recWidth, source.y, arcSetting.radius, arcSetting.degStart, arcSetting.degEnd));
            }
            
            var thePath = document.querySelector(arcModel.get('selector')+' path');
            var pathLength = thePath.getTotalLength();
            var pos = thePath.getPointAtLength(pathLength/2);
            
            // http://stackoverflow.com/questions/20641953/how-to-select-parent-element-of-current-element-in-d3-js
            var d3ArcLabelParent = d3ArcLabel.select(function() { return this.parentNode; })
            d3ArcLabelParent.attr("transform", function(d,i){
                return "translate ("+pos.x+","+pos.y+")";
            });

        });

        // set the marker smaller 
        d3.selectAll('marker').attr('markerWidth', 4).attr('markerHeight', 2);
    },

    renderGraph: function(){
        this.deleteAllGraphElement();
        
        var _this = this;
        // Set up zoom support
        this.zoom = d3.behavior.zoom().on("zoom", function() {
                _this.inner.attr("transform", "translate(" + d3.event.translate + ")" +
                                          "scale(" + d3.event.scale + ")");
            });
        this.svg.call(this.zoom);

        this.g.setGraph(this.model.get('dagreParam'));

        // Run the renderer. This is what draws the final graph.
        this.renderer(this.inner, this.g);
        this.graphScaleToFit();
        this.modifyGraph();
        this.initializeTooltip();
    }, 

    // Zoom and scale to fit
    graphScaleToFit: function(){
        var isUpdate = true;            
        var zoomScale = this.zoom.scale();
        var graphWidth = this.g.graph().width + 80;
        var graphHeight = this.g.graph().height + 40;
        var width = parseInt(this.svg.style("width").replace(/px/, ""));
        var height = parseInt(this.svg.style("height").replace(/px/, ""));
        zoomScale = Math.min(width / graphWidth, height / graphHeight);
        var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
        this.zoom.translate(translate);
        this.zoom.scale(zoomScale);
        this.zoom.event(isUpdate ? this.svg.transition().duration(500) : d3.select("svg"));
    },

    graphScaleToActual: function(){
        var isUpdate = true;            
        var graphWidth = this.g.graph().width + 80;
        var graphHeight = this.g.graph().height + 40;
        var width = parseInt(this.svg.style("width").replace(/px/, ""));
        var height = parseInt(this.svg.style("height").replace(/px/, ""));
        var zoomScale = 1;
        var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
        this.zoom.translate(translate);
        this.zoom.scale(zoomScale);
        this.zoom.event(isUpdate ? this.svg.transition().duration(500) : d3.select("svg"));
    },

    makeArray : function(num, prefix, sufix) {
        var arr = [];
        for (i = 1; i <= num; i++) {
            arr.push(prefix + i + sufix);
        }
        return arr.reverse();
    },

    /*  make range like this : 
        [ 
          {max: 627, min: 405, theClass: "vertex-freq-1"},
          {max: 848, min: 626, theClass: "vertex-freq-2"},
          {max: 1069, min: 847, theClass: "vertex-freq-3"} 
        ]
     */
    setClassRange : function(models, attribute, arrClass) {
        var step = 0;
        var objMinMax = this.getMinMax(models, attribute);
        
        step = (objMinMax.max - objMinMax.min)/(arrClass.length);
        var arrRange = _.range(objMinMax.min, objMinMax.max, step);
        arrRange.push(objMinMax.max);
        
        var result = [];
        if(arrRange.length > arrClass.length){
            arrClass.forEach(function(value, key){
                result.push({ min: arrRange[key], max: Math.ceil(arrRange[key+1]), theClass: value });
            });                
        }
        else{
            // if the range value less than class range
            result.push({ min: arrRange[0], max: arrRange[0]+0.01, theClass: arrClass[0] });
        }

        return result;
    },

    getMinMax: function(models, attribute){

        // algorithm from this URL http://stackoverflow.com/questions/1086404/string-to-object-in-js 
        var properties = attribute.split('.'); 
        var maxValue = 0,  
            minValue = 0;
        var obj = {};
        
        // get model from collection which have minimum and maximum value
        switch(properties.length){
            case 1: 
                var minModel = _.min(models, function(model){
                        if(!model.get('artificial')){
                            return model.get('attribute')[properties[0]];
                        }
                    });
                var maxModel = _.max(models, function(model){
                        if(!model.get('artificial')){
                            return model.get('attribute')[properties[0]];
                        }
                    });
                minValue = minModel.get('attribute')[properties[0]];
                maxValue = maxModel.get('attribute')[properties[0]];
                break;
            case 2:
                var minModel = _.min(models, function(model){
                        if(!model.get('artificial')){
                            return model.get('attribute')[properties[0]][properties[1]];
                        }
                    });
                var maxModel = _.max(models, function(model){
                        if(!model.get('artificial')){
                            return model.get('attribute')[properties[0]][properties[1]];
                        }
                    });
                minValue = minModel.get('attribute')[properties[0]][properties[1]];
                maxValue = maxModel.get('attribute')[properties[0]][properties[1]];
                break;                    
        }
        return {max : maxValue, min: minValue};
    },

    getClassRange: function(classRange, number){

        var result = _.filter(classRange, function(value){
            return number >= value.min && number <= value.max;
        });
        return result;
    },

    /* change all node shape 
        model = model of this view
    */
    changeNodeShape: function(model){
        for(var row in this.g._nodes){
            var node = this.g._nodes[row];
            node.shape = model.get('nodeShape');
        }
        this.renderGraph();
    },

    changeArcDirectionTo: function(ev){
        var arcDirection = $(ev.currentTarget).data('arcdirection');

        var dagreParam = this.model.get('dagreParam');
        var newParam = { rankdir: arcDirection };
        var combinedParam = _.extend(dagreParam, newParam);


        this.renderGraph();
    },

    /* passing parameter backbone events http://stackoverflow.com/questions/7823556/passing-parameters-into-the-backbone-events-object-of-a-backbone-view */
    changeArcTypeTo: function(ev){
        var arcType = $(ev.currentTarget).data('arctype');
        
        for (var row in this.g._edgeLabels){
            var edge = this.g._edgeLabels[row];
            edge.lineInterpolate = arcType;
        }
        this.renderGraph();
    },

    fontIncrease: function(){
        var elem = ".vertex li", maxFont = 10, minFont = 6;
        var curSize = parseInt($(elem).css('font-size')) - 1;
        if (curSize >= minFont) {
            $(elem).css('font-size', curSize);
            $(elem + ' h5').css('font-size', curSize);
        }
    },

    fontDecrease: function(){
        var elem = ".vertex li", maxFont = 10, minFont = 6;
        curSize = parseInt($(elem).css('font-size')) + 1;
        if (curSize <= maxFont) {
            $(elem).css('font-size', curSize);
            $(elem + ' h5').css('font-size', curSize);
        }
    },

    searchNodeName: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.nodeCollection.models, 'label');
    },

    searchNodeFrequency: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.nodeCollection.models, 'attribute.frequency.absolute');
    },

    searchArcFrequency: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.arcCollection.models, 'attribute.frequency.absolute');
    },

    searchArcDependency: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.arcCollection.models, 'dependency');
    },

    searchArcSignificance: function(ev){
        text = $(ev.currentTarget).val();
        this.filterModelInCollection(text, this.arcCollection.models, 'significance');
    },

    /* function to search element based on search value on sidebar */
    filterModelInCollection: function(text, models, attribute){
        var theText = text.trim().toLowerCase();
        var filtered = [];
        
        // reset status of each model
        models.forEach(function(model, key){
            model.set({isFound: false});
        });
        
        if(theText!=''){
            var attrs = attribute.split('.'); 
            var filteredModels = [], 
                excludeModels = [];

            if(attrs.length == 1){
                filteredModels = _.filter(models, function(model){
                    return String(model.get(attrs[0])).trim().toLowerCase().indexOf(String(theText)) >-1;
                });
            }
            else if(attrs.length == 2){
                filteredModels = _.filter(models, function(model){
                    return String(model.get(attrs[0])[attrs[1]]).trim().toLowerCase().indexOf(String(theText)) >-1;
                });
            }
            else if(attrs.length == 3){
                filteredModels = _.filter(models, function(model){
                    return String(model.get(attrs[0])[attrs[1]][attrs[2]]).trim().toLowerCase().indexOf(String(theText)) >-1;
                });
            }

            filteredModels.forEach(function(model, key){
                model.set({isFound: true});
            });
                
            // for another models which is not found
            excludeModels = _.difference(models, filteredModels);
            excludeModels.forEach(function(model, key){
                model.set({isFound: false});
            });                    
        }     
    },

    getNodePosition: function(nodeModel){
        var strPos = d3.select(nodeModel.get('selector')).attr('transform').match(/\(([^)]+)\)/)[1];
        var thePos = strPos.split(",");
        var result = {
            x : parseInt(thePos[0]),
            y : parseInt(thePos[1])
        };

        if(nodeModel.get('shape')=='circle'){
            // get width of actual svg http://stackoverflow.com/questions/11702952/how-to-get-the-width-of-an-svgg-element
            var svgElem = $(nodeModel.get('selector')+' circle')[0].getBoundingClientRect();
            result.x -= Math.round(svgElem.width);
        }
        
        return result;
    }, 

    describeArc : function(x, y, radius, startAngle, endAngle) {
        var start = this.polarToCartesian(x, y, radius, endAngle);
        var end = this.polarToCartesian(x, y, radius, startAngle);

        var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

        var d = [ "M", start.x, start.y, "A", radius, radius, 0, arcSweep, 0,
                end.x, end.y ].join(" ");

        return d;
    },

    polarToCartesian : function(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

        return {
            x : centerX + (radius * Math.cos(angleInRadians)),
            y : centerY + (radius * Math.sin(angleInRadians))
        };
    },

    showArtificial: function(){
        var self = this; 
        console.log('show artificial');


        this.model.set({ showArtificial : !this.model.get('showArtificial') })

        if(this.model.get('showArtificial')){
            d3.select('#button-graph-showartificial').classed('btn-success', true);
        }
        else{
            d3.select('#button-graph-showartificial').classed('btn-success', false);
        }

        var filteredArcs = this.arcCollection.where({ artificial: true });
        var filteredNodes = this.nodeCollection.where({ artificial: true });

        filteredArcs.forEach(function(value, key){
            value.set({ isShow: self.model.get('showArtificial') });
        });

        filteredNodes.forEach(function(value, key){
            value.set({ isShow: self.model.get('showArtificial') });
        });
    },

    createArtificial: function(){
        var self = this;

        var startNodes = this.nodeCollection.filter(function(val, key){
            return val.get('prevNodes').models.length == 0;
        });

        var endNodes = this.nodeCollection.filter(function(val, key){
            return val.get('nextNodes').models.length == 0;
        });
        
        var repeatedNodes = this.nodeCollection.filter(function(val, key){
            return val.get('prevNodes').models.length ==1 && val.get('nextNodes').models.length ==1 && _.isEqual(val, val.get('nextNodes').models[0]);
        });
        
        startNodes = startNodes.concat(repeatedNodes);
        endNodes = endNodes.concat(repeatedNodes);

        var repeatedNodesStart = this.nodeCollection.filter(function(val, key){
            return val.get('prevNodes').models.length ==1 && val.get('nextNodes').models.length > 1 && _.isEqual(val, val.get('nextNodes').models[0]);
        });

        var repeatedNodesEnd = this.nodeCollection.filter(function(val, key){
            return val.get('prevNodes').models.length > 1 && val.get('nextNodes').models.length == 1 && _.isEqual(val, val.get('nextNodes').models[0]);
        });

        startNodes = startNodes.concat(repeatedNodesStart);
        endNodes = endNodes.concat(repeatedNodesEnd);
        
        var artificialNode = ['Start', 'End'];

        // create artificial node model
        artificialNode.forEach(function(value, key){
            var newModel = new NodeModel({
                theId: 'node-artificial-'+S(value).slugify().s,
                label: value,
                artificial: true, 
                type: 'circle',
                selector: self.$el.selector+' #node-artificial-'+S(value).slugify().s, 
                isShow: true,
            });
            self.nodeCollection.push(newModel);
        });

        // create artificial arc model
        startNodes.forEach(function(value, key){
            var newModel = new ArcModel({
                theId: 'arc-artificial-start-to-'+value.get('theId'),
                artificial: true,
                isShow: true, 
                label: 'Start | '+value.get('label'),
                selector: self.$el.selector+' #arc-artificial-start-to-'+value.get('theId'),
                labelId: 'label arc-artificial-start-to-'+value.get('theId'),
                labelSelector: self.$el.selector+'label arc-artificial-start-to-'+value.get('theId'),
            });               
            newModel.set({
                nodeSource: self.nodeCollection.findWhere({theId: 'node-artificial-start' }),
                nodeTarget: self.nodeCollection.findWhere({theId: value.get('theId') })
            });
            self.arcCollection.push(newModel);
        });

        endNodes.forEach(function(value, key){
            var newModel = new ArcModel({
                theId: 'arc-artificial-'+value.get('theId')+'-to-end',
                artificial: true,
                isShow: true, 
                label: value.get('label')+' | End',
                selector: self.$el.selector+' #arc-artificial-'+value.get('theId')+'-to-end',
                labelId: 'label arc-artificial-'+value.get('theId')+'-to-end',
                labelSelector: self.$el.selector+'label arc-artificial-'+value.get('theId')+'-to-end',
            });               
            newModel.set({
                nodeSource: self.nodeCollection.findWhere({theId: value.get('theId') }),
                nodeTarget: self.nodeCollection.findWhere({theId: 'node-artificial-end' })
            });
            self.arcCollection.push(newModel);
        });
    }
});