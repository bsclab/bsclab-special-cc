Rickshaw.namespace('Rickshaw.Graph.Axis.BabTimeDuration');

Rickshaw.Graph.Axis.BabTimeDuration = function(args) {  
  var self = this;
  this.graph = args.graph;
  this.elements = [];
  this.ticksTreatment = args.ticksTreatment || 'plain';
  this.fixedTimeUnit = args.timeUnit;
  var time = args.timeFixture || new Rickshaw.Fixtures.Time();
  
  this.appropriateTimeUnit = function() {
		var unit;
		var units = time.units;
		var domain = this.graph.x.domain();
		units.forEach( function(u) {
			if (Math.floor(domain[1] / (u.seconds * 1000) ) >= 2) {
				unit = unit || u;
          	// console.log(unit);
			}
		} );
		return (unit || time.units[time.units.length - 1]);
	};
  
	this.tickOffsets = function() {
    /* set unit use new format */
    var unit = this.fixedTimeUnit || this.appropriateTimeUnit();
    unit.formatter = BabHelper.printTimeDuration; console.log(unit);
    
    var domain = this.graph.x.domain();
    var count = Math.floor(domain[1] / (unit.seconds * 1000));   
    var offsets = []; 
    var runningTick = 0; 
     /*var newUnit =  {
            name: 'duration',
            seconds: 3600 * 1000, 
            formatter: printTimeDuration
          };*/
     for (var i = 0; i < count; i++) {
			runningTick = runningTick + (unit.seconds * 1000) ;
			offsets.push( { value: runningTick, unit: unit } );
		}
    
     	// console.log(offsets);
		return offsets;
	};

	this.render = function() {
		this.elements.forEach( function(e) {
			e.parentNode.removeChild(e);
		} );
		this.elements = [];
		var offsets = this.tickOffsets();
		offsets.forEach( function(o) {			
			if (self.graph.x(o.value) > self.graph.x.range()[1]) return;	
			var element = document.createElement('div');
			element.style.left = self.graph.x(o.value) + 'px';
			element.classList.add('x_tick');
			element.classList.add(self.ticksTreatment);
			var title = document.createElement('div');
			title.classList.add('title');
			title.innerHTML = o.unit.formatter(o.value);
			element.appendChild(title);
			self.graph.element.appendChild(element);
			self.elements.push(element);
		} );
	};
	this.graph.onUpdate( function() { self.render() } );
}