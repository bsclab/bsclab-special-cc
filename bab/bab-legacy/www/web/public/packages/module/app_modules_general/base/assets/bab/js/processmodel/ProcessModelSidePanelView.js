var ProcessModelView2 = Backbone.View.extend({
	initialize: function(){
		this.render();
	},

	render: function(){

	}, 

	fontDecrease: function(){
		
	}

});

$(document).ready(function() {
	var elem = ".vertex li", maxFont = 14, minFont = 8;

	$(args.elem.fontDecrease).click(function() {
		curSize = parseInt($(elem).css('font-size')) - 2;
		if (curSize >= minFont) {
			$(elem).css('font-size', curSize);
			$(elem + ' h5').css('font-size', curSize);
		}
	});

	$(args.elem.fontIncrease).click(function() {
		curSize = parseInt($(elem).css('font-size')) + 2;
		if (curSize <= maxFont) {
			$(elem).css('font-size', curSize);
			$(elem + ' h5').css('font-size', curSize);
		}
	});

	/* change arc type */
	d3.select(args.elem.arcType).on("change", function() {
		// logminerProcessModel.renderer = new dagreD3.Renderer();
		logminerProcessModel.renderer.edgeInterpolate($(this).val());
		logminerProcessModel.redrawGraph(true);
	});

	/* change arc direction */
	d3.select(args.elem.arcDirection).on("change", function() {
		// logminerProcessModel.renderer = new dagreD3.Renderer();
		logminerProcessModel.layout.rankDir($(this).val());
		logminerProcessModel.redrawGraph(true);
	});

	/* when user type in textfield to search node name */
	d3.select(args.elem.searchNode).on('keyup', function() {
		setFoundElement({
			textInput : this.value,
			dataAttribute : 'data-name',
			elemCellName : '#node_name_name',
			elemCellCount : '#node_name_count',
			elemChange : 'g.vertex',
		});
	});

	/* when user type in textfield to search node frequency */
	d3.select(args.elem.searchNodeFreq).on('keyup', function() {
		setFoundElement({
			textInput : this.value,
			dataAttribute : 'data-freq',
			elemCellName : '#node_freq_name',
			elemCellCount : '#node_freq_count',
			elemChange : 'g.vertex',
		});
	});

	/* when user type in textfield to search arc frequency */
	d3.select(args.elem.searchArcFreq).on('keyup', function() {
		setFoundElement({
			textInput : this.value,
			dataAttribute : 'data-freq',
			elemCellName : '#arc_freq_name',
			elemCellCount : '#arc_freq_count',
			elemChange : 'g.edgePath',
		});
	});

	/* when user type in textfield to search arc dependency */
	d3.select(args.elem.searchArcDep).on('keyup', function() {
		setFoundElement({
			textInput : this.value,
			dataAttribute : 'data-dep',
			elemCellName : '#arc_dep_name',
			elemCellCount : '#arc_dep_count',
			elemChange : 'g.edgePath',
		});
	});
});

function setFoundElement(args) {
	var theText = args.textInput.trim().toLowerCase();
	var count = 0;

	d3.select(args.elemCellName).text(args.textInput);
	d3.selectAll(args.elemChange).filter(function(d, i) {
		var theString = d3.select(this).attr(args.dataAttribute).toLowerCase();
		count = setClassFoundElement({
			elem : this,
			theString : theString,
			theText : theText,
			count : count
		});
	});

	d3.select(args.elemCellCount).text(count);
}

function setClassFoundElement(args) {
	if (args.theString.indexOf(args.theText) > -1) {
		// console.log('found! : '+d3.select(this).attr('id'));
		d3.select(args.elem).classed('found', true);
		args.count++;
	} else {
		// console.log('not found! : '+d3.select(this).attr('id'));
		d3.select(args.elem).classed('found', false);
	}

	/* if search string is empty */
	if (args.theText === '') {
		// console.log('kosong!');
		args.count = 0;
		d3.select(args.elem).classed('found', false);
	}
	return args.count;
}