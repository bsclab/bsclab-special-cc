/* resource : http://codereview.stackexchange.com/questions/21105/pattern-for-creating-a-globally-accessible-custom-plugin */

(function(bab){

	//expose a function
	bab.printHello = function(theString){
		return 'Hello '+theString;
	};

	// function to locate asset in package
	bab.locateAsset = function(moduleName, asset){
		return 'packages/module/app_modules_'+moduleName+'/assets/'+asset;
	};

	bab.locateCss = function(moduleName, fileName){
		return bab.locateAsset(moduleName, 'css/'+fileName);
	};

	bab.locateJs = function(moduleName, fileName){
		return bab.locateAsset(moduleName, 'js/'+fileName);
	};

    bab.locateCommonRequireJs = function(){
        return bab.locateAsset('general/layout', 'bab/js/common.css');
    };

    // print CSS in client side, http://requirejs.org/docs/faq-advanced.html#css
    bab.loadCssModule = function(moduleName, fileName) {
	    var link = document.createElement("link");
	    link.type = "text/css";
	    link.rel = "stylesheet";
	    link.href = bab.locateCss(moduleName, fileName);
	    document.getElementsByTagName("head")[0].appendChild(link);
	};

	bab.emptyAnArray = function(theArray) {
		while (theArray.length > 0) {
			theArray.pop();
		}
		return theArray;
	};

	bab.printNumber = function(number, decimalPoint) {
		if (jQuery.isNumeric(number)) {
			if (number % 1 != 0) { // if decimal
				if (typeof decimalPoint == 'undefined') {
					decimalPoint = 2
				}
				return Number(number.toFixed(decimalPoint)).toLocaleString();
			} else {
				return Number(number).toLocaleString();
			}
		} else {
			return number;
		}
	};

	bab.printSpecialNumber = function(number, type) {
		var result;
		if (type == 'duration') {
			result = this.printTimeDuration(number);
		} else {
			result = number;
		}

		return result;
	};

	bab.createLoadingSpinner = function(args) {
		var loadingWrapper = d3.select(args.container).append('div').classed(
				'loading-spinner', true);

		if (args.class != undefined) {
			loadingWrapper.classed(args.class, true);
		}

		loadingWrapper.append('i').attr('class', 'fa fa-spinner fa-spin fa-3x');
		loadingWrapper.append('span').text('loading');
	};

	bab.createTable = function(args) {
		console.log('create table');
		console.log(args);
		var table = d3.select(args.elem.container).append('table').attr('id',
				args.elem.table.substring(1)).attr('class', 'display');
		var tbody = table.append('tbody');
		var thead = table.append('thead').append('tr');

		for ( var col in args.columnList) {
			var a = args.columnList[col];
			thead.append('th').text(a.label)
		}

		for ( var row in args.objList) {
			var tr = tbody.append('tr');

			for ( var col in args.columnList) {
				var a = args.columnList[col];
				var b = args.objList[row][a.key];
				// console.log(this.printNumber(b)+', is number =
				// '+jQuery.isNumeric(b));
				tr.append('td').text(this.printNumber(b));
			}
		}
	};

	bab.removeSpecialChar = function(theString) {
		return theString.replace(/[^A-Z0-9]+/ig, "_");
	};

	bab.removeSpecialChar2 = function(theString) {
		theString = theString.replace("(", "-");
		theString = theString.replace(")", "-");
		return theString.replace(/[^A-Z0-9]+/ig, "-");
	};

	bab.setActiveFirst = function(obj, current) {
		var keys = Object.keys(obj);
		if (keys[0] == current)
			return 'active';
	};

	bab.setLocalStorage = function(uri, name) {
		jQuery.getJSON(uri, function(data) {
			localStorage.setItem(name, JSON.stringify(data));
		});
		return true;
	};

	bab.getLocalStorage = function(name) {
		return JSON.parse(localStorage.getItem(name));
	};

	bab.setLocalJSON = function(uri) {
		var myJson = null;
		$.ajax({
			'async' : false,
			'global' : false,
			'url' : uri,
			'dataType' : "json",
			'success' : function(data) {
				myJson = data;
			}
		});
		return myJson;
	};

	bab.getFirstKey = function(data) {
		// for (elem in data ) {
		// return elem;
		// }
		var keys = Object.keys(data);
		return keys[0];
	};

	bab.getLastKey = function(data) {
		var keys = Object.keys(data);
		return keys[keys.length - 1];
	};

	bab.setWaiting = function(elem) {
	};
	
	bab.setWaitingTrack = function(elem, jobTrackingId, jobRedirectUri, postParam) {
		$(elem).modal({
			keyboard : false,
			show : true,
			backdrop : 'static'
		});
		if (postParam == 1) {
			setTimeout("BabHelper.setTrackPost('" + jobTrackingId + "', '" + jobRedirectUri + "', 1)", 2500);
		} else {
			setTimeout("BabHelper.setTrack('" + jobTrackingId + "', '" + jobRedirectUri + "', 1)", 2500);
		}
	};
	
	bab.setTrack = function(jobTrackingId, jobRedirectUri, trial) {
		$.getJSON(jobTrackingId, function( data ) {
			console.log("Tracking Job #" + jobTrackingId + " : " + data.State + " (Trial " + trial + ")");
			trial++;
			var pgb = $('#waiting-modal-progress');
			pgb.attr('aria-valuenow', trial * 10);
			pgb.attr('style', 'width: ' + (trial * 10) + '%;');
			if (data.State == 'FINISHED') {
				$('#waiting-modal-progress').parent().parent().html('This page was finished processing data.<br>If it\'s not refresh automatically, please refresh your page manually (Press F5 or CTRL-R or Reload Button). Thank you.');
				if (jobRedirectUri == '') jobRedirectUri = document.location;
				document.location = jobRedirectUri;
			} else {
				setTimeout("BabHelper.setTrack('" + jobTrackingId + "', '" + jobRedirectUri + "', " + trial + ")", 2500);
			};
		});
	}
	
	bab.setTrackPost = function(jobTrackingId, jobRedirectUri, trial) {
		$.getJSON(jobTrackingId, function( data ) {
			console.log("Tracking Job #" + jobTrackingId + " : " + data.State + " (Trial " + trial + ")");
			trial++;
			var pgb = $('#waiting-modal-progress');
			pgb.attr('aria-valuenow', trial * 10);
			pgb.attr('style', 'width: ' + (trial * 10) + '%;');
			if (data.State == 'FINISHED') {
				$('#waiting-modal-progress').parent().parent().html('This page was finished processing data.<br>If it\'s not refresh automatically, please refresh your page manually (Press F5 or CTRL-R or Reload Button). Thank you.');
				if (jobRedirectUri == '') jobRedirectUri = document.location;
				$('#waiting-modal-form').attr('action', jobRedirectUri);
				$('#waiting-modal-form').submit();
			} else {
				setTimeout("BabHelper.setTrackPost('" + jobTrackingId + "', '" + jobRedirectUri + "', " + trial + ")", 2500);
			};
		});
	}
	
	bab.formatDateResourceId = function(theString) {
		var raw = theString.split('_');
		var name = raw[1].substring(0, raw[1].length - 14);
		var date = raw[1].substring(raw[1].length - 14, raw[1].length);
		var y = date.substring(0, 4);
		var m = date.substring(4, 6);
		var d = date.substring(6, 8);
		var h = date.substring(8, 10);
		var i = date.substring(10, 12);
		var s = date.substring(12, 14);
		date = d + '/' + m + '/' + y + " " + h + ':' + i + ':' + s;
		return {
			resourceId : theString,
			date : date,
			name : name,
			dateName : date + "-" + name
		};
	};

	bab.formatDateRepositoryId = function(theString) {
		var raw = theString.split('_');
		var name = raw[1].substring(0, raw[1].length - 14);
		var date = raw[1].substring(raw[1].length - 14, raw[1].length);
		var y = date.substring(0, 4);
		var m = date.substring(4, 6);
		var d = date.substring(6, 8);
		var h = date.substring(8, 10);
		var i = date.substring(10, 12);
		var s = date.substring(12, 14);
		date = d + '/' + m + '/' + y + " " + h + ':' + i + ':' + s;
		return {
			resourceId : theString,
			workspaceId : raw[0],
			datasetId : raw[1],
			repositoryId : raw[2],
			date : date,
			name : name + " - " + raw[2],
			dateName : date + "-" + name
		};
	};

	bab.isWaitingJsonLoad = function(jsonData){
		return _.has(jsonData, 'returnUri');
	};

	bab.printTimeDuration = function(seconds) {
		var objDate = moment.duration(seconds)._data;
		// console.log(objDate);
		var result = '';
		if (objDate.years != 0) {
			result += ' ' + objDate.years + ' years';
			if (objDate.months != 0) {
				result += ' ' + objDate.months + ' months';
			} else if (objDate.days != 0) {
				result += ' ' + objDate.days + ' days';
			} else if (objDate.hours != 0) {
				result += ' ' + objDate.hours + ' hours';
			} else if (objDate.minutes != 0) {
				result += ' ' + objDate.minutes + ' mins';
			} else if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' secs';
			}
		} else if (objDate.months != 0) {
			result += ' ' + objDate.months + ' months';
			if (objDate.days != 0) {
				result += ' ' + objDate.days + ' days';
			} else if (objDate.hours != 0) {
				result += ' ' + objDate.hours + ' hours';
			} else if (objDate.minutes != 0) {
				result += ' ' + objDate.minutes + ' mins';
			} else if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' secs';
			}
		} else if (objDate.days != 0) {
			result += ' ' + objDate.days + ' days';
			if (objDate.hours != 0) {
				result += ' ' + objDate.hours + ' hours';
			} else if (objDate.minutes != 0) {
				result += ' ' + objDate.minutes + ' mins';
			} else if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' secs';
			}
		} else if (objDate.hours != 0) {
			result += ' ' + objDate.hours + ' hours';
			if (objDate.minutes != 0) {
				result += ' ' + objDate.minutes + ' mins';
			} else if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' secs';
			}
		} else if (objDate.minutes != 0) {
			result += ' ' + objDate.minutes + ' mins';
			if (objDate.seconds != 0) {
				result += ' ' + objDate.seconds + ' secs';
			}
		} else if (objDate.seconds != 0) {
			result += ' ' + objDate.seconds + ' secs';
		} else if (objDate.seconds == 0) {
			result = '0 secs';
		}

		return result;
	};

	bab.printPretty = function(theString, isPretty){
		switch(theString){
			case 'years':
			  	return isPretty?'y':' years';
			case 'months':
			  	return isPretty?'m':' months';
			case 'days':
			  	return isPretty?'d':' days';
			case 'hours':
			  	return isPretty?'h':' hours';
			case 'minutes':
			  	return isPretty?"'":' minutes';
			case 'seconds':
			  	return isPretty?'"':' seconds';
		}
	};

	bab.printTimeDurationPretty = function(seconds) {
	  var isPretty = true;
	  var objDate = moment.duration(seconds)._data;
	  // console.log(objDate);
	  var result = '';
	  if (objDate.years != 0) {
	    result += ' ' + objDate.years + this.printPretty('years', true);
	    if (objDate.months != 0) {
	      result += ' ' + objDate.months + this.printPretty('months', true);
	    } else if (objDate.days != 0) {
	      result += ' ' + objDate.days + this.printPretty('days', true);
	    } else if (objDate.hours != 0) {
	      result += ' ' + objDate.hours + this.printPretty('hours', true);
	    } else if (objDate.minutes != 0) {
	      result += ' ' + objDate.minutes + this.printPretty('minutes', true);
	    } else if (objDate.seconds != 0) {
	      result += ' ' + objDate.seconds + this.printPretty('seconds', true);
	    }
	  } else if (objDate.months != 0) {
	    result += ' ' + objDate.months + this.printPretty('months', true);
	    if (objDate.days != 0) {
	      result += ' ' + objDate.days + this.printPretty('days', true);
	    } else if (objDate.hours != 0) {
	      result += ' ' + objDate.hours + this.printPretty('hours', true);
	    } else if (objDate.minutes != 0) {
	      result += ' ' + objDate.minutes + this.printPretty('minutes', true);
	    } else if (objDate.seconds != 0) {
	      result += ' ' + objDate.seconds + this.printPretty('seconds', true);
	    }
	  } else if (objDate.days != 0) {
	    result += ' ' + objDate.days + this.printPretty('days', true);;
	    if (objDate.hours != 0) {
	      result += ' ' + objDate.hours + this.printPretty('hours', true);
	    } else if (objDate.minutes != 0) {
	      result += ' ' + objDate.minutes + this.printPretty('minutes', true);
	    } else if (objDate.seconds != 0) {
	      result += ' ' + objDate.seconds + this.printPretty('seconds', true);
	    }
	  } else if (objDate.hours != 0) {
	    result += ' ' + objDate.hours + this.printPretty('hours', true);
	    if (objDate.minutes != 0) {
	      result += ' ' + objDate.minutes + this.printPretty('minutes', true);
	    } else if (objDate.seconds != 0) {
	      result += ' ' + objDate.seconds + this.printPretty('seconds', true);
	    }
	  } else if (objDate.minutes != 0) {
	    result += ' ' + objDate.minutes + this.printPretty('minutes', true);
	    if (objDate.seconds != 0) {
	      result += ' ' + objDate.seconds + this.printPretty('seconds', true);
	    }
	  } else if (objDate.seconds != 0) {
	    result += ' ' + objDate.seconds + this.printPretty('seconds', true);
	  } else if (objDate.seconds == 0) {
	    result = '0'+this.printPretty('seconds', true);
	  }

	  return result;
	};

	bab.printDuration = function(seconds){
		duration = moment.duration(seconds);
		return duration.years()+':'+duration.months()+':'+duration.days()+' '+duration.hours()+':'+duration.seconds();
	},

	bab.getFormattedValueByType = function(type, value, stringFormat, legendList){
	    if(type == 'category'){
	        return legendList[value];
	    }
	    else if(type == 'indexed'){
	    	if(_.isUndefined(stringFormat))
	        	return numeral(value).format('0,0');
	        else
	        	return numeral(value).format(stringFormat);
	    }
	    else if(type == 'duration'){
	    	if(_.isUndefined(stringFormat))
	        	return bab.printTimeDuration(value);
	        else if(stringFormat == 'pretty')
	        	return bab.printTimeDurationPretty(value);
	    }
	    else if(type='timeseries'){
	    	if(!_.isUndefined(stringFormat))
            	return moment(value).format(stringFormat);
            else{ 
            	console.log('formatted')
            	return moment(value).format('DD/MM/YYYY');
            }
        }
	};

	/* format function from DYChart http://dygraphs.com/1.1.0/dygraph-combined.js */
	bab.getFormattedDYChart = function(e, a, i){
		if(_.isUndefined(i)){
			// console.log(e);
			if(_.isDate(e)){
				return Dygraph.dateString(e,a());
			}
			else{
				var i=a("sigFigs");

		    	if(null!==i)
		    		return Dygraph.floatFormat(e,i);
		    	var r,
		    		n=a("digitsAfterDecimal"),
		    		o=a("maxNumberWidth"),
		    		s=a("labelsKMB"),
		    		l=a("labelsKMG2");

		    	if(r=0!==e&&(Math.abs(e)>=Math.pow(10,o)||Math.abs(e)<Math.pow(10,-n))?e.toExponential(n):""+Dygraph.round_(e,n),s||l){
		    		var h,
		    			p=[],
		    			g=[];
		    		s&&(h=1e3,p=Dygraph.KMB_LABELS),
		    		l&&(s&&console.warn("Setting both labelsKMB and labelsKMG2. Pick one!"),
		    			h=1024,
		    			p=Dygraph.KMG2_BIG_LABELS,
		    			g=Dygraph.KMG2_SMALL_LABELS);

		    		for(var d=Math.abs(e), u=Dygraph.pow(h,p.length),c=p.length-1;c>=0;c--,u/=h)
		    			if(d>=u){
		    				r=Dygraph.round_(e/u,n)+p[c];
		    				break
		    			}

	    			if(l){
	    				var y=String(e.toExponential()).split("e-");
	    				2===y.length&&y[1]>=3&&y[1]<=24&&(r=y[1]%3>0?Dygraph.round_(y[0]/Dygraph.pow(10,y[1]%3),n):Number(y[0]).toFixed(2),
	    					r+=g[Math.floor(y[1]/3)-1])
	    			}
	    		}	
		    	return r;
			}
		}
		else{
			if(_.isDate(e)){
				var r=i("labelsUTC"),
			        n=r?Dygraph.DateAccessorsUTC:Dygraph.DateAccessorsLocal,
			        o=n.getFullYear(e),
			        s=n.getMonth(e),
			        l=n.getDate(e),
			        h=n.getHours(e),
			        p=n.getMinutes(e),
			        g=n.getSeconds(e),
			        d=n.getSeconds(e);
		
		        if(a>=Dygraph.DECADAL)
		            return""+o;
		        if(a>=Dygraph.MONTHLY)
		            return Dygraph.SHORT_MONTH_NAMES_[s]+"&#160;"+o;
		
		        var u=3600*h+60*p+g+.001*d;
		        return 0===u||a>=Dygraph.DAILY?Dygraph.zeropad(l)+"&#160;"+Dygraph.SHORT_MONTH_NAMES_[s]+"&#160;"+o:Dygraph.hmsString_(h,p,g);
		    }
		    else{
		   		return Dygraph.numberValueFormatter(e,i);
		    }			
		}
	};

	/* http://codepen.io/superpikar/pen/raEEWq?editors=101 */
	bab.drawLineChartDY = function(selector, series, labelSeries, chartParam){
		var self = this;
		console.log(series);
		console.log(chartParam);
	    if(series.length==1){
	    	console.log('single series');
	        var chart = new Dygraph(
	            document.querySelector(selector),
	            series[0],
	            {
	                width: chartParam.width, 
	                height: chartParam.height,
	                xlabel: chartParam.axis.x.label,
	                ylabel: chartParam.axis.y.label,
	                axisLabelFontSize: 12,
	                animatedZooms: true,
	                strokeWidth:1.5,
	                legend: 'always',
	                labels: ['X', chartParam.axis.y.label],
	                labelsSeparateLines : true, 
	                labelsDivStyles: {
	                    'text-align': 'right',
	                    'background': '#fff'
	                },
	                axes: {
	                    x: {
	                        axisLabelWidth : 75,
	                        axisLabelFormatter: function (e,a,i){
	                            if(chartParam.axis.x.type=='duration'){
	                                return self.printTimeDurationPretty(e);
	                            }
	                            else if(chartParam.axis.x.type=='timeseries'){
	                                return self.getFormattedDYChart(e,a,i);                               
	                            }
	                            else{
	                                return self.getFormattedDYChart(e,a,i);
	                            }
	                        },
	                        valueFormatter: function(e,a){
	                            if(chartParam.axis.x.type=='duration'){
	                                return self.printTimeDuration(e);
	                            }
	                            else if(chartParam.axis.x.type=='timeseries'){
	                                return moment(e).format('dddd, MMM Do YYYY, h:mm:ss a');
	                            }
	                            else{
	                                return self.getFormattedDYChart(e,a)
	                            }
	                            // console.log()
	                        }
	                        
	                    }, 
	                    y: {
	                        axisLabelFormatter: function(e,a,i) {
	                            if(chartParam.axis.y.type=='duration'){
	                                return self.printTimeDurationPretty(e);
	                            }
	                            else if(chartParam.axis.y.type=='timeseries'){
	                                return self.getFormattedDYChart(e,a,i);
	                            }
	                            else{
	                                return self.getFormattedDYChart(e,a,i);
	                            }
	                        },
	                        valueFormatter: function(e,a){
	                            if(chartParam.axis.y.type=='duration'){
	                                return self.printTimeDuration(e);
	                            }
	                            else if(chartParam.axis.y.type=='timeseries'){
	                                return self.getFormattedDYChart(e,a);
	                            }
	                            else{
	                                return self.getFormattedDYChart(e,a);
	                            }
	                        }
	                    }
	                }
	            }
	        );

	        console.log(chart);
	    }
	    else{
	        var a =  { "20160000": 1, "20640000": 2, "21360000": 3 };
	        var b =  { "21360000": 2, "21780000": 4, "22380000": 1 };
	        // var series = [a,b];
	        // var labelSeries = ["a", "b"];

	        var result = [];
	        series.forEach(function(value, key){
	           result = _.union(result, _.map(value, function(value2, key2){ 
	              return [parseInt(key2), [] ]
	           }));
	           
	           value = _.map(value, function(value, key){
	              return {x:parseInt(key), y:value};
	           });
	           series[key] = value;
	        });

	        result = _.map(result, function(value, key){ return {x: value[0], y:value[1]} });
	        result = _.sortBy(result, 'key');
	        // console.log(result);
	        // console.log(series);

	        result.forEach(function(value, key){
	           series.forEach(function(value2, key2){
	              filtered = _.findWhere(value2, {x:value.x});
	              // console.log(filtered)
	              if(_.isUndefined(filtered)){
	                 value.y.push(undefined)
	              }
	              else{
	                 value.y.push(filtered.y)
	              }            
	           });
	        });
	        // console.log(result);
	        result = _.map(result, function(value, key){ 
	           return _.flatten([value.x, value.y]);
	        });
	        // console.log(result);

	        var arrLabel = ['X'];
	        labelSeries.forEach(function(value){
	            arrLabel.push(value);
	        });

	        var chart = new Dygraph(
	            document.querySelector(selector),
	            result,
	            {
	                width: document.querySelector(selector).clientWidth,
	                height: 0.75 * document.querySelector(selector).clientWidth,
	                xlabel: chartParam.axis.x.label,
	                ylabel: chartParam.axis.y.label,
	                axisLabelFontSize: 12,
	                animatedZooms: true,
	                strokeWidth:1.5,
	                legend: 'always',
	                labels: arrLabel,
	                labelsSeparateLines : true, 
	                labelsDivStyles: {
	                    'text-align': 'right',
	                    'background': '#fff'
	                },
	                axes: {
	                    x: {
	                        axisLabelWidth : 75,
	                        axisLabelFormatter: function (e,a,i){
	                            if(chartParam.axis.x.type=='duration'){
	                                return this.printTimeDurationPretty(e);
	                            }
	                            else if(chartParam.axis.x.type=='timeseries'){
	                                return this.getFormattedDYChart(e,a,i);                               
	                            }
	                            else{
	                                return this.getFormattedDYChart(e,a,i);
	                            }
	                        },
	                        valueFormatter: function(e,a){
	                            if(chartParam.axis.x.type=='duration'){
	                                return this.printTimeDuration(e);
	                            }
	                            else if(chartParam.axis.x.type=='timeseries'){
	                                return this.getFormattedDYChart(e,a);
	                            }
	                            else{
	                                return this.getFormattedDYChart(e,a)
	                            }
	                        }
	                        
	                    }, 
	                    y: {
	                        axisLabelFormatter: function(e,a,i) {
	                            if(chartParam.axis.y.type=='duration'){
	                                return this.printTimeDurationPretty(e);
	                            }
	                            else if(chartParam.axis.y.type=='timeseries'){
	                                return this.getFormattedDYChart(e,a,i);
	                            }
	                            else{
	                                return this.getFormattedDYChart(e,a,i);
	                            }
	                        },
	                        valueFormatter: function(e,a){
	                            if(chartParam.axis.y.type=='duration'){
	                                return this.printTimeDuration(e);
	                            }
	                            else if(chartParam.axis.y.type=='timeseries'){
	                                return this.getFormattedDYChart(e,a);
	                            }
	                            else{
	                                return this.getFormattedDYChart(e,a);
	                            }
	                        }
	                    }
	                }
	            }
	        );
	    }
	};

	bab.createCustomLegendC3 = function(container, theSeries, chart) {
	    console.log('render custom legend in ' + container);

	    d3.select(container).append('div').attr('class','custom-legend col-md-12')
	                        .append('ul').attr('class','row list-unstyled');

	    d3.select(container + ' .custom-legend ul').selectAll(' li')
	        .data(theSeries).enter()
	        .append('li').attr('class', 'legend-item col-md-3')
	        .html(function(obj) {
	                return '<span style="background-color:' + obj.color + '"></span>' + obj.label;
	        })
	        .on('mouseover', function(obj) {
	            chart.focus(obj.label);
	        })
	        .on('mouseout', function(obj) {
	            chart.revert();
	        })
	        .on('click', function(obj) {
	            chart.toggle(obj.label);
	        });
	};

	bab.changeBarChartColorC3 = function(container, theSeries) {
	    d3.selectAll(container + ' .c3-chart-bars .c3-bar').each(
	        function(value, key) {
	            console.log(value, key);
	            console.log(theSeries);
	            var bar = _.findWhere(theSeries, {index: key});
	            d3.select(this).attr(
	                'style',
	                'fill:' + bar.color + '!important ; opacity:1');
	        });
	};

	bab.drawBarChartC3 = function(selector, series, labelSeries, colorSeries, chartParam){
		var self = this; 

	    if(series.length == 1){
	        /*--------- prepare data part ---------*/
	        var self    = this;
	        var pairs   = _.pairs(series[0]); //console.log(pairs);
	        var arrCat  = [];
	        var xData   = [chartParam.axis.x.label];
	        var yData   = [chartParam.axis.y.label];
	        var columns = [];
	        var theSeries = [];
	        var legendList = [];
	        var maxPieSlice = 6;
	        /* push label to the first element */
	        
	        var xFormat = undefined; 
	        var xCount  = undefined; 
	        var paddingLeft = 0;
	        var yFormat = undefined; 

	        if(chartParam.axis.x.type == 'category'){
	            xCount = undefined; 
	            pairs = _.sortBy(pairs, function(val){
	                return Math.min(val[1])
	            });
	            console.log(pairs);
	            
	            var temp=0;
	            pairs.reverse().forEach(function(value, key){

	                if(key < maxPieSlice || (key==maxPieSlice && pairs.length==maxPieSlice)){
	                    theSeries.push({ index: key, label: value[0], value: value[1], color: colorSeries[key] });
	                    xData.push(value[0]);
	                    yData.push(value[1]);
	                    legendList.push(value[0]);
	                }
	                else if(key == pairs.length-1){  // if last key
	                    theSeries.push({ index: maxPieSlice, label: "Other", value: temp+value[1], color: colorSeries[6] });
	                    xData.push("Other");
	                    yData.push(temp+value[1]);
	                    legendList.push("Other");
	                }
	                else{
	                    temp += value[1];
	                }
	            });            
	        }
	        else{
	            xCount = 5;
	            pairs.forEach(function(value, key){
	                xData.push(value[0]);
	                yData.push(value[1]); 
	            });
	            legendList = [];
	        }
	        
	        /*--------- render chart part ---------*/
	        var chart = c3.generate({
	            bindto: selector,
	            size: {
	                width: chartParam.width, 
	                height: chartParam.height
	            },
	            legend: {
	                show: false,
	                position: 'bottom'
	            },
	            padding: {
	                left: chartParam.axis.y.type=='duration'? 100:75,
	            },
	            axis: {
	                x: {
	                    label: {
	                        text: chartParam.axis.x.label,
	                        position: 'outer-right',
	                    },
	                    tick: {
	                        count: function(){ 
	                            if(chartParam.axis.x.type == 'category')
	                                return undefined;
	                            else
	                                return 5;
	                        },
	                        format: function(value){
	                            return self.getFormattedValueByType(chartParam.axis.x.type, value, undefined, legendList);
	                        }
	                    },
	                    type: chartParam.axis.x.type,
	                },
	                y: {
	                    label: {
	                        text: chartParam.axis.y.label,
	                        position: 'inner-top',
	                    },
	                    tick: {
	                        format: function(value){
	                            return self.getFormattedValueByType(chartParam.axis.y.type, value, undefined, legendList);
	                        }
	                    },
	                }
	            }, 
	            data : {
	                x: chartParam.axis.x.label,
	                columns: [xData, yData],
	                type: 'bar'
	            },
	            bar: {
	                width: {
	                    ratio: 0.5 // this makes bar width 50% of length between ticks
	                }
	            },
	            tooltip: {
	                format:{
	                    data: null,
	                    title: function (value) { 
	                        return self.getFormattedValueByType(chartParam.axis.x.type, value, undefined, legendList);
	                    },
	                    name: function(value, id, ratio){ 
	                        return chartParam.axis.y.label;
	                    },
	                    value: function (value, id, ratio){
	                        return self.getFormattedValueByType(chartParam.axis.y.type, value, undefined, legendList);
	                    } 
	                }
	            }         
	        });

	        /* if categorical then change color of barchart */
	        if (chartParam.axis.x.type == 'category') {
	            self.changeBarChartColorC3(selector, theSeries);
	            self.createCustomLegendC3.call(selector, theSeries, chart);
	        }

	        /* make x label upward */
	        d3.select(selector+' .c3-axis-x-label').attr('y', '-15');
	    }
	};

	bab.drawPieChartC3 = function(selector, series, labelSeries, colorSeries, chartParam){
    	var self = this;

	    if(series.length==1){
	        var theSeries   = [];
	        var theColumns  = [];
	        var theColors   = {};
	        var maxPieSlice = 6;
	        var pairs       = _.pairs(series[0]);
	        console.log(pairs);

	        pairs = _.sortBy(pairs, function(val){
	            return Math.min(val[1])
	        })
	        
	        var temp=0;
	        pairs.reverse().forEach(function(value, key){

	            if(key < maxPieSlice || (key==maxPieSlice && pairs.length==maxPieSlice)){
	                theColumns.push([value[0], value[1]]);
	                theColors[value[0]] = colorSeries[key];
	                theSeries.push({ index: key, label: value[0], value: value[1], color: colorSeries[key] });
	            }
	            else if(key == pairs.length-1){  // if last key
	                theColumns.push(["Other", temp+value[1]]);
	                theColors["Other"] = colorSeries[6];
	                theSeries.push({ index: maxPieSlice, label: "Other", value: temp+value[1], color: colorSeries[6] });
	            }
	            else{
	                temp += value[1];
	            }
	        });
	        console.log(theColumns);

	        var chart = c3.generate({
	            bindto: selector,
	            size: {
	                width: chartParam.width, 
	                height: chartParam.height
	            },
	            data: {
	                type    : 'pie',
	                columns : theColumns,
	                colors  : theColors
	            },
	            legend: {
	                position: 'bottom',
	                show: false
	            },
	            tooltip: {
	                format: {
	                    value: function (value, ratio, id) {
	                        return self.getFormattedValueByType(chartParam.axis.y.type, value);
	                    }
	                }
	            }, 
	            pie:{
	                label: {
	                    format: function (value, ratio, id) {
	                        return numeral(ratio).format('0%');
	                    }
	                }
	            }
	        });

	        self.createCustomLegendC3(selector, theSeries, chart);
	    }
	};

}(this.BabHelper = this.BabHelper || {}));