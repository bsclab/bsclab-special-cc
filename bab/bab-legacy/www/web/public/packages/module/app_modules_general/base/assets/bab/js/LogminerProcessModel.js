function LogminerProcessModel(args) {
	this.type = args.type;
	this.additionalAttr = {
		heuristic : 'dependency',
		fuzzy : 'significance'
	}
	this.objList = args.jsonProcessModel;
	this.g = new dagreD3.Digraph(), this.renderer = args.renderer;
	this.logminerHelper = new LogminerHelper();
	this.container = args.elem.processModel;

	this.setting = {
		size : {
			width : 800,
			height : 600
		},
		type : 'heuristic',
		rankDir : 'TB',
		rankSep : 30,
		objList : {},
		objListCompare : {},
		objListDiff : {}
	}

	_.extend(this.setting, args.setting);
	console.log('the setting');
	console.log(this.setting);

	console.log('elem:' + args.elem.processModel);
	/*
	 * var zoom = d3.behavior.zoom() .scaleExtent([0.5, 2]) .on("zoom",
	 * logminerProcessModel.zoomed);
	 */

	/*
	 * var drag = d3.behavior.drag() .origin(function(d) { return d; })
	 * .on("dragstart", logminerProcessModel.dragstarted) .on("drag",
	 * logminerProcessModel.dragged) .on("dragend",
	 * logminerProcessModel.dragended);
	 */

	this.svg = d3.select(args.elem.processModel).append("svg").attr("width",
			this.setting.size.width).attr("height", this.setting.size.height);

	this.containerProcessModel = this.svg.append("g");
	this.layout = new dagreD3.layout().rankSep(this.setting.rankSep).rankDir(
			this.setting.rankDir);

	this.svg.call(this.redrawGraph.bind(this, false, this.objList));
	this.svg.call(this.zoomed);

}

LogminerProcessModel.prototype = {

	zoomed : function(count) {
		// container.attr("transform", "translate(" + d3.event.translate +
		// ")scale(" + d3.event.scale + ")");
		// this.zoomIndicator("#zoom-indicator span",d3.event.scale );
	},

	zoomIndicator : function(elem, zoomScale) {
		var calculate = zoomScale.toPrecision(2) * 100;
		d3.select(elem).html(calculate);
	},

	dragstarted : function(d) {
		d3.event.sourceEvent.stopPropagation();
		d3.select(this).classed("dragging", true);
	},

	dragged : function(d) {
		d3.select(this).attr("cx", d.x = d3.event.x).attr("cy",
				d.y = d3.event.y);
	},

	dragended : function(d) {
		d3.select(this).classed("dragging", false);
	},

	deleteAllGraph : function() {
		var _this = this;
		_this.g.eachNode(function(u, value) {
			_this.g.delNode(u);
		});
		_this.g.eachEdge(function(u, value) {
			_this.g.delEdge(u);
		});
		d3.select("#content-graph svg g.edgePaths").selectAll("*").remove();
		d3.select("#content-graph svg g.edgeLabels").selectAll("*").remove();
		d3.select("#content-graph svg g.nodes").selectAll("*").remove();
		d3.select("#content-graph svg defs").selectAll("*").remove();
	},

	listAllNode : function(parentElem) {
		var _this = this;
		$(parentElem + " li").remove();
		_this.g.eachNode(function(u, value) {
			$(parentElem).append("<li>" + u + "</li>");
		});
	},

	listAllEdge : function(parentElem) {
		var _this = this;
		$(parentElem + " li").remove();
		_this.g.eachEdge(function(u, value) {
			$(parentElem).append("<li>" + u + "</li>");
		});
	},

	polarToCartesian : function(centerX, centerY, radius, angleInDegrees) {
		var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

		return {
			x : centerX + (radius * Math.cos(angleInRadians)),
			y : centerY + (radius * Math.sin(angleInRadians))
		};
	},

	describeArc : function(x, y, radius, startAngle, endAngle) {
		var _this = this;
		var start = _this.polarToCartesian(x, y, radius, endAngle);
		var end = _this.polarToCartesian(x, y, radius, startAngle);

		var arcSweep = endAngle - startAngle <= 180 ? "0" : "1";

		var d = [ "M", start.x, start.y, "A", radius, radius, 0, arcSweep, 0,
				end.x, end.y ].join(" ");

		return d;
	},

	getNodePosition : function(nodeId) {

		var node = d3.select("#" + nodeId + " g"), strPos = node.attr(
				"transform").match(/\(([^)]+)\)/)[1], thePos = strPos
				.split(",");

		return {
			x : parseInt(thePos[0]),
			y : parseInt(thePos[1])
		};
	},

	drawLinkArc : function(source, target) {
		var dx = target.x - source.x, dy = target.y - source.y, dr = Math
				.sqrt(dx * dx + dy * dy);
		return "M" + source.x + "," + source.y + "A" + dr + "," + dr
				+ " 0 0,1 " + target.x + "," + target.y;
	},

	displayLegendBox : function(elem, objRange) {
		d3.select(elem).selectAll('div').data(objRange).enter().append("div")
				.attr("class", function(d) {
					return d.theClass;
				}).classed({
					"legends-box" : true
				}).text(function(d) {
					return Math.round(d.max);
				});
	},

	makeArray : function(num, prefix, sufix) {
		var arr = [];
		for (i = 1; i <= num; i++) {
			arr.push(prefix + i + sufix);
		}
		return arr;
	},

	setClassRange : function(minValue, maxValue, arrClass) {
		var diff = (maxValue - minValue) / arrClass.length;
		var count = 0;
		var result = [];

		arrClass.reverse().forEach(function(d, i) {
			if (i == 0) {
				count = minValue + diff + 0.001;
				result.push({
					min : minValue,
					max : count,
					theClass : d
				});

			} else if (i == arrClass.length - 1) {
				result.push({
					min : count,
					max : maxValue,
					theClass : d
				});
				count += diff + 0.001;
			} else {
				result.push({
					min : count,
					max : count + diff,
					theClass : d
				});
				count += diff + 0.001;
			}

		});

		return result.reverse();
	},

	/* get Class Range by current value */
	getClassRange : function(val, arrRange) {
		var result;
		arrRange.forEach(function(d, i) {
			if (i != arrRange.length) {
				if (val <= d.max)
					result = d.theClass
			}
		});
		return result;
	},

	redrawArcRepeat : function(arcId, nodeSource, nodeTarget) {
		var elem = d3.select("g#" + arcId);
		// console.log(elem.attr('data-from'));
		// console.log(nodes[links[link]['source']]);

		/* if link is repeated to itself */
		// if(nodeSource == nodeTarget){
		// console.log('ada arc repeat cuy! '+nodeSource+'-'+nodeTarget);
		// var source =
		// this.getNodePosition(logminerHelper.removeSpecialChar(nodeSource));
		// var recWidth =
		// d3.select("#"+logminerHelper.removeSpecialChar(nodeSource)+"
		// rect").attr("width")/1.8;
		// var arc = {radius:40, degStart:130, degEnd:440};
		// elem.classed({"edgePath-repeat":true});
		// elem.select("path").attr("data-x", source.x);
		// elem.select("path").attr("d", this.describeArc(source.x-recWidth,
		// source.y, arc.radius, arc.degStart, arc.degEnd));
		// d3.select("#label_"+logminerHelper.removeSpecialChar(arcId))
		// .attr("transform", function(d,i){
		// return "translate ("+(source.x-recWidth-arc.radius)+","+source.y+")"
		// ;
		// });
		// //return true;
		// }
		// return false;
	},

	// redrawGraph: function(isRedraw, objList){
	redrawGraph : function(isRedraw, objList) {
		var _this = this;
		if (!isRedraw) {
			isRedraw = false;
		}

		// var objList = logminerHelper.getLocalStorage(localStorageName);
		// objList = args.jsonProcessModel;
		console.log('name:' + this.type);
		console.log('objlist:')
		console.log(this.objList);
		var nodes = this.objList.nodes, links = this.objList.arcs, linksRepeat = [];
		// console.log(Object.keys(nodes).length+' node generated');
		// console.log(nodes);
		// console.log(Object.keys(links).length+' arc generated');
		// console.log(links);

		var nodesFrequency = [], arcsFrequency = [], arcsAdditionalAttr = [];

		this.deleteAllGraph();

		/* add node in digraph */
		for ( var node in nodes) {
			if (!this.g.hasNode(node)) {
				this.g
						.addNode(
								node.toString(),
								{
									label : '<ul class="node-label list-unstyled"><li class="node-name"><h5>'
											+ nodes[node]['label']
											+ '</h5></li><li class="node-attr">'
											+ numeral(
													nodes[node]['frequency']['absolute'])
													.format('0,0')
											+ '</li></ul>'
								});
				nodesFrequency.push(nodes[node]['frequency']['absolute']);
			}
			// console.log('the node: ');
			// console.log(nodes[node]);
		}
		console.log('nodes finish add label');
		console.log(nodes);

		for ( var link in links) {
			var id = links[link]['source'] + '_to_' + links[link]['target'];
			var attrName = this.additionalAttr[this.type];
			var otherAttr = this.logminerHelper.printNumber(
					parseFloat(links[link][attrName]), 3);
			if (!this.g.hasEdge(link)) {
				this.g.addEdge(
								link,
								links[link]['source'].toString(),
								links[link]['target'].toString(),
								{
									label : '<ul class="node-label list-unstyled text-center"><li>'
											+ otherAttr
											+ '</li><li>'
											+ links[link]['frequency']['absolute']
											+ '</li></ul>'
								});
				arcsFrequency.push(links[link]['frequency']['absolute']);
				arcsAdditionalAttr.push(otherAttr);
			}
		}

		/* generate frequency and dependency range */
		var nodesFrequencyRange = this.setClassRange(d3.min(nodesFrequency), d3
				.max(nodesFrequency), this.makeArray(5, 'vertex-freq-', ''));
		var arcsFrequencyRange = this.setClassRange(d3.min(arcsFrequency), d3
				.max(arcsFrequency), this.makeArray(5, 'edgePath-freq-', ''));
		var arcsAdditionalAttrRange = this.setClassRange(d3
				.min(arcsAdditionalAttr), d3.max(arcsAdditionalAttr), this
				.makeArray(5, 'edgePath-dep-', '').reverse());

		// console.log(arcsAdditionalAttrRange);
		// console.log(arcsFrequencyRange);
		// console.log(nodesFrequencyRange);
		console.log('nodes finish set range');
		console.log(nodes);
		// set ID of each node
		var oldDrawNodes = this.renderer.drawNodes();
		this.renderer.drawNodes(function(graph, root) {
					var svgNodes = oldDrawNodes(graph, root);

					svgNodes.attr("id", function(u) {
						return logminerHelper.removeSpecialChar(u);
					});
					svgNodes.attr("data-name", function(u) {
						return u;
					});
					svgNodes.attr('class', function() {
						var name = $(this).attr('data-name');
						var difference = '';

						if (_this.setting.objListDiff.nodeDifferences != undefined) {
							difference = _this.setting.objListDiff.nodeDifferences[name] == 1 ? 'vertex-difference'
									: '';
						}

						console.log('node name:' + name+ ' the difference is '+ difference);

						var nodeFreq = nodes[name].frequency.absolute;
						if (name.indexOf('Cluster') > -1 || name.indexOf('cluster') > -1) {
							return "vertex enter vertex-cluster "+ _this.getClassRange(nodeFreq,nodesFrequencyRange);
						} 
						else {
							return "vertex enter "+ _this.getClassRange(nodeFreq,nodesFrequencyRange)+ " " + difference;
						}
					});
					/* set node frequency */
					svgNodes.attr("data-freq",function() {
						return nodes[$(this).attr('data-name')].frequency.absolute;
					});
					return svgNodes;
				});

		// set ID of each edge path
		var count = 1;
		var oldDrawEdgePaths = this.renderer.drawEdgePaths();
		this.renderer
				.drawEdgePaths(function(graph, root) {

					var svgEdgePaths = oldDrawEdgePaths(graph, root);
					var arcId, nodeSource, nodeTarget;

					svgEdgePaths.attr("id", function(u) {
						arcId = logminerHelper.removeSpecialChar(u);
						// console.log(count++ +'-'+arcId);
						return arcId;
					});
					svgEdgePaths.attr("data-name", function(u) {
						return u;
					});
					svgEdgePaths.attr('class',function() {
						var name = $(this).attr('data-name');
						var arcFreq = links[$(this).attr(
								'data-name')]['frequency']['absolute'];
						var arcDep = links[$(this).attr(
								'data-name')]['dependency'];
						var difference = '';

						if (_this.setting.objListDiff.arcDifferences != undefined) {
							difference = _this.setting.objListDiff.arcDifferences[name] == 1 ? 'edgePath-difference'
									: '';
						}
						console.log('arc name:' + name
								+ ' the difference is '
								+ difference);

						return "edgePath enter "
								+ _this.getClassRange(arcFreq,
										arcsFrequencyRange)
								+ " "
								+ _this
										.getClassRange(arcDep,
												arcsAdditionalAttrRange)
								+ ' ' + difference;
					});
					svgEdgePaths.attr('data-from', function() {
						nodeSource = links[$(this).attr('data-name')].source;
						return nodeSource;
					});
					svgEdgePaths.attr('data-to', function() {
						nodeTarget = links[$(this).attr('data-name')].target;
						return nodeTarget;
					});
					svgEdgePaths.attr('data-freq',function() {
						return links[$(this).attr('data-name')].frequency.absolute;
					});
					svgEdgePaths.attr('data-dep', function() {
						return links[$(this).attr('data-name')].dependency;
					});
					// console.log(arcId+'-'+nodeSource+'-'+nodeTarget)

					return svgEdgePaths;
				});

		// set ID of each edge label
		var oldDrawEdgeLabels = this.renderer.drawEdgeLabels();
		this.renderer.drawEdgeLabels(function(graph, root) {
			var svgEdgeLabels = oldDrawEdgeLabels(graph, root);
			svgEdgeLabels.attr("id", function(u) {
				return "label_" + logminerHelper.removeSpecialChar(u);
			});
			return svgEdgeLabels;
		});

		/* check if link is repeated to itself */
		// for(var link in links){
		// var elem = d3.select("g#"+logminerHelper.removeSpecialChar(link));
		// console.log(link);
		// if(links[link]['source'] == links[link]['target']){
		// var source = this.getNodePosition(elem.attr("data-from"));
		// var recWidth =
		// d3.select("#"+logminerHelper.removeSpecialChar(elem.attr("data-from"))+"
		// rect").attr("width")/1.8;
		// var arc = {radius:40, degStart:130, degEnd:440};
		// elem.classed({"edgePath-repeat":true});
		// elem.select("path").attr("data-x", source.x);
		// elem.select("path").attr("d", this.describeArc(source.x-recWidth,
		// source.y, arc.radius, arc.degStart, arc.degEnd));
		// d3.select("#label_"+logminerHelper.removeSpecialChar(name))
		// .attr("transform", function(d,i){
		// return "translate ("+(source.x-recWidth-arc.radius)+","+source.y+")"
		// ;
		// });
		// }
		// }
		// console.log('renderer');
		// console.log(this.renderer);
		console.log('container: ' + this.container);
		this.renderer.layout(this.layout).run(this.g,
				d3.select(this.container + " svg g"));

		if (d3.event) {
			d3.event.preventDefault();
			d3.event.stopPropagation();
		}

		/* show qtip for node */
		$("g.vertex")
				.qtip(
						{
							show : "click",
							hide : "unfocus",
							style : "qtip-light",
							position : {
								my : 'center left', // Position my top left...
								at : 'center right', // at the bottom right
														// of...
							},
							events : {
								show : function(event, api) {
									/*
									 * if node clicked, then will add attribute
									 * aria-describedby
									 */
									var theId = $(this).attr('id');
									d3.select(
											"[aria-describedby='" + theId
													+ "']").classed('show',
											true);
								},
								hide : function(event, api) {
									var theId = $(this).attr('id');
									d3.select(
											"[aria-describedby='" + theId
													+ "']").classed('show',
											false);
								}
							},
							content : {
								title : function(event, api) {
									return $(this).attr('data-name');
								},
								text : function(event, api) {
									var elem = d3.select(".tooltip-wrapper"), elemFreq = elem
											.select('.frequency'), elemPerform = elem
											.select('.performance');

									var arrFreq = [ 'absolute', 'case',
											'maxRepetition', 'relative' ];
									var arrPerform = [ 'total', 'median',
											'mean', 'max', 'min' ];
									var arrNodes = nodes[$(this).attr(
											'data-name')];

									/* display frequency information */
									elemFreq.selectAll('tr').remove();
									elemFreq.append('tr').attr('class',
											'bg-info').append('th').attr(
											'class', 'text-center').attr(
											'colspan', 2).text('Frequency');
									arrFreq
											.forEach(function(value, key) {
												elemFreq
														.append('tr')
														.append('td')
														.text(
																value
																		+ ' frequency')
														.append('td')
														.text(
																arrNodes['frequency'][value]);
											});

									/* display duration information */
									elemPerform.selectAll('tr').remove();
									elemPerform.append('tr').attr('class',
											'bg-warning').append('th').attr(
											'class', 'bg-success text-center')
											.attr('colspan', 2).text(
													'Performance');
									arrPerform
											.forEach(function(value, key) {
												var value2;
												if (value == 'min'
														|| value == 'max') {
													value2 = Math
															.floor((Math
																	.random() * 100) + 1);
												} else {
													value2 = arrNodes['duration'][value];
												}
												// elemPerform.append('tr').append('td').text(value+'
												// Duration').append('td').text(value2);
												elemPerform
														.append('tr')
														.append('td')
														.text(
																value
																		+ ' frequency')
														.append('td').text(
																'00:00:00');
											});

									return elem.html();
								}
							}
						});

		/* show qtip for edge */
		$("g.edgePath")
				.qtip(
						{
							show : "click",
							hide : "unfocus",
							style : "qtip-light",
							position : {
								my : 'center left', // Position my top left...
								at : 'center right', // at the bottom right
														// of...
							},
							events : {
								show : function(event, api) {
									/*
									 * if node clicked, then will add attribute
									 * aria-describedby
									 */
									var theId = $(this).attr('id');
									d3.select(
											"[aria-describedby='" + theId
													+ "']").classed('show',
											true);
								},
								hide : function(event, api) {
									var theId = $(this).attr('id');
									d3.select(
											"[aria-describedby='" + theId
													+ "']").classed('show',
											false);
								}
							},
							content : {
								title : function(event, api) {
									var arrNodes = links[$(this).attr(
											'data-name')];
									return arrNodes['source'] + ' to '
											+ arrNodes['target'];
								},
								text : function(event, api) {
									var elem = d3.select(".tooltip-wrapper"), elemFreq = elem
											.select('.frequency'), elemPerform = elem
											.select('.performance');

									var arrFreq = [ 'absolute', 'cases',
											'maxRepetition', 'relative' ];
									var arrPerform = [ 'total', 'median',
											'mean', 'max', 'min' ];
									var arrNodes = links[$(this).attr(
											'data-name')];
									elemFreq.selectAll('tr').remove();
									elemFreq.append('tr').append('th').attr(
											'class', 'bg-info text-center')
											.attr('colspan', 2).text(
													'Frequency');
									arrFreq
											.forEach(function(value, key) {
												elemFreq
														.append('tr')
														.append('td')
														.text(
																value
																		+ ' frequency')
														.append('td')
														.text(
																arrNodes['frequency'][value]);
											});

									elemPerform.selectAll('tr').remove();
									elemPerform.append('tr').append('th').attr(
											'class', 'bg-success text-center')
											.attr('colspan', 2).text(
													'Performance');
									arrPerform
											.forEach(function(value, key) {
												var value2;
												if (value == 'min'
														|| value == 'max') {
													value2 = Math
															.floor((Math
																	.random() * 100) + 1);
												} else {
													value2 = arrNodes['duration'][value];
												}
												// elemPerform.append('tr').append('td').text(value+'
												// Duration').append('td').text(value2);
												elemPerform
														.append('tr')
														.append('td')
														.text(
																value
																		+ ' frequency')
														.append('td').text(
																'00:00:00');
											});

									return elem.html();
								}
							}
						});
		this.displayLegendBox("#legend-vertex", nodesFrequencyRange);
		this.displayLegendBox("#legend-edgepath", arcsFrequencyRange);
	}

}
