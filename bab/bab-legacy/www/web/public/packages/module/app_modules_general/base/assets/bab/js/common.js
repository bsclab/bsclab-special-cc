
//The build will inline common dependencies into this file.
//For any third party dependencies, like jQuery, place them in the lib folder.
//Configure loading modules from the lib directory,
//except for 'app' ones, which are in a sibling
//directory.

var lib = 'app_modules_general/layout/assets/lib/js/';
var bab = 'app_modules_general/layout/assets/bab/js/'

requirejs.config({
	baseUrl: 'packages/module',	
	urlArgs: "bust=" +  (new Date()).getTime(), // disable chaching for requirejs http://stackoverflow.com/questions/8315088/prevent-requirejs-from-caching-required-scripts
	paths: {

		/* path for each module */
		js_layout_bab		: 'app_modules_general/layout/assets/bab/js',
		js_dashboard		: 'app_modules_dashboard/dashboard/assets/js',
		js_processmodel		: 'app_modules_processvisualizer/processmodel/assets/js',

		babhelper			: bab+'helpers',
		
		require 			: lib+'require/require',
		jquery 				: lib+'jquery/jquery.min',
		underscore			: lib+'underscore/underscore.min',
		backbone 			: lib+'backbone/backbone.min',
		backbonerelational 	: lib+'backbone/backbone-relational',
		d3 					: lib+'d3/d3.v3.min',
		dagreD3 			: lib+'d3/dagre-d3',
		stringjs			: lib+'stringjs/string.min',
	}, 
	shim: {
		backbone: {
			deps: ['jquery', 'underscore'],
			exports: 'Backbone'
		}, 
		underscore: {
			exports: '_'
		}
	}
});