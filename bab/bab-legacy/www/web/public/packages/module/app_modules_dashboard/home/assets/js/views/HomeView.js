var HomeView = Backbone.View.extend({
		initialize: function (options) {
			this.options = options || {};  

			this.recentLogCollection = new FileListCollection();
			this.datasetCollection = new FileListCollection();

			this.template = Handlebars.compile($('#file-list-template').html());
			this.$recentloglist = this.$el.find('#panel-recentlog');
		    this.$datasetlist = this.$el.find('#panel-dataset');
		    this.render();
		},
		events: {
			'change #file-upload': 'enableSubmit',
			'keyup #recentlog-search': 'searchRecentLog',
			'keyup #dataset-search': 'searchDataset'
		},
		enableSubmit: function () {
			$('#btn-submit').removeClass('disabled');
		},
		render: function () {
			var self = this;
			
			this.model.get('recentLog').repositories.forEach(function(value, key){
				var jsonURL = self.model.get('jsonURL')+value.repositoryId;
				$.getJSON( jsonURL, function(data){
					theData = BabHelper.formatDateRepositoryId(value.repositoryId);
					
					_.extend(theData, {
						url : self.model.get('dashboardURL')+theData.resourceId,
						noOfCases : numeral(data.noOfCases).format('0,0'),
						noOfEvents : numeral(data.noOfEvents).format('0,0'),
						isLogRepositories : true
					});

					file = new FileListModel(theData);
					fileListView = new FileListView({
						id: 'recentlog-'+theData.resourceId,
						tagName : 'li',
						model: file
					});
					self.recentLogCollection.push(file);

			        self.$recentloglist.find('ul').append(fileListView.render().el);
			        self.$recentloglist.find('.panel-heading small span').html(self.recentLogCollection.length);
				});
			});
			
			this.model.get('dataset').repositories.forEach(function(value, key){
				theData = BabHelper.formatDateRepositoryId(value.repositoryId);

				_.extend(theData, {
					url : self.model.get('datasetURL')+theData.resourceId			
				});

				file = new FileListModel(theData);
				fileListView = new FileListView({
					id: 'dataset-'+theData.resourceId,
					tagName : 'li',
					model: file
				});
				self.datasetCollection.push(file);

		        self.$datasetlist.find('ul').append(fileListView.render().el);
		        self.$datasetlist.find('.panel-heading small span').html(self.datasetCollection.length)
			});
		},
		searchRecentLog: function (ev) {
			filtered = this.recentLogCollection.filter(function(v,k){
		    	return v.get('resourceId').trim().toLowerCase().indexOf(ev.currentTarget.value.trim().toLowerCase()) > -1 ;
		    }); 
		    excluded = _.difference(this.recentLogCollection.models, filtered);
		    
		    filtered.forEach(function(v,k){
		    	v.set({ isShow:true });  
		    });
		    excluded.forEach(function(v,k){
		    	v.set({ isShow:false });  
		    })
			
			this.$recentloglist.find('.panel-search span').html(filtered.length+' log');
		},
		searchDataset: function (ev) {
			filtered = this.datasetCollection.filter(function(v,k){
		    	return v.get('resourceId').trim().toLowerCase().indexOf(ev.currentTarget.value.trim().toLowerCase()) > -1 ;
		    }); 
		    excluded = _.difference(this.datasetCollection.models, filtered);
		    filtered.forEach(function(v,k){
		    	v.set({ isShow:true });  
		    });
		    excluded.forEach(function(v,k){
		    	v.set({ isShow:false });  
		    })
		    this.$datasetlist.find('.panel-search span').html(filtered.length+' dataset');
		}
	});