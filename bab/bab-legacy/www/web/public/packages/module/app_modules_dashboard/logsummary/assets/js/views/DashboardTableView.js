var DashboardTableView = Backbone.View.extend({
	initialize : function(options) {
		this.options = options || {};
		this.template = Handlebars.compile($('#table-row-template').html()),
		// this.renderContent();
		this.model.on('change:rendered', this.render, this);
	},

	render : function() {
		var self = this;
		var html = '';

		var data = this.model.get('data');
		for( var row in data ){
			var dataForRow = {columns: []};
			this.model.get('columns').forEach(function(value, key){
				dataForRow.columns.push({ value: data[row][value.key] });
			});
			html+= this.template(dataForRow);
		}
		this.$el.html(html);

		this.$el.parent().dataTable({
			"paging" : true,
			"filter" : true,
			"sort" : true,
			"info" : false,
			"autoWidth" : false
		});
	},

});