// based on this http://jsfiddle.net/zeck/ar6t3/
var Model = Backbone.Model.extend({
	defaults: {
		rendered: false,
	}
});

var Collection = Backbone.Collection.extend({
	model : Model
});

var DashboardTabView = Backbone.View.extend({
	initialize : function(options) {
		this.options = options || {};
		this.tableCollection = new Collection();
		this.chartCollection = new Collection();
		this.$label     = this.$el.find("ul");
		this.$content   = this.$el.find("div");
		this.labelTemplate   = Handlebars.compile($(this.options.labelTemplateId).html());
		this.contentTemplate = Handlebars.compile($(this.options.contentTemplateId).html());
		this.render();
		this.afterRender();
	},

	events : {
		'click a.tab-enabler': "clickTab", 
		'click a.pill-enabler': "clickPill", 
	},

	clickTab: function(ev){
		
		var table = this.tableCollection.findWhere({ id: 'table-'+ev.currentTarget.hash.substring(1) });
		var firstChart = _.findWhere(this.model.get('tabs'), { id: ev.currentTarget.hash.substring(1) }).charts[0];
		var chart = this.chartCollection.findWhere({ id: firstChart.id });
		
		//if not yet rendered
		if(!table.get('rendered')){
			table.set({rendered: true});
		}
		if(!chart.get('rendered')){
			chart.set({rendered: true});
		}


	},

	clickPill: function(ev){
		
		var chart = this.chartCollection.findWhere({ id: ev.currentTarget.hash.substring(1) });
		// console.log(chart);
		//if not yet rendered
		if(!chart.get('rendered')){
			chart.set({rendered: true});
		}
	},

	render : function() {
		var self = this;
		var labelHtml = '';
		var contentHtml = '';
		// console.log(this.model);
		this.model.get('tabs').forEach(function(tab, key){
			tab.active = key==0? "active" : "";

			tab.charts.forEach(function(chart, key2){
				chart.active = key2==0? "active" : "";

				chart.renderTabs = chart.type=='piechart'?true:false;
				
				var values = _.values(chart.series[0]);
				if(chart.chartParam.axis.y.type == 'duration'){
					chart.statistics = [
						{ label: 'Min', value: BabHelper.printTimeDuration(math.min(values)) },
						{ label: 'Mean', value: BabHelper.printTimeDuration(math.mean(values)) },
						{ label: 'Max', value: BabHelper.printTimeDuration(math.max(values)) },
						{ label: 'Std Deviation', value: BabHelper.printTimeDuration(math.std(values)) },
					];
				}
				else{
					chart.statistics = [
						{ label: 'Min', value: numeral(math.min(values)).format('0,0') },
						{ label: 'Mean', value: numeral(math.mean(values)).format('0,0') },
						{ label: 'Max', value: numeral(math.max(values)).format('0,0') },
						{ label: 'Std Deviation', value: numeral(math.std(values)).format('0,0') },
					];					
				}
			});
			
			// render each label and content html template, fill the data using tab
			labelHtml += self.labelTemplate(tab).trim();
			contentHtml += self.contentTemplate(tab).trim();

		})
		this.$label.html(labelHtml);
		this.$content.html(contentHtml);

		/* render handlebars partial */
		// this.$content.find('.howtouse-button').html(Handlebars.compile($('#howtouse-button').html()));
	},

	afterRender: function(){
		var self = this;

		this.model.get('tabs').forEach(function(tab, key){
			var isFirst = key==0?true:false;

			var tableModel = new Model(tab.table);
			self.tableCollection.add(tableModel);

			var tableView = new DashboardTableView({
				el: '#'+tab.table.id+' tbody',
				model: tableModel,
			});


			var colors = [];

			tab.charts.forEach(function(chart, key2){
				var isFirstChart = key2==0?true:false;

				chart.chartParam.width = 0.9 * $('#chart-overview-eventpercase-chart')[0].clientWidth;
				chart.chartParam.height = 0.45 * $('#chart-overview-eventpercase-chart')[0].clientWidth;

				if(_.isEmpty(colors)){
					switch(chart.chartParam.axis.x.type){
						case 'category':
							colors = randomColor({ count: _.size(chart.series[0]) });
							break;
						default: 
							colors = randomColor({ hue: 'green', count: chart.series.length });
							break;						
					}					
				}
				chart.colorSeries = colors;

				var chartModel = new Model(chart);
				self.chartCollection.add(chartModel);

				var chartView = new DashboardChartView({
					el: '#'+chart.id+'-chart', 
					model: chartModel
				});

				//render only if first tab
				if(isFirst && isFirstChart){
					chartModel.set({rendered: true});
				}
				
			});

			//render only if first tab
			if(isFirst){
				tableModel.set({rendered: true});
			}
		});
	},

});