var FileListView = Backbone.View.extend({
	initialize: function (options) {
		this.options = options;
		this.template = Handlebars.compile($('#file-list-template').html());
		this.model.on('change:isShow', this.showHide, this);
	},
	render: function(){
	    this.$el.html(this.template(this.model.attributes));
	    return this;
	},
	showHide: function(){
		if(this.model.get('isShow')){
			this.$el.removeClass("hidden");
		}
		else{
			this.$el.addClass("hidden");
		}    
	}
});