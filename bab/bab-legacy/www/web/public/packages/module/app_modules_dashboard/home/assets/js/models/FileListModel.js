FileListModel = Backbone.Model.extend({
	defaults: {
		url: undefined,
		noOfCases: 0,
		noOfEvents: 0,
		isLogRepositories: false,
		isShow: true
	}
});