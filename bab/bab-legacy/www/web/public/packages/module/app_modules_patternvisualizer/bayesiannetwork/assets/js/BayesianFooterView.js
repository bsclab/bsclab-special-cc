var BayesianFooterView = Backbone.View.extend({
	
	events: {
		'change select#nodeShape': 'changeNodeShape'
	},

	initialize: function(){
		this.render();
	}, 

	render: function(){

	}, 

	changeNodeShape: function(ev){
		this.model.set({nodeShape: $(ev.currentTarget).val() });
	}
});
