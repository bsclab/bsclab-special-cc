PerformanceChartModel = Backbone.Model.extend({
	defaults: {

	}, 
	initialize: function(){
		var series = this.mapSeries(this.get('logObject'), this.get('post').series);
		this.set({ series : series });
		
		var seriesList = this.getSeriesList(this.get('chartObject').matrix, this.get('series'));
		this.set({ seriesList : seriesList });
	}, 
	mapSeries: function(logObject, seriesType){
		var mapSeries = {
			"Task": "activities",
			"Originator": "originators"
		};
		return logObject[mapSeries[seriesType]];
	},
	getSeriesList: function(matrixData, series){
		var objList = [];
		var idx = 0;
		for(var row in series){
			idx++;
			var filtered = _.filter(matrixData, function(value, key){
			   return value.label == row; 
			});

			filtered = _.map(filtered, function(value, key){

				if(_.isString(value.x) && _.isNumber(parseInt(value.x))){
					value.x = parseInt(value.x);
				}
				else{
					value.x = value.x;
				}
				
				return [value.x, value.y]
			});

			filtered = _.sortBy(filtered, function(value, key){
				return Math.min(value[0]);
			});

			objList.push({ id:idx, label: row, data: filtered });
		}
		return objList;
	}
});