function TaskMatrixModel(args) {
    var data = args.json;
    var margin = args.margin,//{ top: 150, right: 10, bottom: 50, left: 100 },
    cellSize=12;
    col_number= data.hccol.length;//60;
    row_number=data.hcrow.length;//50;
    width = args.width,//cellSize*col_number, // - margin.left - margin.right,
    height = args.height,//cellSize*row_number , // - margin.top - margin.bottom,
    legendElementWidth = cellSize*2.5,
    colorBuckets = 21,
    hcrow = data.hcrow,
    hccol = data.hccol,
    rowLabel = data.rowLabel,
    colLabel = data.colLabel;
    $('.unitTitle').append(data.unit);
    var totalRatio = 0;
    var url  = args.baseUrl+data.casesUrl;
    var caseUrl = (data.casesUrl).split("/");
    var myUnit = data.unit;
    var caseMap;

    console.log(url);
    $.ajax({
			dataType: 'json',
			async	: true,
			url		: url,
			success	: function(res){
        caseMap = res.caseMap;
        console.log(caseMap);
			}}
    );

var heat = data.heatList;
    //console.log(heat);
    //d3.tsv("http://localhost/logminer/public/matrix.tsv",
d3.json(heat, function(d) {
        var dataCell = [], i=0, variance = 0, stdev = 0, min, max, mean;
        var neutral = 0, good = 0, bad = 0, ratio = 0;

		heat.forEach(function (d){
            if (!d.empty)
                variance += Math.pow((data.statistics.mean - d.value), 2);

            ratio = d.value/data.statistics.max;//d.log2ratio
            totalRatio = totalRatio+ratio;
            dataCell[i++] = {
                row:   d.source,//d.row_idx,
                col:   d.target,//d.col_idx,
                value: ratio,//d.value/data.statistics.max,//d.log2ratio
                oValue: d.value, // original value
                isEmpty: d.empty,
                casesNumber: d.casesNumber,
                movement: {
                    neutral: 1,
                    good: 0,
                    bad: 0
                }
            };
            if (!d.empty)
              console.log(rowLabel[d.source]+"=>"+colLabel[d.target]+" = "+d.value);
	  });

    stdev = Math.sqrt(variance/data.statistics.count);
    if (data.unit == 'duration'){
        min = BabHelper.printTimeDuration(data.statistics.min);
        max = BabHelper.printTimeDuration(data.statistics.max);
        mean = BabHelper.printTimeDuration(data.statistics.mean);
        stdev = BabHelper.printTimeDuration(stdev);
    }else{
        min = BabHelper.printNumber(data.statistics.min);
        max = BabHelper.printNumber(data.statistics.max);
        mean = BabHelper.printNumber(data.statistics.mean);
        stdev = BabHelper.printNumber(stdev);
    }

    // put statistic data into table
    $('#statistic').append('<tr><td>Min</td><td class="text-right">'+min+'</td></tr>'+
                                '<tr><td>Max</td><td class="text-right">'+max+'</td></tr>'+
                                '<tr><td>Mean</td><td class="text-right">'+mean+'</td></tr>'+
                                '<tr><td>Standard Deviation</td><td class="text-right">'+stdev+'</td></tr>'+
                                '<tr><td>Null Data</td><td class="text-right">'+(dataCell.length - data.statistics.count)+'</td></tr>'+
                                '<tr><td>Not Null Data</td><td class="text-right">'+data.statistics.count+'</td></tr>');

    cellSize = (width + margin.left + margin.right) / Math.max(col_number, row_number);
    if (cellSize < 25)
      cellSize = (width + margin.left + margin.right) / Math.min(col_number, row_number);

    console.log(cellSize);
    console.log(cellSize*col_number);
    console.log(cellSize*row_number);
    console.log(width +" "+ margin.left +" "+ margin.right);
    console.log(height +" "+ margin.top +" "+ margin.bottom);
    var movement = [0, 100, 0];
    updateMovement(movement);

    $('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var from = (button.data('row')).split("|"); // Extract info from data-* attributes
        var to = (button.data('col')).split("|"); // Extract info from data-* attributes
        var value = button.data('value'); // Extract info from data-* attributes
        var ratio = button.data('ratio'); // Extract info from data-* attributes
        var empty = button.data('empty'); // Extract info from data-* attributes
        var casesNumber = button.data('cases'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-body input[name="from"]').val(from[0]);
        modal.find('.modal-body input[name="fromHidden"]').val(from[1]);
        modal.find('.modal-body input[name="to"]').val(to[0]);
        modal.find('.modal-body input[name="toHidden"]').val(to[1]);
        modal.find('.modal-body input[name="value"]').val(value);
        modal.find('.modal-body input[name="ratio"]').val(ratio);
        $('#casesNumber').text(casesNumber);
        $(".modal-body .list-group").empty();

        if (typeof caseMap[from[1]+"|"+to[1]] !== 'undefined'){
          var count = 1;
          $("#loadMore").show();
          caseMap[from[1]+"|"+to[1]].forEach(
            function(entry){
              //console.log(entry);
              //if (count < 11)
                  $(".modal-body .list-group").append($("<li>").attr("class","list-group-item").text(entry));
                
              count++;
            }
          );

          /*size_li = 10;
          x=3;
          $('#myList li:lt('+x+')').show();
          $('#loadMore').click(function () {
              x= (x+3 <= size_li) ? x+3 : size_li;
              $('#myList li:lt('+x+')').slideDown(400);
              
              if (x >= size_li){
                  $("#loadMore").hide();
                  $("#download").show();
              }
          });*/
        }else{
            $("#loadMore").hide();
            $("#download").hide();
        }

        var data = _.findWhere(dataCell, {row:from[1], col:to[1], isEmpty:false});

        if (empty){
            modal.find('.modal-body .btn-group').hide();
            $('#badChoice').hide();
            $('#neutralChoice').hide();
            $('#goodChoice').hide();
        }
        else{
            modal.find('.modal-body .btn-group').show();

            if (data.movement.bad == 1){
                $('#badChoice').show();
                $('#neutralChoice').hide();
                $('#goodChoice').hide();

                $('#option1').prop('checked', true);
                $('#btnBad').addClass('active');
                $('#btnNeutral').removeClass('active');
                $('#btnGood').removeClass('active');
            }else if (data.movement.neutral == 1){
                $('#badChoice').hide();
                $('#neutralChoice').show();
                $('#goodChoice').hide();

                $('#option2').prop('checked', true);
                $('#btnBad').removeClass('active');
                $('#btnNeutral').addClass('active');
                $('#btnGood').removeClass('active');
            }else if (data.movement.good == 1){
                $('#badChoice').hide();
                $('#neutralChoice').hide();
                $('#goodChoice').show();

                $('#option3').prop('checked', true);
                $('#btnBad').removeClass('active');
                $('#btnNeutral').removeClass('active');
                $('#btnGood').addClass('active');
            }
        }
        
        $('#download').click(function(e) {
            from[0] = from[0].replace(/\s/g, "");
            to[0] = to[0].replace(/\s/g, "");
            e.preventDefault();  //stop the browser from following
            window.location.href = '../download-cases/'+caseUrl[4]+"/"+ from[0] +";"+to[0] +";"+from[1]+";"+to[1]+";"+myUnit+";"+value+";"+ratio+";"+casesNumber;
        });
    });

    $('input:radio[name="options"]').change(function (){
        if ($(this).val() == -1){
            $('#badChoice').show();
            $('#neutralChoice').hide();
            $('#goodChoice').hide();
        }else if ($(this).val() == 0){
            $('#badChoice').hide();
            $('#neutralChoice').show();
            $('#goodChoice').hide();
        }else if ($(this).val() == 1){
            $('#badChoice').hide();
            $('#neutralChoice').hide();
            $('#goodChoice').show();
        }
    });


	//function(error, d) {

//	  var colorScale = d3.scale.quantile()
//		  .domain([ 0, 1])
//		  .range(colors);

     var colorScale = d3.scale.quantize()
        .range(colorbrewer.Oranges[9]);

	  var svg = d3.select(args.content).append("svg")
		  .attr("id", "taskmatrix")
		  .attr("width", width + margin.left + margin.right)
		  .attr("height", cellSize*row_number + margin.top + margin.bottom)
      //.attr("viewBox", "0 0 "+(width+ margin.left + margin.right)+" "+(height+ margin.top + margin.bottom))
		  .append("g")
		  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // var chart = $("#taskmatrix"),
    //     aspect = (width + margin.left + margin.right) / (height + margin.top + margin.bottom);
    // $(window).on("resize", function() {
    //     var targetWidth = $(args.content).width();
    //     chart.attr("width", targetWidth);
    //     chart.attr("height", Math.round(targetWidth / aspect));
    // }).trigger("resize");

	  var rowSortOrder=false;
	  var colSortOrder=false;

	  var rowLabels = svg.append("g")
		  .selectAll(".rowLabelg")
		  .data(rowLabel)
		  .enter()
		  .append("text")
		  .text(function (d) { return d; })
		  .attr("x", 0)
		  .attr("y", function (d, i) { return hcrow.indexOf(i+1) * cellSize - 10; })
      .attr("dy", "0em").call(wrap, 20, "x")
		  .style("text-anchor", "end")
		  .attr("transform", "translate(-6," + cellSize / 1.5 + ")")
		  .attr("class", function (d,i) { return "rowLabel mono r"+i;} )
		  .on("mouseover", function(d) {d3.select(this).classed("text-hover",true);})
		  .on("mouseout" , function(d) {d3.select(this).classed("text-hover",false);})
		  .on("click", function(d,i) {rowSortOrder=!rowSortOrder; sortbylabel("r",i,rowSortOrder);d3.select("#order").property("selectedIndex", 4)
          .node().focus();});

	  var colLabels = svg.append("g")
		  .selectAll(".colLabelg")
		  .data(colLabel)
		  .enter()
		  .append("text")
		  .text(function (d) { return d; })
		  .attr("x", -5)
		  .attr("y", function (d, i) { return hccol.indexOf(i+1) * cellSize - 7; })
      .attr("dy", "0em").call(wrap, 20, "x")
		  .style("text-anchor", "left")
		  .attr("transform", "translate("+cellSize/2 + ",-20) rotate (-90)")
		  .attr("class",  function (d,i) { return "colLabel mono c"+i;} )
		  .on("mouseover", function(d) {d3.select(this).classed("text-hover",true);})
		  .on("mouseout" , function(d) {d3.select(this).classed("text-hover",false);})
		  .on("click", function(d,i) {colSortOrder=!colSortOrder;  sortbylabel("c",i,colSortOrder);d3.select("#order").property("selectedIndex", 4)
          .node().focus();});

	  var heatMap = svg.append("g").attr("class","g3")
			.selectAll(".cellg")
			.data(dataCell,function(d){return d.row+":"+d.col;})
			.enter()
			.append("rect")
			.attr("x", function(d) { return (d.col) * cellSize; })
			.attr("y", function(d) { return (d.row) * cellSize; })
			.attr("class", function(d){return "cell cell-border cr"+(d.row)+" cc"+(d.col)+(d.isEmpty?" null":"");})
			.attr("width", cellSize)
			.attr("height", cellSize)
      .attr("id", function(d){return "cr"+(d.row)+"-cc"+(d.col);})
      .attr("data-toggle", "modal")
      .attr("data-target", "#myModal")
      .attr("data-row", function(d){ return rowLabel[d.row]+"|"+d.row; })
      .attr("data-col", function(d){ return colLabel[d.col]+"|"+d.col; })
      .attr("data-value", function(d){
          return d.isEmpty? "Null": data.unit == 'duration'? BabHelper.printTimeDuration(d.oValue) : 
          numeral(d.oValue).format('0,0'); 
      })
      .attr("data-ratio", function(d){ return d.isEmpty? "Null": BabHelper.printNumber(d.value, 4); })
      .attr("data-empty", function(d){ return d.isEmpty; })
      .attr("data-cases", function(d){ return numeral(d.casesNumber).format('0,0'); })
			.style("fill", function(d) { return colorScale(d.value); })
			.on("mouseover", function(d){
				   //highlight text
				   d3.select(this).classed("cell-hover",true);
				   d3.selectAll(".rowLabel").classed("text-highlight",function(r,ri){ return ri==(d.row);});
				   d3.selectAll(".colLabel").classed("text-highlight",function(c,ci){ return ci==(d.col);});

				   //Update the tooltip position and value
				   d3.select("#tooltip")
                     .style("left", (d3.event.pageX-220) + "px")
					 .style("top", (d3.event.pageY-150) + "px")
					 .select("#value")
					 .text("lables:["+rowLabel[d.row]+", "+colLabel[d.col]+"]\nratio:"+BabHelper.printNumber(d.value,4)+" "
                           +data.unit+": "+numeral(d.oValue).format('0,0')+" \nrow-col-idx:"+d.col+","+d.row);//+"\ncell-xy "+this.x.baseVal.value+", "+this.y.baseVal.value);
//				   //Show the tooltip
				   d3.select("#tooltip").classed("hidden", false);
			})
			.on("mouseout", function(){
				   d3.select(this).classed("cell-hover",false);
				   d3.selectAll(".rowLabel").classed("text-highlight",false);
				   d3.selectAll(".colLabel").classed("text-highlight",false);
				   d3.select("#tooltip").classed("hidden", true);
			});



    $('#saveMovement').click(function (){
        var opt = $('input:radio[name=options]:checked').val();
        var x = $('input:hidden[name=fromHidden]').val();
        var y = $('input:hidden[name=toHidden]').val();

        console.log(opt+" pilihan");

        // find data and pop out
        var data = _.findWhere(dataCell, {row:x, col:y, isEmpty:false});
        var data2 = _.where(dataCell, {isEmpty:false});
        var stroke = '';

        if (data !== undefined){
            console.log(data);
            console.log(data.movement);
            dataCell = _.without(dataCell, data);

            // update data and push back
            if (opt == -1){
                data.movement = {neutral: 0, good: 0, bad: 1};
                stroke = '#d43f3a';
            }else if (opt == 0){
                data.movement = {neutral: 1, good: 0, bad: 0};
                stroke = '#31b0d5';
            }else if (opt == 1){
                data.movement = {neutral: 0, good: 1, bad: 0};
                stroke = '#5cb85c';
            }
            dataCell.push(data);

            // count data
            var neutralData = _.filter(dataCell, function(value){
                   return !value.isEmpty && value.movement.neutral==1 && value.movement.good==0 && value.movement.bad==0;
                });
            var badData = _.filter(dataCell, function(value){
                   return !value.isEmpty && value.movement.neutral==0 && value.movement.good==0 && value.movement.bad==1;
                });
            var goodData = _.filter(dataCell, function(value){
                   return !value.isEmpty && value.movement.neutral==0 && value.movement.good==1 && value.movement.bad==0;
                });

            var badRatio=0, goodRatio=0, neutralRatio=0;

            badData.forEach(function (d){
                badRatio += d.value;
            });
            neutralData.forEach(function (d){
                goodRatio += d.value;
            });
            goodData.forEach(function (d){
                neutralRatio += d.value;
            });

            var movement = [badRatio/totalRatio*100, goodRatio/totalRatio*100, neutralRatio/totalRatio*100]; // bad, neutral, good
            updateMovement(movement);

            var sWidth = d3.select('#cr'+x+'-cc'+y).attr("width") / 10;
            if (sWidth > 5) sWidth = 5;
            d3.select('#cr'+x+'-cc'+y).style("stroke-width", Math.floor(sWidth)).style("stroke", stroke);

            console.log(sWidth);
            console.log(neutralData.length);
            console.log(badData.length);
            console.log(goodData.length);
        }

        // close modal
        $('#myModal').modal('toggle');
    });

    /*$('.g3 rect').qtip({
        show:{
            event:'click'
        },
        hide:{
            event: 'unfocus'
        },
        content: {
            text: function(event, api) {
                var html='<a href="#" id="addNeutral" class="btn btn-default">aaaa</a>';
                console.log($(this).attr('class'));
                return html;
            }
        },
        events: {
            render: function(event, api) {
                console.log('render');
                $('#addNeutral').on('click',function (){
                    console.log('add click');
                });
            },
            show: function(event, api) {
                console.log('shpw');
                //console.log($(this).html();
                $('#addNeutral').on('click',function (){
                    console.log('add click');
                });
            }
        }
    });*/

/*	  var legend = svg.selectAll(".legend")
		  .data([0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1])
		  .enter().append("g")
		  .attr("class", "legend");

	  legend.append("rect")
		.attr("x", function(d, i) { return legendElementWidth * i; })
		.attr("y", height)
		.attr("width", legendElementWidth)
		.attr("height", 12)
		.style("fill", function(d, i) { return colors[i]; });

	  legend.append("text")
		.attr("class", "mono")
		.text(function(d) { return d; })
		.attr("width", legendElementWidth)
		.attr("x", function(d, i) { return legendElementWidth * i; })
		.attr("y", height + (12*2));*/

    var legend = d3.select('#legend')
      .append('ul')
        .attr('class', 'list-inline');

    var keys = legend.selectAll('li.key')
        .data(colorScale.range());

    keys.enter().append('li')
        .attr('class', 'key')
        .style('border-top-color', String)
        .text(function(d) {
            var r = colorScale.invertExtent(d);
            return d3.round(r[0],2);
        });

	// Change ordering of cells

	  function sortbylabel(rORc,i,sortOrder){
		   var t = svg.transition().duration(3000);
		   var log2r=[];
		   var sorted; // sorted is zero-based index
		   d3.selectAll(".c"+rORc+i)
			 .filter(function(ce){
				log2r.push(ce.value);
			  });

		   if(rORc=="r"){ // sort log2ratio of a gene
			 sorted=d3.range(col_number).sort(function(a,b){ if(sortOrder){ return log2r[b]-log2r[a];}else{ return log2r[a]-log2r[b];}});
			 t.selectAll(".cell")
			   .attr("x", function(d) { return sorted.indexOf(d.col-1) * cellSize; });
			 t.selectAll(".colLabel")
			  .attr("y", function (d, i) { return sorted.indexOf(i) * cellSize; });
		   }else{ // sort log2ratio of a contrast
			 sorted=d3.range(row_number).sort(function(a,b){
                 if(sortOrder){ return log2r[b]-log2r[a];}else{ return log2r[a]-log2r[b];}});
			 t.selectAll(".cell")
			   .attr("y", function(d) { return sorted.indexOf(d.row-1) * cellSize; });
			 t.selectAll(".rowLabel")
			  .attr("y", function (d, i) { return sorted.indexOf(i) * cellSize; });
		   }
          console.log(sorted);
	  }

	  d3.select("#order").on("change",function(){
		order(this.value);
	  });

	  function order(value){
	   if(value=="hclust"){
		var t = svg.transition().duration(3000);
		t.selectAll(".cell")
		  .attr("x", function(d) { return hccol.indexOf(d.col) * cellSize; })
		  .attr("y", function(d) { return hcrow.indexOf(d.row) * cellSize; });

		t.selectAll(".rowLabel")
		  .attr("y", function (d, i) { return hcrow.indexOf(i+1) * cellSize; });

		t.selectAll(".colLabel")
		  .attr("y", function (d, i) { return hccol.indexOf(i+1) * cellSize; });

	   }else if (value=="probecontrast"){
		var t = svg.transition().duration(3000);
		t.selectAll(".cell")
		  .attr("x", function(d) { return (d.col ) * cellSize; })
		  .attr("y", function(d) { return (d.row ) * cellSize; });

		t.selectAll(".rowLabel")
		  .attr("y", function (d, i) { return i * cellSize; });

		t.selectAll(".colLabel")
		  .attr("y", function (d, i) { return i * cellSize; });

	   }else if (value=="probe"){
		var t = svg.transition().duration(3000);
		t.selectAll(".cell")
		  .attr("y", function(d) { return (d.row ) * cellSize; });

		t.selectAll(".rowLabel")
		  .attr("y", function (d, i) { return i * cellSize; });
	   }else if (value=="contrast"){
		var t = svg.transition().duration(3000);
		t.selectAll(".cell")
		  .attr("x", function(d) { return (d.col ) * cellSize; });
		t.selectAll(".colLabel")
		  .attr("y", function (d, i) { return i * cellSize; });
	   }
	  }

	  // action trigger
/*	  var sa=d3.select(".g3")
		  .on("mousedown", function() {
			  if( !d3.event.altKey) {
				 d3.selectAll(".cell-selected").classed("cell-selected",false);
				 d3.selectAll(".rowLabel").classed("text-selected",false);
				 d3.selectAll(".colLabel").classed("text-selected",false);
			  }
			 var p = d3.mouse(this);
			 sa.append("rect")
			 .attr({
				 rx      : 0,
				 ry      : 0,
				 class   : "selection",
				 x       : p[0],
				 y       : p[1],
				 width   : 1,
				 height  : 1
			 })
		  })
		  .on("mousemove", function() {
			 var s = sa.select("rect.selection");

			 if(!s.empty()) {
				 var p = d3.mouse(this),
					 d = {
						 x       : parseInt(s.attr("x"), 10),
						 y       : parseInt(s.attr("y"), 10),
						 width   : parseInt(s.attr("width"), 10),
						 height  : parseInt(s.attr("height"), 10)
					 },
					 move = {
						 x : p[0] - d.x,
						 y : p[1] - d.y
					 }
				 ;

				 if(move.x < 1 || (move.x*2<d.width)) {
					 d.x = p[0];
					 d.width -= move.x;
				 } else {
					 d.width = move.x;
				 }

				 if(move.y < 1 || (move.y*2<d.height)) {
					 d.y = p[1];
					 d.height -= move.y;
				 } else {
					 d.height = move.y;
				 }
				 s.attr(d);

					 // deselect all temporary selected state objects
				 d3.selectAll('.cell-selection.cell-selected').classed("cell-selected", false);
				 d3.selectAll(".text-selection.text-selected").classed("text-selected",false);

				 d3.selectAll('.cell').filter(function(cell_d, i) {
					 if(
						 !d3.select(this).classed("cell-selected") &&
							 // inner circle inside selection frame
						 (this.x.baseVal.value)+cellSize >= d.x && (this.x.baseVal.value)<=d.x+d.width &&
						 (this.y.baseVal.value)+cellSize >= d.y && (this.y.baseVal.value)<=d.y+d.height
					 ) {

						 d3.select(this)
						 .classed("cell-selection", true)
						 .classed("cell-selected", true);

						 d3.select(".r"+(cell_d.row-1))
						 .classed("text-selection",true)
						 .classed("text-selected",true);

						 d3.select(".c"+(cell_d.col-1))
						 .classed("text-selection",true)
						 .classed("text-selected",true);
					 }
				 });
			 }
		  })
		  .on("mouseup", function() {
				// remove selection frame
			 sa.selectAll("rect.selection").remove();

				 // remove temporary selection marker class
			 d3.selectAll('.cell-selection').classed("cell-selection", false);
			 d3.selectAll(".text-selection").classed("text-selection",false);
		  })
		  .on("mouseout", function() {
			 if(d3.event.relatedTarget.tagName=='html') {
					 // remove selection frame
				 sa.selectAll("rect.selection").remove();
					 // remove temporary selection marker class
				 d3.selectAll('.cell-selection').classed("cell-selection", false);
				 d3.selectAll(".rowLabel").classed("text-selected",false);
				 d3.selectAll(".colLabel").classed("text-selected",false);
			 }
		  });*/
	}
);

} // end outer task matrixfunction

function updateMovement($val){
    $('#bad').css('width', $val[0] + '%');
    $('#badLabel').text(BabHelper.printNumber($val[0], 2) + '%');

    $('#neutral').css('width', $val[1] + '%');
    $('#neutralLabel').text(BabHelper.printNumber($val[1], 2) + '%');

    $('#good').css('width', $val[2] + '%');
    $('#goodLabel').text(BabHelper.printNumber($val[2], 2) + '%');
}

function wrap(text, width, r) {
    if (r == "x") {
        lineHeight = 0.9, // ems
        x = -6;
        min = 0.5;
    }
    else if(r == "y") {
        lineHeight = 0.1, // ems
        x = 6;
        min = 0;
    }

    if (text.text().length > width)
    text.each(function() {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(), word,
            line = [],
            lineNumber = 0,
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");
        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy - min + "em").text(word);
            }
        }
    });
}
