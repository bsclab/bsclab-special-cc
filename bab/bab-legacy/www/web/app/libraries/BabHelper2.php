<?php
class BabHelper2 {

    public static function babModuleAsset($asset) {
        return asset('packages/module/app_modules_'.$asset);
    }

	public static function displayNumber($number) {
		return number_format ( $number, 2, ',', '.' );
	}
	
	/* function to */
	public static function toLowerCase($theString) {
		return str_replace ( ' ', '_', trim ( strtolower ( $theString ) ) );
	}
	
	/*
	 * function to initialize nav/tabs based on first array
	 * compare current key/value with first key/value in array then return "active"
	 */
	public static function setActiveFirst($theArray, $type, $current) {
		$keys = null;
		if (strcmp ( $type, 'key' ) == 0) {
			$keys = array_keys ( $theArray );
		} elseif (strcmp ( $type, 'value' ) == 0) {
			$keys = array_values ( $theArray );
		}
		$first = array_shift ( $keys );
		return strcmp ( $first, $current ) == 0 ? 'active' : '';
	}
	public static function isWaitingJsonLoaded($jsonUri) {
		$waiting = json_decode ( file_get_contents ( $jsonUri ), TRUE );
		return isset ( $waiting ['returnUri'] );
	}
	public static function isWaitingJsonLoaded2($jsonUri) {
		// if empty >> "returnUri": "devel_repairExample20140913215701_59542e472295ef13d06afe3d322aa6af"
		/*
		 * else
		 * {
		 * "arcs": {
		 * "Register (complete)|Analyze Defect (start)": {
		 * "dependency": 0.99909420289855,
		 * "duration": {
		 * "max": -9223372036854800000,
		 * "mean": 0,
		 * "median": 0,
		 * "min": 9223372036854800000,
		 * "total": 0
		 * },
		 * "frequency": {
		 * "absolute": 1103,
		 * "cases": 0,
		 * "maxRepetition": 0,
		 * "relative": 0
		 * },
		 * "source": "Register (complete)",
		 * "target": "Analyze Defect (start)"
		 * },
		 * }
		 * }
		 */
		var_dump ( html_entity_decode ( $jsonUri ) );
		$waiting = json_decode ( html_entity_decode ( $jsonUri ), TRUE );
		return isset ( $waiting ['returnUri'] );
	}
	public static function removeArrayValue($heystack, $needle) {
		$arrExcept = array ();
		foreach ( $heystack as $key => $value ) {
			if (strcmp ( $value, '-' ) == 0) {
				array_push ( $arrExcept, $key );
			}
		}
		return array_except ( $heystack, $arrExcept );
	}
	public static function formatDateResourceId($theString) {
		$raw = explode ( '_', $theString );
		$name = substr ( $raw [1], 0, count ( $raw [1] ) - 15 );
		$date = substr ( $raw [1], count ( $raw [1] ) - 15 );
		$y = substr ( $date, 0, 4 );
		$m = substr ( $date, 4, 2 );
		$d = substr ( $date, 6, 2 );
		$h = substr ( $date, 8, 2 );
		$i = substr ( $date, 10, 2 );
		$s = substr ( $date, 12, 2 );
		$date = $d . '/' . $m . '/' . $y . " " . $h . ':' . $i . ':' . $s;
		return array (
				'resourceId' => $theString,
				'date' => $date,
				'name' => $name,
				'dateName' => $date . "-" . $name 
		);
	}
}

?>