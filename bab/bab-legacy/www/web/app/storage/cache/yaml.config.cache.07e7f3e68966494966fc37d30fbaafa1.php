<?php

return array (
  'cssfiles' => 
  array (
    0 => 'performancevisualizer/timegap/assets/css/style.css',
    1 => 'processvisualizer/processmodel/assets/css/ProcessModel.css',
  ),
  'jsfiles' => 
  array (
    0 => 'performancevisualizer/timegap/assets/js/script.js',
    1 => 'general/base/assets/lib/js/dagred3/dagre-d3.js',
    2 => 'general/base/assets/lib/js/c3/c3.min.js',
    3 => 'processvisualizer/processmodel/assets/js/models/NodeModel.js',
    4 => 'processvisualizer/processmodel/assets/js/models/ArcModel.js',
    5 => 'processvisualizer/processmodel/assets/js/models/ProcessModelModel.js',
    6 => 'processvisualizer/processmodel/assets/js/collections/NodeCollection.js',
    7 => 'processvisualizer/processmodel/assets/js/collections/ArcCollection.js',
    8 => 'processvisualizer/processmodel/assets/js/views/NodeView.js',
    9 => 'processvisualizer/processmodel/assets/js/views/ArcView.js',
    10 => 'processvisualizer/processmodel/assets/js/views/ProcessModelView.js',
  ),
);