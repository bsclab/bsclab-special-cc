<?php

return array (
  'cssfiles' => 
  array (
    0 => 'dashboard/home/assets/css/style.css',
  ),
  'jsfiles' => 
  array (
    0 => 'dashboard/home/assets/js/models/FileListModel.js',
    1 => 'dashboard/home/assets/js/collections/FileListCollection.js',
    2 => 'dashboard/home/assets/js/views/FileListView.js',
    3 => 'dashboard/home/assets/js/views/HomeView.js',
  ),
);