<?php

return array (
  'cssfiles' => 
  array (
    0 => 'general/layoutdashgum/assets/lib/css/style.css',
    1 => 'general/layoutdashgum/assets/lib/css/style-responsive.css',
    2 => 'general/layoutdashgum/assets/lib/css/table-responsive.css',
    3 => 'general/layoutdashgum/assets/lib/css/to-do.css',
    4 => 'general/layoutdashgum/assets/lib/lineicons/style.css',
    5 => 'general/layoutdashgum/assets/lib/css/zabuto_calendar.css',
    6 => 'general/layoutdashgum/assets/bab/css/style-adjusment.css',
  ),
  'jsfiles' => 
  array (
    0 => 'general/layoutdashgum/assets/lib/js/jquery.dcjqaccordion.2.7.js',
    1 => 'general/layoutdashgum/assets/lib/js/jquery.nicescroll.js',
    2 => 'general/layoutdashgum/assets/lib/js/jquery.scrollTo.min.js',
    3 => 'general/layoutdashgum/assets/lib/js/common-script.js',
  ),
);