<?php

return array (
  'cssfiles' => 
  array (
    0 => 'infographic/performancechart/assets/css/style.css',
    1 => 'general/base/assets/lib/js/jquery-ui/jquery-ui.min.css',
    2 => 'general/base/assets/lib/js/c3/c3.min.css',
    3 => 'general/base/assets/lib/js/d3-nvd3/nv.d3.min.css',
  ),
  'jsfiles' => 
  array (
    0 => 'general/base/assets/lib/js/jquery-ui/jquery-ui.min.js',
    1 => 'general/base/assets/lib/js/chartjs/Chart.min.js',
    2 => 'general/base/assets/bab/js/dygraph/DyGraphView.js',
    3 => 'general/base/assets/bab/js/chartjs/ChartJSView.js',
    4 => 'general/base/assets/bab/js/c3js/C3JSView.js',
    5 => 'infographic/performancechart/assets/js/script.js',
    6 => 'infographic/performancechart/assets/js/models/PerformanceChartModel.js',
    7 => 'infographic/performancechart/assets/js/views/PerformanceChartView.js',
    8 => 'general/base/assets/lib/js/c3/c3.min.js',
    9 => 'general/base/assets/bab/js/logminer-c3.js',
  ),
);