<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="BAB">
    <link rel="shortcut icon" href="{{ babModuleAsset('general/base/assets/bab/images/favicon.ico') }}">

    <title>Error</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ babModuleAsset('general/base/assets/lib/bootstrap3/css/bootstrap.min.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ babModuleAsset('general/base/assets/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="{{ babModuleAsset('general/layoutdashgum/assets/lib/css/style.css') }}" rel="stylesheet">
    <link href="{{ babModuleAsset('general/layoutdashgum/assets/lib/css/style-responsive.css') }}" rel="stylesheet">

    <style>
        .message{
            text-align: center; color:#fe8a17;
        }
    </style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body onload="getTime()">

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  	<div class="container">

	  		<div id="showtime"></div>
	  			<div class="col-lg-8 col-lg-offset-2">
                    <div class="message">
                        <h2>{{ $code }} </h2>
                        <h4>Upss... {{ $exception }}</h4>
                    </div>
	  				<div class="lock-screen">
		  				<h2><a href="#" onclick="history.go(-1);return false;"><i class="fa fa-arrow-circle-left"></i></a></h2>
		  				<p>Take me back</p>
	  				</div><! --/lock-screen -->
	  			</div><!-- /col-lg-4 -->

	  	</div><!-- /container -->

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ babModuleAsset('general/base/assets/lib/js/jquery/jquery.min.js') }}"></script>
    <script src="{{ babModuleAsset('general/base/assets/lib/bootstrap3/js/bootstrap.min.js') }}"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="http://www.blacktie.co/demo/dashgum/assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("{{ babModuleAsset('general/base/assets/bab/images/contact-bg.jpg') }}", {speed: 500});
    </script>

    <script>
        function getTime()
        {
            var today=new Date();
            var h=today.getHours();
            var m=today.getMinutes();
            var s=today.getSeconds();
            // add a zero in front of numbers<10
            m=checkTime(m);
            s=checkTime(s);
            document.getElementById('showtime').innerHTML=h+":"+m+":"+s;
            t=setTimeout(function(){getTime()},500);
        }

        function checkTime(i)
        {
            if (i<10)
            {
                i="0" + i;
            }
            return i;
        }
    </script>

  </body>
</html>
