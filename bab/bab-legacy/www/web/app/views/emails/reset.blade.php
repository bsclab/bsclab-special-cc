<html>

<head>
    <title>BAB Reset Password</title>
</head>

<body>
    <table style="margin:0 auto;border-bottom:1px solid #dddddd" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td style="font-size:30px;text-align:center">
                    <img src="http://bsclab.pusan.ac.kr/bab/web/packages/module/app_modules_general/base/assets/bab/images/logo-bab.png" height="60" width="141" vspace="10" class="CToWUd">
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0 auto" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td style="text-align: center;">
                    <font style="font-size:20px"><br></font>
                    <br>Hi {{ $name }},
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <table style="margin:0 auto" cellpadding="0" cellspacing="0" width="60%">
                            <tbody>
                                <tr>
                                    <td style="color:#999999;line-height:1.5em;text-align: center;">
                                        <br>Can't remember your password?  Don't worry about it - it happens.
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </center>
                </td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    To change your password, please click <a href="{{ $url }}" target="_blank">here</a> <br><br>
                    If the above link does not work for you, please copy and paste the following into your browser address bar:<br>
                    <a href="{{ $url }}" target="_blank">{{ $stringUrl }}</a>
                    <br>
                    <br>
                    Thanks,
                    <br>
                    <br>
                    BSC Lab Team
                    <br><br>
                    <font style="color:#999;font-size:11px">
                        <br>
                        If you didn't ask to reset your password, it's likely that another user entered your email address by mistake while trying to reset their password. We apologize for the inconvenience.<br>
                        <br>
                    </font>
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin:0 auto" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
        <tbody>
            <tr>
                <td style="color:#aaaaaa;font-size:11px;line-height:20px;text-align: center;">
                    <font style="line-height:10px"><br></font>
                    <font style="font-size:11px">
                        <a href="http://www.babcloud.org" target="_blank">Website</a> &nbsp; 
                        <a href="https://plus.google.com/111859401531354921754" target="_blank">Google +</a> &nbsp; 
                        <a href="https://www.youtube.com/channel/UCDwcd67EzTx9F8xTPfozASA" target="_blank">Youtube</a> &nbsp; 
                        <a href="http://bsclab.pusan.ac.kr/" target="_blank">BSC Lab</a> &nbsp; 
                        <a href="http://www.pusan.ac.kr/" target="_blank">PNU</a>
                    </font>
                    <br>Sent by BSC Lab Team. 부산광역시 금정구 부산대학로 63번길 2 제10공학관 10605호
                    <br>
                    2, Busandaehak-ro 63beon-gil, Engineering Building 10605, Geumjeong-gu, Busan, South Korea
                    <br>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>