<?php
/*
	@author : Dzulfikar Adi Putra
	@email  : dzulfikar.adiputra@gmail.com
*/

	if (!function_exists('bab_module_asset')) {
	    function bab_module_asset($module, $asset)
	    {
	        return 'packages/module/app_modules_'.$module.'/assets/'.$asset;
	    }
	}

	if (!function_exists('babModuleAsset')) {
	    function babModuleAsset($asset)
	    {
	        return asset('packages/module/app_modules_'.$asset);
	    }
	}

	if (!function_exists('babModuleAsset2')) {
	    function babModuleAsset2($module, $asset)
	    {
	        return asset('packages/module/app_modules_'.$module.'/assets/'.$asset);
	    }
	}



	if (!function_exists('bab_requirejs_common')) {
	    function bab_requirejs_common()
	    {	
	    	return bab_module_asset('general/layout', 'bab/js/common');
	    }
	}



	function printHello($theString){
		echo "Hello ".$theString ;
	}

	/*
	 * make lowercase string 'Case Duration' become 'case_duration'	 
	 */
	function toLowerCase($theString){
		return str_replace(' ', '_', trim(strtolower($theString)));
	}

	/*
	 * function to initialize nav/tabs based on first array
	 * compare current key/value with first key/value in array then return "active"
	 */
	function setActiveFirst($theArray, $type, $current){
		$keys=null;
		if(strcmp($type, 'key')==0){
			$keys = array_keys($theArray);
		}
		elseif(strcmp($type, 'value')==0){
			$keys = array_values($theArray);		
		}
		$first = array_shift($keys);
		return strcmp($first,$current)==0?'active':'' ;
	}


?>