<?php

return array(
    'welcome' => 'Welcome to our application',
    'status'=>array(
        'success'=>'Success!',
        'failed'=>'Failed!'
    ), 
    'fileupload' => array(
    	'success_mxml'=> 'MXML file upload successfully',
    	'success_csv' => 'CSV file upload successfully, please map the CSV column first!',
    	'nofile' => 'Please select a file'
    ),
    'csv_map' => array(
    	'success' => 'CSV Mapping Success',
    	'failed' => 'CSV Mapping Failed'
    )
);