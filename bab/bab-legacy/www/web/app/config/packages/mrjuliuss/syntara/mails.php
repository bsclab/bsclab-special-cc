<?php

return array(

    /**
     * contact name
     */
    'contact' => 'Syntara',

    /**
     * Admin mail
     * use for mails
     */
    'email' => 'bab.bsclab@gmail.com',

    /**
     * View for user activation email
     */
    'user-activation-view' => 'syntara::mail.user-activation',

    'user-activation-object' => 'Account activation'
);