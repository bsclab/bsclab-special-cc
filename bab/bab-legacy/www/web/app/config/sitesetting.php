<?php 

function getAsset($parent, $module)
{
    return array(
    	'js' => 'packages/module/app_modules_'.$parent.'/'.$module.'/assets/js/',
    	'css' => 'packages/module/app_modules_'.$parent.'/'.$module.'/assets/css/'
    	);
}

return array(
	'assets' => array(
		'requirejs' => array(
			'common' => 'packages/module/app_modules_general/layout/assets/bab/js/common', //relative path from public
			),
		'module'=> array(
			'dashboard'=> array(
				'home' => getAsset('dashboard', 'home'),
				'logsummary' => getAsset('dashboard', 'logsummary'),
				), 
			'processvisualizer'=>array(
				'fuzzyminer'	=> getAsset('processvisualizer','fuzzyminer'),
				'heuristicminer'=> getAsset('processvisualizer','heuristicminer'),
				'logreplay'		=> getAsset('processvisualizer','logreplay'),
				'processmodel' 	=> getAsset('processvisualizer','processmodel'),
				'proximityminer'=> getAsset('processvisualizer','proximityminer'),
				)
			), 
			'patternvisualizer'=>array(
				'associationrule' 	=> getAsset('patternvisualizer', 'associationrule'),
				'bayesiannetwork' 	=> getAsset('patternvisualizer', 'bayesiannetwork'),
				'logicchecker' 		=> getAsset('patternvisualizer', 'logicchecker'),
				'socialnetwork' 	=> getAsset('patternvisualizer', 'socialnetwork'),
				),
			'performancevisualizer'=>array(
				'timegap'		=> getAsset('performancevisualizer', 'deltaanalysis'),
				'deltaanalysis'	=> getAsset('performancevisualizer', 'deltaanalysis'),
				), 
			'processclustering'=> array(
				'kmeansclustering' 		=> getAsset('processclustering', 'kmeansclustering'),
				'hierarchicalclustering' => getAsset('processclustering', 'hierarchicalclustering'),
				),
			'infographic' => array(
				'performancechart' => getAsset('infographic', 'performancechart'),
				'dottedchart' => getAsset('infographic', 'dottedchart'),
				'taskmatrix' => getAsset('infographic', 'taskmatrix'),
				)
		)
	

);