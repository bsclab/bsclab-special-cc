<?php namespace App\Modules\Infographic\Dottedchart\Controllers;

use View, Auth, Redirect, User, Session, Config ;
use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\Infographic\Dottedchart\Models\DottedchartModel;
use App\Modules\General\API\Models\RepositoryAPIModel;
use App\Modules\General\API\Models\AnalysisAPIModel;

class DottedchartController extends BaseController {

    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(DottedchartModel $model, RepositoryAPIModel $repositoryAPIModel, AnalysisAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('isCooming');
        $this->beforeFilter('hasAccess:dc');

        $this->model = $model;
        $this->repositoryAPIModel = $repositoryAPIModel;
		$this->APIModel = $APIModel;
        
        $title = 'Dotted Chart ';
		$this->title = $title;
        
		$cssFiles = Config::get('dottedchart::config.cssfiles');
        $jsFiles = Config::get('dottedchart::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));
    }

    public function getIndex($resourceId)
    {

        $jsonData = array(
			'repository'	=> json_decode($this->repositoryAPIModel->getRepositoryStatistics($resourceId), true),
		);
        $this->layout->maincontent = View::make('dottedchart::index', compact('jsonData'));
    }

    public function postIndex($resourceId)
    {
		$data 	= $_POST['data'];
		$chart 	= $_POST['chart'];
		$colors	= isset($_POST['color']) ? $_POST['color'] : array();
		
        $opts = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query(array(
					'component' 	=> $data['component'],
					'shape' 		=> 'TYPE',
					'color' 		=> $data['color'],
					'timeFormat'	=> $data['timeFormat'],
					'timeSplit'		=> '10',
					'timeRelative'	=> $chart['relative'],
                ))
            )
        );

        $jsonData = array(
			'repository'	=> json_decode($this->repositoryAPIModel->getRepositoryStatistics($resourceId), true),
			'dottedchart' 	=> json_decode($this->APIModel->getDottedChart($resourceId, $opts), true),
		);
		
		if (isset($jsonData['dottedchart']['jobTrackingId'])) {
			return View::make('dottedchart::wait', compact('data', 'colors', 'chart', 'jsonData'));
		} else {
			return View::make('dottedchart::svg', compact('data', 'colors', 'chart', 'jsonData'));
		}
    }
	

    
}