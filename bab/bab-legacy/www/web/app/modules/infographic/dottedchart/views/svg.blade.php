<?php
	date_default_timezone_set('Asia/Seoul');
	
	$repository		= $jsonData['repository'];
	$chartData		= $jsonData['dottedchart'];
 	$dots			= $chartData['data'];
	$legendX		= $chartData['times'];
	$legendY		= $chartData['legend'];
	$dateFormat		= $chart['timeFormat'];
	$transpose 		= $chart['transpose'] == 'true';
	$logical 		= $chart['logical'] == 'true';
	$birdview 		= $chart['birdview'] == 'true';
	$showGridX		= $chart['gridX'] == 'true';
	$showGridY		= $chart['gridY'] == 'true';
	$boxWidth		= $chart['width'];
	$boxHeight		= $chart['height'];
	$sorter			= trim($chart['sorter']);
	$fontSize		= 12;
	$xLegendWidth	= 100;
	$yLegendWidth	= 25;
	$dotRadius		= 4;
	$margin			= 10;
	
	$scaleX 		= (200 - $chart['scaleX']) / 100;
	$scaleY 		= (200 - $chart['scaleY']) / 100;
	$gapX			= $chart['gapX'];
	$gapY			= $chart['gapY'];
	$offsetX		= $chart['gapX'];
	$offsetY		= $chart['gapY'];
	$colorPallete = array(
		"#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9", "#6C9", "#9C9", "#CC9", "#FC9",
		"#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC", "#9CC", "#CCC", "#FCC",
		"#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF", "#CCF", "#FCF",
		"#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0", "#FF0",
		"#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
		"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6",
		"#069", "#369", "#669", "#969", "#C69", "#F69", "#0F9", "#3F9", "#6F9", "#9F9", "#CF9", "#FF9",
		"#06C", "#36C", "#66C", "#96C", "#C6C", "#F6C", "#0FC", "#3FC", "#6FC", "#9FC", "#CFC", "#FFC",
		"#06F", "#36F", "#66F", "#96F", "#C6F", "#F6F", "#0FF", "#3FF", "#6FF", "#9FF", "#CFF", "#FFF",
		"#000", "#300", "#600", "#900", "#C00", "#F00", "#090", "#390", "#690", "#990", "#C90", "#F90",
		"#003", "#303", "#603", "#903", "#C03", "#F03", "#093", "#393", "#693", "#993", "#C93", "#F93",
		"#006", "#306", "#606", "#906", "#C06", "#F06", "#096", "#396", "#696", "#996", "#C96", "#F96",
		"#009", "#309", "#609", "#909", "#C09", "#F09", "#099", "#399", "#699", "#999", "#C99", "#F99",
		"#00C", "#30C", "#60C", "#90C", "#C0C", "#F0C", "#09C", "#39C", "#69C", "#99C", "#C9C", "#F9C",
		"#00F", "#30F", "#60F", "#90F", "#C0F", "#F0F", "#09F", "#39F", "#69F", "#99F", "#C9F", "#F9F",
		"#030", "#330", "#630", "#930", "#C30", "#F30", "#0C0", "#3C0", "#6C0", "#9C0", "#CC0", "#FC0",
		"#033", "#333", "#633", "#933", "#C33", "#F33", "#0C3", "#3C3", "#6C3", "#9C3", "#CC3", "#FC3",
		"#036", "#336", "#636", "#936", "#C36", "#F36", "#0C6", "#3C6", "#6C6", "#9C6", "#CC6", "#FC6",
	);
	
?>
<div class="col-sm-10">
<?php
	
	$colorPalleteCount = count($colorPallete);

	$colorsSingle = array();
	$colorsMeet = array();
	
	switch (strtoupper($data['color'])) {
		case 'CASE':
			$colorsSingle = $repository['cases'];
		break;
		case 'ACTIVITY':
			$colorsSingle = $repository['activities'];
		break;
		case 'ORIGINATOR':
			$colorsSingle = $repository['originators'];
		break;
		case 'RESOURCE':
			$colorsSingle = $repository['resources'];
		break;
	}
	
	foreach ($colorsSingle as $scolor => $row) {
		$colorsSingle[$scolor] = 0;
	}
	
	foreach ($dots as $scomp => $row) {
		foreach ($row as $stime => $event) {
			if ($event['type'] == 'meet') {
				if (!isset($colorsMeet[$event['name']])) {
					$colorsMeet[$event['name']] = 0;
				}
			} else {
				if (!isset($colorsSingle[$event['name']])) {
					$colorsSingle[$event['name']] = 0;
				}
			}
		}
	}
	
	asort($colorsSingle);
	$cm = 0;
	foreach ($colorsSingle as $colorName => $color) {
		$colorName = str_replace(' (start)', '', str_replace(' (complete)', '', $colorName));
		if (isset($colors[$colorName])) continue;
		$colors[$colorName] = $cm;
		$cm = ($cm + 1) % $colorPalleteCount;
	}
	
	asort($colorsMeet);
	foreach ($colorsMeet as $colorName => $color) {
		if (isset($colors[$colorName])) continue;
		$colors[$colorName] = $cm;
		$cm = ($cm + 1) % $colorPalleteCount;
	}
?>
	<style>
<?php foreach ($colorPallete as $colorIndex => $colorValue) { ?>
		.as{{$colorIndex}} {display: inline-block; -webkit-appearance:button; fill: white; stroke: {{$colorValue}}; background-color: white; border: solid 1px {{$colorValue}}; color: {{$colorValue}}; }
		.ac{{$colorIndex}} {display: inline-block; -webkit-appearance:button; fill: {{$colorValue}}; stroke: {{$colorValue}}; background-color: {{$colorValue}}; border: solid 1px {{$colorValue}}; color: {{$colorValue}}; }
<?php } ?>
	</style>
<?php
	
	switch ($sorter) {
		case 'durationShort':
			foreach ($dots as $scomp => $row) {
				$timeMin = null;
				$timeMax = null;
				foreach ($row as $stime => $event) {
					if ($timeMin === null) $timeMin = $stime;
					if ($timeMax === null) $timeMax = $stime;
					if ($stime < $timeMin) $timeMin = $stime;
					if ($stime > $timeMax) $timeMax = $stime;
				}
				$legendY[$scomp] = $timeMax - $timeMin;
			}
			asort($legendY);
		break;
		case 'durationLongest':
			foreach ($dots as $scomp => $row) {
				$timeMin = null;
				$timeMax = null;
				foreach ($row as $stime => $event) {
					if ($timeMin === null) $timeMin = $stime;
					if ($timeMax === null) $timeMax = $stime;
					if ($stime < $timeMin) $timeMin = $stime;
					if ($stime > $timeMax) $timeMax = $stime;
				}
				$legendY[$scomp] = $timeMax - $timeMin;
			}
			arsort($legendY);
		break;
		case 'eventLeast':
			foreach ($dots as $scomp => $row) {
				$legendY[$scomp] = count($row);
			}
			asort($legendY);
		break;
		case 'eventMost':
			foreach ($dots as $scomp => $row) {
				$legendY[$scomp] = count($row);
			}
			arsort($legendY);
		break;
		case 'earliestStart':
			foreach ($dots as $scomp => $row) {
				foreach ($row as $stime => $event) {
					$legendY[$scomp] = $stime;
					break;
				}
			}
			asort($legendY);
		break;
		case 'earliestComplete':
			foreach ($dots as $scomp => $row) {
				foreach ($row as $stime => $event) {
					$legendY[$scomp] = $stime;
				}
			}
			asort($legendY);
		break;
		case 'latestStart':
			foreach ($dots as $scomp => $row) {
				foreach ($row as $stime => $event) {
					$legendY[$scomp] = $stime;
					break;
				}
			}
			arsort($legendY);
		break;
		case 'latestComplete':
			foreach ($dots as $scomp => $row) {
				foreach ($row as $stime => $event) {
					$legendY[$scomp] = $stime;
				}
			}
			arsort($legendY);
		break;
		case 'nameDesc':
			krsort($legendY);
		break;
		case 'nameAsc':
		default:
			ksort($legendY);
		break;
	}
	
	foreach ($legendY as $key => $value) {
		$legendY[$key] = 0;
	}

	if ($transpose) {
		$legendY2 = $legendX;
		$legendX = $legendY;
		$legendY = $legendY2;
	}
	
	$chartWidth  = ($boxWidth  - $xLegendWidth - ($margin * 2));
	$chartHeight = ($boxHeight - $yLegendWidth - ($margin * 2));
	$xLength = count($legendX);
	$yLength = count($legendY);
	
	$gapXTemp = floor($chartWidth  / $xLength);
	$gapYTemp = floor($chartHeight / $yLength);
	if ($gapXTemp > $gapX) $gapX = $gapXTemp;
	if ($gapYTemp > $gapY) $gapY = $gapYTemp;
	if ($gapX % 2 == 1) $gapX = $gapX + 1;
	if ($gapY % 2 == 1) $gapY = $gapY + 1;

	if ($chartWidth < $gapX * $xLength) {
		$chartWidth = $gapX * $xLength;
	}
	if ($chartHeight < $gapY * $yLength) {
		$chartHeight = $gapY * $yLength;
	}
	
	$callWidth  = floor(($chartWidth 	+ $xLegendWidth + ($margin * 2)) * $scaleX) + 1;
	$callHeight = floor(($chartHeight 	+ $yLegendWidth + ($margin * 2)) * $scaleY) + 1;

	echo '<script type="text/javascript">chart.callWidth = ' . $callWidth . '; chart.callHeight = ' . $callHeight . ';</script>';
	echo '<svg id="id-dottedchart" viewBox="' . $offsetX . ' ' . $offsetY . ' ' . $callWidth . ' ' . $callHeight . '" preserveAspectRatio="' . ($birdview ? 'none' : (($transpose ? 'xMinYMid slice' : 'xMidYMin slice'))) . '" width="' . $boxWidth . '" height="' . $boxHeight . '">';
	
	$renderBuffer = '';
	$renderCount = 0;
	
	$xLegendPos 	= $margin + $yLegendWidth;
	$xLegendTextPos = $margin + $yLegendWidth - ($fontSize / 2);
	$yLegendPos 	= $margin + $xLegendWidth;
	$yLegendTextPos = $margin + $xLegendWidth - ($fontSize / 2);
	
	echo '<rect x="' . $yLegendPos . '" y="' . $xLegendPos . '" width="' . $chartWidth . '" height="' . $chartHeight . '" />';

	$y = $xLegendPos;
	$yHalf = $gapY / 2;
	$y += $yHalf;
	$rowClass = "odd";
	foreach ($legendY as $yLegend => $yLegendVal) {
		$legendY[$yLegend] = $y;
		$yLegendText = ($transpose) ? ($logical ? $yLegend : date($dateFormat, $yLegend)) : $yLegend;
		if ($showGridY) {
			echo '<rect x="' . $yLegendPos . '" y="' . ($legendY[$yLegend] - $yHalf) . '" width="' . ($chartWidth) . '" height="' . $gapY . '" class="id-row-' . $rowClass . '" />';
		}
		$y += $gapY;
		$rowClass = ($rowClass == 'odd') ? 'even' : 'odd';
	}
	
	$x = $yLegendPos;
	$x += $gapX / 2;
	$colClass = "odd";
	foreach ($legendX as $xLegend => $xLegendVal) {
		$legendX[$xLegend] = $x;
		$xLegendText = ($transpose) ? $xLegend : ($logical ? $xLegend : date($dateFormat, $xLegend));
		if ($showGridX) {
			echo '<line x1="' . $legendX[$xLegend] . '" y1="' . ($xLegendPos - (($colClass == 'odd') ? 0 : $fontSize)) . '" x2="' . $legendX[$xLegend] . '" y2="' . ($xLegendPos + $chartHeight) . '" class="id-col-' . $colClass . '" />';
		}
		$x += $gapX;
		$colClass = ($colClass == 'odd') ? 'even' : 'odd';
	}
	
	if ($transpose) {
		foreach ($dots as $yLegend => $yLegendVal) {
			foreach ($yLegendVal as $xLegend => $xLegendVal) {
				$event = $dots[$yLegend][$xLegend];
				$xDot = $legendX[$yLegend];
				
				$pLegend2Min = 0;
				$pLegend2Max = 0;
				foreach ($legendY as $yLegend2 => $yLegend2Val) {
					$pLegend2Min = $pLegend2Max;
					$pLegend2Max = $yLegend2;
					if ($yLegend2 > $xLegend) {
						break;
					}
				}
				$yDot = 0;
				if ($pLegend2Min == 0) {
					$pLegend2Min = $pLegend2Max;
					foreach ($legendY as $yLegend2 => $yLegend2Val) {
						if ($yLegend2 > $pLegend2Max) {
							$pLegend2Max = $yLegend2;
							break;
						}
					}
					$lGap = round($pLegend2Max) - round($pLegend2Min) + 1;
					$yDot = $legendY[$pLegend2Min] - (1 - (($xLegend - $pLegend2Min + $lGap) / $lGap)) * $gapY;
				} else {
					$yDot = $legendY[$pLegend2Min] + ($xLegend - $pLegend2Min) / ($pLegend2Max - $pLegend2Min) * $gapY;
				}
				if ($event['type'] == 'meet') {
					$dotClass = 'a' . ($event['type'] == 'start' ? 's' : 'c') . $colors[$event['name']];
					echo '<circle cx="' . $xDot . '" cy="' . $yDot . '" r="' . ($dotRadius * 1.5) . '" data-time="' . $xLegend . '" data-component="' . ($logical ? $yLegend : date($dateFormat, $yLegend)) . '" data-color="' . $event['name'] . '" data-shape="' . $event['type'] . '" class="dot ' . $dotClass . '"/>';
				} else {
					$dotClass = 'a' . ($event['type'] == 'start' ? 's' : 'c') . $colors[str_replace(' (start)', '', str_replace(' (complete)', '', $event['name']))];
					echo '<circle cx="' . $xDot . '" cy="' . $yDot . '" r="' . $dotRadius . '"  data-time="' . $xLegend . '" data-component="' . ($logical ? $yLegend : date($dateFormat, $yLegend)) . '" data-color="' . $event['name'] . '" data-shape="' . $event['type'] . '" class="dot ' . $dotClass . '" />';
				}					
			}
		}
	} else {
		foreach ($dots as $yLegend => $xLegendVal) {
			foreach ($xLegendVal as $xLegend => $yLegendVal) {
				$event = $dots[$yLegend][$xLegend];
				$yDot = $legendY[$yLegend];
				
				$pLegend2Min = 0;
				$pLegend2Max = 0;
				foreach ($legendX as $xLegend2 => $xLegend2Val) {
					$pLegend2Min = $pLegend2Max;
					$pLegend2Max = $xLegend2;
					if ($xLegend2 > $xLegend) {
						break;
					}
				}
				$xDot = 0;
				if ($pLegend2Min == 0) {
					$pLegend2Min = $pLegend2Max;
					foreach ($legendX as $xLegend2 => $xLegend2Val) {
						if ($xLegend2 > $pLegend2Max) {
							$pLegend2Max = $xLegend2;
							break;
						}
					}
					$lGap = round($pLegend2Max) - round($pLegend2Min) + 1;
					$xDot = $legendX[$pLegend2Min] - (1 - (($xLegend - $pLegend2Min + $lGap) / $lGap)) * $gapX;
				} else {
					$xDot = $legendX[$pLegend2Min] + ($xLegend - $pLegend2Min) / ($pLegend2Max - $pLegend2Min) * $gapX;
				}
				if ($event['type'] == 'meet') {
					$dotClass = 'a' . ($event['type'] == 'start' ? 's' : 'c') . $colors[$event['name']];
					echo '<circle cx="' . $xDot . '" cy="' . $yDot . '" r="' . ($dotRadius * 1.5) . '"  data-time="' . ($logical ? $xLegend : date($dateFormat, $xLegend)) . '" data-component="' . $yLegend . '" data-color="' . $event['name'] . '" data-shape="' . $event['type'] . '" class="dot ' . $dotClass . '" />';
				} else {
					$dotClass = 'a' . ($event['type'] == 'start' ? 's' : 'c') . $colors[str_replace(' (start)', '', str_replace(' (complete)', '', $event['name']))];
					echo '<circle cx="' . $xDot . '" cy="' . $yDot . '" r="' . $dotRadius . '"  data-time="' . ($logical ? $xLegend : date($dateFormat, $xLegend)) . '" data-component="' . $yLegend . '" data-color="' . $event['name'] . '" data-shape="' . $event['type'] . '" class="dot ' . $dotClass . '" />';
				}					
			}
		}		
	}

	echo '<rect x="0" y="0" width="' . ($boxWidth + $margin * 2) . '" height="' . ($xLegendPos - 2) . '" fill-color="white" stroke-width="0" class="box-fix" opacity="0.75" />';
	echo '<rect x="0" y="0" width="' . ($yLegendPos - 2) . '" height="' . ($boxHeight + $margin * 2) . '" fill-color="white" stroke-width="0" class="box-fix" opacity="0.75" />';
	
	$y = $xLegendPos;
	$yHalf = $gapY / 2;
	$y += $yHalf;
	$rowClass = "odd";
	foreach ($legendY as $yLegend => $yLegendVal) {
		$legendY[$yLegend] = $y;
		$yLegendText = ($transpose) ? ($logical ? $yLegend : date($dateFormat, $yLegend)) : $yLegend;
		echo '<text text-anchor="end" x="' . $yLegendTextPos . '" y="' . $legendY[$yLegend] . '" class="text-left">' . $yLegendText . '</text>';
		$y += $gapY;
		$rowClass = ($rowClass == 'odd') ? 'even' : 'odd';
	}
	
	$x = $yLegendPos;
	$x += $gapX / 2;
	$colClass = "odd";
	foreach ($legendX as $xLegend => $xLegendVal) {
		$legendX[$xLegend] = $x;
		$xLegendText = ($transpose) ? $xLegend : ($logical ? $xLegend : date($dateFormat, $xLegend));
		echo '<text text-anchor="middle" x="' . $legendX[$xLegend] . '" y="' . ($xLegendTextPos - (($colClass == 'odd') ? 0 : $fontSize)) . '" class="text-top' . ($colClass == 'odd' ? 1 : 2) . '">' . $xLegendText . '</text>';
		$x += $gapX;
		$colClass = ($colClass == 'odd') ? 'even' : 'odd';
	}
	
	echo '</svg>';
?>
</div>
<div class="col-sm-2" style="overflow: auto; height: {{$boxHeight}}px;">
	<form id="color-pickers">
		<button class="pull-right btn btn-small" type="button" onclick="reloadColor();redrawChart();">Update</button>
		<h3>Dots Legend</h3>
		<div>
			<span class="las"></span> Single Event Start<br />
			<span class="lac"></span> Single Event Complete<br />
			<span class="lam"></span> Multiple Events<br />
		</div>
		<h3>Single Event Colors</h3>
		<?php $meet = false; $ci = 0; foreach ($colors as $legend => $class) { 
			if (!$meet && strpos($legend, '|') >= -1) {
				$meet = true;
				echo '<button class="pull-right btn btn-small" type="button" onclick="$(\'#paralel-dots-colors\').toggle();">+</button><h3>Multiple Events Colors</h3><div id="paralel-dots-colors">';
			}; ?>
		<div>
			<select name="{{$legend}}" class="color-picker ac{{$class}}" id="color-{{$ci}}" onchange="$('#color-{{$ci}}').attr('class', 'color-picker ' + $($('#color-{{$ci}} :selected')[0]).attr('class'))">
				<?php foreach ($colorPallete as $colorIndex => $colorValue) { ?>
						<option value="{{$colorIndex}}" class="ac{{$colorIndex}}" <?php echo ($class==$colorIndex ? 'selected="selected"' : ''); ?>>{{$colorValue}}</option>
				<?php } ?>
			</select>
			{{ $legend }}
		</div>
		<?php $ci++; 
			} 
			if ($meet) echo '</div>';
		?>
		<br />
	</form>
</div>
<script>
var color = {
	<?php foreach ($colors as $legend => $class) { ?>
	'{{$legend}}': '{{$class}}',
	<?php } ?>
}
</script>