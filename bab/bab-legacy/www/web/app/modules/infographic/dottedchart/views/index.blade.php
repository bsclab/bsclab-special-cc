@section ('headerscript')
	@foreach( Config::get('dottedchart::config.cssfiles') as $value)
	<link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">
	@endforeach
	<style>
		#id-chart-container {
			overflow: auto;
		}

		svg {
			border: solid 1px black;
		}

		circle {
			fill: white;
			stroke: black;
		}

		rect {
			fill: white;
			stroke: black;
		}
		
		line {
			stroke: black;
		}
		
		.id-row-odd {
			fill: #efefef;
			stroke-width: 0; 
		}
		
		.id-row-even {
			fill: #dfdfdf;
			stroke-width: 0;
		}
		
		.id-col-odd {
			stroke: #efefef;
		}
		
		.id-col-even {
			stroke: #dfdfdf;
		}
		
		.dot {
			
		}
		
		.dot:hover {
			stroke-width: 32px;
		}

		.las { display: inline-block; width: 12px; margin: 2px; height: 12px; border-radius: 6px; fill: white; stroke: black; background-color: white; border: solid 1px black; }
		.lac { display: inline-block; width: 12px; margin: 2px; height: 12px; border-radius: 6px; fill: black; stroke: black; background-color: black; border: solid 1px black; }
		.lam { display: inline-block; width: 16px; height: 16px; border-radius: 8px; fill: black; stroke: black; background-color: black; border: solid 1px black; }
		
		.color-picker {
			width: 16px;
			height: 16px;
			display: inline-block;
			border-radius: 8px;
		}
		
		.color-picker > option {
			width: 32px;
		}
		
		.space {
			width: 16px;
			height: 16px;
			display: inline-block;
		}
		
	</style>
@stop 

@section('maincontent')
<div id="content" class="row mt">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
				<h3>{{ $title }}</h3>
            </div>
            <div id="id-chart-body" class="panel-body">
				<div class="row">
					<div id="graph-toolbox" class="col-sm-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<div class="btn-group">
									<button id="id-chart-birdview-button" type="button" class="btn btn-primary active" 
										data-toggle="tooltip" title="Birdview">
										<i class="fa fa-arrows-alt"></i>
									</button>
									<button id="id-chart-transpose-button" type="button" class="btn btn-default" 
										data-toggle="tooltip" title="Transpose Component(Row) and Time(Column)">
										<i class="fa fa-exchange"></i>
									</button>
									<button id="id-chart-relative-button" type="button" class="btn btn-default" 
										data-toggle="tooltip" title="Relative To First Event">
										<i class="fa fa-terminal"></i>
									</button>
									<button id="id-chart-logical-button" type="button" class="btn btn-default" 
										data-toggle="tooltip" title="Use Logical Time">
										<i class="fa fa-asterisk"></i>
									</button>
								</div>
								<div class="btn-group">
									<button id="dot-inspector-time" type="button" class="btn btn-primary" 
										data-toggle="tooltip" title="Current Time">
										Current TIME
									</button>
									<button type="button" class="btn btn-default" onclick="$('#option-time').toggle();">
										<i class="fa fa-clock-o"></i>
									</button>
									<ul id="option-time" class="dropdown-menu" style="padding: 4px;">
										<li>Time Format</li>
										<li>
											<select id="id-chart-timeformat" class="form-control">
												<option value="M d, y H:i:s" selected="selected">MMM dd, yyyy hh:mm:ss</option>
												<option value="M d, y">MMM dd, yyyy</option>
												<option value="H:i:s">hh:mm:ss</option>
												<option value="M d H:i:s">MMM dd, hh:mm:ss</option>
												<option value="d H:i:s">dd hh:mm:s</option>
												<option value="M d">MMM dd</option>
												<option value="d">dd</option>
												<option value="i:s">mm:ss</option>
												<option value="s">ss</option>
											</select>
										</li>
									</ul>
								</div>
								<div class="btn-group">
									<button id="dot-inspector-component" type="button" class="btn btn-primary" 
										data-toggle="tooltip" title="Current Component">
										Current COMPONENT
									</button>
									<button id="id-data-component-icon" type="button" class="btn btn-default" onclick="$('#option-component').toggle();">
										<i class="fa fa-globe"></i>
									</button>
									<ul id="option-component" class="dropdown-menu" style="padding: 4px;">
										<li>Component Dimension</li>
										<li>
											<select id="id-data-component" class="form-control">
												<option value="CASE" selected="selected">Case ({{ count($jsonData['repository']['cases']) }})</option>
												<option value="ACTIVITY">Activity ({{ count($jsonData['repository']['activities']) }})</option>
												<option value="ORIGINATOR">Human Performer ({{ count($jsonData['repository']['originators']) }})</option>
												<option value="RESOURCE">Non Human Performer ({{ count($jsonData['repository']['resources']) }})</option>
												<?php foreach ($jsonData['repository']['attributes'] as $attName => $attValues) { ?>
												<option value="{{ $attName }}">{{ $attName }} ({{ count($attValues) }})</option>
												<?php } ?>
											</select>
										</li>
										<li>Sort By</li>
										<li>
											<select id="id-chart-sorter" class="form-control">
												<option value="nameAsc">Name</option>
												<option value="nameDesc">Name (Descending)</option>
												<option value="eventLeast">Least Events</option>
												<option value="eventMost">Most Events</option>
												<option value="durationShort">Shortest Duration</option>
												<option value="durationLongest">Longest Duration</option>
												<option value="earliestStart" selected="selected">Earliest Start</option>
												<option value="earliestComplete">Earliest Complete</option>
												<option value="latestStart">Latest Start</option>
												<option value="latestComplete">Latest Complete</option>
											</select>
										</li>
									</ul>
								</div>
								<div class="btn-group">
									<button id="dot-inspector-color" type="button" class="btn btn-primary" 
										data-toggle="tooltip" title="Current Color">
										Current COLOR
									</button>
									<button id="id-data-color-icon" type="button" class="btn btn-default" onclick="$('#option-color').toggle();">
										<i class="fa fa-envelope-o"></i>
									</button>
									<ul id="option-color" class="dropdown-menu" style="padding: 4px;">
										<li>Color Dimension</li>
										<li>
											<select id="id-data-color" class="form-control">
												<option value="CASE">Case ({{ count($jsonData['repository']['cases']) }})</option>
												<option value="ACTIVITY" selected="selected">Activity ({{ count($jsonData['repository']['activities']) }})</option>
												<option value="ORIGINATOR">Human Performer ({{ count($jsonData['repository']['originators']) }})</option>
												<option value="RESOURCE">Non Human Performer ({{ count($jsonData['repository']['resources']) }})</option>
												<?php foreach ($jsonData['repository']['attributes'] as $attName => $attValues) { ?>
												<option value="{{ $attName }}">{{ $attName }} ({{ count($attValues) }})</option>
												<?php } ?>
											</select>
										</li>
									</ul>
								</div>
								<div class="btn-group pull-right">
									<button id="id-chart-gridX-button" type="button" class="btn btn-primary active" 
										data-toggle="tooltip" title="Show Row Stripes">
										<i class="fa fa-arrows-h"></i>
									</button>
									<button id="id-chart-gridY-button" type="button" class="btn btn-primary active" 
										data-toggle="tooltip" title="Show Column Grid">
										<i class="fa fa-arrows-v"></i>
									</button>
									<button type="button" class="btn btn-default" onclick="$('#option-zoom').toggle();">
										<i class="fa fa-search-plus"></i>
									</button>
									<ul id="option-zoom" class="dropdown-menu" style="padding: 4px;">
										<li>Horizontal Scaling</li>
										<li><div id="id-chart-scale-x"></div></li>
										<li>Vertical Scaling</li>
										<li><div id="id-chart-scale-y"></div></li>
										<li>Column Size</li>
										<li><div id="id-chart-gap-x"></div></li>
										<li>Row Size</li>
										<li><div id="id-chart-gap-y"></div></li>
									</ul>
								</div>
								<div class="col-sm-2 pull-right">
									<div data-toggle="tooltip" title="Column-Offset">
										<div id="id-chart-offset-x"></div>
									</div>
									<div data-toggle="tooltip" title="Row-Offset">
										<div id="id-chart-offset-y"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row" id="id-chart-container">
					<div class="col-sm-12">
						Loading chart ... please wait !
					</div>
				</div>
				<div class="row">
					<div id="id-chart-ruler" class="col-sm-10"></div>
					<div id="id-chart-ruler2" class="col-sm-2"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop 

@section('footerscript')
	@foreach( Config::get('dottedchart::config.jsfiles') as $value)
	<script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
	@endforeach
	<script type="text/javascript">
	var chartMinWidth = 600;
	var chartMinHeight = 400;
	var data = {
		component	: 'CASE',
		color		: 'ACTIVITY',
		shape		: 'TYPE',
		timeFormat	: 'MMM dd, yyyy HH mm ss',
		timeSplit	: 10,
	};
	var chart = {
		gapX		: 20,
		gapY		: 20,
		offsetX		: 0,
		offsetY		: 0,
		scaleX		: 100,
		scaleY		: 100,
		transpose	: false,
		birdview	: true,
		relative	: false,
		logical		: false,
		gridX		: true,
		gridY		: true,
		width		: chartMinWidth,
		height		: chartMinHeight,
		callWidth	: 0,
		callHeight	: 0,
		curWidth	: 0,
		curHeight	: 0,
		sorter		: 'earliestStart',
		timeFormat	: 'M d, y H:i:s',
	};
	var color = {};
	var dimIcons = {
		'CASE': 'fa fa-globe',
		'ACTIVITY': 'fa fa-envelope-o',
		'ORIGINATOR': 'fa fa-users',
		'RESOURCE': 'fa fa-gears',
	};

	function resizeChart() {
		chart.width 	= $('#id-chart-ruler').width();
		chart.height 	= $(window).height() 			- 300;
		chart.curWidth 	= chart.callWidth * ((200 - chart.scaleX) / 100);
		chart.curHeight	= chart.callHeight * ((200 - chart.scaleY) / 100);
		if (chart.width 	< chartMinWidth) 	chart.width 	= chartMinWidth;
		if (chart.height 	< chartMinHeight) 	chart.height 	= chartMinHeight;
		$('#id-dottedchart').width(chart.width);
		$('#id-dottedchart').height(chart.height);
		$('#dot-inspector-component').width($('#id-chart-ruler2').width());
		$('#dot-inspector-color').width($('#id-chart-ruler2').width());
		$('#dot-inspector-time').width($('#id-chart-ruler2').width());
		if (typeof $('#id-dottedchart').prop('viewBox') !== "undefined") {
			$('#id-dottedchart').prop('viewBox').baseVal.x = chart.offsetX / 1000 * chart.callWidth;
			$('#id-dottedchart').prop('viewBox').baseVal.y = chart.offsetY / 1000 * chart.callHeight;
			$('.text-top1').each(function(i) {
				$(this).attr('y', $('#id-dottedchart').prop('viewBox').baseVal.y + 29);
			});
			$('.text-top2').each(function(i) {
				$(this).attr('y', $('#id-dottedchart').prop('viewBox').baseVal.y + 17);
			});
			$('.text-left').each(function(i) {
				$(this).attr('x', $('#id-dottedchart').prop('viewBox').baseVal.x + 104);
			});
			$('.box-fix').each(function(i) {
				$(this).attr('x', $('#id-dottedchart').prop('viewBox').baseVal.x);
				$(this).attr('y', $('#id-dottedchart').prop('viewBox').baseVal.y);
			});
			$('#id-dottedchart').prop('viewBox').baseVal.width = chart.curWidth;
			$('#id-dottedchart').prop('viewBox').baseVal.height = chart.curHeight;
			if (chart.transpose) {
				$("#id-chart-scale-y-container").show();
				$("#id-chart-scale-x-container").hide();
			} else {
				$("#id-chart-scale-x-container").show();
				$("#id-chart-scale-y-container").hide();
			}
			if (chart.birdview) {
				$("#id-chart-scale-x-container").show();
				$("#id-chart-scale-y-container").show();
			}
		}
	}
	
	function reloadColor() {
		var colorPickers = $('#color-pickers').serializeArray();
		color = {};
		for (var cp = 0; cp < colorPickers.length; cp++) {
			var colorPicker = colorPickers[cp];
			color[colorPicker.name] = colorPicker.value;
		}
	}
	
	function redrawChart() {
		var params = {
			'data'		: data,
			'chart'		: chart,
			'color'		: color,
		};
		$('#id-chart-container').html('<div class="col-sm-12">Refreshing chart ...</div>');
		$.post(document.location, params, function (res) {
			if (res == 'wait') {
				$('#id-chart-container').html('<div class="col-sm-12">Submitting new server job ...</div>');
				setTimeout('redrawChart()', 5000);
				return;
			}
 			$('#id-chart-container').html(res);
			$('#paralel-dots-colors').toggle();
			$('.dot').on('mouseover', updateDotTooltip);
			$('.dot').on('mouseout', resetDotTooltip);
			resizeChart();
		});
	}
	
	function resetDotTooltip(ev) {
		$('#dot-inspector-time').html('Current TIME');
		$('#dot-inspector-component').html('Current COMPONENT');
		$('#dot-inspector-color').html('Current COLOR');
	}
	
	function updateDotTooltip(ev) {
		var dot = {
			time: $(this).attr('data-time'),
			component: $(this).attr('data-component'),
			shape: $(this).attr('data-shape'),
			color: $(this).attr('data-color'),
		}
		if (chart.transpose) {
			$('#dot-inspector-time').html(dot.component);
			$('#dot-inspector-component').html(dot.time);
		} else {
			$('#dot-inspector-time').html(dot.time);
			$('#dot-inspector-component').html(dot.component);
		}
		$('#dot-inspector-color').html(dot.color);
	}
	
	function toggleButton(btn, val) {
		if (val) {
			$(btn).addClass('active');
			$(btn).removeClass('btn-default');
			$(btn).addClass('btn-primary');
		} else {
			$(btn).removeClass('active');
			$(btn).removeClass('btn-primary');
			$(btn).addClass('btn-default');
		}
	}
	
	$(document).ready(function() {
		$("#id-chart-gap-x").slider({
			min: 20,
			max: 200,
			value: chart.gapX,
			tooltip: 'hide',
		});
		$("#id-chart-gap-y").slider({
			min: 20,
			max: 200,
			value: chart.gapY,
			tooltip: 'hide',
		});
		$("#id-chart-offset-x").slider({
			min: 0,
			max: 1000,
			value: chart.offsetX,
			tooltip: 'hide',
		});
		$("#id-chart-offset-y").slider({
			min: 0,
			max: 1000,
			value: chart.offsetY,
			tooltip: 'hide',
		});
		$("#id-chart-scale-x").slider({
			min: 1,
			max: 199,
			value: chart.scaleX,
			tooltip: 'hide',
		});
		$("#id-chart-scale-y").slider({
			min: 1,
			max: 199,
			value: chart.scaleY,
			tooltip: 'hide',
		});
		$("#id-chart-offset-x").on("slideStop", function(ev) {
			chart.offsetX = ev.value;
			resizeChart();
		});
		$("#id-chart-offset-y").on("slideStop", function(ev) {
			chart.offsetY = ev.value;
			resizeChart();
		});
		$("#id-chart-scale-x").on("slideStop", function(ev) {
			chart.scaleX = ev.value;
			resizeChart();
		});
		$("#id-chart-scale-y").on("slideStop", function(ev) {
			chart.scaleY = ev.value;
			resizeChart();
		});
		$("#id-chart-gap-x").on("slideStop", function(ev) {
			chart.gapX = ev.value;
			redrawChart();
		});
		$("#id-chart-gap-y").on("slideStop", function(ev) {
			chart.gapY = ev.value;
			redrawChart();
		});
		$("#id-chart-birdview-button").on("click", function(ev) {
			chart.birdview = chart.birdview ? false : true;
			toggleButton("#id-chart-birdview-button", chart.birdview);
			redrawChart();
		});
		$("#id-chart-transpose-button").on("click", function(ev) {
			chart.transpose = chart.transpose ? false : true;
			toggleButton("#id-chart-transpose-button", chart.transpose);
			redrawChart();
		});
		$("#id-chart-relative-button").on("click", function(ev) {
			chart.relative = chart.relative ? false : true;
			toggleButton("#id-chart-relative-button", chart.relative);
			redrawChart();
		});
		$("#id-chart-logical-button").on("click", function(ev) {
			chart.logical = chart.logical ? false : true;
			toggleButton("#id-chart-logical-button", chart.logical);
			redrawChart();
		});
		$("#id-chart-gridX-button").on("click", function(ev) {
			chart.gridX = chart.gridX ? false : true;
			toggleButton("#id-chart-gridX-button", chart.gridX);
			redrawChart();
		});
		$("#id-chart-gridY-button").on("click", function(ev) {
			chart.gridY = chart.gridY ? false : true;
			toggleButton("#id-chart-gridY-button", chart.gridY);
			redrawChart();
		});
		$("#id-chart-sorter").on("change", function(ev) {
			chart.sorter = "";
			$("#id-chart-sorter option:selected").each(function() {
				chart.sorter += $(this).val() + " ";
			});
			redrawChart();
		});
		$("#id-data-component").on("change", function(ev) {
			data.component = "";
			$("#id-data-component option:selected").each(function() {
				data.component += $(this).val() + " ";
			});
			$('#id-data-component-icon').html('<i class="' + dimIcons[data.component.trim()] + '"></i>');
			redrawChart();
		});
		$("#id-data-color").on("change", function(ev) {
			color = {};
			data.color = "";
			$("#id-data-color option:selected").each(function() {
				data.color += $(this).val() + " ";
			});
			$('#id-data-color-icon').html('<i class="' + dimIcons[data.color.trim()] + '"></i>');
			redrawChart();
		});
		$("#id-chart-timeformat").on("change", function(ev) {
			chart.timeFormat = "";
			data.timeFormat = "";
			$("#id-chart-timeformat option:selected").each(function() {
				chart.timeFormat += $(this).val() + " ";
				data.timeFormat += $(this).text() + " ";
			});
			redrawChart();
		});
		$(window).resize(resizeChart);
		$('.sidebar-toggle-box').click(resizeChart);
		resizeChart();
		redrawChart();
	});
</script>
@stop


