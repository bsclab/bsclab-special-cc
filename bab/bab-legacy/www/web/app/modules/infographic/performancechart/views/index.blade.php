@section ('headerscript')
@stop 

@section('maincontent')
<div id="performance-chart" class="row mt">
	<!-- Portlet PORTLET - Start-->
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>{{ $title }}</h3>
			</div>
			<div class="panel-body">
				
				<div class="row" id="charts"></div>
				<div class="row text-center">
					<a id="button" data-toggle="modal" data-target="#addChart1" class="btn btn-warning" name="submit">Add Chart</a>
				</div>
                <div class="row">
                    @include('performancechart::chartOption')
                </div>
            </div>
        </div>
    </div>
    @include('base::partial.modal-howtouse-chart')
</div>
@stop 

@section('footerscript')
	@include('performancechart::handlebars-template')

	<script type="text/javascript">
	
	var post = {{ $post }};
	var chartObject = {{ $chartObject }};
	var logObject = {{ $logObject }};

	pcModel = new PerformanceChartModel({
		logObject: logObject,
		chartObject: chartObject,
		post: post
	});

	pcView = new PerformanceChartView({
		el: '#performance-chart', 
		model: pcModel
	});

	if(!_.isEmpty(post) ){
		if(BabHelper.isWaitingJsonLoad(chartObject)){
	       	//BabHelper.setWaiting('#waiting-modal');
		}
		else{
			pcView.render();
			console.log(pcModel);
			console.log(pcView);
			// var log = logObject;
			// var data = chartObject;
			// var mapSeries = {
			// 	"Task": "activities",
			// 	"Originator": "originators"
			// };
			// var objSeriesList = log[mapSeries[post.series]];

			
	    }
	    
	}
	else{
		console.log('data not ready yet');
	}
	</script>
@stop

