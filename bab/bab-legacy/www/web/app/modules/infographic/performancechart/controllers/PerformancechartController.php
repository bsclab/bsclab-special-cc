<?php namespace App\Modules\Infographic\Performancechart\Controllers;

use View, Config, Breadcrumbs, BabHelper;

use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\General\API\Models\AnalysisAPIModel;
use App\Modules\General\API\Models\RepositoryAPIModel;
use App\Modules\Infographic\Performancechart\Models\PerformancechartModel;

class PerformancechartController extends BaseController {

    protected $model;

    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(PerformancechartModel $model, AnalysisAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('isCooming');
        $this->beforeFilter('hasAccess:pc');
        
        $this->model = $model;
        $this->APIModel = $APIModel;

        $title = 'Performance Chart ';
        $cssFiles = Config::get('performancechart::config.cssfiles');
        $jsFiles = Config::get('performancechart::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));

        $this->title = $title;
        
    }


    public function getIndex($resourceId)
    {
        $chartObject = '[]';
        $logObject = '[]';
        $chartUri = '';
        $post = '[]';        
        $this->layout->maincontent =  View::make('performancechart::index', compact('resourceId', 'logObject', 'chartUri', 'chartObject', 'post'));
    }

    public function postIndex($resourceId)
    {
        $view = new RepositoryAPIModel();
        
        
        if (isset($_POST['submit'])) {
            
            $opts = array(
                'http' => array(
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query(array(
                        'series' => $_POST['series'],
                        'y' => $_POST['y'],
                        'unit' => $_POST['unit'],
                        'calculation' => $_POST['calculation'],
                    ))
                )
            );

            $logObject = $view->getRepositoryStatistics($resourceId);
            $chartUri = Config::get('sitesetting.API.analysis.performancechart') . $resourceId;
            $chartObject = $this->APIModel->getPerformanceChart($resourceId, $opts);
            $post = json_encode($_POST);
            $this->trackJobJson($chartObject, null, true);
            $this->layout->maincontent =  View::make('performancechart::index', compact('resourceId', 'logObject', 'chartUri', 'chartObject', 'post'));
        }
    }

    
}