<script type="text/x-handlebars-template" id="small-chart-template">
	<div class="chart-panel col-sm-6">
		<h4>{{ title }}</h4>
		<div id="{{ id }}"></id>
	</div>
</script>

<script type="text/x-handlebars-template" id="big-chart-template">
	<div class="chart-panel col-sm-12">
		<h4>{{ title }}</h4>
		<div id="{{ id }}">
			{{#if isCanvas }}
			<canvas id="{{ id }}-canvas" width="{{canvasWidth}}" height="{{canvasHeight}}"></canvas>
			<div id="chartjs-tooltip"></div>
			{{/if }}
		</id>
	</div>
</script>