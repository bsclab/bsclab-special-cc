<?php namespace App\Modules\Infographic\Taskmatrix\Controllers;

use View, Config, Breadcrumbs, BabHelper, Response, Session;

use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\General\API\Models\AnalysisAPIModel;
use App\Modules\Infographic\Taskmatrix\Models\TaskmatrixModel;

class TaskmatrixController extends BaseController {
    protected $formData = NULL;
    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(TaskmatrixModel $model, AnalysisAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('isCooming');
        $this->beforeFilter('hasAccess:tm');
        
        $this->model = $model;
        $this->APIModel = $APIModel;

        $title = 'Task Matrix ';
        $cssFiles = Config::get('taskmatrix::config.cssfiles');
        $jsFiles = Config::get('taskmatrix::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));

        $this->title = $title;
        
        /* form data */
        $this->formData['entities'] = array(
            "event.activity+event.type" =>"Activity & Event Type",
            "event.activity"=>"Activity",
            "event.originator"=>"Originator",
            "event.resource"=>"Performers"
        );
        $this->formData['row'] = "event.activity+event.type";
        $this->formData['column'] = "event.activity+event.type";
        
        $this->formData['units'] = array(
            "frequency" =>"Frequency",
            "duration"=>"Duration"
        );
        $this->formData['unit'] = "frequency";
        
        $this->formData['orders'] = array(
            "0" =>"by Label",
            "1"=>"by Units",
            "2"=>"by Cluster"
        );
        $this->formData['order'] = "0";
    }


    public function getIndex($resourceId)
    {        
        $jsonData = array(
            'taskmatrix' => $this->APIModel->getTaskMatrix($resourceId)
        );
        
        $this->trackJobJson($jsonData['taskmatrix']);
        
        $formData = $this->formData;
        
        $this->layout->maincontent =  View::make('taskmatrix::index', compact('jsonData', 'resourceId', 'formData'));
    }

    public function postIndex($resourceId)
    {
        if (isset($_POST['update']))
        {
            $data['setting'] = $_POST;
			
			$tseries = array(
				'task' => 'activities',
				'originator' => 'originators',
			);

			$opts = array('http' =>
				array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query(array(
						'row' => $_POST['row'],
                        'column' => $_POST['column'],
                        'unit' => $_POST['unit'],
					)),
				)
			);
            $jsonData = array(
                'taskmatrix' => $this->APIModel->postTaskMatrix($resourceId, $opts)
            );
            
            //$this->trackJobJson($jsonData['taskmatrix'], 'infographic/taskmatrix/update/'.$resourceId.'/'.$_POST['row'].'/'.$_POST['column'].'/'.$_POST['unit'].'/'.$_POST['order']);
            $this->trackJobJson($jsonData['taskmatrix'], null, true);
			
            $this->formData['row'] = $_POST['row'];
            $this->formData['column'] = $_POST['column'];
            $this->formData['unit'] = $_POST['unit'];
            $this->formData['order'] = $_POST['order'];
            $formData = $this->formData;

            $this->layout->maincontent =  View::make('taskmatrix::index', compact('jsonData', 'resourceId', 'formData'));
        }
        else
        {			
        	return Redirect::action('infographic/taskmatrix/index/',$resourceId);
        }
    }
    
    public function getUpdate($resourceId, $row=null, $column=null, $unit=null, $order=null){
        $opts = array('http' =>
                      array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query(array(
                            'row' => $row,
                            'column' => $column,
                            'unit' => $unit,
                        )),
				    )
			     );
        $jsonData = array(
            'taskmatrix' => $this->APIModel->postTaskMatrix($resourceId, $opts)
        );

        $this->formData['row'] = $row;
        $this->formData['column'] = $column;
        $this->formData['unit'] = $unit;
        $this->formData['order'] = $order;
        $formData = $this->formData;

        $this->layout->maincontent =  View::make('taskmatrix::index', compact('jsonData', 'resourceId', 'formData'));
    }
    
    public function getDownloadCases($url, $params=null){
        list($from, $to, $fromId, $toId, $unit, $val, $ratio, $total) = explode(";", $params);
        
        $content = $from." => $to \r\n$unit : $val - Ratio : $ratio 
                    \r\nNumber of cases: $total \r\nCase IDs: \r\n";
        $ws = Session::get('workspaceId');
        $url = "bab/workspaces/".$ws."/".$url;
        $data = json_decode(file_get_contents( url('hdfs', $parameters = array(), $secure = null)."?url=".$url , null, null), true);
        
        foreach($data["caseMap"][$fromId."|".$toId] as $key=>$val)
            $content .= $val."\r\n";
        
        $headers = array(
            'Content-Type' => 'text/plain',
            'Content-Disposition' => 'attachment; filename="Case-list.txt"',
        );
        
        return Response::make(($content), '200', $headers);
    }
    
}
