@section('headerscript')
@stop

@section('maincontent')
	<div id="content" class="row mt">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>{{ $title }}</h3>
				</div>
				<div class="panel-body">
					<div id="content-graph" class="col-sm-10">
						@include('processmodel::snippet-graphtoolbox-simple')
					</div>
					<div class="col-md-2">
						<div id="transformer">							
							<div class="col-md-6">Edge Filter</div>
							<div class="col-md-6">
								<div id="edge-slider" style="height: 200px;"></div>
							</div>							
							<div id="edge-filter" class="col-md-6"></div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('timegap::slidefooter') 
@stop 

@section('footerscript')
@include('processmodel::handlebars-template')
<script type="text/javascript">
	var jsonData = {{ $jsonData['timegapData'] }};
	
	console.log(jsonData);
	if(BabHelper.isWaitingJsonLoad(jsonData)){
       	BabHelper.setWaiting('#waiting-modal');
	}
    else{
    	var pm = jsonData.transitions;
    	var table = null;

    	var model = new ProcessModelModel({
			jsonProcessModel: jsonData, 
			type: 'timegap'			
		});

		var theView = new ProcessModelView({
			el: '#content-graph',
			model : model
		});

		function test(){
			/*console.log("test");
			console.log(table.rows('.selected').data().length);*/
			var data = table.rows('.selected').data();
			var cases = [];
			for(var i=0; i< data.length; i++){
				console.log(data[i][1]);
				cases.push("Case_"+data[i][1]);
			}
			$('#astartActivity')[0].value = $('#select-node-source').val();
			$('#aendActivity')[0].value = $('#select-node-target').val();
			$('#caseIds')[0].value = JSON.stringify(cases);

			console.log($('#astartActivity')[0].value);
			console.log($('#aendActivity')[0].value);
			console.log($('#caseIds')[0].value);
		}

		function nama(caseid, source, target){
			document.getElementById('caseID').value = caseid;
			document.getElementById('startActivity').value = source;
			document.getElementById('endActivity').value = target;
		}

		function updateGraph(arcFilter) {
			var model = {{ $jsonData['timegapData'] }}
			
			// Filter arcs		
			var arcs = {};
			var arc_cluster_candidates = {};
			var nodes = {};
			var nodesAll = model.nodes;
			for (var arc in model.transitions) {
				var a = model.transitions[arc];
				if (a.meanRaw >= arcFilter) {
					arcs[arc] = a;
					if(!(a.source in nodes)) nodes[a.source] = nodesAll[a.source];
					if(!(a.target in nodes)) nodes[a.target] = nodesAll[a.target];
					console.log(arc);

				} else {
					var s = a.meanRaw;
					if (!(s in arc_cluster_candidates)) arc_cluster_candidates[s] = {};
					arc_cluster_candidates[s][arc] = a;
				}
			}
			// Cluster arcs
			var arc_clusters = {};
			var arc_cluster_index = 0;
			var map_arc_clusters = {};
			for (var c in arc_cluster_candidates) {
				var i = 0;
				var s = "";
				for (var d in arc_cluster_candidates[c]) {
					i++;
					s = d;
				}
				if (i > 1) {
					arc_clusters[c] = arc_cluster_candidates[c];
					arc_cluster_index++;
				}
			}
			model.transitions = arcs;
			model.nodes = nodes;
			//assign the model, automatic generate new graph by event binding in ProcessModelView
			theView.model.set({jsonProcessModel: model});
		}
		
		$(document).ready(function(){		

			var minSlider = jsonData.minGap;
			var maxSlider = jsonData.maxGap;

			$("#edge-slider").slider({
				orientation: "vertical",
				min: minSlider,
				max: maxSlider,
				range: "min",
				value: 1,
				tooltip: "hide"
			});

			$("#edge-slider").on("slideStop", function(ev) {			
				var seconds = (ev.value / 1000) % 60;
				var minutes = (ev.value / (1000 * 60)) % 60;
				var hours = (ev.value / (1000 * 60 * 60)) % 24;
				var days = ev.value / (1000 * 60 * 60 * 24);

				var day = "";
				if(days >= 1){
					day = Math.round(days)+"D ";
				}
				console.log(ev.value);
				var hr = (String(Math.round(hours)).length < 2) ? String("0" + Math.round(hours)) :  String(Math.round(hours));
				var mnt = (String(Math.round(minutes)).length < 2) ? String("0" + Math.round(minutes)) :  String(Math.round(minutes));
				var sec = (String(Math.round(seconds)).length < 2) ? String("0" + Math.round(seconds)) :  String(Math.round(seconds));
				//alert(day + hr + ":" + mnt + ":" + sec);
				$("#edge-filter").html(day+ hr + ":" + mnt + ":" + sec);
				updateGraph(ev.value);
			});

			console.log('this is pm');

			//set default value of #sCheckBox == 'unChecked'
			var tbody = d3.select('#SummaryTable tbody');
			tbody.selectAll('tr').remove();
			var tr;
			var i = 1;
			for(var a in pm){								
				if(pm[a].noOfSuccessiveCases > 0){
					//console.log("---- "+pm[a].source+" "+pm[a].target);
					tr = tbody.append('tr');
					tr.append('td').text(i);
					tr.append('td').text(pm[a].source);
					tr.append('td').text(pm[a].target);
					tr.append('td').text(pm[a].meanSuccessive);
					tr.append('td').text(pm[a].stdDevSuccessive);
					tr.append('td').text(pm[a].noOfSuccessiveCases);
					i++;
				}		
			}	
			console.log(i);
			$('#SummaryTable').DataTable();
			
			//onClick #sCheckBox		
			d3.select('#sCheckBox')
				.on('click', function(){
					var tbody = d3.select('#SummaryTable tbody');
					tbody.selectAll('tr').remove();
					var tr;
					var i=1;
					if($('#sCheckBox').is(':checked')){
						console.log("checked");	
						for(var a in pm){								
							if(pm[a].noOfSuccessiveCases > 0){
								//console.log("---- "+pm[a].source+" "+pm[a].target);
								tr = tbody.append('tr');
								tr.append('td').text(i);
								tr.append('td').text(pm[a].source);
								tr.append('td').text(pm[a].target);
								tr.append('td').text(pm[a].meanSuccessive);
								tr.append('td').text(pm[a].stdDevSuccessive);
								tr.append('td').text(pm[a].noOfSuccessiveCases);
								i++;
							}		
						}	
						console.log(i);
						$('#SummaryTable').DataTable();
					}else{
						console.log("unchecked");
						
						for(var a in pm){
							if(pm[a].noOfNonSuccessiveCases > 0){				
								//console.log("---- "+pm[a].source+" "+pm[a].target);	
								tr = tbody.append('tr');
								tr.append('td').text(i);
								tr.append('td').text(pm[a].source);
								tr.append('td').text(pm[a].target);
								tr.append('td').text(pm[a].meanNonSuccessive);
								tr.append('td').text(pm[a].stdDevNonSuccessive);
								tr.append('td').text(pm[a].noOfNonSuccessiveCases);
								i++;
							}
						}
						console.log(i);
						$('#SummaryTable').DataTable();					
					}				
				}
			);

			var to = [];
			var from = [];
			var nodes = jsonData.nodes;
			for(var a in nodes){
				to.push(nodes[a].label);
				from.push(nodes[a].label);			
			}

			to.sort();
			from.sort();

			var toSelect = d3.select('#select-node-source');
			to.forEach(function(value, key){
				toSelect.append('option').text(value);
			});

			var fromSelect = d3.select('#select-node-target');
			from.forEach(function(value, key){
				fromSelect.append('option').text(value);
			});

			if($('#dCheckBox').is(':checked')){
				console.log('checked in d');
			}else{
				console.log('unchecked in d');
			}
			d3.select('#update_detail')
				.on('click', function(){
					
					// var getToOption = document.getElementById('ToSelect');
					// var selectedToOption = getToOption.options[getToOption.selectedIndex].value;
					// var getFromOption = document.getElementById('FromSelect');
					// var selectedFromOption = getFromOption.options[getFromOption.selectedIndex].value;

					var selectedFromOption = $('#select-node-source').val();
					var selectedToOption = $('#select-node-target').val();

					var tbody = d3.select('#DetailTable tbody');
					tbody.selectAll('tr').remove();					
					
					for(var a in pm){
						
						if(pm[a].source == selectedFromOption && pm[a].target == selectedToOption ){
							var cases = null;
							if($('#dCheckBox').is(':checked')){
								console.log('checked in d');
								cases = pm[a].caseIDSuccessive;
							}else{
								console.log('unchecked in d');
								cases = pm[a].caseIDNonSuccessive;
							}
							
							var counter = 1;		
							for(b in cases){
								var tr = tbody.append('tr');
									tr.append('td').text(counter);
									tr.append('td').text(cases[b].label);
									tr.append('td').text(cases[b].gap);
								var caseid = cases[b].label;
								var button = $('<input></input>').attr({'type': 'submit', 'onClick' : 'nama(\''+caseid+'\', \'' + selectedFromOption + '\', \'' + selectedToOption + '\')'}).val('MTGA')
									;								
									button.appendTo(tr.append('td'));						
									counter++;
							}						
						}
					}
				table = $('#DetailTable').DataTable();
			});

			$('#DetailTable tbody').on('click', 'tr', function(){
				$(this).toggleClass('selected');
			});				
		});			
    }
</script>
@stop

