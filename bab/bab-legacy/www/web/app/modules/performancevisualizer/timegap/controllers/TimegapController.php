<?php namespace App\Modules\Performancevisualizer\Timegap\Controllers;

use View, Config, BabHelper ;
use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\Performancevisualizer\Timegap\Models\TimegapModel;
use App\Modules\General\API\Models\AnalysisAPIModel;
use App\Modules\General\API\Models\RepositoryAPIModel;

class TimegapController extends BaseController {

    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(TimegapModel $model, RepositoryAPIModel $repositoryAPIModel, AnalysisAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('isCooming');
        $this->beforeFilter('hasAccess:tg');
        
        $this->model = $model;
        $this->APIModel = $APIModel;
        $this->repositoryAPIModel = $repositoryAPIModel;

        $title = 'Timegap ';
        $cssFiles = Config::get('timegap::config.cssfiles');
        $jsFiles = Config::get('timegap::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));

        $this->title = $title;
    }


    public function getIndex($resourceId)
    {
        $jsonData = array(
            'timegapData' => $this->APIModel->getTimeGap($resourceId)
        );
		$this->trackJobJson($jsonData['timegapData']);
        /*var_dump($jsonData['timegapData']);
        die;*/
        $this->layout->maincontent = View::make('timegap::index', compact('jsonData', 'resourceId'));
    }

    public function postIndex($resourceId)
    {
        $startActivity = $_POST['astartActivity'];
        $endActivity = $_POST['aendActivity'];
		$_POST['caseIds'] = str_replace('&quot;', '"', $_POST['caseIds']);
        $caseId = json_decode($_POST['caseIds'], true);
		$_POST['caseIds'] = str_replace('"', '&quot;', $_POST['caseIds']);
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query(array(                   
                    'caseIds' => $caseId,                    
                    'startActivity' => $startActivity,
                    'endActivity' => $endActivity
                )),
            )
        ); 
        $jsonData = array(
            'timegapData' => $this->APIModel->postTimeGap($resourceId, $opts),
			'repository' => $this->repositoryAPIModel->getRepositoryStatistics($resourceId),
        );
		//debug($_POST);
		//debug(json_decode($jsonData['timegapData'], true));
        $this->trackJobJson($jsonData['timegapData'], null, true);
		
        $this->layout->maincontent = View::make('timegap::compare', compact('jsonData', 'resourceId'));
    }

    
}