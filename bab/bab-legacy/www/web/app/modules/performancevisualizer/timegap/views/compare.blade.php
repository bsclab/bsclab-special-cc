@section('headerscript')
<style type="text/css">
.vertex g div {
	background: #4DB8FF;
}
</style>
@stop

@section('maincontent')
	<div id="content" class="row mt">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>{{ $title }} (Filtered)</h3>
				</div>
				<div class="panel-body">
					<div class="col-sm-12">
						<h3>A. Events / Day</h3>
					</div>
					<div id="timeline-graph" class="col-sm-12">
						
					</div>
					<div class="col-sm-12">
						<h3>B. Top 10 Attribute Count</h3>
					</div>
					<div id="graph-toolbox" class="col-sm-12">
						<div class="panel panel-success">
							<div class="panel-heading">
								<div class="btn-group">
									<select id="dimension-selector" class="form-control">
										<option value="cases">Cases</option>
										<option value="activities" selected="selected">Event Class</option>
										<option value="activityTypes">Event Types</option>
										<option value="originators">Human Performer</option>
										<option value="resources">Non Human Performer</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<h3>Entire Log</h3>
					</div>
					<div class="col-sm-6">
						<h3>Selected Cases</h3>
					</div>
					<div id="whole-graph" class="col-sm-6">
						
					</div>
					<div id="filtered-graph" class="col-sm-6">
						
					</div>
					<div id="whole-table" class="col-sm-6">
						<table class="table table-striped">
							<thead class="cf">
								<tr>
									<th>Label</th>
									<th>Count</th>
									<th>Share</th>
								</tr>
							</thead>
							<tbody id="whole-table-body">
							</tbody>
						</table>
					</div>
					<div id="filtered-table" class="col-sm-6">
						<table class="table table-striped">
							<thead class="cf">
								<tr>
									<th>Label</th>
									<th>Count</th>
									<th>Share</th>
								</tr>
							</thead>
							<tbody id="filtered-table-body">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('processmodel::snippet-tooltipinfo')
@stop 

@section('footerscript')
<?php

$jsonRepository = json_decode($jsonData['repository'], true);

?>
<script type="text/javascript">
	var repository = {{ $jsonData['repository'] }};
	var jsonData = {{ $jsonData['timegapData'] }};
	
	Date.prototype.customFormat = function(formatString){
		var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
		var dateObject = this;
		YY = ((YYYY=dateObject.getFullYear())+"").slice(-2);
		MM = (M=dateObject.getMonth()+1)<10?('0'+M):M;
		MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
		DD = (D=dateObject.getDate())<10?('0'+D):D;
		DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getDay()]).substring(0,3);
		th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
		formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

		h=(hhh=dateObject.getHours());
		if (h==0) h=24;
		if (h>12) h-=12;
		hh = h<10?('0'+h):h;
		AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
		mm=(m=dateObject.getMinutes())<10?('0'+m):m;
		ss=(s=dateObject.getSeconds())<10?('0'+s):s;
		return formatString.replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
	}
	
	function compare(dimension) {
		var wholeData = repository[dimension];
		var filteredData = jsonData.repository[dimension];
		
		var whole = [];
		var filtered = [];
		
		var c = 0;
		var other = 0;
		var total = 0;
		for (var i in wholeData) {
			c++;
			if (c > 9) {
				other += wholeData[i];
			} else {
				whole[whole.length] = [i, wholeData[i]];
			}
			total += wholeData[i];
		}
		if (other > 0) {
			whole[whole.length] = ['Others', other];
		}
		for (var i in whole) {
			whole[i][2] = numeral(whole[i][1] / total).format('0%');
		}
		whole.sort(function(a, b) { return b[1] - a[1] });

		c = 0;
		other = 0;
		total = 0;
		for (var i in filteredData) {
			c++;
			if (c > 9) {
				other += filteredData[i];
			} else {
				filtered[filtered.length] = [i, filteredData[i]];
			}
			total += filteredData[i];
		}
		if (other > 0) {
			filtered[filtered.length] = ['Others', other];
		}
		for (var i in filtered) {
			filtered[i][2] = numeral(filtered[i][1] / total).format('0%');
		}
		filtered.sort(function(a, b) { return b[1] - a[1] });

		generatePieChart('#whole-graph', whole);
		generatePieChart('#filtered-graph', filtered);
		generateDataTable('#whole-table', whole);
		generateDataTable('#filtered-table', filtered);
	}
	
	function generatePieChart(graphId, columnData) {
		var graph = $(graphId);
		graph.html('');
    	var chart = c3.generate({
            bindto: graphId,
            size: {
                width: graph.width(), 
                height: 300
            },
            data: {
                type    : 'pie',
                columns : columnData,
                //colors  : theColors
            },
            legend: {
                position: 'bottom',
                show: true
            },
            pie:{
                label: {
                    format: function (value, ratio, id) {
                        return numeral(ratio).format('0%');
                    }
                }
            }
        });
	}
	
	function generateDataTable(tableId, columnData) {
		var tableBody = $(tableId + '-body');
		tableBody.html('');
		for (var i in columnData) {
			var row = columnData[i];
			tableBody.append('<tr><td>' + row[0] + '</td><td>' + row[1] + '</td><td>' + row[2] + '</td></tr>');
		}
	}
	
	function getTimeLabel(ms) {
		var date = new Date(ms);
		return date.customFormat("#DD# #MMM# #YYYY#");
	}
	
	if(BabHelper.isWaitingJsonLoad(jsonData)){
       	BabHelper.setWaiting('#waiting-modal');
	}
    else{
		var timelineWhole = jsonData.timelineWhole;
		var timelineFiltered = jsonData.timelineFiltered;
		var timelineData = [
			['Time'],
			['Whole Log'],
			['Selected Cases']
		];
		
		var divider = 24 * 3600 * 1000;
		for (var t in timelineWhole) {
			timelineData[0][timelineData[0].length] = getTimeLabel(t * divider);
			timelineData[1][timelineData[1].length] = timelineWhole[t];
			timelineData[2][timelineData[2].length] = timelineFiltered[t];
		}
		
		var timelineChart = c3.generate({
			bindto: '#timeline-graph',
            size: {
                width: $('#timeline-graph').width(), 
                height: 300
            },
			data: {
				columns: timelineData,
				type: 'bar',
				x: 'Time',
			},
			axis: {
				x: {
					type: 'categorized' // this is needed to load string x value
				}
			},
			grid: {
				y: {
					lines: [{value:0}]
				}
			}
		});
		
		compare('activities');
		$('#dimension-selector').bind('change', function() {
			compare(this.value);
		})
    }
</script>
@stop

