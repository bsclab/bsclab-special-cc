@extends('base::partial.slidefooter') 
@section('slidefootertitle')
<li class="active"><a href="#data-satisfied-cases" role="pill"
	data-toggle="pill"> <i class="fa fa-thumbs-up"></i><span> Satisfied Cases</span>
</a></li>
<li><a href="#data-unsatisfied-cases" role="pill" data-toggle="pill"> <i
		class="fa fa-thumbs-down"></i><span> Unsatisfied Cases</span>
</a></li>
@stop 

@section('slidefootercontent')
<div class="tab-pane active fade in" id="data-satisfied-cases">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel-body">
				<table class="table table-hover table-striped table-condensed cf"
					id="SummaryTable">
					<thead>
						<tr>
							<th>Case</th>
							<th>No of Events</th>
							<th>Duration</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="tab-pane fade in" id="data-unsatisfied-cases">
	<div class="row">
		<div class="col-sm-12">
			<div class="panel-body">
				<table class="table table-hover table-striped table-condensed cf"
					id="SummaryTable">
					<thead>
						<tr>
							<th>CaseId</th>
							<th>No of Events</th>
							<th>Duration</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
