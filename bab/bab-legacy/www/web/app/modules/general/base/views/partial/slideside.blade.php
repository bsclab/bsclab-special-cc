<div class="col-md-1 fixto">
	<section class="panel slide-panel slide-side bg-color-10-and-text">
		<header class="panel-heading bg-color-6-and-text">
			<div class="row">
				<div class="col-md-1">
					<a class="slide-opener panel-title" href="#"> <i
						class="fa fa-toggle-left slide-icon slide-icon-inactive"></i> <i
						class="fa fa-toggle-right slide-icon slide-icon-active"></i>
					</a>
				</div>
				<div class="col-md-10 slide-content">
					<a class="slide-opener panel-title" href="#">
						@yield('slidesidetitle')</a>
				</div>
			</div>
		</header>
		<div class="panel-body">
		@yield('slidesidecontent')
		</div>
		@yield('slidesidefooter')
	</section>
</div>