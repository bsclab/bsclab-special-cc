<!-- modal information how to use chart -->
<div class="modal fade" id="howtouse-barchart" tabindex="-1" role="dialog" aria-labelledby="howtouse-barchart" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    	<h4 class="modal-title" id="howtouse-barchart">How to control chart?</h4>
		  	</div>
		  	<div class="modal-body">
		    	<ul class="list-unstyled">
					<li><i class="fa fa-fw fa-info-circle"></i> Info: mouseover on chart</li>
				</ul>
		  	</div>
		</div>
	</div>
</div>

<div class="modal fade" id="howtouse-piechart" tabindex="-1" role="dialog" aria-labelledby="howtouse-piechart" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    	<h4 class="modal-title" id="howtouse-barchart">How to control chart?</h4>
		  	</div>
		  	<div class="modal-body">
		    	<ul class="list-unstyled">
					<li><i class="fa fa-fw fa-info-circle"></i> Info: mouseover on chart</li>
				</ul>
		  	</div>
		</div>
	</div>
</div>

<div class="modal fade" id="howtouse-linechart" tabindex="-1" role="dialog" aria-labelledby="howtouse-barchart" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
		  	<div class="modal-header">
		    	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    	<h4 class="modal-title" id="howtouse-barchart">How to control chart?</h4>
		  	</div>
		  	<div class="modal-body">
		    	<ul class="list-unstyled">
					<li><i class="fa fa-fw fa-info-circle"></i> Info: mouseover on chart</li>
					<li><i class="fa fa-fw fa-search-plus"></i> Zoom: click then drag vertically or horizontally</li>
					<li><i class="fa fa-fw fa-search-minus"></i> Restore: double click</li>
					<li><i class="fa fa-fw fa-arrows"></i> Pan: shift + click then drag</li>
				</ul>
		  	</div>
		</div>
	</div>
</div>