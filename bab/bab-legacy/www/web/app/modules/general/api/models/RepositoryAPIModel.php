<?php namespace App\Modules\General\API\Models;

use Config;

class RepositoryAPIModel extends \Eloquent {

	public function getWorkspaceList()
	{	
		return file_get_contents( Config::get('api::config.repository.workspacelist') );
	}

	public function getRepositoryList()
	{	
		return file_get_contents( Config::get('api::config.repository.repositorylist') );
	}

	public function getRepositoryDatasetList()
	{	
		return file_get_contents( Config::get('api::config.repository.repositorydatasetlist'));
	}

	public function getRepositoryDataset($resourceId)
	{	
		return file_get_contents( Config::get('api::config.repository.viewdataset').$resourceId );
	}

	public function deleteRepository($resourceId)
	{	
		return file_get_contents( Config::get('api::config.repository.delete').$resourceId );
	}

	public function getRepositoryListForWorkspace($workspaceId)
	{	
		return file_get_contents( Config::get('api::config.repository.repositorylist').$workspaceId );
	}
	
	public function getRepositoryDerivedFormDataset($resourceId)
	{	
		return file_get_contents( Config::get('api::config.repository.repositorydatasetderivationlist').$resourceId );
	}
	
	public function postMappingDataset($resourceId, $param=NULL)
	{
        if (!is_null($param))
            return file_get_contents( Config::get('api::config.repository.mapping').$resourceId, false, stream_context_create(array(
				'http' => array(
					'method' => 'POST',
					'header' => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query($param)
				)
			)));
        else
            return NULL;
	}

	public function getApromoreRepositoryList()
	{	
		return file_get_contents( Config::get('api::config.repository.repositorylist').'?ext=hbmodel' );
	}

	public function getImportMxmlGz()
	{	
		return file_get_contents( Config::get('api::config.repository.importmxmlgz') );
	}

	public function getImportMxml()
	{	
		return file_get_contents( Config::get('api::config.repository.importmxml') );
	}

	public function getImport()
	{	
		return file_get_contents( Config::get('api::config.repository.import') );
	}

	public function getRepositoryStatistics($resourceId)
	{	
		return file_get_contents( Config::get('api::config.repository.view').$resourceId );
	}

	public function getRepositorySummary($resourceId)
	{	
		return file_get_contents( Config::get('api::config.repository.summary').$resourceId );
	}

}