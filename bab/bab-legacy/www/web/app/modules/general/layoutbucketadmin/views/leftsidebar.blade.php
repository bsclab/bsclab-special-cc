<?php
$resourceId = null;
if (! is_null ( Session::get ( 'resourceId' ) )) {
	$resourceId = Session::get ( 'resourceId' );
}

if (is_null ( $resourceId )) {
	Menu::handler ( 'main', array (
			'id' => 'nav-accordion',
			'class' => 'sidebar-menu' 
	) )->add ( 'home/index', '<div class="icon-bab-1 icon-bab-dims">정식</div>
                        <span class="title-en title-home">Home</span>
                        <span class="title-kr"></span>' );
} else {
	$user = User::find ( Auth::id () );
	// gpv,pm,hm,fm,lr,
	// gpc,km,hc,
	// gtv,lc,ar,bn,sn,
	// gfv,tg,dl,
	// gig,tm,dc,pc
	$pvmenu = Menu::items ( 'process-visualizer' );
	if (authFeatures ( 'pm' ))
		$pvmenu->add ( 'processvisualizer/proximityminer/index/' . $resourceId, 'Proximity Miner' );
	if (authFeatures ( 'hm' ))
		$pvmenu->add ( 'processvisualizer/heuristicminer/index/' . $resourceId, 'Heuristic Miner' );
	if (authFeatures ( 'fm' ))
		$pvmenu->add ( 'processvisualizer/fuzzyminer/index/' . $resourceId, 'Fuzzy Miner' );
	if (authFeatures ( 'lr' ))
		$pvmenu->add ( 'processvisualizer/logreplay/index/' . $resourceId, 'Log Replay' );

	$pcmenu = Menu::items ( 'process-clustering' );
	if (authFeatures ( 'km' ))
		$pcmenu->add ( '/kmeans/index/' . $resourceId, 'K-Means Clustering' );
	if (authFeatures ( 'hc' ))
		$pcmenu->add ( '/hierarchical/index/' . $resourceId, 'Hierarchical Clustering' );
	
	$tvmenu = Menu::items ( 'pattern-visualizer' );
	if (authFeatures ( 'lc' ))
		$tvmenu->add ( 'patternvisualizer/logicchecker/index/' . $resourceId, 'Logic Checker' );
	if (authFeatures ( 'ar' ))
		$tvmenu->add ( 'patternvisualizer/associationrule/index/' . $resourceId, 'Association Rule' );
	if (authFeatures ( 'bn' ))
		$tvmenu->add ( 'patternvisualizer/bayesian/index/' . $resourceId, 'Bayesian Network' );
	if (authFeatures ( 'sn' ))
		$tvmenu->add ( 'patternvisualizer/socialnetwork/index/' . $resourceId, 'Social Network' );
	
	$fvmenu = Menu::items ( 'performance-visualizer' );
	if (authFeatures ( 'tg' ))
		$fvmenu->add ( 'performancevisualizer/timegap/index/' . $resourceId, 'Time Gap' );
	if (authFeatures ( 'dl' ))
		$fvmenu->add ( 'performancevisualizer/delta/index', 'Delta Analysis' );
	
	$igmenu = Menu::items ( 'infographic' );
	if (authFeatures ( 'tm' ))
		$igmenu->add ( 'taskmatrix/index/' . $resourceId, 'Task Matrix' );
	if (authFeatures ( 'dc' ))
		$igmenu->add ( 'dottedchart/index/' . $resourceId, 'Dotted Chart' );
	if (authFeatures ( 'pc' ))
		$igmenu->add ( 'performancechart/index/' . $resourceId, 'Performance Chart' );
	
	if ($user == null) {
		$menu = Menu::handler ( 'main', array (
				'id' => 'nav-accordion',
				'class' => 'sidebar-menu' 
		) )
		->add ( 'home/index', '<div class="icon-bab-1 icon-bab-dims">정식</div><span class="title-en title-home">Home</span><span class="title-kr"></span>' )
		->add ( 'dashboard/index/' . $resourceId, '<div class="icon-bab-2 icon-bab-dims">오곡밥</div><span class="title-en">Dashboard</span><span class="title-kr"></span>' );
	} else {
		$menu = Menu::handler ( 'main', array (
				'id' => 'nav-accordion',
				'class' => 'sidebar-menu' 
		) )
		->add ( 'home/index', '<div class="icon-bab-1 icon-bab-dims">정식</div><span class="title-en title-home">Home</span><span class="title-kr"></span>' )
		->add ( 'dashboard/index/' . $resourceId, '<div class="icon-bab-2 icon-bab-dims">오곡밥</div>
									<span class="title-en">Dashboard</span>
									<span class="title-kr"></span>' );
		if (authFeatures ( 'gpv' ))
			$menu->add ( '#', '<div class="icon-bab-3 icon-bab-dims">김밥</div>
									<span class="title-en">Process Visualizer</span> 
									<span class="title-kr"></span>', $pvmenu );
		if (authFeatures ( 'gpc' ))
			$menu->add ( '#', '<div class="icon-bab-4 icon-bab-dims dropdown-submenu">주먹밥</div>
									<span class="title-en">Process Clustering</span> 
									<span class="title-kr"></span>', $pcmenu );
		if (authFeatures ( 'gtv' ))
			$menu->add ( '#', '<div class="icon-bab-5 icon-bab-dims">볶음밥</div>
									<span class="title-en">Pattern Visualizer</span>
									<span class="title-kr"></span>', $tvmenu );
		if (authFeatures ( 'gfv' ))
			$menu->add ( '#', '<div class="icon-bab-6 icon-bab-dims">비빔밥</div>
									<span class="title-en">Performance Visualizer</span>
									<span class="title-kr"></span>', $fvmenu );
		if (authFeatures ( 'gig' ))
			$menu->add ( '#', '<div class="icon-bab-7 icon-bab-dims">쌈밥</div>
									<span class="title-en">Infographic</span>
									<span class="title-kr"></span>', $igmenu );
	}
}

Menu::handler ( 'main' )->getItemsAtDepth ( 0 )->map ( function ($item) {
	if ($item->hasChildren ()) {
		$item->addClass ( 'dropdown-submenu' );
		$item->getChildren ()->addClass ( 'dropdown-menu ' );
		
		// $item->getContent()->nest('');
	}
} );
?>

<!--sidebar start-->
<aside>
	<div id="sidebar" class="nav-collapse">
		<!-- sidebar menu start-->
		<div class="leftside-navigation">
            <?php echo Menu::handler('main')->render();?>
        </div>
		<!-- sidebar menu end-->
		<div class="copyright text-center">
			<div class="bsclogo"></div>
			<small> © {{ date("Y") }} PNU IE <a
				href="{{ Config::get('sitesetting.author.developer.url') }}"
				target="_blank">{{ Config::get('sitesetting.author.developer.name') }}</a>.
				<br />All Rights Reserved.
			</small>
		</div>
	</div>
</aside>
<!--sidebar end-->