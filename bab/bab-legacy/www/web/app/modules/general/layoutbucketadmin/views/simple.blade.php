<!DOCTYPE html>
<html>
	<head>
		<title>Sample of Process Model</title>
		<link rel="stylesheet" type="text/css" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		@section('headerscript')
            This is the master sidebar.
        @show
	</head>
	<body>
		<div class="container">
			<div id="main-content" class="row">
				<div class="col-md-12">
					@yield('maincontent')
				</div>
			</div>
		</div>
		
		@foreach( Config::get('layout::config.js') as $value)
	        <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
	    @endforeach
		
		@section('footerscript')
            This is footer.
        @show
		
	</body>
</html>