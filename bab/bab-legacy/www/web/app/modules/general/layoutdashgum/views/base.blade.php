<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="BAB">
    <link rel="shortcut icon" href="{{ babModuleAsset('general/base/assets/bab/images/favicon.ico') }}">

    <title>{{ $title }}</title>
    
    @foreach( Config::get('base::config.cssfiles') as $value)
        <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">
    @endforeach

    @foreach( Config::get('layoutdashgum::config.cssfiles') as $value)
        <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">
    @endforeach
    
    @foreach( $cssFiles as $value)
        <link rel="stylesheet" type="text/css" href="{{ babModuleAsset( $value) }}">
    @endforeach

    @section('headerscript')
    @show

</head>
<body>
	<section id="container">
        
        @include('layoutdashgum::headermenu')
        @include('layoutdashgum::leftsidebar')
        @include('base::partial.waitingmodal')      

        <!--main content start-->
        <section id="main-content">
	        <section class="wrapper">
                
                @yield('maincontent')

			</section>
		</section>
        <!--main content end-->
    	
    </section>
    
    @foreach( Config::get('base::config.jsfiles') as $value)
        <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
    @endforeach

    @if(!is_null(Config::get('layoutdashgum::config.jsfiles')))
        @foreach( Config::get('layoutdashgum::config.jsfiles') as $value)
            <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
        @endforeach
    @endif

    @foreach( $jsFiles as $value)
        <script type="text/javascript" src="{{ babModuleAsset( $value) }}"></script>
    @endforeach

    @section('footerscript')
    @show
    <script type="text/javascript">
        $(document).ready(function () {
			//BabHelper.setWaiting('#waiting-modal');
			
			$('[data-toggle="tooltip"]').tooltip();

            $('.showqtip-right[title]').qtip({
                position: {
                    my: 'center-left',  
                    at: 'center-right', 
                },
                style: {
                    classes: 'qtip-dark qtip-shadow'
                }
            });
            $('.showqtip-top[title]').qtip({
                position: {
                    my: 'bottom-center',  
                    at: 'center-center', 
                },  
                style: {
                    classes: 'qtip-dark qtip-shadow'
                }
            });
        });
    </script>
	
	<?php 
	
	$jobTrackingId = Session::get('bab.api.job.trackingId');
	$jobRedirectUri = Session::get('bab.api.job.redirectUri');
	$jobPostParams = Session::get('bab.api.job.postParams') == '' ? 0 : 1;
	Session::forget('bab.api.job.trackingId');
	Session::forget('bab.api.job.redirectUri');
	Session::forget('bab.api.job.postParams');
	
	if ($jobTrackingId != '') {
		$jobTrackingId = str_replace('=', '', $jobTrackingId);
	?>
		<form id="waiting-modal-form" method="post" action="{{$jobRedirectUri}}">
		<?php foreach ($_POST as $pkey => $pvalue) { ?>
			<?php if (is_array($pvalue)) { ?>
				<?php foreach ($pvalue as $pkey2 => $pvalue2) { ?>
				<input type="hidden" name="{{$pkey}}[{{$pkey2}}]" value="{{$pvalue2}}" />
				<?php } ?>
			<?php } else { ?>
		<input type="hidden" name="{{$pkey}}" value="{{$pvalue}}" />
			<?php } ?>
		<?php } ?>
		</form>
		<script type="text/javascript">
			BabHelper.setWaitingTrack('#waiting-modal', '{{$jobTrackingId}}', '{{$jobRedirectUri}}', {{$jobPostParams}});
		</script>
	<?php
	}
	?>
</body>
</html>