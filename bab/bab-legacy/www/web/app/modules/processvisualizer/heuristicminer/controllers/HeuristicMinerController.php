<?php namespace App\Modules\ProcessVisualizer\HeuristicMiner\Controllers;

use View, Config, BabHelper ;
use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\ProcessVisualizer\HeuristicMiner\Models\HeuristicMinerModel;
use App\Modules\General\API\Models\ModellingAPIModel;

class HeuristicMinerController extends BaseController {

    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(HeuristicMinerModel $model, ModellingAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('isCooming');
        $this->beforeFilter('hasAccess:hm');
        
        $this->model = $model;
        $this->APIModel = $APIModel;

        $title = 'Heuristic Miner ';
        $cssFiles = Config::get('heuristicminer::config.cssfiles');
        $jsFiles = Config::get('heuristicminer::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));

        $this->title = $title;
    }


    public function getIndex($resourceId)
    {           
        $jsonData = array(
            'heuristicMiner' => $this->APIModel->getHeuristicModel($resourceId)
        );
		
		$this->trackJobJson($jsonData['heuristicMiner']);
        
        $this->layout->maincontent = View::make('heuristicminer::index', compact('jsonData'));
    }

    public function postIndex($resourceId)
    {
        if (isset($_POST['submit'])) {
            $opts = array(
                'http' => array(
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query(array(
                        'threshold[dependency]' => $_POST['threshold']['dependency'],
                        'positiveObservation' => $_POST['positiveObservation'],
                        'option[allConnected]' => isset($_POST['option']['allConnected'])?'true':'false',
                    ))
                )
            );

            $jsonData = array(
                'heuristicMiner' => $this->APIModel->postHeuristicModel($resourceId, $opts)
            );
            $this->trackJobJson($jsonData['heuristicMiner'], null, true);
            $this->layout->maincontent =  View::make('heuristicminer::index', compact('jsonData'));
        }
    }

    
}