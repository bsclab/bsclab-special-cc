@section ('headerscript')
@stop 

@section('maincontent')	
	<div id="content" class="row mt">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>{{ $title }}</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div id="content-graph" class="col-md-10">
							@include('processmodel::snippet-graphtoolbox-simple')
						</div>
						<div class="col-md-2">
							@include('processmodel::snippet-basicprocessmodelinfo')
							<div id="post_output"></div>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('heuristicminer::slideside') 
@stop 

@section('footerscript')
@include('processmodel::handlebars-template')
<script type="text/javascript">
	
	var jsonData = {{ $jsonData['heuristicMiner'] }};
	if(BabHelper.isWaitingJsonLoad(jsonData)){
       	//BabHelper.setWaiting('#waiting-modal');
	}
    else{
		var model = new ProcessModelModel({
			jsonProcessModel: jsonData, 
			type: 'heuristic'			
		});

		var theView = new ProcessModelView({
			el: '#content-graph',
			model : model
		});
		
	}
</script>
@stop

