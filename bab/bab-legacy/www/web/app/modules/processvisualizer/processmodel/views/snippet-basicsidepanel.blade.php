<div class="panel slide-side-subpanel">
	<div class="row">
		<div class="col-sm-1">
			<a class="slide-opener panel-title" href="#"><i
				class="fa fa-sitemap slide-icon "></i></a>
		</div>
		<div class="col-sm-10 slide-content ">
			<a data-toggle="collapse" class="panel-title"
				href="#setting-side-render">Process Model Rendering</a>
			<div class="panel-collapse collapse" id="setting-side-render">
				{{ BootForm::openHorizontal(6,6) }}
				<div class="form-group">
					<label class="col-sm-6 control-label">Font Size</label>
					<div class="col-sm-6">
						<a class="btn btn-default" href="#" id="font-inc">A+</a> <a
							class="btn btn-default" href="#" id="font-dec">A-</a>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-6 control-label" for="">Arc Type</label>
					<div class="col-md-6">
							<?php $arcType = array("bundle"=>"Rounded","linear"=>"Linear","cardinal"=>"Linear Smooth","step-before"=>"Rectangular"); ?>
							{{ Form::select("arc-type", $arcType, array("id"=>"arc-type", "class"=>"asem")) }}
						</div>
				</div>
				<div class="form-group">
					<label class="col-sm-6 control-label" for="">Arc Direction</label>
					<div class="col-sm-6">
							<?php $arcDirection = array("TB"=>"Top to Bottom","LR"=>"Left to Right"); ?>
							{{ Form::select("arc-direction", $arcDirection, array("id"=>"arc-direction", "class"=>"asem")) }}
						</div>
				</div>
				{{ BootForm::close() }}
			</div>
		</div>
	</div>
</div>
<div class="panel slide-side-subpanel">
	<div class="row">
		<div class="col-sm-1">
			<a class="slide-opener panel-title" href="#"><i
				class="fa fa-search slide-icon "></i></a>
		</div>
		<div class="col-sm-10 slide-content">
			<a data-toggle="collapse" class="panel-title"
				href="#setting-side-search">Search Arc/Node</a>
			<div class="panel-collapse collapse" id="setting-side-search">
			{{ BootForm::openHorizontal(6,6) }} 
				{{ BootForm::text('Node Name', 'node_name')->placeholder('-') }} 
				{{ BootForm::text('NodeFrequency', 'node_frequency')->placeholder('-') }} 
				{{ BootForm::text('Arc Frequency', 'arc_frequency')->placeholder('-') }} 
				{{ BootForm::text('Arc Dependency', 'arc_dependency')->placeholder('-') }} 
			{{ BootForm::close() }}
			</div>
		</div>
	</div>
</div>