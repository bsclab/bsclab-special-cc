@extends('layout::slideside') 

@section('slidesidetitle') 
Setting Heuristic Miner 
@stop 

@section('slidesidecontent')
    @include('processmodel::snippet-basicsidepanel')

    <div class="panel slide-side-subpanel">
    	<div class="panel slide-side-subpanel">
    		<div class="row">
    			<div class="col-sm-1">
    				<a class="slide-opener panel-title" href="#"><i class="fa fa-flask slide-icon"></i></a>
    			</div>
    			<div class="col-sm-10 slide-content">
    				<a href="#setting-side-linkoptions" data-toggle="collapse" class="panel-title">Link Options</a>
    				<div class="panel-collapse collapse" id="setting-side-linkoptions">
						{{ BootForm::openHorizontal(6,6)->attribute('id','link_options') }}
							<?php $arrVal = array('Critical Pattern', 'Longest Execution Time'); ?>
			      			@foreach ($arrVal as $key => $value)
			      				{{ BootForm::checkbox($value, $key) }}
			      			@endforeach
			      			{{ Form::submit('Submit', array('name'=> 'link_options_submit', 'class'=>'btn btn-info')) }}
			  			{{ BootForm::close() }}
					</div>
    			</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-sm-1">
    			<a class="slide-opener panel-title" href="#"><i class="fa fa-gears slide-icon"></i></a>
    		</div>
    		<div class="col-sm-10 slide-content ">
    			<a href="#setting-side-miningoptions" data-toggle="collapse"class="panel-title">Mining Options</a>
    			<div class="panel-collapse collapse option-slider-container" id="setting-side-miningoptions">
    					{{ BootForm::open()->attribute('id','mining_options') }}
        					<?php
            
                            $arrVal = array(
                                'threshold[relativeToBest]' => array('Relative-to-Best Threshold',0,1,0.01,0.05),
                                'threshold[l1Loop]' => array(
                                    'Length-one-loop Threshold',
                                    0,
                                    1,
                                    0.01,
                                    0.9
                                ),
                                'threshold[l2Loop]' => array(
                                    'Length-two-loops Threshold',
                                    0,
                                    1,
                                    0.01,
                                    0.9
                                ),
                                'threshold[longDistance]' => array(
                                    'Long Distance Threshold',
                                    0,
                                    1,
                                    0.01,
                                    0.9
                                ),
                                'threshold[and]' => array(
                                    'AND threshold',
                                    0,
                                    1,
                                    0.01,
                                    0.1
                                ),
                                'dependencyDivisor' => array(
                                    'Dependency Divisor',
                                    1,
                                    10,
                                    1,
                                    1
                                ),
                                'positiveObservation' => array(
                                    'Positive Observations',
                                    1,
                                    100,
                                    1,
                                    10
                                )
                            );
            
                            $arrVal2 = array(
                                'option[extra]' => 'Extra info',
                                'option[allConnected]' => 'Use all activities-connected-heuristic',
                                'option[longDistance]' => 'Use long distance dependency heuristics'
                            );
                            ?>
    		      			@foreach ($arrVal as $key => $value)
    		      			{{ BootForm::text($value[0], $key)
    										->addClass('option-slider')
    										->addClass('required')
    										->attribute('value', $value[4])
    										->attribute('data-slider-min', $value[1])
    										->attribute('data-slider-max', $value[2])
    										->attribute('data-slider-step', $value[3])
    										->attribute('data-slider-value', $value[4]); }}
    						
    		      			@endforeach
    						
    		      			@foreach ($arrVal2 as $key => $value)
    		      				{{ BootForm::checkbox($value, $key)->value('true') }}
    		      			@endforeach
    		      			{{ Form::submit('Submit', array('name'=> 'mining_options_submit', 'class'=>'btn btn-info')) }}
    		  			{{ BootForm::close() }}
    				</div>
    		</div>
    	</div>
    </div>
@stop
