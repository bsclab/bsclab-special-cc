<?php namespace App\Modules\ProcessVisualizer\FuzzyMiner\Controllers;

use View, Config, Breadcrumbs, BabHelper, Request ;

use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\ProcessVisualizer\FuzzyMiner\Models\FuzzyMinerModel;
use App\Modules\General\API\Models\ModellingAPIModel;

class FuzzyMinerController extends BaseController {

    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(FuzzyMinerModel $model, ModellingAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('isCooming');
        $this->beforeFilter('hasAccess:fm');
        
        $this->model = $model;
        $this->APIModel = $APIModel;

        $title = 'Fuzzy Miner ';
        $cssFiles = Config::get('fuzzyminer::config.cssfiles');
        $jsFiles = Config::get('fuzzyminer::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));

        $this->title = $title;
    }


    public function getIndex($resourceId)
    {        
        $jsonData = array(
            'fuzzyMiner' => $this->APIModel->getFuzzyModel($resourceId)
        );
		
		$this->trackJobJson($jsonData['fuzzyMiner']);
        
        $this->layout->maincontent = View::make('fuzzyminer::index', compact('jsonData'));
    }

    public function postIndex()
    {
        
    }

    public function getTest()
    {
        return 'asem';
    }

    
}