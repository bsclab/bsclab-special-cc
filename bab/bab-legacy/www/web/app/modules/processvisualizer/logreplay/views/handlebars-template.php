<script type="text/x-handlebars-template" id="table-row-template">
	<td class="text-center">{{ part }}</td>
	<td class="text-center"><span class="{{ class }}">{{ state }}</span></td>
	<td>{{ startDateFormatted }}<br/>{{startTimeFormatted}}</td>
	<td>{{ completeDateFormatted }}<br/>{{completeTimeFormatted}}</td>
	<td colspan="2">
		<div class="progress">
			<div class="progress-bar progress-bar-success" style="width: {{ eventsProjectedPercent }}%">
				{{ eventsProjected }}
			</div>
			<div class="progress-bar progress-bar-warning" style="width: {{ eventsNotProjectedPercent }}%">
				{{ eventsNotProjected }}
			</div>
		</div>
	</td>
	<td class="text-right"><span class="badge">{{ loadingTime }}</span></td>
</script>

<script type="text/x-handlebars-template" id="progressbar-stacked-template">
<div class="progress">
	<div class="progress-bar progress-bar-success" style="width: {{ value1Percent }}%">
		{{ value1 }}
	</div>
	<div class="progress-bar progress-bar-danger" style="width: {{ value2Percent }}%">
		{{ value2 }}
	</div>
</div>
</script>

<script type="text/x-handlebars-template" id="progressbar-minmax-template">
<h5>
	<span>0</span>
	<span class="pull-right">{{valueMax}}</span>
</h5>
</script>

<script type="text/x-handlebars-template" id="progressbar-template">

  <div class="progress-bar progress-bar-striped active" role="progressbar" style="min-width: 2em; width: {{ percentValue }}%;">
    {{ value }}
  </div>

</script>

<script type="text/x-handlebars-template" id="attribute-template">
	<input type="text" class="hidden" style="width:50px;"/>
	<!--<span class="label {{ class }}">--</span> -->
	<div>
		{{value}} 
		<span class="pull-right checked {{#unless selected}}hidden{{/unless}}">
			<i class="fa fa-check"></i>
		</span>
	</div>
</script>

<script type="text/x-handlebars-template" id="css-template">
	.{{ class }}{ background: {{ color }};  }
	.{{ class }} circle { fill: {{ color }}; opacity:{{opacity}} }
</script>
<script type="text/x-handlebars-template" id="tooltip-token-template">
	<table class="table table-border">
		<tr>
			<td>Case ID</td>
			<td><span class="case-{{ caseId }} pull-left case-color"></span> {{ caseId }}</td>
		</tr>
		<tr>
			<td>Start</td>
			<td>{{ start }}</td>
		</tr>
		<tr>
			<td>Complete</td>
			<td>{{ complete }}</td>
		</tr>
		<tr>
			<td>Source</td>
			<td>{{ nodeSource }}</td>
		</tr>
		<tr>
			<td>Destination</td>
			<td>{{ nodeTarget }}</td>
		</tr>
	</table>	
</script>
