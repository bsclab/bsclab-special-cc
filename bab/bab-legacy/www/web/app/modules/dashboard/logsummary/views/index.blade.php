@section ('headerscript')
@stop 

@section('maincontent')
<div id="log-summary" class="row mt">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3>{{ $title }}</h3>
			</div>
			<div id="tabs-summary" class="panel-body">
				<header class="log-summary-tab tab-bg-dark-navy-blue">
					<ul id="tabs-summary-nav" class="nav nav-tabs">
					</ul>
				</header>
				<!-- log summary tab  -->
				<div id="tabs-summary-content" class="tab-content">
				</div>
				<!-- end of log summary tab -->
			</div>
		</div>
	</div>
	@include('base::partial.modal-howtouse-chart')
</div>
@stop 

@section('footerscript')

	@include('logsummary::handlebars-template') 

	<script type="text/javascript">
		var jsonData = {{ $jsonData['jsonSummary'] }};

		if(BabHelper.isWaitingJsonLoad(jsonData)){
	       	//BabHelper.setWaiting('#waiting-modal');
		}
	    else{
			var jsonSummary = {{ $jsonData['jsonSummary'] }};
	        var jsonKeyInfo = {{ $jsonData['jsonKeyInfo'] }};
	        
		    var tabs = [
				{
					id : 'overview',
					label : 'Overview',
					description: 'this is description',
					icon : 'fa fa-globe',
					keyInfos : [
						{
							key : numeral(jsonKeyInfo.noOfCases).format('0,0'),
							label : 'Cases',
							description: 'this is description'
						},
						{
							key : numeral(jsonKeyInfo.noOfEvents).format('0,0'),
							label : 'Events',
							description: 'this is description'
						},
						{
							key : numeral(jsonKeyInfo.noOfActivities).format('0,0'),
							label : 'Event Class',
							description: 'this is description'
						},
						{
							key : numeral(jsonKeyInfo.noOfActivityTypes).format('0,0'),
							label : 'Event Types',
							description: 'this is description'
						},
						{
							key : numeral(jsonKeyInfo.noOfOriginators).format('0,0'),
							label : 'Human Performer',
							description: 'this is description'
						},
						{
							key : numeral(jsonKeyInfo.noOfResourceClasses).format('0,0'),
							label : 'Resources',
							description: 'this is description'
						} 
					],
					table : {
						id: 'table-overview',
						data : jsonSummary.caseTab.cases,
						label : 'Table of Cases',
						columns : [ 
							{
								key : 'caseId',
								label : 'Case ID'
							}, 
							{
								key : 'durationStr',
								label : 'Duration'
							}, 
							{
								key : 'noOfEvents',
								label : 'No of Events'
							}, 
							{
								key : 'startedStr',
								label : 'Start'
							}, 
							{
								key : 'finishedStr',
								label : 'Finished'
							} 
						]
					},
					charts : [
						{
							id : 'chart-overview-eventpercase',
							label : 'Event Per Case',
							description: 'this is description',
							type : 'barchart',
							series: [ jsonSummary.caseTab.eventPerCase ],
							chartParam: {
								axis:{
									x:{
										label: 'event',	
										type: 'indexed'
									}, 
									y: {
										label: 'number of case',
										type: 'indexed'
									}
								}																
							}
						},
						{
							id : 'chart-timeline-caseovertime',
							label : 'Active Case Over Time',
							description: 'this is description',
							type : 'linechart',
							series : [ jsonSummary.timelineTab.activeCasesOverTime ],
							chartParam: {
								axis: {
									x:{
										label: 'case over time',
										type: 'timeseries'
									}, 
									y:{
										label: 'number of case',
										type: 'indexed'
									}
								}
							}
						},
						{
							id : 'chart-overview-case-duration',
							label : 'Case Working Duration',
							description: 'this is description',
							type : 'linechart',
							series : [ jsonSummary.caseTab.caseDurations ],
							chartParam: {
								axis: {
									x:{
										label: 'duration',
										type: 'duration'
									}, 
									y: {
										label: 'working case',
										type: 'indexed'
									}
								}									
							}
						},
						{
							id : 'chart-overview-meanactivityduration',
							label : 'Mean Activity Duration',
							description: 'this is description',
							type : 'linechart',
							series : [ jsonSummary.caseTab.meanActivityDurations ],
							chartParam: {
								axis : {
									x: {
										label: 'duration',
										type: 'duration'
									},
									y: {
										label: 'mean activity',
										type: 'indexed'
									}
								},
							}
						},
						{
							id : 'chart-overview-meanwaitingtime',
							label : 'Mean Waiting Time',
							description: 'this is description',
							type : 'linechart',
							series : [ jsonSummary.caseTab.meanWaitingTimes ],
							chartParam:{
								axis : {
									x: {
										label: 'duration',
										type: 'duration'
									}, 
									y: {
										label: 'mean waiting',
										type: 'indexed'
									}
								}
							}
						} 
					]
				},
				{
					id : 'timeline',
					label : 'Timeline',
					description: 'this is description',
					icon : 'fa fa-clock-o',
					keyInfos : [ 
						{
							key : numeral(jsonKeyInfo.noOfCases).format('0,0'),
							label : 'Cases',
							description: 'this is description',
						}, 
						{
							key : numeral(jsonKeyInfo.noOfEvents).format('0,0'),
							label : 'Events',
							description: 'this is description',
						}, 
					],
					table : {
						id: 'table-timeline',
						data : jsonSummary.caseTab.cases,
						label : 'Table of Cases',
						columns : [ 
							{
								key : 'caseId',
								label : 'Case ID'
							}, 
							{
								key : 'durationStr',
								label : 'Duration'
							}, 
							{
								key : 'noOfEvents',
								label : 'No of Events'
							}, 
							{
								key : 'startedStr',
								label : 'Start'
							}, 
							{
								key : 'finishedStr',
								label : 'Finished'
							} 
						]
					},
					charts : [
						{
							id : 'chart-timeline-casestartovertime',
							label : 'Case Start Over Time',
							description: 'this is description',
							type : 'linechart',
							series : [ jsonSummary.timelineTab.caseStartOverTime ],
							chartParam:{
								axis: {
									x: {
										label: 'time',
										type: 'timeseries'
									}, 
									y: {
										label: 'number of cases',
										type: 'indexed'
									}
								}
							}
						},
						{
							id : 'chart-timeline-casecompleteovertime',
							label : 'Case Complete Over Time',
							description: 'this is description',
							type : 'linechart',
							series : [ jsonSummary.timelineTab.caseCompleteOverTime ],
							chartParam:{
								axis: {
									x: {
										label: 'time',
										type: 'timeseries'
									}, 
									y: {
										label: 'number of cases',
										type: 'indexed'
									}
								}
							}
						},
						// {
						// 	id : 'chart-timeline-casestartcompleteovertime',
						// 	label : 'Case Start Complete Over Time',
						// 	description: 'this is description',
						// 	type : 'linechart',
						// 	series : [ jsonSummary.timelineTab.caseStartOverTime, jsonSummary.timelineTab.caseCompleteOverTime ],
						// 	labelSeries: ['case start', 'case complete'],
						// 	chartParam:{
						// 		axis: {
						// 			x: {
						// 				label: 'time',
						// 				type: 'timeseries'
						// 			}, 
						// 			y: {
						// 				label: 'number of cases',
						// 				type: 'indexed'
						// 			}
						// 		}
						// 	}
						// },
						{
							id : 'chart-timeline-eventsovertime',
							label : 'Events Over Time',
							description: 'this is description',
							type : 'linechart',
							series : [ jsonSummary.timelineTab.eventsOverTime ],
							chartParam:{
								axis: {
									x: {
										label: 'time',
										type: 'timeseries'
									}, 
									y: {
										label: 'number of events',
										type: 'indexed'
									}
								}
							}
						} 
					]
				},
				{
					id : 'eventclass',
					label : 'Event Class',
					description: 'this is description',
					icon : 'fa fa-envelope-o',
					keyInfos : [
						{
							key : numeral(jsonKeyInfo.noOfActivities).format('0,0'),
							label : 'Event Class',
							description: 'this is description',
						},
						{
							key :  numeral(math.min(_.values(jsonKeyInfo.activities))).format('0,0'),
							label : 'Min Frequency',
							description: 'this is description',
						},
						{
							key : numeral(math.mean(_.values(jsonKeyInfo.activities))).format('0,0'),
							label : 'Mean Frequency',
							description: 'this is description',
						},
						{
							key : numeral(math.max(_.values(jsonKeyInfo.activities))).format('0,0'),
							label : 'Max Frequency',
							description: 'this is description',
						},
						{
							key : numeral(math.std(_.values(jsonKeyInfo.activities))).format('0,0'),
							label : 'Standard Deviation',
							description: 'this is description',
						} 
					],
					table : {
						id: 'table-eventclass',
						data : jsonSummary.activityTab.states,
						label : 'Table of Event Class',
						columns : [ {
							key : 'label',
							label : 'Event Class'
						}, {
							key : 'durationRangeStr',
							label : 'Duration Range'
						}, {
							key : 'frequency',
							label : 'Frequency'
						}, {
							key : 'minDurationStr',
							label : 'Min Duration'
						}, {
							key : 'maxDurationStr',
							label : 'Max Duration'
						}, {
							key : 'meanDurationStr',
							label : 'Mean Duration'
						}, {
							key : 'sumDurationStr',
							label : 'Sum Duration'
						} ]
					},
					charts : [
						{
							id : 'chart-eventclass-frequency',
							label : 'Frequency',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.activityTab.frequency ],
							chartParam: {
								axis: {
									x: {
										label: 'event class', 
										type: 'category'
									}, 
									y: {
										label: 'frequency',
										type: 'indexed'
									}
								}
							}
						},
						{
							id : 'chart-eventclass-meanduration',
							label : 'Mean Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.activityTab.meanDuration ],
							chartParam: {
								axis: {
									x: {
										label : 'event class',
										type : 'category'
									}, 
									y: {
										label: 'duration',
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-eventclass-medianduration',
							label : 'Median Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.activityTab.medianDuration ],
							chartParam: {
								axis: {
									x: {
										label : 'event class',
										type : 'category'
									}, 
									y: {
										label: 'duration',
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-eventclass-durationrange',
							label : 'Duration Range',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.activityTab.durationRange ],
							chartParam: {
								axis: {
									x: {
										label : 'event class',
										type : 'category'
									}, 
									y: {
										label: 'duration',
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-eventclass-aggregateduration',
							label : 'Aggregate Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.activityTab.aggregateDuration ],
							chartParam: {
								axis: {
									x: {
										label : 'event class',
										type : 'category'
									}, 
									y: {
										label: 'duration',
										type: 'duration'
									}
								}
							}
						} 
					]
				},
				{
					id : 'originator',
					label : 'Human Performer',
					description: 'this is description',
					icon : 'fa fa-users',
					keyInfos : [
						{
							key : numeral(jsonKeyInfo.noOfOriginators).format('0,0'),
							description: 'this is description',
							label : 'Human Performer'
						},
						{
							key : numeral(math.min(_.values(jsonKeyInfo.originators))).format('0,0'),
							description: 'this is description',
							label : 'Min Frequency'
						},
						{
							key : numeral(math.mean(_.values(jsonKeyInfo.originators))).format('0,0'),
							description: 'this is description',
							label : 'Mean Frequency'
						},
						{
							key : numeral(math.max(_.values(jsonKeyInfo.originators))).format('0,0'),
							description: 'this is description',
							label : 'Max Frequency'
						},
						{
							key : numeral(math.std(_.values(jsonKeyInfo.originators))).format('0,0'),
							description: 'this is description',
							label : 'Standard Deviation'
						} 
					],
					table : {
						id: 'table-originator',
						label : 'Table of Human Performer',
						data : jsonSummary.originatorTab.states,
						columns : [ 
							{
								key : 'label',
								label : 'Human Performer'
							}, 
							{
								key : 'frequency',
								label : 'Frequency'
							}, 
							{
								key : 'relativeFrequency',
								label : 'Relative Frequency',
							}, 
							{
								key : 'maxDurationStr',
								label : 'Max Duration',
							}, 
							{
								key : 'meanDurationStr',
								label : 'Mean Duration',
							}, 
							{
								key : 'durationRangeStr',
								label : 'Duration Range'
							} 
						]
					},
					charts : [
						{
							id : 'chart-originator-frequency',
							label : 'Frequency',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.originatorTab.frequency ],
							chartParam: {
								axis: {
									x: {
										label: 'human performer',
										type: 'category'										
									}, 
									y: {
										label: 'frequency',
										type: 'indexed'

									}
								}
							}
						},
						{
							id : 'chart-originator-meanduration',
							label : 'Mean Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.originatorTab.meanDuration ],
							chartParam: {
								axis: {
									x: {
										label: 'human performer',
										type: 'category'										
									}, 
									y: {
										label: 'duration',
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-originator-medianduration',
							label : 'Median Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.originatorTab.medianDuration ],
							chartParam: {
								axis: {
									x: {
										label: 'human performer',
										type: 'category'										
									}, 
									y: {
										label: 'duration',
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-originator-durationrange',
							label : 'Duration Range',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.originatorTab.durationRange ],
							chartParam: {
								axis: {
									x: {
										label: 'human performer',
										type: 'category'										
									}, 
									y: {
										label: 'duration',
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-originator-aggregateduration',
							label : 'Aggregate Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.originatorTab.aggregateDuration ],
							chartParam: {
								axis: {
									x: {
										label: 'human performer',
										type: 'category'										
									}, 
									y: {
										label: 'duration',
										type: 'duration'
									}
								}
							}
							
						} 
					]
				},
				{
					id : 'resources',
					label : 'Non Human Performer',
					description: 'this is description',
					icon : 'fa fa-gears',
					keyInfos : [
						{
							key : numeral(jsonKeyInfo.noOfResourceClasses).format('0,0'),
							label : 'Non Human Performer',
							description: 'this is description',
						},
						{
							key : numeral(math.min(_.values(jsonKeyInfo.resources))).format('0,0'),
							label : 'Min Frequency',
							description: 'this is description',
						},
						{
							key : numeral(math.mean(_.values(jsonKeyInfo.resources))).format('0,0'),
							label : 'Mean Frequency',
							description: 'this is description',
						},
						{
							key : numeral(math.max(_.values(jsonKeyInfo.resources))).format('0,0'),
							label : 'Max Frequency',
							description: 'this is description',
						},
						{
							key : numeral(math.std(_.values(jsonKeyInfo.resources))),
							label : 'Standard Deviation',
							description: 'this is description',
						} 
					],
					table : {
						id: 'table-resources',
						data : jsonSummary.resourceTab.states,
						label : 'Table of Resources',
						columns : [ 
							{
								key : 'label',
								label : 'Non Human Performer'
							}, 
							{
								key : 'frequency',
								label : 'Frequency'
							}, 
							{
								key : 'relativeFrequency',
								label : 'Relative Frequency',
							}, 
							{
								key : 'maxDurationStr',
								label : 'Max Duration',
							}, 
							{
								key : 'meanDurationStr',
								label : 'Mean Duration',
							}, 
							{
								key : 'durationRangeStr',
								label : 'Duration Range'
							} 
						]
					},
					charts : [
						{
							id : 'chart-resource-frequency',
							label : 'Frequency',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.resourceTab.frequency ],
							chartParam: {
								axis: {
									x: {
										label : 'non human performer', 
										type : 'category'
									}, 
									y: {
										label : 'frequency', 
										type: 'indexed'
									}
								}
							}
						},
						{
							id : 'chart-resource-meanduration',
							label : 'Mean Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.resourceTab.meanDuration ],
							chartParam: {
								axis: {
									x: {
										label : 'non human performer', 
										type : 'category'
									}, 
									y: {
										label : 'duration',
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-resource-medianduration',
							label : 'Median Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.resourceTab.medianDuration ],
							chartParam: {
								axis: {
									x: {
										label : 'non human performer', 
										type : 'category'
									}, 
									y: {
										label : 'duration', 
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-resource-durationrange',
							label : 'Duration Range',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.resourceTab.durationRange ],
							chartParam: {
								axis: {
									x: {
										label : 'non human performer', 
										type : 'category'
									}, 
									y: {
										label : 'duration', 
										type: 'duration'
									}
								}
							}
						},
						{
							id : 'chart-resource-aggregateduration',
							label : 'Aggregate Duration',
							description: 'this is description',
							type : 'piechart',
							series : [ jsonSummary.resourceTab.aggregateDuration ],
							chartParam: {
								axis: {
									x: {
										label : 'non human performer', 
										type : 'category'
									}, 
									y: {
										label : 'duration',
										type: 'duration'
									}
								}
							}							
						} 
					]
				}, 
			];
			var Model = Backbone.Model.extend();
			var model = new Model({
		    	jsonSummary 	: jsonSummary, 
		    	jsonKeyInfo 	: jsonKeyInfo, 
		    	tabs 			: tabs, 
		    });

		    var dashboard = new DashboardTabView({
		    	el: '#tabs-summary',
		        model: model,
		    	labelTemplateId : '#label-template',
		    	contentTemplateId: '#content-template'
		    });
		}
	</script>
@stop

