@section('headerscript')
@stop

@section('maincontent')
<div class="row mt"></div>
<div class="row mt">
	<div class="col-md-6 col-md-offset-3 mb">
		<div class="instagram-panel pn">
            <i class="fa fa-spinner fa-5x"></i>
            <p style="font-size:1.25em">Coming Soon.</p>
            <p style="font-size:0.9em">The {{ $feature_name }} feature is not available yet.</p>
        </div>
	</div>
</div>
@stop 

@section('footerscript')

@stop

