@section('headerscript')
@stop

@section('maincontent')
<div id="home" class="row mt">
	<div class="col-sm-4">
		<section id="panel-recentlog" class="panel panel-default panel-file-list">
			<header class="panel-heading">
				<h5>
                    <i class="fa fa fa-history"></i> Log Repositories 
                    <small>(<span>0</span> log)</small>
                    <a class="pull-right" data-toggle="collapse" href="#panel-recentlog-search" aria-expanded="false" aria-controls="panel-recentlog-search"><i data-toggle="tooltip" title="Search recentlog" class="fa fa-filter"></i></a>
                </h5>
				<div id="panel-recentlog-search" class="collapse panel-search">
					<div class="form-group">
					    <label for="recentlog-search">Search Log Repository</label>
					    <input type="text" class="form-control" id="recentlog-search" placeholder="Log Repository Name">
					</div>
					<div class="form-group">
					    <label>Found : <b><span></span></b></label>						
					</div>				
				</div>
			</header>
			<div class="panel-body profile-nav">
				<ul id="recentlog-list" class="nav nav-pills nav-stacked file-list">

				</ul>
			</div>
		</section>
		<section id="panel-dataset" class="panel panel-default panel-file-list">
			<header class="panel-heading">
				<h5>
                    <i class="fa fa fa-history"></i> Original Dataset 
                    <small>(<span>0</span> dataset)</small>
                    <a class="pull-right" data-toggle="collapse" href="#panel-dataset-search" aria-expanded="false" aria-controls="panel-dataset-search"><i data-toggle="tooltip" title="Search dataset" class="fa fa-filter"></i></a>
                </h5>
				<div id="panel-dataset-search" class="collapse panel-search">
					<div class="form-group">
					    <label for="dataset-search">Search Dataset</label>
					    <input type="text" class="form-control" id="dataset-search" placeholder="Dataset Name">
					</div>
					<div class="form-group">
					    <label>Found : <b><span></span></b></label>						
					</div>
				</div>
			</header>
			<div class="panel-body profile-nav">
				<ul id="dataset-list" class="nav nav-pills nav-stacked file-list">

				</ul>
			</div>
		</section>
	</div>

	<div class="col-sm-4">
		<section id="panel-upload-file" class="panel panel-default">
			<header class="panel-heading">
                <h5>
                    <i class="fa fa fa-cloud-upload"></i> Upload Dataset <span class="tools pull-right"> 
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
				    </span>
                </h5>
			</header>
			<div class="panel-body">
				{{
				Form::open(array('id'=>'upload','files'=>true,'method' => 'POST')) }}
				<div class="form-group">
					<label></label> 
					<input id="file-upload" type="file" class="form-control" name="file" />
					<p class="help-block">
						Only <strong>.mxml/.mxml.gz/csv</strong> is allowed
					</p>
				</div>
				<div class="text-center">{{ Form::submit('Upload', array(
					'id'=>'btn-submit', 'name'=> 'submit', 'class'=>'btn btn-info disabled')) }}
				</div>
				<div class="upload-logo empty"></div>
				<?php /*/ ?>
				{{ Form::close() }} @if (!is_null($fileUpload['errmsg']))
				<hr />
				<div class="alert alert-success alert-block">
					<h4>
						<i class="icon-ok-sign"></i> {{
						Lang::get('messages.status.success') }}
					</h4>
					<p>{{ $fileUpload['errmsg'] }}</p>
					@if (strcmp($fileUpload['errmsg'], Lang::get('messages.fileupload.success_mxml'))==0) 
							<a href="{{ URL::action('App\Modules\Dashboard\Home\Controllers\HomeController@getIndex', $fileUpload['json_decode']['resourceId']) }}" class="btn btn-success" type="button">To Log Summary</a> 
					@endif

				</div>
				@endif
				<?php //*/ ?>
			</div>
		</section>
	</div>

	<div class="col-sm-4">
		<section class="panel panel-default">
			<header class="panel-heading">
				<h5>
                    <i class="fa fa fa-tags"></i> Getting Started <span class="tools pull-right"> 
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
				    </span>
                </h5>
			</header>
			<div class="panel-body">
				<!-- <img width="300px" src="{{ babModuleAsset('general/base/assets/bab/images/stepbystep.png') }}"><br />
				<br /> -->
				<a href="{{ asset('files\GettingStarted.avi')}}" class="btn btn-info btn-block">Get video!</a>
				<!-- <button class="btn btn-info btn-block" type="button">Get video!</button> -->
				<a href="{{ asset('files\GettingStarted.pdf') }}" class="btn btn-info btn-block">Get PDF!</a>
			</div>
		</section>
		
	</div>
</div>
@stop 

@section('footerscript')

@include('home::handlebars-template')

<script type="text/javascript">
	var TheModel = Backbone.Model.extend();

	var recentLog = {{ $jsonData['recentLog'] }};
	var dataset = {{ $jsonData['dataset'] }};

	var homeView = new HomeView({
		el: '#home',
		model: new TheModel({
			jsonURL: "{{ Config::get('api::config.repository.view') }}",
			dashboardURL: "{{ URL::to('dashboard/index') }}/",
			datasetURL: "{{ URL::to('home/dataset') }}/",
			recentLog: recentLog,
			dataset: dataset
		}),
	});
    
</script>

@stop

