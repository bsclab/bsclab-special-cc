<script type="text/x-handlebars-template" id="file-list-template">
	<a href="{{url}}">{{ name }}</a>
	<span class="label bg-warning showqtip-top" title="Log uploaded date"><i class="fa fa-clock-o"></i> {{ date }}</span>
	{{#if isLogRepositories}}
	<span class="label bg-info showqtip-top" title="number of cases"><i class="fa fa-briefcase"></i> {{ noOfCases }}</span>
	<span class="label bg-info showqtip-top" title="number of events"><i class="fa fa-star"></i> {{ noOfEvents }}</span>
	{{/if}}

	{{#if canDelete}}
	<span class="pull-right">
		<a class="btn pull-right btn-danger btn-delete" data-resourceid="{{resourceId}}" href="#"><i class="fa fa-icon fa-remove"></i></a>
	</span>
	{{/if}}
</script>