<?php namespace App\Modules\Dashboard\Home\Controllers;

use View, Auth, Redirect, User, Session, Config, CurlFile, Lang ;
use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\Dashboard\Home\Models\HomeModel;
use App\Modules\General\API\Models\RepositoryAPIModel;
use \Cartalyst\Sentry\Facades\Laravel\Sentry;

class HomeController extends BaseController {

    /**
     * Inject the models.
     * @param HomeModel $homeModel
     */
    public function __construct(HomeModel $model, RepositoryAPIModel $APIModel)
    {
        parent::__construct();
        $this->beforeFilter('babAuth');
        $this->beforeFilter('hasAccess:basic');
        
        $this->model = $model;
        $this->APIModel = $APIModel;
        
        $title = 'Home';
        $this->title = $title;
        $cssFiles = Config::get('home::config.cssfiles');
        $jsFiles = Config::get('home::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));
    }


    public function getIndex()
    {
        // echo Hash::make('b'); die();
        // Finished Features : gpv,hm,gtv,ar,sn,gfv,tg,dl,gig,dc,pc
        // gpv,pm,hm,fm,lr,
        // gpc,km,hc,
        // gtv,lc,ar,bn,sn,
        // gfv,tg,dl,
        // gig,tm,dc,pc
        /*if (Auth::id() == null)
            return Redirect::to('user/login');  */      

        $jsonData = array(
            'recentLog' => $this->APIModel->getRepositoryList(),
            'dataset' => $this->APIModel->getRepositoryDatasetList()
        );
        $fileUpload = array(
            'errmsg' => null,
            'json_decode' => null,
            'json_serialize' => null,
            'csv_map' => null
        );
        
        
        $this->layout->maincontent = View::make('home::index', compact('jsonData', 'fileUpload'));
    }

    public function postIndex()
    {
        $errmsg = '';
        $jsonDecode = null;
        
        if (isset($_POST['submit'])) {
            $url = Config::get('api::config.repository.import');
            // var_dump($_FILES); 
            // die();
            $filename = $_FILES['file']['name'];
            $filedata = $_FILES['file']['tmp_name'];
            
            $filesize = $_FILES['file']['size'];
            $filetype = $_FILES['file']['type'];
            
            $filext = substr($filename, - 3);
            list ($rfileext, $o) = explode('.', strrev($filename), 2);
            $filext = strrev($rfileext);
            $uploadpath = Config::get('base::sitesetting.path.uploads_relative');
            // var_dump($filedata); var_dump($uploadpath);
            // die();
            copy($filedata, $uploadpath . $filename); // copy data to local server first
            $uploadFile = public_path().Config::get('base::sitesetting.path.uploads_absolute') . $filename;
            // var_dump($url); die();
            $cfile = new CurlFile($uploadFile, $filetype, $filename);
            
            if ($filedata != '') {
                $headers = array(
                    "Content-Type:multipart/form-data"
                ); // cURL headers for file uploading
                $postfields = array(
                    "file" => $cfile
                );
                
                $ch = curl_init();
                $options = array(
                    CURLOPT_URL => $url,
                    CURLOPT_HEADER => false, // get json only without HTTP header
                    CURLOPT_POST => 1,
                    CURLOPT_HTTPHEADER => $headers,
                    CURLOPT_POSTFIELDS => $postfields,
                    CURLOPT_INFILESIZE => $filesize,
                    CURLOPT_RETURNTRANSFER => true
                ); // cURL options
                curl_setopt_array($ch, $options);
                $result = curl_exec($ch);
                if (! curl_errno($ch)) {
                    $info = curl_getinfo($ch);
                    if ($info['http_code'] == 200) {
                        $jsonDecode = json_decode($result, true);
                        $jsonDecode['resourceId'] = $jsonDecode['repositoryId'];
                        // echo "<br/>"; var_dump($jsonDecode); 
                        list ($workspaceId, $repositoryId) = explode('_', $jsonDecode['resourceId'], 2);
                        // echo "<br/>";var_dump(Config::get('api::repository.importmxmlgz')); var_dump($repositoryId);
                        // die();
						$response = null;
                        if (strcmp($filext, 'gz') == 0) {
                            $response = file_get_contents(Config::get('api::repository.importmxmlgz').'_'.$repositoryId);
                            $errmsg = Lang::get('messages.fileupload.success_mxml');
                        } 
                        elseif (strcmp($filext, 'mxml') == 0) {
                            $response = file_get_contents(Config::get('api::repository.importmxml').'_'.$repositoryId);
                            $errmsg = Lang::get('messages.fileupload.success_mxml');
                        }
                        elseif (strcmp($filext, 'xls') == 0) {
                            $response = file_get_contents(Config::get('api::repository.importxls').'_'.$repositoryId);
                            $errmsg = Lang::get('messages.fileupload.success_mxml');
                        }
                        elseif (strcmp($filext, 'xlsx') == 0) {
                            $response = file_get_contents(Config::get('api::repository.importxlsx').'_'.$repositoryId);
                            $errmsg = Lang::get('messages.fileupload.success_mxml');
                        } 
                        elseif (strcmp($filext, 'csv') == 0) {
                            $response = file_get_contents(Config::get('api::repository.importcsv').'_'.$repositoryId);
                            $errmsg = Lang::get('messages.fileupload.success_csv');
                        }
						if ($response != null) {
							$resObj = json_decode($response, true);
							list($workspaceId, $repositoryId, $etcId) = explode('_', $resObj['returnUri']);
							$this->trackJob($resObj, 'home/dataset/' . $workspaceId . '_' . $repositoryId);
						}
                    }
                } else {
                    $errmsg = curl_error($ch);
                }
                curl_close($ch);
            } else {
                $errmsg = Lang::get('messages.fileupload.nofile');
            }
        }
        $jsonData = array(
            'recentLog' => $this->APIModel->getRepositoryList(),
            'dataset' => $this->APIModel->getRepositoryDatasetList()
        );
        $fileUpload = array(
            'errmsg' => $errmsg,
            'json_decode' => $jsonDecode,
            'json_serialize' => serialize($jsonDecode),
            'csv_map' => Config::get('base::sitesetting.csv_map')
        );
//        if (strcmp($errmsg, Lang::get('messages.fileupload.success_csv')) == 0) {
//            Session::put('json_serialize', serialize($jsonDecode));
//            return Redirect::action('HomeController@getCsvMapping', $jsonDecode['resourceId']);
        //} else {
            // $this->layout->maincontent = View::make('home.index', $data);
            $this->layout->maincontent = View::make('home::index', compact('jsonData', 'fileUpload'));
//        }
    }
	
	public function getDataset($resourceId) {
        $jsonData = array(
			'datasetId' => $resourceId,
            'dataset' => $this->APIModel->getRepositoryDataset($resourceId),
			'repositories' => $this->APIModel->getRepositoryDerivedFormDataset($resourceId)
        );
        
        $this->layout->maincontent = View::make('home::dataset', compact('jsonData'));	
	}
	
	public function postDataset($resourceId) {
		$request = array(
			'name' => $_POST['name'],
			'description' => $_POST['description'],
			'mapping' => array(),
			'filter' => array(),
		);
		foreach ($_POST['mapping'] as $field => $mapping) {
			if ($mapping['purpose'] != '---') {
				$type = 'EVENT';
				if ($mapping['purpose'] == 'CASE') 
					$type = 'CASE';
				$request['mapping'][$mapping['purpose']][$field] = $type;
				if ($mapping['purpose'] == 'TIMESTAMP') {
					if (empty($mapping['remark']) || trim($mapping['remark']) == '') $mapping['remark'] = 'yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX';
					if (empty($mapping['remark2']) || trim($mapping['remark2']) == '') $mapping['remark2'] = 'complete';
					$request['mapping'][$mapping['purpose']][$field] .= ',' . $mapping['remark'] . ',' . $mapping['remark2'];
				}
			}
		}
		if (isset($_POST['filter'])) {
			foreach ($_POST['filter'] as $field => $states) {
				if (count($states) > 0) {
					foreach ($states as $state) {
						$request['filter'][$field][$state] = 'EQUAL';
					}
				}
			}
		}
		$result = json_decode(@$this->APIModel->postMappingDataset($resourceId, $request), true);
		$this->trackJob($result);//, 'dashboard/index/' . $result['returnUri']);
		return Redirect::to('home/dataset/' . $resourceId);
	}
	
	public function getFilter($resourceId) {
		$a = explode('_', $resourceId, 3);
		$resourceId = $a[0] . '_' . $a[1];
        $jsonData = array(
            'dataset' => $this->APIModel->getRepositoryDataset($resourceId),
			'repositories' => $this->APIModel->getRepositoryDerivedFormDataset($resourceId)
        );
		$this->layout->maincontent = View::make('home::filter', compact('jsonData'));	
	}

    public function getLogin()
    {
        $data['pageInfo'] = array(
            'title' => 'Login',
            'resourceId' => null
        );
        return View::make('home::login', $data);
    }

    public function getChangeworkspace($workspaceId = '1407131074')
    {
        Session::put('workspaceId', $workspaceId);
        Session::forget('resourceId');
        return Redirect::to('home/index');
    }

    public function postLogin()
    {
        if (isset($_POST['username']) && isset($_POST['password'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            if (Auth::attempt(array(
                'username' => $username,
                'password' => $password
            ))) {
                $user = User::find(Auth::id());
                Session::put('workspaceId', $user->default_workspace);
                Session::put('userId', Auth::id());
                return Redirect::to('home/index');
            }
        }
        return Redirect::to('home/login');
    }
    
    public function getMoreFeatures($feature=null){
        $mod = Config::get('modules::modules.app/modules/'.$feature);
                    
        $feature_name = $mod['name'];
        $this->layout->maincontent = View::make('home::moreFeatures', compact('feature_name'));
    }
    
    public function getComingSoon($feature=null){
        $mod = Config::get('modules::modules.app/modules/'.$feature);
        
        $feature_name = $mod['name'];
        $this->layout->maincontent = View::make('home::comingSoon', compact('feature_name'));
    }

    public function getLogout()
    {
        //Auth::logout();
        Sentry::logout();
        Session::flush();
        
        return Redirect::to('login');
    }
	
	public function getStatus($jobTrackingId) {
		echo file_get_contents(Config::get('api::config.job.status').str_replace('=', '', $jobTrackingId));
		die();
	}
	
	public function getDelete($repositoryId) {
        Session::forget('resourceId');
        $this->APIModel->deleteRepository($repositoryId);
		return Redirect::to('home/index');
	}
	
	public function getDeleterepository($repositoryId) {
        Session::forget('resourceId');
        $this->APIModel->deleteRepository($repositoryId);
		list($workspaceId, $datasetId, $repoId) = explode('_', $repositoryId, 3);
		return Redirect::to('home/dataset/' . $workspaceId . '_' . $datasetId);
	}
}