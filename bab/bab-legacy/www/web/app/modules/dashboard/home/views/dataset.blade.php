@section('headerscript')
@stop

@section('maincontent')
<div class="row mt">
	<div class="col-sm-4">
		<section id="panel-repository-list" class="panel panel-default">
			<header class="panel-heading">
				<h5>
                    <i class="fa fa fa-history"></i> Log Repository List <span class="tools pull-right"> 
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
				    </span>
                </h5>
			</header>
			<div class="panel-body profile-nav">
				<ul id="repository-list" class="nav nav-pills nav-stacked file-list">
				</ul>
			</div>
			<div class="panel-footer">
				<a class="btn btn-danger" href="javascript:confirmDeleteDataset();">Delete Entire Dataset</a>
			</div>
		</section>
	</div>
	<div class="col-sm-8">
		<form method="post" class="form-horizontal">
		<section id="panel-dataset" class="panel panel-default">
			<header class="panel-heading">
				<h5>
                    <i class="fa fa fa-history"></i> Original Dataset Column Mapping <span class="tools pull-right"> 
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
				    </span>
                </h5>
			</header>
			<div id="column-list" class="panel-body profile-nav">
				<div class="form-group">
					<label for="name" class="col-sm-4 control-label">Name</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="field-name" name="name" placeholder="Repository Name">
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-sm-4 control-label">Description</label>
					<div class="col-sm-8">
						<textarea class="form-control" row="3" id="field-description" name="description"></textarea>
					</div>
				</div>
				<?php 
				$dataset = json_decode($jsonData['dataset'], true);
				$dimensions = $dataset['dimensions'];

				$selected = array(
					'CASE' => array(
						'processinstance.id', 'caseid', 'case',
					),
					'ACTIVITY' => array(
						'audittrailentry.workflowmodelelement', 'activity', 'event', 'label',
					),
					'TYPE' => array(
						'audittrailentry.eventtype', 'eventtype', 'type', 'state',
					),
					'ORIGINATOR' => array(
						'audittrailentry.originator', 'originator', 'performer', 'person',
					),
					'TIMESTAMP' => array(
						'audittrailentry.timestamp', 'timestamp', 'time', 'date', 'datetime', 'start', 'complete', 'starttimestamp', 'completetimestamp', 'starttime', 'completetime',
					),
					'RESOURCE' => array(
						'audittrailentry.resource', 'resource', 'machine',
					),
				);

				$fieldCount = 0;
				foreach ($dimensions as $dimension => $states) {
					$ldimension = trim(str_replace(' ', '', strtolower($dimension)));
				?>
				<div class="form-group">
					<label for="name" class="col-sm-4 control-label">{{$dimension}}</label>
					<div class="col-sm-8">
						<div class="input-group">
							<select class="form-control" id="field-{{$fieldCount}}" name="mapping[{{$dimension}}][purpose]" onchange="javascript:showHideRemark({{$fieldCount}})">
								<option value="---">--- Unused Field ---</option>
								<option value="CASE" <?php if (in_array($ldimension, $selected['CASE'])) echo 'selected="selected"'; ?>>Case Id</option>
								<option value="ACTIVITY" <?php if (in_array($ldimension, $selected['ACTIVITY'])) echo 'selected="selected"'; ?>>Activity Name</option>
								<option value="TYPE" <?php if (in_array($ldimension, $selected['TYPE'])) echo 'selected="selected"'; ?>>Event Type</option>
								<option value="ORIGINATOR" <?php if (in_array($ldimension, $selected['ORIGINATOR'])) echo 'selected="selected"'; ?>>Originator</option>
								<option value="TIMESTAMP" <?php if (in_array($ldimension, $selected['TIMESTAMP'])) echo 'selected="selected"'; ?>>Timestamp</option>
								<option value="RESOURCE" <?php if (in_array($ldimension, $selected['RESOURCE'])) echo 'selected="selected"'; ?>>Resource</option>
								<option value="ATTRIBUTE">Attribute</option>
							</select>
							<span class="input-group-btn">
								<a href="javascript:showHideFilter('#field-{{$fieldCount}}-filter')" class="btn btn-primary pull-right"><i class="fa fa-filter"></i></a>
						    </span>

						</div>
						<select class="form-control" id="field-{{$fieldCount}}-filter" name="filter[{{$dimension}}][]" multiple="multiple" style="display: none;">
							<?php 
							arsort($states);
							$lim = 0;
							foreach ($states as $state => $freq) { if(++$lim > 10000) break; ?>
							<option value="<?php echo $state; ?>"><?php echo $state, ' (', $freq, ')'; ?></option>
							<?php } ?>
						</select>
					</div>
					<div id="field-{{$fieldCount}}-remark-container" class="<?php if (!in_array($ldimension, $selected['TIMESTAMP'])) echo 'hide'; ?>">
						<div class="col-sm-4">
							- Date Format and Event Type
						</div>
						<div class="col-sm-5">
							<input type="text" class="form-control" id="field-{{$fieldCount}}-remark" name="mapping[{{$dimension}}][remark]" placeholder="yyyy-MM-dd'T'HH:mm:ss.SSSXXX">
						</div>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="field-{{$fieldCount}}-remark2" name="mapping[{{$dimension}}][remark2]" placeholder="complete">
						</div>
					</div>
				</div>
				<?php
					$fieldCount++;
				}
					
				?>
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-primary pull-right">Create New Log Repository</button>
				<br />
				<br />
			</div>
		</section>
		</form>
	</div>
</div>
@stop 

@section('footerscript')
	@include('home::handlebars-template')
	<script type="text/javascript">
		function confirmDeleteDataset() {
			bootbox.prompt({
				title: "Type YES for delete this dataset",
				value: "NO",
				callback: function(result) {
					if (result == 'YES'){
						document.location = '{{URL::to("home/delete/" . $jsonData["datasetId"])}}';
					}
				}
			});
		}
		
		function showHideRemark(id) {
			var field = $('#field-' + id);
			var container = $('#field-' + id + '-remark-container');
			if (field.val() == 'TIMESTAMP') {
				container.removeClass('hide');
			} else {
				container.addClass('hide');
			}
		}
		
		function showHideFilter(id) {
			var filter = $(id);
			if (filter.is(":visible")) {
				filter.hide();
			} else {
				filter.show();
			}
		}
		
	    var repositoryList = $('#repository-list');
	    var repositories = {{ $jsonData['repositories'] }};

		$('#field-name').val('REPOSITORY ' + (repositories.repositories.length + 1));
		
		FileListViewExtend = FileListView.extend({
			events: {
				'click .btn-delete': 'deleteRepository'
			},
			deleteRepository: function(ev){
				bootbox.prompt({
					title: "Type YES for delete this repository",
					value: "NO",
					callback: function(result) {
						if (result == 'YES'){
							document.location = '{{URL::to("home/deleterepository")}}' + '/' + $(ev.currentTarget).attr('data-resourceid');
						}
					}
				});
			}
		})

		var count=1;
		for(var val in repositories.repositories){
	        // console.log(val);
	        theData = BabHelper.formatDateRepositoryId(repositories.repositories[val]['repositoryId']);
	        
    		_.extend(theData, {
				url : "{{ URL::to('dashboard/index/') }}/"+theData.resourceId,
				isLogRepositories : false,
				canDelete: true
			});

    		file = new FileListModel(theData);
    		fileListView = new FileListViewExtend({
				id: 'log-'+theData.resourceId,
				tagName : 'li',
				model: file
			});
			repositoryList.append(fileListView.render().el);
	    }
		
	</script>
@stop

