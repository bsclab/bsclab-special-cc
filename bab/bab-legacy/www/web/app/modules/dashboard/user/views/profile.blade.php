@section ('headerscript') @stop @section('maincontent')
<div id="log-summary" class="row mt">
  <div class="col-sm-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3>{{ $title }}</h3>
      </div>
      <div class="panel-body">
        <div style="display:none" id="success-info" class="alert alert-success alert-dismissible col-sm-8" aria-label="Close">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <div id="message"></div>
        </div>
        <div style="display:none" id="error-info" class="alert alert-danger alert-dismissible col-sm-8" aria-label="Close">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <div id="message"></div>
        </div>

        <div class="row">
          <div class="col-lg-6">
            <form class="form-horizontal" id="edit-user-form" method="POST" action="#" data-toggle="validator">
              <div class="form-group">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" id="username" name="username" value="{{ $user->username}}" disabled="true">
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" id="email" name="email" value="{{ $user->email }}" required data-error="Invalid email address">
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input class="form-control" type="password" placeholder="{{ trans('syntara::all.password') }}" id="pass" name="pass" data-minlength="6">
                  <div class="help-block">Leave it blank if you wish no change</div>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Fullname</label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" id="first_name" name="first_name" value="{{ $user->first_name }}" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Affiliation</label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" id="affiliation" name="affiliation" value="{{ $user->affiliation }}" required>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">Country</label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" id="country" name="country" value="{{ $user->country }}" data-provide="typeahead" data-source='{{ $list }}'>
                  <div class="help-block with-errors"></div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button id="update-user" class="btn btn-primary">{{ trans('syntara::all.update') }}</button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-lg-1">

          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 mb">
            <div class="steps pn">
              <input type="submit" value="Activities" id="submit" disabled="disabled">

              <label for="op2">
                <i class="fa fa-users"></i> Join since
                <span id="join"></span>
              </label>
              <label for="op1">
                <i class="fa fa-sign-in"></i> Last login
                <span id="lastLogin"></span>
              </label>
              <div class="profile-01 centered">
                <p>Enjoy Life! Greeting from Busan,
                  <span id="now"></span>
                </p>
              </div>
              <label for=""></label>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>
@stop @section('footerscript')
<script src="{{ babModuleAsset('general/base/assets/lib/js/bootstrap-validator/validator.min.js') }}"></script>
<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-typeahead.js"></script>

<script type="text/javascript">
  $('.typeahead').typeahead();
  var join = moment("{{ $user->created_at }}").fromNow();
  var lastLogin = moment("{{ $user->last_login }}").fromNow();
  var now = moment().format("ddd MMM D, YYYY");
  $('#join').html(join);
  $('#lastLogin').html(lastLogin);
  $('#now').html(now);

  $('#edit-user-form').submit(function(e) {
    var formData = new FormData();
    formData.append('password', $('#password').val());
    formData.append('email', $('#email').val());
    formData.append('first_name', $('#first_name').val());
    formData.append('affiliation', $('#affiliation').val());
    formData.append('country', $('#country').val());

    $.ajax({
      type: "POST",
      url: 'profile',
      data: formData,
      processData: false,
      contentType: false,
      cache: false,
      dataType: "json",
      success: function(data) {
        console.log(data);
        if (!data.success) {
          $('#success-info').hide();
          $('#password').val('');
          $('#message').html(data.errorMessages);
          $('#error-info').fadeIn(1000);
          $('#error-info').fadeOut(3800);
        } else {
          $('#error-info').hide();
          $('#password').val('');
          $('#message').html(data.messages);
          $('#success-info').fadeIn(1000);
          $('#success-info').fadeOut(3800);
        }
      },
      error: function(data) {
        console.log(data);
        $('#message').html(data.messages);
        $('#error-info').fadeIn(1000);
      }
    });

    return false;
  });
</script>
@stop
