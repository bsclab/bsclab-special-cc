<?php namespace App\Modules\Dashboard\User\Controllers;

use View, Auth, Redirect, User, Session, Config, Response, Mail, URL;
use App\Modules\General\Base\Controllers\BaseController;
use App\Modules\Dashboard\User\Models\UserModel;
use \Illuminate\Support\Facades\DB;
use \Cartalyst\Sentry\Facades\Laravel\Sentry;

class UserController extends BaseController {

	public function __construct(UserModel $model)
    {
        parent::__construct();
        $this->beforeFilter('babAuth', array('only' => array('getKosong','getLogout', 'getProfile', 'postProfile')));
        $this->beforeFilter('hasAuth', array('only' => array('getLogin', 'getNewPassword', 'getActivate')));

        $this->model = $model;

        $title = 'Home';
        $this->title = $title;
        $cssFiles = Config::get('user::config.cssfiles');
        $jsFiles = Config::get('user::config.jsfiles');
        View::share(compact('cssFiles', 'jsFiles', 'title'));
    }

	public function getLogin()
    {
        $data['pageInfo'] = array(
            'title' => 'Login',
            'resourceId' => null
        );
        $countries = DB::table('countries')->where('status', '=', 1)->get();
        $list = "[";
        foreach ($countries as $country)
        {
            $list = $list.'"'.addslashes($country->name).'",';
        }

        $data['countries'] = $list.'""]';
        return View::make('user::login', $data);
    }

    public function postLogin(){
        try
        {
            // Login credentials
            $credentials = array(
                'username'    => $_POST['username'],
                'password' => $_POST['password'],
            );

            // Authenticate the user
            $user = Sentry::authenticate($credentials, false);
            $permission = $user->getPermissions();
            $groups = $user->getGroups();

            Session::put('userId', $user->id);
            Session::put('workspaceId', $user->default_workspace);

            return Response::json(array('success' => true, 'code'=>200, 'url' => 'home/index'));

        }
        catch (\Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'Login field is required.'));
        }
        catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'Password field is required.'));
        }
        catch (\Cartalyst\Sentry\Users\WrongPasswordException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'Wrong password, try again.'));
        }
        catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User was not found. <a href="#" onClick="showRegister(0)">Register</a>'));
        }
        catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User is not activated. Contact our support services.'));
        }

        // The following is only required if the throttling is enabled
        catch (\Cartalyst\Sentry\Throttling\UserSuspendedException $e)
        {
            return Response::json(array('success' => false, 'code'=>403, 'errorMessages' => 'User is suspended. Contact our support services.'));
        }
        catch (\Cartalyst\Sentry\Throttling\UserBannedException $e)
        {
            return Response::json(array('success' => false, 'code'=>403, 'errorMessages' => 'User is banned. Contact our support services.'));
        }

    }

    public function postRegister(){
        try
        {
            // Create the user
            $user = Sentry::register(array(
                'email'     => $_POST['email'],
                'username'     => $_POST['username'],
                'password'  => $_POST['password'],
                'first_name'   => $_POST['first_name'],
                'affiliation'   => $_POST['affiliation'],
                'country'   => $_POST['country'],
                'workspaces'   => $_POST['username'].':'.$_POST['first_name'].'-workspace',
                'default_workspace'   => $_POST['username'],
                'permissions' => array('basic' => 1),
            ));

            $data['email'] = $_POST['email']; //'wanprabu@gmail.com'
            $data['name'] = $_POST['first_name'];

            $activationCode = $user->getActivationCode();
            $data['url'] = URL::to('activate', array($_POST['email'], $activationCode));
            $data['stringUrl'] = htmlentities($data['url']);

            Mail::send('emails.activation', $data,
               function($message) use($data){
                    $message->to($data['email'], $data['name'])->subject('Your account is almost active!');
                }
            );

            //echo 'Registration success. <a href="login">login</a>';
            return Response::json(array('success' => true, 'code'=>200, 'messages' => 'Thank you for joining. You will soon receive an email to activate the account. Check out your spam box if not coming to your inbox!'));
        }
        catch (\Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'Login field is required.'));
        }
        catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'Password field is required.'));
        }
        catch (\Cartalyst\Sentry\Users\UserExistsException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User with this login already exists.'));
        }
    }

    public function postResetPassword(){
        try
        {
            // Find the user using the user email address
            $user = Sentry::findUserByEmail($_POST['email']);
            $data['email'] = $_POST['email'];
            $data['name'] = $user->first_name;

            // Get the password reset code
            $resetCode = $user->getResetPasswordCode();
            $data['url'] = URL::to('password_reset', array($data['email'], $resetCode));
            $data['stringUrl'] = htmlentities($data['url']);

            Mail::send('emails.reset', $data,
               function($message) use($data){
                    $message->to($data['email'], $data['name'])->subject('BAB Password Reset');
                }
            );

            return Response::json(array('success' => true, 'code'=>200, 'messages' => 'Your new password request has been send to your email. Check out your spam box if not coming to your inbox!'));
        }
        catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User with "'.$_POST['email'].'" was not found.'));
        }
    }

    public function getNewPassword($email, $token){
        try
        {
            // Find the user using the user email address
            $user = Sentry::findUserByEmail($email);

            // Check if the reset password code is valid
            if ($user->checkResetPasswordCode($token))
            {
                $data['success'] = true;
                $data['email'] = $email;
                $data['token'] = $token;
            }
            else{
                $data['success'] = false;
                $data['messages'] = 'It looks like you clicked on an invalid password reset link. Please try again.';
            }

            return View::make('user::newpassword', $data);
        }
        catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            $data['success'] = false;
            $data['messages'] = 'User was not found.';

            //return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User was not found.'));
            return View::make('user::newpassword', $data);
        }

    }

    public function postNewPassword($email, $token){
        try
        {
            // Find the user using the user email address
            $user = Sentry::findUserByEmail($_POST['email']);

            // Check if the reset password code is valid
            if ($user->checkResetPasswordCode($_POST['token']))
            {
                // Attempt to reset the user password
                if ($user->attemptResetPassword($_POST['token'], $_POST['password']))
                {
                    return Response::json(array('success' => true, 'code'=>200, 'messages' => 'Congratulation, your password has been changed. Try to sign in.'));
                }
                else
                {
                    // Password reset failed
                    return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'Password alteration failed. Please contact our support.'));
                }
            }
            else
            {
                // The provided password reset code is Invalid
                return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'It looks like you clicked on an invalid password reset link. Please try again.'));
            }

        }
        catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User was not found.'));
        }
    }

    public function getActivate($email, $token){
        try
        {
            // Find the user using the user id
            $user = Sentry::findUserByEmail($email);
            $data['success'] = true;
            $data['messages'] = '';

            // Attempt to activate the user
            if ($user->attemptActivation($token))
            {
                // User activation passed
                //return Response::json(array('success' => true, 'code'=>200, 'messages' => 'Congratulation, your account has been activated. Try to sign in.'));
                $data['messages'] = 'Congratulation, your account has been activated. Try to sign in.';
            }
            else
            {
                // User activation failed
                //return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'It looks like you clicked on an invalid activation link. Please try again.'));
                $data['success'] = false;
                $data['messages'] = 'It looks like you clicked on an invalid activation link. Please try again.';
            }

            return View::make('user::activated', $data);
        }
        catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            //return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User was not found.'));

            $data['success'] = false;
            $data['messages'] = 'User was not found.';
            return View::make('user::activated', $data);
        }
        catch (\Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)
        {
            //return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User is already activated.'));

            $data['success'] = false;
            $data['messages'] = 'User is already activated.';
            return View::make('user::activated', $data);
        }

    }

    public function getProfile(){
        $cssFiles = Config::get('home::config.cssfiles');
        $jsFiles = Config::get('home::config.jsfiles');

        $user = Sentry::getUser();
        $title = 'User Profile';

        $countries = DB::table('countries')->where('status', '=', 1)->get();
        $list = "[";
        foreach ($countries as $country)
        {
            $list = $list.'"'.addslashes($country->name).'",';
        }

        $list = $list.'""]';

				View::share(compact('cssFiles', 'jsFiles', 'title'));
        $this->layout->maincontent = View::make('user::profile', compact('title', 'user', 'list'));
    }

    public function postProfile(){
        try
        {
            // Find the user using the user id
            $user = Sentry::findUserById(Session::get('userId'));

            // Update the user details
            $user->email = $_POST['email'];
            $user->first_name = $_POST['first_name'];
            $user->affiliation = $_POST['affiliation'];
            $user->country = $_POST['country'];

            if (!($_POST['password'] === NULL))
            $user->password = $_POST['password'];
            //$this->workspace_alias =

            // Update the user
            if ($user->save())
            {
                // User information was updated
                return Response::json(array('success' => true, 'code'=>200, 'messages' => 'Your profile information has been updated.'));
            }
            else
            {
                // User information was not updated
                return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'Profile update failed. Contact our support.'));
            }
        }
        catch (\Cartalyst\Sentry\Users\UserExistsException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User with this login already exists.'));
        }
        catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return Response::json(array('success' => false, 'code'=>500, 'errorMessages' => 'User was not found.'));
        }
    }

    public function getLogout()
    {
        //Auth::logout();
        Sentry::logout();
        Session::flush();

        return Redirect::to('login');
    }
}
