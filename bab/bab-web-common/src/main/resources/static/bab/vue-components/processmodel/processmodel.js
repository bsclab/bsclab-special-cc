Vue.component('processmodel', {
  template: '#processmodel-template',
  props: ['pmData', 'lrData', 'apiURI'],
  ready: function(){
	  // console.log(this.pmData, this.lrData);
	  this.$http.get(this.apiURI.processModel).then(function (response) {
	      var theResponse = response.data.response;
	      if(!_.isNull(theResponse)){
          this.processModel = new ProcessModel('#processmodel-content', {}, {}, this.pmData.options);
//          console.log("vue process model: " + this.processModel);
	    	  switch (this.pmData.options.type) {
	    	  	case 'timegap':
	    	  		this.processModel.update(theResponse.nodes, theResponse.transitions, this.pmData.options);
	    	  		break;
	    	  	case 'heuristic':
	    	  		this.processModel.update(theResponse.nodes, theResponse.arcs, this.pmData.options);
	    	  		break;
	    	  }
	    	  this.processModel.drawProcessModel();
	    	  //this.logReplay = new LogReplay(undefined, undefined, this.lrData, this.apiURI, this.lrData.options);
	      }
	      else{
	    	  alert('response is NULL!');
	      }
	  }, function (response) {	
	      // error callback
		  alert('error get process model!');
	  });
	  
  },
  methods: {
    // methods for process model
    scaleToFit: function () {
      this.processModel.graphScaleToFit();
    },
    scaleToActual: function () {
      this.processModel.graphScaleToActual();
    },
    goToLogReplay: function(){
      var self = this;

      // set log replay flag true
      this.pmData.options.isLogReplay = true;
      
      reqwest({
        url: this.apiURI.repository,
        method: 'get',
        crossOrigin: true
      })
      .then(function(response){
        console.log('repository', response.status);

        if(response.status == 'QUEUED' || response.status == 'RUNNING'){
          WaitingHelper.show();
        }
        else if(response.status == 'FAILED'){
          alert('Cannot load data.');
        }
        else if(response.status == 'FINISHED'){
          var cases = response.response.cases;
          
          reqwest({
            url: this.apiURI.logreplay,
            method: 'post',
            data: {
              limit: self.lrData.options.tokensPerPartition
            },
            crossOrigin: true
          })
          .then(function(response2){
            console.log('logreplay', response2.status);
            if(response2.status == 'QUEUED' || response2.status == 'RUNNING'){
              WaitingHelper.show();
            }
            else if(response2.status == 'FAILED'){
              alert('Cannot load data.');
            }
            else if(response2.status == 'FINISHED'){
              self.lrData.numberOfTokens = response2.response.eventsNumber;
              self.logReplay.initializeData(self.processModel, response2.response, cases);
              self.logReplay.initialize();
            }
          })
          .fail(function(error2){
            alert('log replay error!');
          });
        }
      })
      .fail(function(error2){
        alert('get repository error!');
      });

    },
    changeAlgorithm: function(){

    },
    decreaseFontSize: function(){
      console.log('decrease font size', ev);
    },
    increaseFontSize: function(){
      console.log('increase font size', ev);
    },
    changeArcType: function(ev){
//      console.log('change arc type', ev.target.value);
      this.processModel.changeArcType(ev.target.value);
    },
    changeArcDirection: function(ev){
//      console.log('change arc direction', ev.target.value);
      this.processModel.changeArcDirection(ev.target.value);
    },
    searchNode: function(ev){
      console.log('search', ev);
    },

    // methods for log replay
    play: function(){
      if(this.lrData.state=='play'){
        this.logReplay.pause();
      }
      else if(this.lrData.state=='pause'){
        this.logReplay.resume();
      }
      else if(this.lrData.state=='stop'){
        this.logReplay.play();
      }
      console.log(this.logReplay.state, '-', this.lrData.state);
    },
    restart: function(){
      this.logReplay.restart();
    },
    stop: function(){
      this.logReplay.stop();
      console.log(this.logReplay.state, '-', this.lrData.state);
    },
    toggleKPI: function(){
      this.lrData.setting.toggleKPI = !this.lrData.setting.toggleKPI;
      if(this.lrData.setting.toggleKPI){
        this.logReplay.startKPICalculation();
      }
      else{
        this.logReplay.stopKPICalculation();
      }
    },
    toggleRealtimeKPI: function(){
      this.lrData.setting.toggleRealtimeKPI = !this.lrData.setting.toggleRealtimeKPI;
    },
    toggleFadeout: function(){
      this.lrData.setting.toggleFadeout = !this.lrData.setting.toggleFadeout;
    }
  }
});
