/*
 * this is process model component based on graphviz in the server
 * author @superpikar
 * */
var ProcessModelInServerComponent = Vue.extend({
  template: '#processmodel-template',
  components: {
    'loader': LoaderComponent
  },
  props: ['pmData', 'lrData', 'apiURI', 'theid'],
  data: function() {
    return {
      isProcessing: false,
      isProcessing2: false,
      repoData: {
        noOfCases: 0,
        noOfEvents: 0,
        noOfActivities: 0
      },
      data: {
    	searchNode: '',
    	nodeFound: 0
      }
    };
  },
  ready: function() {
    console.log('process model is ready');
    this.processModel = new ProcessModelInServer('#'+this.theid, this.pmData.options);
    this.loadSummaryView();
    this.loadProcessModel();
  },
  methods: {
    loadSummaryView: function() {
      this.isProcessing2 = true;
      this.$http.get(this.apiURI.repositoryView)
        .then(function(response) {
          // success callback
          console.log('summary view', response.data);
          this.$set('repoData', response.data.response);
          this.isProcessing2 = false;
        }, function(response) {
          // error callback
          this.isProcessing2 = false;
          alert('error get summary view!');
        });
    },
    loadProcessModel: function() {
      this.isProcessing = true;
      this.$http.get(this.apiURI.processModel)
        .then(function(response) {
          var theResponse = response.data.response;
          if (!_.isNull(theResponse)) {
            if (response.data.status != 'FINISHED') {
              alert('the data response status is ' + response.data.status);
              this.isProcessing = false;
            }
            else {
              console.log(_.keys(response.data.response.nodes).length, 'nodes', _.keys(response.data.response.arcs).length, 'arcs');
              this.$http.get(this.apiURI.processModelInServer)
                .then(function(response2) {
                  var start = new Date();
                  this.processModel.updateData(response.data.response.nodes, response.data.response.arcs, this.pmData.options);
                  this.processModel.draw(response2.data);
                  this.isProcessing = false;
                  
                  var end = new Date();
                  var duration = end - start;
                  console.log('duration '+duration+'ms');
                }, function(reponse2) {
                  // error callback
                  alert('error get process model from server!');
                  this.isProcessing = false;
                });
            }
          } else {
            alert('response is NULL!');
            this.isProcessing = false;
          }
        }, function(response) {
          // error callback
          alert('error get process model!');
        });
    },
    scaleToFit: function() {
      this.processModel.scaleToFit();
    },
    scaleToActual: function() {
      this.processModel.scaleToActual();
    },
    decreaseFontSize: function() {
      alert('decrease font size');
    },
    increaseFontSize: function() {
      alert('increase font size');
    },
    changeArcType: function(ev) {
      alert('change arc type');
    },
    changeArcDirection: function(ev) {
      alert('change arc type');
    },
    searchNode: function() {
      this.processModel.searchNode(this.data.searchNode);
      this.data.nodeFound = this.processModel.nodes.nodeFound;
    },
    changeAlgorithm: function(ev) {

    }
  }
});
