// credit : https://github.com/cpettitt/dagre-d3/issues/202
SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(elem) {
    return elem.getScreenCTM().inverse().multiply(this.getScreenCTM());
};

// Write your package code here!
ProcessModel = (function(){
  var nodes = new Nodees(),
  	  arcs = new Arcs();
  
  var render = new dagreD3.render(),
      g = new dagreD3.graphlib.Graph()
                .setGraph({})
                .setDefaultEdgeLabel(function() { return {}; });
  
  var options = {
	  nodesep: 70,
      ranksep: 50,
      rankdir: "TB",
      marginx: 20,
      marginy: 20,
      isLogReplay: false
  }
  
  var container,
  	  svg,
  	  svgGroup,
  	  zoom;
  
  var size = {
	  width: 0,
	  height: 0
  };
  
  var nodeTemplate = Handlebars.compile($('#node-template').html()),
      arcTemplate = Handlebars.compile($('#arc-template').html()),
      arcHoverTemplate = Handlebars.compile($('#archovertooltip-template').html()),
      nodeHoverTemplate = Handlebars.compile($('#nodehovertooltip-template').html()),
      nodeClickTemplate = Handlebars.compile($('#nodeclicktooltip-template').html());
  
  var arcQtip,
  	  nodeQtip;
  
  function ProcessModel(parentEl, theNodes, theArcs, newOptions){
    var containerEl = parentEl+' .processmodel-content';
    
    nodes.initialize(theNodes, newOptions);
    arcs.initialize(theArcs, newOptions);
    
    _.extend(options, newOptions);
    
    container = d3.select(containerEl);
    svg = container.append('svg');
    svgGroup = svg.append("g");
    
    zoom = d3.behavior.zoom().on("zoom", function(){
      svgGroup.attr("transform", "translate(" + d3.event.translate + ")" + "scale(" + d3.event.scale + ")");
    });

    if(options.isLogReplay){
      size.height = $('body').height() - $('.top.fixed.menu').height() - $('#processmodel-control').height() - $('#logreplay-control').height() - 150;
      size.width = $(containerEl).width();
    }
    else{
      size.height = $('body').height() - $('.top.fixed.menu').height() - $('#processmodel-control').height() - 150;
      size.width = $(containerEl).width();
    }
    
//    this.ticker = TweenLite.ticker;
  }

  ProcessModel.prototype = {
    update: function(thenodes, thearcs, newOptions){
    	
      nodes.initialize(thenodes, newOptions);
      arcs.initialize(thearcs, newOptions);
      
      _.extend(options, newOptions);

      if(options.is3D){
        // write your 3d code here
      }
    },
    destroyProcessModel: function(){
    	// remove node, arc, and tooltip
    	if(!_.isEmpty(g.nodes())){
    		console.log('remove nodes');
    		g.nodes().forEach(function(val) {
    			g.removeNode(val);
    		});
    	}
    	if(!_.isEmpty(g.edges())){
    		console.log('remove edges');
    		g.edges().forEach(function(val) {
    			g.removeEdge(val.v, val.w);
    		});
    	}
    	if(!_.isUndefined(nodeQtip)){
    		nodeQtip.qtip('destroy');
    	}
    	if(!_.isUndefined(arcQtip)){
    		arcQtip.qtip('destroy');
    	}    	
    },
    drawProcessModel: function() {
      this.destroyProcessModel();
      nodes.getAll().forEach(function(val, key){
	    var theClass= val.difference? 'difference': 'normal';
        g.setNode(val.svgId,  {
          id: val.svgId,
          labelType: "html",
          label: nodeTemplate(val),
          rx: 5,
          ry: 5,
          class: theClass
          // label: setNodeLabel(val, options.type, options.isLogreplay, false)
        });
      });

      // Set up edges, no special attributes.
      arcs.getAll().forEach(function(val, key){
    	var theClass= val.difference? 'difference': 'normal';
    	var nodeSource = nodes.get(val.sourceId);
    	var nodeTarget = nodes.get(val.targetId);
    	g.setEdge(nodeSource.svgId, nodeTarget.svgId, {
          id: val.svgId,
          weight: 1,
          labelType: "html",
          label: arcTemplate(val),
          lineInterpolate: 'basis',
          labelpos: 'c',
          labelId: 'label-' + nodeSource.svgId+'-'+nodeTarget.svgId,
          source: val.source,
          target: val.target,
          class: theClass
        });
      });
      
      svg.call(zoom);

      this.setSVGSize(size.width, size.height);
      this.updateGraph();
      
      // add output to render tokens
      d3.select('svg g.output').append('g').attr('class', 'tokens');

      console.log(options.is3D);

      if(options.is3D){
    	  // write update 3d here        
      } else{
        // initialize tooltip
    	// initialize node tooltip
          nodeQtip = $('.graph g.node').qtip({
    	      overwrite: true,
    	      style: {
    	        classes: 'qtip-light qtip-shadow'
    	      },
    	      position: {
    	        target: 'mouse',
    	        adjust: {
    	          mouse: true  // Can be omitted (e.g. default behaviour)
    	        }
    	      },
    	      content: {
    	        title: 'Node Information',
    	        text: function(event, api){
    	          var node = nodes.getBySvgId($(this).attr('id'));
    	          return nodeClickTemplate(node);
    	        }
    	      }
    	  });
          
          // initialize node tooltip
          arcQtip = $('.graph g.edge').qtip({
    	      overwrite: true,
    	      style: {
    	        classes: 'qtip-light qtip-shadow'
    	      },
    	      position: {
    	        target: 'mouse',
    	        adjust: {
    	          mouse: true  // Can be omitted (e.g. default behaviour)
    	        }
    	      },
    	      content: {
    	        title: 'Arc Information',
    	        text: function(event, api){
    	          var node = arcs.getBySvgId($(this).attr('id'));
    	          return arcClickTemplate(node);
    	        }
    	      }
    	  });
      }
    },
    draw3DProcessModel: function(){
      // draw 3d here
    },
    setSVGSize: function(width, height){
      svg.attr('width', width);
      svg.attr('height', height);
    },
    graphScaleToFit: function(){
      var isUpdate = true;
      var zoomScale = zoom.scale();
      var graphWidth = g.graph().width + 80;
      var graphHeight = g.graph().height + 40;
      var width = parseInt(svg.style("width").replace(/px/, ""));
      var height = parseInt(svg.style("height").replace(/px/, ""));
      zoomScale = Math.min(width / graphWidth, height / graphHeight);
      var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
      zoom.translate(translate);
      zoom.scale(zoomScale);
      zoom.event(isUpdate ? svg.transition().duration(500) : d3.select("svg"));
    },
    graphScaleToActual: function(){
      var isUpdate = true;
      var graphWidth = g.graph().width + 80;
      var graphHeight = g.graph().height + 40;
      var width = parseInt(svg.style("width").replace(/px/, ""));
      var height = parseInt(svg.style("height").replace(/px/, ""));
      var zoomScale = 1;
      var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
      zoom.translate(translate);
      zoom.scale(zoomScale);
      zoom.event(isUpdate ? svg.transition().duration(500) : d3.select("svg"));
    },
    updateGraph: function(){
      // update the graph param
      g.setGraph(options);
      // Run the renderer. This is what draws the final graph.
      
      render(svgGroup, g);

//      arcs.getAll().forEach(function(val, key){
//        val.path = d3.select('svg g #'+val.svgId+' path').node();
//      });
//
//      nodes.getAll().forEach(function(val, key){
//        val.nodeDOM = d3.select('svg g #'+val.svgId);
//      });

      // Center the graph
      var xCenterOffset = (svg.attr("width") - g.graph().width) / 2;
      svgGroup.attr("transform", "translate(" + xCenterOffset + ", 20)");
      this.graphScaleToFit();
    },
    changeArcDirection: function(direction){
      options.rankdir = direction;
      this.updateGraph();
    },
    changeArcType: function(type){
      for (var row in g._edgeLabels){
        var edge = g._edgeLabels[row];
        edge.lineInterpolate = type;
      }
      this.updateGraph();
    },
    searchNode: function(nodeName){
      d3.selectAll('.node').classed('found', false);
      if(nodeName.trim() == ''){
      	this.graphScaleToFit();
      	nodes.nodeFound = 0;
      }
      else{
  	    var theNodes = nodes.searchNode(nodeName);
  	    
  	    if(theNodes.length===0){
  	    	nodes.nodeFound = -1;
  	    	this.graphScaleToFit();
  	    }	
  	    else{
  	    	nodes.nodeFound = theNodes.length;
  	    	// console.log('search node '+nodeName+' '+this.nodes.nodeFound.length);
  	    	theNodes.forEach(function(val, key){
  	    		d3.select('#'+val.svgId).classed('found', true);
  	    	});
  	    	
  	    	// if founded nodes only 1 then zoom to the node
  	    	if(theNodes.length==1){
  	    		var bbox = d3.select('#'+theNodes[0].svgId)[0][0].getBBox();
  	    		
  	    		var isUpdate = true;
  	    		var translate = [Math.abs(bbox.x), Math.abs(bbox.y)];
  	    		zoom.translate(translate);
  	    		zoom.scale(1);
  	    		zoom.event(isUpdate ? svg.transition().duration(500) : d3.select("svg"));
  	    	}
  	    	else if(theNodes.length>1){
  	    		this.graphScaleToFit();
  	    	}
  	    }	    
      }
    }
  };

  return ProcessModel;
})();
