// credit : https://github.com/cpettitt/dagre-d3/issues/202
SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(elem) {
    return elem.getScreenCTM().inverse().multiply(this.getScreenCTM());
};

// Write your package code here!
ProcessModelVisjs = (function(){
  function ProcessModelVisjs(containerEl, nodes, arcs, options){
    var self = this;
    
    this.containerEl = containerEl;
    this.nodes = new Nodees();
    this.nodes.initialize(nodes, options);
    this.arcs = new Arcs();
    this.arcs.initialize(arcs, options);
    
    this.g = new dagre.graphlib.Graph()
		    .setGraph({})
		    .setDefaultEdgeLabel(function() { return {}; });
    
    this.options = _.extend({
      nodesep: 70,
      ranksep: 50,
      rankdir: "TB",
      marginx: 20,
      marginy: 20,
      isLogReplay: false
    }, options);
    
    this.nodeTemplate = Handlebars.compile($('#node-template').html());
    this.arcTemplate = Handlebars.compile($('#arc-template').html());
    this.arcHoverTemplate = Handlebars.compile($('#archovertooltip-template').html());
    this.nodeHoverTemplate = Handlebars.compile($('#nodehovertooltip-template').html());
    this.nodeClickTemplate = Handlebars.compile($('#nodeclicktooltip-template').html());
    
    if(options.isLogReplay){
	    this.size = {
	      height: $('body').height() - $('.top.fixed.menu').height() - $('#processmodel-control').height() - $('#logreplay-control').height() - 150,
	      width: $(containerEl).width()
	    }
	}
	else{
	    this.size = {
	      height: $('body').height() - $('.top.fixed.menu').height() - $('#processmodel-control').height() - 150,
	      width: $(containerEl).width()
	    }
	}
    $(containerEl).height(this.size.height);	// set height of container el
}

  ProcessModelVisjs.prototype = {
    update: function(nodes, arcs, options){
      
    },
    drawProcessModel: function() {
    	var self = this;
    	var thenodes = [];
    	var thearcs = [];
    	
    	this.nodes.getAll().forEach(function(val, key){
    		self.g.setNode(val.id, val);
    	});
    	this.arcs.getAll().forEach(function(val, key){
    		self.g.setEdge(val.sourceId, val.targetId, val);
    	});
//    	dagre.layout(this.g);

    	this.arcs.getAll().forEach(function(val, key){
    		val.from = val.sourceId;
    		val.to = val.targetId;
    		val.arrows = {to:{scaleFactor:0.25}};
    		thearcs.push(val);
    	});
    	
    	var container = document.getElementById(this.containerEl.substring(1));
	    var data = {
    	    nodes: this.nodes.getAll(),
    	    edges: thearcs
    	  };
    	var options = {
	      physics: false,
	      layout: {
	    	  improvedLayout: false
	      }
    	};
    	var network = new vis.Network(container, data, options);
    },
    draw3DProcessModel: function(){
      // draw 3d here
    },
    setSVGSize: function(width, height){
      
    },
    graphScaleToFit: function(){
      
    },
    graphScaleToActual: function(){
      
    },
    updateGraph: function(){
      
    },
    changeArcDirection: function(direction){
      
    },
    changeArcType: function(type){
      
    }
  };

  return ProcessModelVisjs;
})();
