/* @description class of Nodes's process model, to initiate list of arc
 *
 * @example
 *    var arcs = new Arcs();
      arcs.add(new Arc({ }));
 *
 * @params options Object
 * @dependency lodash
 * @author http://twitter.com/superpikar
 */
Nodees = function(nodes){
  this.nodes = nodes || [];
  this.nodeFound = 0;
};

Nodees.prototype = {
  initialize: function(nodes, options){
    this.nodes = [];
    var count = 1;
    for(var key in nodes){
    	nodes[key].svgId = 'node'+count;
		nodes[key].type = options.type;
		nodes[key].isLogReplay = options.isLogReplay;
		this.nodes.push(new Nodee(nodes[key]));
		count++;
    }
  },
  add: function(node){
    this.nodes.push(node);
  },
  get: function(id){
    return _.findWhere(this.nodes, {id: id});
  },
  getBySvgId: function(id){
    return _.findWhere(this.nodes, {svgId: id});
  },
  updateAll: function(nodes){
    this.nodes = nodes;
  },
  getAll: function(){
    return this.nodes;
  },
  searchNode: function(nodeName){
	return _.filter(this.nodes, function(val, key){
	  return val.label.toLowerCase().indexOf(nodeName.trim().toLowerCase()) > -1;
	});
  }
};
