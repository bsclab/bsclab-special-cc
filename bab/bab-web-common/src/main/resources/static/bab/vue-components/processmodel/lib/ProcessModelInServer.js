// credit : https://github.com/cpettitt/dagre-d3/issues/202
SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(elem) {
    return elem.getScreenCTM().inverse().multiply(this.getScreenCTM());
};

// Write your package code here!
ProcessModelInServer = (function(){
  function ProcessModelInServer(containerEl, options){
    var self = this;
    console.log('ready', containerEl, options);
    this.containerEl = containerEl;
    this.nodes = new Nodees();
    this.arcs = new Arcs();
    
    this.options = options;
    this.containerSize = {
      width:0,
      height:0
    };
    this.size = {
      width:0,
      height:0
    };

    this.nodeTemplate = Handlebars.compile($('#node-template').html());
    this.arcTemplate = Handlebars.compile($('#arc-template').html());
    this.nodeHoverTemplate = Handlebars.compile($('#nodehovertooltip-template').html());
    this.arcHoverTemplate = Handlebars.compile($('#archovertooltip-template').html());
    this.nodeClickTemplate = Handlebars.compile($('#nodeclicktooltip-template').html());
    this.arcClickTemplate = Handlebars.compile($('#arcclicktooltip-template').html());

    if(options.isLogReplay){
	    this.containerSize = {
	      height: $('body').height() - $('.top.fixed.menu').height() - $('.processmodel-control').height() - $('.logreplay-control').height() - 150,
	      width: $(containerEl).width()
	    };
  	}
  	else{
  	    this.containerSize = {
  	      height: $('body').height() - $('.top.fixed.menu').height() - $('.processmodel-control').height() - 150,
  	      width: $(containerEl).width()
  	    };
  	}
    $(containerEl).height(this.containerSize.height);	// set height of container el
  }

  ProcessModelInServer.prototype = {
    updateData: function(nodes, arcs, options){
      if(_.isUndefined(options)){
	    this.options = options;    	  
      }
      this.nodes.initialize(nodes, options);
      this.arcs.initialize(arcs, options);
    },
    draw: function(svgString) {
      var self = this;
      var $container = $(this.containerEl + ' .processmodel-content');
      $container.append(svgString);
//      console.log(this.nodes.getAll());
//      console.log(this.arcs.getAll());
      this.svg = d3.select('svg');
      var viewBox = this.svg.attr('viewBox').split(' ');
      this.size = {
        width: parseFloat(viewBox[2]),
        height: parseFloat(viewBox[3])
      };
      // svg.attr('width', viewBox[2]+'pt');
      // svg.attr('width', $container.width());
      // svg.attr('height', parseFloat(viewBox[3]));
      this.svg.attr('width', this.containerSize.width);
      this.svg.attr('height', this.containerSize.height);
      this.svg.attr('viewBox', null);	// remove viewbox

      var inner = this.svg.select('g');
      var transform = inner.attr('transform').split('rotate');
      inner.attr('transform', 'scale(1,1) rotate'+transform[1]);

      // Set up zoom support, credit : http://cpettitt.github.io/project/dagre-d3/latest/demo/tcp-state-diagram.html
      var firstMove = true;
      var oldScale = 0;
      this.zoom = d3.behavior.zoom().on("zoom", function() {
        // if not zoom (panning and dragging)
        // if(d3.event.scale == oldScale){
        //   d3.event.translate[1] += svgSize.height;
        // }
        // else{
        //   d3.event.translate[1] += svgSize.height;	// need adjusment when zoom in
        // }
        if(d3.event.scale == oldScale){
          d3.event.translate[1] += self.containerSize.height;
        }
        else{
          d3.event.translate[1] += self.containerSize.height;	// need adjusment when zoom in
        }
        oldScale = d3.event.scale;
        inner.attr("transform", "translate(" + d3.event.translate + ")" +
            "scale(" + d3.event.scale + ")");
      });
      this.svg.call(this.zoom);
      
      // initialize node tooltip
      $('.graph g.node').qtip({
	      overwrite: true,
	      style: {
	        classes: 'qtip-light qtip-shadow'
	      },
	      position: {
	        target: 'mouse',
	        adjust: {
	          mouse: true  // Can be omitted (e.g. default behaviour)
	        }
	      },
	      content: {
	        title: 'Node Information',
	        text: function(event, api){
	          var node = self.nodes.getBySvgId($(this).attr('id'));
	          return self.nodeClickTemplate(node);
	        }
	      }
	  });
      
      // initialize node tooltip
      $('.graph g.edge').qtip({
	      overwrite: true,
	      style: {
	        classes: 'qtip-light qtip-shadow'
	      },
	      position: {
	        target: 'mouse',
	        adjust: {
	          mouse: true  // Can be omitted (e.g. default behaviour)
	        }
	      },
	      content: {
	        title: 'Arc Information',
	        text: function(event, api){
	          var node = self.arcs.getBySvgId($(this).attr('id'));
	          return self.arcClickTemplate(node);
	        }
	      }
	  });
      
      this.scaleToFit();
    },
    draw3DProcessModel: function(){
      // draw 3d here
    },
    setSVGSize: function(width, height){

    },
    scaleToFit: function(){
      var isUpdate = true;
      var zoomScale = this.zoom.scale();
      var graphWidth = this.size.width + 80;
      var graphHeight = this.size.height + 40;
      var width = this.containerSize.width;
      var height = this.containerSize.height;
      zoomScale = Math.min(width / graphWidth, height / graphHeight);
      var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
      this.zoom.translate(translate);
      this.zoom.scale(zoomScale);
      this.zoom.event(isUpdate ? this.svg.transition().duration(500) : d3.select("svg"));
    },
    scaleToActual: function(){
      var isUpdate = true;
      var graphWidth = this.size.width + 80;
      var graphHeight = this.size.height + 40;
      var width = this.containerSize.width;
      var height = this.containerSize.height;
      var zoomScale = 1;
      var translate = [(width/2) - ((graphWidth*zoomScale)/2), (height/2) - ((graphHeight*zoomScale)/2)];
      this.zoom.translate(translate);
      this.zoom.scale(zoomScale);
      this.zoom.event(isUpdate ? this.svg.transition().duration(500) : d3.select("svg"));
    },
    updateGraph: function(){

    },
    changeArcDirection: function(direction){

    },
    changeArcType: function(type){

    },
    searchNode: function(nodeName){
      d3.selectAll('.node').classed('found', false);
      if(nodeName.trim() == ''){
    	this.scaleToFit();
    	this.nodes.nodeFound = 0;
      }
      else{
	    var theNodes = this.nodes.searchNode(nodeName);
	    
	    if(theNodes.length===0){
	    	this.nodes.nodeFound = -1;
	    	this.scaleToFit();
	    }	
	    else{
	    	this.nodes.nodeFound = theNodes.length;
	    	// console.log('search node '+nodeName+' '+this.nodes.nodeFound.length);
	    	theNodes.forEach(function(val, key){
	    		d3.select('#'+val.svgId).classed('found', true);
	    	});
	    	
	    	// if founded nodes only 1 then zoom to the node
	    	if(theNodes.length==1){
	    		var bbox = d3.select('#'+theNodes[0].svgId)[0][0].getBBox();
	    		
	    		var isUpdate = true;
	    		var translate = [this.size.width/2, Math.abs(bbox.y) - (this.size.height/2)];
	    		this.zoom.translate(translate);
	    		this.zoom.scale(1);
	    		this.zoom.event(isUpdate ? this.svg.transition().duration(500) : d3.select("svg"));
	    	}
	    	else if(theNodes.length>1){
	    		this.scaleToFit();
	    	}
	    }	    
      }
    }
  };

  return ProcessModelInServer;
})();
