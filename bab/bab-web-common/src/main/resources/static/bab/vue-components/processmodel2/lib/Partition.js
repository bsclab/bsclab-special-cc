Partition = (function(){
  function Partition(options){
    this.complete = options.complete;
    this.start = options.start;
    this.part = options.part;
    this.state = 'NA';
    this.nextPartitionStart = options.nextPartitionStart || -1;
    
    var arrString = options.url.split("/");
    this.url = apiURI.model.logreplaypart+arrString[2]+'/'+arrString[3]+'/'+arrString[4]+'/'+arrString[5]+'/part'+this.part + '/lrmodel'; 
//    	
    this.isLast = options.isLast || false;
    this.isActive = false;
  }

  Partition.prototype = {

  };

  return Partition;
})();
