	document.addEventListener("DOMContentLoaded", function(event) {
		var m = document.getElementsByTagName("nav")[0].children;
		for ( var i in m) {
			var e = m[i];
			d = e;
			if (e.tagName == "A") {
				e.addEventListener("click", navOnMouseClick);
			}
		}
	});
	
	function navOnMouseClick(caller) {
		var id = caller.srcElement.getAttribute("href");
		activateTab(id);
	}
	
	function activateTab(id) {
		var m = document.getElementsByTagName("nav")[0].children;
		for ( var i in m) {
			var e = m[i];
			if (e.tagName == "A") {
				e.classList.remove("active");
				if (e.getAttribute("href") == id) {
					e.classList.add("active");
				}
			}
		}
		var m = document.getElementsByTagName("main")[0].children;
		for ( var i in m) {
			var e = m[i];
			if (e.tagName == "SECTION") {
				e.classList.remove("active");
				if ("#" + e.getAttribute("id") == id) {
					e.classList.add("active");
				}
			}
		}
	}

	function vizProcessParam(alg, req) {
		var $alg 	= document.getElementById(alg);
		var $req 	= document.getElementById(req);
		var $res 	= document.getElementById("vizProcRes");
		$res.value = "REQUESTING ...";
		var $xhttp = new XMLHttpRequest();
		$xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (this.status == 200) {
					$req.value = this.responseText;
					$res.value = "DONE !";
				} else {
					$res.value = "ERROR !";
				}
			}
		};
		$xhttp.open("POST", "test/default", true);
		var $data = new FormData();
		$data.append("alg", $alg.value);
		$xhttp.send($data);
	}
	
	var vizTrial = 0;
	var maxTrial = 1;
	var delayTry = 1000;
	
	function runVizProcess() {
		vizTrial = 0;
		vizProcess();
	}
	
	function vizProcess() {
		var $alg 	= document.getElementById("vizProcAlg");
		var $req 	= document.getElementById("vizProcReq");
		var $viz 	= document.getElementById("vizProcViz");
		var $res 	= document.getElementById("vizProcRes");
		var $are 	= document.getElementById("vizProcArea");
		var $btn 	= document.getElementById("vizProcBtn");
		$res.value = "REQUESTING ...";
		$are.innerHTML = "UPDATING ...";
		$alg.setAttribute("disabled", "disabled");
		$btn.setAttribute("disabled", "disabled");
		$viz.setAttribute("disabled", "disabled");
		var $xhttp = new XMLHttpRequest();
		$xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				var $error = true;
				if (this.status == 200) {
					$res.value = this.responseText;
					try {
						switch ($viz.value) {
							case "process":
								var image = Viz(this.responseText);
								$are.innerHTML = image;
								$are.getElementsByTagName("svg")[0].style.width = "100%";
								$are.getElementsByTagName("svg")[0].style.height = "100%";
								$error = false;
								break;
							case "matrix2d":
								if (this.responseText != "{}") {
									$are.innerHTML = this.responseText;
									$error = false;
								}
								break;
							case "json":
								if (this.responseText != "{}") {
									var jsonViewer = new JSONViewer();
									$are.innerHTML = "";
									document.querySelector("#vizProcArea").appendChild(jsonViewer.getContainer());
									jsonViewer.showJSON(JSON.parse(this.responseText));
									$error = false;
								}
								break;
						}
					} catch (e) {
					}
				}
				if ($error) {
					if (this.responseText == "N/A") {
						$are.innerHTML = "--- N/A ---";
						$alg.removeAttribute("disabled");
						$btn.removeAttribute("disabled");
						$viz.removeAttribute("disabled");
					} else {
						if (vizTrial < maxTrial) {
							$are.innerHTML = "--- waiting (" + vizTrial + "/" + maxTrial + ") ---";
							setTimeout("vizProcess()", delayTry);
						} else {
							$are.innerHTML = "--- ERROR ---";
							$alg.removeAttribute("disabled");
							$btn.removeAttribute("disabled");
							$viz.removeAttribute("disabled");
						}
					}
				} else {
					$alg.removeAttribute("disabled");
					$btn.removeAttribute("disabled");
					$viz.removeAttribute("disabled");
				}
			}
		};
		$xhttp.open("POST", "test/json", true);
		var $data = new FormData();
		$data.append("alg", $alg.value);
		$data.append("req", $req.value);
		$data.append("viz", $viz.value);
		$xhttp.send($data);
		vizTrial++;
	}