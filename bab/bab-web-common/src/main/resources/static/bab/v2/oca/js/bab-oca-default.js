var $loadDelay = 3000;
var $maxRetries = 10;
var $workspaceId = "default";
var $datasetId = "default";
var $repositoryId = "1";
var $filterId = "";
var $repositoryURI = $workspaceId + "/" + $datasetId + "/" + $repositoryId;
var $serviceUri = 'test/json';

var $lsView = "ls";
var $pdView = "process";
var $snaView = "matrix2d";

var $tmView = "matrix2d";
var $dcView = "matrix2d";
var $pcView = "json";
var $arView = "json";

var $vizEnabled = {

	"container-ls-c": true,
	"container-ls-a": true,
	"container-ls-o": true,
	"container-ls-r": true,

	"container-dc": true,
	"container-ls-t": true,

	"container-pd-h": true,
	"container-pd-f": true,
	"container-pd-b": true,
	"container-pd-t": true,

	"container-tm": true,
	"container-sna-h": true,
	"container-sna-t": true,
	"container-sna-w": true,
	"container-sna-s": true,
	"container-sna-r": true,

	"container-pc": true,
	"container-ar": true,
	
};

function babOcaStart() {
	
	// Data Perspective

	loadTestService($serviceUri, 'container-ls-c', 1, $maxRetries, $loadDelay, {
		"alg": "RepositoryLogSummary",
		"viz": $lsView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "repositoryTab" : false,
			  "caseTab" : true,
			  "timelineTab" : false,
			  "activityTab" : false,
			  "originatorTab" : false,
			  "resourceTab" : false
		}),
		"lsDim1": "caseTab",
		"lsDim2": "caseDurations"
	});	

	loadTestService($serviceUri, 'container-ls-a', 1, $maxRetries, $loadDelay, {
		"alg": "RepositoryLogSummary",
		"viz": $lsView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "repositoryTab" : false,
			  "caseTab" : false,
			  "timelineTab" : false,
			  "activityTab" : true,
			  "originatorTab" : false,
			  "resourceTab" : false
		}),
		"lsDim1": "activityTab",
		"lsDim2": "frequency"
	});	

	loadTestService($serviceUri, 'container-ls-o', 1, $maxRetries, $loadDelay, {
		"alg": "RepositoryLogSummary",
		"viz": $lsView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "repositoryTab" : false,
			  "caseTab" : false,
			  "timelineTab" : false,
			  "activityTab" : false,
			  "originatorTab" : true,
			  "resourceTab" : false
		}),
		"lsDim1": "originatorTab",
		"lsDim2": "frequency"
	});	

	loadTestService($serviceUri, 'container-ls-r', 1, $maxRetries, $loadDelay, {
		"alg": "RepositoryLogSummary",
		"viz": $lsView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "repositoryTab" : false,
			  "caseTab" : false,
			  "timelineTab" : false,
			  "activityTab" : false,
			  "originatorTab" : false,
			  "resourceTab" : true
		}),
		"lsDim1": "resourceTab",
		"lsDim2": "frequency"
	});	
	
	// Time Perspective
	
	loadTestService($serviceUri, 'container-dc', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisDottedChartJob",
		"viz": $dcView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "component" : "CASE",
			  "shape" : "TYPE",
			  "color" : "ACTIVITY",
			  "timeFormat" : "yyyyMMddHHmmss",
			  "timeSplit" : 10,
			  "timeRelative" : false
		})
	});

	loadTestService($serviceUri, 'container-ls-t', 1, $maxRetries, $loadDelay, {
		"alg": "RepositoryLogSummary",
		"viz": $lsView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "repositoryTab" : false,
			  "caseTab" : false,
			  "timelineTab" : true,
			  "activityTab" : false,
			  "originatorTab" : false,
			  "resourceTab" : false
		}),
		"lsDim1": "timelineTab",
		"lsDim2": "activeCasesOverTime"
	});
	
	// Case Perspective
	
	loadTestService($serviceUri, 'container-pd-h', 1, $maxRetries, $loadDelay, {
		"alg": "ModelHeuristicJob",
		"viz": $pdView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "threshold" : {
			    "dependency" : 0.5,
			    "relativeToBest" : 0.05,
			    "l1Loop" : 0.9,
			    "l2Loop" : 0.9,
			    "longDistance" : 0.9,
			    "and" : 0.1,
			    "dependencyDivisor" : 1
			  },
			  "positiveObservation" : 0,
			  "option" : {
			    "allConnected" : false,
			    "longDistance" : false
			  },
			  "cases" : [ ]
		})
	});

	loadTestService($serviceUri, 'container-pd-t', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisTimeGapJob",
		"viz": $pdView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "successiveOnly" : false,
			  "selectedPIs" : { },
			  "startActivity" : null,
			  "endActivity" : null,
			  "caseIds" : null
		})
	});
	
	
	loadTestService($serviceUri, 'container-pd-f', 1, $maxRetries, $loadDelay, {
		"alg": "ModelFuzzyJob",
		"viz": $pdView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "node" : null,
			  "edge" : null,
			  "concurrency" : null
		})
	});

	loadTestService($serviceUri, 'container-pd-b', 1, $maxRetries, $loadDelay, {
		"alg": "ModelHeuristicBpmnJob",
		"viz": $pdView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "threshold" : {
			    "dependency" : 0.5,
			    "relativeToBest" : 0.05,
			    "l1Loop" : 0.9,
			    "l2Loop" : 0.9,
			    "longDistance" : 0.9,
			    "and" : 0.1,
			    "dependencyDivisor" : 1
			  },
			  "positiveObservation" : 0,
			  "option" : {
			    "allConnected" : false,
			    "longDistance" : false
			  },
			  "cases" : [ ]
		})
	});
	
	// Originator Perspective

	loadTestService($serviceUri, 'container-tm', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisTaskMatrixJob",
		"viz": $tmView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "row" : "event.activity+event.type",
			  "column" : "event.activity+event.type",
			  "unit" : "frequency"
		})
	});
	
	loadTestService($serviceUri, 'container-sna-h', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisSocialNetworkJob",
		"viz": $snaView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "dimension" : "event.originator",
			  "states" : [ ],
			  "method" : {
			    "algorithm" : "handoverofwork",
			    "casuality" : true,
			    "directSubcontract" : {
			      "beta" : 0.05000000074505806,
			      "depth" : 0
			    },
			    "workingTogether" : "simultaneousRatio",
			    "distance" : "euclidean",
			    "multipleTransfer" : true
			  }
		})
	});

	loadTestService($serviceUri, 'container-sna-t', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisSocialNetworkJob",
		"viz": $snaView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "dimension" : "event.originator",
			  "states" : [ ],
			  "method" : {
			    "algorithm" : "similartask",
			    "casuality" : true,
			    "directSubcontract" : {
			      "beta" : 0.05000000074505806,
			      "depth" : 0
			    },
			    "workingTogether" : "simultaneousRatio",
			    "distance" : "euclidean",
			    "multipleTransfer" : true
			  }
		})
	});

	loadTestService($serviceUri, 'container-sna-w', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisSocialNetworkJob",
		"viz": $snaView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "dimension" : "event.originator",
			  "states" : [ ],
			  "method" : {
			    "algorithm" : "workingtogether",
			    "casuality" : true,
			    "directSubcontract" : {
			      "beta" : 0.05000000074505806,
			      "depth" : 0
			    },
			    "workingTogether" : "simultaneousRatio",
			    "distance" : "euclidean",
			    "multipleTransfer" : true
			  }
		})
	});

	loadTestService($serviceUri, 'container-sna-s', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisSocialNetworkJob",
		"viz": $snaView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "dimension" : "event.originator",
			  "states" : [ ],
			  "method" : {
			    "algorithm" : "subcontracting",
			    "casuality" : true,
			    "directSubcontract" : {
			      "beta" : 0.05000000074505806,
			      "depth" : 0
			    },
			    "workingTogether" : "simultaneousRatio",
			    "distance" : "euclidean",
			    "multipleTransfer" : true
			  }
		})
	});

	loadTestService($serviceUri, 'container-sna-r', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisSocialNetworkJob",
		"viz": $snaView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "dimension" : "event.originator",
			  "states" : [ ],
			  "method" : {
			    "algorithm" : "reassignment",
			    "casuality" : true,
			    "directSubcontract" : {
			      "beta" : 0.05000000074505806,
			      "depth" : 0
			    },
			    "workingTogether" : "simultaneousRatio",
			    "distance" : "euclidean",
			    "multipleTransfer" : true
			  }
		})
	});

	// Other Perspective
	
	loadTestService($serviceUri, 'container-pc', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisPerformanceChartJob",
		"viz": $pcView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "series" : "Task",
			  "y" : "Sum",
			  "z" : "Weekly",
			  "unit" : "seconds",
			  "calculation" : "Working Time"
		})
	});

	loadTestService($serviceUri, 'container-ar', 1, $maxRetries, $loadDelay, {
		"alg": "AnalysisAssociationRuleJob",
		"viz": $arView,
		"req": JSON.stringify({
			  "uri" : null,
			  "repositoryURI" : $repositoryURI,
			  "workspaceId" : $workspaceId,
			  "datasetId" : $datasetId,
			  "repositoryId" : $repositoryId,
			  "filterId" : $filterId,
			  "algorithm" : "apriori",
			  "threshold" : {
			    "support" : {
			      "min" : 0.2,
			      "max" : 1.0
			    },
			    "confidence" : {
			      "min" : 0.4,
			      "max" : 1.0
			    }
			  },
			  "noOfRules" : 10,
			  "noOfRelation" : 0,
			  "profiles" : [ ]
		})
	});
}