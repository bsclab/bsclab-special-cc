function loadTestService($uri, $containerId, $counter, $maxCounter, $loadingDelay, $params) {
	var $cont = document.getElementById($containerId);
	if (!$vizEnabled[$containerId]) {
		$cont.innerHTML = "This view is disabled.";
		return;
	}
	$cont.innerHTML = "Loading (" + $counter + "/" + $maxCounter + ") ...";
	var $xhttp = new XMLHttpRequest();
	$xhttp.onreadystatechange = function() {
		if (this.readyState == 4) {
			var $error = true;
			if (this.status == 200) {
				try {
					switch ($params["viz"]) {
						case "process":
							var $image = Viz(this.responseText);
							$cont.innerHTML = $image;
							$cont.getElementsByTagName("svg")[0].style.width = "100%";
							$cont.getElementsByTagName("svg")[0].style.height = "100%";
							$error = false;
							break;
						case "matrix2d":
							if (this.responseText != "{}") {
								$cont.innerHTML = this.responseText;
								$error = false;
							}
							break;
						case "json":
							if (this.responseText != "{}") {
								var jsonViewer = new JSONViewer();
								$cont.innerHTML = "";
								document.querySelector("#" + $containerId).appendChild(jsonViewer.getContainer());
								jsonViewer.showJSON(JSON.parse(this.responseText));
								$error = false;
							}
							break;
						case "ls":
							if (this.responseText != "{}") {
								var $dim1 = $params["lsDim1"];
								var $dim2 = $params["lsDim2"];
								var $lsData = JSON.parse(this.responseText)[$dim1][$dim2];
								var $colData = [[$dim1 + "(" + $dim2 + ")"]];
								var $catData = [];
								for (var $ci in $lsData) {
									$colData[0][$colData[0].length] = $lsData[$ci];
									$catData[$catData.length] = $ci;
								}
								var $chart = c3.generate({
									bindto: '#' + $containerId,
									size: {
									    height: 250
									},
								    data: {
								        columns: $colData,
								        type: 'bar'
								    },
								    axis: {
								        x: {
								            type: 'category',
								            categories: $catData
								        }
								    }
								});
								$cont.getElementsByTagName("svg")[0].style.width = "100%";
								$cont.getElementsByTagName("svg")[0].style.height = "100%";
								$error = false;
							}
							break;
					}
				} catch (e) {
					console.log(e);
				}
			}
			if ($error) {
				if (this.responseText == "N/A") {
					$cont.innerHTML = "--- N/A ---";
				} else {
					if ($counter < $maxCounter) {
						setTimeout(function() {
							loadTestService($uri, $containerId, $counter + 1, $maxCounter, $loadingDelay, $params);
						}, $loadingDelay);
//								"loadTestService('" + $uri + "', '" + $containerId + "', " + $counter + ", " + $maxCounter + ", " + $loadingDelay + ", '" + JSON.stringify($params) + "')", $loadingDelay);
					} else {
						$cont.innerHTML = "--- ERROR ---";
					}
				}
			}
		}
	};
	$xhttp.open("POST", $uri, true);
	var $data = new FormData();
	for (var $i in $params) {
		$data.append($i, $params[$i]);
	}
	$xhttp.send($data);
}