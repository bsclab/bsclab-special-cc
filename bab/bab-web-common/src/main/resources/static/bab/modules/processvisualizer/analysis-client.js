new ProcessModelComponent({
  el: '#app',
  components: {
	'loader': LoaderComponent,
	'slider': SliderComponent
  },
  data: {
	lrData: undefined,
    pmData: {
    	URI: apiURI.model.heuristic + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt,
    	config: config,
		options: {
	        is3D: false,
	        rankdir: "TB",
	        arctype: 'bundle',
	        isLogReplay: false,
	        type: 'heuristic'
	    }
    }
  },
  ready: function(){
//	  console.log('processmodel ready');
  },
  methods: {
	  updateProcessModel: function(nodes, arcs, originalConfig){
//		  console.log(_.keys(nodes).length, 'nodes', _.keys(arcs).length, 'arcs');
		  var start = new Date();
		  
		  this.pmData.data.numberOfNodes = _.keys(nodes).length;
		  this.pmData.data.numberOfArcs = _.keys(arcs).length;
		  this.processModel.update(nodes, arcs, this.pmData.options);
		  this.processModel.drawProcessModel();
		  
		  var end = new Date();
		  var duration = end - start;
//		  console.log('duration '+duration+'ms');
		  
		  // reset to previous value
//		  console.log(this);
		  this.$set('pmData.config', JSON.parse(originalConfig))
		  // access children component by ref ID, the ID is set in the component using v-ref in parent template
		  this.$refs.sliderdependency.setSliderValue(this.pmData.config.threshold.dependency);
		  this.$refs.sliderpositiveobservation.setSliderValue(this.pmData.config.positiveObservation);
	  },
	  submitHeuristic: function(){
		  var intervalDuration = 3000;
		  var URI = this.pmData.URI;
		  
		  if(this.pmData.waiting){
			  alert('wait for previous processing')
		  }
		  else{
//			  console.log(this.pmData.config);
//		  var args = JSON.flatten(config);
//		  delete args.repositoryURI;	// remove attribute
			  
			  /*var args = JSON.stringify({
				  "positiveObservation": parseInt(this.pmData.config.positiveObservation),
				  "option.allConnected": this.pmData.config.option.allConnected, 
				  "threshold.dependency": parseFloat(this.pmData.config.threshold.dependency)
			  });*/

			  var args = JSON.stringify({
				  "positiveObservation": parseInt(this.pmData.config.positiveObservation),
				  "option": {
					  "allConnected": this.pmData.config.option.allConnected
				  },
				  "threshold": {
					  "dependency": parseFloat(this.pmData.config.threshold.dependency)
				  }
			  });
			  
			  var originalConfig = JSON.stringify(this.pmData.config);
			  
			  this.pmData.waiting = true;
			  console.log("[STEP1] request : " + URI);
			  this.$http.post(URI, args).then(function (response) {
				  // success callback
				  if(response.data.status == 'FINISHED'){
					  this.updateProcessModel(response.data.response.nodes, response.data.response.arcs, originalConfig);
					  this.pmData.waiting = false;
				  }
				  else if(response.data.status == 'FAILED'){
					  // if status is not FINISHED or RUNNING
					  this.pmData.waiting = false;
				  }
				  else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUE'){
					  // if status is running then check for each 3000 second
					  var intervalDuration = 3000;
					  var intervalId = window.setInterval(_.bind(function(){
//						  console.log('this is check every '+intervalDuration+'ms');
						  this.$http.get(apiURI.job.queue).then(function(response2){
							  // success callback
							  var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.id}));
							  var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.id}));
							  
							  if(isJobInQueue || isJobInRunning){
//								  console.log('current status is QUEUE/RUNNING');
								  // do nothing
							  }
							  else{
//								  console.log('current status is FINISHED, request POST');
								  this.$http.post(URI, args).then(function (response3) {
									  // error callback
									  if(response3.data.status == 'FINISHED'){
										  window.clearInterval(intervalId)
										  this.updateProcessModel(response3.data.response.nodes, response3.data.response.arcs, originalConfig);
										  this.pmData.waiting = false;
									  }
								  }, function(response3){
									  this.pmData.waiting = false;
								  });
							  }
						  }, function(response2){
							  // error callback
							  this.pmData.waiting = false;
						  })			  
						  
					  }, this), intervalDuration);					  
				  }
			  }, function(response){
				  // error callback
				  this.pmData.waiting = false;
			  });			  
		  }
	  }
  }
});