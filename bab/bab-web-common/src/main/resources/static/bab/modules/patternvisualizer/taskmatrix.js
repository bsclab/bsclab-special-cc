new Vue({
  el: '#app',
  components: {
	  'loader': LoaderComponent
  },
  data: {
	  isProcessing: false,
	  responseData: ""
	},
	ready: function(){
		
		$('.pointing.menu .item').tab();
		
		var taskmatrixURI =  apiURI.analysis.taskmatrix+jsonData.workspaceId+'/'+jsonData.datasetId
			+'/'+jsonData.sdt+'/'+jsonData.edt;
		var jobQueueURI = apiURI.job.queue;
		var interval;
		var intervalDuration = 1000;

		this.isProcessing = true;
		
		console.log("[STEP1-1] req : " + taskmatrixURI);
		var httpGet = this.$http;
		interval = setInterval(function(){
			httpGet.get(taskmatrixURI).then(function(response) {
				console.log(response);
				var taskmatrixRes = response;
				var taskmatrixResStatus = taskmatrixRes.data.status;
				
				if (taskmatrixResStatus === 'FINISHED') {
					this.isProcessing = false;
					drawTaskmatrix(this, taskmatrixRes.data.response);
					console.log(taskmatrixRes);
					window.clearInterval(interval);
//					window.location.reload();
				} else if (taskmatrixResStatus === 'FAILED') {
					alert('FAILED');
					window.clearInterval(interval);
				} else if (taskmatrixResStatus === 'RUNNING' || taskmatrixResStatus == 'QUEUED') {
					var taskmatrixQueueChecker = window.setInterval(_.bind(function() {
						console.log("[STEP1-2] req : " + jobQueueURI + summaryViewRes.data.request.id);
						this.$http.get(jobQueueURI + summaryViewRes.data.request.id).then(function(response) {
							console.log(response)
							var taskmatrixQueueRes = response;
							var taskmatrixQueueResStatus = response.data.status;
							
							if (taskmatrixQueueResStatus === 'FINISHED') {
								window.clearInterval(interval);
								window.clearInterval(taskmatrixQueueChecker);
								window.location.reload();
							} else if (taskmatrixQueueResStatus === 'FAILED') {
								window.clearInterval(taskmatrixQueueChecker);
								window.clearInterval(interval);
								alert("FAILED")
								console.log("[SETP1-3] req : " + taskmatrixURI);
								this.$http.get(taskmatrixURI).then(function(response) {
									console.log(response);
									this.isProcessing = false;
									drawTaskmatrix(this, response.data.response);
									clearInterval(interval);
								});
							}
						});
					}, this), intervalDuration);
				}
			});
		}, intervalDuration);
	},
	methods: {
		updateBtn: function(){
			var row, column, unit, order;
//			console.log($('#row option:selected').index());
			switch ($('#row option:selected').index()) {
			case(0):
				row = 'event.activity+event.type';
			break;
			case(1):
				row = 'event.activity';
				break;
			case(2):
				row = 'event.originator';
				break;
			case(3):
				row = 'event.resource';
				break;
			}
//			console.log($('#column option:selected').index());
			switch ($('#column option:selected').index()) {
			case(0):
				column = 'event.activity+event.type';
			break;
			case(1):
				column = 'event.activity';
				break;
			case(2):
				column = 'event.originator';
				break;
			case(3):
				column = 'event.resource';
				break;
			}
			
			switch ($('#unit option:selected').index()) {
			case(0):
				unit = 'frequency';
			break;
			case(1):
				unit = 'duration';
				break;
			}
			var self = this;
			self.isProcessing = true;
			var status = "";
			var interval = window.setInterval(function() {
				// call API
				self.$http.post(apiURI.analysis.taskmatrix+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.sdt+'/'+jsonData.edt, JSON.stringify({
					'row': row,
					'column':column,
					'unit': unit })
				).then(function(response) {
//					console.log("start Interval",response);
					status = response.data.status;
					if (status == "FINISHED") {
//						console.log("finished");

						if(!_.isNull(response.data.response)){
							var data = response.data.response;
							$('#content-graph').empty();
//							this.$http({
//								url : "/bab/api/v1_0/job/webhdfs"
//									, method : 'POST' 
//									, data : {
//										'path' : data.casesUrl
//									}
//							}).then(function(response) {
//								drawHeatMap(data, response.data.caseMap);
//							})
							drawHeatMap(data, null);
					      }

//						console.log("end Interval", interval);
						self.isProcessing = false;
						window.clearInterval(interval);
					} else if(status == "RUNNING"){
//						console.log("wait");
					} else {
//						console.log("error");
						self.isProcessing = false;
						window.clearInterval(interval);
					}

				});	
			}, 3000);
		}
	}
});

function drawTaskmatrix(vue, data) {
	var caseMap;
	var config = {
		'path' : data.casesUrl
	}
//	vue.$http.post("/bab/api/v1_0/job/webhdfs", config).then(function(response) {
//		drawHeatMap(data, response.data.caseMap);
//	}, function(response) {
//		console.log(response);
//	});
	drawHeatMap(data, null);
	var min = data.statistics.min;
	var max = data.statistics.max;
	var mean = data.statistics.mean;
	var stdev = 1;
	var dataCell = data.heatList.length;
	$('#minText').text(BabHelper.formatNumber(min, "0,0"));
	$('#maxText').text(BabHelper.formatNumber(max, "0,0"));
	$('#meanText').text(BabHelper.formatNumber(mean, "0,0.00"));
	$('#stdText').text(BabHelper.formatNumber(stdev, "0,0"));
	$('#nullText').text(BabHelper.formatNumber(dataCell - data.statistics.count, "0,0"));
	$('#notnullText').text(BabHelper.formatNumber(data.statistics.count, "0,0"));
}

function updateMovement($val){
    $('#bad').css('width', $val[0] + '%');
    $('#badLabel').text(BabHelper.printNumber($val[0], 2) + '%');

    $('#neutral').css('width', $val[1] + '%');
    $('#neutralLabel').text(BabHelper.printNumber($val[1], 2) + '%');

    $('#good').css('width', $val[2] + '%');
    $('#goodLabel').text(BabHelper.printNumber($val[2], 2) + '%');
}


function wrap(text, width, r) {
    if (r == "x") {
        lineHeight = 0.9, // ems
        x = -6;
        min = 0.5;
    }
    else if(r == "y") {
        lineHeight = 0.1, // ems
        x = 6;
        min = 0;
    }

    if (text.text().length > width)
    text.each(function() {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(), word,
            line = [],
            lineNumber = 0,
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");
        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy - min + "em").text(word);
            }
        }
    });
}

function drawHeatMap(data, caseMap) {
//	console.log(this.caseMap);
	if(data.unit == "frequency") {
		$('.unitTitle').show();
		$('.unitTitleDuration').hide();
	} else {
		$('.unitTitle').hide();
		$('.unitTitleDuration').show();
	}
			
	var content = "#content-graph";
	var margin = {top: 150, right: 10, bottom: 50, left: 10};
	var col_number = data.hccol.length;// 60;
	var row_number = data.hcrow.length;// 50;
	var width = $("#content-graph").width();	//align center // args.width,//cellSize*col_number, // -
			// margin.left - margin.right,
//	console.log(width);
	var height = $("#content-graph").height(); // args.height,//cellSize*row_number , // -
			// margin.top - margin.bottom,
	//var moreSize = (col_number > row_number) ? width : height;
	var cellSize = (col_number > row_number) ? width/col_number : height/row_number;//height / col_number;
//	console.log(cellSize);
	var legendElementWidth = cellSize*2.5;
	var colorBuckets = 21;
	var hcrow = data.hcrow;
	var hccol = data.hccol;
	var rowLabel = data.rowLabel;
	var colLabel = data.colLabel;
	var totalRatio = 0;
//	 var url = args.baseUrl+data.casesUrl;
	var myUnit = data.unit;
	var heat = data.heatList;
//	console.log(heat);
	
//	d3.json(heat, function(d) {
		var dataCell = [], i=0, variance = 0, stdev = 0, min, max, mean;
		var neutral = 0, good = 0, bad = 0, ratio = 0;
		heat.forEach(function (d){
//			console.log(d);
    		if (!d.empty)
	            variance += Math.pow((data.statistics.mean - d.value), 2);

	        ratio = d.value/data.statistics.max;//d.log2ratio
	        totalRatio = totalRatio+ratio;
	        dataCell[i++] = {
	            row:   d.source,//d.row_idx,
	            col:   d.target,//d.col_idx,
	            value: ratio,//d.value/data.statistics.max,//d.log2ratio
	            oValue: d.value, // original value
	            isEmpty: d.empty,
	            casesNumber: d.casesNumber,
	            movement: {
	                neutral: 1,
	                good: 0,
	                bad: 0
	            }
	        };
//	        if (!d.empty)
//	          console.log(rowLabel[d.source]+"=>"+colLabel[d.target]+" = "+d.value);
		  });
	    stdev = Math.sqrt(variance/data.statistics.count);
	    if (data.unit == 'duration'){
	        min = BabHelper.printTimeDuration(data.statistics.min);
	        max = BabHelper.printTimeDuration(data.statistics.max);
	        mean = BabHelper.printTimeDuration(data.statistics.mean);
	        stdev = BabHelper.printTimeDuration(stdev);
	    }else{
	        min = BabHelper.printNumber(data.statistics.min);
	        max = BabHelper.printNumber(data.statistics.max);
	        mean = BabHelper.printNumber(data.statistics.mean);
	        stdev = BabHelper.printNumber(stdev);
	    }
//	    cellSize = (width + margin.left + margin.right) / Math.max(col_number, row_number);
//	    if (cellSize < 25)
//	      cellSize = (width + margin.left + margin.right) / Math.min(col_number, row_number);
	    
	    
	    // 'Relative Frequency' color legends
	    var colorScale = d3.scale.quantize()
	        .range(colorbrewer.Oranges[9]);
	    
	    $('#legend').empty();	// color legends clear
	    var legend = d3.select('#legend')
	    	.append('table')
	    	.style('width', '100%');
	
	    var keys = legend.selectAll('td.key')
	    	.data(colorScale.range());
	    
	    keys.enter().append('td')
	    	.attr('class', 'key')
	    	.style('border-top-color', String)
	    	.text(function(d) {
		        var r = colorScale.invertExtent(d);
		        return d3.round(r[0],2);
		});

	    
	    var svg = d3.select(content).append("svg")
// 		                              .attr("id", "taskmatrix")
                            		  .attr("width", width)
                            		  .attr("height", height)
                                      .attr("viewBox", "0 0 "+(width+ margin.left + margin.right)+" "+(height+ margin.top + margin.bottom))
                            		  .append("g")
                            		  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	     var chart = $("#taskmatrix"),
	         aspect = (width + margin.left + margin.right) / (height + margin.top + margin.bottom);
	     $(window).on("resize", function() {
	         var targetWidth = $(content).width();
	         chart.attr("width", targetWidth);
	         chart.attr("height", Math.round(targetWidth / aspect));
	     }).trigger("resize");
	    
	    var rowSortOrder=false;
		var colSortOrder=false;
		
		var movement = [0, 100, 0];
		updateMovement(movement);
		
		

		var rowLabels = svg.append("g")
	                  		.selectAll(".rowLabelg")
	                  		.data(rowLabel)
	                  		.enter()
	                  		.append("text")
	                  		.text(function (d) { return d; })
	                  		.attr("x", 0)
	                  		.attr("y", function (d, i) { return hcrow.indexOf(i+1) * cellSize - 10; })
	                        .attr("dy", "0em").call(wrap, 20, "x")
	                  		.style("text-anchor", "end")
	                  		.attr("transform", "translate(-6," + cellSize / 1.5 + ")")
	                  		.attr("class", function (d,i) { return "rowLabel mono r"+i;} )
	                  		//.on("mouseover", function(d) {d3.select(this).classed("text-hover",true);})
	                  		//.on("mouseout" , function(d) {d3.select(this).classed("text-hover",false);})
	                  		//.on("click", function(d,i) {rowSortOrder=!rowSortOrder;sortbylabel("r",i,rowSortOrder);d3.select("#order").property("selectedIndex", 4).node().focus();});

		  var colLabels = svg.append("g")
	                  		  .selectAll(".colLabelg")
	                  		  .data(colLabel)
	                  		  .enter()
	                  		  .append("text")
	                  		  .text(function (d) { return d; })
	                  		  .attr("x", -5)
	                  		  .attr("y", function (d, i) { return hccol.indexOf(i+1) * cellSize - 7; })
	                  		  .attr("dy", "0em").call(wrap, 20, "x")
	                  		  .style("text-anchor", "left")
	                  		  .attr("transform", "translate("+cellSize/2 + ",-20) rotate (-90)")
	                  		  .attr("class",  function (d,i) { return "colLabel mono c"+i;} )
	                  		  //.on("mouseover", function(d) {d3.select(this).classed("text-hover",true);})
	                  		  //.on("mouseout" , function(d) {d3.select(this).classed("text-hover",false);})
	                  		  //.on("click", function(d,i) {colSortOrder=!colSortOrder;sortbylabel("c",i,colSortOrder);d3.select("#order").property("selectedIndex", 4).node().focus();});
		  // graph click sort
		  function sortbylabel(rORc,i,sortOrder){
			  var t = svg.transition().duration(3000);
			  var log2r=[];
			  var sorted; // sorted is zero-based index
			  d3.selectAll(".c"+rORc+i)
			  	.filter(function(ce){
			  		log2r.push(ce.value);
//			  		console.log(ce.value);
			  	});
//			  console.log(log2r);
			  if(rORc=="r"){ // sort log2ratio of a gene
				  sorted=d3.range(col_number).sort(function(a,b){ if(sortOrder){ return log2r[b]-log2r[a];}else{ return log2r[a]-log2r[b];}});
				  t.selectAll(".cell")
				  	.attr("x", function(d) { 
//				  		console.log(sorted.indexOf(d.col-1));
				  		return (sorted.indexOf(d.col-1)+1) * cellSize; });
				  t.selectAll(".colLabel")
				  	.attr("y", function (d, i) { return sorted.indexOf(i) * cellSize; });
			  }else{ // sort log2ratio of a contrast
				  sorted=d3.range(row_number).sort(function(a,b){
					  if(sortOrder){ return log2r[b]-log2r[a];}else{ return log2r[a]-log2r[b];}});
				  t.selectAll(".cell")
				  	.attr("y", function(d) { return (sorted.indexOf(d.row-1)+1) * cellSize; });
				  t.selectAll(".rowLabel")
				  	.attr("y", function (d, i) { return sorted.indexOf(i) * cellSize; });
			  }
//			  console.log(sorted);
		  }
		  
		  var heatMap = svg.append("g").attr("class","g3")
                            			.selectAll(".cellg")
                            			.data(dataCell,function(d){return d.row+":"+d.col;})
                            			.enter()
                            			.append("rect")
                            			.attr("x", function(d) { return (d.col) * cellSize; })
                            			.attr("y", function(d) { return (d.row) * cellSize; })
                            			.attr("class", function(d){return "modal cell cell-border cr"+(d.row)+" cc"+(d.col)+(d.isEmpty?" null":"");})
                            			.attr("width", cellSize)
                            			.attr("height", cellSize)
                            			.attr("id", function(d){return "cr"+(d.row)+"-cc"+(d.col);})
//	                                    .attr("data-toggle", "modal")
//	                                    .attr("data-target", "#myModal")
                            			.attr("data-row", function(d){ return rowLabel[d.row]+"|"+d.row; })
                            			.attr("data-col", function(d){ return colLabel[d.col]+"|"+d.col; })
                            			.attr("data-value", function(d){
	                                    return d.isEmpty? "Null": data.unit == 'duration'? BabHelper.printTimeDuration(d.oValue) :
	                                    numeral(d.oValue).format('0,0');
	                                  })
	                                  	.attr("data-ratio", function(d){ return d.isEmpty? "Null": BabHelper.printNumber(d.value, 4); })
	                                  	.attr("data-empty", function(d){ return d.isEmpty; })
	                                  	.attr("data-cases", function(d){ return numeral(d.casesNumber).format('0,0'); })
                            			.style("fill", function(d) { return colorScale(d.value); })
	                                  
                            			.on("mouseover", function(d){
                        				   //highlight text
                        				   d3.select(this).classed("cell-hover",true);
                        				   d3.selectAll(".rowLabel").classed("text-highlight",function(r,ri){ return ri==(d.row);});
                        				   d3.selectAll(".colLabel").classed("text-highlight",function(c,ci){ return ci==(d.col);});
//	                             				   console.log(d.row, d.col);
                        				   //Update the tooltip position and value
                        				   d3.select("#tooltip")
	                                          .style("left", (d3.event.pageX-80) + "px")
	                                          .style("top", (d3.event.pageY-570) + "px")
	                                          .select("#value")
	                                          .html("Labels : ["+rowLabel[d.row]+", "+colLabel[d.col]+"]"+"<br/>"+" Ratio : "+BabHelper.printNumber(d.value,4)+"<br/>"
                                                       +data.unit+"(%) : "+numeral(d.oValue).format('0,0')+"<br/>"+"Row-col-idx : "+d.col+", "+d.row);//+"\ncell-xy "+this.x.baseVal.value+", "+this.y.baseVal.value);
                            				   //Show the tooltip
                            				   d3.select("#tooltip").classed("hidden", false);
                            			 })

                            			 .on("mouseout", function(){
                            				d3.select(this).classed("cell-hover",false);
                            				d3.selectAll(".rowLabel").classed("text-highlight",false);
                            				d3.selectAll(".colLabel").classed("text-highlight",false);
                            				d3.select("#tooltip").classed("hidden", true);
                            			 })
	                            			
                            			.on("click", function(d) {
                            				$("#myModal").modal({
                            				    onShow: function(){},
                            					onHide: function() {
                            						// 'Bad,Neutral,Good' RadioButton select clear
                            						//$("input:radio").removeAttr("checked");
//                            						$("input:radio").attr("checked", false);
                            						
                            						// '.list-group' scroll clear
                            						var scroll = $('body');
                            						$(".list-group").html( scroll.scrollTop() );
                            					}
                            				  })
                            				  // just calling a behavior
                            				  .modal('show');
                            				
                            				// input value
											var rectInfo = this.attributes;
//											console.log(rectInfo);
											var from = rectInfo['data-row'].value.split("|");
									        var to = rectInfo['data-col'].value.split("|");
									        var empty = rectInfo['data-value'].value;
									        $('#from-name-hidden').attr("value", from[1]);
									        $('#to-name-hidden').attr("value", to[1]);
									        
											
											$("#from-name").attr("value", from[0]);							// From:
											$("#to-name").attr("value", to[0]);								// To:
											$("#frequency").attr("value", numeral(d.oValue));				// Frequency:
											$("#ratio").attr("value", BabHelper.printNumber(d.value,4));	// Ratio:
											$("#casesNumber").text(numeral(d.casesNumber).format('0,0'));	// Number of cases:
											
											
											$(".list-group").empty();
											if (typeof caseMap[from[1]+"|"+to[1]] !== 'undefined'){
												var count = 1;
												caseMap[from[1]+"|"+to[1]].forEach(
													function(entry){
//														console.log(entry);
//														if (count < 11)
														$(".list-group").append($("<div>").attr("class", "item").text(entry));
														
													count++;
													}
												);
											}
											
											var data = _.findWhere(dataCell, {row:from[1], col:to[1], isEmpty:false});
//											console.log("click event : ",data);
											if (empty == '' || _.isUndefined(data)){
									            //modal.find('.modal-body .btn-group').hide();
//												console.log("empty");
									            $('#badChoice').hide();
									            $('#neutralChoice').hide();
									            $('#goodChoice').hide();
									            $('#radioButton').hide();
									        } else{
//									        	console.log("!empty");
									            //modal.find('.content .buttons').show();
									        	$('#radioButton').show();
									        	if (data.movement.bad == 1){
									                $('#badChoice').show();
									                $('#neutralChoice').hide();
									                $('#goodChoice').hide();

									                $('#option1').prop('checked', true);
									                $('#btnBad').addClass('active');
									                $('#btnNeutral').removeClass('active');
									                $('#btnGood').removeClass('active');
									            }else if (data.movement.neutral == 1){
									                $('#badChoice').hide();
									                $('#neutralChoice').show();
									                $('#goodChoice').hide();

									                $('#option2').prop('checked', true);
									                $('#btnBad').removeClass('active');
									                $('#btnNeutral').addClass('active');
									                $('#btnGood').removeClass('active');
									            }else if (data.movement.good == 1){
									                $('#badChoice').hide();
									                $('#neutralChoice').hide();
									                $('#goodChoice').show();

									                $('#option3').prop('checked', true);
									                $('#btnBad').removeClass('active');
									                $('#btnNeutral').removeClass('active');
									                $('#btnGood').addClass('active');
									            }
									        }
											
											
											$('input:radio[name="options"]').change(function (){
										        if ($(this).val() == -1){
										            $('#badChoice').show();
										            $('#neutralChoice').hide();
										            $('#goodChoice').hide();
										        }else if ($(this).val() == 0){
										            $('#badChoice').hide();
										            $('#neutralChoice').show();
										            $('#goodChoice').hide();
										        }else if ($(this).val() == 1){
										            $('#badChoice').hide();
										            $('#neutralChoice').hide();
										            $('#goodChoice').show();
										        }
										    })
											
											$('input:radio[name="options"]').change(function (){
										        if ($(this).val() == -1){
										            $('#badChoice').show();
										            $('#neutralChoice').hide();
										            $('#goodChoice').hide();
										        }else if ($(this).val() == 0){
										            $('#badChoice').hide();
										            $('#neutralChoice').show();
										            $('#goodChoice').hide();
										        }else if ($(this).val() == 1){
										            $('#badChoice').hide();
										            $('#neutralChoice').hide();
										            $('#goodChoice').show();
										        }
										    })
											
                            			});
		  
		  $('#saveMovement').click(function (){
		        var opt = $('input:radio[name=options]:checked').val();
		        var x = $('input:hidden[name=fromHidden]').val();
		        var y = $('input:hidden[name=toHidden]').val();

//		        console.log(opt+" pilihan");

		        // find data and pop out
		        var data = _.findWhere(dataCell, {row:x, col:y, isEmpty:false});
		        var data2 = _.where(dataCell, {isEmpty:false});
		        var stroke = '';
//		        console.log(dataCell);
		        if (data !== undefined){
//		            console.log(data);
//		            console.log(data.movement);
		            dataCell = _.without(dataCell, data);
		            
		            // update data and push back
		            if (opt == -1){
		                data.movement = {neutral: 0, good: 0, bad: 1};
		                stroke = '#d43f3a';
		            }else if (opt == 0){
		                data.movement = {neutral: 1, good: 0, bad: 0};
		                stroke = '#31b0d5';
		            }else if (opt == 1){
		                data.movement = {neutral: 0, good: 1, bad: 0};
		                stroke = '#5cb85c';
		            }
		            dataCell.push(data);

		            // count data
		            var neutralData = _.filter(dataCell, function(value){
		                   return !value.isEmpty && value.movement.neutral==1 && value.movement.good==0 && value.movement.bad==0;
		                });
		            var badData = _.filter(dataCell, function(value){
		                   return !value.isEmpty && value.movement.neutral==0 && value.movement.good==0 && value.movement.bad==1;
		                });
		            var goodData = _.filter(dataCell, function(value){
		                   return !value.isEmpty && value.movement.neutral==0 && value.movement.good==1 && value.movement.bad==0;
		                });

		            var badRatio=0, goodRatio=0, neutralRatio=0;

		            badData.forEach(function (d){
		                badRatio += d.value;
		            });
		            neutralData.forEach(function (d){
		                goodRatio += d.value;
		            });
		            goodData.forEach(function (d){
		                neutralRatio += d.value;
		            });

		            var movement = [badRatio/totalRatio*100, goodRatio/totalRatio*100, neutralRatio/totalRatio*100]; // bad, neutral, good
		            updateMovement(movement);

		            var sWidth = d3.select('#cr'+x+'-cc'+y).attr("width") / 10;
		            if (sWidth > 5) sWidth = 5;
		            d3.select('#cr'+x+'-cc'+y).style("stroke-width", Math.floor(sWidth)).style("stroke", stroke);

//		            console.log(sWidth);
//		            console.log(neutralData.length);
//		            console.log(badData.length);
//		            console.log(goodData.length);
		        }
		        

		        // close modal
		        $('#myModal').modal('hide');
		        
		        
		        
		        function sortbylabel(rORc,i,sortOrder){
		        	var t = svg.transition().duration(3000);
		        	var log2r=[];
		        	var sorted; // sorted is zero-based index
		        	d3.selectAll(".c"+rORc+i)
		        		.filter(function(ce){
		        			log2r.push(ce.value);
		        		});
		        	
		        	if(rORc=="r"){ // sort log2ratio of a gene
		        		sorted=d3.range(col_number).sort(function(a,b){ if(sortOrder){ return log2r[b]-log2r[a];}else{ return log2r[a]-log2r[b];}});
		        		t.selectAll(".cell")
		        			.attr("x", function(d) { return sorted.indexOf(d.col-1) * cellSize; });
		        		t.selectAll(".colLabel")
		        			.attr("y", function (d, i) { return sorted.indexOf(i) * cellSize; });
		        	}else{ // sort log2ratio of a contrast
		        		sorted=d3.range(row_number).sort(function(a,b){
		        			if(sortOrder){ return log2r[b]-log2r[a];}else{ return log2r[a]-log2r[b];}});
		        		t.selectAll(".cell")
		        			.attr("y", function(d) { return sorted.indexOf(d.row-1) * cellSize; });
		        		t.selectAll(".rowLabel")
		        			.attr("y", function (d, i) { return sorted.indexOf(i) * cellSize; });
		        	}
//		        	console.log(sorted);
		        }
		    });
//	  });
	

	d3.select("#order").on("change",function(){
		order(this.value);
	});

	function order(value) {
		if (value == "hclust") {
			var t = svg.transition().duration(3000);
			t.selectAll(".cell").attr("x", function(d) {
				return hccol.indexOf(d.col) * cellSize;
			}).attr("y", function(d) {
				return hcrow.indexOf(d.row) * cellSize;
			});

			t.selectAll(".rowLabel").attr("y", function(d, i) {
				return hcrow.indexOf(i + 1) * cellSize;
			});

			t.selectAll(".colLabel").attr("y", function(d, i) {
				return hccol.indexOf(i + 1) * cellSize;
			});

		} else if (value == "probecontrast") {
			var t = svg.transition().duration(3000);
			t.selectAll(".cell").attr("x", function(d) {
				return (d.col) * cellSize;
			}).attr("y", function(d) {
				return (d.row) * cellSize;
			});

			t.selectAll(".rowLabel").attr("y", function(d, i) {
				return i * cellSize;
			});

			t.selectAll(".colLabel").attr("y", function(d, i) {
				return i * cellSize;
			});

		} else if (value == "probe") {
			var t = svg.transition().duration(3000);
			t.selectAll(".cell").attr("y", function(d) {
				return (d.row) * cellSize;
			});

			t.selectAll(".rowLabel").attr("y", function(d, i) {
				return i * cellSize;
			});
		} else if (value == "contrast") {
			var t = svg.transition().duration(3000);
			t.selectAll(".cell").attr("x", function(d) {
				return (d.col) * cellSize;
			});
			t.selectAll(".colLabel").attr("y", function(d, i) {
				return i * cellSize;
			});
		}
	}
}
