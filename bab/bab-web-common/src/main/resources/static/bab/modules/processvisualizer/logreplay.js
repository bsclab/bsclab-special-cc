new ProcessModelComponent({
  el: '#app',
  components: {
	'loader': LoaderComponent,
	'statistic': StatisticComponent,
	'slider': SliderComponent,
	'tokens': TokensComponent,
  },
  data: {
    pmData: {
	  URI: apiURI.model.heuristic + jsonData.workspaceId + '/' + jsonData.datasetId
	  + '/' + jsonData.sdt + '/' + jsonData.edt,
	  options: {
		  is3D: false,
		  rankdir: "TB",
		  arctype: 'bundle',
		  isLogReplay: true,
		  type: 'heuristic'
	  }
    },
	lrData: {
		tokens: [],
		partitions: [],
		attributes:[
            { key: 'caseId', label: 'Case ID', active: true, items: [] }
        ],
		data: {
			waiting: false,
			waitingLabel: undefined,
			eventsNumber: 0,
			eventsNumberFormatted: '0,0',
			startTime: 0, 
			endTime: 0,
			tokensFinished:0,
			tokensRunning: 0,
			time: 0,
			speed: 1,
			state: 'initial',
			speedSlider: undefined,
			timelineSlider: undefined,
			selectedAttribute: 'caseId',
			showSameColor: false,
			showKPI: false,
			showFadeOut: true,
			showCaseId: false,
		},
		options: {
		
		}
	},
  },
  watch: {
	  'lrData.data.waiting': function(newVal, oldVal){
//		  console.log('change waiting from ', oldVal, 'to', newVal)
	  }
  },
  ready: function(){	  
	  var svgSelector = '.processmodel-content svg .output';
	  this.lrData.waiting = true;
	  this.lrData.waitingLabel = 'Loading .. downloading the data';
	  
	  
		var intervalDuration = 3000;
		var logreplayURI = apiURI.model.logreplay + jsonData.workspaceId + '/' + jsonData.datasetId
							+ '/' + jsonData.sdt;
		var jobQueueURI = apiURI.job.queue;
		
		console.log(apiURI)
		
		var args = {
				limit: 5000
		};
		
		
	  var intval = window.setInterval(_.bind(function(){
		if($(svgSelector).length==1){
//			console.log('svg exist');
			window.clearInterval(intval);

			
			
			this.lrData.data.waiting = true;
			console.log("[STEP1-1] req : " + logreplayURI)
			this.$http.post(logreplayURI, JSON.stringify(args)).then(function (response) {
				console.log(response);
				var logreplayRes = response;
				var logreplayResStatus = logreplayRes.data.status;
				if(logreplayResStatus == 'FINISHED'){
					this.lrData.data.waiting = false;
					this.initializeLogReplay(logreplayRes.data.response);
				}
				else if(logreplayResStatus == 'FAILED'){
					  // if status is not FINISHED or RUNNING
					this.lrData.data.waiting = false;
				}
				else if(logreplayResStatus == 'RUNNING' || logreplayResStatus == 'QUEUE'){
					  // if status is running then check for each 3000 second
					  var logreplayQueueChecker = window.setInterval(_.bind(function(){
//						  console.log('this is check every '+intervalDuration+'ms');
						  console.log('[STEP1-2] req : ' + jobQueueURI + logreplayRes.data.request.id);
						  this.$http.get(jobQueueURI + logreplayRes.data.request.id).then(function(response){
							  console.log(response);
							  // success callback
//							  var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.id}));
//							  var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.id}));
							  var logreplayQueueRes = response;
							  var logreplayQueueResStatus = response.data.status;
							  if (logreplayQueueResStatus == 'RUNNING') {
							  } else if (logreplayQueueResStatus == 'FAILED') {
								  window.clearInterval(logreplayQueueChecker)
								  this.lrData.data.waiting = false;
								  alert('FAILED')
							  } else if (logreplayQueueResStatus == 'FINISHED') {
								  console.log('[STEP1-3] req : ' + logreplayURI);
								  this.$http.post(logreplayURI, JSON.stringify(args)).then(function(response) {
									  console.log(response);
									 var logreplayRes = response;
									 var logreplayResStatus = logreplayRes.data.status;
									 if (logreplayResStatus == 'FINISHED') {
										 window.clearInterval(logreplayQueueChecker)
										  this.lrData.data.waiting = false;
										  this.initializeLogReplay(response.data.response);
									 }
								  });
								  
							  }
						  }, function(response){
							  // error callback
							  this.lrData.data.waiting = false;
						  })			  
						  
					  }, this), intervalDuration);					  
				  }
			}, function(error){
				this.lrData.data.waiting = false;		  
			});						
		} else{
//			console.log('svg not exist');				
		}
	  }, this), 1000)
	  
	  this.intervalId = undefined;
  },
  methods: {
	  /*
	   * in log replay, passing all lrData, so we directly change the value of the UI in the LogReplay.js
	   * */
	  initializeLogReplay: function(response){
//		  console.log('initialize');
		  this.lrData.partitions = response.eventPartitions;
//		  console.log('processmodel', this.processModel);
//		  this.lrData.data.state = this.logReplay.state;
		  this.lrData.data.time = response.startTime;
		  this.lrData.data.eventsNumber = response.eventsNumber;
		  this.lrData.data.eventsNumberFormatted = numeral(response.eventsNumber).format('0,0');
		  
		  this.lrData.attributes[0].items = _.map(this.pmData.data.cases, function(val, key){
			  return val.id;
		  });
		  this.logReplay = new LogReplay(response.startTime, response.endTime, this.lrData, this.processModel, this.pmData);
	  },
	  play: function(){
		  this.logReplay.play();
	  },
	  pause: function(){
		  this.logReplay.pause();
	  },
	  isState: function(state){
		  return this.lrData.data.state == state;  
	  },
	  togglePlay: function(){
//		  console.log(this.lrData.data.state);
		  if(this.lrData.data.state == 'initial' || this.lrData.data.state == 'pause'){
			  this.logReplay.play();
		  }
		  else if(this.lrData.data.state == 'play'){
			  this.logReplay.pause();			  
		  }
//		  console.log(this.logReplay.state);
//		  this.lrData.data.state = this.logReplay.state;
	  },
	  renderTokensCssClass: function(){
		  this.logReplay.pause();
		  this.lrData.data.showSameColor = !this.lrData.data.showSameColor;
		  this.logReplay.renderTokensCssClass(!this.lrData.data.showSameColor)
		  this.logReplay.play();
	  },
	  renderTokensCaseId: function(){
		  this.logReplay.pause();
		  this.lrData.data.showCaseId = !this.lrData.data.showCaseId;
		  this.logReplay.hideTokensCaseId(!this.lrData.data.showCaseId);
		  this.logReplay.play();		  
	  },
	  showTokensFadeout: function(){
		  this.logReplay.pause();
		  this.lrData.data.showFadeOut = !this.lrData.data.showFadeOut;
		  this.logReplay.play();
	  },
	  showKPI: function(){
		  this.lrData.data.showKPI = !this.lrData.data.showKPI;
		  if(this.lrData.data.showKPI){
			  this.intervalid = window.setInterval(_.bind(function(){
				  this.lrData.data.tokensFinished = this.logReplay.calculateTokensByState('finish');
				  this.lrData.data.tokensRunning = this.logReplay.calculateTokensByState('running');
			  }, this), 1000);			  
		  }
		  else{
			  window.clearInterval(this.intervalId);
			  this.intervalId = undefined;
		  }
	  }
  }
});