new ProcessModelComponent({
  el: '#app',
  components: {
	  'loader': LoaderComponent  
  },
  data: {
	isProcessing: false,
	lrData: undefined,
    pmData: {
    	URI: apiURI.analysis.timegap + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt,
		options: {
	        is3D: false,
	        rankdir: "TB",
	        arctype: 'bundle',
	        isLogReplay: false,
	        type: 'timegap',
	        renderer: 'dagred3'
	      }
    }
  },
  ready: function(){
	  var self = this;
	  console.log('TimegapComponent is ready');
	  setTimeout(function(){
			$('.pointing.menu .item').tab();			
		}, 1000)

		var self = this;
		var status = "";
		var tgURI = apiURI.analysis.timegap+'/'+jsonData.workspaceId+'/'+jsonData.datasetId
		+'/'+jsonData.sdt+'/'+jsonData.edt;
		var jobQueueURI = apiURI.job.queue;
		var intervalDuration = 3000;

		self.isProcessing = true;
		this.$http.get(tgURI).then(function (response) {
			var tgRes = response;
			var tgResStatus = tgRes.data.status;
			
			if (tgResStatus == 'FINISHED') {
				self.isProcessing = false;
				this.drawTimegap(self, tgRes.data.response);
			} else if (tgResStatus == 'FAILED') {
				self.isProcessing = false;
				console.log("FAILED");
			} else if (tgResStatus == 'RUNNING' || tgResStatus == 'QUEUE') {
				var tgQueueChecker = window.setInterval(_.bind(function() {
					this.$http.get(jobQueueURI + tgRes.data.request.id)
						.then(function(response) {
							var tgQueueRes = response;
							var tgQueueResStatus = response.data.status;
							if (tgQueueResStatus == 'RUNNING') {
							} else if (tgQueueResStatus == 'FAILED') {
								window.clearInterval(tgQueueChecker);
								console.log("FAILED");
							} else if (tgQueueResStatus == 'FINISHED') {
								window.clearInterval(tgQueueChecker);
								this.$http.get(tgURI).then(function(response) {
									var tgRes = response;
									var tgResStatus = tgRes.data.status;
									if (tgResStatus == 'FINISHED') {
										this.drawTimegap(self, tgRes.data.response);
									} else {
										console.log('error');
									}
								});
							}
						});
				}, this), intervalDuration);
			}
		});
  },
  methods: {
	  drawTimegap: function(self, data) {
//		  	var responseData = response.data.response;
		  	var responseData = data;
			self.arrayData = _.values(responseData.transitions);
			
			summaryTable = $('#SummaryTable').DataTable({
				
				data: self.arrayData,
				columns: [
				          { "data" : 'source'},
				          { "data" : 'target'},
				          { "data" : 'meanSuccessive'},
				          { "data" : 'stdDevSuccessive'},
				          { "data" : 'noOfSuccessiveCases'}
				          ]
			});
			
			$.each(responseData.nodes, function(i, obj){
	            $('#fromSelect').append($('<option>').text(i).attr('value', i));
	            $('#toSelect').append($('<option>').text(i).attr('value', i));
			});
			
			var skipSlider = document.getElementById('skipstep');
			var sliderText = $('#sliderText');
			
			//<!-- http://refreshless.com/nouislider/ -->
			noUiSlider.create(skipSlider, {
				start: 0,
				range: {
					'min': responseData.minGap,
					'max': responseData.maxGap
					},
				orientation: "vertical"
//					    tooltips: true
//						pips: {
//						mode: 'positions',
//						values: [0, 100]
//					}
			});
			
			var value = "";
			skipSlider.noUiSlider.on('update', function( values, handle ) {

				value = values[handle];
				var absValue = Math.abs(value);
				var seconds = (absValue / 1000) % 60;
				var minutes = (absValue / (1000 * 60)) % 60;
				var hours = (absValue / (1000 * 60 * 60)) % 24;
				var days = absValue / (1000 * 60 * 60 * 24);
				var timeText;
				
				var day = "";
				if(days >= 1){
					day = Math.round(days)+"D ";
				}
				
				var hr = (String(Math.round(hours)).length < 2) ? String("0" + Math.round(hours)) :  String(Math.round(hours));
				var mnt = (String(Math.round(minutes)).length < 2) ? String("0" + Math.round(minutes)) :  String(Math.round(minutes));
				var sec = (String(Math.round(seconds)).length < 2) ? String("0" + Math.round(seconds)) :  String(Math.round(seconds));
				
				if(value < 0) {
					timeText ='- '+ day+ hr + ":" + mnt + ":" + sec; 
				} else {
					timeText =day+ hr + ":" + mnt + ":" + sec; 
				}
				sliderText.text(timeText);

			});
			
			skipSlider.noUiSlider.on('end', function() {
				
				var arcFilter = value;
				var model = responseData;
				
				var arcs = {};
				var arc_cluster_candidates = {};
				var nodes = {};
				var nodesAll = model.nodes;
				for (var arc in model.transitions) {
					var a = model.transitions[arc];
					if (a.meanRaw >= arcFilter) {
						arcs[arc] = a;
						if(!(a.source in nodes)) nodes[a.source] = nodesAll[a.source];
						if(!(a.target in nodes)) nodes[a.target] = nodesAll[a.target];

					} else {
						var s = a.meanRaw;
						if (!(s in arc_cluster_candidates)) arc_cluster_candidates[s] = {};
						arc_cluster_candidates[s][arc] = a;
					}
				}
				// Cluster arcs
				var arc_clusters = {};
				var arc_cluster_index = 0;
				var map_arc_clusters = {};
				for (var c in arc_cluster_candidates) {
					var i = 0;
					var s = "";
					for (var d in arc_cluster_candidates[c]) {
						i++;
						s = d;
					}
					if (i > 1) {
						arc_clusters[c] = arc_cluster_candidates[c];
						arc_cluster_index++;
					}
				}
				console.log(arcs);
				self.processModel.update(nodes, arcs, self.pmData.options.type);
				self.processModel.drawProcessModel();
			});
	  },
	  overallBtn: function(e) {
		  $('.overallDetail')
			.sidebar('setting', 'transition', 'overlay')
			.sidebar('toggle')
			.sidebar('attach events', '.bottomslide', 'show');
	  }
  }
});