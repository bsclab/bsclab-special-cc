/*
 * author @superpikar (dzulfikar.adiputra@gmail.com)
 * */

// vue initialization
new Vue({
	el: '#app',
	components: {
		'loader': LoaderComponent
	},
	data: {
		isProcessing: false,
		selectedRepository: undefined,
		repositories: [],
		datasets: [],
		options: {
			CASE: { label: 'Case ID', value: {} },
			ACTIVITY: {label: 'Activity', value: {} },
			TYPE: { label: 'Event Type', value: {} },
			TIMESTAMP: { label: 'Timestamp', value: {} },
			ORIGINATOR: { label: 'Originator', value: {} },
			RESOURCE: { label: 'Resource', value: {} },
			ATTRIBUTE: { label: 'Attributes', value: {} }
		},
		mappingInfo: {
			name: undefined,
			description: undefined
		},
		mapping: []
	},
	ready: function(){
		this.isProcessing = true;
		
		this.$http.get(apiURI.repository.mappings + jsonData.workspaceId).then(function (response) {
			console.log(response);
			response.data.response.forEach(function(val, key){
				val.id = val.id;
				val.workspaceId = jsonData.workspaceId,
				val.datasetId = jsonData.datasetId;
				val.active = false;
			});			
			this.$set('datasets', response.data.response);
			this.isProcessing = false;
			
			var repository = _.find(this.datasets, {id: jsonData.datasetId});
			
			this.selectRepository(repository);

			console.log(apiURI);
			
	    }, function (response) {	
	    	// error callback
	    	this.isProcessing = false;
	    	alert('cannot get datasets');
	    });
		
		console.log('jsondata', jsonData);
		
		if (jsonData.repositoryId == null) return;
	},
	methods: {
		selectRepository: function(repository){
			this.isProcessing = true;
			this.datasets.forEach(function(val, key){
				val.active = false;
			});
			
			this.selectedRepository = repository;	// set selected repository
			
			repository.active = true;	// set active item on list
			this.$set('mapping', []);
			
			this.$http.get(apiURI.repository.repositories + jsonData.workspaceId + '/' + jsonData.datasetId).then(function (response) {
				console.log(response);
				response.data.response.forEach(function(val, key){
					val.id = val.id;
					val.workspaceId = jsonData.workspaceId,
					val.datasetId = jsonData.datasetId;	
					val.active = false;
				});			
				this.$set('repositories', response.data.response);
				this.isProcessing = false;
				
		    }, function (response) {	
		    	// error callback
		    	alert('cannot get datas');
		    	this.isProcessing = false;
		    });			
			
			this.repositories.forEach(function(val, key){
				val.active = false;
			});
			
			this.$http.get(apiURI.repository.viewmap + jsonData.workspaceId+'/'+jsonData.datasetId).then(function (response) {
				var dimensions = response.data.response.dimensions; 
				console.log(response);
				for(var i in dimensions){
					this.mapping.push({
						label: i, 
						key: undefined, 
						purpose: undefined, 
						remark: undefined, 
						remark2: undefined, 
						data: dimensions[i], 
						showData: false
					});
				}	        
				this.isProcessing = false;
				
			}, function (response) {	
				alert('cannot get data');
				this.isProcessing = false;
			});
		},
		openMapping: function(attribute){
			console.log(attribute);
			$('.ui.modal').modal('show');
		},
		filterOptions: function(option){
			return _.isUndefined(option.value) || option.label=='Attributes';
		},
		showMapData: function(map){
			map.showData = !map.showData;
		},
		removeMapping: function(map){
			delete this.options[map.purpose].value[map.label];
			console.log('remove mapping', this.options[map.purpose].value);
			
			map.key = undefined;
			map.purpose = undefined;
			
		},
		selectMapping: function(map){
			map.key = this.options[map.purpose].label;
			
			if(this.options[map.purpose].label=='Timestamp'){
				map.remark = "yyyy-MM-dd HH:mm:ss";
			}
			else{
				map.remark = undefined;
				map.remark2 = undefined;
			}
			this.options[map.purpose].value[map.label] = map.purpose;
			console.log('select mapping', this.options[map.purpose].value[map.label]);
		},
		submitMapping: function(event){
			event.preventDefault();
			console.log('form submit', this.mappingInfo, this.mapping);
			var args = {
				name: _.isUndefined(this.mappingInfo.name)? 'repository': this.mappingInfo.name,
				description: _.isUndefined(this.mappingInfo.description)? 'description': this.mappingInfo.description,
				datasetUri: '/workspaces/'+jsonData.workspaceId+'/'+jsonData.datasetId,
				mapping: {}
			}
			
			// if type is timestamp then set additional info here : i.e, EVENT,yyyy-MM-dd'T'HH:mm:ss.SSSXXX,complete
			for(var index in this.options){
				this.options[index].valueTemp = JSON.stringify(this.options[index].value);
				
				var theValue = this.options[index].value;
				if(index == 'CASE'){
					for(var j in theValue){
						theValue[j] = 'CASE';
					}					
				}
				else if(index == 'TIMESTAMP'){
					for(var j in theValue){
						var map = _.find(this.mapping, {purpose: theValue[j]});
						console.log('map', theValue[j], map);
						theValue[j] = 'EVENT,'+map.remark+','+map.remark2;
					}
					args.mapping[index] = theValue;
				}
				else{
					for(var j in theValue){
						theValue[j] = 'EVENT';
					}
				}
				args.mapping[index] = this.options[index].value;
			}			
			console.log('[POST] data to map API', args);
			
			// start mapping process 
			this.isProcessing = true;
			this.$http.post(apiURI.repository.map + jsonData.workspaceId+'/'+jsonData.datasetId + "/" + args.name, JSON.stringify(args)).then(function (response) {
				console.log('reponse', response.data);
				
				var interval;
				
				var id = response.data.id;
				var status = response.data.status;
				
				var httpGet = this.$http;
				
				if(status === 'QUEUE') {
					interval = setInterval(function(){
						httpGet.get(apiURI.job.report + id).then(function(response){
							if(response.data.status === 'FINISHED') {
								clearInterval(interval);
								this.isProcessing = false;				
								this.resetForm();
								window.location.reload();
							}
						}, function(response){
							alert('cannot get data');
							this.isProcessing = false;
						});
					}, 1000);
				}
			}, function (response) {	
				alert('cannot get data');
				this.isProcessing = false;
			});
		},
		resetForm: function(){
			// reset value 
			this.mappingInfo = {
				name: undefined,
				description: undefined
			};
			
			for(var index in this.options){
				this.options[index].value = {};
			}
			
			for(var index in this.mapping){
				this.mapping[index].purpose = undefined;
				this.mapping[index].remark = undefined;
				this.mapping[index].remark2 = undefined;
			}
		}
	}
});
