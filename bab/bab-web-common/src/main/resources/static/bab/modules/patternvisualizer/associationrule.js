new Vue({
	el: '#app',
	components: {
		'loader': LoaderComponent
	},
	data: {
		isProcessing: false,
		arrayData: [],
		summaryTable: null,
		arURI:"",
		intervalDuration:3000,
		jobQueueURI:""
	},
	ready: function(){
		$("#activity").hide();
		$('.tooltip .ui.button').popup();
		// loader position middle
		$('.asso-table').css("min-height", $(window).height()/2);
		
		setTimeout(function(){
			$('.pointing.menu .item').tab();			
		}, 1000)
		
		
		var self = this;
		self.isProcessing = true;
		this.jobQueueURI = apiURI.job.queue;
		this.arURI = apiURI.analysis.associationrule + jsonData.workspaceId
			+ '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt;
		this.getAr();
		
		
//		self.$http.get(arURI).then(function(response) {
//			var arRes = response;
//			var arResStatus = arRes.data.status;
//			if (arResStatus == 'FINISHED') {
//				self.isProcessing = false;
//				self.arrayData = _.values(arRes);
//				summaryTable = $('#SummaryTable').DataTable({
//					data : self.arrayData,
//					columns : [
//						{"data" : 'left[<br/>]'},{"data" : 'right[<br/>]'},
//						{"data" : function(data) {
//							return BabHelper.formatNumber(data.confidence,"0.00")
//						}},
//						{"data" : function(data) {
//							return BabHelper.formatNumber(data.support,"0.00")
//						}} 
//					]
//				});
//			} else if (arResStatus == 'FAILED') {
//				this.isProcessing = false;
//				console.log('FAILED');
//			} else if (arResStatus == 'RUNNING' || arResStatus == 'QUEUE') {
//				var arQueueChecker = window.setInterval(_.bind(function() {
//					this.$http.get(jobQueueURI + arRes.data.request.id).then(function(response) {
//						var arQueueRes = response;
//						var arQueueResStatus = arQueueRes.data.status;
//						
//						if (arQueueResStatus == 'RUNNING') {
//						} else if (arQueueResStatus == 'FAILED') {
//						} else if (arQueueResStatus == 'FINISHED') {
//							this.$http.get(arURI).then(function(response) {
//								var arRes = response;
//								var arResStatus = arRes.data.status;
//								if (arResStatus == 'FINISHED') {
//									self.isProcessing = false;
//									self.arrayData = _.values(arRes.data.response.associationRule);
//									summaryTable = $('#SummaryTable').DataTable({
//										data : self.arrayData,
//										columns : [
//											{"data" : 'left[<br/>]'},
//											{"data" : 'right[<br/>]'},
//											{"data" : function(data) {
//												return BabHelper.formatNumber(data.confidence,"0.00")
//											}},
//											{"data" : function(data) {
//												return BabHelper.formatNumber(data.support,"0.00")
//											}} 
//										]
//									});
//								}
//							});
//						}
//					});
//				}, this), intervalDuration);
//			}
//		});
	},
	methods: {
		getAr: function(){
//			console.log('supportText', $('#supportText').val());
//			console.log('confidenceText', $('#confidenceText').val());
			
			var supportText =  parseInt($('#supportText').val());
			var confidenceText = parseInt($('#confidenceText').val());
			var postData = JSON.stringify({
				"threshold" : {
				    "support" : {
				      "min" : supportText,
				      "max" : 1.0
				    },
				    "confidence" : {
				      "min" : confidenceText,
				      "max" : 1.0
				    } 
				}
			});
			this.isProcessing = true;
			this.$http.post(this.arURI, postData).then(function(response) {
				var arRes = response;
				var arResStatus = arRes.data.status;
				if (arResStatus == 'FINISHED') {
					this.isProcessing = false;
					arData = arRes.data.response.associationRules;
					this.arrayData = _.values(arData);
					this.setSummaryTable(this.arrayData);					
				} else if (arResStatus == 'FAILED') {
					this.isProcessing = false;
					console.log('FAILED');
				} else if (arResStatus == 'RUNNING' || arResStatus == 'QUEUE') {
					var arQueueChecker = window.setInterval(_.bind(function() {
						this.$http.get(this.jobQueueURI + arRes.data.request.id).then(function(response) {
							var arQueueRes = response;
							var arQueueResStatus = arQueueRes.data.status;
							
							if (arQueueResStatus == 'RUNNING') {
								
							} else if (arQueueResStatus == 'FAILED') {
								window.clearInterval(arQueueChecker);
								this.isProcessing = false;
								console.log('FAILED')
							} else if (arQueueResStatus == 'FINISHED') {
								window.clearInterval(arQueueChecker);
								this.$http.post(this.arURI, postData).then(function(response) {
									var arRes = response;
									var arResStatus = arRes.data.status;
									if (arResStatus == 'FINISHED') {
										this.isProcessing = false;
										arData = arRes.data.response.associationRules;
										this.arrayData = _.values(arData);
										this.setSummaryTable(this.arrayData);
									}
								})
							}
						})
					}, this), this.intervalDuration);
				}
			});
			
//			var self = this;
//			var interval = window.setInterval(function() {
//				// call API
//				self.$http.post(apiURI.analysis.associationrule+jsonData.workspaceId+'/'+jsonData.datasetId+ '/' + jsonData.sdt + '/' + jsonData.edt, { 
//					"threshold.confidence.min": confidenceText,
//					"threshold.support.min": supportText 
//					}).then(function(response) {
//					console.log("start Interval",response);
//					status = response.data.status;
//					if (status == "FINISHED") {
//						console.log("finished");
//						
//						if(!_.isNull(response.data.response)) {
//							var responseData = response.data.response.associationRules;
//							this.arrayData = _.values(responseData);
//							console.log(this.arrayData);
//							
//							console.log('arrayData', this.arrayData);
//							summaryTable.clear();
//							summaryTable.rows.add(this.arrayData);
//							summaryTable.columns.adjust().draw();
//						}
//						
//						console.log("end Interval", interval);
//						window.clearInterval(interval);
//					} else if(status == "RUNNING" || status == "QUEUED"){
//						console.log("wait");
//					} else {
//						console.log("error");
//						window.clearInterval(interval);
//					}
//				});	
//			}, 3000);
		},
		setSummaryTable : function(arrayData) {
			if (this.summaryTable != null )
				this.summaryTable.clear();
			this.summaryTable = $('#SummaryTable').DataTable({
				data : arrayData,
				columns : [
					{"data" : 'left[<br/>]'},{"data" : 'right[<br/>]'},
					{"data" : function(data) {
						return BabHelper.formatNumber(data.confidence,"0.00")
					}},
					{"data" : function(data) {
						return BabHelper.formatNumber(data.support,"0.00")
					}} 
				]
			});
			this.summaryTable.clear();
			this.summaryTable.rows.add(arrayData);
			this.summaryTable.columns.adjust().draw();
		},
		relationBtn: function(e) {
			console.log(e);
			var supportText =  $('#supportText').val();
			var confidenceText = $('#confidenceText').val();
			var relationBtn = $('#relation');
			var self = this;
			var interval = window.setInterval(function() {
				// call API
				self.$http.post(apiURI.analysis.associationrule+jsonData.workspaceId+'/'+jsonData.datasetId+ '/' + jsonData.sdt + '/' + jsonData.edt, { 
					"threshold.confidence.min": confidenceText, 
					"threshold.support.min": supportText, 
					noOfRelation : relationBtn.val() 
					}).then(function(response) {
					console.log("start Interval",response);
					status = response.data.status;
					if (status == "FINISHED") {
						console.log("finished");
						
						if(!_.isNull(response.data.response)) {
							var responseData = response.data.response.associationRules;
							this.arrayData = _.values(responseData);
							console.log(this.arrayData);
		
							console.log('arrayData', this.arrayData);
							this.summaryTable.clear();
							this.summaryTable.rows.add(this.arrayData);
							this.summaryTable.columns.adjust().draw();
		
							// button change
							if(relationBtn.val() == 0) {
								relationBtn.val(1);
								$("#relation").hide();
								$("#activity").show();
							} else {
								relationBtn.val(0);
								$("#activity").hide();
								$("#relation").show();
							}
						}
						
						console.log("end Interval", interval);
						window.clearInterval(interval);
					} else if(status == "RUNNING" || status == "QUEUED"){
						console.log("wait");
					} else {
						console.log("error");
						window.clearInterval(interval);
					}
				});	
			}, 3000);
			
//			this.isProcessing = true;
//			this.$http.post(apiURI.analysis.associationrule+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.repositoryId, { 
//				"threshold.confidence.min": confidenceText, 
//				"threshold.support.min": supportText, 
//				noOfRelation : relationBtn.val() 
//				}).then(function(response){
//				
//				if(!_.isNull(response.data.response)) {
//					var responseData = response.data.response.associationRules;
//					this.arrayData = _.values(responseData);
//					console.log(this.arrayData);
//
//					console.log('arrayData', this.arrayData);
//					summaryTable.clear();
//					summaryTable.rows.add(this.arrayData);
//					summaryTable.columns.adjust().draw();
//
//					// button change
//					if(relationBtn.val() == 0) {
//						relationBtn.val(1);
//						$("#relation").hide();
//						$("#activity").show();
//					} else {
//						relationBtn.val(0);
//						$("#activity").hide();
//						$("#relation").show();
//					}
//				}
//				
//			}, function(){
//				this.isProcessing = false;
//			});
			// button change Test
//			if(relationBtn.val() == 0) {
//				relationBtn.val(1);
//				$("#relation").hide();
//				$("#activity").show();
//			} else {
//				relationBtn.val(0);
//				$("#activity").hide();
//				$("#relation").show();
//			}

		}
	}
});