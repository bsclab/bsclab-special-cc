/*
 * author @superpikar (dzulfikar.adiputra@gmail.com)
 * */

// vue initialization
new Vue({
	el: '#app',
	ready: function(){
		// set height of list item in column 1
		var baseHeight = $('body').height() - $('.top.menu').height();
		var height1 =  baseHeight - $('.ui.grid .column:nth-child(1) > div:nth-child(1)').outerHeight();
		$('.ui.grid .column:nth-child(1) > .segment.scroll-y').outerHeight(height1);
		
		// set height of list item in column 2 
		var height2 = baseHeight 
						- $('.ui.grid .column:nth-child(2) > div:nth-child(1)').outerHeight() 
						- $('.ui.grid .column:nth-child(2) > div:nth-child(2)').outerHeight() 
						- 14 
						- $('.ui.grid .column:nth-child(2) > div:nth-child(3)').outerHeight();
		$('.ui.grid .column:nth-child(2) > .segment.scroll-y').outerHeight(height2);
		
		// console.log(height1);
		// console.log(height2);
	}
});
