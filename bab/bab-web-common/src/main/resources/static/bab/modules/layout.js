jsonData = /*[[${jsonData}]]*/ {};
apiURI = /*[[${apiURI}]]*/ {} ;

$(document).ready(function () {
	$('.ui.stackable.menu#top-bar .toggle').on('click', function () {
		$('.ui.left.sidebar').toggle('fast', function(){
			if($('.ui.left.sidebar').css('display') === 'none') {
				var currentWidth = $('#main-content').width();
				// console.log(currentWidth);
				$('#main-content').css('transform', 'translate3d(0,0,0)');
				$('#main-content').css('width', currentWidth + 260);
			} else {
				var currentWidth = $('#main-content').width();
				$('#main-content').css('transform', 'translate3d(260px,0,0)');
				$('#main-content').css('width', currentWidth - 260);
			}
		});
		$('a.logo.item').toggle();
	});

	$('.ui.accordion').accordion();
	$('.ui.checkbox').checkbox();
	$('select.dropdown').dropdown();
	$('.item-tooltip .header, .item-tooltip .menu .item').popup();

	// setting for pusher area	
	$('#main-content').css('width', $('#top-bar').width() - 260);
    $(window).resize(() => {
      $('#main-content').css('width', $('#top-bar').width() - 260);
    })

	
	// setting right side bar
	$('.ui.right.sidebar').sidebar('setting', 'transition', 'overlay')
		.sidebar('attach events', '.item.showjob');

	$("#sdt").calendar({
		type: 'date',
		formatter: {
			date: calendarCustomFormat
		},
		onChange: function(date, text, mode) {
			// console.log(text);
		}
	});
	$("#edt").calendar({
		type: 'date',
		formatter: {
			date: calendarCustomFormat
		},
		onChange: function(date, text, mode) {
			// console.log(text);
		}
	});

	function calendarCustomFormat(date, settings) {
		if (!date) return '';
		var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
		var month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
		var year = date.getFullYear();
		return year + "-" + month + "-" + day;
	}

	var UPLOAD_MAX_SIZE = 30; // MB
	var ALLOWED_UPLOAD_FILE_TYPE = '.csv .log';

	$("#logUploader").change(function () {
		var input = $(this);

		if (navigator.appVersion.indexOf("MSIE") != -1) { // IE
			var label = input.val();

			input.trigger('fileselect', [1, label, 0]);
		} else {
			var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			var numFiles = input.get(0).files ? input.get(0).files.length : 1;
			var size = input.get(0).files[0].size;

			input.trigger('fileselect', [numFiles, label, size]);
		}
	})
	$("#logUploader").on('fileselect', function (event, numFiles, label, size) {
		$('#logUploader').attr('name', 'logUploader'); // allow upload.

		var postfix = label.substr(label.lastIndexOf('.'));
		if (ALLOWED_UPLOAD_FILE_TYPE.indexOf(postfix.toLowerCase()) > -1) {
			if (size > 1024 * 1024 * ALLOWED_UPLOAD_FILE_TYPE) {
				alert('max size：<strong>' + UPLOAD_MAX_SIZE + '</strong> MB.');

				$('#logUploader').removeAttr('name'); // cancel upload file.
			} else {
				$('#_logUploader').val(label);
			}
		} else {
			alert('file type：<br/> <strong>' + ALLOWED_UPLOAD_FILE_TYPE + '</strong>');

			$('#logUploader').removeAttr('name'); // cancel upload file.
		}
	})
});

var vueTopbar = new Vue({
	el : '#top-bar',
	components: {
		'fileupload': FileUploadComponent
	},
	methods : {
		clickFileUploadBtn : function() {
			
			var formData = new FormData();
			formData.append("uploadFile", $('#logUploader')[0].files[0]);
			
			$.ajax({
				url : apiURI.repository.upload,
				type : 'POST',
				data : formData,
				mimeType : "multipart/form-data",
				contentType : false,
				cache : false,
				processData : false,
				success : function(data, textStatus, jqXHR) {
					console.log("success")
				},
				error : function(jqXHR, textStatus, errorThrown) {
					console.log("error")
				}
			});
		},
	}
})

var vueRightSidebar = new Vue({
	el : '#right-sidebar'
	, data : {
		jobs : []
	}, ready : function() {
		// setInterval(function() {
		// 	Vue.http.get(apiURI.job.list).then(function (response) {
		// 		var jobs = response.data.response
		// 		for (var i=0; i<jobs.length; i++) {
		// 			jobs[i].start
		// 				= moment(jobs[i].start).format("YYYY/MM/DD HH:mm:ss");
		// 			jobs[i].end
		// 				= moment(jobs[i].end).format("YYYY/MM/DD HH:mm:ss");
		// 			jobs[i].jobIdNick=jobs[i].jobId.substring(0,26);
		// 		}
		// 		console.log(jobs);
		// 		if (_.isEqual(vueRightSidebar.jobs, jobs)) {
		// 		} else {
		// 			vueRightSidebar.jobs = jobs;
		// 		}
				
		// 	}, function (response) {
		// 	});
		// }, 5000)
	}
	, methods : {
//		test1 : function() {
//			Vue.http.get("http://localhost:8080/bab/api/v1_0/job").then(function (response) {
//				var jobs = response.data.response
//				for (var i=0; i<jobs.length; i++) {
//					jobs[i].start
//						= moment(jobs[i].start).format("YYYY/MM/DD HH:mm:ss");
//					jobs[i].end
//						= moment(jobs[i].end).format("YYYY/MM/DD HH:mm:ss");
//					jobs[i].jobIdNick=jobs[i].jobId.substring(0,26);
//				}
//				vueRightSidebar.jobs = jobs;
//			}, function (response) {
//			});
//		},
//		test2 : function() {
//			$('.ui.accordion').accordion('refresh');
//		}
	}, watch: {
		jobs: function(newVal, oldVal) {
			$('.ui.accordion').accordion('refresh');

//			$(".item.showjob").attr("data-content", (newVal.length - oldVal.length) + " jobs was created");
			$(".item.showjob").attr("data-content", "new job is created");

			$(".item.showjob").popup('show', true);
		}
	}
});

//function clickFileUploadBtn() {
//	var formData = new FormData();
//	formData.append("uploadFile", logUploader.files[0]);
//	
//	var uploadFormDom = $("#upload-form");
//	console.log(apiURI.repository.upload);
//	$.ajax({
//		url: apiURI.repository.upload,
//		type: 'POST',
//		data: formData,
//		mimeType: "multipart/form-data",
//		contentType: false,
//		cache: false,
//		processData: false,
//		success: function (data, textStatus, jqXHR) {
//			console.log("success")
//		},
//		error: function (jqXHR, textStatus, errorThrown) {
//			console.log("error")
//		}
//	});
//}
//
