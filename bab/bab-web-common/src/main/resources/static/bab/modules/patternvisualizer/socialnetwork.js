new Vue({
  el: '#app',
  data: {
    apiURI: {
    	socialnetwork: apiURI.analysis.socialnetwork+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.repositoryId
    },
    conf: {
    	dimension: "event.originator",
    	states: [],
    	method: {
    		algorithm: "workingTogether",
    		casuality: true,
    		multipleTransfer: true,
    		workingTogether: "something",
    		directSubcontract: {
    			beta:0.5,
    			depth:0
    		},
    		distance: "euclidean"
    	}
    }
  },
  ready: function(){
				  $('.menu .item').tab();
				$('.ui.dropdown').dropdown();
				$(function() {
					$("#machin-dropdown").dropdown({
						allowLabels : true
					});
				});
				$('.custom.button').popup({
					popup : $('.custom.popup'),
					on : 'click'
				});
				$('.algo1').on("click", function() {
					$('.ui.modal.modal1').modal('show');
				});
				$('.algo2').on("click", function() {
					$('.ui.modal.modal2').modal('show');
				});
				$('.algo3').on("click", function() {
					$('.ui.modal.modal3').modal('show');
				});
				$('.algo4').on("click", function() {
					$('.ui.modal.modal4').modal('show');
				});
				$('.algo5').on("click", function() {
					$('.ui.modal.modal5').modal('show');
				});

  }
});