/*
 * author @superpikar (dzulfikar.adiputra@gmail.com)
 * */

// vue initialization
new Vue({
	el: '#app',
	ready: function(){
		this.isProcessing = true;
		
		this.$http.get('/bab/api/v1_0/repository/repositories/'+jsonData.workspaceId+'/'+jsonData.datasetId).then(function (response) {
			response.data.response.forEach(function(val, key){
				val.id = val.id.substring(0, val.id.length - 5);
				val.workspaceId = jsonData.workspaceId,
				val.datasetId = jsonData.datasetId;					
			});			
			this.$set('repositories', response.data.response);
	        
	    }, function (response) {	
	    	// error callback
	    	alert('cannot get data');
	    });
		
		$('.show-popup').popup();
	},
	data: {
		isProcessing: false,
		repositories: [],
		mapping: {
			name: { label: 'Name', value: undefined },
			description: { label: 'Description', value: undefined },
			caseId: { label: 'Case ID', value: undefined },
			activity: { label: 'Activity', value: undefined },
			startTimestamp: { label: 'Start', value: undefined },
			startTimestampFormat: { label: 'Start Format', value: undefined },
			completeTimestamp: { label: 'Complete', value: undefined },
			completeTimestampFormat: { label: 'Complete Format', value: undefined },
			eventType: { label: 'Event Type', value: undefined },
			originator: { label: 'Originator', value: undefined },
			resource: { label: 'Resource', value: undefined },
			attributes: { label: 'Attributes', value: [] }
		},
		repositoryAttributes: ['Case Id', 'Activity', 'Resource', 'Complete Timestamp', 'Start Timestamp', 'Variant', 'BLOCK_BAY', 'FULL_EMPTY', 'POD', 'VESSEL']
	},
	methods: {
		openMapping: function(attribute){
			// console.log(attribute);
			$('.ui.modal').modal('show');
		}
	}
});
