/*
 * author @superpikar (dzulfikar.adiputra@gmail.com)
 */

var DELAY = 200;
var INTERVALDURATION = 3000;
var LABEL = {
	DOWNLOAD_DATA: 'Loading .. download the data',
	RENDER_CHART: 'Loading .. render the chart'
};

ColumnData = function(column, label){
	return {
		data: column,
		title: label
	}
};

StatisticData = function(label, format){
	return {
		label: label,
		value: 0, 
		format: format
	}
};

TabData = function(id, label, type, format, statisticsValue, xAxis, yAxis){
	return {
		id: id,
		label: label,
		waiting: false,
		waitingLabel: LABEL.RENDER_CHART,
		type: type,
		axis: {
			x: xAxis,
			y: yAxis
		},
		format: format,
		rendered: false,
		data: undefined,
		status: 'WAITING',
		statisticsValue: statisticsValue,
		statistics:[
			new StatisticData('Min', format),
			new StatisticData('Mean', format),
			new StatisticData('Max', format),
			new StatisticData('Std Deviation', format)			
		]
	}
};

/*
 * credit : calculate std deviation http://derickbailey.com/2014/09/21/calculating-standard-deviation-with-array-map-and-array-reduce-in-javascript/
 * */
function calculateStatisticsData(values){
	if(values.length > 1){
		var sum = _.sum(values);
		var mean = sum/values.length;
		var max = _.max(values);
		var min = _.min(values);
		var diffs = _.map(values, function(value){
			var diff = value - mean;
			return diff;
		});
		var squareDiffs = _.map(values, function(value){
			var diff = value - mean;
			var sqr = diff * diff;
			return sqr;
		});
		var avgSquareDiff = _.sum(squareDiffs)/squareDiffs.length;
		return {
			min: min,
			mean: mean,
			max: max,
			stdDev: Math.round(Math.sqrt(avgSquareDiff))		
		}		
	}
	else{
		return {
			min: values[0],
			mean: values[0],
			max: values[0],
			stdDev: values[0]
		}
	}
}

function setStatisticsData(tab, objData){
	if(tab.statisticsValue == 'value'){
		values = _.map(objData, function(val, key){ return parseInt(val); });	
	}
	else if(tab.statisticsValue == 'key'){
		values = _.map(objData, function(val, key){ return parseInt(key); });		
	}
	var stat = calculateStatisticsData(values);
	tab.statistics[0].value = stat.min;
	tab.statistics[1].value = stat.mean;
	tab.statistics[2].value = stat.max;
	tab.statistics[3].value = stat.stdDev;	
}

function preProcessTableData(table, jsonData){
	if(!_.isUndefined(table)){
		table.data = _.map(jsonData, function(val, key){
			return val;
		});
	}
}

function preProcessData(tabs, jsonData){
	for(var i in tabs){
		var obj = jsonData[tabs[i].id];
		if(tabs[i].type == 'barchart'){
			tabs[i].data = [];
			
			if(tabs[i].format == 'formatNumber'){
				for(var j in obj){
					tabs[i].data.push({ key: parseInt(i), value: parseInt(obj[j]) });
				}
				setStatisticsData(tabs[i], obj);				
			}
			else if(tabs[i].format == 'formatDuration'){
				var data = calculateAggregateDurationData(obj);
				for(var j in data.chartData){
					//console.log(j, data.chartData[j]);
					tabs[i].data.push({ key: j, value: parseInt(data.chartData[j]) });				
				}
				setStatisticsData(tabs[i], obj);
				
				// update x axis label if the x axis label is duration
				tabs[i].axis.x = tabs[i].axis.x + '('+data.unit+')';
			}
		}
		
		else if(tabs[i].type == 'linechart'){
			tabs[i].data = [];
			for(var j in obj){
				var thedate = new Date(parseInt(j)); 
				if(_.isDate(thedate)){
					tabs[i].data.push([ new Date(parseInt(j)), obj[j] ]);					
				}
			}
		}
	}
}

/* from old bab */
function calculateAggregateDurationData(series){
	var maxDuration = _.max(_.map(_.keys(series), function(val){ return parseInt(val); }));
	var unit = BabHelper.getDurationUnit(maxDuration);

	var arrMap = _.map(series, function(val, key){
		return {
			key: parseInt(key),
			val: val,
			duration: BabHelper.getDurationUnitValue(parseInt(key), unit)
		}
	});
	var objGroup = _.groupBy(arrMap, function(val, key){ return val.duration });
	var objGroupSum = _.mapValues(objGroup, function(val, key){
		arrValue = _.map(val, function(v, k){ return v.val });
		return _.sum(arrValue);
	});

	// make dummy object to show zero value on non existing groupsum key
	var dummyObj = {};
	var maxGroupKey = _.max(_.without(_.map(_.keys(objGroupSum), function(val){ return parseInt(val); }), "<1"));
	for(var i=1; i<= maxGroupKey; i++){
		dummyObj[i] = 0;
	}

	return {
		unit: unit,
		chartData: _.extend(dummyObj, objGroupSum),
		statisticsData: _.map(series, function(val, key){ return parseInt(key); })
	};
}

function renderChart(el, tab){
	tab.waiting = true;
	tab.waitingLabel = LABEL.RENDER_CHART;
	//console.log('render', el, tab.data);
	if(tab.type == 'barchart'){	
		window.setTimeout(function(){
			c3.generate({
				bindto: el,
				data: {
					json: tab.data, 
					keys: {
						x: 'key', // it's possible to specify 'x' when category axis
						value: ['value'],
					},
					names: {
						value: tab.axis.y,
					},
					type: 'bar'
						
				},
				bar: {
					width: {
						ratio: 0.5 // this makes bar width 50% of length between ticks
					}
				},
				axis: {
					x: {
						label: tab.axis.x,
						type: 'category'
					},
					y: {
						label: tab.axis.y
					}
				}
			});
			tab.waiting = false;
		}, DELAY);
	}
	else if(tab.type == 'linechart'){
		window.setTimeout(function(){
//			console.log('render linechart', el.substr(1), tab.data);
			var dygraph = new Dygraph(document.getElementById(el.substr(1)),
				tab.data,
				{
					legend: 'always',
					//showRoller: true,
					//showRangeSelector: true,
					//rollPeriod: 14,
					ylabel: tab.axis.y,
					labels: [ "x", tab.axis.x ]
				}
<<<<<<< HEAD
			)			
			tab.waiting = false;
		}, DELAY);
=======
			)
			// update the size
			//dygraph.resize(1,1);
			//dygraph.resize($('.ten.wide.column').width(),$('.ten.wide.column').height());
			
			tab.isProcessing = false;			
			
		}, 500);
>>>>>>> 9483c4ccf34b1a4d52e3462345035ea1da301213
	}
	tab.status = 'COMPLETED' ;
	tab.rendered = true;
}

// vue initialization
new Vue({
	el: '#app',
	components: {
		'statistic': StatisticComponent,
		'loader': LoaderComponent
	},
	data: {
<<<<<<< HEAD
		summaryView: {
			waiting: false,
			waitingLabel: LABEL.DOWNLOAD_DATA,
			data: {}
		},
		summary:{
			waiting: false,
			waitingLabel: LABEL.DOWNLOAD_DATA,
			data: {}
		},
		tabs: tabsData
	},
	ready: function(){
		var summaryURI = apiURI.repository.summary + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.repositoryId;
		var summaryViewURI = apiURI.repository.view + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.repositoryId ;
		var jobQueueURI = apiURI.job.queue;
		
=======
		summary: {},
		summaryView: {},
		isProcessing: false,
		tabs: [
			{
				id: 'caseTab',
				label: 'Overview',
				rendered: false,
				statistics: [
					new StatisticData('Cases', 'formatNumber'),
					new StatisticData('Events', 'formatNumber'),
					new StatisticData('Event Class', 'formatNumber'),
					new StatisticData('Event Type', 'formatNumber'),
					new StatisticData('Human Performer', 'formatNumber'),
					new StatisticData('Resource', 'formatNumber'),
					new StatisticData('Start', 'formatDateTime'),
					new StatisticData('Complete', 'formatDateTime')
				],
//				table: [ 'Case ID', 'Duration', 'No of Events', 'Start', 'Finished' ],
				table:{
					data: [],
					columns:[
						new ColumnData('caseId', 'Case ID'),
						new ColumnData('durationStr', 'Duration'),
						new ColumnData('noOfEvents', 'No of Events'),
						new ColumnData('startedStr', 'Started'),
						new ColumnData('finishedStr', 'Finished')
					],
				},
				tabs: [
					new TabData('eventPerCase', 'Event Per Case', 'barchart', 'formatNumber', 'key', 'event', 'number of case'),
					new TabData('caseUtilizations', 'Active Case Over Time', 'linechart', 'formatNumber', 'value', 'case over time', 'number of case'),
					new TabData('caseDurations', 'Case Working Duration', 'barchart', 'formatDuration', 'key', 'duration', 'working case'),
					new TabData('meanActivityDuration', 'Mean Activity Duration', 'barchart', 'formatDuration', 'key', 'duration', 'mean activity'),
					new TabData('meanWaitingTimes', 'Mean Waiting Time', 'linechart', 'formatNumber', 'key', 'duration', 'mean waiting')
				]
			},
			{
				id: 'timelineTab',
				label: '타임라인',
				rendered: false,
				statistics: [
				    new StatisticData('Cases', 'formatNumber'),
				    new StatisticData('Events', 'formatNumber')
				],
//				table: [ 'Case ID', 'Duration', 'No of Events', 'Start', 'Finished' ],
				table: {
					data: [],
					columns:[
						new ColumnData('caseId', 'Case ID'),
						new ColumnData('durationStr', 'Duration'),
						new ColumnData('noOfEvents', 'No of Events'),
						new ColumnData('startedStr', 'Started'),
						new ColumnData('finishedStr', 'Finished')
					],
				},
				tabs: [
					new TabData('caseStartOverTime', 'Case Start Over Time', 'linechart', 'formatNumber', 'value', 'time', 'number of cases'),
					new TabData('caseCompleteOverTime', 'Case Complete Over Time', 'linechart', 'formatNumber', 'value', 'time', 'number of cases'),
					new TabData('eventsOverTime', 'Event Over Time', 'linechart', 'formatNumber', 'value', 'time', 'number of events') 
				]
			},
			{
				id: 'activityTab',
				label: '공정',
				rendered: false,
				statistics: [
				    new StatisticData('Event Class', 'formatNumber'),
				    new StatisticData('Min Frequency', 'formatNumber'),
				    new StatisticData('Mean Frequency', 'formatNumber'),
				    new StatisticData('Max Frequency', 'formatNumber'),
				    new StatisticData('Std Deviation', 'formatNumber')
				],
//				table: [ 'Event Class', 'Duration Range', 'Frequency', 'Min Duration', 'Sum Duration' ],
				table: {
					data: [],
					columns:[
						new ColumnData('label', 'Event Class'),
						new ColumnData('durationRangeStr', 'Duration Range'),
						new ColumnData('frequency', 'Frequency'),
						new ColumnData('minDurationStr', 'Min Duration'),
						new ColumnData('sumDurationStr', 'Sum Duration')
					],
				},
				tabs: [
					new TabData('frequency', 'Frequency', 'barchart', 'formatNumber', 'value', 'event class', 'frequency'),
					new TabData('meanDuration', 'Mean Duration', 'barchart', 'formatDuration', 'key', 'event class', 'duration'),
					new TabData('medianDuration', 'Median Duration', 'barchart', 'formatDuration', 'key', 'event class', 'duration'), 
					new TabData('aggregateDuration', 'Aggregate Duration', 'barchart', 'formatDuration', 'key', 'event class', 'duration') 
				]
			},
			{
				id: 'worker',
				label: '작업자',
				rendered: false,
				statistics: [
					new StatisticData('Human Performer', 'formatNumber'),
					new StatisticData('Min Frequency', 'formatNumber'),
					new StatisticData('Mean Frequency', 'formatNumber'),
					new StatisticData('Max Frequency', 'formatNumber'),
					new StatisticData('Std Deviation', 'formatNumber')
				],
//				table: [ 'Human Performer', 'Frequency', 'Relative Frequency', 'Max Duration', 'Mean Duration', 'Duration Range' ],
				table: {
					data: [],
					columns:[
						new ColumnData('label', 'Human Performer'),
						new ColumnData('durationRangeStr', 'Duration Range'),
						new ColumnData('frequency', 'Frequency'),
						new ColumnData('minDurationStr', 'Min Duration'),
						new ColumnData('sumDurationStr', 'Sum Duration')
					],
				},
				tabs: [
					new TabData('frequency', 'Frequency', 'barchart', 'formatNumber', 'key', 'human performer', 'duration'),
					new TabData('meanDuration', 'Mean Duration', 'barchart', 'formatDuration', 'key', 'human performer', 'duration'),
					new TabData('medianDuration', 'Median Duration', 'barchart', 'formatDuration', 'key', 'human performer', 'duration'), 
					new TabData('aggregateDuration', 'Aggregate Duration', 'barchart', 'formatDuration', 'key', 'human performer', 'duration') 
				]
			},
			{
				id: 'equipment',
				label: '장비',
				rendered: false,
				statistics: [
					new StatisticData('Non Human Performer', 'formatNumber'),
					new StatisticData('Min Frequency', 'formatNumber'),
					new StatisticData('Mean Frequency', 'formatNumber'),
					new StatisticData('Max Frequency', 'formatNumber'),
					new StatisticData('Std Deviation', 'formatNumber')
				],
//				table: [ 'Non Human Performer', 'Frequency', 'Relative Frequency', 'Max Duration', 'Mean Duration', 'Duration Range' ],
				table: {
					data: [],
					columns:[
						new ColumnData('label', 'Non Human Performer'),
						new ColumnData('durationRangeStr', 'Duration Range'),
						new ColumnData('frequency', 'Frequency'),
						new ColumnData('minDurationStr', 'Min Duration'),
						new ColumnData('sumDurationStr', 'Sum Duration')
					],
				},
				tabs: [
					new TabData('frequency', 'Frequency', 'barchart', 'formatNumber', 'key', 'non human performer', 'duration'),
					new TabData('meanDuration', 'Mean Duration', 'barchart', 'formatDuration', 'key', 'non human performer', 'duration'),
					new TabData('medianDuration', 'Median Duration', 'barchart', 'formatDuration', 'key', 'non human performer', 'duration'), 
					new TabData('aggregateDuration', 'Aggregate Duration', 'barchart', 'formatDuration', 'key', 'non human performer', 'duration') 
				]
			},
		]
	},
	ready: function(){
>>>>>>> 9483c4ccf34b1a4d52e3462345035ea1da301213
		setTimeout(function(){
			$('.pointing.menu .item').tab();
			$('.tabular.menu .item').tab();
		}, 1000)
		
<<<<<<< HEAD
		/*
		 * ALGORITHM of WAITING
		 * 	
		 * 	start page loading
		 * 	request to summary API, then retrieve the response
		 * 		if response status is COMPLETED
		 * 			execute the main function
		 * 			hide page loading
		 * 		if response status is FAILED
		 * 			hide page loading
		 * 		if response status is RUNNING or QUEUED
		 * 			set interval, request to jobQueue API, then retrieve the response
		 * 				if is in queue or in running job
		 * 					do nothing
		 * 				else 
		 * 					request to summary API, then retrieve the response
		 * 						if response status is COMPLETED
		 * 							clear interval
		 * 							execute the main function
		 * 							hide page loading
		 * */
		this.summaryView.waiting = true;
		this.$http.get(summaryViewURI).then(function (response){
			if(response.data.status == 'COMPLETED'){
				this.initializeTabs(response.data.response);
				this.summaryView.waiting = false;
			}
			else if(response.data.status == 'FAILED' || response.data.status == 'UNKNOWN'){
				// if status is not COMPLETED or RUNNING
				this.summaryView.waiting = false;				
		    }
			else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUED'){
				var intervalId = window.setInterval(_.bind(function(){
					console.log('this is check every '+INTERVALDURATION+'ms');
					this.$http.get(jobQueueURI).then(function(response2){
						// success callback
						var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.id}));
						var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.id}));
					  
						if(isJobInQueue || isJobInRunning){
							console.log('current status is QUEUE/RUNNING');
						}
						else{
							console.log('current status is COMPLETED, request POST');
							this.$http.get(summaryViewURI).then(function (response3){
								if(response3.data.status == 'COMPLETED'){
									window.clearInterval(intervalId)
									this.initializeTabs(response3.data.response);
									this.summaryView.waiting = false;
								}
							}, function(response3){
								this.summaryView.waiting = false;
							});
					  }
				  }, function(response2){
					  // error callback
					  this.summaryView.waiting = false;
				  })	  
				}, this), INTERVALDURATION);	
			}			
		}, function(){
			this.summaryView.waiting = false;
=======
		this.isProcessing = true;
		this.$http.get(apiURI.repository.view + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.repositoryId).then(function (response){
			this.$set('summaryView', response.data.response);
			this.isProcessing = false;			
			this.tabs[0].statistics[0].value = response.data.response.noOfCases;
			this.tabs[0].statistics[1].value = response.data.response.noOfEvents;
			this.tabs[0].statistics[2].value = response.data.response.noOfActivities;
			this.tabs[0].statistics[3].value = response.data.response.noOfActivityTypes;
			this.tabs[0].statistics[4].value = response.data.response.noOfOriginators;
			this.tabs[0].statistics[5].value = response.data.response.noOfResourceClasses;
			this.tabs[0].statistics[6].value = response.data.response.timestampStart;
			this.tabs[0].statistics[7].value = response.data.response.timestampEnd;
			
			this.tabs[1].statistics[0].value = response.data.response.noOfCases;
			this.tabs[1].statistics[1].value = response.data.response.noOfEvents;
			this.tabs[2].statistics[0].value = response.data.response.noOfActivities;
			this.tabs[3].statistics[0].value = response.data.response.noOfOriginators;
			this.tabs[4].statistics[0].value = response.data.response.noOfResourceClasses;
			
		}, function(){
			this.isProcessing = false;
>>>>>>> 9483c4ccf34b1a4d52e3462345035ea1da301213
		});
		
		var data = {
			caseTab: true, 
			timelineTab: true, 
			activityTab: true,
			originatorTab: true,
			resourceTab: true
		};
<<<<<<< HEAD
		
		this.summary.waiting = true;		
		this.$http.post(summaryURI, data).then(function (response){
			if(response.data.status == 'COMPLETED'){
				this.summary.waitingLabel = LABEL.RENDER_CHART;
				this.initializeSubTabs(response.data.response, data);
				this.summary.waiting = false;
			}
			else if(response.data.status == 'FAILED'){
				// if status is not COMPLETED or RUNNING
				this.summary.waiting = false;
		    }
			else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUE'){
				// if status is not COMPLETED or RUNNING
				var intervalId = window.setInterval(_.bind(function(){
					console.log('this is check every '+INTERVALDURATION+'ms');
					this.$http.get(jobQueueURI).then(function(response2){
						// success callback
						var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.id}));
						var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.id}));
					  
						if(isJobInQueue || isJobInRunning){
							console.log('current status is QUEUE/RUNNING');
						}
						else{
							console.log('current status is COMPLETED, request POST');
							this.$http.get(summaryURI).then(function (response3){
								if(response3.data.status == 'COMPLETED'){
									window.clearInterval(intervalId)
									this.summary.waitingLabel = 'Loading .. render the chart';
									this.initializeSubTabs(response3.data.response, data);
									this.summary.waiting = false;
								}
							}, function(response3){
								this.summary.waiting = false;
							});
					  }
				  }, function(response2){
					  // error callback
					  this.summary.waiting = false;
				  })	  
				}, this), INTERVALDURATION);
		    }					
			this.summary.waiting = false;
		}, function(){
			this.summary.waiting = false;
=======
		this.isProcessing = true;
		this.$http.post(apiURI.repository.summary + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.repositoryId, data).then(function (response2){
			
			var count=0;
			for(var i in data){	//preformat all data
				preProcessData(this.tabs[count].tabs, response2.data.response[i]);
				
				if(i=='caseTab' || i=='timelineTab'){
					preProcessTableData(this.tabs[count].table, response2.data.response['caseTab'].cases);
				}
				else{
					preProcessTableData(this.tabs[count].table, response2.data.response[i].states);
				}
				count++;
			}			
			
			// render first chart
			this.selectTab(0);			
			this.isProcessing = false;
		}, function(){
			this.isProcessing = false;
>>>>>>> 9483c4ccf34b1a4d52e3462345035ea1da301213
		});			
	},
	methods: {
		selectTab: function(idx){
			console.log('select tab', this.tabs[idx].label);
			if(!this.tabs[idx].tabs[0].rendered){
				renderChart('#'+this.tabs[idx].id+'-'+this.tabs[idx].tabs[0].id, this.tabs[idx].tabs[0]);	// render first chart only
				
				$('#table-'+this.tabs[idx].id).DataTable({
					paging: true,
					pageLength: 50,
                    filter: true,
                    sort: true,
                    info: true,
                    autoWidth: false,
				    data: this.tabs[idx].table.data,
				    columns: this.tabs[idx].table.columns
				});
			}
		},
		selectSubTab: function(idx1, idx2){
			console.log('select tab', this.tabs[idx1].tabs[idx2].label);
			if(!this.tabs[idx1].tabs[idx2].rendered){
<<<<<<< HEAD
				renderChart('#'+this.tabs[idx1].id+'-'+this.tabs[idx1].tabs[idx2].id, this.tabs[idx1].tabs[idx2]);	
			}				
		},
		initializeTabs: function(response){
			this.summaryView.data = response;
			
			this.tabs[0].statistics[0].value = response.noOfCases;
			this.tabs[0].statistics[1].value = response.noOfEvents;
			this.tabs[0].statistics[2].value = response.noOfActivities;
			this.tabs[0].statistics[3].value = response.noOfActivityTypes;
			this.tabs[0].statistics[4].value = response.noOfOriginators;
			this.tabs[0].statistics[5].value = response.noOfResourceClasses;
			this.tabs[0].statistics[6].value = response.timestampStart;
			this.tabs[0].statistics[7].value = response.timestampEnd;
			
			this.tabs[1].statistics[0].value = response.noOfCases;
			this.tabs[1].statistics[1].value = response.noOfEvents;
			this.tabs[2].statistics[0].value = response.noOfActivities;
			this.tabs[3].statistics[0].value = response.noOfOriginators;
			this.tabs[4].statistics[0].value = response.noOfResourceClasses;
		},
		initializeSubTabs: function(response, data){
			
			var count=0;
			for(var i in data){	//preformat all data
				preProcessData(this.tabs[count].tabs, response[i]);
				
				if(i=='caseTab' || i=='timelineTab'){
					preProcessTableData(this.tabs[count].table, response['caseTab'].cases);
				}
				else{
					preProcessTableData(this.tabs[count].table, response[i].states);
				}
				count++;
			}			
			// render first chart
			this.selectTab(0);	
=======
				renderChart('#'+this.tabs[idx1].id+'-'+this.tabs[idx1].tabs[idx2].id, this.tabs[idx1].tabs[idx2]);
			}
>>>>>>> 9483c4ccf34b1a4d52e3462345035ea1da301213
		}
	}
});