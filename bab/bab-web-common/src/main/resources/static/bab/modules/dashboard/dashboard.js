/*
 * author @superpikar (dzulfikar.adiputra@gmail.com)
 */

// default value for chart size
chartSize = {
	width: 600,
	height: 300
}

/*
 * credit : calculate std deviation http://derickbailey.com/2014/09/21/calculating-standard-deviation-with-array-map-and-array-reduce-in-javascript/
 * */
function calculateStatisticsData(values){
	if(values.length > 1){
		var sum = _.sum(values);
		var mean = sum/values.length;
		var max = _.max(values);
		var min = _.min(values);
		var diffs = _.map(values, function(value){
			var diff = value - mean;
			return diff;
		});
		var squareDiffs = _.map(values, function(value){
			var diff = value - mean;
			var sqr = diff * diff;
			return sqr;
		});
		var avgSquareDiff = _.sum(squareDiffs)/squareDiffs.length;
		return {
			min: min,
			mean: mean,
			max: max,
			stdDev: Math.round(Math.sqrt(avgSquareDiff))		
		}		
	}
	else{
		return {
			min: values[0],
			mean: values[0],
			max: values[0],
			stdDev: values[0]
		}
	}
}

function setStatisticsData(tab, objData){
	if(tab.statisticsValue == 'value'){
		values = _.map(objData, function(val, key){ return parseInt(val); });	
	}
	else if(tab.statisticsValue == 'key'){
		values = _.map(objData, function(val, key){ return parseInt(key); });		
	}
	var stat = calculateStatisticsData(values);
//	console.log('statistic data of tab ', tab.id, objData, stat);
	tab.statistics[0].value = stat.min;
	tab.statistics[1].value = stat.mean;
	tab.statistics[2].value = stat.max;
	tab.statistics[3].value = stat.stdDev;	
}

function preProcessTableData(table, jsonData){
	if(!_.isUndefined(table)){
		table.data = _.map(jsonData, function(val, key){
			return val;
		});
	}
}

function preProcessData(tabs, jsonData){
	for(var i in tabs){
		var count = 0;
//		console.log('---------------------- preprocess ', tabs[i].id);
		var obj = jsonData[tabs[i].id];
		if(tabs[i].type == 'barchart'){
			tabs[i].data = [];
			
			if(tabs[i].format == 'formatNumber'){
				for(var j in obj){
//					console.log(parseInt(i), parseInt(obj[j]));
					tabs[i].data.push({ key: parseInt(i), value: parseInt(obj[j]) });
					count++;
				}
//				setStatisticsData(tabs[i], obj);				
			}
			else if(tabs[i].format == 'formatDuration'){
				var data = calculateAggregateDurationData(obj);
				for(var j in data.chartData){
//					console.log(j, parseInt(data.chartData[j]));
					tabs[i].data.push({ key: j, value: parseInt(data.chartData[j]) });
					count++;
				}
//				setStatisticsData(tabs[i], obj);
				
				// update x axis label if the x axis label is duration
				tabs[i].axis.x = tabs[i].axis.x + '('+data.unit+')';
			}
		}
		
		else if(tabs[i].type == 'linechart'){
			tabs[i].data = [];
			for(var j in obj){
				var thedate = new Date(parseInt(j)); 
				if(_.isDate(thedate)){
//					console.log(thedate)
					tabs[i].data.push([ thedate, obj[j] ]);
					count++;
				}
			}
		}
		setStatisticsData(tabs[i], obj);
		tabs[i].dataLength = count;
	}
}

/* from old bab */
function calculateAggregateDurationData(series){
	var maxDuration = _.max(_.map(_.keys(series), function(val){ return parseInt(val); }));
	var unit = BabHelper.getDurationUnit(maxDuration);

	var arrMap = _.map(series, function(val, key){
		return {
			key: parseInt(key),
			val: val,
			duration: BabHelper.getDurationUnitValue(parseInt(key), unit)
		}
	});
	var objGroup = _.groupBy(arrMap, function(val, key){ return val.duration });
	var objGroupSum = _.mapValues(objGroup, function(val, key){
		arrValue = _.map(val, function(v, k){ return v.val });
		return _.sum(arrValue);
	});

	// make dummy object to show zero value on non existing groupsum key
	var dummyObj = {};
	var maxGroupKey = _.max(_.without(_.map(_.keys(objGroupSum), function(val){ return parseInt(val); }), "<1"));
	for(var i=1; i<= maxGroupKey; i++){
		dummyObj[i] = 0;
	}

	return {
		unit: unit,
		chartData: _.extend(dummyObj, objGroupSum),
		statisticsData: _.map(series, function(val, key){ return parseInt(key); })
	};
}

function renderChart(el, tab){
//	console.log('render chart ', el, tab.type, chartSize.width+'px', chartSize.height+'px', tab.dataLength, tab.data);
	tab.isProcessing = true;
	//console.log('render', el, tab.data);
	if(tab.type == 'barchart'){
		if(tab.dataLength==0){
			tab.dataNotEnough = true;
			tab.isProcessing = false;
		}
		else if(tab.dataLength>0){
			window.setTimeout(function(){				
				c3.generate({
					bindto: el,
					size: chartSize,
					data: {
						json: tab.data, 
						keys: {
							x: 'key', // it's possible to specify 'x' when category axis
							value: ['value'],
						},
						names: {
							value: tab.axis.y,
						},
						type: 'bar'
							
					},
					bar: {
						width: {
							ratio: 0.5 // this makes bar width 50% of length between ticks
						}
					},
					axis: {
						x: {
							label: tab.axis.x,
							type: 'category'
						},
						y: {
							label: tab.axis.y
						}
					}
				});
				tab.isProcessing = false;
			}, 500);
		}			
	}
	else if(tab.type == 'linechart'){
		if(tab.dataLength < 2){
			tab.dataNotEnough = true;
			tab.isProcessing = false;
		}
		else if(tab.dataLength > 1){			
			window.setTimeout(function(){
				var dygraph = new Dygraph(document.getElementById(el.substr(1)),
						tab.data,
						{
					legend: 'always',
					height: chartSize.height, 
					width: chartSize.width, 
//					showRoller: true,
//					showRangeSelector: true,
//					rollPeriod: 14,
					ylabel: tab.axis.y,
					labels: [ "x", tab.axis.x ]
						}
				)
				tab.isProcessing = false;
			}, 500);
		}
	}
	tab.status = 'FINISHED' ;
	tab.rendered = true;
}

var StatisticComponent = Vue.extend({
	props: ['label', 'value', 'format', 'theclass'],
	computed: {
		formatNumber: function(){
			return this.format == 'formatNumber';
		},
		formatDateTime: function(){
			return this.format == 'formatDateTime';
		},
		noFormat: function(){
			return _.isUndefined(this.format);
		}
	},
	template:
		'<div class="ui statistic" v-bind:class="{mini:formatDateTime, tiny:formatNumber}">'+
		'<div class="value" v-if="formatNumber">{{value | formatNumber}}</div>'+
		'<div class="value" v-if="formatDateTime">{{value | formatDateTime}}</div>'+
		'<div class="value" v-if="noFormat">{{value}}</div>'+
		'<div class="label">{{label}}</div>'+
		'</div>'
});

var LoaderComponent = Vue.extend({
	  props: {
	  	label: { default: 'Loading' },
	  	color: { default: 'inverted' },
	  	size: { default: 'medium' },
	  	isactive: { default: false }
	  },
	  template:
		'<div class="ui dimmer" v-bind:class="[size, color, { active: isactive }]">' +
		    '<div class="ui text loader">{{label}}</div>' +
		  '</div>'
	  ,
	  ready: function(){
	  }
	})

// vue initialization
new Vue({
	el: '#app',
	components: {
		'statistic': StatisticComponent,
		'loader': LoaderComponent
	},
	data: {
		
		summaryView: {
			waiting: false,
			data: {}
		},
		summary:{
			waiting: false
		},
		tabs: tabsData,
		summaryURI : "",
		summaryViewURI : "",
		jobQueueURI : "",
		intervalDuration : 0
	},
	ready: function(){
		
		this.summaryURI = apiURI.repository.summary
			+ jsonData.workspaceId
			+ '/' + jsonData.datasetId
			+ '/' + jsonData.sdt
			+ '/' + jsonData.edt;
		this.summaryViewURI = apiURI.repository.view
			+ jsonData.workspaceId
			+ '/' + jsonData.datasetId
			+ '/' + jsonData.sdt
			+ '/' + jsonData.edt
			
			
		this.jobQueueURI = apiURI.job.queue;
		this.intervalDuration = 3000;
		
//		console.log("jobQueueURI : " + jobQueueURI);
//		console.log("summaryURI : " + summaryURI);
//		console.log("summaryViewURI : " + summaryViewURI);
		
		setTimeout(function(){
			$('.pointing.menu .item').tab();
			$('.tabular.menu .item').tab();
		}, 1000)
		
		/*
		 * ALGORITHM of WAITING
		 * 	
		 * 	start page loading
		 * 	request to summary API, then retrieve the response
		 * 		if response status is FINISHED
		 * 			execute the main function
		 * 			hide page loading
		 * 		if response status is FAILED
		 * 			hide page loading
		 * 		if response status is RUNNING or QUEUED
		 * 			set interval, request to jobQueue API, then retrieve the response
		 * 				if is in queue or in running job
		 * 					do nothing
		 * 				else 
		 * 					request to summary API, then retrieve the response
		 * 						if response status is FINISHED
		 * 							clear interval
		 * 							execute the main function
		 * 							hide page loading
		 * */
//		console.log(this.summary.waiting);
//		console.log(this.summaryView.waiting);
		
		
		this.setSummaryView();


	},
	methods: {
		setSummaryView : function() {
			this.summaryView.waiting = true;
			console.log("[STEP1-1] getView request : " + this.summaryViewURI);
			this.$http.get(this.summaryViewURI).then(function (response){
				console.log(response);
				var summaryViewRes = response;
				var summaryViewResStatus = summaryViewRes.data.status;

				if(summaryViewResStatus == 'FINISHED'){
					this.initializeTabs(summaryViewRes.data.response);
					this.summaryView.waiting = false;
					this.setSummary();
				} else if(summaryViewResStatus == 'FAILED') {
					// if status is not FINISHED or RUNNING
					this.summaryView.waiting = false;
					alert("error");
			    } else if(summaryViewResStatus == 'RUNNING' || summaryViewResStatus == 'QUEUE') {
					var summaryViewQueueChecker = window.setInterval(_.bind(function(){
						console.log("[STEP1-2] get View's Queue request : " + this.jobQueueURI + summaryViewRes.data.request.id);
						this.$http.get(this.jobQueueURI + summaryViewRes.data.request.id).then(function(response){
							console.log(response);
							var summaryViewQueueRes = response;
							var summaryViewQueueResStatus = summaryViewQueueRes.data.status;
							// success callback	
							if (summaryViewQueueResStatus == 'RUNNING') {
							} else if (summaryViewQueueResStatus == 'FAILED'){
								window.clearInterval(summaryViewQueueChecker);
								alert("FAILED : " + this.jobQueueURI + summaryViewQueueRes.data.request.id);
							} else if (summaryViewQueueResStatus == 'FINISHED') {
								window.clearInterval(summaryViewQueueChecker);
								console.log("[STEP1-3] get FINISHED summary view request : " + this.summaryViewURI);
								this.$http.get(this.summaryViewURI).then(function (response){
									console.log(response);
									var summaryViewRes = response;
									var summaryViewResStatus = summaryViewRes.data.status;
									if(summaryViewResStatus == 'FINISHED'){
										this.initializeTabs(summaryViewRes.data.response);	
										this.setSummary();
										this.$http.get(apiURI.model.heuristic + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt)
										this.$http.post(apiURI.model.logreplay + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt, JSON.stringify({"limit":5000}))
										this.$http.get(apiURI.analysis.taskmatrix + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt)
										this.$http.get(apiURI.analysis.dottedchart + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt)
										this.$http.post(apiURI.analysis.associationrule + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt, JSON.stringify({"threshold":{"support":{"min":0,"max":1},"confidence":{"min":0,"max":1}}}))
										this.$http.get(apiURI.analysis.socialnetwork + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt)
										this.$http.get(apiURI.analysis.timegap + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt)
									} else {
										alert("error")
									}
								}, function(response3){
									this.summaryView.waiting = false;
								});
							}
						}, function(response){
							// error callback
							this.summaryView.waiting = false;
						})
					}, this), this.intervalDuration);
				}
			}, function(){
				this.summaryView.waiting = false;
			});
		},	
		setSummary : function() {
			this.summary.waiting = true;
			var data = {
					caseTab: true, 
					timelineTab: true, 
					activityTab: true,
					originatorTab: true,
					resourceTab: true
			}	
			console.log("[STEP2-1] get summary request : " + this.summaryURI);
			this.$http.post(this.summaryURI, JSON.stringify(data)).then(function (response) {
				console.log(response);
				var summaryRes = response;
				var summaryResStatus = summaryRes.data.status;
				if(summaryResStatus == 'FINISHED') {
					this.summary.waiting = false;
					this.initializeSubTabs(summaryRes.data.response, data);
				} else if(summaryResStatus == 'FAILED'){
					// if status is not FINISHED or RUNNING
					this.summary.waiting = false;
				} else if(summaryResStatus== 'RUNNING' || summaryResStatus == 'QUEUE') {
					// if status is not FINISHED or RUNNING
					var summaryQueueChecker = window.setInterval(_.bind(function(){
						console.log("[STEP2-2] get summary's queue request : " + this.jobQueueURI + summaryRes.data.request.id);
						this.$http.get(this.jobQueueURI + summaryRes.data.request.id).then(function(response){
							console.log(response);
							// success callback
							var summaryQueueRes = response;
							var summaryQueueResStatus = summaryQueueRes.data.status;
							
							if (summaryQueueResStatus == 'RUNNING') {
							} else if (summaryQueueResStatus == 'FAILED'){
								window.clearInterval(summaryQueueChecker);
								alert("FAILED : " + this.jobQueueURI + summaryQueueRes.data.request.id);
							} else if (summaryQueueResStatus == 'FINISHED'){
								window.clearInterval(summaryQueueChecker);
								console.log("[STEP2-3] get FINISHED summary request : " + this.summaryURI);
								this.$http.post(this.summaryURI, JSON.stringify(data)).then(function (response){
									console.log(response);
									var summaryRes = response;
									var summaryResStatus = summaryRes.data.status;
									if(summaryResStatus == 'FINISHED'){
										window.clearInterval(summaryQueueChecker)
										this.summaryView.waiting = false;
										this.summary.waiting = false;
										this.initializeSubTabs(summaryRes.data.response, data);
										}
									}, function(response){
										this.summary.waiting = false;
									});
							}
						} , function(response) {
						  // error callback
						  this.summary.waiting = false;
						});  
					}, this), this.intervalDuration);
				}
				this.summary.waiting = false;
			}, function(){
				this.summary.waiting = false;
			});
		},
		selectTab: function(idx){
//			console.log('select tab', this.tabs[idx].label);
			if(!this.tabs[idx].tabs[0].rendered){
				renderChart('#'+this.tabs[idx].id+'-'+this.tabs[idx].tabs[0].id, this.tabs[idx].tabs[0]);	// render first chart only
				
				$('#table-'+this.tabs[idx].id).DataTable({
					paging: true,
					pageLength: 50,
                    filter: true,
                    sort: true,
                    info: true,
                    autoWidth: false,
				    data: this.tabs[idx].table.data,
				    columns: this.tabs[idx].table.columns
				});
			}
		},
		selectSubTab: function(idx1, idx2){
//			console.log('select tab', this.tabs[idx1].tabs[idx2].label);
			
			if(!this.tabs[idx1].tabs[idx2].rendered){
//				window.setTimeout(_.bind(function(){
					renderChart('#'+this.tabs[idx1].id+'-'+this.tabs[idx1].tabs[idx2].id, this.tabs[idx1].tabs[idx2]);
//				}, this), 1000)
			}
		},
		initializeTabs: function(response){
			this.summaryView.data = response;
			
			this.tabs[0].statistics[0].value = response.noOfCases;
			this.tabs[0].statistics[1].value = response.noOfEvents;
			this.tabs[0].statistics[2].value = response.noOfActivities;
			this.tabs[0].statistics[3].value = response.noOfActivityTypes;
			this.tabs[0].statistics[4].value = response.noOfOriginators;
			this.tabs[0].statistics[5].value = response.noOfResourceClasses;
			this.tabs[0].statistics[6].value = response.timestampStart;
			this.tabs[0].statistics[7].value = response.timestampEnd;
			
			this.tabs[1].statistics[0].value = response.noOfCases;
			this.tabs[1].statistics[1].value = response.noOfEvents;
			this.tabs[2].statistics[0].value = response.noOfActivities;
			this.tabs[3].statistics[0].value = response.noOfOriginators;
			this.tabs[4].statistics[0].value = response.noOfResourceClasses;
			
			chartSize.width = $('.ten.wide.column').width() - 60;
			chartSize.height = $('.six.wide.column').height() - 50;
//			console.log('chartSize', chartSize)	
		},
		initializeSubTabs: function(response, data){
			var count=0;
			for(var i in data){	//preformat all data
				preProcessData(this.tabs[count].tabs, response[i]);
				
				if(i=='caseTab' || i=='timelineTab'){
					preProcessTableData(this.tabs[count].table, response['caseTab'].cases);
				}
				else{
					preProcessTableData(this.tabs[count].table, response[i].states);
				}
				count++;
			}			
			// render first chart
			this.selectTab(0);
		}
	}
});