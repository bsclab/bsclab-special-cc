/* 
 * Semantic-UI calendar 
 * https://github.com/mdehoog/Semantic-UI-Calendar
 * https://jsbin.com/qifuhadoqa/edit?html,js,output
 */



/*
 * loading animation when ajax is working
 */
$body = $("body");
$(document).on({
	ajaxStart : function() {
		$body.addClass("loading");
	},
	ajaxStop : function() {
		$body.removeClass("loading");
	}
});

/*
 * #factory_select On Change Action for Factory Combo Box To Show Machine of
 * selected Factory
 */

$(document).ready(function() {
	
	$('#end_date').calendar({
		type : 'datetime',
		ampm : false,
		formatter : {
			date : function(date, settings) {
				if (!date)
					return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return year + '-' + month + '-' + day;
			}
		}
	});

	$('#start_date').calendar({
		type : 'datetime',
		ampm : false,
		formatter : {
			date : function(date, settings) {
				if (!date)
					return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return year + '-' + month + '-' + day;
			}
		}
	});

	$.ajax({
		url : baseUrl + '/pmfcc/facility_rest/get_factories',
		type : 'POST',
		dataType : 'json',
		contentType : "application/json",
		success : function(res) {
			for (var i = 0; i < res.length; i++) {
				var opt = "<option value='" + res[i].id + "'>" + res[i].name + "</option>";

				$("#factory_select").append(opt);
			}

			$('#factory_select').trigger('change');
		},
		error : function(x, e) {
			console.log(e);
		}
	});

	$('#factory_select').on('change', function() {
		var factory_id = this.value;
		$.ajax({
			url : baseUrl + '/pmfcc/facility_rest/get_machines',
			dataType : 'json',
			contentType : "application/json",
			type : 'POST',
			data : JSON.stringify({
				"id" : factory_id
			}),
			success : function(res) {

				if (res.length > 0) {
					$('#machines_select').empty();

					var isHardCode = false;
					if (machineIdPrev == '')
						isHardCode = true;

					for (var i = 0; i < res.length; i++) {
						var opt = "<option value='" + res[i].id + "'>" + res[i].name + "</option>";
						if (res[i].id == machineIdPrev) {
							opt = "<option value='" + res[i].id + "' selected>" + res[i].name + "</option>";
							machineIdPrev = '';
						} else if (isHardCode == true && res[i].id == 'MC_0001') {
							opt = "<option value='" + res[i].id + "' selected>" + res[i].name + "</option>";
							$('#start_date_field').val('2018-11-30 0:00');
							$('#end_date_field').val('2018-12-01 0:00');
						}
						$("#machines_select").append(opt);

						$('#machines_select').trigger('change');
					}
					renderChart();
				} else {
					$('#machines_select').empty();
					$("#machines_select").append("<option>설비명</option>");
				}
			},
			error : function(x, e) {
				console.log(e);
			}
		});
	});

	$('#machines_select').on('change', function() {
		var mc_id = this.value;
		$.ajax({
			url : baseUrl + '/pmfcc/monitoring_rest/get_machine_parameters',
			type : 'POST',
			contentType : "application/json",
			data : JSON.stringify({
				"id" : mc_id
			}),
			success : function(response) {
				$('#cbx_machine_parameters').empty();
				$('#cbx_machine_parameters').html(response);
				$('.ui.checkbox').checkbox();
			},
			error : function(x, e) {
				console.log(e);
			}
		});
	});

	/*
	 * #runquery On Click Action for Run Button To get result from database
	 */
	$('.runquery').on('click', function() {
		renderChart();
	})

});

/*
 * Global variables regarding chart
 */
var DATA_CHART, GRAPH, VIS;

/*
 * generate chart http://dygraphs.com/
 */
function renderChart() {
	var factory_id = $('#factory_select').val();
	var mc_id = $('#machines_select').val();
	var end_date_field = $('#end_date_field').val();
	var start_date_field = $('#start_date_field').val();

	$.ajax({
		url : baseUrl + '/pmfcc/facility_rest/get_results',
		type : 'POST',
		dataType : 'json',
		contentType : "application/json",
		data : JSON.stringify({
			"factory_id" : factory_id,
			"machine_id" : mc_id,
			"start_date_field" : start_date_field,
			"end_date_field" : end_date_field
		}),
		success : function(log) {
			DATA_CHART = log;

			for (var i = 0; i < DATA_CHART['data'].length; i++) {
				DATA_CHART['data'][i][0] = new Date(DATA_CHART['data'][i][0]);

				for (var j = 1; j < DATA_CHART['data'][i].length; j++) {
					DATA_CHART['data'][i][j] = parseFloat(DATA_CHART['data'][i][j]);
				}
			}

			VIS = [];
			for (var i = 0; i < DATA_CHART['column_idx'].length; i++) {
				if ($('[name=D' + DATA_CHART['column_idx'][i] + ']').is(":checked"))
					VIS.push(true);
				else
					VIS.push(false);
			}

			GRAPH = new Dygraph(document.getElementById("chart"), DATA_CHART['data'], {
				labels : DATA_CHART['label'],
				legend : 'always',
				visibility : VIS
			});
		},
		error : function(x, e) {
			console.log(e);
		}
	});
}

/*
 * On Change Action for parameters Checkbox
 */
$(document).on("change", '.checkparameter', function() {
	VIS = [];
	for (var i = 0; i < DATA_CHART['column_idx'].length; i++) {
		if ($('[name=D' + DATA_CHART['column_idx'][i] + ']').is(":checked"))
			VIS.push(true);
		else
			VIS.push(false);
	}

	GRAPH.updateOptions({
		visibility : VIS
	});
});

/*
 * shifting event click
 */
$('.shifthour').click(function() {
	var start = $('#start_date_field').val() + ':00';
	start = start.replace("-", "/");
	start = new Date(start);

	var end = $('#end_date_field').val() + ':00';
	end = end.replace("-", "/");
	end = new Date(end);

	if ($(this).attr('data') == 'forward') {
		start.setHours(start.getHours() + 1);
		end.setHours(end.getHours() + 1);
	} else if ($(this).attr('data') == 'backward') {
		start.setHours(start.getHours() - 1);
		end.setHours(end.getHours() - 1);
	} else if ($(this).attr('data') == 'ninety-forward') { // shift forward 90%
		var timeDiff = Math.abs(end.getTime() - start.getTime());
		timeDiff /= 1000;
		timeDiff *= 90;
		timeDiff /= 100;

		start.setSeconds(start.getSeconds() + (timeDiff));
		end.setSeconds(end.getSeconds() + (timeDiff));
	} else {
		var timeDiff = Math.abs(end.getTime() - start.getTime());
		timeDiff /= 1000;
		timeDiff *= 90;
		timeDiff /= 100;

		start.setSeconds(start.getSeconds() - (timeDiff));
		end.setSeconds(end.getSeconds() - (timeDiff));
	}

	var newStart = start.getFullYear() + '-' + (start.getMonth() + 1) + '-' + start.getDate() + ' ' + start.getHours() + ':' + pad(start.getMinutes().toString(), 2);
	var newEnd = end.getFullYear() + '-' + (end.getMonth() + 1) + '-' + end.getDate() + ' ' + end.getHours() + ':' + pad(end.getMinutes().toString(), 2);

	$('#start_date_field').val(newStart);
	$('#end_date_field').val(newEnd);

	renderChart();
});

/*
 * select all checkbox event click
 */
$('.selectall').click(function() {
	if ($(this).attr('data') == 'select') {
		for (var i = 0; i < DATA_CHART['column_idx'].length - 1; i++) {
			$('[name=D' + DATA_CHART['column_idx'][i] + ']').prop('checked', true);
		}
		$('[name=D' + DATA_CHART['column_idx'][DATA_CHART['column_idx'].length - 1] + ']').prop('checked', true).trigger('change');
	} else {
		for (var i = 0; i < DATA_CHART['column_idx'].length - 1; i++) {
			$('[name=D' + DATA_CHART['column_idx'][i] + ']').prop('checked', false);
		}
		$('[name=D' + DATA_CHART['column_idx'][DATA_CHART['column_idx'].length - 1] + ']').prop('checked', false).trigger('change');
	}
});
