/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.api.common.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;

@Controller
public class SparkController extends ASparkServiceController {

	public static final String BASE_URL = AServiceController.BASE_URL + "/spark";

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/submit/{packageId}/{serviceId}/{workspaceId}/{datasetId}")
	@ResponseBody
	public IServiceResponse postSubmit(@PathVariable(value = "packageId") String packageId,
			@PathVariable(value = "serviceId") String serviceId,
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@RequestBody(required = false) String json) {
		return postSubmit(packageId, serviceId, workspaceId, datasetId, "", "", json);
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/submit/{packageId}/{serviceId}/{workspaceId}/{datasetId}/{repositoryId}")
	@ResponseBody
	public IServiceResponse postSubmit(@PathVariable(value = "packageId") String packageId,
			@PathVariable(value = "serviceId") String serviceId,
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId, @RequestBody(required = false) String json) {
		return postSubmit(packageId, serviceId, workspaceId, datasetId, repositoryId, "", json);
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/submit/{packageId}/{serviceId}/{workspaceId}/{datasetId}/{repositoryId}/{filterId}")
	@ResponseBody
	public IServiceResponse postSubmit(@PathVariable(value = "packageId") String packageId,
			@PathVariable(value = "serviceId") String serviceId,
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId,
			@PathVariable(value = "filterId") String filterId, 
			@RequestBody(required = false) String json) {
		ServiceEndpoint service = getJobManager().getServices(0).get(packageId).get(serviceId);
		Class<? extends IServiceRequest> configClass = service.getService().requestClass();
		IServiceRequest config = null;
		if (json != null) {
			try {
				config = getMapper().readValue(json, configClass);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (config == null) {
			try {
				config = configClass.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		IServiceResponse response = runService(service, workspaceId, datasetId, repositoryId, config);
		return response;
	}

}
