/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.services.spark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import kr.ac.pusan.bsclab.bab.v2.web.models.AConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.models.BabConfiguration;

@Configuration
@ConfigurationProperties("spark")
public class SparkConfiguration extends AConfiguration {

	public static final String MODE_CLUSTER = "cluster";
	public static final String MODE_LOCAL = "local";

	@Autowired
	protected BabConfiguration bab;

	protected String masterIp;
	protected String masterUrl;
	protected String url;
	protected String restUrl;
	protected String mode = MODE_CLUSTER;
	protected String version = "1.6.0";

	protected String jars = "";

	protected String babName = "";

	protected String driverSupervise = "false";
	protected String driverCores = "0";
	protected String driverMemory = "0";
	protected String driverClassPath = "";
	protected String driverExtraClassPath = "";

	protected String executorCores = "0";
	protected String executorMemory = "0";
	protected String executorExtraClassPath = "";

	protected String eventLogEnabled = "true";
	protected String eventLogDir = "";

	public BabConfiguration getBab() {
		return bab;
	}

	public void setBab(BabConfiguration bab) {
		this.bab = bab;
	}

	public String getMasterIp() {
		return masterIp;
	}

	public void setMasterIp(String masterIp) {
		this.masterIp = masterIp;
	}

	public String getMasterUrl() {
		return masterUrl;
	}

	public void setMasterUrl(String masterUrl) {
		this.masterUrl = masterUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRestUrl() {
		return restUrl;
	}

	public void setRestUrl(String restUrl) {
		this.restUrl = restUrl;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getJars() {
		return jars;
	}

	public void setJars(String jars) {
		this.jars = jars;
	}

	public String getBabName() {
		return babName;
	}

	public void setBabName(String babName) {
		this.babName = babName;
	}

	public String getDriverSupervise() {
		return driverSupervise;
	}

	public void setDriverSupervise(String driverSupervise) {
		this.driverSupervise = driverSupervise;
	}

	public String getDriverCores() {
		return driverCores;
	}

	public void setDriverCores(String driverCores) {
		this.driverCores = driverCores;
	}

	public String getDriverMemory() {
		return driverMemory;
	}

	public void setDriverMemory(String driverMemory) {
		this.driverMemory = driverMemory;
	}

	public String getDriverClassPath() {
		return driverClassPath;
	}

	public void setDriverClassPath(String driverClassPath) {
		this.driverClassPath = driverClassPath;
	}

	public String getDriverExtraClassPath() {
		return driverExtraClassPath;
	}

	public void setDriverExtraClassPath(String driverExtraClassPath) {
		this.driverExtraClassPath = driverExtraClassPath;
	}

	public String getExecutorCores() {
		return executorCores;
	}

	public void setExecutorCores(String executorCores) {
		this.executorCores = executorCores;
	}

	public String getExecutorMemory() {
		return executorMemory;
	}

	public void setExecutorMemory(String executorMemory) {
		this.executorMemory = executorMemory;
	}

	public String getExecutorExtraClassPath() {
		return executorExtraClassPath;
	}

	public void setExecutorExtraClassPath(String executorExtraClassPath) {
		this.executorExtraClassPath = executorExtraClassPath;
	}

	public String getEventLogEnabled() {
		return eventLogEnabled;
	}

	public void setEventLogEnabled(String eventLogEnabled) {
		this.eventLogEnabled = eventLogEnabled;
	}

	public String getEventLogDir() {
		return eventLogDir;
	}

	public void setEventLogDir(String eventLogDir) {
		this.eventLogDir = eventLogDir;
	}

}