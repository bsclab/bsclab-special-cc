/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.web.BabWebCommon;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.models.Response;
import kr.ac.pusan.bsclab.bab.v2.web.common.controllers.ACommonWebController;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.models.ApiManager;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.models.JsonResponse;

@Controller
public abstract class AbstractWebController extends ACommonWebController {
//	public final BabConfiguration config = new BabConfiguration();
//	public final Map<String, Map<String, String>> APIURI = config.APIURI; 
//	public final Map<String, Map<String, String>> WEBURI = config.WEBURI; 

	public static final String BASE_URL = BabWebCommon.BASE_URL;

	@Autowired
	protected ApiManager apiManager;
	
	public <K, T> Response<T> callBabService(String uri, K config, Class<T> responseClass) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			MultiValueMap<String, Object> request = new LinkedMultiValueMap<String, Object>();
			request.add("config", mapper.writeValueAsString(config));
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			HttpEntity<MultiValueMap<String, Object>> entity
				= new HttpEntity<MultiValueMap<String, Object>>(request, headers);
			ResponseEntity<String> json = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
			JsonResponse immResult = mapper.readValue(json.getBody(), JsonResponse.class);
			String responseJson = mapper.writeValueAsString(immResult.getResponse());
			T response = mapper.readValue(responseJson, responseClass);
			Response<T> result = new Response<T>(immResult.getRequest(), response);
			result.setId(immResult.getId());
			result.setStatus(immResult.getStatus());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public <K, T> Response<T> callBabServiceV2(String uri, K config, Class<T> responseClass) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			String jsonReq = mapper.writeValueAsString(config);
			HttpEntity<String> entity = new HttpEntity<String>(jsonReq, headers);
			ResponseEntity<String> json = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
			JsonResponse immResult = mapper.readValue(json.getBody(), JsonResponse.class);
			String responseJson = mapper.writeValueAsString(immResult.getResponse());
			T response = mapper.readValue(responseJson, responseClass);
			Response<T> result = new Response<T>(immResult.getRequest(), response);
			result.setId(immResult.getId());
			result.setStatus(immResult.getStatus());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Map<String, String> buildJSONWorkspaceDatasetRepository(String workspaceId, String datasetId, String sdt, String edt){
		Map<String, String> jsonData= new HashMap<String, String>();
        jsonData.put("workspaceId", workspaceId);
        jsonData.put("datasetId", datasetId);
        jsonData.put("sdt", sdt);
        jsonData.put("edt", edt);
        jsonData.put("wd", workspaceId+"/"+datasetId);
        jsonData.put("wdr", workspaceId+"/"+datasetId+"/"+sdt+"/"+edt);
        
        return jsonData;
	}

}
