/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.common.controllers;

import java.util.Map;
import java.util.TreeMap;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import kr.ac.pusan.bsclab.bab.v2.common.data.matrix.models.Matrix2D;
import kr.ac.pusan.bsclab.bab.v2.common.process.graph.models.Node;
import kr.ac.pusan.bsclab.bab.v2.common.views.NodeRenderer;
import kr.ac.pusan.bsclab.bab.v2.common.views.Matrix2DRenderer;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.legacy.factories.Matrix2DFactory;
import kr.ac.pusan.bsclab.bab.v2.legacy.factories.NodeFactory;
import kr.ac.pusan.bsclab.bab.v2.web.api.common.controllers.SparkController;
import kr.ac.pusan.bsclab.bab.v2.web.components.WebPage;
import kr.ac.pusan.bsclab.bab.v2.web.services.Job;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkResponse;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.Result;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialNetworkAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config.SocialNetworkAnalysisConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.SocialNetworkAnalysis;

@Controller
public class BabTestController extends ACommonWebController {

	public static final String BASE_URL = ACommonWebController.BASE_URL + "/test";

	protected ObjectMapper prettyMapper;
	protected Map<String, TestScenario> scenarios;

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
	public @ResponseBody WebPage<Node> getIndex() {
		Node model = null;
		WebPage<Node> page = new WebPage<Node>("web/bab/test/index", model);
		page.addObject("scenarios", getScenarios());
		return page;
	}

	@RequestMapping(method = RequestMethod.POST, path = BASE_URL + "/default")
	public @ResponseBody String postDefault(@RequestParam("alg") String alg) {
		String result = "{}";
		if (getScenarios().containsKey(alg)) {
			try {
				TestScenario scenario = getScenarios().get(alg);
				result = getPrettyMapper().writeValueAsString(scenario.getDefaultConfig());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.POST, path = BASE_URL + "/json")
	public @ResponseBody String postJson(@RequestParam("alg") String alg, @RequestParam("req") String req,
			@RequestParam("viz") String viz) {
		String result = "{}";
		if (getScenarios().containsKey(alg)) {
			try {
				TestScenario scenario = getScenarios().get(alg);
				Configuration creq = getPrettyMapper().readValue(req, scenario.getConfigClass());
				String uri = config.getUrl() + SparkController.BASE_URL + "/submit/" + scenario.getServiceUri() + "/"
						+ creq.getWorkspaceId() + "/" + creq.getDatasetId() + "/" + creq.getRepositoryId();
				String json = req;
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
				HttpEntity<String> entity = new HttpEntity<String>(json, headers);
				ResponseEntity<SparkResponse> resApi = restService.exchange(uri, HttpMethod.POST, entity,
						SparkResponse.class);
				SparkResponse res = resApi.getBody();
				if (res.getStatus().equals(Job.STATUS_COMPLETED) || res.getStatus().equals(Job.STATUS_FINISHED)) {
					if (res.getResponse() != null) {
						String resJson = getPrettyMapper().writeValueAsString(res.getResponse());
						ServiceResponse model = getPrettyMapper().readValue(resJson, scenario.getResultClass());
						result = getPrettyMapper().writeValueAsString(model);
						try {
							if (viz.equalsIgnoreCase("process")) {
								Node processModel = NodeFactory
										.create(getPrettyMapper().readValue(result, scenario.getResultClass()));
								if (processModel != null) {
									NodeRenderer r = new NodeRenderer();
									result = r.render(processModel);
								} else {
									result = "N/A";
								}
							}
							if (viz.equalsIgnoreCase("matrix2d")) {
								Matrix2D<String, String, String> matrix2d = Matrix2DFactory
										.create(getPrettyMapper().readValue(result, scenario.getResultClass()));
								if (matrix2d != null) {
									Matrix2DRenderer<String, String, String> r = new Matrix2DRenderer<String, String, String>();
									result = r.render(matrix2d);
								} else {
									result = "N/A";
								}
							}
						} catch (Exception ex) {
							ex.printStackTrace();
							result = getPrettyMapper().writeValueAsString(model);
						}
					}
				}
			} catch (Exception e) {
				result = "ERROR";
			}
		}
		return result;
	}

	public ObjectMapper getPrettyMapper() {
		if (prettyMapper == null) {
			prettyMapper = new ObjectMapper();
			prettyMapper.enable(SerializationFeature.INDENT_OUTPUT);
			prettyMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		}
		return prettyMapper;
	}

	public Map<String, TestScenario> getScenarios() {
		if (scenarios == null) {
			scenarios = new TreeMap<String, TestScenario>();
			for (String jarName : getJobManager().getServices(0).keySet()) {
				Map<String, ServiceEndpoint> services = getJobManager().getServices(0).get(jarName);
				for (String serviceName : services.keySet()) {
					ServiceEndpoint service = services.get(serviceName);
					try {
						Configuration d = (Configuration) service.getService().requestClass().getConstructors()[0]
								.newInstance();
						TestScenario t = new TestScenario(serviceName, service, d);
						scenarios.put(t.getName(), t);
					} catch (Exception e) {
						e.printStackTrace();
					}
					toString();
				}
			}
			TestScenario t;
			SocialNetworkAnalysisConfiguration da;
			da = new SocialNetworkAnalysisConfiguration();
			da.getMethod().setAlgorithm(SocialNetworkAnalysisJob.handoverofwork);
			t = new TestScenario("AnalysisSocialNetworkJob-HandOverWork",
					scenarios.get("AnalysisSocialNetworkJob").getServiceUri(), SocialNetworkAnalysisConfiguration.class,
					SocialNetworkAnalysis.class, (Configuration) da);
			scenarios.put(t.getName(), t);
			da = new SocialNetworkAnalysisConfiguration();
			da.getMethod().setAlgorithm(SocialNetworkAnalysisJob.reassignment);
			t = new TestScenario("AnalysisSocialNetworkJob-Reassignment",
					scenarios.get("AnalysisSocialNetworkJob").getServiceUri(), SocialNetworkAnalysisConfiguration.class,
					SocialNetworkAnalysis.class, (Configuration) da);
			scenarios.put(t.getName(), t);
			da = new SocialNetworkAnalysisConfiguration();
			da.getMethod().setAlgorithm(SocialNetworkAnalysisJob.similartask);
			t = new TestScenario("AnalysisSocialNetworkJob-SimilarTask",
					scenarios.get("AnalysisSocialNetworkJob").getServiceUri(), SocialNetworkAnalysisConfiguration.class,
					SocialNetworkAnalysis.class, (Configuration) da);
			scenarios.put(t.getName(), t);
			da = new SocialNetworkAnalysisConfiguration();
			da.getMethod().setAlgorithm(SocialNetworkAnalysisJob.subconstracting);
			t = new TestScenario("AnalysisSocialNetworkJob-Subcontracting",
					scenarios.get("AnalysisSocialNetworkJob").getServiceUri(), SocialNetworkAnalysisConfiguration.class,
					SocialNetworkAnalysis.class, (Configuration) da);
			scenarios.put(t.getName(), t);
			da = new SocialNetworkAnalysisConfiguration();
			da.getMethod().setAlgorithm(SocialNetworkAnalysisJob.workingtogether);
			t = new TestScenario("AnalysisSocialNetworkJob-WorkingTogether",
					scenarios.get("AnalysisSocialNetworkJob").getServiceUri(), SocialNetworkAnalysisConfiguration.class,
					SocialNetworkAnalysis.class, (Configuration) da);
			scenarios.put(t.getName(), t);
		}
		return scenarios;
	}

	class TestScenario {
		protected final String name;
		protected final String serviceUri;
		protected final Configuration defaultConfig;
		protected final Class<? extends Configuration> configClass;
		protected final Class<? extends ServiceResponse> resultClass;

		public TestScenario(String n, ServiceEndpoint e, Configuration d) throws Exception {
			name = n;
			try {
				serviceUri = e.getPack().name() + "/" + e.getService().name();
				defaultConfig = d;
				configClass = ((Configuration) e.getService().requestClass().getConstructors()[0].newInstance())
						.getClass();
				resultClass = ((ServiceResponse) e.getService().responseClass().getConstructors()[0].newInstance())
						.getClass();
			} catch (Exception ex) {
				System.err.println(name);
				throw ex;
			}
		}

		public TestScenario(String n, String u, Class<? extends Configuration> c, Class<? extends Result> r,
				Configuration d) {
			name = n;
			serviceUri = u;
			defaultConfig = d;
			configClass = c;
			resultClass = r;
		}

		public String getName() {
			return name;
		}

		public String getServiceUri() {
			return serviceUri;
		}

		public Configuration getDefaultConfig() {
			return defaultConfig;
		}

		public Class<? extends Configuration> getConfigClass() {
			return configClass;
		}

		public Class<? extends ServiceResponse> getResultClass() {
			return resultClass;
		}
	}
}
