/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.models.Response;
import kr.ac.pusan.bsclab.bab.v2.web.services.hdfs.IFileManager;
import kr.ac.pusan.bsclab.bab.v2.web.services.hdfs.LocalFileManager;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.ImportJobResult;

@Controller
public class StreamingController extends AbstractWebController {

	public static final String BASE_URL = AServiceController.BASE_URL + "/legacy/streaming";
	
	protected IFileManager localFileManager = new LocalFileManager();
	
	@Autowired
	protected HdfsConfiguration hconfig;
	@Autowired
	protected SparkConfiguration sconfig;
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.POST }, path = BASE_URL + "/create/{workspaceId}/{datasetId}")
	public @ResponseBody Response<ImportJobResult> getCreate(
			@PathVariable(value = "workspaceId") String workspaceId, 
			@PathVariable(value = "datasetId") String datasetId,
			@RequestBody ImportJobResult project,
			ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String uri = getHdfsConfig().getBaseDir() + "/workspaces/" + workspaceId + "/" + datasetId;
			String rootPath = System.getProperty("catalina.home");
			File uploadDir = new File(rootPath + File.separator + "uploads");
			if (!uploadDir.exists())
				uploadDir.mkdirs();
			if (!getFileManager().isExists(uri + ".mrepo")) {
				project.setRepositoryURI("/workspaces/" + workspaceId + "/" + datasetId);
				project.setRawPath("stream");
				project.setOriginalFormat("stream");
				project.toString();
				
				String suri = uploadDir.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".mrepo";
				localFileManager.saveAsTextFile(suri, getMapper().writeValueAsString(project));
				getFileManager().upload(uri + ".mrepo", suri);
				
				suri = uploadDir.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".irepo";
				getFileManager().saveAsTextFile(uri + ".irepo/part-0000", "");
				return new Response<ImportJobResult>(null, project);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Map<String, List<Map<String, Object>>> buffer = new LinkedHashMap<String, List<Map<String, Object>>>();
	private int BUFFER_MAX_SIZE = 64 * 1024 * 1024;
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.POST }, path = BASE_URL + "/messages/{workspaceId}/{datasetId}")
	public @ResponseBody Response<List<Map<String, Object>>> postMessages(
			@PathVariable(value = "workspaceId") String workspaceId, 
			@PathVariable(value = "datasetId") String datasetId, 
			@RequestBody List<Map<String, Object>> data,
			ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		return postMessages(workspaceId, datasetId, 0, data, modelMap, request, response);
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.POST }, path = BASE_URL + "/messages/{workspaceId}/{datasetId}/{flush}")
	public @ResponseBody Response<List<Map<String, Object>>> postMessages(
			@PathVariable(value = "workspaceId") String workspaceId, 
			@PathVariable(value = "datasetId") String datasetId, 
			@PathVariable(value = "flush") int flush, 
			@RequestBody List<Map<String, Object>> data,
			ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String uri = getHdfsConfig().getBaseDir() + "/workspaces/" + workspaceId + "/" + datasetId;
			if (getFileManager().isExists(uri + ".mrepo")) {
				String json = getFileManager().loadFromTextFile(uri + ".mrepo");
				ImportJobResult project = getMapper().readValue(json, ImportJobResult.class);
				if (!buffer.containsKey(uri)) {
					buffer.put(uri, new ArrayList<Map<String, Object>>());
				}
				if (project.getOriginalFormat().equalsIgnoreCase("stream")) {
					if (data.size() > 0) {
						for (Map<String, Object> datum : data) {
							buffer.get(uri).add(datum);
						}
					}
					if (data.size() > BUFFER_MAX_SIZE || flush == 1) {
						getFlush(workspaceId, datasetId, modelMap, request, response);
					}
				}
			}
			
			return new Response<List<Map<String, Object>>>(null, data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/flush/{workspaceId}/{datasetId}")
	public @ResponseBody Response<Boolean> getFlush(
			@PathVariable(value = "workspaceId") String workspaceId, 
			@PathVariable(value = "datasetId") String datasetId, 
			ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String uri = getHdfsConfig().getBaseDir() + "/workspaces/" + workspaceId + "/" + datasetId;
			if (getFileManager().isExists(uri + ".mrepo")) {
				String json = getFileManager().loadFromTextFile(uri + ".mrepo");
				ImportJobResult project = getMapper().readValue(json, ImportJobResult.class);
				if (!buffer.containsKey(uri)) {
					buffer.put(uri, new ArrayList<Map<String, Object>>());
				}
				if (project.getOriginalFormat().equalsIgnoreCase("stream")) {
					StringBuilder jsons = new StringBuilder();
					for (Map<String, Object> datum : buffer.get(uri)) {
						jsons.append(getMapper().writeValueAsString(datum)).append("\n");
					}
					String puri = getHdfsConfig().getBaseDir() + project.getRepositoryURI();
					getFileManager().saveAsTextFile(puri + ".irepo/part-" + System.currentTimeMillis(), jsons.toString());
					
					buffer.get(uri).clear();
				}
			}
			return new Response<Boolean>(null, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
