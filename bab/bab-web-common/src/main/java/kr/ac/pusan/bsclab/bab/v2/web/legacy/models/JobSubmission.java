/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.legacy.models;

import java.sql.Timestamp;

import kr.ac.pusan.bsclab.bab.ws.api.JobConfiguration;

public class JobSubmission extends JobConfiguration {

	protected String forceKillUrl;

	public JobSubmission() {

	}

	public JobSubmission(String home, String jobId, String jobClass, String jobExtension, String workspaceId,
			String datasetId, String repositoryId, String configuration, Timestamp sdt, Timestamp edt) {
		setJobId(jobClass + "+" + jobId);
		setJobClass(jobClass);
		setWorkspaceId(workspaceId);
		setDatasetId(datasetId);
		setRepositoryId(repositoryId);
		setPath(home + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + jobId);
		setExt(jobExtension);
		setJobPath(home + "/jobs/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + jobId + "."
				+ jobExtension);
		setResourcePath(home + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/basecsv");
		setResultDir(home + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId);
		setResultPath(home + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + jobId + "."
				+ jobExtension);
		setConfiguration(configuration);
		setSdt(sdt);
		setEdt(edt);

	}

	public JobSubmission(String home, String jobId, String jobClass, String jobExtension, String workspaceId,
			String datasetId, String repositoryId, String configuration) {
		setJobId(jobClass + "+" + jobId);
		setJobClass(jobClass);
		setWorkspaceId(workspaceId);
		setDatasetId(datasetId);
		setRepositoryId(repositoryId);
		setPath(home + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + jobId);
		setExt(jobExtension);
		setJobPath(home + "/jobs/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + jobId + "."
				+ jobExtension);
		setResourcePath(home + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/basecsv");
		setResultDir(home + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId);
		setResultPath(home + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + jobId + "."
				+ jobExtension);
		setConfiguration(configuration);
	}

	public String getForceKillUrl() {
		return forceKillUrl;
	}

	public void setForceKillUrl(String forceKillUrl) {
		this.forceKillUrl = forceKillUrl;
	}
}
