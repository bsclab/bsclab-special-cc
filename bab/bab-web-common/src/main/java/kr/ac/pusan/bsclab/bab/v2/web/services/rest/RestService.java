/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.services.rest;

import java.nio.charset.Charset;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.web.BabWeb;

@Service
public class RestService {

	protected ObjectMapper mapper = new ObjectMapper();

	@Async
	public <T> void exchangeAsync(String uri, HttpMethod httpMethod, HttpEntity<?> httpEntity, Class<T> responseType,
			RestServiceCallback<T> callback) {
		BabWeb.log(this).info("method={}, uri={}, data={}", httpMethod.name(), uri, httpEntity.getBody());
		try {
			RestTemplate rt = new RestTemplate();
			rt.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
			ResponseEntity<T> result = rt.exchange(uri, httpMethod, httpEntity, responseType);
			callback.done(result);
		} catch (Exception e) {
			BabWeb.log(this).info("RestClientException : " + e.getMessage());
		}
	}

	public <T> ResponseEntity<T> exchange(String uri, HttpMethod httpMethod, HttpEntity<?> httpEntity,
			Class<T> responseType) {
		BabWeb.log(this).info("method={}, uri={}, data={}", httpMethod.name(), uri, httpEntity.getBody());
		try {
			RestTemplate rt = new RestTemplate();
			rt.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
			ResponseEntity<T> result = rt.exchange(uri, httpMethod, httpEntity, responseType);
			return result;
		} catch (Exception e) {
			BabWeb.log(this).info("RestClientException : " + e.getMessage());
		}
		return null;
	}
}
