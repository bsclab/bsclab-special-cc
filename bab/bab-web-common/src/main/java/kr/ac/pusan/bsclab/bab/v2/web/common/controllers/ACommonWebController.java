/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.common.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import kr.ac.pusan.bsclab.bab.v2.web.controllers.AWebController;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers.HdfsConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.rest.RestService;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkConfiguration;

@Controller
public abstract class ACommonWebController extends AWebController {

	public static final String BASE_URL = AWebController.BASE_URL;

	@Autowired
	protected RestService restService;

	@Autowired
	protected HdfsConfiguration hdfsConfig;
	
	@Autowired
	protected SparkConfiguration sparkConfig;

	public RestService getRestService() {
		return restService;
	}

	public HdfsConfiguration getHdfsConfig() {
		return hdfsConfig;
	}

	public SparkConfiguration getSparkConfig() {
		return sparkConfig;
	}
	
	// public <K, T> Response<T> callBabService(String uri, K config, Class<T>
	// responseClass) {
	// try {
	// ObjectMapper mapper = new ObjectMapper();
	// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
	// false);
	// RestTemplate restTemplate = new RestTemplate();
	// HttpHeaders headers = new HttpHeaders();
	// MultiValueMap<String, Object> request = new LinkedMultiValueMap<String,
	// Object>();
	// request.add("config", mapper.writeValueAsString(config));
	// headers.setContentType(MediaType.MULTIPART_FORM_DATA);
	// HttpEntity<MultiValueMap<String, Object>> entity
	// = new HttpEntity<MultiValueMap<String, Object>>(request, headers);
	// ResponseEntity<String> json = restTemplate.exchange(uri, HttpMethod.POST,
	// entity, String.class);
	// JsonResponse immResult = mapper.readValue(json.getBody(),
	// JsonResponse.class);
	// String responseJson = mapper.writeValueAsString(immResult.getResponse());
	// T response = mapper.readValue(responseJson, responseClass);
	// Response<T> result = new Response<T>(immResult.getRequest(), response);
	// result.setId(immResult.getId());
	// result.setStatus(immResult.getStatus());
	// return result;
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return null;
	// }

}
