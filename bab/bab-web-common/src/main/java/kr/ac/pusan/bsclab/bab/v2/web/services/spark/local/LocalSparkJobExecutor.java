/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.services.spark.local;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.web.services.Job;
import kr.ac.pusan.bsclab.bab.v2.web.services.rest.RestService;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkJob;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkSubmission;
import kr.ac.pusan.bsclab.bab.v2.web.services.IJobExecutor;

@Service
@ConditionalOnProperty(name = "bab.serviceJobExecutor", havingValue = "local-spark")
public class LocalSparkJobExecutor implements IJobExecutor {

	@Autowired
	SparkConfiguration sparkConfig;

	@Autowired
	RestService restService;

	@Override
	public Job run(Job j) {
		try {
			SparkJob job = (SparkJob) j;
			job.setStarted(new Date().getTime());
			job.setStatus(SparkJob.STATUS_SUBMITTED);
			job.setRemarks("");
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString((SparkJob) job);
			String uri = sparkConfig.getRestUrl() + "/v1/submissions/create";
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			HttpEntity<String> entity = new HttpEntity<String>(json, headers);
			//System.out.println("SPARK Job Request: " + uri);
			//System.out.println("SPARK Job Request JSON: " + json);
			ResponseEntity<SparkSubmission> status = restService.exchange(uri, HttpMethod.POST, entity,
					SparkSubmission.class);
			if (status != null) {
				job.setSparkSubmission(status.getBody());
				return status(job);
			} else {
				job.setStatus(SparkJob.STATUS_RUNNING);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Job status(Job j) {
		try {
			SparkJob job = (SparkJob) j;
			String uri = sparkConfig.getRestUrl() + "/v1/submissions/status/"
					+ job.getSparkSubmission().getSubmissionId();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<SparkSubmission> status = restService.exchange(uri, HttpMethod.GET, entity,
					SparkSubmission.class);
			if (status != null) {
				job.setSparkSubmission(status.getBody());
				job.setStatus(job.getSparkSubmission().getDriverState().trim());
			}
			return job;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Job kill(Job j) {
		SparkJob job = (SparkJob) status(j);
		if (job.isRunning() && job.getSparkSubmission() != null) {
			if (!job.getSparkSubmission().getDriverState().equalsIgnoreCase(Job.STATUS_COMPLETED)
					|| !job.getSparkSubmission().getDriverState().equalsIgnoreCase(Job.STATUS_FINISHED)) {
				try {
					String uri = sparkConfig.getRestUrl() + "/v1/submissions/kill/"
							+ job.getSparkSubmission().getSubmissionId();
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
					HttpEntity<String> entity = new HttpEntity<String>("", headers);
					ResponseEntity<SparkSubmission> status = restService.exchange(uri, HttpMethod.POST, entity,
							SparkSubmission.class);
					if (status != null) {
						job.setSparkSubmission(status.getBody());
						job = (SparkJob) status(job);
						job.setStatus(Job.STATUS_KILLED);
						return job;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return job;
	}

	@Override
	public boolean isBusy() {
		return false;
	}

}
