/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.services.hdfs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.fs.FileStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.web.services.rest.RestService;

@Service
@ConditionalOnProperty(name = "bab.serviceFileManager", havingValue = "remote-hdfs")
public class RemoteHdfsFileManager implements IFileManager {

	@Autowired
	RestService restService;

	@Override
	public boolean isExists(String hdfsPath) {
		try {
			String uri = hdfsPath + "?user.name=default&op=GETFILESTATUS";
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restService.exchange(uri, HttpMethod.GET, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK && result.getBody().indexOf("FileStatus") > -1) {
				return true;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	@Override
	public void saveAsTextFile(String hdfsPath, String data) {
		try {
			String uri = hdfsPath + "?user.name=default&op=CREATE";
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restService.exchange(uri, HttpMethod.PUT, entity, String.class);
			String saveUri = result.getHeaders().get("Location").get(0);
			HttpEntity<String> entityFile = new HttpEntity<String>(data, headers);
			result = restService.exchange(saveUri, HttpMethod.PUT, entityFile, String.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public String loadFromTextFile(String hdfsPath) {
		try {
			String uri = hdfsPath + "?user.name=default&op=OPEN";
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restService.exchange(uri, HttpMethod.GET, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK) {
				return result.getBody();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public void upload(String hdfsPath, String localUri) {
		try {
			File file = new File(localUri);
			if (file.exists()) {
				String uri = hdfsPath + "?user.name=default&op=CREATE";
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
				HttpEntity<String> entity = new HttpEntity<String>("", headers);
				ResponseEntity<String> result = restService.exchange(uri, HttpMethod.PUT, entity, String.class);
				String saveUri = result.getHeaders().get("Location").get(0);
				HttpEntity<Resource> entityFile = new HttpEntity<Resource>(new FileSystemResource(file.getAbsolutePath()), headers);
				result = restService.exchange(saveUri, HttpMethod.PUT, entityFile, String.class);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public String download(String hdfsPath) {
		if (isExists(hdfsPath)) {
			try {
				File temp = File.createTempFile("bab-download-" + System.currentTimeMillis(), ".tmp");
				FileWriter fw = new FileWriter(temp.getAbsolutePath());
				BufferedWriter out = new BufferedWriter(fw);
				out.write(loadFromTextFile(hdfsPath));
				out.close();
				return temp.getAbsolutePath();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}

	@Override
	public List<String[]> listDirectory(String hdfsPath) {
		List<String[]> contents = new ArrayList<String[]>();
		try {
			String uri = hdfsPath + "?user.name=default&op=LISTSTATUS";
			HttpHeaders headers = new HttpHeaders();
			HttpEntity<String> entity = new HttpEntity<String>("", headers);
			ResponseEntity<String> result = restService.exchange(uri, HttpMethod.GET, entity, String.class);
			if (result.getStatusCode() == HttpStatus.OK) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				HdfsListStatuses hdfsListStatuses = mapper.readValue(result.getBody(), HdfsListStatuses.class);
				List<FileStatus> fileStatuses = hdfsListStatuses.FileStatuses.toFileStatus();
				for (FileStatus f : fileStatuses) {
					contents.add(new String[] { f.getPath().toUri().getPath(), f.getLen() + "", f.isFile() ? TYPE_FILE : TYPE_DIR, f.getModificationTime() + "", f.getPath().toUri().getPath()});
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return contents;
	}
}
