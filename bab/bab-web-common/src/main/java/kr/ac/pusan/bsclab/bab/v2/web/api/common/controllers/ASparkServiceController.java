/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.api.common.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.api.controllers.JobController;
import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers.HdfsConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.Job;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkResponse;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkJob;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.JobConfiguration;

public abstract class ASparkServiceController extends AServiceController {

	@Autowired
	protected SparkConfiguration sparkConfig;

	@Autowired
	protected HdfsConfiguration hdfsConfig;

	public SparkConfiguration getSparkConfig() {
		return sparkConfig;
	}

	public HdfsConfiguration getHdfsConfig() {
		return hdfsConfig;
	}

	public IServiceResponse runService(ServiceEndpoint service, String workspaceId, String datasetId,
			String repositoryId, IServiceRequest request) {
		if (request instanceof Configuration) {
			Configuration reqConfig = (Configuration) request;
			reqConfig.setWorkspaceId(workspaceId);
			reqConfig.setDatasetId(datasetId);
			if (repositoryId.length() > 0) {
				reqConfig.setRepositoryId(repositoryId);
				if (reqConfig.getRepositoryURI() == null || reqConfig.getRepositoryURI().trim().length() < 1) {
					reqConfig.setRepositoryURI("/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId);
				}
			} else {
				if (reqConfig.getRepositoryURI() == null || reqConfig.getRepositoryURI().trim().length() < 1) {
					reqConfig.setRepositoryURI("/workspaces/" + workspaceId + "/" + datasetId);
				}
			}
		}
		SparkJob job = (SparkJob) getJobManager().getJobFactory().create(service, request);
		getJobManager().register(job);
		job.getAppArgs().add(0, getConfig().getUrl() + JobController.BASE_URL + "/report/" + job.getId());
		String configFileName = "";
		if (repositoryId.trim().length() > 0 ) {
			configFileName = getHdfsConfig().getRestUrl() + "/jobs/" + workspaceId + "/" + datasetId + "/"
					+ repositoryId + "/" + job.getHash() + service.getService().legacyJobExtension();
		} else {
			configFileName = getHdfsConfig().getRestUrl() + "/jobs/" + workspaceId + "/" + datasetId + "/"
					+ job.getHash() + service.getService().legacyJobExtension();
		}
		//System.out.println("CONFIG: " + configFileName);
		JobConfiguration config = null;
		if (!getFileManager().isExists(configFileName)) {
			config = new JobConfiguration();
			config.setJobId(service.toString() + "+" + job.getId());
			config.setJobClass(service.getService().name());
			config.setWorkspaceId(workspaceId);
			config.setDatasetId(datasetId);
			if (repositoryId.trim().length() > 0 ) {
				config.setRepositoryId(repositoryId);
				config.setPath("/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + job.getHash());
				config.setExt(service.getService().legacyJobExtension());
				config.setJobPath("/jobs/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + job.getHash()
						+ service.getService().legacyJobExtension());
			} else {
				config.setPath("/workspaces/" + workspaceId + "/" + datasetId + "/" + job.getHash());
				config.setExt(service.getService().legacyJobExtension());
				config.setJobPath("/jobs/" + workspaceId + "/" + datasetId + "/" + job.getHash()
						+ service.getService().legacyJobExtension());
			}
			config.setResultPath(config.getPath() + service.getService().legacyJobExtension());
			String configJson = "";
			try {
				configJson = getMapper().writeValueAsString(request);
			} catch (Exception e) {
				e.printStackTrace();
			}
			config.setConfiguration(configJson);
			String json = "";
			try {
				json = getMapper().writeValueAsString(config);
			} catch (Exception e) {
				e.printStackTrace();
			}
			getFileManager().saveAsTextFile(configFileName, json);
		}
		String configJson = getFileManager().loadFromTextFile(configFileName);
		try {
			config = getMapper().readValue(configJson, JobConfiguration.class);
		} catch (Exception e) {
			e.printStackTrace();
			config = null;
		}
		if (config != null) {
			if (repositoryId.length() > 0) {
				request.setUri("/jobs/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + job.getHash()
				+ service.getService().legacyJobExtension());
			} else {
				request.setUri("/jobs/" + workspaceId + "/" + datasetId + "/" + job.getHash() + service.getService().legacyJobExtension());
			}
			job.getAppArgs().add(request.getUri());
			job.getAppArgs().add(getConfig().getUrl());
			job.getAppArgs().add(hdfsConfig.getMode());
			String resultFileName = getHdfsConfig().getRestUrl() + config.getResultPath();
			Class<? extends IServiceResponse> responseClass = service.getService().responseClass();
			if (getFileManager().isExists(resultFileName)) {
				String resultJson = getFileManager().loadFromTextFile(resultFileName);
				try {
					IServiceResponse response = getMapper().readValue(resultJson, responseClass);
					job.setStatus(Job.STATUS_FINISHED);
					return new SparkResponse<IServiceResponse>(job, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			getJobManager().submit(job);
			return new SparkResponse<IServiceResponse>(job);
		}
		return null;
	}

}
