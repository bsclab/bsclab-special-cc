/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import kr.ac.pusan.bsclab.bab.v2.web.legacy.models.Response;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.config.DottedChartAnalysisJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models.*;

@Controller
public class PerformanceVisualizerController extends AbstractWebController {

	private static final Logger logger = LoggerFactory.getLogger(PerformanceVisualizerController.class);

	public static final String BASE_URL = AbstractWebController.BASE_URL + "/performancevisualizer";

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
	public ModelAndView getIndex() {
		ModelAndView view = new ModelAndView("bab/index");

		String message = this.getClass().getName();
		view.addObject("message", message);

		return view;
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/delta/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getDeltaAnlysis(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId, @PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, HttpServletRequest request, HttpSession session) {

		ModelAndView view = new ModelAndView("performancevisualizer/delta");

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
		jsonData.put("datasetId", datasetId);

		// view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());

		view.addObject("jsonData", jsonData);
		return view;
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/dotted/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getDottedChart(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId, @PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, HttpServletRequest request, HttpSession session)
			throws JsonParseException, JsonMappingException, IOException, InterruptedException {

		ModelAndView view = new ModelAndView("performancevisualizer/dotted");

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);

		// String uri = APIURI.get("analysis").get("dottedchart") + workspaceId
		// + "/" + datasetId + "/" + repositoryId;
		// Response<DottedChartModel> model = callBabService(uri, config,
		// DottedChartModel.class);

		String othURL = BASE_URL + "/" + "dotted" + "/" + workspaceId + "/" + datasetId + "/" + sdt + "/" + edt;
		String apiURI = apiManager.getAPIURI().get("analysis").get("dottedchart") + workspaceId + "/" + datasetId + "/"
				+ sdt + "/" + edt;
		logger.debug("othURL={}", othURL);
		logger.debug("apiURI={}", apiURI);

		// Opened 2018.05.16 Taufik NA
		DottedChartAnalysisJobConfiguration config = new DottedChartAnalysisJobConfiguration();
		Response<DottedChartModel> model = callBabService(apiURI, config, DottedChartModel.class);

		// while (true) {
		// String jobId = model.getRequest().getJobId();
		// if (jobManager.getManagedJobQueues().containsKey(jobId)) {
		// JobQueue job = jobManager.getManagedJobQueues().get(jobId);
		// if (job.getJobQueueStatus().equals(JobQueueStatus.COMPLETED))
		// break;
		// else {
		// Thread.sleep(3000);
		// continue;
		// }
		// } else
		// return view;
		// }

		// Collection<Long> valTimes = new HashSet<Long>();
		// Long[] timesArray = new Long[0];
		//
		// Collection<String> valLegend = new HashSet<String>();
		// String[] legendArray = new String[0];
		//
		Collection<String> valColors = new HashSet<String>();
		String[] colorsArray = new String[0];

		// Opened 2018.05.16 Taufik NA
		// Map<String, Map<Long, Data>> data = new HashMap<String, Map<Long,
		// Data>>();

		// Opened 2018.05.16 Taufik NA
		if (model != null) {
			DottedChartModel response = model.getResponse();
			if (response != null) {
				//
				// valTimes = response.getTimes().keySet();
				// timesArray = valTimes.toArray(new Long[valTimes.size()]);
				//
				// valLegend = response.getLegend().keySet();
				// legendArray = valLegend.toArray(new
				// String[valLegend.size()]);
				//
				valColors = response.getColors().keySet();
				colorsArray = valColors.toArray(new String[valColors.size()]);
				//
				// data = response.getData();
			}
		}

		/*
		 * 1. Preparing the color pallet code and form the css style code for
		 * the view 2. Storing each css style class name into array
		 */
		String[] masterColorArray = new String[] { "#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9",
				"#6C9", "#9C9", "#CC9", "#FC9", "#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC",
				"#9CC", "#CCC", "#FCC", "#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF",
				"#CCF", "#FCF", "#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0",
				"#FF0", "#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
				"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6", "#069",
				"#369", "#669", "#969", "#C69", "#F69", "#0F9", "#3F9", "#6F9", "#9F9", "#CF9", "#FF9", "#06C", "#36C",
				"#66C", "#96C", "#C6C", "#F6C", "#0FC", "#3FC", "#6FC", "#9FC", "#CFC", "#FFC", "#06F", "#36F", "#66F",
				"#96F", "#C6F", "#F6F", "#0FF", "#3FF", "#6FF", "#9FF", "#CFF", "#FFF", "#000", "#300", "#600", "#900",
				"#C00", "#F00", "#090", "#390", "#690", "#990", "#C90", "#F90", "#003", "#303", "#603", "#903", "#C03",
				"#F03", "#093", "#393", "#693", "#993", "#C93", "#F93", "#006", "#306", "#606", "#906", "#C06", "#F06",
				"#096", "#396", "#696", "#996", "#C96", "#F96", "#009", "#309", "#609", "#909", "#C09", "#F09", "#099",
				"#399", "#699", "#999", "#C99", "#F99", "#00C", "#30C", "#60C", "#90C", "#C0C", "#F0C", "#09C", "#39C",
				"#69C", "#99C", "#C9C", "#F9C", "#00F", "#30F", "#60F", "#90F", "#C0F", "#F0F", "#09F", "#39F", "#69F",
				"#99F", "#C9F", "#F9F", "#030", "#330", "#630", "#930", "#C30", "#F30", "#0C0", "#3C0", "#6C0", "#9C0",
				"#CC0", "#FC0", "#033", "#333", "#633", "#933", "#C33", "#F33", "#0C3", "#3C3", "#6C3", "#9C3", "#CC3",
				"#FC3", "#036", "#336", "#636", "#936", "#C36", "#F36", "#0C6", "#3C6", "#6C6", "#9C6", "#CC6",
				"#FC6" };

		String cssClassName = "dotc";
		String css = "";
		String singleEventColors = "";
		String cssEventColorsName = "cec";
		String cssEventColors = "";
		String colorSelectOption = "";

		int colorIndex = 0;

		for (String color : masterColorArray) {
			// Creating CSS for coloring the dots
			css += "." + cssClassName + colorIndex + " {";
			css += "display: inline-block;";
			css += "-webkit-appearance:button;";
			css += "fill: " + color + ";";
			css += "stroke: " + color + ";";
			css += "background-color: " + color + ";";
			css += "border: solid 1px " + color + ";";
			css += "color: " + color + ";";
			css += "}\n";

			// Creating Single Event Colors CSS and Legend
			cssEventColors += "." + cssEventColorsName + colorIndex + "{";
			cssEventColors += "display: inline-block;";
			cssEventColors += "width: 14px;";
			cssEventColors += "margin: 2px;";
			cssEventColors += "height: 14px;";
			cssEventColors += "border-radius: 6px;";
			cssEventColors += "fill: " + color + ";";
			cssEventColors += "stroke: " + color + ";";
			cssEventColors += "background-color: " + color + ";";
			cssEventColors += "border: solid 1px " + color + ";";
			cssEventColors += "}\n";

			if (colorsArray.length > colorIndex) {
				// Creating color options dropdown
				// Inspired from http://codepen.io/kaorun343/pen/wGXBBg
				// The dotted data-color to be filter is store in NAME attribute
				// <-- important

				singleEventColors += "<select id='sec_" + colorIndex + "' name='" + colorsArray[colorIndex]
						+ "' v-on:change='changeDotColor' class='" + cssEventColorsName + colorIndex + "'>\n";
				String optionSelected;
				int j = 0;
				for (String colorOptions : masterColorArray) {
					optionSelected = (color.equals(colorOptions) == true) ? "selected" : "";
					singleEventColors += "<option value='" + cssClassName + j + "' class='" + cssClassName + j + "' "
							+ optionSelected + ">" + colorOptions + "</option>\n";
					j++;
				}
				singleEventColors += "</select>" + colorsArray[colorIndex] + "<br/>";
			}
			colorIndex++;
		}

		css += cssEventColors;

		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		// view.addObject("apiURI", apiURI);
		view.addObject("apiURI", apiManager.getAPIURI());
		view.addObject("othURL", othURL);
		view.addObject("css", css);
		view.addObject("colorIndex", colorIndex);
		view.addObject("colorIndex", masterColorArray.length);
		view.addObject("singleEventColors", singleEventColors);
		view.addObject("colorSelectOption", colorSelectOption);
		view.addObject("jsonData", jsonData);

		return view;
	}

	/*
	 * Method for generating DEFAULT, RELATIVE and TRANSPOSE dotted chart
	 */
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL
			+ "/dotted/{workspaceId}/{datasetId}/{sdt}/{edt}/{dottedMode}/{colorIndex}")
	public ModelAndView getDottedChart(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId, @PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, @PathVariable(value = "dottedMode") String dottedMode,
			@PathVariable(value = "colorIndex") int colorIndex, HttpServletRequest request, HttpSession session)
			throws JsonParseException, JsonMappingException, IOException, InterruptedException {

		ModelAndView view = new ModelAndView("performancevisualizer/dotted2");

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);

		DottedChartAnalysisJobConfiguration config = new DottedChartAnalysisJobConfiguration();
		String apiURI = apiManager.getAPIURI().get("analysis").get("dottedchart") + workspaceId + "/" + datasetId + "/"
				+ sdt + "/" + edt;
		Response<DottedChartModel> model = callBabService(apiURI, config, DottedChartModel.class);

		// TODO:
		// while (true) {
		// String jobId = model.getRequest().getJobId();
		// if (jobManager.getManagedJobQueues().containsKey(jobId)) {
		// JobQueue job = jobManager.getManagedJobQueues().get(jobId);
		// if (job.getJobQueueStatus().equals(JobQueueStatus.COMPLETED))
		// break;
		// else {
		// Thread.sleep(3000);
		// continue;
		// }
		// } else
		// return view;
		// }
		model = callBabService(apiURI, config, DottedChartModel.class);
		if (model.getResponse() != null) {
			Collection<Long> valTimes = model.getResponse().getTimes().keySet();
			Long[] timesArray = valTimes.toArray(new Long[valTimes.size()]);

			/*
			 * Collection<String> valLegend =
			 * model.getResponse().getLegend().keySet(); String[] legendArray =
			 * valLegend.toArray(new String[valLegend.size()]);
			 * 
			 * Collection<String> valColors =
			 * model.getResponse().getColors().keySet(); String[] colorsArray =
			 * valColors.toArray(new String[valColors.size()]);
			 */

			Map<String, Map<Long, Data>> data = model.getResponse().getData();
			// long noOfYAxisData = data.size();

			String cssClassName = "dotc";

			/*
			 * - Initialize the chart properties - Initialize chart Top X Label
			 * - Initialize chart Left Y Label - Initialize X and Y chart line -
			 * Print out the dotted to the chart
			 */

			int chartWidth = 815; // default 900
			int chartHeight = 600;
			int colWidth = chartWidth / valTimes.size();
			int rowHeight = 35;
			double halfColWidth = colWidth / 2;
			double halfRowheight = rowHeight / 2;
			int viewBoxWidth = chartWidth;
			int viewBoxHeight = chartHeight;
			int chartTopMargin = 0; // default 40
			int chartLeftMargin = 0; // default 85
			long columnDensity = timesArray[1] - timesArray[0];
			// long halfColumnDensity = columnDensity/2;
			long columnRatio = colWidth / columnDensity;
			// double rowRatio = rowHeight/noOfYAxisData;
			double rowRatio;
			double startingYPos = 2 * chartTopMargin;

			String svg = "";
			String svgTopLabel = "";
			String svgLeftLabel = "";
			String topLabelLogical = "";
			String topLabelNonLogical = "";
			String leftLabel = "";
			String leftLabelNonLogical = "";
			String columnLine = "";
			String svgRect = "";
			String svgCircle = "";
			String rowClass = "";
			String colClass = "";
			String dateFormat1 = "yyyy-MM-dd HH:mm:ss";

			// long latestTime = timesArray[valTimes.size()-1];

			svgTopLabel += "<svg id='dottedTopLabel' width='" + chartWidth
					+ "' height='40' preserveAspectRatio='none' viewBox='0 0 " + viewBoxWidth + " 40'>";
			svgLeftLabel += "<svg id='dottedLeftLabel' width='85' height='" + chartHeight
					+ "' preserveAspectRatio='none' viewBox='0 0 85 " + viewBoxHeight + "'>";
			svg += "<svg id='dotted' width='" + chartWidth + "' height='" + chartHeight
					+ "' preserveAspectRatio='none' viewBox='0 0 " + viewBoxWidth + " " + viewBoxHeight + "'>";

			/*
			 * Top Label Generation (Logical and Non Logical Time in Top X Axis)
			 * for DEFAULT and RELATIVE only Generate Column line border
			 */

			if (dottedMode.equals("Relative") || dottedMode.equals("Default")) {
				int colIndex = 0;
				int labelYAxis;
				int allY2 = data.size() * rowHeight;
				for (Long xTimes : timesArray) {

					Date dateXTimes = new Date(xTimes);
					SimpleDateFormat sdf = new SimpleDateFormat(dateFormat1);
					String strDate = sdf.format(dateXTimes);

					labelYAxis = ((colIndex % 2) == 0) ? 35 : 20;

					double centerColumn = (colIndex * colWidth) + halfColWidth;

					topLabelLogical += "<text x='" + (chartLeftMargin + centerColumn) + "' y='" + labelYAxis
							+ "' style='text-anchor:middle;font-size:11;'>";
					if (dottedMode.equals("Relative")) {
						topLabelLogical += colIndex;
					} else if (dottedMode.equals("Default")) {
						topLabelLogical += strDate;
					}
					topLabelLogical += "</text>\n";

					topLabelNonLogical += "<text x='" + (chartLeftMargin + centerColumn) + "' y='" + labelYAxis
							+ "' style='text-anchor:middle;font-size:9;'>" + strDate + "</text>\n";

					colClass = ((colIndex % 2) == 0) ? "id-col-even" : "id-col-odd";
					columnLine += "<line x1='" + (chartLeftMargin + centerColumn) + "' y1='" + labelYAxis + "' x2='"
							+ (chartLeftMargin + centerColumn) + "' y2='" + allY2 + "' class='" + colClass
							+ "'></line>";

					colIndex++;
				}
			} else if (dottedMode.equals("Transpose")) {
				int rowIndex = 0;
				double labelYAxis;
				for (Long xTimes : timesArray) {
					Date dateXTimes = new Date(xTimes);
					SimpleDateFormat sdf = new SimpleDateFormat(dateFormat1);
					String strDate = sdf.format(dateXTimes);

					labelYAxis = ((rowIndex + 1) * rowHeight) + startingYPos;
					leftLabel += "<text x='80' y='" + labelYAxis
							+ "' style='text-anchor:end;font-size:11;text-align:left;'>" + xTimes + "</text>\n";
					leftLabelNonLogical += "<text x='80' y='" + labelYAxis
							+ "' style='text-anchor:end;font-size:8;text-align:left;'>" + strDate + "</text>\n";

					rowIndex++;
				}
			}

			/*
			 * Conditional IF to select the right Dotted Chart to Render
			 * Relative, Transpose or Default
			 */

			if (dottedMode.equals("Relative")) { /* RELATIVE DOTTED CHART */
				int i = 0;
				for (String colYData : data.keySet()) {
					Map<Long, Data> subXData = data.get(colYData);
					leftLabel += "<text x='80' y='" + ((i * halfRowheight) + (halfRowheight + chartTopMargin))
							+ "' style='text-anchor:end;font-size:9;text-align:left;'>" + colYData + "</text>\n";

					rowClass = ((i % 2) == 0) ? "id-row-even" : "id-row-odd";
					svgRect += "<rect x='" + chartLeftMargin + "' y='" + ((i * rowHeight) + chartTopMargin)
							+ "' width='" + chartWidth + "' height='" + rowHeight + "' class='" + rowClass
							+ "'></rect>";

					int idxColorPallet = 1;
					long currColIndex = 0;
					for (Long colXData : subXData.keySet()) {
						Data d = subXData.get(colXData);

						Date dateXTimes = new Date(colXData);
						SimpleDateFormat sdf = new SimpleDateFormat(dateFormat1);
						String nonLogicalTime = sdf.format(dateXTimes);

						double cx = (currColIndex * colWidth) + halfColWidth;
						svgCircle += "<circle cx='" + (cx + chartLeftMargin) + "' cy='"
								+ ((i * halfRowheight) + (halfRowheight + chartTopMargin)) + "' r='4' class='"
								+ cssClassName + idxColorPallet + "' data-time-nl='" + nonLogicalTime + "' data-time='"
								+ colXData + "' data-component='" + colYData + "' data-color='" + d.getName()
								+ "' data-shape='" + d.getType() + "'></circle>";
						if (idxColorPallet > colorIndex) {
							idxColorPallet = 1;
						}

						currColIndex++;
						idxColorPallet++;
					}
					i++;
				}

			} else if (dottedMode
					.equals("Transpose")) { /* TRANSPOSE DOTTED CHART */
				rowRatio = rowHeight / columnDensity;

				int i = 0;
				for (String colYData : data.keySet()) {
					Map<Long, Data> subXData = data.get(colYData);

					topLabelLogical += "<text x='" + (chartLeftMargin + 20 + (i * 20))
							+ "' y='0' style='font-size:9;writing-mode:tb;'>" + colYData + "</text>\n";

					colClass = ((i % 2) == 0) ? "id-col-even" : "id-col-odd";
					columnLine += "<line x1='" + (chartLeftMargin + (i * 20)) + "' y1='" + startingYPos + "' x2='"
							+ (chartLeftMargin + (i * 20)) + "' y2='600' class='" + colClass + "'></line>";

					rowClass = ((i % 2) == 0) ? "id-row-even" : "id-row-odd";
					svgRect += "<rect x='" + chartLeftMargin + "' y='" + ((i * rowHeight) + startingYPos) + "' width='"
							+ chartWidth + "' height='" + rowHeight + "' class='" + rowClass + "'></rect>";

					int idxColorPallet = 1;
					for (Long colXData : subXData.keySet()) {
						Data d = subXData.get(colXData);

						long currRowIndex = 0;
						for (long currTime : valTimes) {
							if (colXData < currTime) {
								break;
							}
							// leftLabel += colXData+" "+currTime+"\n";
							currRowIndex++;
						}

						Date dateXTimes = new Date(colXData);
						SimpleDateFormat sdf = new SimpleDateFormat(dateFormat1);
						String nonLogicalTime = sdf.format(dateXTimes);

						double cy = (currRowIndex * rowHeight) + (rowRatio * colXData) + startingYPos;

						// svgCircle += "<circle cx='"+(cx+chartLeftMargin)+"'
						// cy='"+((i*halfRowheight)+(halfRowheight+chartTopMargin))+"'
						// r='4' class='"+cssClassName+idxColorPallet+"'
						// data-time-nl='"+nonLogicalTime+"'
						// data-time='"+colXData+"'
						// data-component='"+colYData+"'
						// data-color='"+d.getName()+"'
						// data-shape='"+d.getType()+"'></circle>";
						svgCircle += "<circle cx='" + (chartLeftMargin + 20 + (i * 20)) + "' cy='" + cy
								+ "' r='4' class='" + cssClassName + idxColorPallet + "' data-time-nl='"
								+ nonLogicalTime + "' data-time='" + colXData + "' data-component='" + colYData
								+ "' data-color='" + d.getName() + "' data-shape='" + d.getType() + "'></circle>";
						if (idxColorPallet > colorIndex) {
							idxColorPallet = 1;
						}

						idxColorPallet++;
					}

					i++;
				}
			} else if (dottedMode
					.equals("Default")) { /* DEFAULT DOTTED CHART */
				int i = 0; // int jmlcircle = 0; double temp_cx = 0;
				for (String colYData : data.keySet()) {
					Map<Long, Data> subXData = data.get(colYData);

					leftLabel += "<text x='80' y='" + ((i * halfRowheight) + (halfRowheight + chartTopMargin))
							+ "' style='text-anchor:end;font-size:9;text-align:left;'>" + colYData + "</text>\n";

					rowClass = ((i % 2) == 0) ? "id-row-even" : "id-row-odd";
					svgRect += "<rect x='" + chartLeftMargin + "' y='" + ((i * rowHeight) + chartTopMargin)
							+ "' width='" + chartWidth + "' height='" + rowHeight + "' class='" + rowClass
							+ "'></rect>";

					int idxColorPallet = 1;
					for (Long colXData : subXData.keySet()) {
						Data d = subXData.get(colXData);

						long currColIndex = 0;
						for (long currTime : valTimes) {
							if (colXData < currTime) {
								break;
							}
							currColIndex++;
						}

						Date dateXTimes = new Date(colXData);
						SimpleDateFormat sdf = new SimpleDateFormat(dateFormat1);
						String nonLogicalTime = sdf.format(dateXTimes);

						double cx = (currColIndex * colWidth) + (columnRatio * colXData);

						svgCircle += "<circle cx='" + (cx + chartLeftMargin) + "' cy='"
								+ ((i * halfRowheight) + (halfRowheight + chartTopMargin)) + "' r='4' class='"
								+ cssClassName + idxColorPallet + "' data-time-nl='" + nonLogicalTime + "' data-time='"
								+ colXData + "' data-component='" + colYData + "' data-color='" + d.getName()
								+ "' data-shape='" + d.getType() + "'></circle>";
						if (idxColorPallet > colorIndex) {
							idxColorPallet = 1;
						}
						// if(temp_cx != cx){jmlcircle++; temp_cx = cx;}
						idxColorPallet++;
					}
					i++;
				}
			}

			svgTopLabel += "<g id='topLabelLogical'>" + topLabelLogical + "</g>";
			svgTopLabel += "<g id='topLabelNonLogical' style='visibility:hidden'>" + topLabelNonLogical + "</g>";
			svgTopLabel += "</svg>";

			svg += "<g>" + columnLine + "</g>";
			svgLeftLabel += "<g id='leftLabel'>" + leftLabel + "</g>";
			if (dottedMode.equals("Transpose")) {
				svgLeftLabel += "<g id='leftLabelNonLogical' style='visibility:hidden'>" + leftLabelNonLogical + "</g>";
			}
			svgLeftLabel += "</svg>";

			svg += "<g>" + svgRect + "</g>";
			svg += "<g id='allCircle'>" + svgCircle + "</g>";
			svg += "<svg>";

			view.addObject("svgLeftLabel", svgLeftLabel);
			view.addObject("svgTopLabel", svgTopLabel);
			view.addObject("svg", svg);

		}
		return view;
	}
}
