/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.services.spark.hdfs;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.web.services.Job;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.spark.SparkJob;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.BabWeb;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers.HdfsConfiguration;
import kr.ac.pusan.bsclab.bab.v2.web.services.IJobFactory;

@Service
@ConditionalOnProperty(name = "bab.serviceJobFactory", havingValue = "hdfs-spark")
public class HdfsSparkJobFactory implements IJobFactory {

	@Autowired
	protected SparkConfiguration sparkConfig;

	@Autowired
	protected HdfsConfiguration hdfsConfig;

	protected ObjectMapper mapper = new ObjectMapper();

	@Override
	public Job create(ServiceEndpoint endpoint, IServiceRequest request) {
		try {
			SparkJob job = new SparkJob(sparkConfig);
			String requestJson = "";
			if (request != null) {
				requestJson = mapper.writeValueAsString(request);
			}
			String hash = SparkJob.getHash(endpoint.toString() + requestJson);
			job.setHash(hash);
			job.setName(job.getName() + "://" + endpoint.getPack().name() + "/ " + endpoint.getService().name() + "/"
					+ job.getHash());
			job.getAppArgs().add(hdfsConfig.getUrl());
			job.getAppArgs().add(sparkConfig.getUrl());
			job.getAppArgs().add(endpoint.getService().name());

			File jarFile = new File(endpoint.getJarPath());
			String jarPath = hdfsConfig.getUrl() + "/packages/" + jarFile.getName();
			job.setAppResource(jarPath);
			job.setMainClass(endpoint.getPack().mainClass());

			job.getSparkProperties().put("spark.jars", sparkConfig.getJars() + "," + jarPath);
			job.getSparkProperties().put("spark.driver.extraClassPath", sparkConfig.getDriverExtraClassPath());
			job.getSparkProperties().put("spark.executor.extraClassPath",
					sparkConfig.getExecutorExtraClassPath() + "," + jarPath);

			return job;
		} catch (Exception ex) {
			BabWeb.log(this).error(ex.getMessage());
		}
		return null;
	}

}
