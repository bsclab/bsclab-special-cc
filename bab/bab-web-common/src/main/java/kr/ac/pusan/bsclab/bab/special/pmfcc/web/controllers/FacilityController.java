package kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.v2.web.BabWebCommon;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers.AbstractWebController;

@Controller
public class FacilityController extends AbstractWebController {
	
	public static final String BASE_URL = BabWebCommon.BASE_URL;
		
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/pmfcc/facility/")
	public ModelAndView getIndex(HttpServletRequest request, HttpSession session) {
		
		ModelAndView view = new ModelAndView("facility/facility");
		
		String message = this.getClass().getName();

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", "default");
		jsonData.put("BASE_URL", BASE_URL);
		jsonData.put("importUri", apiManager.getAPIURI().get("repository").get("import"));
		jsonData.put("sdt", (String) session.getAttribute("sdt"));
		jsonData.put("edt", (String) session.getAttribute("edt"));

		session.setAttribute("workspaceId", "default");

		view.addObject("jsonData", jsonData);
		view.addObject("message", message);
		view.addObject("apiURI", apiManager.getAPIURI());
		return view;
	};
}