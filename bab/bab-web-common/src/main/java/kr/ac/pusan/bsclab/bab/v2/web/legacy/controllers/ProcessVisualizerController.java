/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.v2.web.legacy.models.Response;
import kr.ac.pusan.bsclab.bab.v2.web.services.hdfs.IFileManager;
import kr.ac.pusan.bsclab.bab.v2.web.services.hdfs.LocalFileManager;
//import kr.ac.pusan.bsclab.bab.assembly.GraphViz;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.EventListModel;

@Controller
public class ProcessVisualizerController extends AbstractWebController {

	public static final String BASE_URL = AbstractWebController.BASE_URL + "/processvisualizer";

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/analysis-server/{workspaceId}/{datasetId}/{repositoryId}")
	public ModelAndView getAnalysis(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,

			HeuristicMinerJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpSession session) {
		ModelAndView view = new ModelAndView("processvisualizer/analysis");

		if (config == null) {
			config = new HeuristicMinerJobConfiguration();
		}
//		String uri = APIURI.get("model").get("heuristic") + workspaceId + "/" + datasetId + "/" + repositoryId;
//		String uri = apiManager.getAPIURI().get("model").get("heuristic") + workspaceId + "/" + datasetId + "/" + sdt + "/" + edt;

		// Response<HeuristicModel> response = callBabService(uri, config,
		// HeuristicModel.class);

		// add jsondata here
		Map<String, String> jsonData = buildJSONWorkspaceDatasetRepository(workspaceId, datasetId, sdt, edt);

		view.addObject("jsonData", jsonData);
		view.addObject("apiURI", apiManager.getAPIURI());
		view.addObject("webURI", apiManager.getWEBURI());
		// view.addObject("processModel", response);
		view.addObject("config", config);

		return view;
	}

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/analysis/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getAnalysisClient(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, HeuristicMinerJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpSession session) {
		ModelAndView view = new ModelAndView("processvisualizer/analysis-client");

		if (config == null) {
			config = new HeuristicMinerJobConfiguration();
		}
//		String uri = apiManager.getAPIURI().get("model").get("heuristic") + workspaceId + "/" + datasetId + "/" + sdt + "/" + edt; 

		// add jsondata here
		Map<String, String> jsonData = buildJSONWorkspaceDatasetRepository(workspaceId, datasetId, sdt, edt);
		
		view.addObject("jsonData", jsonData);
		view.addObject("apiURI", apiManager.getAPIURI());
		view.addObject("webURI", apiManager.getWEBURI());
		view.addObject("config", config);

		return view;
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/logreplay/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getSimulation(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("processvisualizer/logreplay");

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
		
//		jsonData.put("repositoryId", repositoryId);

		view.addObject("jsonData", jsonData);
		view.addObject("apiURI", apiManager.getAPIURI());


		return view;
	}

	
	protected IFileManager localFileManager = new LocalFileManager();
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/logreplaypart/{workspaceId}/{datasetId}/{repositoryId}/{file}/{part}/{extension}")
	@ResponseBody
	public Response<EventListModel> getIndex(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId, 
			@PathVariable(value = "file") String file, 
			@PathVariable(value = "part") String part, 
			@PathVariable(value = "extension") String extension, 
			HttpServletRequest request,
			HttpSession session) {
		String uri = getHdfsConfig().getRestUrl() + "/workspaces/" + workspaceId + "/" + datasetId + "/" + repositoryId + "/" + file + "/" + part + "." + extension;
		String result = getFileManager().download(uri);
		if (result != null) {
			result = localFileManager.loadFromTextFile(result);
			try {
				return new Response<EventListModel>(null, getMapper().readValue(result, EventListModel.class));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/*
	 * round to 2 decimal : http://stackoverflow.com/a/30929419/1843755
	 */
	public static float round(double d, int decimalPlace) {
		return BigDecimal.valueOf(d).setScale(decimalPlace, BigDecimal.ROUND_HALF_UP).floatValue();
	}
	
	
	/*
	 * use servlet context
	 * http://stackoverflow.com/questions/26923907/how-to-get-getservletcontext-
	 * in-spring-mvc-controller use servlet context
	 * http://www.codejava.net/frameworks/spring/spring-mvc-sample-application-
	 * for-downloading-files get absoulte path
	 * http://stackoverflow.com/questions/3093423/getting-spring-mvc-relative-
	 * path return as svg xml
	 * http://stackoverflow.com/questions/19190243/can-you-set-the-src-attribute
	 * -of-an-html-image-tag-to-a-controller-method read and write file
	 * https://docs.oracle.com/javase/tutorial/essential/io/file.html
	 */
//	@Autowired
//	ServletContext servletContext;
//
//	@RequestMapping(method = RequestMethod.GET, path = BASE_URL
//			+ "/renderheuristic/{workspaceId}/{datasetId}/{repositoryId}")
//	public @ResponseBody ResponseEntity<String> getHeuristicGraphviz(
//			@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId, HttpServletRequest request,
//			HttpSession session) {
//		HeuristicMinerJobConfiguration config = new HeuristicMinerJobConfiguration();
//		config.setPositiveObservation(0);
//		config.getThreshold().setDependency(0.1);
//
////		String uri = APIURI.get("model").get("heuristic") + workspaceId + "/" + datasetId + "/" + repositoryId;
//		String uri = apiManager.getAPIURI().get("model").get("heuristic") + workspaceId + "/" + datasetId + "/" + repositoryId;
//
//		Response<HeuristicModel> model = callBabService(uri, config, HeuristicModel.class);
//
//		String executable = "";
//		String tempDir = "";
//
//		ServletContext context = request.getServletContext();
//		String absoluteDiskPath = context.getRealPath("/static/images");
//		// check OS
//		// http://stackoverflow.com/questions/14288185/detecting-windows-or-linux
//		if (SystemUtils.IS_OS_WINDOWS) {
//			//executable = "C:/Program Files (x86)/Graphviz2.38/bin/dot.exe";
//			executable = "C:/dev/lib/graphviz-2.38/release/bin/dot.exe";
//			tempDir = "C:/tmp/";
//		} else if (SystemUtils.IS_OS_LINUX) {
//			executable = "/usr/local/bin/dot";
//			tempDir = "/tmp/";
//		}
//
//		long startTime = System.nanoTime();
//
//		GraphViz gv = new GraphViz(executable, tempDir);
//		gv.addln(gv.start_graph());
//
//		/*
//		 * graphviz label
//		 * http://stackoverflow.com/questions/1494492/graphviz-how-to-go-from-
//		 * dot-to-a-graph
//		 */
//		Integer countNode = 1;
//		Map<String, Integer> nodes = new HashMap<String, Integer>();
//		for (Node node : model.getResponse().getNodes().values()) {
//			nodes.put(node.getLabel(), countNode);
//			String label = String.valueOf(countNode);
//			String subLabel = String.valueOf(node.getFrequency().getAbsolute());
//			String options = "[label=\"{<f0>" + node.getLabel() + "|<f1>" + subLabel + "}\" shape=Mrecord]";
//			gv.addln(label + " " + options + ";");
//			countNode++;
//		}
//
//		for (Arc arc : model.getResponse().getArcs().values()) {
//			String source = String.valueOf(nodes.get(arc.getSource()));
//			String destination = String.valueOf(nodes.get(arc.getTarget()));
//			String options = "[label=<" + String.valueOf(arc.getFrequency().getAbsolute()) + "<br/>"
//					+ String.valueOf(round(arc.getDependency(), 2)) + ">]";
//			gv.addln(source + " -> " + destination + " " + options + ";");
//		}
//		gv.addln(gv.end_graph());
//
//		String type = "svg"; // gif / dot / fig / pdf / ps / svg / png / plain
//		String repesentationType = "dot"; // dot / neato fdp / sfdp / twopi /
//											// circo
//		String path = tempDir + "out" + workspaceId + "-" + datasetId + "-" + repositoryId + "." + type;
//		File out = new File(path);
//		gv.writeGraphToFile(gv.getGraph(gv.getDotSource(), type, repesentationType), out);
//
//		long stopTime = System.nanoTime();
//
//		// reading file
//		String SVG = "";
//		Charset charset = Charset.forName("UTF-8");
//		Path thePath = Paths.get(path);
//		try (BufferedReader reader = Files.newBufferedReader(thePath, charset)) {
//			String line = null;
//			while ((line = reader.readLine()) != null) {
//				SVG += line;
//			}
//		} catch (IOException x) {
//			System.err.format("IOException: %s%n", x);
//		}
//
//		// return SVG string
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.valueOf("image/svg+xml"));
//		ResponseEntity<String> svgEntity = new ResponseEntity<String>(SVG, headers, HttpStatus.OK);
//		return svgEntity;
//	}
}