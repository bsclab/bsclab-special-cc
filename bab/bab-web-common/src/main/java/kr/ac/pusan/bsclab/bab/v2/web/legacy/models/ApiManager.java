package kr.ac.pusan.bsclab.bab.v2.web.legacy.models;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kr.ac.pusan.bsclab.bab.v2.web.models.BabConfiguration;

@Component
public class ApiManager {

	@Autowired
	private BabConfiguration ap;
	//
	// public final sMap<String, Map<String, String>> APIURI = setAPIURI();
	// public final Map<String, Map<String, String>> WEBURI = setWEBURI();

	public Map<String, Map<String, String>> getWEBURI() {
		Map<String, Map<String, String>> WEBURI = new HashMap<String, Map<String, String>>();
		Map<String, String> processvisualizer = new HashMap<String, String>();
		// processvisualizer.put("renderheuristic",
		// WEB_ROOT+"/processvisualizer/renderheuristic/");
		processvisualizer.put("renderheuristic", ap.getUrl() + "/processvisualizer/renderheuristic/");
		WEBURI.put("processvisualizer", processvisualizer);

		return WEBURI;
	}

	public Map<String, Map<String, String>> getAPIURI() {
		Map<String, Map<String, String>> API = new HashMap<String, Map<String, String>>();
		Map<String, String> analysis = new HashMap<String, String>();
		Map<String, String> model = new HashMap<String, String>();
		Map<String, String> repository = new HashMap<String, String>();
		Map<String, String> job = new HashMap<String, String>();
		Map<String, String> schedule = new HashMap<String, String>();
		Map<String, String> rservices = new HashMap<String, String>();

		String apiRoot = ap.getApiUrl();
		String root = ap.getUrl();

		analysis.put("associationrule",
				apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/AnalysisAssociationRuleJob/");
		analysis.put("socialnetwork",
				apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/AnalysisSocialNetworkJob/");
		analysis.put("timegap", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/AnalysisTimeGapJob/");
		analysis.put("mtga", apiRoot + "/analysis/mtga/");
		analysis.put("delta", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/AnalysisDeltaJob/");
		analysis.put("taskmatrix", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/AnalysisTaskMatrixJob/");
		analysis.put("dottedchart", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/AnalysisDottedChartJob/");

		model.put("heuristic", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/ModelHeuristicJob/");
		model.put("logreplay", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/ModelLogReplayJob/");
		model.put("logreplaypart", root + "/bab/processvisualizer/logreplaypart/");
		repository.put("view", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/RepositoryViewJob/");
		repository.put("summary", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/RepositoryLogSummaryJob/");
		repository.put("viewmap", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/RepositoryViewDatasetJob/");
		repository.put("importcsv", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/RepositoryCsvImportJob/");
		repository.put("mappings", apiRoot + "/legacy/repository/datasets/");
		
		repository.put("import", apiRoot + "/legacy/repository/import/");
		repository.put("map", apiRoot + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/RepositoryMappingJob/");
		repository.put("workspace", apiRoot + "/legacy/repository/workspace/");
		repository.put("datasets", apiRoot + "/legacy/repository/datasets/");
		repository.put("repositories", apiRoot + "/legacy/repository/repositories/");
		repository.put("upload", apiRoot + "/legacy/repository/upload");

		job.put("list", apiRoot + "/job");
		job.put("kill", apiRoot + "/job/kill/");
		job.put("pull", apiRoot + "/job/pull/");
		job.put("queue", apiRoot + "/job/status/");
		job.put("report", apiRoot + "/job/report/");
		job.put("services", apiRoot + "/job/services/");

		schedule.put("logdata", apiRoot + "/logdata/");
		schedule.put("services", apiRoot + "/job/services/");
		rservices.put("rservices", apiRoot + "/rserver/rservices/");

		API.put("analysis", analysis);
		API.put("repository", repository);
		API.put("model", model);
		API.put("job", job);
		API.put("schedule", schedule);
		API.put("rservices", rservices);

		return API;
	}

}
