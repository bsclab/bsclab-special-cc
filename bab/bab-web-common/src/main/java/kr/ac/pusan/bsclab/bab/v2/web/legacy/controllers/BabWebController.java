/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.v2.web.BabWebCommon;

@Controller
public class BabWebController extends AbstractWebController {

	public static final String BASE_URL = BabWebCommon.BASE_URL;
	
	@RequestMapping(method = RequestMethod.GET, path = "/")
	public @ResponseBody ModelAndView getRootIndex(HttpSession session) {
		return new ModelAndView("redirect:/bab/login");
	}
	
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/home/{workspaceId}")
	public ModelAndView getIndex(@PathVariable(value = "workspaceId") String workspaceId, HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("home/home");
		
		String message = this.getClass().getName();

		String[] vehicles = { "KIA", "Hyundai", "Daewoo", "Samsung" };

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("importUri", apiManager.getAPIURI().get("repository").get("import"));
		jsonData.put("sdt", (String) session.getAttribute("sdt"));
		jsonData.put("edt", (String) session.getAttribute("edt"));

		session.setAttribute("workspaceId", workspaceId);

		view.addObject("jsonData", jsonData);
		view.addObject("message", message);
		view.addObject("vehicles", vehicles);
		view.addObject("apiURI", apiManager.getAPIURI());
		return view;
	};

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
	public ModelAndView getIndex(HttpServletRequest request, HttpSession session) {
		return new ModelAndView("redirect:/bab/home/default");
	}

	@RequestMapping(method = RequestMethod.POST, path = BASE_URL + "/home/{workspaceId}")
	public ModelAndView getIndex(@RequestParam("files") MultipartFile[] files) {
		ModelAndView view = new ModelAndView("bab/index");

		String message = this.getClass().getName();
		view.addObject("message", message);

		String rootPath = System.getProperty("catalina.home");
		File uploadDir = new File(rootPath + File.separator + "uploads");
		if (!uploadDir.exists())
			uploadDir.mkdirs();

		for (MultipartFile file : files) {
			if (!file.isEmpty()) {
				try {
					byte[] bytes = file.getBytes();
					File serverFile = new File(
							uploadDir.getAbsolutePath() + File.separator + file.getOriginalFilename());
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(bytes);
					stream.close();
//					TODO: 
//					webHdfs.copyFromLocal(serverFile.getPath(), "/test/test.txt");
//					webHdfs.listStatus("/test");
//					webHdfs.isExists("/test/test.txt");
//					webHdfs.openAsTextFile("/test/test.txt");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return view;
	}
}
