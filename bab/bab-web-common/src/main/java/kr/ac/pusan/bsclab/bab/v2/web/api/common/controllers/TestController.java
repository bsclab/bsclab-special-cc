/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.api.common.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceResponse;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;

@Controller
public class TestController extends ASparkServiceController {

	public static final String BASE_URL = AServiceController.BASE_URL + "/test";

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/submit")
	public @ResponseBody IServiceResponse getTestSubmit() {
		ServiceEndpoint se = getJobManager().getServices(0).get("kr.ac.pusan.bsclab.bab.v2.legacy")
				.get("HelloWorldJob");
		IServiceRequest sr = null;
		try {
			sr = se.getService().requestClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		if (sr == null) {
			sr = new Configuration();
		}
		Configuration config = (Configuration) sr;
		config.setRepositoryURI("/default/default/1");
		config.setWorkspaceId("default");
		config.setDatasetId("default");
		config.setRepositoryId("1");
		IServiceResponse response = runService(se, "default", "default", "1", sr);
		return response;
	}

	@CrossOrigin
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/heuristic")
	public @ResponseBody IServiceResponse getTestHeuristic() {
		ServiceEndpoint se = getJobManager().getServices(0).get("kr.ac.pusan.bsclab.bab.v2.legacy")
				.get("ModelHeuristicJob");
		IServiceRequest sr = null;
		try {
			sr = se.getService().requestClass().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		if (sr == null) {
			sr = new Configuration();
		}
		Configuration config = (Configuration) sr;
		config.setRepositoryURI("/default/default/1");
		config.setWorkspaceId("default");
		config.setDatasetId("default");
		config.setRepositoryId("1");
		IServiceResponse response = runService(se, "default", "default", "1", sr);
		return response;
	}

}
