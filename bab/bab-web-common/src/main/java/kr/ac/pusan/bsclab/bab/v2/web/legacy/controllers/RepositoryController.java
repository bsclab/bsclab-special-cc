/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.v2.web.controllers.AServiceController;
import kr.ac.pusan.bsclab.bab.v2.web.legacy.models.Response;
import kr.ac.pusan.bsclab.bab.v2.web.services.Job;
import kr.ac.pusan.bsclab.bab.v2.web.services.hdfs.IFileManager;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.ImportJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.ImportJobResult;
import kr.ac.pusan.bsclab.bab.ws.model.BDataset;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;
import kr.ac.pusan.bsclab.bab.ws.model.BWorkspace;

@Controller
public class RepositoryController extends AbstractWebController {

	public static final String BASE_URL = AServiceController.BASE_URL + "/legacy/repository";
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/workspaces")
	public @ResponseBody Response<List<BWorkspace>> getWorkspaces(ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			List<String[]> d = getFileManager().listDirectory(getHdfsConfig().getRestUrl() + "/workspaces");
			List<BWorkspace> workspaces = new ArrayList<BWorkspace>();
			for (String[] f : d) {
				if (f[2].equalsIgnoreCase(IFileManager.TYPE_DIR)) {
					BWorkspace w = new BWorkspace();
					w.setId(f[0]);
					w.setName(f[0]);
					w.setDescription(f[0]);
					w.setUri(getConfig().getApiUrl() + "/legacy/repository/datasets/" + f[0]);
					workspaces.add(w);
				}
			}
			Response<List<BWorkspace>> babResponse = new Response<List<BWorkspace>>(null, workspaces);
			babResponse.setStatus(Job.STATUS_FINISHED);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/datasets/{workspaceId}")
	public @ResponseBody Response<List<BDataset>> getDatasets(@PathVariable(value = "workspaceId") String workspaceId, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			List<String[]> d = getFileManager().listDirectory(getHdfsConfig().getRestUrl() + "/workspaces/" + workspaceId);
			List<BDataset> datasets = new ArrayList<BDataset>();
			if (d != null) {
				for (String[] f : d) {
					if (f[2].equalsIgnoreCase(IFileManager.TYPE_FILE) && f[0].toLowerCase().endsWith(".mrepo")) {
						BDataset w = new BDataset();
						String name = f[0].substring(0, f[0].length() - 6);
						w.setId(name);
						w.setName(name);
						w.setDescription(name);
						w.setUri(getConfig().getApiUrl() + "/legacy/repository/repositories/" + workspaceId + "/" + name);
						w.setMappingUri(getConfig().getApiUrl() + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/RepositoryViewDatasetJob/" + workspaceId + "/" + name);
						datasets.add(w);
					}
				}
			}
			Response<List<BDataset>> babResponse = new Response<List<BDataset>>(null, datasets);
			babResponse.setStatus(Job.STATUS_FINISHED);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/repositories/{workspaceId}")
	public @ResponseBody Response<List<BRepository>> getRepositories(@PathVariable(value = "workspaceId") String workspaceId,
			ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		Response<List<BRepository>> babResponse = null;
		try {
			List<String[]> w = getFileManager().listDirectory(getHdfsConfig().getRestUrl() + "/workspaces/" + workspaceId);
			List<BRepository> repositories = new ArrayList<BRepository>();
			for (String[] wd : w) {
				if (wd[2].equalsIgnoreCase(IFileManager.TYPE_FILE) && wd[0].toLowerCase().endsWith(".mrepo")) {
					String datasetId = wd[0].substring(0, wd[0].length() - 6);;
					List<String[]> d = getFileManager().listDirectory(getHdfsConfig().getRestUrl() + "/workspaces/" + workspaceId + "/" + datasetId);
					for (String[] f : d) {
						if (f[2].equalsIgnoreCase(IFileManager.TYPE_FILE) && f[0].toLowerCase().endsWith(".brepo")) {
							BRepository br = new BRepository();
							String name = datasetId + "/" + f[0].substring(0, f[0].length() - 6);
							br.setId(name);
							br.setName(name);
							br.setDescription(name);
							br.setUri(getConfig().getApiUrl() + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/RepositoryViewJob/" + workspaceId + "/" + name);
							repositories.add(br);
						}
					}
				}
			}
			babResponse = new Response<List<BRepository>>(null, repositories);
			babResponse.setStatus(Job.STATUS_FINISHED);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return babResponse;
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/repositories/{workspaceId}/{datasetId}")
	public @ResponseBody Response<List<BRepository>> getRepositories(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		Response<List<BRepository>> babResponse = null;
		try {
			List<String[]> d = getFileManager().listDirectory(getHdfsConfig().getRestUrl() + "/workspaces/" + workspaceId + "/" + datasetId);
			List<BRepository> repositories = new ArrayList<BRepository>();
			for (String[] f : d) {
				if (f[2].equalsIgnoreCase(IFileManager.TYPE_FILE) && f[0].toLowerCase().endsWith(".brepo")) {
					BRepository br = new BRepository();
					String name = f[0].substring(0, f[0].length() - 6);
					br.setId(name);
					br.setName(name);
					br.setDescription(name);
					br.setUri(getConfig().getApiUrl() + "/spark/submit/kr.ac.pusan.bsclab.bab.v2.legacy/RepositoryViewJob/" + workspaceId + "/" + datasetId + "/" + name);
					repositories.add(br);
				}
			}
			babResponse = new Response<List<BRepository>>(null, repositories);
			babResponse.setStatus(Job.STATUS_FINISHED);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return babResponse;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/dataset/{workspaceId}/{datasetId}")
	public @ResponseBody Response<ImportJobResult> getDataset(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		String apiURI = apiManager.getAPIURI().get("repository").get("viewdataset")+ workspaceId + "/" + datasetId;
		ImportJobConfiguration importConfig = new ImportJobConfiguration();
		importConfig.setWorkspaceId(workspaceId);
		importConfig.setDatasetId(datasetId);
		importConfig.setRepositoryURI("/" + workspaceId + "/" + datasetId);
		Response<ImportJobResult> model = callBabServiceV2(apiURI, importConfig, ImportJobResult.class);
		return model;
	}
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.POST }, path = BASE_URL
			+ "/import/{workspaceId}")
	public @ResponseBody ModelAndView postImport(@PathVariable(value = "workspaceId") String workspaceId,
			@RequestParam(value = "format") String format,
			@RequestParam(value = "datasetId") String datasetId,
			@RequestParam("file") MultipartFile file, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String rootPath = System.getProperty("catalina.home");
			File uploadDir = new File(rootPath + File.separator + "uploads");
			if (!uploadDir.exists())
				uploadDir.mkdirs();
			if (!file.isEmpty()) {
				File serverFile = new File(uploadDir.getAbsolutePath() + File.separator + file.getOriginalFilename());
				file.transferTo(serverFile);
				String uri = "/uploads/" + System.currentTimeMillis() + "." + format;
				getFileManager().upload(getHdfsConfig().getRestUrl() + uri, serverFile.getAbsolutePath());
				String apiURI = apiManager.getAPIURI().get("repository").get("import" + format)+ workspaceId + "/" + datasetId;
				ImportJobConfiguration importConfig = new ImportJobConfiguration();
				importConfig.setWorkspaceId(workspaceId);
				importConfig.setDatasetId(datasetId);
				importConfig.setRepositoryURI("/workspaces/" + workspaceId + "/" + datasetId);
				importConfig.setRawPath(getHdfsConfig().getUrl() + uri);
				callBabServiceV2(apiURI, importConfig, ImportJobResult.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/bab/home/" + workspaceId);
	}

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/mappings/{workspaceId}/{datasetId}")
//	public @ResponseBody Response<List<ImportJobResult>> getMappings(
//			@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId, ModelMap modelMap, HttpServletRequest request,
//			HttpServletResponse response) {
//		try {
//			List<FileStatus> fileStatuses = webHdfs
//					.listStatus(webHdfs.getPathOnly("/" + workspaceId + "/" + datasetId, WebHdfs.PATH_WORKSPACES));
//			List<ImportJobResult> mappings = new ArrayList<ImportJobResult>();
//			ObjectMapper mapper = new ObjectMapper();
//			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//			for (FileStatus fs : fileStatuses) {
//				if (!fs.isDirectory() && fs.getPath().getName().endsWith(".mrepo")) {
//					ImportJobResult mr = new ImportJobResult();
//					mr.setId(fs.getPath().getName().substring(0, fs.getPath().getName().length() - 6));
//					mr.setName(mr.getId());
//					mr.setDescription(mr.getId());
//					mr.setUri(BASE_URL + "/viewmap/" + workspaceId + "/" + datasetId + "/" + mr.getId());
//					mappings.add(mr);
//				}
//			}
//			Response<List<ImportJobResult>> babResponse = new Response<List<ImportJobResult>>(null, mappings);
//			babResponse.setStatus(Response.STATUS_FINISHED);
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
//
//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
//			+ "/filter/{workspaceId}/{datasetId}/{sdt}/{edt}")
//	public @ResponseBody Response<BRepository> postFilter(
//			@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "sdt") String sdt,
//			@PathVariable(value = "edt") String edt,
//			FilteringJobConfiguration config,
//			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//			
//		try {
//			Response<BRepository> babResponse = null;
//			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
//					, dc.getTimestamp(edt));
//			if (config != null) {
//	//				if (config.getRepositoryURI() == null) {
//	//				config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, logDao.getChecksum(dc.getTimestamp(sdt), dc.getTimestamp(edt))));
//	//			}
//				ObjectMapper om = new ObjectMapper();
//				String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
//				config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
//						+ "/" + hash);
//				String jobConfig = om.writeValueAsString(config);
//				babResponse = this.submitJob("RepositoryFilteringJob", "brepo", workspaceId,
//						datasetId, resourceDataHash, hash, jobConfig
//						, dc.getTimestamp(sdt), dc.getTimestamp(edt), BRepository.class);
//			}
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

}
