package kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import kr.ac.pusan.bsclab.bab.v2.web.legacy.controllers.AbstractWebController;

@RestController
@RequestMapping("/bab/pmfcc/monitoring_rest")
public class MonitoringRestController extends AbstractWebController {

	@Autowired
	private Environment env;
	
	@RequestMapping(value = "get_factories")
	public ArrayList<Factory> getFactories() {

		ArrayList<Factory> f = new ArrayList<Factory>();

		Factory f1 = new Factory();
		Factory f2 = new Factory();

		f1.id = "1";
		f1.name = "(F001) Factory 1";
		f2.id = "2";
		f2.name = "(F002) Factory 2";

		f.add(f1);
		f.add(f2);

		return f;
	}

	@RequestMapping(value = "get_machines")
	public ArrayList<Machine> getMachines(@RequestBody Factory fact) {

		ArrayList<Machine> f = new ArrayList<Machine>();

		Machine m1 = new Machine();
		Machine m2 = new Machine();
		Machine m3 = new Machine();

		m1.id = "MC_0001";
		m1.name = "MC_0001";
		m2.id = "MC_0002";
		m2.name = "MC_0002";
		m3.id = "MC_0003";
		m3.name = "MC_0003";

		f.add(m1);
		f.add(m2);
		f.add(m3);

		return f;
	}
	
	private Connection getConnection() throws SQLException {
		Properties props = new Properties();

		props.setProperty("user", env.getProperty("spring.datasource.username"));
		props.setProperty("password", env.getProperty("spring.datasource.password"));

		return DriverManager.getConnection(env.getProperty("spring.datasource.url"), props);
	}
	
	private Connection conn;
	
	protected void finalize() throws Throwable  
	{  
	    try { conn.close(); } 
	    catch (SQLException e) { 
	        e.printStackTrace();
	    }
	    super.finalize();  
	}  

	@RequestMapping(value = "get_results_by_hour")
	public HashMap<String, ArrayList<?>> getResultsByHour(@RequestBody ResultsByHour fact) throws SQLException {

		conn = this.getConnection();
		
		HashMap<String, ArrayList<?>> m = new HashMap<String, ArrayList<?>>();
		ArrayList<String> a = new ArrayList<String>();
		for (int i = 0; i < 33; i++) {
			a.add(Integer.toString(i));
		}
		m.put("column_idx", a);
		
		Statement stmt = conn.createStatement();
		String sql = "SELECT * FROM rd001t_mc0001_v_1hour order by time_stamp ;";
		
		ResultSet rs = stmt.executeQuery(sql);
		ArrayList<ArrayList<String>> a2 = new ArrayList<ArrayList<String>>();
		while (rs.next()) {
			
			ArrayList<String> b = new ArrayList<String>();
			b.add(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(rs.getTimestamp("time_stamp")));
			for(int i=1;i<=33;i++) {
				b.add(Double.toString(rs.getDouble("D"+String.format("%02d", i))));
			}
			a2.add(b);
		}
		
		m.put("data", a2);

		ArrayList<String> a3 = new ArrayList<String>();
		a3.add("Date");
		a3.add("Comm check");
		a3.add("Setting Status");
		a3.add("Current Status");
		a3.add("Total Power(current)");
		a3.add("Delata input Voltage");
		a3.add("Star input voltage");
		a3.add("Delta input current");
		a3.add("Star input current");
		a3.add("DC Voltage");
		a3.add("Sequence number");
		a3.add("Fault Number(current)");
		a3.add("Inverter No.1 Power Command (SV)");
		a3.add("Inverter No.2 Power Command (SV)");
		a3.add("Converter Cooling water temp");
		a3.add("Inverter no.1 cooling water temp");
		a3.add("Furnace No.1 cooling water temp");
		a3.add("Inverter no.2 cooling water temp");
		a3.add("Furnace No.2 cooling water temp");
		a3.add("Leak Level(100%)");
		a3.add("Comm check");
		a3.add("Inverter internal Status");
		a3.add("Inverter external Status");
		a3.add("Current Power(current)");
		a3.add("Frequency");
		a3.add("DC Voltage");
		a3.add("DC Current");
		a3.add("Output Voltage");
		a3.add("Output Current");
		a3.add("Fault Number");
		a3.add("Sequence Number");
		a3.add("Last Heating Time");
		a3.add("Q Factor");
		a3.add("Leak Level(1000%)");

		m.put("label", a3);

		return m;
	}

	@RequestMapping(value = "get_machine_parameters")
	public String getMachineParameters(@RequestBody Machine m) {

		return "<div class=\"field\">\r\n" + "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D0\" class='checkparameter' checked> <label>Comm\r\n"
				+ "											Check</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D1\" class='checkparameter' checked> <label>Setting\r\n"
				+ "											Status</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D2\" class='checkparameter' checked> <label>Current\r\n"
				+ "											Status</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D3\" class='checkparameter' checked> <label>Total\r\n"
				+ "											Power(current)</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D4\" class='checkparameter' checked> <label>Delta\r\n"
				+ "											Input Voltage</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D5\" class='checkparameter' checked> <label>Star\r\n"
				+ "											Input Voltage</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D6\" class='checkparameter' checked> <label>Delta\r\n"
				+ "											Input Current</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D7\" class='checkparameter' checked> <label>Star\r\n"
				+ "											Input Current</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D8\" class='checkparameter' checked> <label>DC\r\n"
				+ "											Voltage</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D9\" class='checkparameter' checked> <label>Sequence\r\n"
				+ "											Number</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D10\" class='checkparameter' checked> <label>Fault\r\n"
				+ "											Number(current)</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D11\" class='checkparameter' checked>\r\n"
				+ "										<label>Inverter No. 1 Power Command (SV)</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D12\" class='checkparameter' checked>\r\n"
				+ "										<label>Inverter No. 2 Power Command (SV)</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D13\" class='checkparameter' checked>\r\n"
				+ "										<label>Converter Cooling Water Temperature</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D14\" class='checkparameter' checked>\r\n"
				+ "										<label>Inverter no. 1 Cooling Water Temperature</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D15\" class='checkparameter' checked>\r\n"
				+ "										<label>Furnace no. 1 Cooling Water Temperature</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D16\" class='checkparameter' checked>\r\n"
				+ "										<label>Inverter no. 2 Cooling Water Temperature</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D17\" class='checkparameter' checked>\r\n"
				+ "										<label>Furnace no. 2 Cooling Water Temperature</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D18\" class='checkparameter' checked> <label>Leak\r\n"
				+ "											Level (100%)</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D19\" class='checkparameter' checked> <label>Comm\r\n"
				+ "											Check</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D20\" class='checkparameter' checked>\r\n"
				+ "										<label>Inverter Internal Status</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D21\" class='checkparameter' checked>\r\n"
				+ "										<label>Inverter External Status</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D22\" class='checkparameter' checked> <label>Current\r\n"
				+ "											Power (Current)</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D23\" class='checkparameter' checked> <label>Frequency</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D24\" class='checkparameter' checked> <label>DC\r\n"
				+ "											Voltage</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D25\" class='checkparameter' checked> <label>DC\r\n"
				+ "											Current</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D26\" class='checkparameter' checked> <label>Output\r\n"
				+ "											Voltage</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D27\" class='checkparameter' checked> <label>Output\r\n"
				+ "											Current</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D28\" class='checkparameter' checked> <label>Fault\r\n"
				+ "											Number</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D29\" class='checkparameter' checked> <label>Sequence\r\n"
				+ "											Number</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D30\" class='checkparameter' checked> <label>Last\r\n"
				+ "											Heating Time</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D31\" class='checkparameter' checked> <label>Q\r\n"
				+ "											Factor</label>\r\n"
				+ "									</div>\r\n" + "								</div>\r\n"
				+ "								<div class=\"field\">\r\n"
				+ "									<div class=\"ui checkbox\">\r\n"
				+ "										<input type=\"checkbox\" name=\"D32\" class='checkparameter' checked> <label>Leak\r\n"
				+ "											Level (1000%)</label>\r\n"
				+ "									</div>\r\n" + "								</div>";
	}
}

class Factory {
	public String id;
	public String name;
}

class Machine {
	public String id;
	public String name;
}

class ResultsByHour {
	public String factory_id;
	public String machine_id;
	public String hour;
	public String cnt;
}

class Results {
	public String factory_id;
	public String machine_id;
	public String start_date_field;
	public String end_date_field;
}