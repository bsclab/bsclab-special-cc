package kr.ac.pusan.bsclab.bab.special.pmfcc.web.controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bab/pmfcc/facility_rest")
public class FacilityRestController {

	@Autowired
	private Environment env;
	
	@RequestMapping(value = "get_factories")
	public ArrayList<Factory> getFactories() {

		ArrayList<Factory> f = new ArrayList<Factory>();
		
		Factory f1 = new Factory(); 
		Factory f2 = new Factory(); 
		
		f1.id="1"; f1.name="(F001) Factory 1";		
		f2.id="2"; f2.name="(F002) Factory 2";
		
		f.add(f1);
		f.add(f2);
		
		return f;
	}
	
	@RequestMapping(value = "get_machines")
	public ArrayList<Machine> getMachines(@RequestBody Factory fact) {

		ArrayList<Machine> f = new ArrayList<Machine>();
		
		Machine m1 = new Machine();
		Machine m2 = new Machine();
		Machine m3 = new Machine();
		
		m1.id="MC_0001"; m1.name="MC_0001";
		m2.id="MC_0002"; m2.name="MC_0002";
		m3.id="MC_0003"; m3.name="MC_0003";
		
		f.add(m1);
		f.add(m2);
		f.add(m3);
		
		return f;
	}
	
	private Connection getConnection() throws SQLException {
		Properties props = new Properties();

		props.setProperty("user", env.getProperty("spring.datasource.username"));
		props.setProperty("password", env.getProperty("spring.datasource.password"));

		return DriverManager.getConnection(env.getProperty("spring.datasource.url"), props);
	}
	
	@RequestMapping(value = "get_results")
	public HashMap<String, ArrayList<?>> getResults(@RequestBody Results rest) throws SQLException {
		
		Connection conn = this.getConnection();
		
		HashMap<String, ArrayList<?>> m = new HashMap<String, ArrayList<?>>();
		
		ArrayList<String> a = new ArrayList<String>();
		for(int i=0;i<33;i++) {
			a.add(Integer.toString(i));
		}
		m.put("column_idx", a);
		
		Statement stmt = conn.createStatement();
		String sql = "SELECT * FROM rd001t where MC_ID='"+rest.machine_id+"' "
						+ "and DT>='"+rest.start_date_field+"' and DT<='"+rest.end_date_field+"' "
						+ "order by DT desc;";
		
		ResultSet rs = stmt.executeQuery(sql);
		ArrayList<ArrayList<String>> a2 = new ArrayList<ArrayList<String>>();
		while (rs.next()) {
			
			ArrayList<String> b = new ArrayList<String>();
			b.add(rs.getString("DT"));
			for(int i=1;i<=33;i++) {
				b.add(Double.toString(rs.getDouble("D"+String.format("%02d", i))));
			}
			a2.add(b);
		}
		
		m.put("data", a2);
		
		ArrayList<String> a3 = new ArrayList<String>();
		a3.add("Date");
		a3.add("Comm check");
		a3.add("Setting Status");
		a3.add("Current Status");
		a3.add("Total Power(current)");
		a3.add("Delata input Voltage");
		a3.add("Star input voltage");
		a3.add("Delta input current");
		a3.add("Star input current");
		a3.add("DC Voltage");
		a3.add("Sequence number");
		a3.add("Fault Number(current)");
		a3.add("Inverter No.1 Power Command (SV)");
		a3.add("Inverter No.2 Power Command (SV)");
		a3.add("Converter Cooling water temp");
		a3.add("Inverter no.1 cooling water temp");
		a3.add("Furnace No.1 cooling water temp");
		a3.add("Inverter no.2 cooling water temp");
		a3.add("Furnace No.2 cooling water temp");
		a3.add("Leak Level(100%)");
		a3.add("Comm check");
		a3.add("Inverter internal Status");
		a3.add("Inverter external Status");
		a3.add("Current Power(current)");
		a3.add("Frequency");
		a3.add("DC Voltage");
		a3.add("DC Current");
		a3.add("Output Voltage");
		a3.add("Output Current");
		a3.add("Fault Number");
		a3.add("Sequence Number");
		a3.add("Last Heating Time");
		a3.add("Q Factor");
		a3.add("Leak Level(1000%)");
		
		m.put("label", a3);
		
		conn.close();
		
		return m;
	}
}
