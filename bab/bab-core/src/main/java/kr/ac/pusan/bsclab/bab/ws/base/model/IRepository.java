/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.base.model;

import java.util.Map;

/**
 * Interface for represent BAB repository
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public interface IRepository extends IAttributable, IResource {

	/**
	 * Repository original name on upload
	 * 
	 * @return original repository name
	 */
	String getOriginalName();

	/**
	 * Get repository creation date
	 * 
	 * @return created date
	 */
	long getCreatedDate();

	/**
	 * Get repository number of cases
	 * 
	 * @return number of cases
	 */
	int getNoOfCases();

	/**
	 * Get repository number of events
	 * 
	 * @return number of events
	 */
	int getNoOfEvents();

	/**
	 * Get repository number of activities
	 * 
	 * @return number of activities
	 */
	int getNoOfActivities();

	/**
	 * Get repository number of activity types
	 * 
	 * @return number of activity type
	 */
	int getNoOfActivityTypes();

	/**
	 * Get repository number of originator
	 * 
	 * @return number of originator
	 */
	int getNoOfOriginators();

	/**
	 * Get repository number of resource
	 * 
	 * @return number of resource
	 */
	int getNoOfResourceClasses();

	/**
	 * Get repository number of case attributes
	 * 
	 * @return number of case attributes for each case attribute
	 */
	Map<String, Integer> getNoOfCaseAttributes();

	/**
	 * Get repositroy number of event attributes
	 * 
	 * @return number of event attributes for each event attribute
	 */
	Map<String, Integer> getNoOfEventAttributes();

	/**
	 * Get repository case name and frequency
	 * 
	 * @return case name and frequency
	 */
	Map<String, Integer> getCases();

	/**
	 * Get repository activity name and frequency
	 * 
	 * @return activity name and frequency
	 */
	Map<String, Integer> getActivities();

	/**
	 * Get reposotory activity type name and frequency
	 * 
	 * @return activity type name and frequency
	 */
	Map<String, Integer> getActivityTypes();

	/**
	 * Get repository originator name and frequency
	 * 
	 * @return originator name and frequency
	 */
	Map<String, Integer> getOriginators();

	/**
	 * Get repository resource name and frequency
	 * 
	 * @return resource name and frequency
	 */
	Map<String, Integer> getResources();

	/**
	 * Get repository case attribute name and frequency
	 * 
	 * @return case attribute name and frequency
	 */
	Map<String, Map<String, Integer>> getCaseAttributes();

	/**
	 * Get repository event attribute name and frequency
	 * 
	 * @return event attribute name and frequency
	 */
	Map<String, Map<String, Integer>> getEventAttributes();

	/**
	 * Get repository first event timestamp
	 * 
	 * @return first event timestamp
	 */
	long getTimestampStart();

	/**
	 * Get repository last event timestamp
	 * 
	 * @return last event timestamp
	 */
	long getTimestampEnd();
}
