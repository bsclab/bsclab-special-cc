//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

// @ Project : Log Miner Web Service
// @ File Name : BWorkspace.java
// @ Date : 7/14/2014
// @ Author : Iq Reviessay Pulshashi
//

package kr.ac.pusan.bsclab.bab.ws.model;

import java.util.Map;
import java.util.TreeMap;
import kr.ac.pusan.bsclab.bab.ws.base.model.IWorkspace;

/**
 * Basic implementation of BAB workspace <br>
 * <br >
 * See {@link IWorkspace}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */

public class BWorkspace implements IWorkspace {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String description;
	private String uri;
	private Map<String, Object> attributes;

	@Override
	public Map<String, Object> getAttributes() {
		if (attributes == null) {
			attributes = new TreeMap<String, Object>();
		}
		return attributes;
	}

	@Override
	public String getResourceClass() {
		return "repository.BWorkspace";
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getUri() {
		return uri;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

}
