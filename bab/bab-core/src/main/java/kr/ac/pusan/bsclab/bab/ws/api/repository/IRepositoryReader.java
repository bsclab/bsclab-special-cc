/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository;

import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;

/**
 * Abstraction for repositor reader
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 */
public interface IRepositoryReader {

	/**
	 * Get processed repository
	 * 
	 * @return repository instance
	 */
	public IRepository getRepository();

	/**
	 * Get repository cases on normal list Cautions: this not scale to million
	 * records
	 * 
	 * @return cases
	 */
	public Map<String, ICase> getCases();

}
