/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.model;

import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.api.repository.IRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;

public class RepositoryResource implements IResource, IRepositoryReader {

	protected String id;
	protected final String uri;
	protected IRepositoryReader reader;
	protected IRepository repository;
	protected Map<String, ICase> cases;

	public RepositoryResource(String uri, IRepositoryReader reader) {
		this.uri = uri;
		this.reader = reader;
	}

	public RepositoryResource(String uri, IRepository repository) {
		this.uri = uri;
		this.repository = repository;
	}

	public IRepositoryReader getRepositoryReader() {
		return reader;
	}

	@Override
	public IRepository getRepository() {
		return reader == null ? repository : reader.getRepository();
	}

	@Override
	public Map<String, ICase> getCases() {
		return reader == null ? cases : reader.getCases();
	}

	@Override
	public String getResourceClass() {
		return "IRepository Resource";
	}

	@Override
	public String getId() {
		if (id == null)
			id = "IRepositoryResource@" + String.valueOf(System.nanoTime());
		return id;
	}

	@Override
	public String getUri() {
		return uri;
	}
}
