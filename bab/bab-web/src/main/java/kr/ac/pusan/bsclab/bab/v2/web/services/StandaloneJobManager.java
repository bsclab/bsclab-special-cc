/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2.web.services;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.scannotation.AnnotationDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabPackage;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceExecutor;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceEndpoint;
import kr.ac.pusan.bsclab.bab.v2.web.BabWeb;
import kr.ac.pusan.bsclab.bab.v2.web.models.BabConfiguration;
import kr.ac.pusan.bsclab.bab.ws.controller.DateUtil;

@Component
@ConditionalOnProperty(name = "bab.serviceJobManager", havingValue = "standalone", matchIfMissing = true)
public class StandaloneJobManager implements IJobManager {

	protected final String sessionId;
	protected final Map<String, Job> all = new LinkedHashMap<String, Job>();
	protected final Map<String, Job> hash = new LinkedHashMap<String, Job>();

	protected Map<String, Job> queue;
	protected Map<String, Job> running;
	protected Map<String, Job> completed;

	@Autowired
	protected BabConfiguration config;

	@Autowired
	protected IJobFactory jobFactory;

	@Autowired
	protected IJobExecutor jobExecutor;

	protected Map<String, Map<String, ServiceEndpoint>> services;
	protected Map<String, Map<String, Set<ServiceEndpoint>>> jars;

	protected int maxSimultaneousRun = 1;

	public StandaloneJobManager() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		sessionId = sdf.format(new Date());
	}

	public IJobFactory getJobFactory() {
		return jobFactory;
	}

	public IJobExecutor getJobExecutor() {
		return jobExecutor;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Map<String, ServiceEndpoint>> getServices(int reload) {
		if (services == null) {
			services = new TreeMap<String, Map<String, ServiceEndpoint>>();
			reload = 1;
		}
		if (reload == 1) {
			try {
				services.clear();
				File packageDirectory = new File(config.getPackageDirectory());
				if (packageDirectory.exists()) {
					File[] jars = packageDirectory.listFiles(new FileFilter() {
						@Override
						public boolean accept(File file) {
							return file.getAbsolutePath().toLowerCase().endsWith(".jar");
						}
					});
					if (jars != null && jars.length > 0) {
						for (File jar : jars) {
							BabWeb.log(this).info("Found jar: " + jar.getAbsolutePath() + ", loading packages ...");
							URL[] urls = new URL[] { jar.toURI().toURL() };
							URLClassLoader cl = new URLClassLoader(urls,
									Thread.currentThread().getContextClassLoader());
							AnnotationDB db = new AnnotationDB();
							db.scanArchives(urls);
							Set<String> packageClassnames = db.getAnnotationIndex().get(BabPackage.class.getName());
							if (packageClassnames != null && packageClassnames.size() == 1) {
								Class<? extends IServiceExecutor> epPackageClass = (Class<? extends IServiceExecutor>) Class
										.forName(packageClassnames.iterator().next(), true, cl);
								String epJarPath = jar.getAbsolutePath();
								BabPackage epBabPackage = epPackageClass.getAnnotation(BabPackage.class);
								BabWeb.log(this)
										.info("Package found: " + epBabPackage.name() + ", loading services ... ");
								Set<String> serviceClassnames = db.getAnnotationIndex().get(BabService.class.getName());
								if (serviceClassnames != null) {
									for (String ec : serviceClassnames) {
										Class<?> uc = Class.forName(ec, true, cl);
										Method[] methods = uc.getMethods();
										if (methods.length > 0) {
											for (Method um : methods) {
												BabService epBabService = um.getAnnotation(BabService.class);
												if (epBabService != null) {
													ServiceEndpoint ep = new ServiceEndpoint(epJarPath, epPackageClass,
															epBabPackage, epBabService);
													if (!services.containsKey(epBabPackage.name())) {
														services.put(epBabPackage.name(),
																new TreeMap<String, ServiceEndpoint>());
													}
													services.get(epBabPackage.name()).put(epBabService.name(), ep);
													BabWeb.log(this)
															.info("Service Registered \"" + ep.toString() + "\"");
												}
											}
										}
									}
								}
							} else {
								BabWeb.log(this).error("Ambiguous package identifier on " + jar.getAbsolutePath());
							}
						}
					}
				}
			} catch (Exception ex) {
				BabWeb.log(this).error(ex.getMessage());
			}
		}
		return services;
	}

	@Override
	public Map<String, Map<String, Set<ServiceEndpoint>>> getJars(int reload) {
		if (jars == null) {
			jars = new LinkedHashMap<String, Map<String, Set<ServiceEndpoint>>>();
			reload = 1;
		}
		if (reload == 1) {
			jars.clear();
			Map<String, Map<String, ServiceEndpoint>> registeredServices = getServices(0);
			for (String packageId : registeredServices.keySet()) {
				for (String serviceId : registeredServices.get(packageId).keySet()) {
					ServiceEndpoint service = registeredServices.get(packageId).get(serviceId);
					File jar = new File(service.getJarPath());
					if (!jars.containsKey(jar.getName())) {
						jars.put(jar.getName(), new LinkedHashMap<String, Set<ServiceEndpoint>>());
					}
					if (!jars.get(jar.getName()).containsKey(packageId)) {
						jars.get(jar.getName()).put(packageId, new LinkedHashSet<ServiceEndpoint>());
					}
					jars.get(jar.getName()).get(packageId).add(service);
				}
			}
		}
		return jars;
	}

	public String getSessionId() {
		return sessionId;
	}

	public int getMaxSimultaneousRun() {
		return maxSimultaneousRun;
	}

	public void setMaxSimultaneousRun(int maxSimultaneousRun) {
		this.maxSimultaneousRun = maxSimultaneousRun;
	}

	public Map<String, Job> getAll() {
		return all;
	}

	public Map<String, Job> getQueue() {
		if (queue == null) {
			queue = new LinkedHashMap<String, Job>();
		}
		return queue;
	}

	public Map<String, Job> getRunning() {
		if (running == null) {
			running = new LinkedHashMap<String, Job>();
		}
		return running;
	}

	public Map<String, Job> getCompleted() {
		if (completed == null) {
			completed = new LinkedHashMap<String, Job>();
		}
		return completed;
	}

	public Job register(Job job) {
		if (job == null) {
			return null;
		}
		if (hash.containsKey(job.getHash())) {
			return hash.get(job.getHash());
		}
		if (job.getId() == null || job.getId().equals("")) {
			String id = sessionId + "-" + String.format("%04d", all.size());
			job.setId(id);
		}
		all.put(job.getId(), job);
		hash.put(job.getHash(), job);
		return job;
	}

	public Job submit(Job job) {
		if (job.getId() == null || job.getId().length() < 1) {
			job = register(job);
		}
		if (!getQueue().containsKey(job.getId()) && !getRunning().containsKey(job.getId())) {
			getQueue().put(job.getId(), job);
		}
		return job;
	}

	@Scheduled(fixedDelay = 5000)
	public Job pullAndRun() {
		for (String jobId : getRunning().keySet()) {
			Job job = getJobExecutor().status(getRunning().get(jobId));
			if (!job.isRunning()) {
				getRunning().remove(jobId);
				getCompleted().put(jobId, job);
			}
		}
		if (!getJobExecutor().isBusy() && getRunning().size() < maxSimultaneousRun) {
			if (getQueue().size() > 0) {
				Job job = getQueue().values().iterator().next();
				getQueue().remove(job.getId());
				job.setStarted(new Date().getTime());
				job.setStatus(Job.STATUS_RUNNING);
				job.setRemarks("");
				start(job.getId());
				return job;
			}
		}
		return null;
	}

	public Job start(String jobId) {
		if (all.containsKey(jobId)) {
			Job job = all.get(jobId);
			getJobExecutor().run(job);
			getRunning().put(job.getId(), job);
			return job;
		}
		return null;
	}

	public Job update(String jobId, String status) {
		if (all.containsKey(jobId)) {
			Job job = all.get(jobId);
			String time = DateUtil.getInstance().toDateTimeString(new Date().getTime());
			job.setRemarks(time + " : " + status);
			return job;
		}
		return null;
	}

	protected void complete(String jobId, String status) {
		complete(jobId, status, "");
	}

	protected Job complete(String jobId, String status, String remarks) {
		if (all.containsKey(jobId)) {
			Job job = all.get(jobId);
			job.setFinished(new Date().getTime());
			job.setRemarks(remarks);
			if (status != null) {
				job.setStatus(status);
			}
			getJobExecutor().kill(job);
			getCompleted().put(job.getId(), job);
			return job;
		}
		return null;
	}

	public Job finish(String jobId, String status, String remarks) {
		if (getRunning().containsKey(jobId)) {
			getRunning().remove(jobId);
			if (remarks == null) {
				remarks = "";
			}
			if (status == null) {
				status = Job.STATUS_COMPLETED;
			}
			return complete(jobId, status, remarks);
		}
		return null;
	}

	public Job status(String jobId) {
		if (getAll().containsKey(jobId)) {
			return getJobExecutor().status(getAll().get(jobId));
		}
		return null;
	}

	public Job kill(String jobId, String remarks) {
		if (getRunning().containsKey(jobId)) {
			getRunning().remove(jobId);
			if (remarks == null) {
				remarks = "";
			}
			return complete(jobId, Job.STATUS_KILLED, remarks);
		}
		return null;
	}

	public Job cancel(String jobId, String remarks) {
		if (getQueue().containsKey(jobId)) {
			Job job = getQueue().get(jobId);
			job.setStatus(Job.STATUS_CANCELED);
			if (remarks == null) {
				remarks = "";
			}
			job.setRemarks(remarks);
			getQueue().remove(jobId);
			getCompleted().put(job.getId(), job);
			return job;
		}
		return null;
	}

}
