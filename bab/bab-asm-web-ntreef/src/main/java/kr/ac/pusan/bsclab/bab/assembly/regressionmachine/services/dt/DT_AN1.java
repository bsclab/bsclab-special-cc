package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;

public class DT_AN1 implements DT_base {

@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10169")) return 2878.88333333333;
if(p.equals("AN10240")) return 2395.68333333333;
if(p.equals("AN10244"))
 if(t <= 6.5)
  if(we <= 24750) return 3330.65;
  if(we > 24750) return 3370.8;
 if(t > 6.5)
  if(we <= 24750) return 2454.33333333333;
  if(we > 24750) return 2824.23333333333;
if(p.equals("AN10245"))
 if(t <= 7)
  if(w <= 425)
   if(we <= 7864)
    if(t <= 4.85)
     if(t <= 4.4) return 2955.68333333333;
     if(t > 4.4)
      if(w <= 371) return 2881.11666666667;
      if(w > 371)
       if(t <= 4.6) return 3566;
       if(t > 4.6)
        if(we <= 7500) return 3201.11666666667;
        if(we > 7500) return 3149.76666666667;
    if(t > 4.85)
     if(w <= 421) return 2802.16666666667;
     if(w > 421) return 2751.61666666667;
   if(we > 7864)
    if(we <= 8310)
     if(w <= 421)
      if(w <= 419) return 3173.46666666667;
      if(w > 419)
       if(we <= 8155)
        if(we <= 7947) return 2734.75;
        if(we > 7947) return 2962.61666666667;
       if(we > 8155)
        if(we <= 8206) return 3028.36666666667;
        if(we > 8206) return 2960.21666666667;
     if(w > 421)
      if(w <= 422) return 2880;
      if(w > 422) return 3149.76666666667;
    if(we > 8310)
     if(we <= 8445)
      if(we <= 8414) return 3566;
      if(we > 8414) return 3116.2;
     if(we > 8445)
      if(w <= 421) return 3310.43333333333;
      if(w > 421) return 3566;
  if(w > 425)
   if(we <= 9008)
    if(t <= 5.7)
     if(t <= 5.12) return 2975.8;
     if(t > 5.12) return 2947.1;
    if(t > 5.7)
     if(t <= 5.8) return 2701.05;
     if(t > 5.8) return 3173.46666666667;
   if(we > 9008)
    if(we <= 9053) return 2876.38333333333;
    if(we > 9053)
     if(w <= 452) return 2866.9;
     if(w > 452) return 3230.28333333333;
 if(t > 7)
  if(w <= 650)
   if(w <= 430)
    if(we <= 8150)
     if(we <= 7864)
      if(w <= 375)
       if(w <= 371) return 3089.33333333333;
       if(w > 371) return 3505.85;
      if(w > 375)
       if(we <= 7662)
        if(w <= 381) return 2969.68333333333;
        if(w > 381)
         if(w <= 400) return 2885.9;
         if(w > 400) return 3209.98333333333;
       if(we > 7662)
        if(w <= 402) return 3505.85;
        if(w > 402) return 2969.68333333333;
     if(we > 7864)
      if(w <= 419) return 4248.63333333333;
      if(w > 419)
       if(w <= 421) return 2923.28333333333;
       if(w > 421) return 2833.85;
    if(we > 8150)
     if(we <= 8310)
      if(w <= 421)
       if(we <= 8206) return 3241.96666666667;
       if(we > 8206) return 2647.53333333333;
      if(w > 421) return 3179.68333333333;
     if(we > 8310)
      if(we <= 8414) return 3505.85;
      if(we > 8414) return 3153.28333333333;
   if(w > 430)
    if(w <= 440)
     if(w <= 436)
      if(we <= 8326) return 2969.68333333333;
      if(we > 8326) return 3184.1;
     if(w > 436) return 2754.26666666667;
    if(w > 440)
     if(we <= 8780) return 2885.9;
     if(we > 8780)
      if(w <= 449) return 3946.8;
      if(w > 449)
       if(w <= 452) return 3209.98333333333;
       if(w > 452) return 3946.8;
  if(w > 650)
   if(w <= 1249)
    if(we <= 24590)
     if(we <= 22490) return 2859.5;
     if(we > 22490) return 3218.88333333333;
    if(we > 24590) return 3363.98333333333;
   if(w > 1249)
    if(w <= 1275)
     if(we <= 24740)
      if(we <= 24300) return 2967.43333333333;
      if(we > 24300) return 2941.18333333333;
     if(we > 24740)
      if(we <= 24790) return 3049.95;
      if(we > 24790) return 2932.5;
    if(w > 1275)
     if(we <= 24680)
      if(we <= 24587) return 3243.68333333333;
      if(we > 24587) return 2327;
     if(we > 24680)
      if(we <= 24790) return 2951.7;
      if(we > 24790) return 2924;
if(p.equals("AN10252"))
 if(t <= 6.5)
  if(t <= 4.35) return 3252.31666666667;
  if(t > 4.35) return 3169.98333333333;
 if(t > 6.5)
  if(w <= 650) return 3946.8;
  if(w > 650) return 3363.98333333333;
if(p.equals("AN10258")) return 3579.3;
if(p.equals("AN10259")) return 2587.76666666667;
if(p.equals("AN10260")) return 2955.68333333333;
if(p.equals("AN10261")) return 3374.6;
if(p.equals("AN10262")) return 2969.68333333333;
if(p.equals("AN10263")) return 3109.21666666667;
if(p.equals("AN10264")) return 3218.88333333333;
if(p.equals("AN10265")) return 2859.5;
if(p.equals("PP11009"))
 if(w <= 340) return 2980.88333333333;
 if(w > 340)
  if(w <= 358) return 2337.98333333333;
  if(w > 358) return 2560.53333333333;
if(p.equals("PP11010"))
 if(t <= 4.6)
  if(t <= 3.3)
   if(t <= 3.1)
    if(t <= 2.52)
     if(t <= 2.23)
      if(t <= 1.92) return 2946.68333333333;
      if(t > 1.92) return 2880.71666666667;
     if(t > 2.23)
      if(t <= 2.49)
       if(w <= 407) return 3376.15;
       if(w > 407) return 2690.16666666667;
      if(t > 2.49) return 2879.56666666667;
    if(t > 2.52)
     if(w <= 281)
      if(t <= 3.01)
       if(t <= 2.76) return 2443.86666666667;
       if(t > 2.76) return 2706.25;
      if(t > 3.01)
       if(w <= 250) return 2218.55;
       if(w > 250) return 3324.91666666667;
     if(w > 281)
      if(t <= 2.69)
       if(we <= 5567) return 3604.68333333333;
       if(we > 5567) return 3516.55;
      if(t > 2.69)
       if(t <= 3) return 3021.18333333333;
       if(t > 3)
        if(w <= 328) return 2946.68333333333;
        if(w > 328) return 3385.7;
   if(t > 3.1)
    if(t <= 3.26)
     if(w <= 296) return 2690.16666666667;
     if(w > 296) return 3652.06666666667;
    if(t > 3.26)
     if(we <= 5076) return 3966.86666666667;
     if(we > 5076)
      if(we <= 6610) return 3652.06666666667;
      if(we > 6610) return 2927.2;
  if(t > 3.3)
   if(we <= 4655)
    if(we <= 3772)
     if(we <= 3553)
      if(w <= 238) return 3028.4;
      if(w > 238)
       if(w <= 260) return 3024.78333333333;
       if(w > 260) return 2827.83333333333;
     if(we > 3553) return 2500.21666666667;
    if(we > 3772)
     if(t <= 4.02)
      if(t <= 3.52) return 2256.36666666667;
      if(t > 3.52) return 2719;
     if(t > 4.02)
      if(t <= 4.25) return 3028.4;
      if(t > 4.25) return 2671.9;
   if(we > 4655)
    if(we <= 5997)
     if(t <= 3.95)
      if(w <= 367) return 2881.85;
      if(w > 367) return 2765.13333333333;
     if(t > 3.95)
      if(w <= 323)
       if(w <= 309) return 2545.76666666667;
       if(w > 309) return 3238;
      if(w > 323) return 2869.3;
    if(we > 5997)
     if(we <= 6269) return 2787;
     if(we > 6269)
      if(we <= 6512) return 2316.56666666667;
      if(we > 6512) return 2545.76666666667;
 if(t > 4.6)
  if(t <= 5.42)
   if(t <= 4.85) return 2218.18333333333;
   if(t > 4.85)
    if(w <= 312)
     if(we <= 3748)
      if(w <= 276) return 2545.76666666667;
      if(w > 276) return 2592;
     if(we > 3748) return 3164.95;
    if(w > 312)
     if(we <= 5280)
      if(we <= 4765) return 2519.96666666667;
      if(we > 4765) return 2851.15;
     if(we > 5280) return 3136.98333333333;
  if(t > 5.42)
   if(we <= 5269)
    if(we <= 4776)
     if(w <= 298)
      if(we <= 3485)
       if(w <= 238) return 2813.56666666667;
       if(w > 238) return 2784.03333333333;
      if(we > 3485)
       if(w <= 280) return 2554.91666666667;
       if(w > 280) return 2726.95;
     if(w > 298)
      if(w <= 323) return 2661.25;
      if(w > 323) return 2925.83333333333;
    if(we > 4776)
     if(w <= 347) return 2474;
     if(w > 347) return 2726.95;
   if(we > 5269)
    if(we <= 6090)
     if(w <= 381)
      if(we <= 5368) return 3026.58333333333;
      if(we > 5368) return 2649.8;
     if(w > 381)
      if(we <= 5612) return 2813.56666666667;
      if(we > 5612) return 2925.83333333333;
    if(we > 6090)
     if(we <= 6628)
      if(w <= 421) return 3425.46666666667;
      if(w > 421) return 2474;
     if(we > 6628) return 2785.23333333333;
if(p.equals("PP11012")) return 3028.4;
if(p.equals("PP11013")) return 2005;
if(p.equals("PP11015"))
 if(t <= 1.63) return 2854.38333333333;
 if(t > 1.63) return 2005;
if(p.equals("PP11017"))
 if(w <= 301) return 2545.76666666667;
 if(w > 301) return 2672.58333333333;
if(p.equals("PP11018")) return 2890.18333333333;
if(p.equals("PP11021"))
 if(t <= 4.01)
  if(t <= 2.78)
   if(t <= 2.49) return 2671.53333333333;
   if(t > 2.49)
    if(w <= 328) return 3007.98333333333;
    if(w > 328) return 3084.15;
  if(t > 2.78)
   if(w <= 271)
    if(we <= 3262) return 3022.71666666667;
    if(we > 3262)
     if(t <= 3.69) return 2697.33333333333;
     if(t > 3.69) return 3123.26666666667;
   if(w > 271)
    if(t <= 3.26) return 3251.25;
    if(t > 3.26)
     if(w <= 337) return 3807;
     if(w > 337) return 3041;
 if(t > 4.01)
  if(t <= 4.08)
   if(t <= 4.03)
    if(we <= 5080) return 2447.51666666667;
    if(we > 5080)
     if(w <= 348) return 3490.28333333333;
     if(w > 348) return 2956.46666666667;
   if(t > 4.03) return 3112.16666666667;
  if(t > 4.08)
   if(w <= 333) return 2465.08333333333;
   if(w > 333) return 3026.83333333333;
if(p.equals("PP11036"))
 if(t <= 1.7)
  if(t <= 1.38)
   if(w <= 312) return 3112.16666666667;
   if(w > 312) return 3852.91666666667;
  if(t > 1.38)
   if(t <= 1.48)
    if(we <= 3304) return 3293;
    if(we > 3304)
     if(we <= 3594) return 3189.93333333333;
     if(we > 3594) return 3503.96666666667;
   if(t > 1.48)
    if(t <= 1.53) return 2643.15;
    if(t > 1.53) return 3045.95;
 if(t > 1.7)
  if(t <= 2.45)
   if(t <= 2)
    if(w <= 312) return 2584.51666666667;
    if(w > 312) return 2600.5;
   if(t > 2)
    if(t <= 2.16) return 2500.15;
    if(t > 2.16) return 2545.76666666667;
  if(t > 2.45)
   if(t <= 2.9) return 2547.86666666667;
   if(t > 2.9) return 2366.91666666667;
if(p.equals("PP11037"))
 if(t <= 1.46)
  if(t <= 1.07)
   if(t <= 0.91)
    if(we <= 4640) return 3255.6;
    if(we > 4640)
     if(we <= 4662) return 3168.6;
     if(we > 4662) return 3255.6;
   if(t > 0.91)
    if(t <= 1.01)
     if(w <= 387)
      if(w <= 367) return 2904.86666666667;
      if(w > 367) return 3170.01666666667;
     if(w > 387) return 2580.6;
    if(t > 1.01) return 2688.36666666667;
  if(t > 1.07)
   if(we <= 3311)
    if(w <= 215) return 4133.78333333333;
    if(w > 215) return 3761;
   if(we > 3311)
    if(t <= 1.16)
     if(we <= 4265)
      if(w <= 263)
       if(w <= 262) return 3461.9;
       if(w > 262) return 2968.96666666667;
      if(w > 263) return 4029.75;
     if(we > 4265)
      if(w <= 319) return 3454.45;
      if(w > 319)
       if(w <= 321) return 2995.53333333333;
       if(w > 321) return 2898.93333333333;
    if(t > 1.16)
     if(t <= 1.26)
      if(w <= 236) return 3966.86666666667;
      if(w > 236)
       if(w <= 255) return 3454.45;
       if(w > 255) return 3121;
     if(t > 1.26)
      if(w <= 305) return 3293;
      if(w > 305) return 3022.71666666667;
 if(t > 1.46)
  if(t <= 1.9)
   if(w <= 331)
    if(we <= 4649) return 2658.65;
    if(we > 4649) return 2870;
   if(w > 331)
    if(w <= 384) return 2891.85;
    if(w > 384) return 2822.65;
  if(t > 1.9)
   if(w <= 263)
    if(w <= 236)
     if(w <= 215) return 3322.08333333333;
     if(w > 215)
      if(we <= 3594) return 3684.81666666667;
      if(we > 3594) return 2851.15;
    if(w > 236)
     if(w <= 261)
      if(w <= 254) return 2651.5;
      if(w > 254) return 2519.96666666667;
     if(w > 261)
      if(w <= 262)
       if(we <= 4014) return 3322.08333333333;
       if(we > 4014) return 2649.8;
      if(w > 262) return 2619;
   if(w > 263)
    if(w <= 352)
     if(we <= 4505)
      if(w <= 275) return 2851.15;
      if(w > 275) return 2613;
     if(we > 4505)
      if(w <= 321) return 2651.5;
      if(w > 321) return 2851.15;
    if(w > 352)
     if(w <= 385) return 2519.96666666667;
     if(w > 385) return 2358;
if(p.equals("PP11038"))
 if(t <= 1.86) return 3019;
 if(t > 1.86)
  if(t <= 2.7) return 3085.41666666667;
  if(t > 2.7) return 3021.66666666667;
if(p.equals("PP11039"))
 if(t <= 1.31)
  if(w <= 326)
   if(t <= 1)
    if(t <= 0.8) return 3032.88333333333;
    if(t > 0.8) return 3024.78333333333;
   if(t > 1)
    if(w <= 286) return 3140.65;
    if(w > 286) return 2976.36666666667;
  if(w > 326)
   if(w <= 351)
    if(t <= 1.03) return 3112.66666666667;
    if(t > 1.03) return 3371.51666666667;
   if(w > 351) return 3249.6;
 if(t > 1.31)
  if(t <= 1.7)
   if(t <= 1.38) return 3589.23333333333;
   if(t > 1.38) return 3434.41666666667;
  if(t > 1.7)
   if(we <= 5088)
    if(w <= 286) return 3040.06666666667;
    if(w > 286) return 2780;
   if(we > 5088)
    if(we <= 5774) return 2600.5;
    if(we > 5774) return 2826.75;
if(p.equals("PP11041"))
 if(w <= 324)
  if(t <= 1.03) return 2637.35;
  if(t > 1.03) return 2475.71666666667;
 if(w > 324)
  if(w <= 395)
   if(t <= 0.985) return 2775.88333333333;
   if(t > 0.985) return 2600.5;
  if(w > 395)
   if(t <= 1.12) return 2972.51666666667;
   if(t > 1.12) return 2697.33333333333;
if(p.equals("PP11045"))
 if(t <= 3.34) return 2366.91666666667;
 if(t > 3.34) return 2148.11666666667;
if(p.equals("PP11046"))
 if(t <= 2.15)
  if(we <= 3839)
   if(w <= 259) return 2673;
   if(w > 259) return 2266.05;
  if(we > 3839)
   if(w <= 353) return 3349;
   if(w > 353) return 2447.16666666667;
 if(t > 2.15)
  if(we <= 3772)
   if(we <= 2758) return 1933;
   if(we > 2758) return 1745.18333333333;
  if(we > 3772)
   if(w <= 394)
    if(we <= 3951)
     if(we <= 3873) return 2594.25;
     if(we > 3873) return 2808.26666666667;
    if(we > 3951)
     if(we <= 4087)
      if(we <= 4000) return 2387.51666666667;
      if(we > 4000) return 2889.63333333333;
     if(we > 4087) return 2985.31666666667;
   if(w > 394)
    if(we <= 6600) return 2268;
    if(we > 6600)
     if(we <= 6646) return 2808.26666666667;
     if(we > 6646)
      if(we <= 6802) return 2387.51666666667;
      if(we > 6802)
       if(we <= 6836) return 2889.63333333333;
       if(we > 6836) return 2387.51666666667;
if(p.equals("PP11047"))
 if(t <= 2.84) return 3085.41666666667;
 if(t > 2.84) return 3021.66666666667;
if(p.equals("PP11048"))
 if(t <= 1.7)
  if(w <= 288)
   if(t <= 1.38)
    if(we <= 3503)
     if(we <= 3144) return 2706.48333333333;
     if(we > 3144) return 3414.11666666667;
    if(we > 3503) return 2968.96666666667;
   if(t > 1.38) return 3759.51666666667;
  if(w > 288)
   if(t <= 1.33) return 2902.91666666667;
   if(t > 1.33)
    if(w <= 387) return 3157.83333333333;
    if(w > 387)
     if(w <= 389) return 2790;
     if(w > 389) return 3019;
 if(t > 1.7)
  if(w <= 387)
   if(we <= 3656)
    if(w <= 242) return 2569.98333333333;
    if(w > 242) return 2725.83333333333;
   if(we > 3656)
    if(w <= 286) return 2584.51666666667;
    if(w > 286) return 2673;
  if(w > 387)
   if(w <= 389) return 3244.66666666667;
   if(w > 389)
    if(w <= 398) return 2943;
    if(w > 398) return 2635.4;
if(p.equals("PP11050")) return 2740.56666666667;
if(p.equals("PP11052")) return 2165.45;
if(p.equals("PP11059")) return 2888.78333333333;
if(p.equals("PP11060")) return 2469.58333333333;
if(p.equals("PP11064"))
 if(t <= 3.74) return 2555.93333333333;
 if(t > 3.74) return 3488;
if(p.equals("PP11065"))
 if(we <= 22140)
  if(t <= 5.36)
   if(we <= 21660) return 3259.23333333333;
   if(we > 21660) return 3012.58333333333;
  if(t > 5.36) return 2849.18333333333;
 if(we > 22140)
  if(t <= 4.6) return 3370.4;
  if(t > 4.6) return 3441.4;
if(p.equals("PP11081"))
 if(t <= 1.73)
  if(t <= 1.41)
   if(w <= 255)
    if(t <= 1.2)
     if(t <= 0.99) return 3096.11666666667;
     if(t > 0.99)
      if(w <= 250)
       if(w <= 233) return 3320.01666666667;
       if(w > 233) return 3019;
      if(w > 250) return 3320.01666666667;
    if(t > 1.2)
     if(t <= 1.34) return 2671.53333333333;
     if(t > 1.34) return 3414.11666666667;
   if(w > 255)
    if(we <= 5063) return 3660;
    if(we > 5063)
     if(w <= 421)
      if(w <= 389) return 3320.01666666667;
      if(w > 389) return 3417;
     if(w > 421) return 3004.68333333333;
  if(t > 1.41)
   if(we <= 3634)
    if(w <= 250)
     if(w <= 233) return 2706.48333333333;
     if(w > 233) return 3085.41666666667;
    if(w > 250)
     if(w <= 255) return 3157.83333333333;
     if(w > 255) return 3019;
   if(we > 3634)
    if(w <= 390)
     if(w <= 305) return 3274.63333333333;
     if(w > 305) return 2702.13333333333;
    if(w > 390)
     if(w <= 421) return 3457.05;
     if(w > 421)
      if(we <= 5153) return 3004.68333333333;
      if(we > 5153) return 2702.13333333333;
 if(t > 1.73)
  if(t <= 2.02)
   if(w <= 255)
    if(we <= 3665)
     if(we <= 3171) return 2725.83333333333;
     if(we > 3171)
      if(w <= 251)
       if(we <= 3513) return 2758.85;
       if(we > 3513) return 2725.83333333333;
      if(w > 251) return 2673;
    if(we > 3665) return 3021.66666666667;
   if(w > 255)
    if(we <= 4978)
     if(w <= 265) return 2943;
     if(w > 265) return 2635.4;
    if(we > 4978)
     if(we <= 5488) return 2813.83333333333;
     if(we > 5488) return 3244.66666666667;
  if(t > 2.02)
   if(t <= 2.63) return 2811.46666666667;
   if(t > 2.63) return 3381.96666666667;
if(p.equals("PP11260"))
 if(t <= 2.84)
  if(t <= 2.57)
   if(t <= 2.37) return 3310.93333333333;
   if(t > 2.37) return 2849.41666666667;
  if(t > 2.57)
   if(w <= 1078) return 3120;
   if(w > 1078) return 2990.41666666667;
 if(t > 2.84)
  if(t <= 3.1)
   if(we <= 23410) return 2974.16666666667;
   if(we > 23410)
    if(we <= 23540) return 3062.1;
    if(we > 23540) return 3050;
  if(t > 3.1)
   if(t <= 4) return 3215.56666666667;
   if(t > 4) return 3000.81666666667;
if(p.equals("PP11261"))
 if(t <= 3.52)
  if(t <= 1.82)
   if(t <= 1.11) return 3342.91666666667;
   if(t > 1.11)
    if(we <= 20590) return 3332.21666666667;
    if(we > 20590) return 3364.48333333333;
  if(t > 1.82)
   if(we <= 20290) return 3565.06666666667;
   if(we > 20290) return 3256.93333333333;
 if(t > 3.52)
  if(we <= 20470)
   if(we <= 19980)
    if(we <= 19590)
     if(we <= 19190) return 3330.56666666667;
     if(we > 19190) return 3615.06666666667;
    if(we > 19590) return 3588.58333333333;
   if(we > 19980)
    if(we <= 20240)
     if(we <= 20100) return 3259.53333333333;
     if(we > 20100) return 3341.18333333333;
    if(we > 20240) return 3684.16666666667;
  if(we > 20470)
   if(we <= 21240)
    if(t <= 4.03)
     if(we <= 20860)
      if(we <= 20580) return 3648.15;
      if(we > 20580) return 3354.2;
     if(we > 20860) return 3276.58333333333;
    if(t > 4.03) return 3483.01666666667;
   if(we > 21240)
    if(we <= 21450) return 3432.78333333333;
    if(we > 21450)
     if(we <= 21550) return 3636;
     if(we > 21550) return 3795.43333333333;
if(p.equals("PP11264"))
 if(w <= 1184)
  if(t <= 2.9)
   if(t <= 2.31)
    if(t <= 1.61)
     if(w <= 1164)
      if(w <= 1045)
       if(t <= 1.21) return 2571;
       if(t > 1.21) return 2661.7;
      if(w > 1045)
       if(w <= 1131) return 3412.16666666667;
       if(w > 1131)
        if(we <= 20820) return 2863.98333333333;
        if(we > 20820) return 3072.81666666667;
     if(w > 1164)
      if(we <= 20150)
       if(we <= 19860) return 3040.6;
       if(we > 19860) return 3538.88333333333;
      if(we > 20150)
       if(we <= 20940) return 3680.2;
       if(we > 20940)
        if(we <= 22480) return 3046.58333333333;
        if(we > 22480)
         if(we <= 24460) return 3448.41666666667;
         if(we > 24460) return 3629.3;
    if(t > 1.61)
     if(w <= 1131)
      if(t <= 2.11)
       if(w <= 1112) return 3163.23333333333;
       if(w > 1112) return 2700.61666666667;
      if(t > 2.11)
       if(we <= 19860)
        if(we <= 19540) return 3245.16666666667;
        if(we > 19540) return 3617.26666666667;
       if(we > 19860)
        if(we <= 20950) return 3228.28333333333;
        if(we > 20950) return 3490.6;
     if(w > 1131)
      if(t <= 1.99)
       if(w <= 1174) return 3399.91666666667;
       if(w > 1174)
        if(we <= 19440) return 3085;
        if(we > 19440) return 2949.85;
      if(t > 1.99) return 3566.4;
   if(t > 2.31)
    if(t <= 2.51)
     if(t <= 2.4)
      if(we <= 20270)
       if(we <= 18600) return 2707.03333333333;
       if(we > 18600)
        if(we <= 19500) return 3692.98333333333;
        if(we > 19500) return 2814.2;
      if(we > 20270)
       if(we <= 21220) return 4555.1;
       if(we > 21220) return 3828.85;
     if(t > 2.4)
      if(we <= 23460)
       if(we <= 22930) return 3884.71666666667;
       if(we > 22930) return 3528.2;
      if(we > 23460)
       if(w <= 1142) return 3907;
       if(w > 1142) return 3742.8;
    if(t > 2.51)
     if(t <= 2.78)
      if(w <= 1130)
       if(t <= 2.63) return 3405.55;
       if(t > 2.63)
        if(we <= 22730) return 3555.88333333333;
        if(we > 22730) return 3640.3;
      if(w > 1130)
       if(we <= 20380) return 3269.41666666667;
       if(we > 20380) return 2868.25;
     if(t > 2.78)
      if(we <= 20520)
       if(we <= 20150) return 2998.18333333333;
       if(we > 20150) return 3067.41666666667;
      if(we > 20520)
       if(we <= 22480) return 3362;
       if(we > 22480)
        if(we <= 24460) return 4846.1;
        if(we > 24460) return 3511;
  if(t > 2.9)
   if(t <= 3.74)
    if(w <= 1107)
     if(w <= 1093)
      if(t <= 3.1)
       if(t <= 3.01)
        if(w <= 1045) return 3387.5;
        if(w > 1045) return 3515.41666666667;
       if(t > 3.01) return 3573.23333333333;
      if(t > 3.1)
       if(we <= 19220)
        if(we <= 18610) return 2586.86666666667;
        if(we > 18610) return 3713.15;
       if(we > 19220)
        if(we <= 19670) return 3908.11666666667;
        if(we > 19670) return 3366.56666666667;
     if(w > 1093)
      if(we <= 22600)
       if(we <= 22240) return 3448.46666666667;
       if(we > 22240)
        if(we <= 22400) return 3371.83333333333;
        if(we > 22400) return 3723.38333333333;
      if(we > 22600)
       if(we <= 22950)
        if(we <= 22870) return 3370;
        if(we > 22870) return 3442.31666666667;
       if(we > 22950)
        if(we <= 23010) return 3273.06666666667;
        if(we > 23010) return 3525.48333333333;
    if(w > 1107)
     if(t <= 3.01)
      if(w <= 1154)
       if(w <= 1135) return 3453.38333333333;
       if(w > 1135) return 2922.28333333333;
      if(w > 1154)
       if(w <= 1174) return 3674;
       if(w > 1174)
        if(we <= 19440) return 3201.11666666667;
        if(we > 19440) return 4241.18333333333;
     if(t > 3.01)
      if(t <= 3.34)
       if(w <= 1144) return 3833.43333333333;
       if(w > 1144) return 3407.38333333333;
      if(t > 3.34)
       if(we <= 20430)
        if(we <= 19680) return 3067.41666666667;
        if(we > 19680) return 2734.83333333333;
       if(we > 20430)
        if(we <= 21030) return 2868.25;
        if(we > 21030) return 3362;
   if(t > 3.74)
    if(w <= 1104)
     if(w <= 1093)
      if(t <= 4.6)
       if(w <= 1045)
        if(w <= 1020) return 3102.9;
        if(w > 1020) return 3182.18333333333;
       if(w > 1045)
        if(w <= 1062) return 3407.81666666667;
        if(w > 1062) return 3095.28333333333;
      if(t > 4.6)
       if(we <= 19410)
        if(we <= 19130) return 3163.28333333333;
        if(we > 19130) return 2807.83333333333;
       if(we > 19410)
        if(we <= 19780) return 2901.4;
        if(we > 19780) return 3348.68333333333;
     if(w > 1093)
      if(we <= 22600)
       if(we <= 22110) return 4823.95;
       if(we > 22110)
        if(we <= 22310) return 3281.18333333333;
        if(we > 22310)
         if(we <= 22400) return 3069.11666666667;
         if(we > 22400) return 3281.18333333333;
      if(we > 22600) return 3229;
    if(w > 1104)
     if(t <= 4.08)
      if(we <= 20270)
       if(we <= 18890)
        if(we <= 18210) return 3225.18333333333;
        if(we > 18210) return 2845.41666666667;
       if(we > 18890)
        if(we <= 20010) return 2726.58333333333;
        if(we > 20010) return 3323.21666666667;
      if(we > 20270)
       if(we <= 21220)
        if(we <= 20980) return 3715.7;
        if(we > 20980) return 3009.66666666667;
       if(we > 21220) return 2829.56666666667;
     if(t > 4.08)
      if(we <= 23000)
       if(w <= 1126) return 3092.31666666667;
       if(w > 1126)
        if(w <= 1150) return 3728.55;
        if(w > 1150) return 2561.93333333333;
      if(we > 23000)
       if(we <= 23790)
        if(w <= 1126) return 3909.01666666667;
        if(w > 1126) return 3597.86666666667;
       if(we > 23790) return 3655.9;
 if(w > 1184)
  if(we <= 21730)
   if(t <= 2.11)
    if(we <= 17140)
     if(w <= 1199)
      if(we <= 12830)
       if(we <= 12370) return 3110.56666666667;
       if(we > 12370) return 3873;
      if(we > 12830) return 2843;
     if(w > 1199)
      if(w <= 1219)
       if(t <= 1.63) return 2928;
       if(t > 1.63) return 2606.1;
      if(w > 1219)
       if(w <= 1230) return 2926.28333333333;
       if(w > 1230) return 2985.86666666667;
    if(we > 17140)
     if(we <= 19940)
      if(t <= 1.77) return 3889.03333333333;
      if(t > 1.77)
       if(we <= 18140) return 2779.95;
       if(we > 18140) return 2955.11666666667;
     if(we > 19940)
      if(w <= 1224)
       if(t <= 1.93) return 4765.83333333333;
       if(t > 1.93) return 3038.5;
      if(w > 1224)
       if(w <= 1230)
        if(t <= 1.47)
         if(t <= 1.32) return 3040.6;
         if(t > 1.32) return 2867.71666666667;
        if(t > 1.47) return 3446.05;
       if(w > 1230)
        if(t <= 1.5) return 2908.05;
        if(t > 1.5) return 2910.4;
   if(t > 2.11)
    if(w <= 1219)
     if(w <= 1190)
      if(we <= 12830)
       if(we <= 12370) return 2655.83333333333;
       if(we > 12370) return 2310.2;
      if(we > 12830) return 2655.83333333333;
     if(w > 1190)
      if(we <= 19650)
       if(t <= 3.15)
        if(t <= 2.57) return 5933.46666666667;
        if(t > 2.57) return 3141.11666666667;
       if(t > 3.15)
        if(t <= 3.74) return 3102.9;
        if(t > 3.74) return 3548.65;
      if(we > 19650)
       if(t <= 3.34)
        if(t <= 2.94) return 3589.55;
        if(t > 2.94)
         if(w <= 1208) return 3650.51666666667;
         if(w > 1208) return 3539.06666666667;
       if(t > 3.34) return 2893.85;
    if(w > 1219)
     if(w <= 1224)
      if(t <= 3.74)
       if(t <= 3.52) return 2533.11666666667;
       if(t > 3.52) return 2926.28333333333;
      if(t > 3.74)
       if(t <= 4.92) return 2485;
       if(t > 4.92) return 2533.11666666667;
     if(w > 1224)
      if(t <= 2.45)
       if(t <= 2.24) return 3052.88333333333;
       if(t > 2.24) return 3440.51666666667;
      if(t > 2.45)
       if(we <= 19940) return 2779.95;
       if(we > 19940) return 3345.43333333333;
  if(we > 21730)
   if(t <= 3.6)
    if(w <= 1201)
     if(we <= 23330)
      if(we <= 23210)
       if(we <= 22400) return 3466.26666666667;
       if(we > 22400) return 3422;
      if(we > 23210)
       if(we <= 23240) return 3577.11666666667;
       if(we > 23240)
        if(we <= 23300) return 3944.41666666667;
        if(we > 23300) return 3725.8;
     if(we > 23330)
      if(we <= 24600)
       if(we <= 24010)
        if(we <= 23560) return 3832;
        if(we > 23560) return 3465.45;
       if(we > 24010)
        if(we <= 24460) return 3450.15;
        if(we > 24460) return 3383.83333333333;
      if(we > 24600)
       if(we <= 24840) return 3406.33333333333;
       if(we > 24840)
        if(we <= 24930) return 3521.6;
        if(we > 24930) return 3602.65;
    if(w > 1201)
     if(w <= 1212)
      if(we <= 24200)
       if(we <= 23880)
        if(we <= 23310) return 3335.65;
        if(we > 23310) return 3583.81666666667;
       if(we > 23880)
        if(we <= 24020) return 3406.33333333333;
        if(we > 24020) return 3486.16666666667;
      if(we > 24200)
       if(we <= 24950)
        if(we <= 24800) return 3590.55;
        if(we > 24800) return 3455.93333333333;
       if(we > 24950)
        if(we <= 25110) return 4105.4;
        if(we > 25110) return 3728.28333333333;
     if(w > 1212)
      if(t <= 2)
       if(t <= 1.36) return 2907.35;
       if(t > 1.36) return 3502.11666666667;
      if(t > 2) return 3125.83333333333;
   if(t > 3.6)
    if(t <= 4.08)
     if(we <= 23560)
      if(we <= 23210)
       if(we <= 22640)
        if(we <= 22560) return 3685.56666666667;
        if(we > 22560) return 3340.81666666667;
       if(we > 22640) return 3618.4;
      if(we > 23210)
       if(we <= 23260)
        if(we <= 23230) return 3685.73333333333;
        if(we > 23230) return 3891;
       if(we > 23260)
        if(we <= 23310) return 3854.03333333333;
        if(we > 23310) return 3617.86666666667;
     if(we > 23560)
      if(we <= 24530)
       if(we <= 24260)
        if(we <= 23890) return 3816.56666666667;
        if(we > 23890) return 4099.13333333333;
       if(we > 24260) return 4096.2;
      if(we > 24530)
       if(we <= 24840)
        if(we <= 24690) return 4323.86666666667;
        if(we > 24690) return 4141.68333333333;
       if(we > 24840) return 4434.86666666667;
    if(t > 4.08)
     if(we <= 24110)
      if(we <= 23960)
       if(we <= 23180)
        if(we <= 22700) return 3620.78333333333;
        if(we > 22700) return 3004.75;
       if(we > 23180)
        if(we <= 23440) return 3665.03333333333;
        if(we > 23440) return 3303;
      if(we > 23960)
       if(we <= 24050) return 3272.8;
       if(we > 24050) return 7590;
     if(we > 24110)
      if(we <= 24920)
       if(we <= 24800)
        if(we <= 24280) return 3900.1;
        if(we > 24280) return 3734.85;
       if(we > 24800) return 3858.35;
      if(we > 24920)
       if(we <= 25110)
        if(we <= 24980) return 3631.73333333333;
        if(we > 24980) return 3805.25;
       if(we > 25110) return 3534;
if(p.equals("PP11266"))
 if(t <= 2.84)
  if(w <= 1084)
   if(w <= 1016)
    if(w <= 980) return 3550.86666666667;
    if(w > 980)
     if(t <= 1.8)
      if(we <= 17770) return 3675.83333333333;
      if(we > 17770) return 2795.31666666667;
     if(t > 1.8)
      if(we <= 20110)
       if(we <= 19880) return 2717.86666666667;
       if(we > 19880) return 4017.96666666667;
      if(we > 20110)
       if(we <= 20210) return 2617.08333333333;
       if(we > 20210) return 2900.98333333333;
   if(w > 1016)
    if(t <= 2.3)
     if(we <= 21070)
      if(we <= 20980) return 3500.4;
      if(we > 20980) return 2413.96666666667;
     if(we > 21070)
      if(we <= 21220) return 2909.85;
      if(we > 21220) return 2454;
    if(t > 2.3)
     if(we <= 20430)
      if(we <= 19550) return 3021.03333333333;
      if(we > 19550) return 3349.28333333333;
     if(we > 20430)
      if(we <= 20840) return 3369.46666666667;
      if(we > 20840) return 3021.03333333333;
  if(w > 1084)
   if(w <= 1120)
    if(w <= 1104)
     if(we <= 15790)
      if(we <= 15470) return 2586.86666666667;
      if(we > 15470) return 2733.46666666667;
     if(we > 15790)
      if(we <= 15850) return 2585.78333333333;
      if(we > 15850) return 2648.76666666667;
    if(w > 1104)
     if(t <= 2.3)
      if(we <= 22210) return 2534.1;
      if(we > 22210)
       if(we <= 22500) return 3266;
       if(we > 22500) return 3043.33333333333;
     if(t > 2.3) return 3011;
   if(w > 1120)
    if(w <= 1134)
     if(we <= 16190) return 2703.46666666667;
     if(we > 16190) return 2511.23333333333;
    if(w > 1134)
     if(we <= 22500)
      if(we <= 21730)
       if(we <= 21390) return 3697.88333333333;
       if(we > 21390) return 3204.68333333333;
      if(we > 21730) return 3257.68333333333;
     if(we > 22500) return 8322.66666666667;
 if(t > 2.84)
  if(t <= 3.24)
   if(w <= 1078)
    if(w <= 1042)
     if(we <= 20760)
      if(we <= 20320) return 2647.86666666667;
      if(we > 20320) return 3164.15;
     if(we > 20760)
      if(we <= 20930)
       if(we <= 20810) return 2868.93333333333;
       if(we > 20810) return 3258.68333333333;
      if(we > 20930) return 2984.96666666667;
    if(w > 1042)
     if(we <= 21200)
      if(we <= 20300) return 2839;
      if(we > 20300)
       if(we <= 20430) return 3517;
       if(we > 20430) return 3482.6;
     if(we > 21200)
      if(we <= 22060) return 3706;
      if(we > 22060) return 3054.61666666667;
   if(w > 1078)
    if(w <= 1120)
     if(w <= 1100)
      if(we <= 21910) return 2820.48333333333;
      if(we > 21910) return 3424.46666666667;
     if(w > 1100)
      if(we <= 21400) return 3355.98333333333;
      if(we > 21400)
       if(we <= 21510) return 3179.96666666667;
       if(we > 21510) return 4913.23333333333;
    if(w > 1120)
     if(we <= 20810) return 4287.86666666667;
     if(we > 20810) return 3358.06666666667;
  if(t > 3.24)
   if(t <= 3.6)
    if(w <= 988)
     if(we <= 19670)
      if(we <= 18610) return 2681.68333333333;
      if(we > 18610) return 2416.01666666667;
     if(we > 19670)
      if(we <= 19930) return 3239.03333333333;
      if(we > 19930)
       if(we <= 19990) return 3200.95;
       if(we > 19990) return 3289.18333333333;
    if(w > 988)
     if(we <= 20600)
      if(we <= 18900) return 2355.56666666667;
      if(we > 18900) return 3114.41666666667;
     if(we > 20600)
      if(we <= 21110) return 2673.61666666667;
      if(we > 21110) return 3077.11666666667;
   if(t > 3.6)
    if(t <= 4.5)
     if(t <= 3.84) return 3255;
     if(t > 3.84)
      if(w <= 1036) return 2607.58333333333;
      if(w > 1036)
       if(w <= 1076) return 3459.76666666667;
       if(w > 1076) return 3809.36666666667;
    if(t > 4.5)
     if(we <= 21760) return 2959.65;
     if(we > 21760) return 2795.81666666667;
if(p.equals("PP11275"))
 if(t <= 2.35)
  if(we <= 14520) return 2638.71666666667;
  if(we > 14520) return 3151.58333333333;
 if(t > 2.35)
  if(we <= 14520) return 2714.55;
  if(we > 14520) return 2551.61666666667;
if(p.equals("PP11287")) {
 if(we <= 19990)
  if(we <= 18920)
   if(t <= 1.14) return 7610.6;
   if(t > 1.14) return 3038.5;
  if(we > 18920) return 2776.1;
 if(we > 19990)
  if(we <= 20480)
   if(w <= 1224) return 2536.51666666667;
   if(w > 1224) return 3899.71666666667;
  if(we > 20480)
   if(we <= 21320) return 3400.61666666667;
   if(we > 21320) return 2726.51666666667;
}
else {

	return getDuration2(t,w,we,p);
}
return 2523.58333333333;
}

public double getDuration2(double t, double w, double we, String p) {
	if(p.equals("PP11288"))
		 if(t <= 3.87) return 3154.18333333333;
		 if(t > 3.87) return 2608;
		if(p.equals("PP11325"))
		 if(t <= 6) return 3387.5;
		 if(t > 6) return 3101;
		if(p.equals("PP11351")) return 2087.93333333333;
		if(p.equals("PP11403"))
		 if(w <= 1072)
		  if(t <= 1) return 3253.03333333333;
		  if(t > 1) return 3727.33333333333;
		 if(w > 1072)
		  if(t <= 1.2) return 2913.61666666667;
		  if(t > 1.2) return 3399.81666666667;
		if(p.equals("PP11435")) return 3448;
		if(p.equals("PP11443")) return 2448.73333333333;
		if(p.equals("PP11539"))
		 if(t <= 2.06) return 3137.65;
		 if(t > 2.06) return 2870.53333333333;
		if(p.equals("PP11659")) return 3372;
		if(p.equals("PP11671")) return 3039.63333333333;
		if(p.equals("PP11683"))
		 if(t <= 0.41)
		  if(we <= 16230)
		   if(we <= 15970) return 2849.41666666667;
		   if(we > 15970)
		    if(we <= 16080) return 4376.7;
		    if(we > 16080) return 3252.63333333333;
		  if(we > 16230)
		   if(we <= 20300) return 3487.21666666667;
		   if(we > 20300) return 3217.38333333333;
		 if(t > 0.41)
		  if(we <= 16130)
		   if(we <= 15600) return 2577.51666666667;
		   if(we > 15600) return 3065.15;
		  if(we > 16130)
		   if(we <= 17760) return 2977.93333333333;
		   if(we > 17760) return 2946.95;
		if(p.equals("PP11686")) return 3039.63333333333;
		if(p.equals("PP11880")) return 3372;
		if(p.equals("PP11884"))
		 if(t <= 2.75)
		  if(t <= 1.85) return 2627.8;
		  if(t > 1.85)
		   if(we <= 5584) return 2531.05;
		   if(we > 5584) return 3263.4;
		 if(t > 2.75)
		  if(we <= 5740) return 3263.15;
		  if(we > 5740) return 2818;
		if(p.equals("PP11891"))
		 if(we <= 3376) return 2758.85;
		 if(we > 3376)
		  if(w <= 350) return 3429.15;
		  if(w > 350) return 2787.15;
		if(p.equals("PP11892")) return 2107;
		if(p.equals("PP11894")) return 3285.66666666667;
		if(p.equals("PP11910")) return 2274.58333333333;
		if(p.equals("PP11912"))
		 if(t <= 3)
		  if(w <= 1090) return 3777.15;
		  if(w > 1090)
		   if(t <= 2.45) return 3020;
		   if(t > 2.45) return 3994.48333333333;
		 if(t > 3) return 3838.58333333333;
		if(p.equals("PP11917")) return 3157.78333333333;
		if(p.equals("PP11931"))
		 if(t <= 2.8)
		  if(t <= 2)
		   if(w <= 1105)
		    if(we <= 16900)
		     if(we <= 16600) return 2766.53333333333;
		     if(we > 16600) return 2718.81666666667;
		    if(we > 16900)
		     if(we <= 17020) return 2645.08333333333;
		     if(we > 17020) return 3327.2;
		   if(w > 1105)
		    if(we <= 17710)
		     if(we <= 17260)
		      if(we <= 14900) return 2531.65;
		      if(we > 14900) return 2961;
		     if(we > 17260)
		      if(we <= 17600) return 4609.93333333333;
		      if(we > 17600) return 3050.85;
		    if(we > 17710)
		     if(we <= 18080)
		      if(we <= 17830) return 6875.7;
		      if(we > 17830)
		       if(we <= 17950) return 3301.98333333333;
		       if(we > 17950) return 2531.65;
		     if(we > 18080)
		      if(t <= 1.75) return 3091.36666666667;
		      if(t > 1.75) return 3258.31666666667;
		  if(t > 2)
		   if(we <= 17010)
		    if(we <= 16350)
		     if(we <= 14720)
		      if(we <= 14470)
		       if(we <= 14270) return 2890.33333333333;
		       if(we > 14270) return 2726.45;
		      if(we > 14470) return 2626.6;
		     if(we > 14720)
		      if(t <= 2.2)
		       if(we <= 15130) return 2799.18333333333;
		       if(we > 15130) return 5505.48333333333;
		      if(t > 2.2) return 2538.05;
		    if(we > 16350)
		     if(w <= 1087)
		      if(t <= 2.35) return 2718.81666666667;
		      if(t > 2.35) return 2773.88333333333;
		     if(w > 1087)
		      if(we <= 16750)
		       if(we <= 16540)
		        if(we <= 16450) return 2664.65;
		        if(we > 16450) return 2610.35;
		       if(we > 16540)
		        if(we <= 16580) return 2839.16666666667;
		        if(we > 16580) return 2506;
		      if(we > 16750)
		       if(we <= 16960) return 2576.33333333333;
		       if(we > 16960) return 2664.65;
		   if(we > 17010)
		    if(we <= 17170)
		     if(we <= 17090)
		      if(we <= 17050) return 2991.61666666667;
		      if(we > 17050) return 2619.11666666667;
		     if(we > 17090)
		      if(we <= 17135) return 2962.31666666667;
		      if(we > 17135) return 2739.51666666667;
		    if(we > 17170)
		     if(t <= 2.2)
		      if(we <= 17290)
		       if(we <= 17250) return 2538.05;
		       if(we > 17250) return 2763.46666666667;
		      if(we > 17290)
		       if(we <= 22600) return 2850.31666666667;
		       if(we > 22600) return 3248.06666666667;
		     if(t > 2.2) return 3030.78333333333;
		 if(t > 2.8)
		  if(w <= 1126)
		   if(t <= 3.3)
		    if(we <= 16850)
		     if(we <= 16310) return 2842.78333333333;
		     if(we > 16310) return 2785;
		    if(we > 16850)
		     if(we <= 17000) return 2710.63333333333;
		     if(we > 17000)
		      if(we <= 17050) return 2526.78333333333;
		      if(we > 17050) return 2888.75;
		   if(t > 3.3)
		    if(we <= 16580)
		     if(we <= 14830)
		      if(we <= 14470)
		       if(we <= 14270) return 2786.78333333333;
		       if(we > 14270) return 2489.01666666667;
		      if(we > 14470)
		       if(we <= 14620) return 2330;
		       if(we > 14620) return 2484;
		     if(we > 14830)
		      if(we <= 16330)
		       if(we <= 15130) return 2348.16666666667;
		       if(we > 15130) return 2896.58333333333;
		      if(we > 16330)
		       if(we <= 16520) return 2780.85;
		       if(we > 16520) return 2782.23333333333;
		    if(we > 16580)
		     if(we <= 17140)
		      if(we <= 17010)
		       if(we <= 16960) return 4658.51666666667;
		       if(we > 16960) return 2610.05;
		      if(we > 17010)
		       if(we <= 17070)
		        if(we <= 17040) return 4888.53333333333;
		        if(we > 17040) return 2832.98333333333;
		       if(we > 17070)
		        if(we <= 17100) return 2627.05;
		        if(we > 17100) return 2972.5;
		     if(we > 17140)
		      if(we <= 17260)
		       if(we <= 17180) return 2786.78333333333;
		       if(we > 17180) return 6904.3;
		      if(we > 17260)
		       if(we <= 19880) return 2780.85;
		       if(we > 19880)
		        if(we <= 22950) return 2749.45;
		        if(we > 22950) return 3152.31666666667;
		  if(w > 1126)
		   if(we <= 17600)
		    if(we <= 16320)
		     if(we <= 14900) return 2638.55;
		     if(we > 14900)
		      if(we <= 15100) return 2540;
		      if(we > 15100) return 2527.4;
		    if(we > 16320)
		     if(we <= 17520) return 3854.45;
		     if(we > 17520)
		      if(we <= 17560) return 2540;
		      if(we > 17560) return 2737.31666666667;
		   if(we > 17600)
		    if(we <= 18080)
		     if(we <= 17830) return 2731.76666666667;
		     if(we > 17830)
		      if(we <= 17950) return 2724.81666666667;
		      if(we > 17950) return 2779.41666666667;
		    if(we > 18080)
		     if(t <= 2.845) return 3636.95;
		     if(t > 2.845) return 3072.25;
		if(p.equals("PP11932"))
		 if(t <= 1.95)
		  if(w <= 1105)
		   if(t <= 1.57)
		    if(t <= 1.3)
		     if(we <= 15410) return 3141.43333333333;
		     if(we > 15410) return 2679.16666666667;
		    if(t > 1.3)
		     if(we <= 16870)
		      if(we <= 14450) return 2619.11666666667;
		      if(we > 14450) return 2833.9;
		     if(we > 16870) return 2529.06666666667;
		   if(t > 1.57)
		    if(we <= 16890)
		     if(we <= 15410) return 3013.93333333333;
		     if(we > 15410)
		      if(we <= 16220) return 2870.21666666667;
		      if(we > 16220) return 3414.88333333333;
		    if(we > 16890)
		     if(t <= 1.77)
		      if(we <= 16990) return 2679.16666666667;
		      if(we > 16990) return 2619.11666666667;
		     if(t > 1.77) return 2749.75;
		  if(w > 1105)
		   if(we <= 18010)
		    if(we <= 17420) return 2765.66666666667;
		    if(we > 17420) return 2835.71666666667;
		   if(we > 18010)
		    if(t <= 1.62) return 2918.56666666667;
		    if(t > 1.62) return 2721.33333333333;
		 if(t > 1.95)
		  if(t <= 3.3)
		   if(we <= 16890)
		    if(we <= 15200)
		     if(t <= 2.6) return 2837.85;
		     if(t > 2.6)
		      if(we <= 14450) return 2330;
		      if(we > 14450) return 2484;
		    if(we > 15200)
		     if(t <= 2.6) return 2513.73333333333;
		     if(t > 2.6)
		      if(we <= 15520) return 2833.45;
		      if(we > 15520) return 3423.31666666667;
		   if(we > 16890)
		    if(we <= 17000)
		     if(we <= 16940) return 2620.93333333333;
		     if(we > 16940)
		      if(we <= 16970) return 2484;
		      if(we > 16970) return 2934.58333333333;
		    if(we > 17000)
		     if(t <= 2.6) return 2703.46666666667;
		     if(t > 2.6)
		      if(we <= 17060) return 2552.96666666667;
		      if(we > 17060) return 2895.61666666667;
		  if(t > 3.3)
		   if(we <= 18010)
		    if(we <= 17420) return 3063.76666666667;
		    if(we > 17420) return 3058.2;
		   if(we > 18010)
		    if(we <= 18300) return 2854.08333333333;
		    if(we > 18300) return 3367.4;
		if(p.equals("PP11954"))
		 if(t <= 1.9)
		  if(we <= 16390) return 2456.98333333333;
		  if(we > 16390) return 2779.6;
		 if(t > 1.9)
		  if(w <= 1020) return 2610.76666666667;
		  if(w > 1020)
		   if(w <= 1049) return 2766.03333333333;
		   if(w > 1049) return 2749.45;
		if(p.equals("PP11982"))
		 if(we <= 12430)
		  if(t <= 2.14)
		   if(we <= 11364)
		    if(we <= 11103) return 2815.45;
		    if(we > 11103) return 2850;
		   if(we > 11364)
		    if(we <= 11566) return 3661.71666666667;
		    if(we > 11566) return 5146.76666666667;
		  if(t > 2.14)
		   if(we <= 12010)
		    if(we <= 11652) return 3031.65;
		    if(we > 11652)
		     if(we <= 11815) return 2952;
		     if(we > 11815) return 3415.31666666667;
		   if(we > 12010) return 2763.9;
		 if(we > 12430)
		  if(we <= 12915)
		   if(we <= 12730)
		    if(we <= 12627) return 3170.75;
		    if(we > 12627) return 2875.25;
		   if(we > 12730)
		    if(we <= 12865) return 2873.85;
		    if(we > 12865) return 9752.71666666667;
		  if(we > 12915)
		   if(we <= 13128)
		    if(we <= 12942) return 3685.73333333333;
		    if(we > 12942) return 3093.78333333333;
		   if(we > 13128)
		    if(we <= 13173) return 3888.98333333333;
		    if(we > 13173) return 3170.75;
		if(p.equals("PP11984"))
		 if(t <= 3.6)
		  if(we <= 4916) return 3489;
		  if(we > 4916) return 2822.65;
		 if(t > 3.6) return 2549.56666666667;
		if(p.equals("PP11985")) return 2787;
		if(p.equals("PP11988")) return 2666.33333333333;
		if(p.equals("PP11991")) return 10478.4833333333;
		if(p.equals("PP12002"))
		 if(t <= 2.35)
		  if(we <= 16720) return 2543.16666666667;
		  if(we > 16720) return 2749.75;
		 if(t > 2.35)
		  if(we <= 16720) return 3058.51666666667;
		  if(we > 16720) return 2739.4;
		if(p.equals("PP12011")) return 2919.93333333333;
		if(p.equals("PP12035"))
		 if(t <= 1.49)
		  if(t <= 0.99) return 3463.05;
		  if(t > 0.99) return 2895.93333333333;
		 if(t > 1.49)
		  if(t <= 2.4) return 3589.23333333333;
		  if(t > 2.4) return 3844.88333333333;
		if(p.equals("PP12037"))
		 if(w <= 350) return 2785.23333333333;
		 if(w > 350)
		  if(t <= 1.21) return 3553.75;
		  if(t > 1.21) return 3034;
		if(p.equals("PP12040")) return 2643.81666666667;
		if(p.equals("PP12047"))
		 if(t <= 1.9)
		  if(w <= 1114)
		   if(we <= 17000)
		    if(t <= 1.65)
		     if(we <= 16280) return 3193.28333333333;
		     if(we > 16280) return 7008.48333333333;
		    if(t > 1.65) return 2765.66666666667;
		   if(we > 17000)
		    if(we <= 17130)
		     if(we <= 17080) return 2766.53333333333;
		     if(we > 17080) return 2780;
		    if(we > 17130) return 2845.63333333333;
		  if(w > 1114)
		   if(t <= 0.81) return 3347.9;
		   if(t > 0.81) return 3253.03333333333;
		 if(t > 1.9)
		  if(t <= 2.6)
		   if(we <= 17610) return 3348;
		   if(we > 17610) return 2850;
		  if(t > 2.6)
		   if(we <= 17000)
		    if(we <= 16530) return 2910.68333333333;
		    if(we > 16530)
		     if(we <= 16840) return 3011.91666666667;
		     if(we > 16840) return 2523.91666666667;
		   if(we > 17000)
		    if(we <= 17150) return 2842.78333333333;
		    if(we > 17150) return 4028.05;
		if(p.equals("PP12069")) return 3315.2;
		if(p.equals("PP12073"))
		 if(t <= 4.6) return 2655.03333333333;
		 if(t > 4.6)
		  if(we <= 5192) return 2301.11666666667;
		  if(we > 5192) return 2225.41666666667;
		if(p.equals("PP12087")) return 3174.11666666667;
		if(p.equals("PP12129")) return 2955.28333333333;
		if(p.equals("PP12141"))
		 if(t <= 1.31) return 2866.13333333333;
		 if(t > 1.31) return 2435.58333333333;
		if(p.equals("PP12152")) return 3162.36666666667;
		if(p.equals("PP12158"))
		 if(t <= 2.94)
		  if(w <= 322)
		   if(t <= 2.1) return 3429.15;
		   if(t > 2.1) return 3239.81666666667;
		  if(w > 322) return 2996.43333333333;
		 if(t > 2.94)
		  if(w <= 322) return 2643.81666666667;
		  if(w > 322) return 2582.93333333333;
		if(p.equals("PP12164")) return 2447.16666666667;
		if(p.equals("PP12178"))
		 if(w <= 347)
		  if(w <= 331) return 3526.95;
		  if(w > 331) return 2719;
		 if(w > 347)
		  if(we <= 5359)
		   if(w <= 356) return 2627;
		   if(w > 356) return 2549.56666666667;
		  if(we > 5359)
		   if(we <= 5434) return 2640.5;
		   if(we > 5434) return 2558.51666666667;
		if(p.equals("PP12183")) return 3092.33333333333;
		if(p.equals("PP12184"))
		 if(w <= 328)
		  if(t <= 2.15)
		   if(w <= 307)
		    if(w <= 304) return 2833;
		    if(w > 304)
		     if(we <= 4314) return 3210.85;
		     if(we > 4314) return 2856.2;
		   if(w > 307)
		    if(we <= 2851) return 3429.15;
		    if(we > 2851) return 2920.83333333333;
		  if(t > 2.15) return 2659.66666666667;
		 if(w > 328)
		  if(w <= 343)
		   if(w <= 342)
		    if(w <= 337) return 2777.98333333333;
		    if(w > 337) return 3588.95;
		   if(w > 342) return 2856.2;
		  if(w > 343)
		   if(w <= 357) return 3240.93333333333;
		   if(w > 357)
		    if(we <= 5047) return 2721.75;
		    if(we > 5047) return 3101.65;
		if(p.equals("PP12186"))
		 if(w <= 317)
		  if(w <= 306)
		   if(w <= 263) return 3240.93333333333;
		   if(w > 263) return 2744.93333333333;
		  if(w > 306)
		   if(w <= 310) return 2856.2;
		   if(w > 310)
		    if(we <= 4390) return 2869.38333333333;
		    if(we > 4390) return 2744.93333333333;
		 if(w > 317)
		  if(we <= 4529)
		   if(w <= 339) return 2796.1;
		   if(w > 339) return 2866.68333333333;
		  if(we > 4529)
		   if(w <= 334) return 3092.31666666667;
		   if(w > 334) return 2700.31666666667;
		if(p.equals("PP12187")) return 2850;
		if(p.equals("PP12188"))
		 if(w <= 322) return 2903.73333333333;
		 if(w > 322) return 2625.76666666667;
		if(p.equals("PP12190")) return 3276;
		if(p.equals("PP12192"))
		 if(w <= 315) return 2725.45;
		 if(w > 315) return 2700.31666666667;
		if(p.equals("PP12193"))
		 if(w <= 230)
		  if(w <= 220)
		   if(w <= 215) return 2936.61666666667;
		   if(w > 215) return 2858.11666666667;
		  if(w > 220)
		   if(we <= 3067)
		    if(we <= 2879) return 2721.75;
		    if(we > 2879) return 2725.45;
		   if(we > 3067) return 3101.65;
		 if(w > 230)
		  if(w <= 235)
		   if(w <= 233) return 2450.66666666667;
		   if(w > 233) return 3588.95;
		  if(w > 235)
		   if(w <= 286) return 3052.86666666667;
		   if(w > 286) return 2864.81666666667;
		if(p.equals("PP12194"))
		 if(we <= 4320)
		  if(we <= 3762) return 2925.23333333333;
		  if(we > 3762) return 3002.28333333333;
		 if(we > 4320)
		  if(w <= 315) return 3578.96666666667;
		  if(w > 315)
		   if(we <= 4491) return 2767;
		   if(we > 4491) return 3507;
		if(p.equals("PP12195"))
		 if(we <= 4431) return 3574.05;
		 if(we > 4431) return 2556.53333333333;
		if(p.equals("PP12196"))
		 if(t <= 2.3)
		  if(w <= 402) return 2812.76666666667;
		  if(w > 402)
		   if(we <= 5372)
		    if(we <= 4195) return 2362.7;
		    if(we > 4195) return 2597;
		   if(we > 5372) return 3378.21666666667;
		 if(t > 2.3) return 2484.51666666667;
		if(p.equals("PP12197"))
		 if(w <= 322) return 2636;
		 if(w > 322)
		  if(w <= 504) return 2699.11666666667;
		  if(w > 504) return 2636;
		if(p.equals("PP12198"))
		 if(t <= 2.63)
		  if(t <= 2.15)
		   if(w <= 338)
		    if(w <= 323)
		     if(we <= 4289) return 2864.81666666667;
		     if(we > 4289) return 2920.83333333333;
		    if(w > 323)
		     if(w <= 326) return 2450.66666666667;
		     if(w > 326) return 2850;
		   if(w > 338)
		    if(w <= 348) return 2755;
		    if(w > 348)
		     if(we <= 4870) return 3065.68333333333;
		     if(we > 4870)
		      if(we <= 4896) return 2858.11666666667;
		      if(we > 4896) return 2876.06666666667;
		  if(t > 2.15)
		   if(t <= 2.45)
		    if(we <= 4534) return 3261.45;
		    if(we > 4534) return 2721.75;
		   if(t > 2.45)
		    if(w <= 334)
		     if(we <= 4556) return 3219.61666666667;
		     if(we > 4556) return 2131.26666666667;
		    if(w > 334)
		     if(we <= 4439) return 2729.63333333333;
		     if(we > 4439)
		      if(w <= 358)
		       if(we <= 4582) return 2556;
		       if(we > 4582) return 2639.26666666667;
		      if(w > 358) return 2556;
		 if(t > 2.63)
		  if(t <= 2.84)
		   if(w <= 378)
		    if(w <= 358)
		     if(w <= 350)
		      if(w <= 341) return 1948.33333333333;
		      if(w > 341)
		       if(we <= 3329)
		        if(we <= 3182) return 3464;
		        if(we > 3182) return 3773.36666666667;
		       if(we > 3329)
		        if(we <= 3471)
		         if(we <= 3401) return 3010.16666666667;
		         if(we > 3401) return 3190.55;
		        if(we > 3471)
		         if(we <= 3527) return 2670.56666666667;
		         if(we > 3527) return 3130;
		     if(w > 350) return 2861.73333333333;
		    if(w > 358)
		     if(we <= 3633)
		      if(we <= 3570)
		       if(w <= 360)
		        if(we <= 3471) return 2827.83333333333;
		        if(we > 3471)
		         if(we <= 3515) return 2258.16666666667;
		         if(we > 3515) return 2973.6;
		       if(w > 360)
		        if(we <= 3414) return 2582.55;
		        if(we > 3414) return 2603.66666666667;
		      if(we > 3570) return 2596.78333333333;
		     if(we > 3633)
		      if(we <= 3704) return 2258.16666666667;
		      if(we > 3704)
		       if(w <= 369) return 2875.26666666667;
		       if(w > 369) return 2947.45;
		   if(w > 378)
		    if(w <= 386)
		     if(w <= 379) return 2677.36666666667;
		     if(w > 379)
		      if(we <= 3716) return 3185.38333333333;
		      if(we > 3716) return 2973.6;
		    if(w > 386)
		     if(w <= 393)
		      if(we <= 3810) return 3065.2;
		      if(we > 3810)
		       if(we <= 3908) return 3010.16666666667;
		       if(we > 3908) return 2625.76666666667;
		     if(w > 393)
		      if(we <= 3893) return 3773.36666666667;
		      if(we > 3893)
		       if(we <= 4000) return 3190.55;
		       if(we > 4000) return 2670.56666666667;
		  if(t > 2.84)
		   if(t <= 3.1)
		    if(we <= 4259)
		     if(w <= 349)
		      if(we <= 3641) return 2578.11666666667;
		      if(we > 3641) return 2668.46666666667;
		     if(w > 349)
		      if(w <= 386) return 3098.21666666667;
		      if(w > 386)
		       if(we <= 4146) return 2827.45;
		       if(we > 4146) return 2578.11666666667;
		    if(we > 4259)
		     if(we <= 4421) return 2837.6;
		     if(we > 4421) return 2888;
		   if(t > 3.1)
		    if(we <= 4424)
		     if(we <= 4138) return 2556.53333333333;
		     if(we > 4138) return 2670;
		    if(we > 4424)
		     if(t <= 3.34) return 2862.18333333333;
		     if(t > 3.34) return 2484.51666666667;
		if(p.equals("PP12199"))
		 if(t <= 2.9)
		  if(t <= 2.45) return 2850;
		  if(t > 2.45)
		   if(w <= 348) return 3104.3;
		   if(w > 348) return 2958.05;
		 if(t > 2.9)
		  if(w <= 367) return 2729.63333333333;
		  if(w > 367) return 2556.53333333333;
		if(p.equals("PP12201"))
		 if(we <= 3444) return 3137.03333333333;
		 if(we > 3444) return 3295.33333333333;
		if(p.equals("PP12202"))
		 if(t <= 2.9) return 2131.26666666667;
		 if(t > 2.9)
		  if(w <= 268) return 2965;
		  if(w > 268) return 2777.98333333333;
		if(p.equals("PP12206"))
		 if(w <= 288) return 2556.53333333333;
		 if(w > 288) return 2777.98333333333;
		if(p.equals("PP12213")) return 2865.26666666667;
		if(p.equals("PP12221"))
		 if(w <= 317) return 3137.03333333333;
		 if(w > 317) return 2598.18333333333;
		if(p.equals("PP12222")) return 2642.85;
		if(p.equals("PP12226")) return 2635.4;
		if(p.equals("PP12227")) return 2721.75;
		if(p.equals("PP12228")) return 3142.86666666667;
		if(p.equals("PP12229"))
		 if(we <= 4259) return 2577.28333333333;
		 if(we > 4259)
		  if(w <= 335) return 2796.1;
		  if(w > 335)
		   if(we <= 4746) return 2837.18333333333;
		   if(we > 4746) return 2965;
		if(p.equals("PP12231"))
		 if(t <= 1.77) return 2503.9;
		 if(t > 1.77) return 3132.66666666667;
		if(p.equals("PP12233")) return 2563;
		if(p.equals("PP12234"))
		 if(w <= 504) return 2792.83333333333;
		 if(w > 504) return 2864.81666666667;
		if(p.equals("PP12235")) return 2864.81666666667;
		if(p.equals("PP12237")) return 2556.53333333333;
		if(p.equals("PP12238"))
		 if(w <= 343) return 2862.18333333333;
		 if(w > 343)
		  if(w <= 359) return 2872.23333333333;
		  if(w > 359) return 2777.98333333333;
		if(p.equals("PP12239"))
		 if(w <= 233)
		  if(we <= 3164) return 3162.58333333333;
		  if(we > 3164) return 2939;
		 if(w > 233)
		  if(we <= 3593) return 2876;
		  if(we > 3593) return 3242.66666666667;
		if(p.equals("PP12245"))
		 if(w <= 315) return 2725.45;
		 if(w > 315) return 2598.18333333333;
		if(p.equals("PP12247")) return 2777.98333333333;
		if(p.equals("PP12248"))
		 if(w <= 314)
		  if(we <= 4337) return 2755;
		  if(we > 4337) return 3507;
		 if(w > 314)
		  if(we <= 4111) return 2577.28333333333;
		  if(we > 4111) return 2837.18333333333;
		if(p.equals("PP12249"))
		 if(w <= 222) return 3280;
		 if(w > 222)
		  if(t <= 2.45) return 2850;
		  if(t > 2.45) return 2639.26666666667;
		if(p.equals("PP12251"))
		 if(w <= 349) return 3578.96666666667;
		 if(w > 349)
		  if(w <= 355) return 2725.45;
		  if(w > 355) return 3130;
		if(p.equals("PP12252"))
		 if(w <= 482) return 2970.23333333333;
		 if(w > 482) return 2916.68333333333;
		if(p.equals("PP12253"))
		 if(w <= 539)
		  if(w <= 495) return 2801.78333333333;
		  if(w > 495)
		   if(we <= 7005) return 2577.28333333333;
		   if(we > 7005) return 2820.25;
		 if(w > 539) return 2755;
		if(p.equals("PP12254")) return 3461.48333333333;
		if(p.equals("PP12256"))
		 if(w <= 313)
		  if(w <= 306) return 2869.38333333333;
		  if(w > 306)
		   if(w <= 309) return 2744.93333333333;
		   if(w > 309) return 2780.63333333333;
		 if(w > 313)
		  if(w <= 322) return 2450.66666666667;
		  if(w > 322)
		   if(w <= 330) return 2700.31666666667;
		   if(w > 330) return 2833;
		if(p.equals("PP12257"))
		 if(w <= 238)
		  if(w <= 233)
		   if(w <= 209)
		    if(w <= 208)
		     if(t <= 2.84) return 2582.55;
		     if(t > 2.84) return 3098.21666666667;
		    if(w > 208)
		     if(we <= 2037)
		      if(we <= 2015) return 2691;
		      if(we > 2015) return 3185.38333333333;
		     if(we > 2037)
		      if(we <= 2080) return 3010.16666666667;
		      if(we > 2080)
		       if(we <= 2137) return 2625.76666666667;
		       if(we > 2137) return 2973.6;
		   if(w > 209)
		    if(w <= 219)
		     if(w <= 214)
		      if(we <= 2083)
		       if(we <= 2051) return 3773.36666666667;
		       if(we > 2051) return 3037;
		      if(we > 2083)
		       if(we <= 2144)
		        if(we <= 2104) return 2596.78333333333;
		        if(we > 2104) return 3190.55;
		       if(we > 2144)
		        if(we <= 2155) return 2824.96666666667;
		        if(we > 2155) return 2670.56666666667;
		     if(w > 214)
		      if(t <= 2.63) return 2767;
		      if(t > 2.63) return 2827.83333333333;
		    if(w > 219)
		     if(w <= 232)
		      if(w <= 227) return 2861.73333333333;
		      if(w > 227)
		       if(we <= 2264) return 3185.38333333333;
		       if(we > 2264) return 2973.6;
		     if(w > 232)
		      if(we <= 2292) return 3773.36666666667;
		      if(we > 2292)
		       if(we <= 2363) return 3190.55;
		       if(we > 2363) return 2670.56666666667;
		  if(w > 233)
		   if(w <= 234)
		    if(we <= 2363) return 2664.81666666667;
		    if(we > 2363) return 3561.88333333333;
		   if(w > 234)
		    if(w <= 235)
		     if(t <= 2.84)
		      if(we <= 2287) return 2603.66666666667;
		      if(we > 2287) return 2947.45;
		     if(t > 2.84) return 3098.21666666667;
		    if(w > 235)
		     if(we <= 2287) return 3130;
		     if(we > 2287) return 3404.71666666667;
		 if(w > 238)
		  if(t <= 2.84)
		   if(t <= 2.63)
		    if(w <= 280) return 2937.01666666667;
		    if(w > 280) return 2659.66666666667;
		   if(t > 2.63)
		    if(w <= 242) return 2861.73333333333;
		    if(w > 242)
		     if(we <= 2479) return 2952.7;
		     if(we > 2479) return 2824.96666666667;
		  if(t > 2.84)
		   if(t <= 3.1)
		    if(we <= 2792) return 2578.11666666667;
		    if(we > 2792) return 3298.83333333333;
		   if(t > 3.1)
		    if(t <= 3.34) return 2729.63333333333;
		    if(t > 3.34) return 2400;
		if(p.equals("PP12262")) return 2556;
		if(p.equals("PP12266")) return 3065.45;
		if(p.equals("PP12268")) return 3137.03333333333;
		if(p.equals("PP12269")) return 3100.21666666667;
		if(p.equals("PP12270")) return 2700.31666666667;
		if(p.equals("PP12271")) return 2337.3;
		if(p.equals("PP12272")) return 3240.93333333333;
		if(p.equals("PP12273")) return 2753.5;
		if(p.equals("PP12274")) return 2868;
		if(p.equals("PP12275"))
		 if(w <= 476)
		  if(w <= 469)
		   if(we <= 4688) return 3847.55;
		   if(we > 4688) return 2970.7;
		  if(w > 469)
		   if(we <= 4716) return 3847.55;
		   if(we > 4716) return 3561.88333333333;
		 if(w > 476)
		  if(t <= 2.84)
		   if(we <= 4836) return 2568.06666666667;
		   if(we > 4836) return 2242.16666666667;
		  if(t > 2.84)
		   if(we <= 5911) return 2837.6;
		   if(we > 5911) return 2888;
		if(p.equals("PP12276")) return 2780.63333333333;
		if(p.equals("PP12277")) return 2899.61666666667;
		if(p.equals("PP12278")) return 3276;
		if(p.equals("PP12279")) return 3302.18333333333;
		if(p.equals("PP12280")) return 3574.05;
		if(p.equals("PP12282")) return 3292.35;
		if(p.equals("PP12283")) return 2792.3;
		if(p.equals("PP12284")) return 3096.11666666667;
		if(p.equals("PP12287")) return 2725.55;
		if(p.equals("PP12289"))
		 if(w <= 350)
		  if(w <= 299)
		   if(t <= 2.42) return 3589.33333333333;
		   if(t > 2.42) return 2836.63333333333;
		  if(w > 299) return 2346.85;
		 if(w > 350)
		  if(w <= 399) return 2827.83333333333;
		  if(w > 399) return 2671.9;
		if(p.equals("PP12306")) return 2635.03333333333;
		if(p.equals("PP12307")) return 2796.1;
		if(p.equals("PP12308")) return 3104.3;
		if(p.equals("PP12313"))
		 if(t <= 2.84)
		  if(we <= 2500)
		   if(w <= 221)
		    if(we <= 2169) return 2997.81666666667;
		    if(we > 2169) return 2244.25;
		   if(w > 221)
		    if(w <= 237) return 2287.33333333333;
		    if(w > 237) return 2997.81666666667;
		  if(we > 2500)
		   if(t <= 2.35) return 3001.55;
		   if(t > 2.35)
		    if(we <= 2517) return 2244.25;
		    if(we > 2517)
		     if(w <= 237)
		      if(w <= 221) return 2501.96666666667;
		      if(w > 221) return 2542;
		     if(w > 237)
		      if(we <= 2912)
		       if(we <= 2899) return 2394.88333333333;
		       if(we > 2899) return 2501.96666666667;
		      if(we > 2912) return 2542;
		 if(t > 2.84)
		  if(w <= 221) return 2244.25;
		  if(w > 221)
		   if(we <= 2287) return 2112.85;
		   if(we > 2287)
		    if(we <= 2559) return 2650;
		    if(we > 2559) return 2381.53333333333;
		if(p.equals("PP12315")) return 3592.53333333333;
		if(p.equals("PP12319")) return 2635.03333333333;
		if(p.equals("PP12324"))
		 if(we <= 5223)
		  if(we <= 4898) return 2864.18333333333;
		  if(we > 4898) return 2650;
		 if(we > 5223) return 2864.18333333333;
		if(p.equals("PP12325")) return 3029.13333333333;
		if(p.equals("PP12326"))
		 if(we <= 3422)
		  if(w <= 228) return 2821;
		  if(w > 228) return 2807.66666666667;
		 if(we > 3422) return 2653.7;
		if(p.equals("PP12327"))
		 if(we <= 3252) return 2583;
		 if(we > 3252)
		  if(w <= 255) return 2862.18333333333;
		  if(w > 255) return 2926;
		if(p.equals("PP12328"))
		 if(we <= 4724)
		  if(we <= 4487) return 2807.66666666667;
		  if(we > 4487) return 2821;
		 if(we > 4724) return 2807.66666666667;
		if(p.equals("PP12329")) return 2583;
		if(p.equals("PP12330")) return 2625.76666666667;
		if(p.equals("PP12331")) return 3658;
		if(p.equals("PP12332"))
		 if(we <= 4493)
		  if(we <= 4275) return 2598.18333333333;
		  if(we > 4275)
		   if(w <= 318) return 2908.21666666667;
		   if(w > 318) return 2753.5;
		 if(we > 4493)
		  if(w <= 339) return 2877.08333333333;
		  if(w > 339)
		   if(w <= 350) return 3100.21666666667;
		   if(w > 350) return 2625.76666666667;
		if(p.equals("PP12336")) return 2670;
		if(p.equals("PP12337"))
		 if(t <= 4.7)
		  if(w <= 243) return 2841.5;
		  if(w > 243) return 2837.18333333333;
		 if(t > 4.7)
		  if(we <= 3983)
		   if(t <= 5.08) return 4629.93333333333;
		   if(t > 5.08) return 2924.58333333333;
		  if(we > 3983)
		   if(we <= 4343) return 2636.68333333333;
		   if(we > 4343) return 4519.18333333333;
		if(p.equals("PP12340"))
		 if(w <= 233)
		  if(w <= 207)
		   if(w <= 206) return 2875.26666666667;
		   if(w > 206) return 2801.78333333333;
		  if(w > 207) return 2877.08333333333;
		 if(w > 233)
		  if(w <= 243)
		   if(w <= 241) return 3130;
		   if(w > 241) return 2757.01666666667;
		  if(w > 243)
		   if(w <= 317) return 2868;
		   if(w > 317) return 3295.33333333333;
		if(p.equals("PP12342"))
		 if(w <= 238) return 2792.3;
		 if(w > 238) return 2635.03333333333;
		if(p.equals("PP12344")) return 2767;
		if(p.equals("PP12349")) return 3378.36666666667;
		if(p.equals("PP12350")) return 2869.38333333333;
		if(p.equals("PP12351")) return 2337.3;
		if(p.equals("PP12352"))
		 if(we <= 3105) return 2777.98333333333;
		 if(we > 3105) return 3023;
		if(p.equals("PP12353")) return 2721.75;
		if(p.equals("PP12354")) return 3104.3;
		if(p.equals("PP12355")) return 2594.2;
		if(p.equals("PP12356"))
		 if(we <= 7113) return 4031.56666666667;
		 if(we > 7113) return 2688.06666666667;
		if(p.equals("PP12357"))
		 if(t <= 2.15)
		  if(we <= 4665) return 3758.9;
		  if(we > 4665) return 2767;
		 if(t > 2.15)
		  if(t <= 2.45)
		   if(w <= 343) return 2850;
		   if(w > 343) return 3280;
		  if(t > 2.45)
		   if(w <= 382) return 3280;
		   if(w > 382) return 2131.26666666667;
		if(p.equals("PP12358")) return 2777.98333333333;
		if(p.equals("PP12359"))
		 if(we <= 3466)
		  if(we <= 3134) return 6963.81666666667;
		  if(we > 3134) return 6308.76666666667;
		 if(we > 3466) return 2796.1;
		if(p.equals("PP12360"))
		 if(we <= 4458) return 2862.18333333333;
		 if(we > 4458) return 2639.26666666667;
		if(p.equals("PP12361"))
		 if(w <= 335) return 3403;
		 if(w > 335) return 2753.5;
		if(p.equals("PP12362")) return 2783;
		if(p.equals("PP12363"))
		 if(w <= 222) return 2868;
		 if(w > 222) return 2724.2;
		if(p.equals("PP12364"))
		 if(w <= 271) return 2833;
		 if(w > 271) return 3130;
		if(p.equals("PP12366")) return 2639;
		if(p.equals("PP12369"))
		 if(t <= 2.94)
		  if(we <= 3732)
		   if(we <= 3640)
		    if(we <= 3601) return 2444.93333333333;
		    if(we > 3601) return 2997.81666666667;
		   if(we > 3640) return 2444.93333333333;
		  if(we > 3732)
		   if(w <= 371) return 2244.25;
		   if(w > 371) return 2287.33333333333;
		 if(t > 2.94)
		  if(t <= 3.74) return 2398.05;
		  if(t > 3.74) return 2349.63333333333;
		if(p.equals("PP12370")) return 2908.21666666667;
		if(p.equals("PP12373")) return 2908.21666666667;
		if(p.equals("PP12376")) return 3001.55;
		if(p.equals("PP12377")) return 2712;
		if(p.equals("PP12378")) return 2712;
		if(p.equals("PP12383")) return 3001.55;
		if(p.equals("PP12384")) return 3346.51666666667;
		if(p.equals("PP12389")) return 2398.05;
		if(p.equals("PP12390")) return 2398.05;
		if(p.equals("PP12392")) return 2672.08333333333;
		if(p.equals("PP12393")) return 2672.08333333333;
		if(p.equals("PP12394"))
		 if(we <= 3207)
		  if(t <= 2.33) return 3140.65;
		  if(t > 2.33)
		   if(t <= 3.17) return 3206.68333333333;
		   if(t > 3.17) return 3174.11666666667;
		 if(we > 3207)
		  if(w <= 290) return 3277.6;
		  if(w > 290)
		   if(w <= 364) return 2785.23333333333;
		   if(w > 364) return 2617;
		if(p.equals("PP12395")) return 2672.08333333333;
		if(p.equals("PP12399")) return 2700.31666666667;
		if(p.equals("PP12401"))
		 if(t <= 2.84)
		  if(we <= 10519) return 2820.86666666667;
		  if(we > 10519)
		   if(we <= 22920) return 3266.96666666667;
		   if(we > 22920) return 4162;
		 if(t > 2.84)
		  if(we <= 15610) return 3348;
		  if(we > 15610)
		   if(we <= 22920) return 3285.01666666667;
		   if(we > 22920) return 3465.58333333333;
		if(p.equals("PP12403"))
		 if(t <= 1.22)
		  if(t <= 1.08) return 3303.66666666667;
		  if(t > 1.08) return 3922.2;
		 if(t > 1.22)
		  if(we <= 20630) return 3451.6;
		  if(we > 20630) return 3139.8;
		if(p.equals("PP12408")) return 3280;
		if(p.equals("PP12409")) return 3758.9;
		if(p.equals("PP12413"))
		 if(t <= 0.85) return 2537.7;
		 if(t > 0.85)
		  if(t <= 1.34) return 2387;
		  if(t > 1.34) return 2366.91666666667;
		if(p.equals("PP12414")) return 2865.26666666667;
		if(p.equals("PP12421"))
		 if(we <= 6509) return 2729.63333333333;
		 if(we > 6509) return 2556;
		if(p.equals("PP12422"))
		 if(we <= 4531) return 2780.63333333333;
		 if(we > 4531) return 3052.86666666667;
		if(p.equals("PP12423")) return 2965;
		if(p.equals("PP12424")) return 2965;
		if(p.equals("PP12428"))
		 if(we <= 3341) return 3100.21666666667;
		 if(we > 3341) return 2865.26666666667;
		if(p.equals("PP12429")) return 2865.26666666667;
		if(p.equals("PP12430")) return 3504.48333333333;
		if(p.equals("PP12431")) return 3504.48333333333;
		if(p.equals("PP12432")) return 3276;
		if(p.equals("PP12433"))
		 if(w <= 230) return 2903.73333333333;
		 if(w > 230) return 2556.53333333333;
		if(p.equals("PP12435"))
		 if(w <= 480) return 2112.85;
		 if(w > 480) return 2244.25;
		if(p.equals("PP12436"))
		 if(t <= 3.64)
		  if(w <= 379)
		   if(t <= 3.1)
		    if(we <= 3940)
		     if(we <= 3748) return 3368.06666666667;
		     if(we > 3748) return 2635.03333333333;
		    if(we > 3940)
		     if(we <= 4596) return 2695.81666666667;
		     if(we > 4596) return 2635.03333333333;
		   if(t > 3.1) return 1995.23333333333;
		  if(w > 379) return 2691;
		 if(t > 3.64)
		  if(w <= 369) return 2837.18333333333;
		  if(w > 369)
		   if(w <= 407) return 2744.93333333333;
		   if(w > 407) return 2513.75;
		if(p.equals("PP12437"))
		 if(w <= 357)
		  if(we <= 4553) return 2651;
		  if(we > 4553) return 3808.33333333333;
		 if(w > 357)
		  if(we <= 5546) return 3160.08333333333;
		  if(we > 5546)
		   if(we <= 5587) return 3209.03333333333;
		   if(we > 5587) return 3045;
		if(p.equals("PP12438")) return 3403;
		if(p.equals("PP12439"))
		 if(t <= 2.9) return 2724.2;
		 if(t > 2.9) return 2869.38333333333;
		if(p.equals("PP12440"))
		 if(t <= 2.3)
		  if(we <= 5378) return 2903.73333333333;
		  if(we > 5378) return 3398;
		 if(t > 2.3) return 2639.26666666667;
		if(p.equals("PP12443"))
		 if(t <= 2.15)
		  if(w <= 310) return 2337.3;
		  if(w > 310) return 2868;
		 if(t > 2.15)
		  if(we <= 4247) return 2659.66666666667;
		  if(we > 4247) return 2937.01666666667;
		if(p.equals("PP12444"))
		 if(w <= 517) return 2945;
		 if(w > 517) return 2872.23333333333;
		if(p.equals("PP12446"))
		 if(t <= 4.25)
		  if(w <= 357) return 3004.9;
		  if(w > 357) return 3065.45;
		 if(t > 4.25)
		  if(we <= 4291) return 3052.86666666667;
		  if(we > 4291) return 2796.1;
		if(p.equals("PP12447")) return 2543;
		if(p.equals("PP12449"))
		 if(w <= 221) return 3158.13333333333;
		 if(w > 221) return 2752.66666666667;
		if(p.equals("PP12450")) return 2796.1;
		if(p.equals("PP12452"))
		 if(t <= 3.26)
		  if(w <= 1073)
		   if(t <= 2.3)
		    if(we <= 19570) return 3266.18333333333;
		    if(we > 19570) return 4037.35;
		   if(t > 2.3)
		    if(we <= 19570) return 3867.55;
		    if(we > 19570) return 4263.75;
		  if(w > 1073)
		   if(we <= 23960)
		    if(t <= 2.88)
		     if(w <= 1186)
		      if(t <= 2.36) return 3081.46666666667;
		      if(t > 2.36) return 3468.86666666667;
		     if(w > 1186) return 3821.21666666667;
		    if(t > 2.88) return 3870.51666666667;
		   if(we > 23960)
		    if(t <= 2.8) return 4125.83333333333;
		    if(t > 2.8) return 3602;
		 if(t > 3.26)
		  if(w <= 1223)
		   if(t <= 3.9)
		    if(w <= 1073) return 3523;
		    if(w > 1073) return 3314.68333333333;
		   if(t > 3.9)
		    if(t <= 4.3)
		     if(we <= 24280) return 6159.36666666667;
		     if(we > 24280) return 4015.7;
		    if(t > 4.3) return 3706.35;
		  if(w > 1223)
		   if(we <= 23520)
		    if(w <= 1236)
		     if(t <= 3.79) return 3366.56666666667;
		     if(t > 3.79)
		      if(t <= 5.26) return 3370.36666666667;
		      if(t > 5.26) return 3877.96666666667;
		    if(w > 1236) return 3895.83333333333;
		   if(we > 23520)
		    if(w <= 1236)
		     if(t <= 3.79) return 3596.71666666667;
		     if(t > 3.79)
		      if(t <= 5.26) return 4831.98333333333;
		      if(t > 5.26) return 4049.11666666667;
		    if(w > 1236) return 3663.63333333333;
		if(p.equals("PP12455")) return 3663.63333333333;
		if(p.equals("PP12457")) return 2688.06666666667;
		if(p.equals("PP12459")) return 2655.83333333333;
		if(p.equals("PP12460")) return 2858.11666666667;
		if(p.equals("PP12461"))
		 if(t <= 3.24)
		  if(t <= 2.84) return 3417.18333333333;
		  if(t > 2.84)
		   if(we <= 2680)
		    if(we <= 2178) return 3037;
		    if(we > 2178) return 2695.81666666667;
		   if(we > 2680)
		    if(we <= 2879) return 3065.2;
		    if(we > 2879) return 2635.03333333333;
		 if(t > 3.24)
		  if(w <= 214) return 2561.46666666667;
		  if(w > 214) return 2623.1;
		if(p.equals("PP12462"))
		 if(t <= 3.1)
		  if(t <= 2.84)
		   if(we <= 4654)
		    if(w <= 469)
		     if(we <= 4571) return 2521.18333333333;
		     if(we > 4571) return 2725.45;
		    if(w > 469)
		     if(we <= 4601) return 3276.25;
		     if(we > 4601) return 2521.18333333333;
		   if(we > 4654) return 2507.31666666667;
		  if(t > 2.84)
		   if(w <= 465) return 2691;
		   if(w > 465)
		    if(we <= 5685) return 2876.06666666667;
		    if(we > 5685) return 2635.03333333333;
		 if(t > 3.1)
		  if(we <= 5298)
		   if(we <= 5023)
		    if(we <= 4856) return 2470.53333333333;
		    if(we > 4856) return 2623.1;
		   if(we > 5023) return 3073;
		  if(we > 5298)
		   if(we <= 5477)
		    if(we <= 5347) return 2561.46666666667;
		    if(we > 5347) return 3133.51666666667;
		   if(we > 5477) return 3275.5;
		if(p.equals("PP12464")) return 2780.63333333333;
		if(p.equals("PP12466")) return 2450.66666666667;
		if(p.equals("PP12468")) return 3174;
		if(p.equals("PP12469"))
		 if(w <= 319) return 2543;
		 if(w > 319) return 2837.18333333333;
		if(p.equals("PP12470")) return 1995.23333333333;
		if(p.equals("PP12479")) return 3581.41666666667;
		if(p.equals("PP12483"))
		 if(t <= 1.8) return 3053.96666666667;
		 if(t > 1.8) return 3083.65;
		if(p.equals("PP12484")) return 3216.18333333333;
		if(p.equals("PP12485")) return 2207.68333333333;
		if(p.equals("PP12486")) return 3012.58333333333;
		if(p.equals("PP12487")) return 3134.41666666667;
		if(p.equals("PP12491")) return 3578.96666666667;
		if(p.equals("PP12501")) return 2752.66666666667;
		if(p.equals("PP12519")) return 2888.15;
		if(p.equals("PP12524"))
		 if(t <= 2.98) return 2886.95;
		 if(t > 2.98) return 2664;
		if(p.equals("PP12531")) return 2817.91666666667;
		if(p.equals("PP12532")) return 2817.91666666667;
		if(p.equals("PP12536"))
		 if(t <= 1.6) return 3290.95;
		 if(t > 1.6) return 3655.93333333333;
		if(p.equals("PP12537")) return 2866.9;
		if(p.equals("PP12538")) return 3065.45;
		if(p.equals("PP12539")) return 2362.55;
		if(p.equals("PP12541")) return 2749.75;
		if(p.equals("PP12546")) return 2755.28333333333;
		if(p.equals("PP12550")) return 3481.2;
		if(p.equals("PP12551")) return 3481.2;
		if(p.equals("PP12563"))
		 if(we <= 9299)
		  if(t <= 2.45)
		   if(w <= 411) return 2769.95;
		   if(w > 411)
		    if(we <= 8702) return 2780.43333333333;
		    if(we > 8702) return 3287.76666666667;
		  if(t > 2.45) return 2939.81666666667;
		 if(we > 9299)
		  if(t <= 2.45)
		   if(w <= 631) return 3280.68333333333;
		   if(w > 631)
		    if(we <= 13646) return 3093.2;
		    if(we > 13646) return 2843.28333333333;
		  if(t > 2.45) return 3192.78333333333;
		if(p.equals("PP12575")) return 2858;
		if(p.equals("PP21039"))
		 if(w <= 247)
		  if(t <= 1.26) return 3151.41666666667;
		  if(t > 1.26) return 3422.43333333333;
		 if(w > 247)
		  if(t <= 1.26)
		   if(we <= 3543) return 3421.06666666667;
		   if(we > 3543) return 3592.53333333333;
		  if(t > 1.26)
		   if(t <= 1.73)
		    if(we <= 3709) return 3589.23333333333;
		    if(we > 3709) return 3277.6;
		   if(t > 1.73) return 2676.08333333333;
		if(p.equals("PP21089"))
		 if(t <= 3.79)
		  if(t <= 3.15)
		   if(t <= 2.24)
		    if(t <= 1.9) return 2550.63333333333;
		    if(t > 1.9) return 5885.46666666667;
		   if(t > 2.24)
		    if(t <= 2.9)
		     if(t <= 2.55) return 2818;
		     if(t > 2.55) return 3936.5;
		    if(t > 2.9)
		     if(w <= 1149) return 2952;
		     if(w > 1149)
		      if(we <= 24140) return 3847.88333333333;
		      if(we > 24140) return 3161.65;
		  if(t > 3.15)
		   if(t <= 3.45)
		    if(t <= 3.34)
		     if(w <= 1152) return 2966.91666666667;
		     if(w > 1152) return 3407.76666666667;
		    if(t > 3.34) return 3643.58333333333;
		   if(t > 3.45)
		    if(we <= 19910)
		     if(we <= 18610) return 3447.41666666667;
		     if(we > 18610) return 2778.95;
		    if(we > 19910)
		     if(we <= 20860) return 3211.73333333333;
		     if(we > 20860) return 3130.48333333333;
		 if(t > 3.79)
		  if(we <= 20880)
		   if(w <= 975)
		    if(we <= 20500) return 3081.08333333333;
		    if(we > 20500)
		     if(we <= 20750) return 3279.66666666667;
		     if(we > 20750) return 3164.45;
		   if(w > 975)
		    if(t <= 4.5)
		     if(we <= 19840) return 2577.51666666667;
		     if(we > 19840) return 3073.48333333333;
		    if(t > 4.5) return 3059.53333333333;
		  if(we > 20880)
		   if(we <= 23370)
		    if(t <= 4.92)
		     if(w <= 1099) return 3072.2;
		     if(w > 1099)
		      if(w <= 1142) return 3551.15;
		      if(w > 1142) return 4785.53333333333;
		    if(t > 4.92) return 3047.66666666667;
		   if(we > 23370)
		    if(w <= 1173)
		     if(we <= 23960) return 3136.5;
		     if(we > 23960) return 3126.73333333333;
		    if(w > 1173)
		     if(we <= 24450) return 3076.06666666667;
		     if(we > 24450) return 3122;
		if(p.equals("PP21105"))
		 if(t <= 1.3)
		  if(w <= 1131)
		   if(t <= 0.91) return 3677.5;
		   if(t > 0.91)
		    if(we <= 16450) return 2913.61666666667;
		    if(we > 16450) return 3082.05;
		  if(w > 1131)
		   if(w <= 1189) return 2520;
		   if(w > 1189)
		    if(we <= 18020) return 2863.98333333333;
		    if(we > 18020) return 3120.81666666667;
		 if(t > 1.3)
		  if(w <= 1164)
		   if(we <= 17440) return 3544.28333333333;
		   if(we > 17440) return 3358;
		  if(w > 1164)
		   if(w <= 1224)
		    if(t <= 2.11) return 2958;
		    if(t > 2.11) return 3220.51666666667;
		   if(w > 1224)
		    if(we <= 18020) return 3259.23333333333;
		    if(we > 18020) return 3516.96666666667;
		if(p.equals("PP21107"))
		 if(t <= 3.01)
		  if(w <= 1177)
		   if(w <= 1113)
		    if(we <= 22400)
		     if(w <= 1084)
		      if(w <= 1015)
		       if(we <= 20820)
		        if(we <= 20490)
		         if(we <= 20080) return 2726.45;
		         if(we > 20080) return 3103.01666666667;
		        if(we > 20490)
		         if(we <= 20790) return 3142.53333333333;
		         if(we > 20790) return 3437.21666666667;
		       if(we > 20820)
		        if(we <= 20970)
		         if(we <= 20850) return 3660.1;
		         if(we > 20850)
		          if(we <= 20900) return 3316.65;
		          if(we > 20900) return 3103.01666666667;
		        if(we > 20970)
		         if(we <= 21070) return 3830.85;
		         if(we > 21070)
		          if(we <= 21110) return 3400.95;
		          if(we > 21110) return 3330.78333333333;
		      if(w > 1015)
		       if(t <= 2.71) return 3228.51666666667;
		       if(t > 2.71) return 3455.13333333333;
		     if(w > 1084)
		      if(w <= 1110)
		       if(w <= 1098) return 3928.55;
		       if(w > 1098)
		        if(we <= 21810)
		         if(we <= 21480) return 3455.13333333333;
		         if(we > 21480) return 4279;
		        if(we > 21810)
		         if(we <= 22110) return 4699.03333333333;
		         if(we > 22110) return 3701.8;
		      if(w > 1110)
		       if(t <= 2.5)
		        if(we <= 21190) return 3052.85;
		        if(we > 21190) return 3364.48333333333;
		       if(t > 2.5)
		        if(we <= 20540) return 3366.38333333333;
		        if(we > 20540)
		         if(we <= 21430) return 5196.1;
		         if(we > 21430) return 3273.06666666667;
		    if(we > 22400)
		     if(we <= 22910)
		      if(w <= 1110)
		       if(t <= 2.61) return 3356.53333333333;
		       if(t > 2.61) return 3230;
		      if(w > 1110)
		       if(we <= 22840)
		        if(we <= 22810) return 3388.25;
		        if(we > 22810) return 3869.6;
		       if(we > 22840)
		        if(we <= 22890) return 5170.28333333333;
		        if(we > 22890) return 4642;
		     if(we > 22910)
		      if(t <= 2.63)
		       if(we <= 23060)
		        if(we <= 23010) return 3445.65;
		        if(we > 23010) return 3517.6;
		       if(we > 23060)
		        if(we <= 23120) return 3437.88333333333;
		        if(we > 23120) return 3511.1;
		      if(t > 2.63) return 3362.6;
		   if(w > 1113)
		    if(t <= 2.63)
		     if(w <= 1172)
		      if(w <= 1164)
		       if(t <= 2.165)
		        if(t <= 1.72) return 8259.3;
		        if(t > 1.72)
		         if(we <= 23360) return 3193.41666666667;
		         if(we > 23360)
		          if(we <= 23570) return 4147;
		          if(we > 23570) return 3228.51666666667;
		       if(t > 2.165) return 5171.41666666667;
		      if(w > 1164)
		       if(we <= 24090)
		        if(we <= 23030) return 3472.93333333333;
		        if(we > 23030) return 3176.78333333333;
		       if(we > 24090)
		        if(we <= 24260) return 3244.65;
		        if(we > 24260) return 3634.06666666667;
		     if(w > 1172)
		      if(we <= 24250)
		       if(we <= 23790)
		        if(we <= 22490)
		         if(we <= 21560) return 2811.73333333333;
		         if(we > 21560) return 3397.3;
		        if(we > 22490) return 7099.35;
		       if(we > 23790) return 3103.18333333333;
		      if(we > 24250)
		       if(we <= 24380)
		        if(we <= 24320) return 3109;
		        if(we > 24320) return 3076.71666666667;
		       if(we > 24380) return 3414.88333333333;
		    if(t > 2.63)
		     if(w <= 1164)
		      if(t <= 3)
		       if(t <= 2.94)
		        if(w <= 1149)
		         if(we <= 23290) return 3317.85;
		         if(we > 23290) return 3543.6;
		        if(w > 1149)
		         if(t <= 2.71) return 2943.1;
		         if(t > 2.71) return 3506.66666666667;
		       if(t > 2.94)
		        if(we <= 23360) return 3494;
		        if(we > 23360)
		         if(we <= 23570) return 3294.78333333333;
		         if(we > 23570) return 7609.03333333333;
		      if(t > 3)
		       if(we <= 23100)
		        if(w <= 1127) return 3370.2;
		        if(w > 1127) return 3724.8;
		       if(we > 23100)
		        if(w <= 1124) return 3391.25;
		        if(w > 1124) return 3191.03333333333;
		     if(w > 1164)
		      if(we <= 24110)
		       if(we <= 23510)
		        if(we <= 21560) return 5949.8;
		        if(we > 21560) return 2820.48333333333;
		       if(we > 23510)
		        if(we <= 23830) return 7938.18333333333;
		        if(we > 23830)
		         if(we <= 23900) return 3531.21666666667;
		         if(we > 23900) return 3150.58333333333;
		      if(we > 24110)
		       if(we <= 24290)
		        if(we <= 24250) return 3204;
		        if(we > 24250) return 3540.63333333333;
		       if(we > 24290)
		        if(we <= 24380)
		         if(we <= 24350) return 3411.31666666667;
		         if(we > 24350) return 3178.05;
		        if(we > 24380) return 3575.2;
		  if(w > 1177)
		   if(t <= 2.11)
		    if(t <= 1.66)
		     if(w <= 1217)
		      if(w <= 1193)
		       if(we <= 16360) return 3301;
		       if(we > 16360) return 3607.98333333333;
		      if(w > 1193)
		       if(w <= 1206)
		        if(we <= 17470) return 2991.43333333333;
		        if(we > 17470)
		         if(we <= 18120) return 3029.86666666667;
		         if(we > 18120) return 3833.43333333333;
		       if(w > 1206)
		        if(we <= 22030) return 3048.65;
		        if(we > 22030) return 3553.83333333333;
		     if(w > 1217)
		      if(t <= 1.57)
		       if(t <= 1.47)
		        if(w <= 1224) return 2858.55;
		        if(w > 1224) return 3412.16666666667;
		       if(t > 1.47)
		        if(w <= 1219)
		         if(we <= 17800) return 3141.93333333333;
		         if(we > 17800) return 2915.45;
		        if(w > 1219) return 2523.33333333333;
		      if(t > 1.57)
		       if(w <= 1219)
		        if(we <= 19350) return 3140;
		        if(we > 19350) return 2955.11666666667;
		       if(w > 1219) return 3392.31666666667;
		    if(t > 1.66)
		     if(t <= 2)
		      if(w <= 1208)
		       if(we <= 17470)
		        if(w <= 1200.5) return 3326.65;
		        if(w > 1200.5) return 2912.06666666667;
		       if(we > 17470) return 2633.21666666667;
		      if(w > 1208) return 2170.71666666667;
		     if(t > 2)
		      if(w <= 1206)
		       if(we <= 24610)
		        if(we <= 24520) return 3502.21666666667;
		        if(we > 24520) return 3532.78333333333;
		       if(we > 24610) return 3343.6;
		      if(w > 1206)
		       if(we <= 18810) return 3455.93333333333;
		       if(we > 18810) return 3449.65;
		   if(t > 2.11)
		    if(we <= 19160)
		     if(t <= 2.52)
		      if(w <= 1217)
		       if(we <= 16360) return 4766.58333333333;
		       if(we > 16360) return 3167.88333333333;
		      if(w > 1217)
		       if(w <= 1219)
		        if(we <= 17800) return 2545.05;
		        if(we > 17800) return 2704.45;
		       if(w > 1219) return 2477.53333333333;
		     if(t > 2.52)
		      if(w <= 1219)
		       if(t <= 2.55)
		        if(w <= 1208) return 3301.08333333333;
		        if(w > 1208) return 3485.1;
		       if(t > 2.55)
		        if(t <= 2.8) return 3138.28333333333;
		        if(t > 2.8) return 3428;
		      if(w > 1219)
		       if(t <= 2.61) return 2545.05;
		       if(t > 2.61) return 2523.33333333333;
		    if(we > 19160)
		     if(w <= 1215)
		      if(t <= 2.49)
		       if(t <= 2.37) return 3212.9;
		       if(t > 2.37)
		        if(we <= 24010) return 3145.03333333333;
		        if(we > 24010) return 3248.06666666667;
		      if(t > 2.49)
		       if(w <= 1201) return 3928.55;
		       if(w > 1201)
		        if(we <= 22030) return 3088.33333333333;
		        if(we > 22030) return 3886.06666666667;
		     if(w > 1215)
		      if(w <= 1224)
		       if(t <= 2.69)
		        if(t <= 2.55) return 3383.18333333333;
		        if(t > 2.55) return 2822.35;
		       if(t > 2.69) return 3403.66666666667;
		      if(w > 1224)
		       if(w <= 1255)
		        if(t <= 2.4) return 3468;
		        if(t > 2.4) return 3006.66666666667;
		       if(w > 1255) return 3636.95;
		 if(t > 3.01)
		  if(w <= 1131)
		   if(w <= 1084)
		    if(w <= 1032)
		     if(w <= 1000)
		      if(we <= 20940)
		       if(we <= 20790)
		        if(we <= 20130) return 2461.5;
		        if(we > 20130)
		         if(we <= 20490) return 3523.2;
		         if(we > 20490) return 8351.85;
		       if(we > 20790)
		        if(we <= 20840) return 2931.3;
		        if(we > 20840)
		         if(we <= 20870) return 2767;
		         if(we > 20870) return 2842;
		      if(we > 20940)
		       if(we <= 21070) return 2739.4;
		       if(we > 21070)
		        if(we <= 21110) return 3567.05;
		        if(we > 21110) return 2917.01666666667;
		     if(w > 1000)
		      if(w <= 1008)
		       if(t <= 4.7) return 3310.76666666667;
		       if(t > 4.7) return 3487;
		      if(w > 1008)
		       if(w <= 1021) return 3530;
		       if(w > 1021) return 3149;
		    if(w > 1032)
		     if(w <= 1045)
		      if(t <= 4.52)
		       if(t <= 3.54)
		        if(we <= 19620) return 4332.08333333333;
		        if(we > 19620)
		         if(we <= 19730) return 4674.55;
		         if(we > 19730) return 3835.08333333333;
		       if(t > 3.54) return 3052.85;
		      if(t > 4.52)
		       if(t <= 5.6)
		        if(we <= 19620) return 2807.83333333333;
		        if(we > 19620)
		         if(we <= 19730) return 3421.05;
		         if(we > 19730) return 2901.4;
		       if(t > 5.6) return 3551.21666666667;
		     if(w > 1045)
		      if(w <= 1078)
		       if(w <= 1056)
		        if(t <= 4.08) return 2915.45;
		        if(t > 4.08) return 2709.15;
		       if(w > 1056)
		        if(t <= 4.6) return 4002;
		        if(t > 4.6) return 3101;
		      if(w > 1078)
		       if(t <= 3.52)
		        if(we <= 21670) return 3145.66666666667;
		        if(we > 21670) return 3080.11666666667;
		       if(t > 3.52)
		        if(we <= 21670) return 2555.93333333333;
		        if(we > 21670) return 9669.11666666667;
		   if(w > 1084)
		    if(w <= 1111)
		     if(we <= 22400)
		      if(w <= 1109)
		       if(w <= 1105)
		        if(t <= 4.35) return 2856;
		        if(t > 4.35) return 3170.25;
		       if(w > 1105)
		        if(we <= 21810)
		         if(we <= 21480) return 3031.7;
		         if(we > 21480) return 5839.65;
		        if(we > 21810)
		         if(we <= 22110) return 3682.56666666667;
		         if(we > 22110) return 3299.11666666667;
		      if(w > 1109)
		       if(w <= 1110)
		        if(t <= 4.08) return 3260.93333333333;
		        if(t > 4.08) return 3320.45;
		       if(w > 1110)
		        if(we <= 21190) return 2942.95;
		        if(we > 21190) return 3402.93333333333;
		     if(we > 22400)
		      if(we <= 22950)
		       if(w <= 1108) return 3735.25;
		       if(w > 1108)
		        if(we <= 22840) return 3473.88333333333;
		        if(we > 22840) return 3459.16666666667;
		      if(we > 22950)
		       if(we <= 23285) return 3721.66666666667;
		       if(we > 23285) return 3307.96666666667;
		    if(w > 1111)
		     if(we <= 22870)
		      if(we <= 22790)
		       if(t <= 4.85)
		        if(we <= 20540) return 3092.31666666667;
		        if(we > 20540)
		         if(we <= 22530) return 3182.18333333333;
		         if(we > 22530) return 3131.65;
		       if(t > 4.85) return 2815.28333333333;
		      if(we > 22790)
		       if(we <= 22830) return 3498.9;
		       if(we > 22830)
		        if(we <= 22850) return 3868.53333333333;
		        if(we > 22850) return 3773;
		     if(we > 22870)
		      if(t <= 4.85)
		       if(we <= 23000) return 3841.53333333333;
		       if(we > 23000)
		        if(we <= 23030) return 3277.9;
		        if(we > 23030) return 3186.01666666667;
		      if(t > 4.85)
		       if(we <= 23270) return 3825.33333333333;
		       if(we > 23270) return 3307.96666666667;
		  if(w > 1131)
		   if(w <= 1197)
		    if(t <= 4.25)
		     if(t <= 3.1)
		      if(we <= 23170)
		       if(w <= 1135)
		        if(we <= 22960) return 4500.45;
		        if(we > 22960) return 3374.21666666667;
		       if(w > 1135)
		        if(w <= 1145) return 2985.86666666667;
		        if(w > 1145) return 2944.9;
		      if(we > 23170)
		       if(we <= 23380)
		        if(we <= 23280) return 3445.8;
		        if(we > 23280) return 3362.28333333333;
		       if(we > 23380)
		        if(we <= 23410) return 3343.6;
		        if(we > 23410) return 3142.53333333333;
		     if(t > 3.1)
		      if(w <= 1190)
		       if(we <= 24720)
		        if(we <= 24450) return 2456.98333333333;
		        if(we > 24450) return 3370.9;
		       if(we > 24720)
		        if(we <= 24790) return 3096;
		        if(we > 24790)
		         if(we <= 24880) return 3787;
		         if(we > 24880) return 3304.88333333333;
		      if(w > 1190)
		       if(t <= 3.6)
		        if(we <= 24670)
		         if(we <= 24520) return 3462.1;
		         if(we > 24520) return 3694.8;
		        if(we > 24670) return 3157.78333333333;
		       if(t > 3.6)
		        if(we <= 24010) return 3424.46666666667;
		        if(we > 24010) return 4450.36666666667;
		    if(t > 4.25)
		     if(w <= 1138)
		      if(w <= 1135)
		       if(we <= 23270)
		        if(we <= 22960) return 3501.91666666667;
		        if(we > 22960) return 3170.4;
		       if(we > 23270) return 3735.56666666667;
		      if(w > 1135)
		       if(we <= 23290) return 4323.86666666667;
		       if(we > 23290) return 3319;
		     if(w > 1138)
		      if(we <= 24140)
		       if(t <= 4.7)
		        if(we <= 23030) return 5660.88333333333;
		        if(we > 23030)
		         if(we <= 23960) return 3611.96666666667;
		         if(we > 23960) return 3170.4;
		       if(t > 4.7)
		        if(w <= 1145) return 3553.53333333333;
		        if(w > 1145)
		         if(t <= 5.08) return 2782.23333333333;
		         if(t > 5.08) return 3757.48333333333;
		      if(we > 24140)
		       if(t <= 4.85)
		        if(t <= 4.6)
		         if(we <= 24260) return 3438.25;
		         if(we > 24260) return 3015;
		        if(t > 4.6) return 4008.6;
		       if(t > 4.85)
		        if(we <= 24720)
		         if(we <= 24450) return 7276.46666666667;
		         if(we > 24450) return 3605.88333333333;
		        if(we > 24720)
		         if(we <= 24790) return 3410.98333333333;
		         if(we > 24790)
		          if(we <= 24880) return 3814.25;
		          if(we > 24880) return 3372;
		   if(w > 1197)
		    if(t <= 3.84)
		     if(we <= 19500)
		      if(w <= 1219)
		       if(we <= 18740)
		        if(we <= 18540) return 2520;
		        if(we > 18540) return 3239.03333333333;
		       if(we > 18740)
		        if(t <= 3.1)
		         if(we <= 19140) return 3143.73333333333;
		         if(we > 19140) return 3332.21666666667;
		        if(t > 3.1) return 3344.61666666667;
		      if(w > 1219)
		       if(w <= 1224) return 2830.4;
		       if(w > 1224) return 3354.2;
		     if(we > 19500)
		      if(w <= 1213)
		       if(we <= 22660)
		        if(we <= 22440) return 3702.46666666667;
		        if(we > 22440)
		         if(we <= 22530) return 3861.45;
		         if(we > 22530) return 3282;
		       if(we > 22660)
		        if(we <= 23750)
		         if(we <= 22780) return 3317.83333333333;
		         if(we > 22780)
		          if(we <= 22940) return 3702.46666666667;
		          if(we > 22940)
		           if(we <= 23310) return 3614.03333333333;
		           if(we > 23310) return 3131.91666666667;
		        if(we > 23750) return 3347.9;
		      if(w > 1213)
		       if(t <= 3.29) return 3282;
		       if(t > 3.29) return 2710;
		    if(t > 3.84)
		     if(t <= 5.24)
		      if(t <= 4.52)
		       if(w <= 1219)
		        if(t <= 4.25)
		         if(w <= 1208) return 3637.83333333333;
		         if(w > 1208) return 2863.98333333333;
		        if(t > 4.25)
		         if(we <= 19320) return 2638.55;
		         if(we > 19320)
		          if(we <= 19510) return 3637.83333333333;
		          if(we > 19510) return 3238.66666666667;
		       if(w > 1219)
		        if(t <= 4.08) return 2477.53333333333;
		        if(t > 4.08) return 4353;
		      if(t > 4.52)
		       if(w <= 1224)
		        if(we <= 18650)
		         if(we <= 18360)
		          if(we <= 12415) return 2364.46666666667;
		          if(we > 12415) return 2918.66666666667;
		         if(we > 18360) return 3091.83333333333;
		        if(we > 18650)
		         if(we <= 18750) return 2692.8;
		         if(we > 18750) return 2742.3;
		       if(w > 1224)
		        if(t <= 4.85) return 3369.46666666667;
		        if(t > 4.85) return 2704.45;
		     if(t > 5.24)
		      if(w <= 1213)
		       if(we <= 22710)
		        if(we <= 22460)
		         if(we <= 22370) return 3861.41666666667;
		         if(we > 22370) return 3734.31666666667;
		        if(we > 22460)
		         if(we <= 22530) return 4227;
		         if(we > 22530)
		          if(we <= 22660) return 4110.01666666667;
		          if(we > 22660) return 3734.31666666667;
		       if(we > 22710)
		        if(we <= 23750) return 6736.21666666667;
		        if(we > 23750) return 3861.41666666667;
		      if(w > 1213)
		       if(t <= 5.7)
		        if(we <= 19270) return 3403.66666666667;
		        if(we > 19270) return 2475.45;
		       if(t > 5.7)
		        if(we <= 18330) return 3794;
		        if(we > 18330) return 2901.1;
		if(p.equals("PP21108"))
		 if(t <= 1.29)
		  if(t <= 0.96)
		   if(t <= 0.66)
		    if(t <= 0.55) return 2645.08333333333;
		    if(t > 0.55) return 3080.11666666667;
		   if(t > 0.66)
		    if(t <= 0.75) return 6293.03333333333;
		    if(t > 0.75)
		     if(w <= 1208) return 3478;
		     if(w > 1208) return 3146.53333333333;
		  if(t > 0.96)
		   if(t <= 1.15)
		    if(t <= 1.06) return 3141.43333333333;
		    if(t > 1.06) return 3298.76666666667;
		   if(t > 1.15)
		    if(we <= 17680) return 8910;
		    if(we > 17680) return 2710;
		 if(t > 1.29)
		  if(w <= 1208)
		   if(t <= 1.99)
		    if(t <= 1.33) return 3310.76666666667;
		    if(t > 1.33)
		     if(we <= 22420) return 3489;
		     if(we > 22420) return 3977.68333333333;
		   if(t > 1.99)
		    if(t <= 2.5)
		     if(we <= 17910) return 3040.7;
		     if(we > 17910) return 2692.8;
		    if(t > 2.5) return 3537.88333333333;
		  if(w > 1208)
		   if(we <= 20040)
		    if(t <= 1.72) return 3269.41666666667;
		    if(t > 1.72) return 2797.23333333333;
		   if(we > 20040)
		    if(we <= 20370) return 3300.53333333333;
		    if(we > 20370) return 2402.68333333333;
		if(p.equals("PP21116"))
		 if(t <= 1.34)
		  if(we <= 14760) return 2448.43333333333;
		  if(we > 14760)
		   if(we <= 14830) return 3198.75;
		   if(we > 14830) return 2448.73333333333;
		 if(t > 1.34)
		  if(t <= 1.93)
		   if(we <= 14790) return 2867.03333333333;
		   if(we > 14790) return 3074;
		  if(t > 1.93)
		   if(we <= 14760) return 2742.45;
		   if(we > 14760)
		    if(we <= 14830) return 3207.1;
		    if(we > 14830) return 2638.75;
		if(p.equals("PP21173")) return 3496.48333333333;
		if(p.equals("PP21180"))
		 if(t <= 3.63)
		  if(we <= 20490)
		   if(we <= 19960)
		    if(we <= 18890)
		     if(t <= 2.75) return 3219.05;
		     if(t > 2.75)
		      if(we <= 18170) return 2970.7;
		      if(we > 18170) return 3276.6;
		    if(we > 18890)
		     if(we <= 19150) return 3321.21666666667;
		     if(we > 19150) return 3512.66666666667;
		   if(we > 19960)
		    if(we <= 20300)
		     if(we <= 20030) return 3349.63333333333;
		     if(we > 20030) return 4525.08333333333;
		    if(we > 20300)
		     if(we <= 20430) return 3696.3;
		     if(we > 20430)
		      if(we <= 20450) return 3785;
		      if(we > 20450) return 3885.88333333333;
		  if(we > 20490)
		   if(t <= 3.22)
		    if(t <= 1.93) return 3082.06666666667;
		    if(t > 1.93)
		     if(we <= 21570) return 3376.71666666667;
		     if(we > 21570) return 3650.33333333333;
		   if(t > 3.22)
		    if(we <= 20590)
		     if(we <= 20550)
		      if(we <= 20510) return 3616.38333333333;
		      if(we > 20510) return 3613.83333333333;
		     if(we > 20550) return 3578.85;
		    if(we > 20590)
		     if(we <= 20640)
		      if(we <= 20610) return 3681.58333333333;
		      if(we > 20610) return 3452.21666666667;
		     if(we > 20640)
		      if(we <= 20700) return 3639.96666666667;
		      if(we > 20700) return 3321.21666666667;
		 if(t > 3.63)
		  if(t <= 4.03)
		   if(we <= 20270)
		    if(t <= 4.02)
		     if(we <= 20210) return 3426.21666666667;
		     if(we > 20210) return 3583.4;
		    if(t > 4.02)
		     if(we <= 19970)
		      if(we <= 18320) return 3075.61666666667;
		      if(we > 18320) return 3135.78333333333;
		     if(we > 19970) return 3308.51666666667;
		   if(we > 20270)
		    if(we <= 20700)
		     if(we <= 20470) return 3502.7;
		     if(we > 20470)
		      if(we <= 20630) return 3393.86666666667;
		      if(we > 20630) return 3430.23333333333;
		    if(we > 20700)
		     if(we <= 20955) return 3075.61666666667;
		     if(we > 20955)
		      if(we <= 21450) return 4353.06666666667;
		      if(we > 21450) return 3663.15;
		  if(t > 4.03)
		   if(t <= 4.25)
		    if(we <= 20360)
		     if(we <= 19600)
		      if(we <= 19510) return 3239.21666666667;
		      if(we > 19510) return 2991.43333333333;
		     if(we > 19600)
		      if(we <= 20000) return 3223;
		      if(we > 20000) return 3496.23333333333;
		    if(we > 20360)
		     if(we <= 20600)
		      if(we <= 20510) return 3571;
		      if(we > 20510) return 3548;
		     if(we > 20600)
		      if(we <= 20740) return 3483.01666666667;
		      if(we > 20740) return 3154.2;
		   if(t > 4.25)
		    if(w <= 1015)
		     if(we <= 20580) return 3502.7;
		     if(we > 20580) return 3421;
		    if(w > 1015) return 5219.26666666667;
		if(p.equals("PP21185"))
		 if(t <= 1.4) return 3082.05;
		 if(t > 1.4) return 3465;
		if(p.equals("PP21187"))
		 if(w <= 1138)
		  if(t <= 1.65) return 2844.73333333333;
		  if(t > 1.65) return 2897.7;
		 if(w > 1138) return 3288.81666666667;
		if(p.equals("PP21188"))
		 if(w <= 1215)
		  if(w <= 1124)
		   if(t <= 3.34) return 3293.75;
		   if(t > 3.34) return 3091.95;
		  if(w > 1124) return 3830.85;
		 if(w > 1215)
		  if(t <= 2.55)
		   if(t <= 1.79) return 3013.93333333333;
		   if(t > 1.79) return 3299.28333333333;
		  if(t > 2.55)
		   if(we <= 25110) return 3011.91666666667;
		   if(we > 25110) return 2895.61666666667;
		if(p.equals("PP21208"))
		 if(we <= 21400)
		  if(w <= 1012)
		   if(w <= 1000)
		    if(we <= 20300)
		     if(t <= 2.13)
		      if(t <= 2.12) return 3429.56666666667;
		      if(t > 2.12)
		       if(we <= 19750) return 3044.75;
		       if(we > 19750) return 2996.13333333333;
		     if(t > 2.13)
		      if(we <= 19480)
		       if(t <= 2.24)
		        if(we <= 18850) return 2819.66666666667;
		        if(we > 18850)
		         if(we <= 19080) return 3352.88333333333;
		         if(we > 19080) return 2735.43333333333;
		       if(t > 2.24) return 2683.15;
		      if(we > 19480)
		       if(t <= 2.24)
		        if(we <= 19980) return 2963.05;
		        if(we > 19980) return 3063.1;
		       if(t > 2.24) return 4035.8;
		    if(we > 20300)
		     if(w <= 975)
		      if(t <= 2.11)
		       if(t <= 2) return 3047.66666666667;
		       if(t > 2) return 2886.68333333333;
		      if(t > 2.11)
		       if(t <= 2.23)
		        if(we <= 21051)
		         if(we <= 20870) return 2683.15;
		         if(we > 20870) return 3081.8;
		        if(we > 21051) return 2882.48333333333;
		       if(t > 2.23) return 3155.81666666667;
		     if(w > 975)
		      if(we <= 20610)
		       if(we <= 20570)
		        if(we <= 20440) return 2886.68333333333;
		        if(we > 20440) return 3773.25;
		       if(we > 20570) return 3295.25;
		      if(we > 20610)
		       if(t <= 2.24)
		        if(we <= 20650) return 3656.73333333333;
		        if(we > 20650) return 2992.65;
		       if(t > 2.24)
		        if(t <= 2.37) return 3224.06666666667;
		        if(t > 2.37) return 2978.35;
		   if(w > 1000)
		    if(t <= 2.12)
		     if(we <= 20940)
		      if(we <= 20730)
		       if(we <= 19830) return 2909.46666666667;
		       if(we > 19830) return 2990.41666666667;
		      if(we > 20730)
		       if(we <= 20920) return 4706.46666666667;
		       if(we > 20920) return 2909.46666666667;
		     if(we > 20940)
		      if(we <= 21060)
		       if(we <= 21000) return 3291.85;
		       if(we > 21000) return 3262.8;
		      if(we > 21060) return 2920.75;
		    if(t > 2.12)
		     if(we <= 20600)
		      if(we <= 19170) return 3082.06666666667;
		      if(we > 19170)
		       if(t <= 2.13)
		        if(we <= 20340) return 2849.58333333333;
		        if(we > 20340)
		         if(we <= 20420) return 3216.3;
		         if(we > 20420) return 3438.86666666667;
		       if(t > 2.13) return 3039.98333333333;
		     if(we > 20600)
		      if(t <= 2.21)
		       if(we <= 20710)
		        if(we <= 20670) return 2990.41666666667;
		        if(we > 20670) return 3134.23333333333;
		       if(we > 20710)
		        if(we <= 20740) return 2849.58333333333;
		        if(we > 20740) return 2979.05;
		      if(t > 2.21)
		       if(t <= 2.52) return 4447.13333333333;
		       if(t > 2.52)
		        if(we <= 20710) return 2849.41666666667;
		        if(we > 20710) return 3081.08333333333;
		  if(w > 1012)
		   if(t <= 2.45)
		    if(w <= 1036)
		     if(we <= 17830)
		      if(we <= 17300) return 3060.01666666667;
		      if(we > 17300) return 3423.71666666667;
		     if(we > 17830)
		      if(we <= 18000) return 3397;
		      if(we > 18000)
		       if(we <= 18170) return 3182.11666666667;
		       if(we > 18170) return 2788.31666666667;
		    if(w > 1036)
		     if(w <= 1095)
		      if(w <= 1058)
		       if(we <= 20630) return 2961;
		       if(we > 20630) return 2909.46666666667;
		      if(w > 1058) return 3060.05;
		     if(w > 1095)
		      if(w <= 1201) return 2761.9;
		      if(w > 1201) return 3125.9;
		   if(t > 2.45)
		    if(w <= 1099)
		     if(w <= 1036)
		      if(we <= 20820)
		       if(t <= 3.01) return 2946.95;
		       if(t > 3.01)
		        if(t <= 3.15) return 2761.9;
		        if(t > 3.15) return 2832;
		      if(we > 20820)
		       if(we <= 20900) return 3925.35;
		       if(we > 20900)
		        if(t <= 2.91) return 3241;
		        if(t > 2.91) return 3429.56666666667;
		     if(w > 1036)
		      if(w <= 1070)
		       if(w <= 1060) return 3072.2;
		       if(w > 1060) return 2735.43333333333;
		      if(w > 1070)
		       if(we <= 20010) return 2780.53333333333;
		       if(we > 20010) return 2898.68333333333;
		    if(w > 1099)
		     if(w <= 1134)
		      if(we <= 20590) return 2795.81666666667;
		      if(we > 20590)
		       if(we <= 20970) return 3220.61666666667;
		       if(we > 20970) return 2852.85;
		     if(w > 1134)
		      if(w <= 1174) return 3222.36666666667;
		      if(w > 1174)
		       if(we <= 17980)
		        if(we <= 17790) return 3126.73333333333;
		        if(we > 17790) return 4494.58333333333;
		       if(we > 17980) return 3015.91666666667;
		 if(we > 21400)
		  if(w <= 1150)
		   if(w <= 1119)
		    if(t <= 2.83)
		     if(w <= 1073)
		      if(we <= 22240)
		       if(we <= 21740)
		        if(w <= 1055) return 2937.45;
		        if(w > 1055)
		         if(t <= 2.45) return 3163.35;
		         if(t > 2.45) return 3216.36666666667;
		       if(we > 21740)
		        if(we <= 21820) return 3155.53333333333;
		        if(we > 21820) return 2928.21666666667;
		      if(we > 22240)
		       if(w <= 1064)
		        if(we <= 22640) return 3155.18333333333;
		        if(we > 22640) return 3301.53333333333;
		       if(w > 1064)
		        if(t <= 2) return 3364.83333333333;
		        if(t > 2) return 3006;
		     if(w > 1073)
		      if(w <= 1113)
		       if(w <= 1084) return 3092.1;
		       if(w > 1084)
		        if(t <= 1.99) return 3829.1;
		        if(t > 1.99) return 3435;
		      if(w > 1113)
		       if(t <= 2.24)
		        if(we <= 22960) return 3291.05;
		        if(we > 22960) return 3249;
		       if(t > 2.24) return 2891.21666666667;
		    if(t > 2.83)
		     if(w <= 1068)
		      if(we <= 22020)
		       if(we <= 21900) return 3155.53333333333;
		       if(we > 21900) return 2902.7;
		      if(we > 22020)
		       if(we <= 22140) return 2957;
		       if(we > 22140) return 2975.15;
		     if(w > 1068)
		      if(t <= 2.94)
		       if(w <= 1087)
		        if(we <= 21550) return 3258.31666666667;
		        if(we > 21550) return 3154.2;
		       if(w > 1087)
		        if(t <= 2.9) return 3102;
		        if(t > 2.9) return 3317.4;
		      if(t > 2.94)
		       if(w <= 1100) return 3705.05;
		       if(w > 1100)
		        if(we <= 23540) return 3197.03333333333;
		        if(we > 23540) return 3460.83333333333;
		   if(w > 1119)
		    if(w <= 1127)
		     if(w <= 1120)
		      if(we <= 22480)
		       if(we <= 22070)
		        if(we <= 21870) return 3067.21666666667;
		        if(we > 21870) return 2795.81666666667;
		       if(we > 22070)
		        if(we <= 22210) return 2984.11666666667;
		        if(we > 22210) return 3060;
		      if(we > 22480)
		       if(we <= 23470)
		        if(we <= 23160)
		         if(we <= 22990) return 6795.68333333333;
		         if(we > 22990) return 2976.46666666667;
		        if(we > 23160)
		         if(we <= 23380) return 2949.51666666667;
		         if(we > 23380) return 3544.71666666667;
		       if(we > 23470)
		        if(we <= 23550) return 3060;
		        if(we > 23550) return 3209.58333333333;
		     if(w > 1120)
		      if(t <= 2.86) return 3085.6;
		      if(t > 2.86)
		       if(we <= 24480) return 3189.65;
		       if(we > 24480) return 3529.36666666667;
		    if(w > 1127)
		     if(w <= 1133)
		      if(w <= 1131)
		       if(t <= 2)
		        if(we <= 22550)
		         if(we <= 22430) return 2849.26666666667;
		         if(we > 22430) return 3277.65;
		        if(we > 22550) return 2966.91666666667;
		       if(t > 2)
		        if(t <= 2.45) return 3133.18333333333;
		        if(t > 2.45) return 3399.75;
		      if(w > 1131)
		       if(we <= 24520) return 2965.93333333333;
		       if(we > 24520)
		        if(we <= 24670) return 3416.23333333333;
		        if(we > 24670) return 2965.93333333333;
		     if(w > 1133)
		      if(w <= 1138)
		       if(t <= 2.72)
		        if(t <= 2.165) return 3073.48333333333;
		        if(t > 2.165)
		         if(we <= 23700) return 2946.95;
		         if(we > 23700)
		          if(we <= 24160) return 3176.66666666667;
		          if(we > 24160) return 3281.53333333333;
		       if(t > 2.72)
		        if(t <= 3.1) return 3189.65;
		        if(t > 3.1) return 3253.03333333333;
		      if(w > 1138)
		       if(t <= 2.8) return 3154;
		       if(t > 2.8)
		        if(t <= 2.93) return 3025;
		        if(t > 2.93) return 2886.68333333333;
		  if(w > 1150)
		   if(t <= 2.13)
		    if(w <= 1187)
		     if(w <= 1172)
		      if(t <= 2)
		       if(w <= 1164)
		        if(w <= 1156) return 3446.8;
		        if(w > 1156) return 2920;
		       if(w > 1164)
		        if(we <= 24420) return 3039.98333333333;
		        if(we > 24420) return 3581.65;
		      if(t > 2)
		       if(we <= 24550) return 3176.66666666667;
		       if(we > 24550)
		        if(we <= 24630) return 3754.91666666667;
		        if(we > 24630)
		         if(we <= 24720) return 3197.56666666667;
		         if(we > 24720) return 3300.16666666667;
		     if(w > 1172)
		      if(t <= 2)
		       if(t <= 1.89)
		        if(t <= 1.83) return 2975.15;
		        if(t > 1.83) return 5035.51666666667;
		       if(t > 1.89)
		        if(we <= 23720) return 3641.15;
		        if(we > 23720) return 3085.6;
		      if(t > 2) return 3377.75;
		    if(w > 1187)
		     if(w <= 1208)
		      if(w <= 1201) return 3222.36666666667;
		      if(w > 1201)
		       if(t <= 2)
		        if(we <= 24720) return 3252.63333333333;
		        if(we > 24720) return 2925.81666666667;
		       if(t > 2) return 3176.08333333333;
		     if(w > 1208)
		      if(w <= 1222)
		       if(we <= 23180) return 3210.1;
		       if(we > 23180)
		        if(we <= 24570) return 3376.38333333333;
		        if(we > 24570) return 3210.9;
		      if(w > 1222)
		       if(t <= 2)
		        if(t <= 1.89) return 3295.96666666667;
		        if(t > 1.89)
		         if(we <= 24750) return 3015.91666666667;
		         if(we > 24750) return 3127.78333333333;
		       if(t > 2)
		        if(t <= 2.12) return 3376.38333333333;
		        if(t > 2.12)
		         if(we <= 24130) return 3104.38333333333;
		         if(we > 24130) return 3365.21666666667;
		   if(t > 2.13)
		    if(t <= 2.63)
		     if(w <= 1205)
		      if(we <= 24590)
		       if(t <= 2.5) return 3233.63333333333;
		       if(t > 2.5)
		        if(we <= 24350) return 2941;
		        if(we > 24350) return 3060.05;
		      if(we > 24590)
		       if(we <= 24830)
		        if(we <= 24690) return 3225.21666666667;
		        if(we > 24690) return 3048.48333333333;
		       if(we > 24830)
		        if(we <= 24880) return 2902.7;
		        if(we > 24880) return 3309.71666666667;
		     if(w > 1205)
		      if(w <= 1220)
		       if(t <= 2.4) return 3407.6;
		       if(t > 2.4) return 3272.96666666667;
		      if(w > 1220)
		       if(we <= 24670)
		        if(we <= 24490) return 3272.5;
		        if(we > 24490) return 3429.65;
		       if(we > 24670)
		        if(we <= 24960) return 3366.35;
		        if(we > 24960) return 2393.91666666667;
		    if(t > 2.63)
		     if(t <= 2.78)
		      if(t <= 2.72)
		       if(w <= 1162) return 3399.75;
		       if(w > 1162)
		        if(we <= 24650) return 3210.1;
		        if(we > 24650) return 3039.98333333333;
		      if(t > 2.72)
		       if(w <= 1173) return 3077.91666666667;
		       if(w > 1173) return 3194;
		     if(t > 2.78)
		      if(t <= 3.1)
		       if(t <= 2.87)
		        if(w <= 1164) return 3073.4;
		        if(w > 1164) return 2996.13333333333;
		       if(t > 2.87) return 3197.03333333333;
		      if(t > 3.1)
		       if(t <= 3.33)
		        if(we <= 24780) return 3276.91666666667;
		        if(we > 24780) return 3401.06666666667;
		       if(t > 3.33)
		        if(t <= 3.45) return 2963.05;
		        if(t > 3.45) return 3397.28333333333;
		if(p.equals("PP21209"))
		 if(t <= 4.08)
		  if(t <= 3.34) return 3210.35;
		  if(t > 3.34)
		   if(we <= 18950) return 4210.7;
		   if(we > 18950) return 3605.65;
		 if(t > 4.08)
		  if(we <= 18950) return 3070;
		  if(we > 18950) return 3075.7;
		if(p.equals("PP21288")) return 3485.1;
		if(p.equals("PP21292"))
		 if(t <= 1.53)
		  if(w <= 1230) return 3310.93333333333;
		  if(w > 1230)
		   if(we <= 18390) return 3208.56666666667;
		   if(we > 18390) return 3140.23333333333;
		 if(t > 1.53)
		  if(we <= 18390) return 4115.96666666667;
		  if(we > 18390) return 3972.46666666667;
		if(p.equals("PP21312"))
		 if(t <= 2.63) return 2570;
		 if(t > 2.63) return 2815.75;
		if(p.equals("PP21320"))
		 if(t <= 2.45)
		  if(t <= 2.37)
		   if(t <= 2.27)
		    if(we <= 9078)
		     if(we <= 8462)
		      if(we <= 5428)
		       if(we <= 4526) return 2607.68333333333;
		       if(we > 4526) return 2839.85;
		      if(we > 5428) return 3187;
		     if(we > 8462)
		      if(w <= 478)
		       if(w <= 474) return 3245.15;
		       if(w > 474)
		        if(we <= 8726)
		         if(we <= 8524) return 2713.58333333333;
		         if(we > 8524) return 2715.83333333333;
		        if(we > 8726)
		         if(we <= 8957) return 3249.21666666667;
		         if(we > 8957) return 2715.83333333333;
		      if(w > 478) return 2973.91666666667;
		    if(we > 9078)
		     if(we <= 9408)
		      if(w <= 478)
		       if(we <= 9309) return 3294.58333333333;
		       if(we > 9309) return 2962.26666666667;
		      if(w > 478)
		       if(w <= 483) return 2400.83333333333;
		       if(w > 483) return 2881.78333333333;
		     if(we > 9408)
		      if(w <= 535)
		       if(we <= 9548) return 2906.8;
		       if(we > 9548) return 2863.95;
		      if(w > 535) return 3113.33333333333;
		   if(t > 2.27)
		    if(t <= 2.36)
		     if(w <= 384)
		      if(we <= 7618) return 2815.95;
		      if(we > 7618) return 2676;
		     if(w > 384)
		      if(w <= 386) return 2848.71666666667;
		      if(w > 386) return 2835.78333333333;
		    if(t > 2.36)
		     if(w <= 543) return 2916.71666666667;
		     if(w > 543) return 2555.31666666667;
		  if(t > 2.37)
		   if(w <= 543)
		    if(w <= 437)
		     if(we <= 8761)
		      if(we <= 8494) return 3013.45;
		      if(we > 8494)
		       if(we <= 8554) return 3093.15;
		       if(we > 8554) return 3047.13333333333;
		     if(we > 8761) return 3483.96666666667;
		    if(w > 437)
		     if(we <= 8814)
		      if(we <= 8480)
		       if(we <= 6199) return 2585.61666666667;
		       if(we > 6199) return 2920.05;
		      if(we > 8480) return 3204.7;
		     if(we > 8814)
		      if(we <= 9224)
		       if(we <= 9105)
		        if(we <= 8941) return 3245;
		        if(we > 8941) return 3243.98333333333;
		       if(we > 9105) return 3263.76666666667;
		      if(we > 9224)
		       if(we <= 9283) return 3027.31666666667;
		       if(we > 9283)
		        if(we <= 9433) return 3209.41666666667;
		        if(we > 9433) return 3010.35;
		   if(w > 543)
		    if(we <= 12680)
		     if(we <= 11810) return 2668.3;
		     if(we > 11810)
		      if(we <= 12251) return 2833;
		      if(we > 12251) return 3081.91666666667;
		    if(we > 12680)
		     if(we <= 13543)
		      if(we <= 12948) return 2768.73333333333;
		      if(we > 12948)
		       if(we <= 13180)
		        if(we <= 13128) return 3159.16666666667;
		        if(we > 13128) return 2741.63333333333;
		       if(we > 13180)
		        if(we <= 13303) return 3093.65;
		        if(we > 13303)
		         if(we <= 13460) return 3556.2;
		         if(we > 13460) return 3159.16666666667;
		     if(we > 13543)
		      if(we <= 13686) return 3081.91666666667;
		      if(we > 13686) return 2833;
		 if(t > 2.45)
		  if(t <= 2.94)
		   if(we <= 9213)
		    if(w <= 448)
		     if(we <= 8695) return 2996.9;
		     if(we > 8695) return 2650.65;
		    if(w > 448)
		     if(w <= 452) return 2705.86666666667;
		     if(w > 452)
		      if(w <= 646)
		       if(we <= 7668) return 2615.36666666667;
		       if(we > 7668) return 2940;
		      if(w > 646) return 2705.45;
		   if(we > 9213)
		    if(t <= 2.63)
		     if(t <= 2.55) return 2785.81666666667;
		     if(t > 2.55)
		      if(w <= 547) return 3123.93333333333;
		      if(w > 547) return 2999.11666666667;
		    if(t > 2.63)
		     if(t <= 2.78) return 2826.9;
		     if(t > 2.78) return 2311.31666666667;
		  if(t > 2.94)
		   if(t <= 3.79)
		    if(we <= 11292)
		     if(t <= 3.18)
		      if(we <= 10432) return 2800.56666666667;
		      if(we > 10432) return 3213.7;
		     if(t > 3.18)
		      if(t <= 3.64)
		       if(w <= 529) return 2379.15;
		       if(w > 529) return 2717.8;
		      if(t > 3.64) return 2216.26666666667;
		    if(we > 11292)
		     if(we <= 11853)
		      if(t <= 3.4)
		       if(w <= 545) return 2787.21666666667;
		       if(w > 545) return 3148.08333333333;
		      if(t > 3.4)
		       if(t <= 3.64) return 2852.3;
		       if(t > 3.64) return 2878.46666666667;
		     if(we > 11853)
		      if(w <= 576) return 2759.4;
		      if(w > 576)
		       if(t <= 3.34) return 2954.63333333333;
		       if(t > 3.34) return 2770;
		   if(t > 3.79)
		    if(t <= 3.96)
		     if(t <= 3.91)
		      if(we <= 11482) return 2595.86666666667;
		      if(we > 11482) return 2241.43333333333;
		     if(t > 3.91)
		      if(w <= 575) return 2477.71666666667;
		      if(w > 575) return 2379.15;
		    if(t > 3.96)
		     if(w <= 357)
		      if(w <= 353)
		       if(we <= 6970) return 2764;
		       if(we > 6970) return 2944.83333333333;
		      if(w > 353) return 3095.08333333333;
		     if(w > 357)
		      if(w <= 529)
		       if(we <= 9251) return 3333.28333333333;
		       if(we > 9251)
		        if(w <= 523) return 3064.71666666667;
		        if(w > 523)
		         if(w <= 526) return 3163.26666666667;
		         if(w > 526) return 2866.7;
		      if(w > 529) return 2504.5;
		if(p.equals("PP21323")) return 2673.93333333333;
		if(p.equals("PP21328"))
		 if(t <= 4.85)
		  if(t <= 3.4)
		   if(we <= 12331)
		    if(we <= 12230) return 2781.41666666667;
		    if(we > 12230) return 2673.93333333333;
		   if(we > 12331)
		    if(we <= 12420)
		     if(we <= 12370) return 2876.06666666667;
		     if(we > 12370) return 2886.93333333333;
		    if(we > 12420)
		     if(we <= 12483) return 3193.88333333333;
		     if(we > 12483) return 3326.48333333333;
		  if(t > 3.4)
		   if(we <= 11065)
		    if(t <= 4.25) return 2445.48333333333;
		    if(t > 4.25)
		     if(we <= 7756) return 3556.98333333333;
		     if(we > 7756) return 2480.1;
		   if(we > 11065)
		    if(w <= 561)
		     if(we <= 11356) return 2737.8;
		     if(we > 11356) return 2645.26666666667;
		    if(w > 561)
		     if(w <= 568) return 2771.4;
		     if(w > 568) return 2847.31666666667;
		 if(t > 4.85)
		  if(w <= 499)
		   if(t <= 6.1)
		    if(we <= 7893)
		     if(we <= 7633)
		      if(w <= 435) return 2640.61666666667;
		      if(w > 435) return 2687.95;
		     if(we > 7633)
		      if(we <= 7752) return 2718.15;
		      if(we > 7752) return 2652.61666666667;
		    if(we > 7893)
		     if(we <= 8021)
		      if(we <= 7933) return 2933.31666666667;
		      if(we > 7933) return 2748.88333333333;
		     if(we > 8021) return 2962.11666666667;
		   if(t > 6.1)
		    if(t <= 6.5)
		     if(we <= 8047)
		      if(we <= 7808) return 3156.48333333333;
		      if(we > 7808) return 2609.38333333333;
		     if(we > 8047)
		      if(we <= 8268) return 3176.1;
		      if(we > 8268) return 3164.86666666667;
		    if(t > 6.5) return 2851.08333333333;
		  if(w > 499)
		   if(w <= 625)
		    if(t <= 5.56)
		     if(we <= 11475)
		      if(w <= 547) return 2876.05;
		      if(w > 547)
		       if(w <= 556) return 2926.7;
		       if(w > 556)
		        if(we <= 11344) return 3300.93333333333;
		        if(we > 11344) return 2824.86666666667;
		     if(we > 11475)
		      if(w <= 590) return 3323.1;
		      if(w > 590)
		       if(w <= 622)
		        if(we <= 12352) return 2767;
		        if(we > 12352) return 2926.7;
		       if(w > 622) return 2846.9;
		    if(t > 5.56)
		     if(w <= 521) return 3165.26666666667;
		     if(w > 521) return 2766.73333333333;
		   if(w > 625)
		    if(we <= 11074)
		     if(we <= 11031)
		      if(we <= 10989) return 2559.68333333333;
		      if(we > 10989) return 2767;
		     if(we > 11031) return 2795.93333333333;
		    if(we > 11074)
		     if(we <= 12255) return 2562.71666666667;
		     if(we > 12255)
		      if(we <= 12424) return 3172.48333333333;
		      if(we > 12424) return 2945.31666666667;
		if(p.equals("PP21330"))
		 if(t <= 2.05)
		  if(t <= 1.33) return 2654.15;
		  if(t > 1.33)
		   if(we <= 5306) return 2696.75;
		   if(we > 5306) return 2975.73333333333;
		 if(t > 2.05)
		  if(t <= 2.75) return 3276.8;
		  if(t > 2.75) return 2613.48333333333;
		if(p.equals("PP21331"))
		 if(t <= 2.55)
		  if(t <= 1.79) return 2999.51666666667;
		  if(t > 1.79)
		   if(w <= 632) return 2585;
		   if(w > 632) return 3162.2;
		 if(t > 2.55)
		  if(w <= 632) return 2480.1;
		  if(w > 632) return 3265.63333333333;
		if(p.equals("PP21334"))
		 if(t <= 2.35) return 2696.96666666667;
		 if(t > 2.35) return 2907.98333333333;
		if(p.equals("PP21335")) return 3155;
		if(p.equals("PP21338")) return 2567.75;
		if(p.equals("PP21339")) return 3027.31666666667;
		if(p.equals("PP21340")) return 2848.53333333333;
		if(p.equals("PP21341")) return 2996.9;
		if(p.equals("PP21344")) return 2600.18333333333;
		if(p.equals("PP21345")) return 2785;
		if(p.equals("PP21346"))
		 if(t <= 4.85)
		  if(t <= 4.08)
		   if(we <= 10761)
		    if(w <= 478)
		     if(w <= 427)
		      if(t <= 2.89)
		       if(we <= 7139) return 3223.35;
		       if(we > 7139) return 3732.16666666667;
		      if(t > 2.89) return 2278.68333333333;
		     if(w > 427)
		      if(t <= 2.63)
		       if(we <= 9062) return 2973.95;
		       if(we > 9062) return 2625;
		      if(t > 2.63) return 2939.6;
		    if(w > 478)
		     if(w <= 495)
		      if(w <= 482) return 2951.96666666667;
		      if(w > 482) return 3058.95;
		     if(w > 495)
		      if(t <= 2.63)
		       if(w <= 582) return 2989.43333333333;
		       if(w > 582) return 2597.61666666667;
		      if(t > 2.63)
		       if(w <= 508) return 3390.95;
		       if(w > 508) return 3223.35;
		   if(we > 10761)
		    if(w <= 578)
		     if(w <= 534)
		      if(w <= 529) return 2764.73333333333;
		      if(w > 529) return 2858.93333333333;
		     if(w > 534)
		      if(w <= 569)
		       if(we <= 10963) return 3450.01666666667;
		       if(we > 10963)
		        if(we <= 11540)
		         if(t <= 2.63) return 3202.6;
		         if(t > 2.63) return 3572.78333333333;
		        if(we > 11540)
		         if(we <= 11640) return 3450.01666666667;
		         if(we > 11640) return 3572.78333333333;
		      if(w > 569)
		       if(w <= 572) return 3732.16666666667;
		       if(w > 572) return 3202.6;
		    if(w > 578)
		     if(w <= 583) return 3390.95;
		     if(w > 583)
		      if(t <= 2.61) return 3579.3;
		      if(t > 2.61) return 3793.81666666667;
		  if(t > 4.08)
		   if(t <= 4.6)
		    if(t <= 4.52)
		     if(w <= 482)
		      if(w <= 478)
		       if(w <= 456) return 2678.9;
		       if(w > 456)
		        if(we <= 9062) return 2912.61666666667;
		        if(we > 9062) return 2421.75;
		      if(w > 478) return 3057.26666666667;
		     if(w > 482)
		      if(we <= 9884) return 2762.18333333333;
		      if(we > 9884) return 2588.4;
		    if(t > 4.52)
		     if(w <= 535)
		      if(t <= 4.54) return 2903.11666666667;
		      if(t > 4.54) return 2586;
		     if(w > 535)
		      if(w <= 561)
		       if(we <= 11206) return 3083.23333333333;
		       if(we > 11206) return 3335.63333333333;
		      if(w > 561) return 3580;
		   if(t > 4.6)
		    if(we <= 10495)
		     if(w <= 465) return 2903.3;
		     if(w > 465)
		      if(w <= 540) return 3008.58333333333;
		      if(w > 540) return 3323;
		    if(we > 10495)
		     if(w <= 578)
		      if(w <= 569)
		       if(w <= 532) return 2927;
		       if(w > 532)
		        if(we <= 10963) return 3017.28333333333;
		        if(we > 10963)
		         if(w <= 565)
		          if(w <= 548) return 3057.58333333333;
		          if(w > 548) return 3146.9;
		         if(w > 565)
		          if(we <= 11640) return 3017.28333333333;
		          if(we > 11640) return 3057.58333333333;
		      if(w > 569)
		       if(w <= 572) return 2927;
		       if(w > 572) return 3146.9;
		     if(w > 578)
		      if(w <= 583) return 3008.58333333333;
		      if(w > 583) return 3095.31666666667;
		 if(t > 4.85)
		  if(t <= 6.5)
		   if(w <= 544)
		    if(w <= 474)
		     if(w <= 418)
		      if(w <= 392)
		       if(t <= 5.26) return 3038.53333333333;
		       if(t > 5.26) return 2665.16666666667;
		      if(w > 392)
		       if(we <= 8192) return 3294.38333333333;
		       if(we > 8192) return 2858.93333333333;
		     if(w > 418)
		      if(t <= 5.08) return 2878.46666666667;
		      if(t > 5.08) return 2920.05;
		    if(w > 474)
		     if(t <= 5.03)
		      if(t <= 5.02)
		       if(we <= 10258) return 3130;
		       if(we > 10258) return 3403;
		      if(t > 5.02) return 2618.95;
		     if(t > 5.03)
		      if(t <= 5.3)
		       if(w <= 522)
		        if(w <= 519) return 2999.65;
		        if(w > 519) return 2826.9;
		       if(w > 522) return 3136.3;
		      if(t > 5.3)
		       if(t <= 6.1) return 2804.36666666667;
		       if(t > 6.1) return 3136.3;
		   if(w > 544)
		    if(t <= 5.36)
		     if(w <= 600)
		      if(t <= 5.08)
		       if(w <= 557)
		        if(we <= 11101) return 3109.4;
		        if(we > 11101) return 3580;
		       if(w > 557)
		        if(we <= 11340) return 3280.68333333333;
		        if(we > 11340) return 2704.35;
		      if(t > 5.08)
		       if(we <= 12116) return 2939.81666666667;
		       if(we > 12116)
		        if(we <= 12180) return 2833.53333333333;
		        if(we > 12180)
		         if(we <= 12234) return 3491.51666666667;
		         if(we > 12234) return 2833.53333333333;
		     if(w > 600)
		      if(t <= 5.03)
		       if(w <= 613) return 2965;
		       if(w > 613) return 3385.26666666667;
		      if(t > 5.03) return 2903.11666666667;
		    if(t > 5.36)
		     if(t <= 6.17)
		      if(w <= 597)
		       if(t <= 5.8) return 2952.1;
		       if(t > 5.8) return 3002.05;
		      if(w > 597)
		       if(w <= 599) return 3323.1;
		       if(w > 599) return 3083.88333333333;
		     if(t > 6.17)
		      if(w <= 614)
		       if(w <= 573) return 3155.78333333333;
		       if(w > 573) return 3087.91666666667;
		      if(w > 614)
		       if(w <= 617) return 3173.46666666667;
		       if(w > 617) return 3083.65;
		  if(t > 6.5)
		   if(t <= 7.05)
		    if(w <= 540)
		     if(w <= 437)
		      if(w <= 376) return 3304.11666666667;
		      if(w > 376)
		       if(we <= 7722) return 2829.13333333333;
		       if(we > 7722) return 2836.25;
		     if(w > 437)
		      if(w <= 509) return 2781;
		      if(w > 509)
		       if(we <= 10485) return 3190;
		       if(we > 10485) return 2984.61666666667;
		    if(w > 540)
		     if(w <= 582)
		      if(we <= 11101) return 2840.38333333333;
		      if(we > 11101)
		       if(we <= 11298)
		        if(we <= 11242) return 2887.66666666667;
		        if(we > 11242) return 2665.98333333333;
		       if(we > 11298)
		        if(we <= 11577)
		         if(w <= 559) return 2781;
		         if(w > 559) return 2998.53333333333;
		        if(we > 11577) return 2887.66666666667;
		     if(w > 582)
		      if(w <= 598)
		       if(we <= 12157)
		        if(we <= 12089) return 3053.91666666667;
		        if(we > 12089) return 2576.48333333333;
		       if(we > 12157)
		        if(we <= 12191) return 2578.18333333333;
		        if(we > 12191) return 2998.53333333333;
		      if(w > 598)
		       if(w <= 612) return 2635.06666666667;
		       if(w > 612) return 2905.98333333333;
		   if(t > 7.05)
		    if(w <= 525)
		     if(w <= 494)
		      if(w <= 445)
		       if(w <= 412) return 2748.88333333333;
		       if(w > 412) return 3209.88333333333;
		      if(w > 445) return 2622;
		     if(w > 494)
		      if(w <= 519)
		       if(w <= 510) return 3048.73333333333;
		       if(w > 510) return 2844;
		      if(w > 519)
		       if(w <= 522) return 3043.38333333333;
		       if(w > 522) return 3048.73333333333;
		    if(w > 525)
		     if(we <= 11731)
		      if(w <= 573)
		       if(w <= 571) return 2562.71666666667;
		       if(w > 571) return 2374.31666666667;
		      if(w > 573)
		       if(we <= 11409) return 3015.76666666667;
		       if(we > 11409) return 2562.71666666667;
		     if(we > 11731)
		      if(w <= 600)
		       if(w <= 590) return 2619.76666666667;
		       if(w > 590)
		        if(we <= 12014) return 3113.25;
		        if(we > 12014) return 3222.13333333333;
		      if(w > 600)
		       if(w <= 614)
		        if(w <= 613) return 2979.05;
		        if(w > 613) return 2940;
		       if(w > 614)
		        if(we <= 12312) return 3113.25;
		        if(we > 12312) return 3043.38333333333;
		if(p.equals("PP21347")) return 3180.25;
		if(p.equals("PP21348")) return 2886.93333333333;
		if(p.equals("PP21368")) return 2676.73333333333;
		if(p.equals("PP21379"))
		 if(we <= 22580) return 4394.33333333333;
		 if(we > 22580) return 3864.25;
		if(p.equals("PP21389"))
		 if(w <= 544)
		  if(t <= 3.79)
		   if(w <= 503)
		    if(we <= 10059)
		     if(we <= 9864) return 2952.1;
		     if(we > 9864) return 2868.71666666667;
		    if(we > 10059)
		     if(we <= 10197) return 2780.88333333333;
		     if(we > 10197) return 2919.75;
		   if(w > 503)
		    if(w <= 515) return 2930.01666666667;
		    if(w > 515)
		     if(we <= 10650) return 3157.6;
		     if(we > 10650) return 2992;
		  if(t > 3.79)
		   if(w <= 492)
		    if(w <= 484)
		     if(we <= 6802) return 2948.35;
		     if(we > 6802)
		      if(we <= 9402) return 3342;
		      if(we > 9402) return 3193;
		    if(w > 484)
		     if(we <= 9340)
		      if(we <= 9276) return 3172.03333333333;
		      if(we > 9276) return 2890.4;
		     if(we > 9340)
		      if(we <= 9848)
		       if(we <= 9444) return 3103.51666666667;
		       if(we > 9444)
		        if(we <= 9582) return 3286.53333333333;
		        if(we > 9582)
		         if(we <= 9666) return 2890.4;
		         if(we > 9666) return 2958.13333333333;
		      if(we > 9848) return 3103.51666666667;
		   if(w > 492)
		    if(t <= 4.55)
		     if(we <= 10046) return 2138.63333333333;
		     if(we > 10046)
		      if(we <= 10184) return 2964.68333333333;
		      if(we > 10184) return 2964.35;
		    if(t > 4.55)
		     if(t <= 4.6) return 3318.68333333333;
		     if(t > 4.6) return 2969.58333333333;
		 if(w > 544)
		  if(w <= 600)
		   if(t <= 4.08)
		    if(w <= 560)
		     if(w <= 554)
		      if(t <= 3.17) return 2956.43333333333;
		      if(t > 3.17) return 3097.75;
		     if(w > 554) return 2842.21666666667;
		    if(w > 560)
		     if(t <= 3.69) return 2634.08333333333;
		     if(t > 3.69)
		      if(w <= 591) return 5150.76666666667;
		      if(w > 591) return 3158.7;
		   if(t > 4.08)
		    if(t <= 4.8)
		     if(t <= 4.54) return 3115.8;
		     if(t > 4.54) return 3079.63333333333;
		    if(t > 4.8)
		     if(w <= 555) return 2852.3;
		     if(w > 555) return 2920.91666666667;
		  if(w > 600)
		   if(t <= 4.3)
		    if(we <= 13234)
		     if(we <= 12680) return 2981.63333333333;
		     if(we > 12680)
		      if(we <= 12747) return 3198.03333333333;
		      if(we > 12747) return 3368.78333333333;
		    if(we > 13234) return 3233.36666666667;
		   if(t > 4.3) return 2994.03333333333;
		if(p.equals("PP21391"))
		 if(we <= 20440)
		  if(t <= 1.95)
		   if(t <= 1.7)
		    if(w <= 1134)
		     if(we <= 15240) return 2893.63333333333;
		     if(we > 15240)
		      if(we <= 15320)
		       if(we <= 15280) return 3058.51666666667;
		       if(we > 15280) return 2640.2;
		      if(we > 15320)
		       if(we <= 15360) return 2613.95;
		       if(we > 15360) return 2615.03333333333;
		    if(w > 1134)
		     if(we <= 14750)
		      if(we <= 13826) return 2506;
		      if(we > 13826) return 3654;
		     if(we > 14750)
		      if(we <= 15180) return 2404.28333333333;
		      if(we > 15180)
		       if(we <= 15280) return 2551.61666666667;
		       if(we > 15280) return 2506;
		   if(t > 1.7)
		    if(we <= 18010)
		     if(we <= 17960) return 3266.16666666667;
		     if(we > 17960) return 2629;
		    if(we > 18010)
		     if(t <= 1.86) return 2545.18333333333;
		     if(t > 1.86) return 2715.26666666667;
		  if(t > 1.95)
		   if(w <= 995)
		    if(we <= 19280)
		     if(we <= 18570)
		      if(we <= 18090) return 2747.5;
		      if(we > 18090) return 3963.68333333333;
		     if(we > 18570)
		      if(we <= 18630) return 2413.96666666667;
		      if(we > 18630) return 2683;
		    if(we > 19280)
		     if(we <= 20160)
		      if(we <= 19800) return 3535.9;
		      if(we > 19800)
		       if(we <= 19960) return 2996.03333333333;
		       if(we > 19960) return 3140.38333333333;
		     if(we > 20160) return 3459.76666666667;
		   if(w > 995)
		    if(t <= 2.5)
		     if(w <= 1034) return 2461.11666666667;
		     if(w > 1034) return 2554.15;
		    if(t > 2.5) return 3475.06666666667;
		 if(we > 20440)
		  if(w <= 1036)
		   if(w <= 1025)
		    if(t <= 2.5)
		     if(we <= 20890)
		      if(we <= 20680) return 3278.83333333333;
		      if(we > 20680) return 3532.9;
		     if(we > 20890)
		      if(we <= 21280)
		       if(we <= 21200) return 2764.26666666667;
		       if(we > 21200) return 3108.26666666667;
		      if(we > 21280) return 3411.6;
		    if(t > 2.5)
		     if(we <= 20930) return 3258.58333333333;
		     if(we > 20930) return 3213.7;
		   if(w > 1025)
		    if(w <= 1034)
		     if(we <= 21120)
		      if(we <= 20990) return 2590.6;
		      if(we > 20990) return 2778.95;
		     if(we > 21120)
		      if(we <= 21210) return 3697.83333333333;
		      if(we > 21210) return 2590.6;
		    if(w > 1034)
		     if(we <= 21100)
		      if(we <= 20960) return 2657.65;
		      if(we > 20960) return 2534.1;
		     if(we > 21100)
		      if(we <= 21320) return 2717.28333333333;
		      if(we > 21320) return 3350.8;
		  if(w > 1036)
		   if(w <= 1130)
		    if(w <= 1090)
		     if(t <= 2.5)
		      if(w <= 1065) return 3348.4;
		      if(w > 1065) return 3342.11666666667;
		     if(t > 2.5) return 3528.15;
		    if(w > 1090)
		     if(we <= 21090)
		      if(we <= 20890)
		       if(we <= 20840) return 3077.11666666667;
		       if(we > 20840) return 3057;
		      if(we > 20890)
		       if(we <= 20960) return 2714.88333333333;
		       if(we > 20960) return 2855.01666666667;
		     if(we > 21090)
		      if(we <= 22110) return 3409;
		      if(we > 22110) return 3485.05;
		   if(w > 1130)
		    if(we <= 23000)
		     if(we <= 21790)
		      if(we <= 21770) return 3116;
		      if(we > 21770) return 2863.08333333333;
		     if(we > 21790) return 3050.63333333333;
		    if(we > 23000)
		     if(we <= 23340) return 3252.21666666667;
		     if(we > 23340)
		      if(we <= 23660) return 2884.58333333333;
		      if(we > 23660) return 2876.03333333333;
		if(p.equals("PP21393"))
		 if(t <= 4.8)
		  if(t <= 3.34)
		   if(t <= 2.94)
		    if(t <= 2.57)
		     if(t <= 2.51)
		      if(t <= 2.4)
		       if(w <= 411) return 3637;
		       if(w > 411) return 2757.66666666667;
		      if(t > 2.4)
		       if(t <= 2.48)
		        if(w <= 518) return 2843.28333333333;
		        if(w > 518) return 2605.38333333333;
		       if(t > 2.48) return 2878.46666666667;
		     if(t > 2.51)
		      if(w <= 495)
		       if(w <= 482) return 2585;
		       if(w > 482) return 3457.26666666667;
		      if(w > 495)
		       if(w <= 525) return 3007.61666666667;
		       if(w > 525)
		        if(w <= 562) return 3206;
		        if(w > 562) return 3136.1;
		    if(t > 2.57)
		     if(t <= 2.79)
		      if(t <= 2.75)
		       if(we <= 9849) return 3374.78333333333;
		       if(we > 9849) return 3342.28333333333;
		      if(t > 2.75) return 3294.38333333333;
		     if(t > 2.79)
		      if(t <= 2.88) return 2597.61666666667;
		      if(t > 2.88) return 3180.26666666667;
		   if(t > 2.94)
		    if(t <= 3.05)
		     if(we <= 7969)
		      if(t <= 3.01) return 2967;
		      if(t > 3.01)
		       if(w <= 417) return 2713.58333333333;
		       if(w > 417) return 2939.81666666667;
		     if(we > 7969)
		      if(w <= 571)
		       if(w <= 413)
		        if(we <= 8251) return 2973.91666666667;
		        if(we > 8251) return 3170.01666666667;
		       if(w > 413)
		        if(we <= 10654) return 3336.26666666667;
		        if(we > 10654) return 2567.75;
		      if(w > 571)
		       if(w <= 611)
		        if(w <= 586) return 2555.31666666667;
		        if(w > 586) return 3204;
		       if(w > 611)
		        if(w <= 613) return 3176;
		        if(w > 613) return 3243.98333333333;
		    if(t > 3.05)
		     if(t <= 3.26)
		      if(t <= 3.21) return 3659.06666666667;
		      if(t > 3.21) return 2967;
		     if(t > 3.26)
		      if(we <= 9296)
		       if(we <= 6587) return 3413.41666666667;
		       if(we > 6587) return 2992.93333333333;
		      if(we > 9296)
		       if(w <= 519) return 3123.68333333333;
		       if(w > 519) return 3334;
		  if(t > 3.34)
		   if(t <= 4.08)
		    if(we <= 8894)
		     if(w <= 504)
		      if(t <= 4.01)
		       if(t <= 3.96)
		        if(w <= 359) return 2619.76666666667;
		        if(w > 359) return 11640.0333333333;
		       if(t > 3.96)
		        if(w <= 388) return 2527.75;
		        if(w > 388) return 2591.21666666667;
		      if(t > 4.01)
		       if(t <= 4.03) return 2785.75;
		       if(t > 4.03) return 2996;
		     if(w > 504)
		      if(w <= 570)
		       if(t <= 4.02)
		        if(w <= 511) return 2480.96666666667;
		        if(w > 511) return 2141.81666666667;
		       if(t > 4.02) return 2812.78333333333;
		      if(w > 570)
		       if(w <= 581) return 2568.06666666667;
		       if(w > 581) return 2770.11666666667;
		    if(we > 8894)
		     if(t <= 3.96)
		      if(t <= 3.5)
		       if(t <= 3.48) return 3005;
		       if(t > 3.48)
		        if(w <= 614) return 2905;
		        if(w > 614) return 2582.33333333333;
		      if(t > 3.5)
		       if(t <= 3.74)
		        if(t <= 3.54) return 2940;
		        if(t > 3.54) return 3035.28333333333;
		       if(t > 3.74)
		        if(w <= 563) return 2671.2;
		        if(w > 563) return 2870.68333333333;
		     if(t > 3.96)
		      if(t <= 4.03)
		       if(w <= 581)
		        if(w <= 533)
		         if(w <= 460) return 2699.3;
		         if(w > 460) return 2622.45;
		        if(w > 533)
		         if(we <= 11048)
		          if(t <= 4.01) return 3048;
		          if(t > 4.01) return 2704.8;
		         if(we > 11048) return 2966.01666666667;
		       if(w > 581)
		        if(w <= 613) return 2958.13333333333;
		        if(w > 613) return 3341.1;
		      if(t > 4.03)
		       if(w <= 522)
		        if(w <= 498) return 2986.43333333333;
		        if(w > 498) return 3136.1;
		       if(w > 522)
		        if(w <= 557) return 2870.68333333333;
		        if(w > 557) return 3183.86666666667;
		   if(t > 4.08)
		    if(t <= 4.51)
		     if(t <= 4.5)
		      if(w <= 539)
		       if(we <= 8155)
		        if(t <= 4.35) return 2678.9;
		        if(t > 4.35) return 2207.68333333333;
		       if(we > 8155)
		        if(w <= 482)
		         if(we <= 8414) return 3048.46666666667;
		         if(we > 8414) return 2483.28333333333;
		        if(w > 482)
		         if(we <= 10213) return 3044;
		         if(we > 10213) return 2915.65;
		      if(w > 539)
		       if(w <= 587) return 2548.91666666667;
		       if(w > 587)
		        if(we <= 12463) return 2645.26666666667;
		        if(we > 12463) return 3130;
		     if(t > 4.5)
		      if(w <= 512)
		       if(w <= 511) return 2905;
		       if(w > 511) return 2829.13333333333;
		      if(w > 512)
		       if(w <= 539)
		        if(we <= 10299) return 2847.58333333333;
		        if(we > 10299) return 3403;
		       if(w > 539) return 2582.33333333333;
		    if(t > 4.51)
		     if(t <= 4.6)
		      if(w <= 535)
		       if(t <= 4.53)
		        if(w <= 475) return 2907.01666666667;
		        if(w > 475) return 2880.95;
		       if(t > 4.53) return 3038.53333333333;
		      if(w > 535)
		       if(w <= 575)
		        if(we <= 9937) return 2770.11666666667;
		        if(we > 9937) return 2884.5;
		       if(w > 575) return 2932.9;
		     if(t > 4.6)
		      if(we <= 9375) return 3209.88333333333;
		      if(we > 9375)
		       if(w <= 600)
		        if(w <= 508) return 2812.5;
		        if(w > 508) return 3378.93333333333;
		       if(w > 600)
		        if(w <= 611) return 3209.88333333333;
		        if(w > 611) return 2812.5;
		 if(t > 4.8)
		  if(t <= 5.12)
		   if(t <= 5)
		    if(t <= 4.92)
		     if(w <= 490)
		      if(w <= 467)
		       if(we <= 8872) return 2634.03333333333;
		       if(we > 8872) return 3091.08333333333;
		      if(w > 467) return 2783.63333333333;
		     if(w > 490)
		      if(w <= 521)
		       if(w <= 496) return 3370.28333333333;
		       if(w > 496) return 2499.13333333333;
		      if(w > 521)
		       if(w <= 567) return 2943.71666666667;
		       if(w > 567) return 3031.95;
		    if(t > 4.92)
		     if(we <= 8546)
		      if(w <= 498)
		       if(we <= 7697) return 2609.38333333333;
		       if(we > 7697) return 2740.7;
		      if(w > 498)
		       if(we <= 7837)
		        if(we <= 7179) return 2374.31666666667;
		        if(we > 7179) return 2635.06666666667;
		       if(we > 7837) return 2924.9;
		     if(we > 8546)
		      if(w <= 564)
		       if(we <= 11240)
		        if(we <= 9805) return 3008.23333333333;
		        if(we > 9805) return 3304.11666666667;
		       if(we > 11240) return 2648.56666666667;
		      if(w > 564)
		       if(w <= 586)
		        if(w <= 571) return 2265.53333333333;
		        if(w > 571) return 2975.3;
		       if(w > 586) return 2665.98333333333;
		   if(t > 5)
		    if(t <= 5.02)
		     if(t <= 5.01)
		      if(w <= 518) return 2825;
		      if(w > 518)
		       if(we <= 6896) return 2374.31666666667;
		       if(we > 6896) return 3025.65;
		     if(t > 5.01)
		      if(w <= 542)
		       if(w <= 450) return 2631.26666666667;
		       if(w > 450) return 3200.31666666667;
		      if(w > 542)
		       if(we <= 10668) return 2998.35;
		       if(we > 10668) return 3322.43333333333;
		    if(t > 5.02)
		     if(t <= 5.03)
		      if(we <= 10771)
		       if(we <= 10409)
		        if(w <= 535) return 3248;
		        if(w > 535) return 3129;
		       if(we > 10409) return 3346.03333333333;
		      if(we > 10771)
		       if(w <= 581)
		        if(w <= 563) return 2932.9;
		        if(w > 563) return 2989.43333333333;
		       if(w > 581)
		        if(w <= 596)
		         if(we <= 11982) return 2966.01666666667;
		         if(we > 11982) return 3033.05;
		        if(w > 596) return 2932.9;
		     if(t > 5.03)
		      if(we <= 11022) return 2998.35;
		      if(we > 11022) return 2906.8;
		  if(t > 5.12)
		   if(t <= 6.5)
		    if(w <= 500)
		     if(t <= 5.99)
		      if(w <= 413)
		       if(we <= 6722)
		        if(w <= 339) return 2805;
		        if(w > 339) return 3480;
		       if(we > 6722)
		        if(w <= 373)
		         if(w <= 364) return 3410.21666666667;
		         if(w > 364) return 2207.68333333333;
		        if(w > 373)
		         if(w <= 381) return 3326.48333333333;
		         if(w > 381) return 3461;
		      if(w > 413)
		       if(w <= 497)
		        if(w <= 456) return 2665.98333333333;
		        if(w > 456) return 2705.11666666667;
		       if(w > 497) return 2548.91666666667;
		     if(t > 5.99)
		      if(w <= 414)
		       if(we <= 6316) return 3362.91666666667;
		       if(we > 6316) return 3184.1;
		      if(w > 414) return 3164.86666666667;
		    if(w > 500)
		     if(t <= 5.99)
		      if(we <= 9723)
		       if(w <= 575)
		        if(w <= 524) return 2846.9;
		        if(w > 524)
		         if(we <= 8662) return 2752.06666666667;
		         if(we > 8662) return 2876.05;
		       if(w > 575)
		        if(we <= 9189)
		         if(we <= 8780) return 3116.63333333333;
		         if(we > 8780) return 2634.06666666667;
		        if(we > 9189)
		         if(we <= 9544) return 2265.53333333333;
		         if(we > 9544) return 2814.9;
		      if(we > 9723)
		       if(w <= 591)
		        if(we <= 10613)
		         if(w <= 528)
		          if(w <= 509) return 2870.25;
		          if(w > 509) return 2846.9;
		         if(w > 528)
		          if(w <= 567) return 2979.51666666667;
		          if(w > 567) return 2635.06666666667;
		        if(we > 10613)
		         if(we <= 11031)
		          if(w <= 543) return 2924.9;
		          if(w > 543) return 2962.11666666667;
		         if(we > 11031)
		          if(w <= 579) return 3030.58333333333;
		          if(w > 579)
		           if(w <= 581) return 2924.9;
		           if(w > 581) return 2962.11666666667;
		       if(w > 591)
		        if(w <= 615) return 2870.25;
		        if(w > 615)
		         if(t <= 5.56) return 3000;
		         if(t > 5.56) return 2265.53333333333;
		     if(t > 5.99)
		      if(t <= 6.17)
		       if(w <= 615) return 2873.66666666667;
		       if(w > 615) return 3322.43333333333;
		      if(t > 6.17)
		       if(we <= 10997)
		        if(we <= 10409)
		         if(w <= 591) return 2972.88333333333;
		         if(w > 591) return 3550.11666666667;
		        if(we > 10409)
		         if(we <= 10771) return 2740.7;
		         if(we > 10771) return 3097.96666666667;
		       if(we > 10997)
		        if(w <= 610)
		         if(w <= 565) return 2560.93333333333;
		         if(w > 565)
		          if(we <= 11951) return 2822.3;
		          if(we > 11951)
		           if(we <= 12206) return 3401.6;
		           if(we > 12206) return 2560.93333333333;
		        if(w > 610) return 2740.7;
		   if(t > 6.5)
		    if(t <= 7.05)
		     if(we <= 9884) return 3058.55;
		     if(we > 9884)
		      if(w <= 530) return 2984.61666666667;
		      if(w > 530)
		       if(w <= 587) return 3424.46666666667;
		       if(w > 587)
		        if(w <= 609)
		         if(w <= 597) return 2940;
		         if(w > 597) return 2686.85;
		        if(w > 609)
		         if(w <= 612) return 2651.5;
		         if(w > 612) return 3015.76666666667;
		    if(t > 7.05)
		     if(t <= 7.5) return 2665.41666666667;
		     if(t > 7.5)
		      if(w <= 542)
		       if(w <= 446) return 2748.88333333333;
		       if(w > 446) return 2686.85;
		      if(w > 542) return 3289.45;
		if(p.equals("PP21394"))
		 if(t <= 2.14)
		  if(we <= 10637)
		   if(t <= 2.1)
		    if(we <= 6569)
		     if(we <= 6269)
		      if(we <= 6014)
		       if(we <= 4606) return 2586;
		       if(we > 4606) return 2833;
		      if(we > 6014) return 3213.7;
		     if(we > 6269) return 3109.4;
		    if(we > 6569)
		     if(w <= 369)
		      if(we <= 6798)
		       if(we <= 6717) return 2833;
		       if(we > 6717) return 2400.83333333333;
		      if(we > 6798)
		       if(we <= 6893) return 2863.95;
		       if(we > 6893) return 2863.08333333333;
		     if(w > 369)
		      if(w <= 524) return 2835.78333333333;
		      if(w > 524) return 3123.68333333333;
		   if(t > 2.1)
		    if(w <= 534)
		     if(we <= 10588) return 2603.98333333333;
		     if(we > 10588) return 2692.7;
		    if(w > 534) return 2958.96666666667;
		  if(we > 10637)
		   if(we <= 11815)
		    if(t <= 2.05)
		     if(w <= 596) return 3011.13333333333;
		     if(w > 596) return 2770;
		    if(t > 2.05)
		     if(t <= 2.11)
		      if(w <= 581) return 2451.73333333333;
		      if(w > 581)
		       if(we <= 11692) return 2531.93333333333;
		       if(we > 11692)
		        if(we <= 11709) return 2499.96666666667;
		        if(we > 11709) return 2659.98333333333;
		     if(t > 2.11)
		      if(w <= 558) return 2709.01666666667;
		      if(w > 558)
		       if(we <= 11170) return 2837.11666666667;
		       if(we > 11170) return 2676.73333333333;
		   if(we > 11815)
		    if(t <= 2.11)
		     if(t <= 2.05)
		      if(w <= 588) return 2543.38333333333;
		      if(w > 588) return 2666.33333333333;
		     if(t > 2.05)
		      if(we <= 12145) return 2754.08333333333;
		      if(we > 12145)
		       if(we <= 12200) return 2742.45;
		       if(we > 12200) return 2524.36666666667;
		    if(t > 2.11)
		     if(t <= 2.12) return 2478.53333333333;
		     if(t > 2.12)
		      if(w <= 599) return 2451.73333333333;
		      if(w > 599) return 2615.18333333333;
		 if(t > 2.14)
		  if(t <= 2.37)
		   if(t <= 2.35)
		    if(t <= 2.15) return 2921.05;
		    if(t > 2.15) return 2696.96666666667;
		   if(t > 2.35)
		    if(w <= 598) return 2663.1;
		    if(w > 598)
		     if(w <= 599)
		      if(we <= 12295) return 2837.53333333333;
		      if(we > 12295) return 2942.33333333333;
		     if(w > 599) return 2728.51666666667;
		  if(t > 2.37)
		   if(t <= 2.73)
		    if(we <= 11011)
		     if(we <= 10400)
		      if(w <= 516) return 2893.16666666667;
		      if(w > 516) return 3045.41666666667;
		     if(we > 10400)
		      if(we <= 10869) return 2138.63333333333;
		      if(we > 10869) return 2901.9;
		    if(we > 11011)
		     if(t <= 2.55)
		      if(w <= 525)
		       if(t <= 2.42)
		        if(we <= 11322) return 3041.93333333333;
		        if(we > 11322) return 2888.5;
		       if(t > 2.42) return 3156.68333333333;
		      if(w > 525)
		       if(we <= 11388) return 2591.93333333333;
		       if(we > 11388) return 2370;
		     if(t > 2.55)
		      if(w <= 619)
		       if(w <= 614) return 3000;
		       if(w > 614)
		        if(we <= 12603) return 2893.16666666667;
		        if(we > 12603)
		         if(we <= 13371) return 2901.9;
		         if(we > 13371) return 3156.68333333333;
		      if(w > 619) return 2888.5;
		   if(t > 2.73)
		    if(t <= 2.84) return 2803.58333333333;
		    if(t > 2.84) return 2281.98333333333;
		if(p.equals("PP21404"))
		 if(we <= 20740)
		  if(we <= 20600)
		   if(w <= 1002)
		    if(we <= 19420)
		     if(we <= 18480) return 3042.51666666667;
		     if(we > 18480) return 3279;
		    if(we > 19420) return 3846.71666666667;
		   if(w > 1002) return 3145.45;
		  if(we > 20600)
		   if(we <= 20680)
		    if(we <= 20640) return 3371.31666666667;
		    if(we > 20640) return 3279;
		   if(we > 20680)
		    if(we <= 20710) return 3309.68333333333;
		    if(we > 20710) return 5306.3;
		 if(we > 20740)
		  if(we <= 20870)
		   if(we <= 20800)
		    if(we <= 20770) return 3252.36666666667;
		    if(we > 20770) return 3283.06666666667;
		   if(we > 20800)
		    if(we <= 20830) return 3231.16666666667;
		    if(we > 20830) return 3033.71666666667;
		  if(we > 20870)
		   if(we <= 20960)
		    if(we <= 20900) return 3313.18333333333;
		    if(we > 20900) return 3481.68333333333;
		   if(we > 20960)
		    if(t <= 6) return 3503.18333333333;
		    if(t > 6)
		     if(we <= 21030) return 3252.36666666667;
		     if(we > 21030) return 2747.96666666667;
		if(p.equals("PP21405"))
		 if(w <= 1169)
		  if(t <= 6.02)
		   if(t <= 5.02) return 3437.88333333333;
		   if(t > 5.02)
		    if(we <= 21720) return 3665.3;
		    if(we > 21720) return 3143.56666666667;
		  if(t > 6.02)
		   if(t <= 7.5)
		    if(w <= 1080) return 3245;
		    if(w > 1080) return 3717.61666666667;
		   if(t > 7.5)
		    if(w <= 1105) return 3585.43333333333;
		    if(w > 1105) return 3369.46666666667;
		 if(w > 1169)
		  if(t <= 5.02) return 3267;
		  if(t > 5.02) return 3553;
		if(p.equals("PP21408"))
		 if(t <= 6.5)
		  if(t <= 5.3) return 3445;
		  if(t > 5.3)
		   if(we <= 19420) return 3084.1;
		   if(we > 19420) return 2893.23333333333;
		 if(t > 6.5)
		  if(t <= 7) return 3727.33333333333;
		  if(t > 7)
		   if(we <= 21390) return 2849.18333333333;
		   if(we > 21390) return 3764.33333333333;
		if(p.equals("PP21409"))
		 if(t <= 6.04)
		  if(w <= 1095)
		   if(we <= 21900)
		    if(we <= 21570)
		     if(we <= 20230) return 3717.33333333333;
		     if(we > 20230) return 2834.4;
		    if(we > 21570) return 2850;
		   if(we > 21900)
		    if(we <= 22040)
		     if(we <= 21970) return 2831.86666666667;
		     if(we > 21970) return 3054.21666666667;
		    if(we > 22040)
		     if(we <= 22120) return 3560.21666666667;
		     if(we > 22120)
		      if(we <= 22160) return 3544.71666666667;
		      if(we > 22160) return 3403.25;
		  if(w > 1095)
		   if(we <= 22720)
		    if(t <= 5.5)
		     if(we <= 21930) return 3092.51666666667;
		     if(we > 21930) return 3687.31666666667;
		    if(t > 5.5) return 3752.7;
		   if(we > 22720)
		    if(we <= 22860)
		     if(we <= 22740) return 3319;
		     if(we > 22740) return 2925.2;
		    if(we > 22860)
		     if(we <= 23020) return 2962.66666666667;
		     if(we > 23020) return 2834.4;
		 if(t > 6.04)
		  if(we <= 20810)
		   if(we <= 20690) return 3058.03333333333;
		   if(we > 20690) return 3756.88333333333;
		  if(we > 20810)
		   if(we <= 21220)
		    if(we <= 20940) return 2971;
		    if(we > 20940) return 3260.05;
		   if(we > 21220)
		    if(we <= 21660) return 3706.56666666667;
		    if(we > 21660) return 3448.61666666667;
		if(p.equals("PP21414"))
		 if(we <= 19460)
		  if(we <= 19080)
		   if(we <= 17810) return 3075.61666666667;
		   if(we > 17810)
		    if(we <= 18260) return 3310.86666666667;
		    if(we > 18260) return 3357.16666666667;
		  if(we > 19080) return 4991.05;
		 if(we > 19460)
		  if(we <= 19630)
		   if(we <= 19560)
		    if(we <= 19540) return 3658;
		    if(we > 19540) return 3327.08333333333;
		   if(we > 19560)
		    if(we <= 19580) return 3413.9;
		    if(we > 19580) return 3420.95;
		  if(we > 19630)
		   if(t <= 4.3)
		    if(we <= 19700) return 3233.21666666667;
		    if(we > 19700) return 3357.16666666667;
		   if(t > 4.3) return 3415.08333333333;
		if(p.equals("PP21419"))
		 if(t <= 1.5)
		  if(we <= 17970) return 2979.75;
		  if(we > 17970) return 3029.86666666667;
		 if(t > 1.5)
		  if(we <= 17970) return 2726.51666666667;
		  if(we > 17970) return 2633.21666666667;
		if(p.equals("PP21420"))
		 if(t <= 5.42)
		  if(we <= 22350)
		   if(we <= 21330)
		    if(we <= 20040)
		     if(t <= 4.7) return 2921.68333333333;
		     if(t > 4.7)
		      if(w <= 1087) return 3101.05;
		      if(w > 1087)
		       if(we <= 12415) return 2364.46666666667;
		       if(we > 12415) return 2893.23333333333;
		    if(we > 20040)
		     if(we <= 21120)
		      if(we <= 20200) return 3347.86666666667;
		      if(we > 20200) return 2771.26666666667;
		     if(we > 21120) return 3435.08333333333;
		   if(we > 21330)
		    if(we <= 21800) return 3288.5;
		    if(we > 21800)
		     if(we <= 22130)
		      if(we <= 22000) return 3051.9;
		      if(we > 22000) return 2954.98333333333;
		     if(we > 22130)
		      if(we <= 22260) return 3030.9;
		      if(we > 22260)
		       if(we <= 22300) return 3154.46666666667;
		       if(we > 22300) return 2893.23333333333;
		  if(we > 22350)
		   if(we <= 22540)
		    if(we <= 22460)
		     if(we <= 22410)
		      if(we <= 22380) return 3009.98333333333;
		      if(we > 22380) return 3100;
		     if(we > 22410)
		      if(we <= 22440) return 3319;
		      if(we > 22440) return 3359.11666666667;
		    if(we > 22460)
		     if(we <= 22500)
		      if(we <= 22480) return 3638.6;
		      if(we > 22480) return 3661.76666666667;
		     if(we > 22500)
		      if(we <= 22520) return 3200.66666666667;
		      if(we > 22520) return 3943.45;
		   if(we > 22540)
		    if(we <= 22680)
		     if(we <= 22600)
		      if(we <= 22570) return 2992.23333333333;
		      if(we > 22570) return 3367.01666666667;
		     if(we > 22600)
		      if(we <= 22640) return 3633.41666666667;
		      if(we > 22640)
		       if(we <= 22660) return 3372.23333333333;
		       if(we > 22660) return 3662.33333333333;
		    if(we > 22680)
		     if(we <= 22740)
		      if(we <= 22710) return 3210.98333333333;
		      if(we > 22710) return 3015;
		     if(we > 22740)
		      if(we <= 22790) return 2992.23333333333;
		      if(we > 22790)
		       if(we <= 22860) return 3185;
		       if(we > 22860) return 3051.9;
		 if(t > 5.42)
		  if(w <= 1040)
		   if(we <= 20310)
		    if(we <= 19600)
		     if(we <= 18390)
		      if(we <= 18030)
		       if(we <= 17550) return 2971;
		       if(we > 17550) return 3026.96666666667;
		      if(we > 18030)
		       if(we <= 18120) return 3209.48333333333;
		       if(we > 18120) return 2979.66666666667;
		     if(we > 18390)
		      if(we <= 18900)
		       if(we <= 18610) return 3153;
		       if(we > 18610)
		        if(we <= 18770) return 3202.6;
		        if(we > 18770) return 3285.01666666667;
		      if(we > 18900)
		       if(we <= 19170) return 2979.66666666667;
		       if(we > 19170)
		        if(we <= 19280) return 3125.21666666667;
		        if(we > 19280) return 3448.41666666667;
		    if(we > 19600)
		     if(we <= 20100)
		      if(we <= 19930)
		       if(we <= 19720) return 3220.98333333333;
		       if(we > 19720) return 2899.98333333333;
		      if(we > 19930)
		       if(we <= 20010) return 3368.63333333333;
		       if(we > 20010) return 3063.53333333333;
		     if(we > 20100)
		      if(we <= 20200)
		       if(we <= 20120) return 3358.43333333333;
		       if(we > 20120) return 3449.3;
		      if(we > 20200)
		       if(we <= 20260) return 3492.76666666667;
		       if(we > 20260) return 3435.08333333333;
		   if(we > 20310)
		    if(we <= 20610)
		     if(we <= 20510)
		      if(we <= 20380)
		       if(we <= 20350) return 3181.25;
		       if(we > 20350) return 3531.46666666667;
		      if(we > 20380)
		       if(we <= 20460) return 2917;
		       if(we > 20460) return 2940.9;
		     if(we > 20510)
		      if(we <= 20560)
		       if(we <= 20530) return 3583.43333333333;
		       if(we > 20530) return 2735.4;
		      if(we > 20560)
		       if(we <= 20580) return 3151.73333333333;
		       if(we > 20580) return 2968.35;
		    if(we > 20610)
		     if(we <= 20730)
		      if(we <= 20660)
		       if(we <= 20630) return 3407.7;
		       if(we > 20630) return 3401.61666666667;
		      if(we > 20660)
		       if(we <= 20690) return 3269.95;
		       if(we > 20690) return 3237.38333333333;
		     if(we > 20730)
		      if(we <= 20800)
		       if(we <= 20750) return 3541.25;
		       if(we > 20750) return 3002.96666666667;
		      if(we > 20800)
		       if(we <= 20820) return 3759.18333333333;
		       if(we > 20820) return 3403.25;
		  if(w > 1040)
		   if(w <= 1087)
		    if(we <= 20870) return 2962.1;
		    if(we > 20870) return 3744;
		   if(w > 1087)
		    if(t <= 5.99) return 2617.31666666667;
		    if(t > 5.99) return 3637.8;
		if(p.equals("PP21423"))
		 if(t <= 1.72)
		  if(t <= 1.11)
		   if(w <= 442)
		    if(w <= 420)
		     if(w <= 389)
		      if(w <= 358) return 3282.85;
		      if(w > 358) return 2956.95;
		     if(w > 389)
		      if(w <= 419) return 3142.73333333333;
		      if(w > 419) return 2508.68333333333;
		    if(w > 420)
		     if(w <= 423) return 2710.53333333333;
		     if(w > 423)
		      if(w <= 440)
		       if(w <= 430) return 3025.65;
		       if(w > 430) return 2932.9;
		      if(w > 440) return 2633.6;
		   if(w > 442)
		    if(t <= 1.01) return 3199.91666666667;
		    if(t > 1.01)
		     if(w <= 495)
		      if(w <= 486) return 3076.88333333333;
		      if(w > 486) return 2578.78333333333;
		     if(w > 495)
		      if(w <= 614) return 2958.56666666667;
		      if(w > 614) return 3076.88333333333;
		  if(t > 1.11)
		   if(w <= 572)
		    if(t <= 1.33)
		     if(t <= 1.31) return 2618.95;
		     if(t > 1.31)
		      if(w <= 387) return 3112.06666666667;
		      if(w > 387)
		       if(we <= 5762) return 2508.68333333333;
		       if(we > 5762) return 2528.3;
		    if(t > 1.33)
		     if(t <= 1.52) return 2873.66666666667;
		     if(t > 1.52) return 2797.5;
		   if(w > 572)
		    if(t <= 1.34)
		     if(w <= 597) return 2822.9;
		     if(w > 597)
		      if(w <= 616) return 2967.88333333333;
		      if(w > 616) return 3025.65;
		    if(t > 1.34) return 2760.9;
		 if(t > 1.72)
		  if(w <= 442)
		   if(w <= 420)
		    if(w <= 387)
		     if(t <= 2.4)
		      if(w <= 358)
		       if(w <= 341) return 2278.08333333333;
		       if(w > 341) return 2681.88333333333;
		      if(w > 358) return 2401.51666666667;
		     if(t > 2.4) return 2421.75;
		    if(w > 387)
		     if(we <= 5616) return 2401.51666666667;
		     if(we > 5616) return 2889.41666666667;
		   if(w > 420)
		    if(w <= 430)
		     if(w <= 423)
		      if(we <= 5919) return 2324;
		      if(we > 5919) return 2728.36666666667;
		     if(w > 423) return 2362.28333333333;
		    if(w > 430)
		     if(w <= 440) return 2724.46666666667;
		     if(w > 440) return 2728.36666666667;
		  if(w > 442)
		   if(t <= 2.35)
		    if(we <= 8055)
		     if(w <= 495)
		      if(w <= 486) return 2469.98333333333;
		      if(w > 486) return 2334.91666666667;
		     if(w > 495)
		      if(w <= 539) return 2278.08333333333;
		      if(w > 539) return 2362.28333333333;
		    if(we > 8055)
		     if(w <= 608)
		      if(w <= 583) return 2687.01666666667;
		      if(w > 583) return 2324;
		     if(w > 608)
		      if(w <= 617) return 2626.85;
		      if(w > 617) return 2469.98333333333;
		   if(t > 2.35) return 2480.1;
		if(p.equals("PP21429")) return 2619.76666666667;
		if(p.equals("PP21433"))
		 if(t <= 3.5)
		  if(w <= 1160.3)
		   if(we <= 23040) return 3160;
		   if(we > 23040)
		    if(we <= 23640) return 3221.3;
		    if(we > 23640) return 3522;
		  if(w > 1160.3) return 3239.21666666667;
		 if(t > 3.5)
		  if(w <= 1160.3)
		   if(we <= 23040) return 3356.55;
		   if(we > 23040)
		    if(we <= 23640) return 3522.98333333333;
		    if(we > 23640) return 3501.03333333333;
		  if(w > 1160.3) return 4953.83333333333;
		if(p.equals("PP21456"))
		 if(t <= 1.47)
		  if(t <= 0.96)
		   if(t <= 0.75)
		    if(t <= 0.66) return 2504.5;
		    if(t > 0.66)
		     if(w <= 616) return 2518.01666666667;
		     if(w > 616) return 2578.78333333333;
		   if(t > 0.75)
		    if(t <= 0.85) return 2654.75;
		    if(t > 0.85) return 2834.81666666667;
		  if(t > 0.96)
		   if(we <= 8755)
		    if(t <= 1.31)
		     if(t <= 1.19)
		      if(w <= 588) return 2899.08333333333;
		      if(w > 588) return 2825.9;
		     if(t > 1.19) return 2353;
		    if(t > 1.31) return 2439.3;
		   if(we > 8755)
		    if(w <= 613)
		     if(t <= 1.11) return 2952.1;
		     if(t > 1.11) return 2704.28333333333;
		    if(w > 613)
		     if(w <= 635)
		      if(w <= 623) return 2627.86666666667;
		      if(w > 623) return 2665.16666666667;
		     if(w > 635) return 2998.35;
		 if(t > 1.47)
		  if(t <= 2)
		   if(t <= 1.99)
		    if(t <= 1.63)
		     if(w <= 584) return 2948.35;
		     if(w > 584) return 2604.35;
		    if(t > 1.63) return 3083.23333333333;
		   if(t > 1.99)
		    if(w <= 618)
		     if(we <= 8280) return 2838.18333333333;
		     if(we > 8280) return 2422;
		    if(w > 618)
		     if(w <= 626) return 2457.7;
		     if(w > 626)
		      if(w <= 640) return 2778.53333333333;
		      if(w > 640) return 2278.08333333333;
		  if(t > 2)
		   if(w <= 480)
		    if(w <= 422) return 2801.53333333333;
		    if(w > 422)
		     if(t <= 2.42) return 2898.81666666667;
		     if(t > 2.42)
		      if(t <= 3.59) return 2870.68333333333;
		      if(t > 3.59) return 3719.6;
		   if(w > 480)
		    if(w <= 584)
		     if(t <= 2.74) return 2849;
		     if(t > 2.74) return 2419.15;
		    if(w > 584)
		     if(t <= 2.165) return 2706.65;
		     if(t > 2.165) return 2385.26666666667;
		if(p.equals("PP21469"))
		 if(t <= 4.52) return 2845.51666666667;
		 if(t > 4.52) return 3605.88333333333;
		if(p.equals("PP21473"))
		 if(t <= 5.99)
		  if(w <= 533)
		   if(t <= 5.24)
		    if(w <= 391)
		     if(w <= 332) return 3719.6;
		     if(w > 332) return 2430.8;
		    if(w > 391)
		     if(t <= 4.25) return 2582.05;
		     if(t > 4.25) return 2640.03333333333;
		   if(t > 5.24)
		    if(w <= 388) return 2797.6;
		    if(w > 388) return 3254.03333333333;
		  if(w > 533)
		   if(w <= 615)
		    if(w <= 581) return 2706.93333333333;
		    if(w > 581)
		     if(we <= 12033) return 3035.06666666667;
		     if(we > 12033) return 2939.7;
		   if(w > 615)
		    if(t <= 4.5) return 2425.35;
		    if(t > 4.5) return 3567.31666666667;
		 if(t > 5.99)
		  if(t <= 6.04)
		   if(w <= 542) return 3526.18333333333;
		   if(w > 542)
		    if(w <= 559) return 2914.11666666667;
		    if(w > 559)
		     if(w <= 575) return 3526.18333333333;
		     if(w > 575) return 2914.11666666667;
		  if(t > 6.04)
		   if(t <= 6.09)
		    if(w <= 591)
		     if(we <= 8662) return 3132.33333333333;
		     if(we > 8662) return 2706.93333333333;
		    if(w > 591) return 8793.65;
		   if(t > 6.09)
		    if(t <= 7.2) return 2810.45;
		    if(t > 7.2) return 2815.61666666667;
		if(p.equals("PP21475"))
		 if(w <= 492)
		  if(t <= 1.215) return 2958.56666666667;
		  if(t > 1.215)
		   if(t <= 1.7) return 2529.8;
		   if(t > 1.7) return 2470;
		 if(w > 492)
		  if(t <= 1.215) return 2962.23333333333;
		  if(t > 1.215)
		   if(t <= 1.7) return 2678.25;
		   if(t > 1.7) return 2828;
		if(p.equals("PP21476"))
		 if(t <= 4.18)
		  if(we <= 10664)
		   if(t <= 3.34)
		    if(w <= 502)
		     if(t <= 3.1)
		      if(t <= 2.42) return 2842.6;
		      if(t > 2.42)
		       if(w <= 329) return 2899.25;
		       if(w > 329) return 3092.5;
		     if(t > 3.1)
		      if(t <= 3.22)
		       if(w <= 467) return 3072.13333333333;
		       if(w > 467)
		        if(we <= 9510) return 2809.03333333333;
		        if(we > 9510) return 2845.21666666667;
		      if(t > 3.22)
		       if(w <= 480)
		        if(w <= 478) return 2877;
		        if(w > 478) return 2876.38333333333;
		       if(w > 480) return 2868.86666666667;
		    if(w > 502)
		     if(t <= 3.1)
		      if(we <= 9935) return 2877;
		      if(we > 9935) return 2876.38333333333;
		     if(t > 3.1)
		      if(w <= 519)
		       if(w <= 509)
		        if(w <= 506) return 2568.06666666667;
		        if(w > 506) return 3225.05;
		       if(w > 509) return 3326.58333333333;
		      if(w > 519)
		       if(w <= 523) return 2893.41666666667;
		       if(w > 523) return 3227.1;
		   if(t > 3.34)
		    if(w <= 544)
		     if(we <= 8313)
		      if(w <= 448) return 2781.43333333333;
		      if(w > 448)
		       if(w <= 503)
		        if(t <= 4.01) return 2809.03333333333;
		        if(t > 4.01) return 2627.86666666667;
		       if(w > 503) return 2809.03333333333;
		     if(we > 8313)
		      if(t <= 4.01)
		       if(w <= 494) return 2728.91666666667;
		       if(w > 494) return 2866.9;
		      if(t > 4.01)
		       if(w <= 517) return 2451.53333333333;
		       if(w > 517) return 3031.18333333333;
		    if(w > 544)
		     if(w <= 549)
		      if(w <= 547) return 2471.7;
		      if(w > 547)
		       if(w <= 548) return 2585;
		       if(w > 548) return 2871.01666666667;
		     if(w > 549)
		      if(w <= 583)
		       if(w <= 566) return 2553.31666666667;
		       if(w > 566) return 2931.3;
		      if(w > 583) return 2799.98333333333;
		  if(we > 10664)
		   if(t <= 2.42)
		    if(we <= 12195)
		     if(we <= 11730) return 3375.21666666667;
		     if(we > 11730)
		      if(w <= 614)
		       if(we <= 11933) return 3244.31666666667;
		       if(we > 11933)
		        if(we <= 12157) return 3366.45;
		        if(we > 12157) return 3245;
		      if(w > 614) return 2740.33333333333;
		    if(we > 12195)
		     if(we <= 12227)
		      if(we <= 12218) return 3316.95;
		      if(we > 12218) return 3091.86666666667;
		     if(we > 12227)
		      if(we <= 12245) return 2869;
		      if(we > 12245) return 2889.33333333333;
		   if(t > 2.42)
		    if(t <= 3.74)
		     if(t <= 3.29)
		      if(t <= 2.86) return 2690.61666666667;
		      if(t > 2.86)
		       if(w <= 577) return 2756.25;
		       if(w > 577) return 2922.71666666667;
		     if(t > 3.29)
		      if(t <= 3.44) return 2783.63333333333;
		      if(t > 3.44)
		       if(w <= 578) return 3053.23333333333;
		       if(w > 578) return 2916.2;
		    if(t > 3.74)
		     if(w <= 579)
		      if(w <= 575)
		       if(w <= 555) return 2705;
		       if(w > 555) return 2451.53333333333;
		      if(w > 575)
		       if(we <= 11494) return 2852.3;
		       if(we > 11494) return 2738.65;
		     if(w > 579)
		      if(w <= 586) return 3131.43333333333;
		      if(w > 586)
		       if(w <= 604) return 2937;
		       if(w > 604) return 2756.25;
		 if(t > 4.18)
		  if(t <= 4.55)
		   if(t <= 4.54)
		    if(t <= 4.53)
		     if(t <= 4.51)
		      if(w <= 562) return 2241.43333333333;
		      if(w > 562) return 3138.5;
		     if(t > 4.51) return 3257.61666666667;
		    if(t > 4.53)
		     if(w <= 553) return 3014.1;
		     if(w > 553) return 3150.16666666667;
		   if(t > 4.54)
		    if(w <= 497)
		     if(w <= 430) return 3342;
		     if(w > 430)
		      if(w <= 493) return 3183.86666666667;
		      if(w > 493)
		       if(w <= 495) return 2740.33333333333;
		       if(w > 495) return 2845.91666666667;
		    if(w > 497)
		     if(w <= 509)
		      if(we <= 10029) return 10939.1166666667;
		      if(we > 10029) return 3000.81666666667;
		     if(w > 509)
		      if(we <= 10015) return 2901.73333333333;
		      if(we > 10015)
		       if(w <= 520) return 2699.45;
		       if(w > 520) return 2916.2;
		  if(t > 4.55)
		   if(t <= 4.6)
		    if(w <= 524) return 3318.68333333333;
		    if(w > 524)
		     if(w <= 530) return 3243.75;
		     if(w > 530)
		      if(w <= 542) return 2721;
		      if(w > 542) return 3017.75;
		   if(t > 4.6)
		    if(t <= 5.02) return 2709.01666666667;
		    if(t > 5.02)
		     if(w <= 522) return 3053.96666666667;
		     if(w > 522) return 3054.71666666667;
		if(p.equals("PP21478")) return 3808.73333333333;
		if(p.equals("PP21484"))
		 if(t <= 2.24)
		  if(t <= 2.04)
		   if(we <= 12053)
		    if(we <= 11530)
		     if(we <= 11443) return 4307.33333333333;
		     if(we > 11443) return 3037.13333333333;
		    if(we > 11530) return 3590.23333333333;
		   if(we > 12053)
		    if(we <= 13310) return 3470.31666666667;
		    if(we > 13310) return 2879.41666666667;
		  if(t > 2.04)
		   if(we <= 16550) return 3577.8;
		   if(we > 16550)
		    if(we <= 18180) return 7790;
		    if(we > 18180) return 3541.31666666667;
		 if(t > 2.24)
		  if(t <= 2.32)
		   if(we <= 21430)
		    if(we <= 20580) return 3953.58333333333;
		    if(we > 20580) return 2852.85;
		   if(we > 21430)
		    if(we <= 22470) return 3229.55;
		    if(we > 22470) return 2557.45;
		  if(t > 2.32)
		   if(t <= 2.52)
		    if(we <= 22080) return 2973.16666666667;
		    if(we > 22080) return 3062.1;
		   if(t > 2.52)
		    if(we <= 22690)
		     if(we <= 22100) return 4399.71666666667;
		     if(we > 22100) return 3996.3;
		    if(we > 22690) return 6590;
		if(p.equals("PP21485")) return 3561;
		if(p.equals("PP21486"))
		 if(we <= 22520) return 3605.26666666667;
		 if(we > 22520) return 3653.78333333333;
		if(p.equals("PP21487"))
		 if(t <= 2.21)
		  if(t <= 1.66) return 2528.3;
		  if(t > 1.66) return 2836.18333333333;
		 if(t > 2.21)
		  if(t <= 3.1) return 2626.85;
		  if(t > 3.1) return 2282.36666666667;
		if(p.equals("PP21489")) return 2390.93333333333;
		if(p.equals("PP21493")) return 2845.41666666667;
		if(p.equals("PP21504"))
		 if(w <= 486)
		  if(t <= 0.8) return 2694.8;
		  if(t > 0.8) return 2341;
		 if(w > 486)
		  if(t <= 0.8) return 2606;
		  if(t > 0.8) return 2481.13333333333;
		if(p.equals("PP21508"))
		 if(t <= 3.96) return 3015.48333333333;
		 if(t > 3.96) return 3254;
		if(p.equals("PP21516")) return 2920.91666666667;
		if(p.equals("PP21526"))
		 if(t <= 2.32)
		  if(t <= 2.15)
		   if(w <= 552)
		    if(t <= 1.9) return 2988.06666666667;
		    if(t > 1.9)
		     if(w <= 493)
		      if(w <= 400) return 2300.28333333333;
		      if(w > 400) return 2382.11666666667;
		     if(w > 493) return 2362.28333333333;
		   if(w > 552)
		    if(w <= 615) return 2778.53333333333;
		    if(w > 615)
		     if(w <= 618) return 2687.01666666667;
		     if(w > 618) return 2425.35;
		  if(t > 2.15)
		   if(we <= 8068)
		    if(w <= 483) return 2624;
		    if(w > 483) return 2300.28333333333;
		   if(we > 8068) return 2899.36666666667;
		 if(t > 2.32)
		  if(t <= 3.1)
		   if(t <= 2.8) return 2524.88333333333;
		   if(t > 2.8)
		    if(we <= 7467) return 2696.95;
		    if(we > 7467) return 2937.33333333333;
		  if(t > 3.1)
		   if(we <= 8667)
		    if(t <= 3.34) return 7425.58333333333;
		    if(t > 3.34)
		     if(w <= 335) return 2588.8;
		     if(w > 335) return 2427.55;
		   if(we > 8667)
		    if(t <= 3.6) return 2705.11666666667;
		    if(t > 3.6) return 2810.45;
		if(p.equals("PP21529"))
		 if(t <= 1.26) return 2692.15;
		 if(t > 1.26)
		  if(t <= 1.73) return 3504.13333333333;
		  if(t > 1.73) return 3379.88333333333;
		if(p.equals("PP21531")) return 3020;
		if(p.equals("PP21533"))
		 if(t <= 1.95)
		  if(w <= 411) return 2528.3;
		  if(w > 411) return 3020;
		 if(t > 1.95) return 2470;
		if(p.equals("PP21551")) return 3367.4;
		if(p.equals("PP21557"))
		 if(t <= 4.25)
		  if(t <= 3.29)
		   if(t <= 2.7) return 2944.83333333333;
		   if(t > 2.7)
		    if(t <= 3.1) return 2784.88333333333;
		    if(t > 3.1) return 2787.51666666667;
		  if(t > 3.29)
		   if(we <= 7985) return 2610.6;
		   if(we > 7985) return 2477.53333333333;
		 if(t > 4.25)
		  if(t <= 4.7)
		   if(w <= 580) return 2401.51666666667;
		   if(w > 580)
		    if(w <= 1219) return 2599.13333333333;
		    if(w > 1219) return 3131.73333333333;
		  if(t > 4.7) return 2886;
		if(p.equals("PP21563"))
		 if(t <= 0.85) return 3211.7;
		 if(t > 0.85)
		  if(t <= 1.5) return 3245;
		  if(t > 1.5) return 2671.95;
		if(p.equals("PP21576"))
		 if(t <= 0.99) return 3271.26666666667;
		 if(t > 0.99) return 3633.66666666667;
		if(p.equals("PP21577"))
		 if(t <= 2.24)
		  if(t <= 1.83)
		   if(t <= 1.66)
		    if(t <= 1.25)
		     if(t <= 1.11)
		      if(w <= 517)
		       if(w <= 435) return 2522.25;
		       if(w > 435) return 2578.78333333333;
		      if(w > 517)
		       if(w <= 595) return 10651.4333333333;
		       if(w > 595) return 2634.03333333333;
		     if(t > 1.11)
		      if(w <= 432) return 2795;
		      if(w > 432) return 3053.23333333333;
		    if(t > 1.25)
		     if(t <= 1.3)
		      if(w <= 439) return 2721;
		      if(w > 439) return 2842.41666666667;
		     if(t > 1.3)
		      if(w <= 626) return 2532.6;
		      if(w > 626)
		       if(we <= 8625) return 2912.9;
		       if(we > 8625)
		        if(we <= 9032) return 2942;
		        if(we > 9032) return 2795;
		   if(t > 1.66)
		    if(w <= 567)
		     if(w <= 559)
		      if(t <= 1.77) return 2720.76666666667;
		      if(t > 1.77)
		       if(w <= 383) return 2707.38333333333;
		       if(w > 383) return 2920.05;
		     if(w > 559) return 3269.6;
		    if(w > 567)
		     if(w <= 604)
		      if(w <= 571) return 2897.41666666667;
		      if(w > 571)
		       if(w <= 586) return 3030.58333333333;
		       if(w > 586) return 2835.78333333333;
		     if(w > 604)
		      if(w <= 616) return 2655.08333333333;
		      if(w > 616) return 2897.41666666667;
		  if(t > 1.83)
		   if(t <= 1.99)
		    if(t <= 1.95)
		     if(t <= 1.92) return 2493.68333333333;
		     if(t > 1.92) return 3094.91666666667;
		    if(t > 1.95)
		     if(w <= 587)
		      if(w <= 486) return 2757.66666666667;
		      if(w > 486) return 2585;
		     if(w > 587) return 2986.76666666667;
		   if(t > 1.99)
		    if(t <= 2)
		     if(w <= 541)
		      if(w <= 406) return 2220.88333333333;
		      if(w > 406)
		       if(w <= 461) return 2278.08333333333;
		       if(w > 461) return 2583.85;
		     if(w > 541)
		      if(we <= 9053) return 3875.78333333333;
		      if(we > 9053)
		       if(w <= 598) return 3162.2;
		       if(w > 598) return 2382.11666666667;
		    if(t > 2)
		     if(t <= 2.02)
		      if(w <= 476)
		       if(w <= 412) return 3131.4;
		       if(w > 412) return 2899.45;
		      if(w > 476) return 3030.78333333333;
		     if(t > 2.02) return 2720.76666666667;
		 if(t > 2.24)
		  if(t <= 3.1)
		   if(w <= 457)
		    if(t <= 2.75)
		     if(t <= 2.63)
		      if(we <= 5462) return 2445.48333333333;
		      if(we > 5462) return 2300.28333333333;
		     if(t > 2.63)
		      if(w <= 439)
		       if(we <= 8300) return 2613.48333333333;
		       if(we > 8300) return 2696.78333333333;
		      if(w > 439) return 2880;
		    if(t > 2.75)
		     if(t <= 2.9) return 2354.2;
		     if(t > 2.9) return 2699.3;
		   if(w > 457)
		    if(t <= 2.63)
		     if(we <= 8625) return 2227.3;
		     if(we > 8625) return 2425.35;
		    if(t > 2.63)
		     if(we <= 9540)
		      if(w <= 536) return 2574.86666666667;
		      if(w > 536) return 2645.26666666667;
		     if(we > 9540)
		      if(we <= 12970)
		       if(w <= 604) return 2436.36666666667;
		       if(w > 604)
		        if(w <= 618) return 2937.51666666667;
		        if(w > 618) return 2436.36666666667;
		      if(we > 12970) return 2856.63333333333;
		  if(t > 3.1)
		   if(t <= 3.24)
		    if(we <= 10878)
		     if(we <= 8314)
		      if(we <= 7546) return 2887.66666666667;
		      if(we > 7546) return 2427.55;
		     if(we > 8314) return 2480.1;
		    if(we > 10878)
		     if(we <= 11795)
		      if(we <= 11586) return 2876.06666666667;
		      if(we > 11586) return 3180.25;
		     if(we > 11795) return 7425.58333333333;
		   if(t > 3.24)
		    if(t <= 3.4)
		     if(w <= 597)
		      if(w <= 566) return 2591.21666666667;
		      if(w > 566) return 3265.63333333333;
		     if(w > 597)
		      if(we <= 12443)
		       if(we <= 12085) return 2797.8;
		       if(we > 12085) return 2708.66666666667;
		      if(we > 12443) return 2723.33333333333;
		    if(t > 3.4)
		     if(t <= 3.84) return 2706.65;
		     if(t > 3.84)
		      if(we <= 7608) return 2737.8;
		      if(we > 7608) return 2856.16666666667;
		if(p.equals("PP21578"))
		 if(w <= 1060) return 2911.66666666667;
		 if(w > 1060)
		  if(we <= 22920) return 3087.28333333333;
		  if(we > 22920) return 2959.65;
		if(p.equals("PP21580"))
		 if(w <= 475)
		  if(t <= 1.79) return 2870.33333333333;
		  if(t > 1.79) return 3245;
		 if(w > 475)
		  if(we <= 11217)
		   if(t <= 1.65) return 2928.8;
		   if(t > 1.65)
		    if(w <= 558) return 3117.23333333333;
		    if(w > 558) return 2992.3;
		  if(we > 11217)
		   if(w <= 590)
		    if(we <= 11524) return 3225.05;
		    if(we > 11524) return 2945.08333333333;
		   if(w > 590) return 2928.8;
		if(p.equals("PP21592"))
		 if(t <= 1.41)
		  if(w <= 435)
		   if(w <= 408) return 2720.76666666667;
		   if(w > 408) return 2578.78333333333;
		  if(w > 435)
		   if(w <= 501) return 2988.73333333333;
		   if(w > 501)
		    if(w <= 562) return 2884.5;
		    if(w > 562) return 2785.75;
		 if(t > 1.41)
		  if(t <= 1.73)
		   if(w <= 501) return 2575.03333333333;
		   if(w > 501) return 2493.68333333333;
		  if(t > 1.73)
		   if(w <= 488) return 2574.86666666667;
		   if(w > 488)
		    if(w <= 562) return 2354.2;
		    if(w > 562) return 2574.86666666667;
		if(p.equals("PP21597")) return 3163.23333333333;
		if(p.equals("PP21601"))
		 if(w <= 259)
		  if(w <= 252) return 2679.18333333333;
		  if(w > 252)
		   if(t <= 5.42) return 2545.3;
		   if(t > 5.42) return 2629.33333333333;
		 if(w > 259)
		  if(w <= 272)
		   if(t <= 4.02) return 2517.65;
		   if(t > 4.02) return 2640.5;
		  if(w > 272)
		   if(t <= 5.01) return 3516.55;
		   if(t > 5.01) return 2787;
		if(p.equals("PP21605")) return 3288.81666666667;
		if(p.equals("PP21620"))
		 if(t <= 2.09)
		  if(we <= 16970) return 3548.65;
		  if(we > 16970) return 3339.26666666667;
		 if(t > 2.09)
		  if(we <= 22020) return 3210.9;
		  if(we > 22020) return 3120.35;
		if(p.equals("PP21632")) return 2599.13333333333;
		if(p.equals("PP21644")) return 2867;
		if(p.equals("PP21647")) return 3068.33333333333;
		if(p.equals("PP21667"))
		 if(t <= 0.66)
		  if(w <= 1213) return 3089.81666666667;
		  if(w > 1213) return 3160.18333333333;
		 if(t > 0.66) return 3478;
		if(p.equals("PP21671")) return 2925.83333333333;
		if(p.equals("PP21677"))
		 if(t <= 3.74) return 3287.76666666667;
		 if(t > 3.74) return 2819.71666666667;
		if(p.equals("PP21684")) return 2830.16666666667;
		if(p.equals("PP21689")) return 2797.23333333333;
		if(p.equals("PP21690")) return 3180;
		if(p.equals("PP21694"))
		 if(w <= 557) return 2928.8;
		 if(w > 557)
		  if(we <= 11069)
		   if(t <= 1.53) return 2899.08333333333;
		   if(t > 1.53) return 2992.3;
		  if(we > 11069) return 2945.08333333333;
		if(p.equals("PP21701"))
		 if(t <= 1.85)
		  if(w <= 1100) return 3864.25;
		  if(w > 1100)
		   if(we <= 13686)
		    if(we <= 13341) return 3821.75;
		    if(we > 13341) return 2714.26666666667;
		   if(we > 13686) return 3690;
		 if(t > 1.85)
		  if(we <= 16620) return 3496.48333333333;
		  if(we > 16620) return 3923;
		if(p.equals("PP21702"))
		 if(t <= 1.9)
		  if(w <= 1162)
		   if(w <= 1120)
		    if(we <= 15820) return 2922.81666666667;
		    if(we > 15820) return 2585.78333333333;
		   if(w > 1120)
		    if(we <= 16220)
		     if(we <= 15830) return 2492.08333333333;
		     if(we > 15830) return 3109.78333333333;
		    if(we > 16220)
		     if(we <= 16420) return 2492.08333333333;
		     if(we > 16420) return 3854.45;
		  if(w > 1162)
		   if(we <= 15930) return 2630;
		   if(we > 15930)
		    if(we <= 16130) return 2550;
		    if(we > 16130) return 3087.7;
		 if(t > 1.9)
		  if(t <= 2.63)
		   if(w <= 1042)
		    if(w <= 1032) return 2409.61666666667;
		    if(w > 1032) return 3484.3;
		   if(w > 1042)
		    if(w <= 1124) return 2416.01666666667;
		    if(w > 1124)
		     if(we <= 23230) return 2893.71666666667;
		     if(we > 23230) return 3643.3;
		  if(t > 2.63)
		   if(w <= 1206)
		    if(we <= 24690) return 3120.35;
		    if(we > 24690) return 3120;
		   if(w > 1206)
		    if(we <= 23020) return 2612.86666666667;
		    if(we > 23020) return 3536;
		if(p.equals("PP21726")) return 2840.9;
		if(p.equals("PP21752")) return 2365.21666666667;
		if(p.equals("PP21753")) return 2699.3;
		if(p.equals("PP21756")) return 2739.51666666667;
		if(p.equals("PP21762")) return 3076.08333333333;
		if(p.equals("PP21769")) return 2811.46666666667;
		if(p.equals("PP21773")) return 2581.93333333333;
		if(p.equals("PP21777"))
		 if(we <= 23260)
		  if(we <= 22650)
		   if(we <= 22300) return 3074.83333333333;
		   if(we > 22300) return 3662.16666666667;
		  if(we > 22650) return 3081.83333333333;
		 if(we > 23260)
		  if(we <= 23830) return 3273.16666666667;
		  if(we > 23830)
		   if(we <= 23860) return 3117.66666666667;
		   if(we > 23860) return 3022;
		if(p.equals("PP21779"))
		 if(w <= 241) return 2876;
		 if(w > 241)
		  if(t <= 2.4) return 2756.85;
		  if(t > 2.4) return 2545.3;
		if(p.equals("PP21782"))
		 if(w <= 236) return 3179.11666666667;
		 if(w > 236)
		  if(w <= 303) return 2781.43333333333;
		  if(w > 303) return 2627;
		if(p.equals("PP21789")) return 2754.9;
		if(p.equals("PP21790")) return 2827.83333333333;
		if(p.equals("PP21791")) return 3612.51666666667;
		if(p.equals("PP21792")) return 3336;
		if(p.equals("PP21794"))
		 if(t <= 2.75)
		  if(we <= 5620) return 2901.21666666667;
		  if(we > 5620) return 2650.65;
		 if(t > 2.75)
		  if(we <= 5620) return 2341.53333333333;
		  if(we > 5620) return 2555;
		if(p.equals("PP21798")) return 11050.85;
		if(p.equals("PP21802"))
		 if(t <= 1.95)
		  if(w <= 374) return 2705.96666666667;
		  if(w > 374) return 3306.25;
		 if(t > 1.95)
		  if(w <= 388) return 2802.15;
		  if(w > 388) return 2905.98333333333;
		if(p.equals("PP21804")) return 2819.71666666667;
		if(p.equals("PP21805"))
		 if(t <= 2.75)
		  if(we <= 5532)
		   if(we <= 5057)
		    if(we <= 4962)
		     if(we <= 4882) return 2584.25;
		     if(we > 4882) return 2781.66666666667;
		    if(we > 4962)
		     if(we <= 4977) return 2500.85;
		     if(we > 4977) return 2757;
		   if(we > 5057)
		    if(we <= 5490)
		     if(we <= 5312)
		      if(we <= 5280) return 2543;
		      if(we > 5280) return 2612.91666666667;
		     if(we > 5312) return 2867.68333333333;
		    if(we > 5490)
		     if(we <= 5505) return 2500.85;
		     if(we > 5505) return 2543;
		  if(we > 5532)
		   if(we <= 5573)
		    if(we <= 5557)
		     if(we <= 5543) return 2621;
		     if(we > 5543) return 3353.85;
		    if(we > 5557)
		     if(we <= 5563) return 2743.8;
		     if(we > 5563) return 2964.23333333333;
		   if(we > 5573)
		    if(we <= 5620)
		     if(we <= 5597) return 2626.26666666667;
		     if(we > 5597) return 2600.33333333333;
		    if(we > 5620)
		     if(we <= 5657) return 2958.28333333333;
		     if(we > 5657) return 2678.41666666667;
		 if(t > 2.75)
		  if(we <= 5496)
		   if(we <= 5196)
		    if(t <= 3.7)
		     if(we <= 4977)
		      if(we <= 4962) return 2297.83333333333;
		      if(we > 4962) return 2187.13333333333;
		     if(we > 4977)
		      if(we <= 4984) return 2527.75;
		      if(we > 4984) return 2137.83333333333;
		    if(t > 3.7) return 2764.73333333333;
		   if(we > 5196)
		    if(we <= 5451)
		     if(we <= 5312) return 2487.18333333333;
		     if(we > 5312) return 2189.18333333333;
		    if(we > 5451)
		     if(we <= 5487) return 2281.35;
		     if(we > 5487) return 2189.18333333333;
		  if(we > 5496)
		   if(we <= 5567) return 2187.13333333333;
		   if(we > 5567)
		    if(we <= 5597)
		     if(we <= 5573) return 2378.8;
		     if(we > 5573) return 2080.55;
		    if(we > 5597) return 2236.11666666667;
		if(p.equals("PP21809")) return 2735.28333333333;
		if(p.equals("PP21814")) return 2678.9;
		if(p.equals("PP21833"))
		 if(w <= 1199)
		  if(t <= 3.96) return 3398.95;
		  if(t > 3.96) return 3592.36666666667;
		 if(w > 1199) return 3027.25;
		if(p.equals("PP21855"))
		 if(we <= 11680)
		  if(t <= 2.24) return 3178.21666666667;
		  if(t > 2.24) return 2956.43333333333;
		 if(we > 11680)
		  if(t <= 2.24) return 3637;
		  if(t > 2.24) return 3180;
		if(p.equals("PP21880"))
		 if(t <= 1.82)
		  if(w <= 980) return 3724.76666666667;
		  if(w > 980)
		   if(we <= 9862) return 3765.46666666667;
		   if(we > 9862)
		    if(we <= 13128) return 4291.63333333333;
		    if(we > 13128) return 2972.43333333333;
		 if(t > 1.82)
		  if(we <= 13341)
		   if(we <= 12950) return 15121.9333333333;
		   if(we > 12950) return 11547.9;
		  if(we > 13341)
		   if(we <= 22900) return 11925.7166666667;
		   if(we > 22900) return 15121.9333333333;
		if(p.equals("PP21884"))
		 if(t <= 2.55)
		  if(w <= 1126) return 3027.25;
		  if(w > 1126) return 3048.65;
		 if(t > 2.55)
		  if(w <= 1126) return 2678.65;
		  if(w > 1126) return 3581;
		if(p.equals("PP21886")) return 3468;
		if(p.equals("PP21890")) return 2773.88333333333;
		if(p.equals("PP21901")) return 2972.55;
		if(p.equals("PP21903"))
		 if(t <= 5.16)
		  if(t <= 3.91)
		   if(t <= 3.5)
		    if(t <= 3.3) return 3142.73333333333;
		    if(t > 3.3) return 2961.41666666667;
		   if(t > 3.5)
		    if(we <= 7456) return 2760.9;
		    if(we > 7456) return 3227.1;
		  if(t > 3.91)
		   if(t <= 4.04) return 2980.53333333333;
		   if(t > 4.04)
		    if(t <= 4.3) return 2861.1;
		    if(t > 4.3) return 3233.36666666667;
		 if(t > 5.16)
		  if(t <= 5.99)
		   if(w <= 490)
		    if(we <= 7397)
		     if(w <= 339) return 2640.61666666667;
		     if(w > 339) return 2704;
		    if(we > 7397) return 3413.13333333333;
		   if(w > 490) return 3435.2;
		  if(t > 5.99)
		   if(t <= 6.1) return 3036.6;
		   if(t > 6.1) return 2846.61666666667;
		if(p.equals("PP21908")) return 2720.08333333333;
		if(p.equals("PP22001"))
		 if(t <= 3.74)
		  if(t <= 3.1)
		   if(t <= 2.15) return 3206.06666666667;
		   if(t > 2.15)
		    if(we <= 14820) return 3295.11666666667;
		    if(we > 14820) return 2489.01666666667;
		  if(t > 3.1)
		   if(we <= 15520)
		    if(we <= 13630) return 2829.56666666667;
		    if(we > 13630) return 2383.16666666667;
		   if(we > 15520)
		    if(we <= 16950) return 3345;
		    if(we > 16950) return 2461.5;
		 if(t > 3.74)
		  if(we <= 14370)
		   if(we <= 12456)
		    if(we <= 12150) return 2219.56666666667;
		    if(we > 12150) return 2721.7;
		   if(we > 12456) return 2716;
		  if(we > 14370)
		   if(t <= 4.25)
		    if(we <= 14710) return 2400.03333333333;
		    if(we > 14710) return 2535.86666666667;
		   if(t > 4.25) return 2714.55;
		if(p.equals("PP22002"))
		 if(t <= 4.5)
		  if(we <= 14210)
		   if(t <= 3.74)
		    if(we <= 13630) return 2829.56666666667;
		    if(we > 13630) return 2383.16666666667;
		   if(t > 3.74)
		    if(we <= 13160)
		     if(we <= 12342) return 3295.11666666667;
		     if(we > 12342) return 3425;
		    if(we > 13160) return 1919.5;
		  if(we > 14210)
		   if(t <= 3.74)
		    if(we <= 15740) return 3328.28333333333;
		    if(we > 15740) return 2818.4;
		   if(t > 3.74)
		    if(we <= 14740)
		     if(we <= 14430) return 2906.31666666667;
		     if(we > 14430) return 2570.33333333333;
		    if(we > 14740) return 2465.83333333333;
		 if(t > 4.5)
		  if(we <= 14470)
		   if(w <= 1222)
		    if(we <= 14270)
		     if(we <= 12810) return 2364.46666666667;
		     if(we > 12810)
		      if(we <= 14060)
		       if(we <= 13820) return 2615.05;
		       if(we > 13820) return 2672.06666666667;
		      if(we > 14060)
		       if(we <= 14130) return 2742.06666666667;
		       if(we > 14130) return 2659;
		    if(we > 14270)
		     if(we <= 14370)
		      if(we <= 14300) return 2446.3;
		      if(we > 14300) return 2585.48333333333;
		     if(we > 14370)
		      if(we <= 14430) return 2782.23333333333;
		      if(we > 14430) return 2400.03333333333;
		   if(w > 1222) return 3112.53333333333;
		  if(we > 14470)
		   if(we <= 14680)
		    if(we <= 14580)
		     if(we <= 14550) return 2719.25;
		     if(we > 14550) return 2525;
		    if(we > 14580)
		     if(we <= 14642) return 2510.83333333333;
		     if(we > 14642)
		      if(we <= 14660) return 2550;
		      if(we > 14660) return 2699.65;
		   if(we > 14680)
		    if(w <= 1164) return 3128.38333333333;
		    if(w > 1164)
		     if(we <= 14740)
		      if(we <= 14700) return 2824.61666666667;
		      if(we > 14700) return 2654.38333333333;
		     if(we > 14740)
		      if(we <= 14790) return 2497.3;
		      if(we > 14790) return 3078;
		if(p.equals("PP22003"))
		 if(t <= 3.74)
		  if(t <= 2.9)
		   if(we <= 14642) return 2525;
		   if(we > 14642) return 2551.03333333333;
		  if(t > 2.9)
		   if(we <= 14670) return 2383.16666666667;
		   if(we > 14670) return 2818.4;
		 if(t > 3.74)
		  if(we <= 12420)
		   if(we <= 12150) return 2219.56666666667;
		   if(we > 12150) return 2721.7;
		  if(we > 12420)
		   if(we <= 14430) return 2716;
		   if(we > 14430) return 2400.03333333333;
		if(p.equals("PP22005")) return 2747;
		if(p.equals("PP22006"))
		 if(we <= 14470)
		  if(w <= 1222)
		   if(we <= 14270)
		    if(we <= 13128)
		     if(we <= 12415) return 2364.46666666667;
		     if(we > 12415) return 2372;
		    if(we > 13128)
		     if(we <= 14050) return 2615.05;
		     if(we > 14050) return 2391.53333333333;
		   if(we > 14270)
		    if(we <= 14320)
		     if(we <= 14290) return 2446.3;
		     if(we > 14290) return 2416.95;
		    if(we > 14320)
		     if(we <= 14400) return 2936.76666666667;
		     if(we > 14400) return 2400.03333333333;
		  if(w > 1222) return 3112.53333333333;
		 if(we > 14470)
		  if(we <= 14680)
		   if(we <= 14580)
		    if(we <= 14550) return 2706.08333333333;
		    if(we > 14550) return 2358.9;
		   if(we > 14580)
		    if(we <= 14642) return 2364.46666666667;
		    if(we > 14642)
		     if(we <= 14660) return 2550;
		     if(we > 14660) return 2699.65;
		  if(we > 14680)
		   if(w <= 1164) return 3128.38333333333;
		   if(w > 1164)
		    if(we <= 14760)
		     if(we <= 14700) return 2853;
		     if(we > 14700) return 2525;
		    if(we > 14760)
		     if(we <= 14790) return 2342.55;
		     if(we > 14790) return 3078;
		if(p.equals("PP22007"))
		 if(we <= 14610)
		  if(we <= 14270) return 2364.46666666667;
		  if(we > 14270) return 2760;
		 if(we > 14610)
		  if(we <= 14690) return 2510.83333333333;
		  if(we > 14690) return 2681.03333333333;
		if(p.equals("PP22010"))
		 if(t <= 5.66) return 3653.78333333333;
		 if(t > 5.66) return 3501.03333333333;
		if(p.equals("PP22014"))
		 if(we <= 13790) return 3360;
		 if(we > 13790) return 2972.55;
		if(p.equals("PP22018"))
		 if(t <= 3.63)
		  if(w <= 525)
		   if(w <= 492)
		    if(w <= 365) return 2605.38333333333;
		    if(w > 365) return 2400.83333333333;
		   if(w > 492)
		    if(w <= 518) return 3374.46666666667;
		    if(w > 518) return 3093.65;
		  if(w > 525)
		   if(t <= 2.83)
		    if(t <= 2.57)
		     if(we <= 11760) return 3092.73333333333;
		     if(we > 11760) return 2705.86666666667;
		    if(t > 2.57)
		     if(w <= 577) return 3412.26666666667;
		     if(w > 577) return 9492.63333333333;
		   if(t > 2.83)
		    if(t <= 3.15)
		     if(w <= 553) return 3092.73333333333;
		     if(w > 553)
		      if(we <= 11451) return 2721.26666666667;
		      if(we > 11451) return 4907.81666666667;
		    if(t > 3.15) return 2770.11666666667;
		 if(t > 3.63)
		  if(t <= 4.7)
		   if(t <= 4.25)
		    if(w <= 492) return 2665.41666666667;
		    if(w > 492)
		     if(w <= 518) return 2705;
		     if(w > 518) return 2737.8;
		   if(t > 4.25)
		    if(w <= 577)
		     if(w <= 462) return 3306.48333333333;
		     if(w > 462) return 2873.71666666667;
		    if(w > 577) return 2847.58333333333;
		  if(t > 4.7)
		   if(t <= 5)
		    if(we <= 9444) return 3116.63333333333;
		    if(we > 9444)
		     if(w <= 543) return 3306.48333333333;
		     if(w > 543)
		      if(w <= 578) return 3200.83333333333;
		      if(w > 578) return 3306.48333333333;
		   if(t > 5)
		    if(w <= 392) return 3033.66666666667;
		    if(w > 392) return 2748.88333333333;
		if(p.equals("PP22020"))
		 if(t <= 3.5)
		  if(t <= 2.75)
		   if(we <= 24100) return 3373.86666666667;
		   if(we > 24100) return 3500.16666666667;
		  if(t > 2.75)
		   if(we <= 22970) return 3478.11666666667;
		   if(we > 22970) return 3155.06666666667;
		 if(t > 3.5)
		  if(t <= 4.5)
		   if(we <= 24100) return 3010.15;
		   if(we > 24100)
		    if(we <= 24580) return 3438.25;
		    if(we > 24580) return 3291.85;
		  if(t > 4.5)
		   if(we <= 22970) return 3449.46666666667;
		   if(we > 22970) return 3757.75;
		if(p.equals("PP22026"))
		 if(we <= 21300)
		  if(we <= 20900) return 4642.78333333333;
		  if(we > 20900) return 3816.16666666667;
		 if(we > 21300)
		  if(we <= 22160) return 3383.18333333333;
		  if(we > 22160) return 4011;
		if(p.equals("PP22037"))
		 if(we <= 13160) return 3295.11666666667;
		 if(we > 13160) return 2461.5;
		if(p.equals("PP22038")) return 2238.1;
		if(p.equals("PP22041")) return 3499;
		if(p.equals("PP22049"))
		 if(t <= 6.02) return 3339.01666666667;
		 if(t > 6.02)
		  if(t <= 7) return 3129;
		  if(t > 7) return 2972.88333333333;
		if(p.equals("PP22059")) return 3132.33333333333;
		if(p.equals("PP22060"))
		 if(t <= 3.91)
		  if(w <= 510)
		   if(t <= 3.43)
		    if(t <= 3.23) return 2899.25;
		    if(t > 3.23) return 2992.93333333333;
		   if(t > 3.43)
		    if(t <= 3.7) return 2822.9;
		    if(t > 3.7) return 3083.88333333333;
		  if(w > 510)
		   if(we <= 11170)
		    if(w <= 559) return 3360;
		    if(w > 559)
		     if(we <= 10258) return 3036.91666666667;
		     if(we > 10258) return 3098.2;
		   if(we > 11170) return 3361.53333333333;
		 if(t > 3.91)
		  if(w <= 578)
		   if(w <= 567)
		    if(w <= 484)
		     if(w <= 457) return 2795;
		     if(w > 457)
		      if(w <= 463) return 3094.91666666667;
		      if(w > 463) return 2975.8;
		    if(w > 484)
		     if(w <= 528) return 3469.78333333333;
		     if(w > 528) return 3098.2;
		   if(w > 567)
		    if(we <= 11394)
		     if(we <= 11048) return 3021.38333333333;
		     if(we > 11048) return 2845.91666666667;
		    if(we > 11394)
		     if(we <= 11667) return 4541.51666666667;
		     if(we > 11667)
		      if(we <= 11736) return 2720.08333333333;
		      if(we > 11736) return 2931.3;
		  if(w > 578)
		   if(t <= 4.03)
		    if(we <= 11549)
		     if(we <= 11263) return 2935.66666666667;
		     if(we > 11263) return 2993.65;
		    if(we > 11549) return 2955.8;
		   if(t > 4.03)
		    if(t <= 4.25) return 2922.71666666667;
		    if(t > 4.25) return 2850;
		if(p.equals("PP22066"))
		 if(we <= 3554) return 2416.05;
		 if(we > 3554) return 2534;
		if(p.equals("PP22067"))
		 if(we <= 4875) return 2416.05;
		 if(we > 4875) return 2534;
		if(p.equals("PP22069"))
		 if(t <= 1.82)
		  if(t <= 1.63)
		   if(we <= 7383)
		    if(t <= 1.31) return 10708.05;
		    if(t > 1.31) return 3028.51666666667;
		   if(we > 7383)
		    if(we <= 7756) return 2865.08333333333;
		    if(we > 7756)
		     if(we <= 7922) return 2947.65;
		     if(we > 7922) return 2866.8;
		  if(t > 1.63) return 2633.6;
		 if(t > 1.82)
		  if(w <= 594)
		   if(t <= 2.45) return 2950;
		   if(t > 2.45)
		    if(we <= 7383) return 2747;
		    if(we > 7383)
		     if(we <= 7799) return 3109.21666666667;
		     if(we > 7799) return 2419.15;
		  if(w > 594) return 2925.13333333333;
		if(p.equals("PP22071"))
		 if(we <= 15650)
		  if(we <= 14950) return 1919.5;
		  if(we > 14950) return 2334;
		 if(we > 15650)
		  if(we <= 15700) return 2785;
		  if(we > 15700) return 2902.45;
		if(p.equals("PP22072")) return 3066;
		if(p.equals("PP22074")) return 3499;
		if(p.equals("PP22075"))
		 if(we <= 14770)
		  if(we <= 14320) return 1919.5;
		  if(we > 14320) return 2634.5;
		 if(we > 14770) return 2535.86666666667;
		if(p.equals("PP22076")) return 2721.26666666667;
		if(p.equals("PP22079"))
		 if(t <= 1.63) return 2870.33333333333;
		 if(t > 1.63) return 2928.8;
		if(p.equals("PP22085"))
		 if(t <= 2.165)
		  if(w <= 545)
		   if(t <= 1.91)
		    if(w <= 533) return 2906.8;
		    if(w > 533) return 3092.73333333333;
		   if(t > 1.91) return 2840.38333333333;
		  if(w > 545)
		   if(we <= 10409) return 2605.38333333333;
		   if(we > 10409)
		    if(we <= 12850) return 3336.26666666667;
		    if(we > 12850) return 3213.7;
		 if(t > 2.165)
		  if(t <= 3.64)
		   if(t <= 3.15) return 2856.63333333333;
		   if(t > 3.15) return 2873.71666666667;
		  if(t > 3.64)
		   if(t <= 4.25)
		    if(we <= 10258) return 2622.45;
		    if(we > 10258) return 2665.41666666667;
		   if(t > 4.25) return 3188.23333333333;
		if(p.equals("PP22086"))
		 if(we <= 15630) return 2482;
		 if(we > 15630) return 3068.66666666667;
		if(p.equals("PP22104"))
		 if(we <= 21210) return 3759.18333333333;
		 if(we > 21210) return 3870.51666666667;
		if(p.equals("PP22107"))
		 if(w <= 285)
		  if(t <= 1.03) return 2637.35;
		  if(t > 1.03) return 3412.2;
		 if(w > 285)
		  if(w <= 303)
		   if(t <= 1.215) return 2989.35;
		   if(t > 1.215) return 3140;
		  if(w > 303)
		   if(t <= 1.03) return 2813.86666666667;
		   if(t > 1.03) return 2635.4;
		if(p.equals("PP22135")) return 2170.71666666667;
		if(p.equals("PP22136")) return 3970.46666666667;
		if(p.equals("PP22137")) return 2525;
		if(p.equals("PP22157"))
		 if(t <= 2.3) return 2868.5;
		 if(t > 2.3)
		  if(t <= 2.9)
		   if(we <= 4465) return 3002.28333333333;
		   if(we > 4465) return 2862.18333333333;
		  if(t > 2.9) return 2824.96666666667;
		if(p.equals("PP22158")) return 3509.15;
		if(p.equals("PP22160"))
		 if(t <= 4.4) return 3579;
		 if(t > 4.4) return 2934.8;
		if(p.equals("PP22171"))
		 if(t <= 1.44)
		  if(w <= 515)
		   if(we <= 8310) return 2677.28333333333;
		   if(we > 8310) return 2774.55;
		  if(w > 515) return 2720.76666666667;
		 if(t > 1.44)
		  if(w <= 510)
		   if(t <= 2.45)
		    if(we <= 8359) return 2787.51666666667;
		    if(we > 8359) return 3034.48333333333;
		   if(t > 2.45) return 3003.83333333333;
		  if(w > 510)
		   if(w <= 565) return 2643.95;
		   if(w > 565) return 3195;
		if(p.equals("PP22175")) return 2527.78333333333;
		if(p.equals("PP22178"))
		 if(t <= 3.5)
		  if(w <= 1162) return 3368.98333333333;
		  if(w > 1162) return 3373.38333333333;
		 if(t > 3.5)
		  if(w <= 1162) return 3197.83333333333;
		  if(w > 1162) return 2978.86666666667;
		if(p.equals("PP22179"))
		 if(t <= 3.48) return 3155.78333333333;
		 if(t > 3.48)
		  if(t <= 5.08) return 2950.88333333333;
		  if(t > 5.08) return 3282.83333333333;
		if(p.equals("PP22180"))
		 if(t <= 6.1)
		  if(w <= 378)
		   if(w <= 363) return 3286.93333333333;
		   if(w > 363) return 3201.11666666667;
		  if(w > 378)
		   if(we <= 7922) return 2975.8;
		   if(we > 7922) return 3316.3;
		 if(t > 6.1)
		  if(w <= 650)
		   if(w <= 370) return 3088.5;
		   if(w > 370)
		    if(w <= 390) return 2885.9;
		    if(w > 390)
		     if(w <= 419) return 3212.16666666667;
		     if(w > 419) return 2885.9;
		  if(w > 650) return 3656.5;
		if(p.equals("PP22181"))
		 if(t <= 5.7)
		  if(w <= 480) return 2665.16666666667;
		  if(w > 480)
		   if(t <= 3.6) return 2866.8;
		   if(t > 3.6) return 3058.61666666667;
		 if(t > 5.7)
		  if(w <= 650) return 2847.28333333333;
		  if(w > 650) return 3656.5;
		if(p.equals("PP22186")) return 2972.55;
		if(p.equals("PP22201"))
		 if(t <= 5.76) return 2939.6;
		 if(t > 5.76) return 2934.8;
		if(p.equals("PP22204")) return 3247.01666666667;
		if(p.equals("PP22215")) return 2482;
		if(p.equals("PP22232"))
		 if(we <= 23980)
		  if(we <= 23120)
		   if(we <= 22360) return 3785.36666666667;
		   if(we > 22360)
		    if(we <= 22690) return 6585.5;
		    if(we > 22690) return 3127;
		  if(we > 23120)
		   if(we <= 23840) return 3086.1;
		   if(we > 23840) return 3483.88333333333;
		 if(we > 23980)
		  if(we <= 24180) return 3301.53333333333;
		  if(we > 24180) return 2669.76666666667;
		if(p.equals("PP22233"))
		 if(t <= 6.5)
		  if(w <= 517) return 2928.8;
		  if(w > 517) return 3189.6;
		 if(t > 6.5)
		  if(w <= 517) return 3461;
		  if(w > 517) return 3200.81666666667;
		if(p.equals("PP22234")) return 2719.95;
		if(p.equals("PP22236"))
		 if(t <= 1.42) return 2776.1;
		 if(t > 1.42)
		  if(t <= 1.97) return 2527.78333333333;
		  if(t > 1.97) return 3075.7;
		if(p.equals("PP22240")) return 3219.75;
		if(p.equals("PP22243")) return 2820.56666666667;
		if(p.equals("PP22259")) return 3020.95;
		if(p.equals("PP22267")) return 2334;
		if(p.equals("PP22269"))
		 if(we <= 24110)
		  if(t <= 4.3)
		   if(w <= 1213)
		    if(t <= 3.2) return 3612.35;
		    if(t > 3.2) return 3541;
		   if(w > 1213)
		    if(t <= 3.79) return 3969.78333333333;
		    if(t > 3.79) return 4005;
		  if(t > 4.3)
		   if(w <= 1213) return 3505.98333333333;
		   if(w > 1213) return 3541;
		 if(we > 24110)
		  if(w <= 1223) return 3573.23333333333;
		  if(w > 1223) return 3315.48333333333;
		if(p.equals("PP22270"))
		 if(t <= 2.98) return 3189.6;
		 if(t > 2.98) return 3307.25;
		if(p.equals("PP22271"))
		 if(t <= 2.02)
		  if(we <= 17190) return 3490;
		  if(we > 17190) return 3339.26666666667;
		 if(t > 2.02)
		  if(we <= 17500)
		   if(we <= 16300) return 2927;
		   if(we > 16300) return 3376.71666666667;
		  if(we > 17500)
		   if(we <= 17960)
		    if(we <= 17860) return 3219;
		    if(we > 17860) return 3045;
		   if(we > 17960)
		    if(we <= 18020) return 3670.11666666667;
		    if(we > 18020) return 3827.66666666667;
		if(p.equals("PP22272")) return 2788.46666666667;
		if(p.equals("PP22273"))
		 if(t <= 2.19)
		  if(t <= 1.99)
		   if(w <= 1051)
		    if(t <= 1.8) return 3619.55;
		    if(t > 1.8)
		     if(we <= 17310) return 2899.71666666667;
		     if(we > 17310) return 3612.35;
		   if(w > 1051)
		    if(w <= 1105)
		     if(we <= 17450)
		      if(we <= 17310) return 3525.01666666667;
		      if(we > 17310) return 3704.5;
		     if(we > 17450)
		      if(we <= 22420) return 3550.26666666667;
		      if(we > 22420) return 3662.68333333333;
		    if(w > 1105) return 3612.35;
		  if(t > 1.99)
		   if(t <= 2.06) return 3446;
		   if(t > 2.06)
		    if(we <= 17450) return 3430.61666666667;
		    if(we > 17450)
		     if(we <= 22420) return 3778.43333333333;
		     if(we > 22420) return 3766.91666666667;
		 if(t > 2.19)
		  if(we <= 17450)
		   if(w <= 995)
		    if(t <= 3) return 2600;
		    if(t > 3) return 2760;
		   if(w > 995)
		    if(w <= 1051) return 2625;
		    if(w > 1051)
		     if(we <= 17310) return 3505.98333333333;
		     if(we > 17310) return 3270.95;
		  if(we > 17450)
		   if(t <= 3.34)
		    if(t <= 2.8) return 3830.21666666667;
		    if(t > 2.8) return 3099.51666666667;
		   if(t > 3.34) return 4081.45;
		if(p.equals("PP22274")) return 2600;
		if(p.equals("PP22276")) return 2657.25;
		if(p.equals("PP22277")) return 2657.25;
		if(p.equals("PP22279")) return 2673;
		if(p.equals("PP22282")) return 2956.43333333333;
		if(p.equals("PP22285")) return 2781.41666666667;
		if(p.equals("PP22288"))
		 if(t <= 4.25) return 3487.55;
		 if(t > 4.25)
		  if(t <= 4.8) return 2779.08333333333;
		  if(t > 4.8) return 3236.51666666667;
		if(p.equals("PP22289")) return 2759;
		if(p.equals("PP22291"))
		 if(we <= 20160)
		  if(we <= 19580)
		   if(we <= 19540) return 3486.65;
		   if(we > 19540) return 3513.63333333333;
		  if(we > 19580)
		   if(we <= 19650) return 3210.4;
		   if(we > 19650) return 3207.63333333333;
		 if(we > 20160)
		  if(t <= 0.45) return 3217.38333333333;
		  if(t > 0.45) return 4005.16666666667;
		if(p.equals("PP22296"))
		 if(t <= 2.8)
		  if(we <= 25090)
		   if(we <= 23580)
		    if(w <= 1206) return 3567.43333333333;
		    if(w > 1206) return 3739.63333333333;
		   if(we > 23580)
		    if(we <= 24320) return 3873;
		    if(we > 24320)
		     if(we <= 24900) return 3684.33333333333;
		     if(we > 24900) return 3739.63333333333;
		  if(we > 25090) return 3634.86666666667;
		 if(t > 2.8)
		  if(we <= 23080)
		   if(w <= 1100) return 3368.75;
		   if(w > 1100)
		    if(we <= 21051) return 3355.41666666667;
		    if(we > 21051) return 3639.3;
		  if(we > 23080)
		   if(w <= 1127) return 3652;
		   if(w > 1127) return 3657.58333333333;
		if(p.equals("PP22304"))
		 if(t <= 4.85)
		  if(t <= 4.2)
		   if(w <= 501) return 3196.85;
		   if(w > 501) return 2816.71666666667;
		  if(t > 4.2) return 2933.31666666667;
		 if(t > 4.85)
		  if(t <= 5.6) return 2671.2;
		  if(t > 5.6) return 3022.88333333333;
		if(p.equals("PP22305")) return 3088.06666666667;
		if(p.equals("PP22310")) return 2299.4;
		if(p.equals("PP22315"))
		 if(we <= 9011) return 3237.53333333333;
		 if(we > 9011) return 3089.81666666667;
		if(p.equals("PP22321"))
		 if(we <= 12942) return 3092.51666666667;
		 if(we > 12942)
		  if(we <= 13646)
		   if(we <= 13170) return 3008.95;
		   if(we > 13170) return 2719.23333333333;
		  if(we > 13646) return 3008.95;
		if(p.equals("PP22322"))
		 if(we <= 12942) return 3092.51666666667;
		 if(we > 12942)
		  if(we <= 13646)
		   if(we <= 13047) return 3008.95;
		   if(we > 13047) return 2719.23333333333;
		  if(we > 13646) return 3008.95;
		if(p.equals("PP22323"))
		 if(we <= 17060) return 3208.56666666667;
		 if(we > 17060) return 3104.31666666667;
		if(p.equals("PP22324")) return 2889.33333333333;
		if(p.equals("PP22325")) return 3115;
		if(p.equals("PP22326")) return 3115;
		if(p.equals("PP22333")) return 3479.46666666667;
		if(p.equals("PP22334"))
		 if(w <= 572)
		  if(w <= 528) return 2788.31666666667;
		  if(w > 528) return 2735.15;
		 if(w > 572)
		  if(w <= 598)
		   if(t <= 2.23) return 2430.8;
		   if(t > 2.23) return 2931.85;
		  if(w > 598) return 2907.98333333333;
		if(p.equals("PP22336"))
		 if(t <= 4.25) return 2618;
		 if(t > 4.25)
		  if(we <= 3506)
		   if(w <= 303) return 2327.18333333333;
		   if(w > 303)
		    if(we <= 3431)
		     if(we <= 3357) return 2553.75;
		     if(we > 3357) return 2378;
		    if(we > 3431) return 2327.18333333333;
		  if(we > 3506)
		   if(we <= 3564) return 2378;
		   if(we > 3564) return 2705.58333333333;
		if(p.equals("PP22343")) return 2849;
		if(p.equals("PP22345")) return 3499;
		if(p.equals("PP22348")) return 3730.73333333333;
		if(p.equals("PP22352"))
		 if(t <= 4.25)
		  if(w <= 414) return 3200.23333333333;
		  if(w > 414)
		   if(we <= 4700) return 2958.05;
		   if(we > 4700)
		    if(we <= 4960) return 2757.3;
		    if(we > 4960) return 3871.65;
		 if(t > 4.25)
		  if(we <= 4934)
		   if(we <= 4709) return 2553.75;
		   if(we > 4709) return 3070.3;
		  if(we > 4934)
		   if(we <= 5249)
		    if(we <= 4999)
		     if(we <= 4962) return 2910.41666666667;
		     if(we > 4962) return 2396.18333333333;
		    if(we > 4999) return 2705.58333333333;
		   if(we > 5249) return 2649.93333333333;
		if(p.equals("PP22354")) return 3215.31666666667;
		if(p.equals("PP22355"))
		 if(we <= 15650) return 3177.95;
		 if(we > 15650) return 2994;
		if(p.equals("PP22361")) return 2853;
		if(p.equals("PP22366")) return 2868.5;
		if(p.equals("PP22367"))
		 if(we <= 5426) return 2758.95;
		 if(we > 5426) return 3777.85;
		if(p.equals("PP22369")) return 3777.85;
		if(p.equals("PP22374")) return 3953.93333333333;
		if(p.equals("PP22376"))
		 if(we <= 25040) return 3688.85;
		 if(we > 25040) return 3486.93333333333;
		if(p.equals("PP22378")) return 3376.3;
		if(p.equals("PP22380")) return 2459.76666666667;
		if(p.equals("PP22386"))
		 if(t <= 3.84) return 2958.56666666667;
		 if(t > 3.84)
		  if(w <= 650) return 2471.7;
		  if(w > 650) return 2614;
		if(p.equals("PP22387"))
		 if(w <= 650) return 2774.31666666667;
		 if(w > 650) return 3329.85;
		if(p.equals("PP22389")) return 2747.68333333333;
		if(p.equals("PP22390"))
		 if(t <= 1.5) return 2747.68333333333;
		 if(t > 1.5) return 2912.06666666667;
		if(p.equals("PP22391")) return 3005;
		if(p.equals("PP22395"))
		 if(t <= 1.42)
		  if(we <= 7894)
		   if(we <= 7397) return 2504.5;
		   if(we > 7397) return 3025.65;
		  if(we > 7894)
		   if(we <= 8150) return 2850;
		   if(we > 8150) return 3121.26666666667;
		 if(t > 1.42)
		  if(we <= 7894) return 2508.68333333333;
		  if(we > 7894) return 2943.71666666667;
		if(p.equals("PP22399")) return 3300.53333333333;
		if(p.equals("PP22404")) return 3059.11666666667;
		if(p.equals("PP22406")) return 3360;
		if(p.equals("PP22408"))
		 if(t <= 1.42) return 8238.38333333333;
		 if(t > 1.42)
		  if(t <= 1.97) return 16866.1833333333;
		  if(t > 1.97) return 3360;
		if(p.equals("PP22409"))
		 if(t <= 1.97) return 8584.63333333333;
		 if(t > 1.97) return 2614;
		if(p.equals("PP22410")) return 3360;
		if(p.equals("PP22411")) return 3221.63333333333;
		if(p.equals("PP22412")) return 2618.95;
		if(p.equals("PP22413")) return 2858.93333333333;
		if(p.equals("PP22415"))
		 if(w <= 408)
		  if(w <= 383) return 2471.7;
		  if(w > 383) return 2806.68333333333;
		 if(w > 408)
		  if(we <= 14060) return 2614;
		  if(we > 14060) return 2833;
		if(p.equals("PP22416"))
		 if(t <= 3.1)
		  if(t <= 2.84) return 2507.31666666667;
		  if(t > 2.84)
		   if(we <= 2542) return 2725.45;
		   if(we > 2542) return 3010.58333333333;
		 if(t > 3.1)
		  if(t <= 3.6)
		   if(w <= 229) return 2561.08333333333;
		   if(w > 229)
		    if(w <= 279) return 2841.5;
		    if(w > 279) return 2561.08333333333;
		  if(t > 3.6)
		   if(we <= 3585) return 2610.23333333333;
		   if(we > 3585)
		    if(we <= 3634) return 2840.1;
		    if(we > 3634) return 2618;
		if(p.equals("PP22417"))
		 if(t <= 3.74)
		  if(t <= 3.24) return 3276.25;
		  if(t > 3.24) return 2385.55;
		 if(t > 3.74)
		  if(we <= 5783)
		   if(w <= 427)
		    if(we <= 3770) return 3196.51666666667;
		    if(we > 3770) return 2739;
		   if(w > 427)
		    if(we <= 5609) return 2479.25;
		    if(we > 5609) return 2327.8;
		  if(we > 5783)
		   if(w <= 527)
		    if(we <= 6010)
		     if(we <= 5902) return 2610.23333333333;
		     if(we > 5902) return 2840.1;
		    if(we > 6010)
		     if(we <= 6521) return 2880;
		     if(we > 6521) return 2354.13333333333;
		   if(w > 527)
		    if(w <= 528) return 7640.21666666667;
		    if(w > 528) return 2739;
		if(p.equals("PP22419")) return 2400.83333333333;
		if(p.equals("PP22420")) return 2824.96666666667;
		if(p.equals("PP22421")) return 2824.96666666667;
		if(p.equals("PP22422"))
		 if(w <= 650) return 2806.68333333333;
		 if(w > 650) return 2833;
		if(p.equals("PP22423"))
		 if(t <= 1.65) return 2795;
		 if(t > 1.65) return 2816.71666666667;
		if(p.equals("PP22425"))
		 if(we <= 4319) return 3100.41666666667;
		 if(we > 4319)
		  if(we <= 4856) return 3774.33333333333;
		  if(we > 4856) return 3997.18333333333;
		if(p.equals("PP22426"))
		 if(we <= 2906) return 3275.5;
		 if(we > 2906) return 3997.18333333333;
		if(p.equals("PP22427"))
		 if(we <= 4746) return 3275.5;
		 if(we > 4746) return 3997.18333333333;
		if(p.equals("PP22431")) return 3398.66666666667;
		if(p.equals("PP22435"))
		 if(t <= 1.62) return 8238.38333333333;
		 if(t > 1.62) return 4497;
		if(p.equals("PP22438"))
		 if(t <= 2.28) return 2721.2;
		 if(t > 2.28) return 4497;
		if(p.equals("PP22442")) return 3326.65;
		if(p.equals("PP22456")) return 2950;
		if(p.equals("PP22457")) return 2950;
		if(p.equals("PP22459"))
		 if(t <= 1.57) return 2648.98333333333;
		 if(t > 1.57) return 2667.45;
		if(p.equals("PP22460")) return 2935.66666666667;
		if(p.equals("PP22461")) return 2672;
		if(p.equals("PP22470")) return 3808.86666666667;
		if(p.equals("PP22473"))
		 if(w <= 363) return 2989;
		 if(w > 363) return 2807.46666666667;
		if(p.equals("PP22474"))
		 if(w <= 299) return 3605.1;
		 if(w > 299) return 2807.46666666667;
		if(p.equals("PP22478")) return 2672;
		if(p.equals("PP22479"))
		 if(t <= 4.02) return 2504.5;
		 if(t > 4.02)
		  if(w <= 650) return 2741.78333333333;
		  if(w > 650) return 3329.85;
		if(p.equals("PP22480")) return 3461;
		if(p.equals("PP22481"))
		 if(t <= 2.55) return 2928.78333333333;
		 if(t > 2.55) return 2465;
		if(p.equals("PP22482"))
		 if(t <= 1.8) return 2928.78333333333;
		 if(t > 1.8) return 2465;
		if(p.equals("PP22484")) return 2807.46666666667;
		if(p.equals("PP22485"))
		 if(t <= 1.62) return 2939.45;
		 if(t > 1.62) return 2807.46666666667;
		if(p.equals("PP22490")) return 3531.61666666667;
		if(p.equals("PP22493")) return 2687.38333333333;
		if(p.equals("PP22494")) return 3273.03333333333;
		if(p.equals("PP22496")) return 2872.23333333333;
		if(p.equals("PP22498"))
		 if(t <= 3.74)
		  if(t <= 3.34) return 2561.08333333333;
		  if(t > 3.34) return 2805.76666666667;
		 if(t > 3.74)
		  if(w <= 319)
		   if(w <= 307) return 2866.68333333333;
		   if(w > 307)
		    if(we <= 3404) return 2521.18333333333;
		    if(we > 3404) return 2757.3;
		  if(w > 319)
		   if(we <= 4391) return 2528;
		   if(we > 4391)
		    if(t <= 4.25)
		     if(w <= 358) return 2866.68333333333;
		     if(w > 358) return 4226.18333333333;
		    if(t > 4.25) return 2706.6;
		if(p.equals("PP22499")) return 3100.21666666667;
		if(p.equals("PP22500")) return 3100.21666666667;
		if(p.equals("PP22502")) return 3092.31666666667;
		if(p.equals("PP22503"))
		 if(w <= 246) return 2757.01666666667;
		 if(w > 246) return 2695.81666666667;
		if(p.equals("PP22504")) return 2695.81666666667;
		if(p.equals("PP22505"))
		 if(t <= 3.6)
		  if(t <= 2.94)
		   if(w <= 402)
		    if(w <= 350) return 3515.45;
		    if(w > 350)
		     if(w <= 368) return 2507.31666666667;
		     if(w > 368)
		      if(w <= 385) return 2571.76666666667;
		      if(w > 385) return 2507.31666666667;
		   if(w > 402)
		    if(we <= 4333) return 2364.31666666667;
		    if(we > 4333) return 2825.5;
		  if(t > 2.94)
		   if(we <= 4251) return 2825.5;
		   if(we > 4251) return 2841.5;
		 if(t > 3.6)
		  if(we <= 3497)
		   if(we <= 3391)
		    if(we <= 3322) return 2403.51666666667;
		    if(we > 3322) return 2479.25;
		   if(we > 3391) return 2327.8;
		  if(we > 3497)
		   if(w <= 368)
		    if(we <= 3812)
		     if(we <= 3582) return 2550.53333333333;
		     if(we > 3582) return 2350.18333333333;
		    if(we > 3812) return 2354.13333333333;
		   if(w > 368)
		    if(we <= 4606) return 3583.55;
		    if(we > 4606) return 2354.13333333333;
		if(p.equals("PP22508"))
		 if(t <= 3.34) return 2910.71666666667;
		 if(t > 3.34)
		  if(t <= 3.74)
		   if(we <= 3979) return 2752.66666666667;
		   if(we > 3979) return 2984.91666666667;
		  if(t > 3.74) return 2757.3;
		if(p.equals("PP22512")) return 2758.95;
		if(p.equals("PP22513")) return 4348.51666666667;
		if(p.equals("PP22514")) return 2757.3;
		if(p.equals("PP22515")) return 2984.91666666667;
		if(p.equals("PP22516")) return 2888.15;
		if(p.equals("PP22519")) return 3070.3;
		if(p.equals("PP22521")) return 3070.3;
		if(p.equals("PP22522")) return 2635.41666666667;
		if(p.equals("PP22524")) return 4575.1;
		if(p.equals("PP22526")) return 4575.1;
		if(p.equals("PP22528")) return 3219.61666666667;
		if(p.equals("PP22530"))
		 if(t <= 4.25) return 2866.68333333333;
		 if(t > 4.25) return 2865.48333333333;
		if(p.equals("PP22531"))
		 if(w <= 457)
		  if(w <= 455) return 2771.48333333333;
		  if(w > 455) return 2561.08333333333;
		 if(w > 457)
		  if(we <= 4749)
		   if(we <= 4427) return 2635.33333333333;
		   if(we > 4427)
		    if(w <= 475) return 3007.65;
		    if(w > 475) return 3200.23333333333;
		  if(we > 4749) return 2598.18333333333;
		if(p.equals("PP22534"))
		 if(we <= 2505)
		  if(t <= 4.25) return 3200.23333333333;
		  if(t > 4.25) return 2771.48333333333;
		 if(we > 2505)
		  if(w <= 304)
		   if(we <= 2633) return 3537.01666666667;
		   if(we > 2633)
		    if(t <= 4.7)
		     if(t <= 4.25) return 3427.76666666667;
		     if(t > 4.25) return 3163;
		    if(t > 4.7) return 2635.33333333333;
		  if(w > 304) return 3537.01666666667;
		if(p.equals("PP22535"))
		 if(t <= 4.35)
		  if(we <= 2945)
		   if(w <= 236)
		    if(w <= 224) return 2825.5;
		    if(w > 224)
		     if(we <= 2608) return 2528;
		     if(we > 2608) return 2350.18333333333;
		   if(w > 236)
		    if(we <= 2892) return 2465.78333333333;
		    if(we > 2892) return 2540.96666666667;
		  if(we > 2945)
		   if(w <= 281)
		    if(w <= 239) return 2618;
		    if(w > 239) return 2846;
		   if(w > 281) return 2618;
		 if(t > 4.35)
		  if(t <= 4.85) return 3174.63333333333;
		  if(t > 4.85)
		   if(w <= 341)
		    if(w <= 307)
		     if(w <= 262) return 3504.33333333333;
		     if(w > 262) return 2666.63333333333;
		    if(w > 307) return 3504.33333333333;
		   if(w > 341) return 2666.63333333333;
		if(p.equals("PP22537"))
		 if(we <= 18100)
		  if(t <= 0.85) return 3188.76666666667;
		  if(t > 0.85) return 3501.78333333333;
		 if(we > 18100) return 3544.9;
		if(p.equals("PP22539"))
		 if(we <= 11694) return 3354.85;
		 if(we > 11694) return 2889;
		if(p.equals("PP22540")) return 2991.43333333333;
		if(p.equals("PP22541")) return 2991.43333333333;
		if(p.equals("PP22542")) return 2991.43333333333;
		if(p.equals("PP22553")) return 3445.41666666667;
		if(p.equals("PP22556")) return 2676.08333333333;
		if(p.equals("PP22558")) return 2706.6;
		if(p.equals("PP22561")) return 3076.88333333333;
		if(p.equals("PP22566"))
		 if(w <= 650) return 2603.61666666667;
		 if(w > 650) return 3329.85;
		if(p.equals("PP22567"))
		 if(t <= 2.63)
		  if(we <= 25000) return 3523.8;
		  if(we > 25000) return 3377.33333333333;
		 if(t > 2.63)
		  if(we <= 25000) return 3706.13333333333;
		  if(we > 25000) return 3422.36666666667;
		if(p.equals("PP22568")) return 3092.31666666667;
		if(p.equals("PP22569"))
		 if(w <= 614) return 2666.63333333333;
		 if(w > 614) return 2515.88333333333;
		if(p.equals("PP22570")) return 2364.31666666667;
		if(p.equals("PP22571")) return 3537.01666666667;
		if(p.equals("PP22573"))
		 if(we <= 3732) return 2465.78333333333;
		 if(we > 3732) return 2846;
		if(p.equals("PP22574")) return 2540.96666666667;
		if(p.equals("PP22576")) return 3481.2;
		if(p.equals("PP22581")) return 2825;
		if(p.equals("PP22586")) return 3100.41666666667;
		if(p.equals("PP22587")) return 3100.41666666667;
		if(p.equals("PP22595")) return 3100.41666666667;
		if(p.equals("PP22604")) return 2141.81666666667;
		if(p.equals("SS11131")) return 2176.3;
		if(p.equals("WS21130")) return 2870.21666666667;
		if(p.equals("WS21271")) return 2523.58333333333;
		return 2523.58333333333;
}
}
