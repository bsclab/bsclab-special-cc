package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_ANA implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-1.75571477,6.99701262,-0.46498638,-0.45114353,-0.02616116,8.48018551
            			,0.91531104,1.43007791,1.1092006,-1.26978254,5.2989006,0.38655108
            			,0.80046064,-0.69152373,2.15030813,3.16238308,-0.3077718,0.24072015
            			,-1.20261085,-1.40400469}, 
        			{0.77312678,0.1927941,0.32941875,-1.14170921,-0.81058073,1.62371504
            			,0.62331933,0.58471501,0.81201839,-0.27631217,0.925798,-1.26072288
            			,-1.14510608,-2.48986483,-0.10144523,-0.40103436,-0.87423319,-1.29042172
            			,1.03638816,-1.06102479},
        			{-0.27550006,-0.72292805,-0.06704924,-1.71713626,-0.9048565,0.02670403
            				,-0.03811284,-1.36251342,-0.83816212,-0.57025307,0.55685943,-0.64915711
            				,-0.36107993,-0.43923593,0.90854323,-0.79313916,-0.02924326,-1.74885523
            				,-0.96982187,-0.56955534}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-0.54516429,1.60086429,-0.40272379,-0.04001475,-0.34078363,2.51412654
            			,-1.6210897,-0.53521419,-0.58839273,0.06823552,0.6072548,-0.7007134
            			,-0.84545058,-0.10339985,0.78110445,0.62541622,-0.54197645,0.5324868
            			,-0.14095682,0.17555219};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {0.02987036,
            			1.86935782,
            			-0.89443117,
            			0.81022513,
            			0.50528252,
            			1.25595915,
            			-0.7024672,
            			0.08105538,
            			0.54131025,
            			0.14904439,
            			1.40443969,
            			0.87694895,
            			0.28398207,
            			0.43202817,
            			0.53570431,
            			0.22699712,
            			0.64791238,
            			0.20604144,
            			-0.17420208,
            			-0.46394193};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.12863135};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 5.4;
	}

	@Override
	public double getMaxWidth() {
		
		return 610.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 8172.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.21;
	}

	@Override
	public double getMinWidth() {
		
		return 150.0;
	}

	@Override
	public double getMinWeight() {
		
		return 937.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
	
	public static int maxBase() {
		return 10;
	}
	
	public static float getMaxWeight(int index) { // in ton
		if(index >= 0 && index < 10) // 6 bases, 35 ton, ebner
			return 75000;
		else
			return 25000;
		
	}
}