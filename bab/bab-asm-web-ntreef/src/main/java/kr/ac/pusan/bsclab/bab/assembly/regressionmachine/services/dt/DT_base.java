package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;

public interface DT_base {
	public double getDuration(double thick, double width, double weight, String plancode);
}
