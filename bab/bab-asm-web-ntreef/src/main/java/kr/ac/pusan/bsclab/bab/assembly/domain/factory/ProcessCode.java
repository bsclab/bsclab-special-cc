package kr.ac.pusan.bsclab.bab.assembly.domain.factory;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="sf_process_code")
public class ProcessCode {
	
	@Id
	@Column(name="ZONE_NAME")
	String zoneName;
	
	@Column(name="MINOR_ID")
	String minorId;
	
	@Column(name="MINOR_NAM")
	String minorNam;
	
	@Column(name="REF8_CLS")
	String ref8Cls;

}
