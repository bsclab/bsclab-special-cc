package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue;

import org.springframework.stereotype.Component;


@Component
public class JobQueueFactory {
	
	
//	public JobQueue createJobQueue(Job<? extends JobConfiguration>... jobs) {
//		
//		JobQueue jobQueue = new JobQueue(jobListener);
//		
//		for (Job<? extends JobConfiguration> job : jobs)
//			jobQueue.add(job);
//		
//		
//		return jobQueue;
//	}
	
	public JobQueue createJobQueue(String jobQueueId) {
		
		return new JobQueue(jobQueueId);
	}

}
