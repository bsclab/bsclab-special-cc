package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.JobConfiguration;

public class JobQueue implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String jobQueueId;
	private Queue<Job<? extends JobConfiguration>> jobQueue;
	private JobQueueStatus jobQueueStatus=JobQueueStatus.WAITING;
	
	JobQueue(String jobQueueId) {
		jobQueue = new LinkedList<Job<? extends JobConfiguration>>();
		this.jobQueueId = jobQueueId;
	}

	public String getJobQueueId() {
		return jobQueueId;
	}



	public JobQueue setJobQueueId(String jobQueueId) {
		this.jobQueueId = jobQueueId;
		return this;
	}



	public Queue<Job<? extends JobConfiguration>> getJobQueue() {
		return jobQueue;
	}



	public JobQueueStatus getJobQueueStatus() {
		return jobQueueStatus;
	}



	public void setJobQueue(Queue<Job<? extends JobConfiguration>> jobQueue) {
		this.jobQueue = jobQueue;
	}



	public void setJobQueueStatus(JobQueueStatus jobQueueStatus) {
		this.jobQueueStatus = jobQueueStatus;
	}



	public int size() {
		return jobQueue.size();
	}
	public boolean isEmpty() {
		return jobQueue.isEmpty();
	}
	public boolean contains(Object o) {
		return jobQueue.contains(o);
	}
	public Iterator<Job<? extends JobConfiguration>> iterator() {
		return jobQueue.iterator();
	}
	public Object[] toArray() {
		return jobQueue.toArray();
	}
	public <T> T[] toArray(T[] a) {
		return jobQueue.toArray(a);
	}
	public boolean remove(Object o) {
		return jobQueue.remove(o);
	}
	public boolean containsAll(Collection<?> c) {
		return jobQueue.containsAll(c);
	}
	public boolean addAll(Collection<? extends Job<? extends JobConfiguration>> c) {
		return jobQueue.addAll(c);
	}
	public boolean removeAll(Collection<?> c) {
		return jobQueue.removeAll(c);
	}
	public boolean retainAll(Collection<?> c) {
		return jobQueue.retainAll(c);
	}
	public void clear() {
		jobQueue.clear();
	}

	public boolean add(Job<? extends JobConfiguration> e) {
		return jobQueue.add(e);
	}
	public boolean offer(Job<? extends JobConfiguration> e) {
		return jobQueue.offer(e);
	}
	public Job<? extends JobConfiguration> remove() {
		return jobQueue.remove();
	}
	public Job<? extends JobConfiguration> poll() {
		return jobQueue.poll();
	}
	public Job<? extends JobConfiguration> element() {
		return jobQueue.element();
	}
	
	public Job<? extends JobConfiguration> peek() {
		return jobQueue.peek();
	}

}
