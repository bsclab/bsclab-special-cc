package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.to;

import java.util.Arrays;

public enum ToHdfsCompression {
	NONE,DEFAULT,DEFLATE,GZIP,BZIP2,LZO,LZ4,SNAPPY,CUSTOM;
	
	public static String getAll() {
		return Arrays.toString(ToHdfsCompression.values()).replace("[", "").replace("]", "").replace(" ", "");
	}
}
