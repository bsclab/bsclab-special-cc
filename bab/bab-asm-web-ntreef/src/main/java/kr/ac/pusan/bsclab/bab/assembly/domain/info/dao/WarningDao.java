package kr.ac.pusan.bsclab.bab.assembly.domain.info.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import kr.ac.pusan.bsclab.bab.assembly.domain.info.Warning;

@Repository
public class WarningDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Map<String, Object>> showWarning() {
		return jdbcTemplate.queryForList("SHOW WARNINGS");
	}
}


class WarningsRowMapper implements RowMapper<Warning>{
	@Override
	public Warning mapRow(ResultSet rs, int rowNum) throws SQLException {
		Warning warning = new Warning();
		warning.setCode(rs.getString("code"));
		warning.setLevel(rs.getString("level"));
		warning.setMessage(rs.getString("message"));
		return warning;
	} 
}
