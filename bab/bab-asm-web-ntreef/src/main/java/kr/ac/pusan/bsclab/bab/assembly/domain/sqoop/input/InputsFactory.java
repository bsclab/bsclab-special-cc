package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input;

public interface InputsFactory {
	
	public Input[] createInputs(Object ...args);

}
