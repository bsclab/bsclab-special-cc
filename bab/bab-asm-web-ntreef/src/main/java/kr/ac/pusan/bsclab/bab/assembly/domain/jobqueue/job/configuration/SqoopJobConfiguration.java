package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration;

import java.sql.Timestamp;

public class SqoopJobConfiguration implements JobConfiguration {
		
	private Timestamp sdt;
	private Timestamp edt;
	private String serverIp;
	private String resourceTable;
	private String outputDir;
	private String jdbcDriver;
	private String connectionString;
	private String dbUser;
	private String dbPw;
	
	
	
	
	public SqoopJobConfiguration(Timestamp sdt, Timestamp edt, String serverIp, String resourceTable, String outputDir,
			String jdbcDriver, String connectionString, String dbUser, String dbPw) {
		this.sdt = sdt;
		this.edt = edt;
		this.serverIp = serverIp;
		this.resourceTable = resourceTable;
		this.outputDir = outputDir;
		this.jdbcDriver = jdbcDriver;
		this.connectionString = connectionString;
		this.dbUser = dbUser;
		this.dbPw = dbPw;
	}
	public Timestamp getSdt() {
		return sdt;
	}
	public Timestamp getEdt() {
		return edt;
	}
	public String getServerIp() {
		return serverIp;
	}
	public String getResourceTable() {
		return resourceTable;
	}
	public String getOutputDir() {
		return outputDir;
	}
	public String getJdbcDriver() {
		return jdbcDriver;
	}
	public String getConnectionString() {
		return connectionString;
	}
	public String getDbUser() {
		return dbUser;
	}
	public String getDbPw() {
		return dbPw;
	}
	public void setSdt(Timestamp sdt) {
		this.sdt = sdt;
	}
	public void setEdt(Timestamp edt) {
		this.edt = edt;
	}
	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}
	public void setResourceTable(String resourceTable) {
		this.resourceTable = resourceTable;
	}
	public void setOutputDir(String outputDir) {
		this.outputDir = outputDir;
	}
	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}
	public void setConnectionString(String connectionString) {
		this.connectionString = connectionString;
	}
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}
	public void setDbPw(String dbPw) {
		this.dbPw = dbPw;
	}
	
	

	

	

}
