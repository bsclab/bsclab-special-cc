package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop;

public enum SqoopConnector {
	
	// GENERIC_JDBC_CONNECTOR's number is 1 in sqoop
	// so i use 'NONE'
	NONE
	, GENERIC_JDBC_CONNECTOR
	, KITE_CONNECTOR
	, HDFS_CONNECTOR
	, KAFKA_CONNECTOR
}