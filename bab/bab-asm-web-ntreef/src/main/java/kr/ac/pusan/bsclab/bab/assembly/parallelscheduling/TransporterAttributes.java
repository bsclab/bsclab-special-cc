package kr.ac.pusan.bsclab.bab.assembly.parallelscheduling;

public class TransporterAttributes {
	public String transporter;
	public double speed_unloaded;
	public double speed_loaded;
	public double max_weight;
	public String initial_area;
}
