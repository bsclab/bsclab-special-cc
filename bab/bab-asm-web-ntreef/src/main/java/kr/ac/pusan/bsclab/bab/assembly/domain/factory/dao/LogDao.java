package kr.ac.pusan.bsclab.bab.assembly.domain.factory.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;

@Repository
public class LogDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	AppProperties ap;
	
	/**
	 * copy the table. it just copy structure
	 * @param resourceTableName
	 * @param newTableName
	 */
	public int createTemporaryTable(String resourceTableName, String newTableName) {

//		this is H2 sql query
//		String sql = "CREATE TABLE " + tableName + " AS SELECT * FROM woo_sf_log WHERE 1=0";
		
		String sql = "CREATE TABLE " + newTableName + " LIKE " + resourceTableName;
//		String sql = "CREATE TABLE " + newTableName + "("
//				+ "`COIL_NO` varchar(24) NOT NULL"
//				+ ", `HISCOI_NO` varchar(24) NOT NULL"
//				+ ", `PRC_CD` varchar(8) NOT NULL"
//				+ ", `PRC_CD1` varchar(8) DEFAULT NULL"
//				+ ", `PRC_REPEAT` int(11) DEFAULT NULL"
//				+ ", `CSEQ` int(11) NOT NULL"
//				+ ", `THK` varchar(8) NOT NULL"
//				+ ", `WDT` varchar(8) NOT NULL"
//				+ ", `WGT` varchar(8) NOT NULL"
//				+ ", `SDT` varchar(19) NOT NULL"
//				+ ", `EDT` varchar(19) NOT NULL"
//				+ ", `CHG_YN` varchar(8) DEFAULT NULL"
//				+ ", `FIRCRT_YMD` varchar(8) NOT NULL"
//				+ ", `PLNPRC_CD` varchar(8) NOT NULL"
//				+ ", `ORD_NO` varchar(10) NOT NULL"
//				+ ", `ORD_SEQ` varchar(8) NOT NULL"
//				+ ", `DRTCOI_NO` varchar(16) NOT NULL"
//				+ ", `SPC_NO` varchar(16) NOT NULL"
//				+ ", `PRC_CD_SHORT` varchar(8) DEFAULT NULL"
//				+ ", `EMG_NAM` varchar(8) NOT NULL)";
		return jdbcTemplate.update(sql);
	}
	
	public int uploadCsvToTable(String tableName, String csvFilePath) {
		
//		String sql = "INSERT INTO ?(?)"
//				+ "SELECT * FROM CSVREAD(?)";
//		jdbcTemplate.update(sql, new Object[]{tableName, this.columnStr(columnArr), csvFilePath});
		
		String sql = "LOAD DATA LOCAL INFILE '"
						+ csvFilePath.replace("\\", "/")
						+ "' INTO TABLE "
						+ tableName
						+ " FIELDS TERMINATED BY ',' IGNORE 1 LINES";
		return jdbcTemplate.update(sql);
	}
	
	public List<Map<String, Object>> getOverlaps(String tableAName, String tableBName) {
		
//		select * from sf_log a
//		where exists(
//			select w.coil_no, w.prc_cd, w.cseq, w.ord_no
//		    from woo_sf_log w
//		    where a.coil_no = w.coil_no
//		    and a.prc_cd = w.PRC_CD
//		    and a.CSEQ = w.CSEQ
//		    and a.ORD_NO = w.ORD_NO);
		String sql = "SELECT * FROM " + tableAName + " a"
				+ " WHERE EXISTS("
					+ "SELECT b.coil_no, b.hiscoi_no, b.prc_cd"
					+ "			, b.cseq, b.thk, b.wdt"
					+ "			, b.wgt, b.sdt, b.edt"
					+ "			, b.fircrt_ymd, b.plnprc_cd, b.ord_no"
					+ "			, b.ord_seq, b.drtcoi_no, b.spc_no"
					+ "			, b.emg_nam"
					+ " FROM " + tableBName + " b"
					+ " WHERE a.coil_no=b.coil_no"
					+ " AND a.hiscoi_no=b.hiscoi_no"
					+ " AND a.prc_cd=b.prc_cd"
					+ " AND a.cseq=b.cseq"
					+ " AND a.thk=b.thk"
					+ " AND a.wdt=b.wdt"
					+ " AND a.wgt=b.wgt"
					+ " AND a.sdt=b.sdt"
					+ " AND a.edt=b.edt"
					+ " AND a.fircrt_ymd=b.fircrt_ymd"
					+ " AND a.plnprc_cd=b.plnprc_cd"
					+ " AND a.ord_no=b.ord_no"
					+ " AND a.ord_seq=b.ord_seq"
					+ " AND a.drtcoi_no=b.drtcoi_no"
					+ " AND a.spc_no=b.spc_no"
					+ " AND a.emg_nam=b.emg_nam)";
		return jdbcTemplate.queryForList(sql);
	}
	
	public int save(String tableAName, String tableBName) {
		String sql = "INSERT INTO " + tableAName
				+ " SELECT * FROM " + tableBName;
		return jdbcTemplate.update(sql);
	}
	
	public int dropTable(String tableName) {
		String sql = "DROP TABLE " + tableName;
		return jdbcTemplate.update(sql);
	}
	
	public String arrToColumnStr(String[] arr) {
		String columnStr = "";
		for (int i=0; i<arr.length; i++) {
			if (arr[i]!=null) {
				if (i==0) columnStr += arr[i];
				else columnStr += "," + arr[i];
			}
		}
		return columnStr;
	}

	public String getChecksum(Timestamp startDt, Timestamp endDt) {
		String sql = "SELECT sum(crc32(concat(COIL_NO, PRC_CD, CSEQ, ORD_NO)))"
				+ " as hashcode from woo_sf_log"
				+ " WHERE SDT > ?"
				+ " AND EDT < ? ORDER by SDT";

		
		String result =  jdbcTemplate.queryForObject(sql, new Object[] {
				startDt, endDt
		}, String.class);
		return result;
	}
	
//	SELECT sdt, edt FROM test.woo_sf_log
//	where sdt > unix_timestamp('2016-01-04 00:00:00')
//	and edt < unix_timestamp('2016-03-01 00:00:00') order by sdt;
}
