package kr.ac.pusan.bsclab.bab.assembly.abs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.HdfsProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.SparkProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.SqoopProperties;
import kr.ac.pusan.bsclab.bab.assembly.domain.factory.dao.LogDao;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.dao.JobDao;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueueFactory;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.JobFactory;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.JobConfigurationFactory;
import kr.ac.pusan.bsclab.bab.assembly.job.JobManager;
import kr.ac.pusan.bsclab.bab.assembly.utils.DateConvertor;
import kr.ac.pusan.bsclab.bab.assembly.utils.hdfs.WebHdfs;
import kr.ac.pusan.bsclab.bab.assembly.ws.service.BabService;

@Controller
public abstract class AbstractController {

	@Autowired
	protected WebHdfs webHdfs;
	
	@Autowired
	protected HdfsProperties hp;
	
	@Autowired
	protected AppProperties ap;
	
	@Autowired
	protected JobFactory jf;
	
	@Autowired
	protected JobConfigurationFactory jcf;
	
	@Autowired
	protected SqoopProperties sqp;
	
	@Autowired
	protected SparkProperties spp;
	
	@Autowired
	protected JobManager jobManager;
	
	@Autowired
	protected JobQueueFactory jobQueueFactory;
	
	@Autowired
	protected DateConvertor dc;
	
	@Autowired
	protected LogDao logDao;
	
	@Autowired
	protected JobDao jobDao;
	
	@Autowired
	protected BabService babService;
	
//	@Autowired
//	protected InfoService infoService;
//	
//	@Autowired
//	protected UploadService uploadService;
//	
//	@Autowired
//	protected RepositoryService repositoryService;
//	
//	@Autowired
//	protected JobDao jobDao;

	
}
