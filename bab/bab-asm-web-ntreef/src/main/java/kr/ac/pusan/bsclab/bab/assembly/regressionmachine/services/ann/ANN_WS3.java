package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_WS3 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-4.06639874e-01, -8.42664778e-01, 5.47871590e-01, 6.52390957e-01
            				, -8.72722149e-01, -4.12527800e-01, 6.59454763e-01, -2.19554567e+00
            				, 2.77604669e-01, -9.07721698e-01, -1.74165106e+00, -9.53518152e-01
            				, -2.66216904e-01, -1.04790342e+00, 2.50870377e-01, -1.91012943e+00
            				, -7.00081110e-01, -5.34884036e-01, -1.22348703e-01, 1.20118201e+00}, 
            			{-6.77695692e-01, -6.79118276e-01, -1.11419034e+00, 3.53171498e-01
        					, 1.11272788e+00, 1.15254843e+00, 8.65059495e-01, -1.04201026e-02
        					, 4.52914774e-01, -2.28054672e-01, -4.11571592e-01, -6.42261863e-01
        					, 5.55464089e-01, 2.16525221e+00, -2.97844261e-02, -7.98723578e-01
        					, -1.77653646e+00, 8.33605409e-01, -3.95754844e-01, -3.17987740e-01},
            			{7.06737697e-01, 3.41034532e-01, -9.96806800e-01, 3.67586285e-01
    						, 1.89354554e-01, -3.05689454e+00, -6.49749398e-01, -1.29611993e+00
    						, -7.12171853e-01, 9.41191018e-01, -1.21268237e+00, 2.07577214e-01
    						, 3.12606782e-01, -1.27043045e+00, -9.62660730e-01, 1.43959522e-01
    						, -3.43854380e+00, -2.59366236e-03, 1.25187838e+00, -8.20913851e-01}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {1.6520263, 2.0164628, -0.68212348, 0.74594837, -0.95820999, -0.34344628
            			, -0.84001058, -0.22412476, 1.80835736, -2.14189196, -0.81973112, -0.30645627
            			, -2.4485929, 1.62170374, 1.72454846, 1.59502292, -0.27157319, 0.91510385
            			, -0.29707783, -0.66129911};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-2.22748089,
            			0.38163406,
            			0.44987518,
            			1.796121,
            			-1.1223433,
            			-0.12663251,
            			0.18703172,
            			0.95409822,
            			0.4868241,
            			-0.43683359,
            			-1.73954797,
            			0.170177,
            			-0.84551573,
            			-1.20746291,
            			1.79366827,
            			1.65907359,
            			-0.13218519,
            			0.45877042,
            			0.52308756,
            			-0.67124182};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.78805649};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 9.9;
	}

	@Override
	public double getMaxWidth() {
		
		return 1290.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 25400.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.36;
	}

	@Override
	public double getMinWidth() {
		
		return 164.0;
	}

	@Override
	public double getMinWeight() {
		
		return 353.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
