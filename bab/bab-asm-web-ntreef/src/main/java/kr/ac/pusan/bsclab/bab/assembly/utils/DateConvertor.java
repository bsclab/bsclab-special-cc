package kr.ac.pusan.bsclab.bab.assembly.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class DateConvertor {

	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * yyyy-MM-dd date format string convert to timestamp
	 * @param strDate
	 * @return
	 * @throws ParseException 
	 */
	public Timestamp getTimestamp(String strDate) throws ParseException {
//		logger.debug(strDate);
		return new Timestamp(sdf.parse(strDate).getTime());
	}
	
	public Timestamp getTimestamp(Date date) {
		return new Timestamp(date.getTime());
	}
	
	public String getDateStr(Date date) {
		return sdf.format(date);
	}
	
	public Timestamp getNow() {
		return new Timestamp(new Date().getTime());
	}
}
