package kr.ac.pusan.bsclab.bab.assembly.abs;

import org.springframework.beans.factory.annotation.Autowired;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.HdfsProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.SparkProperties;

public abstract class AbstractModel {
	
	@Autowired
	protected HdfsProperties hp;
	
	@Autowired
	protected AppProperties ap;
	
	@Autowired
	protected SparkProperties sp;

}
