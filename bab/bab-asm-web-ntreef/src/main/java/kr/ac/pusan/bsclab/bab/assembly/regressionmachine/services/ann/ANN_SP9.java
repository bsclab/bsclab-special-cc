package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SP9 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{3.85597140e-01, -3.04937649e+00, -2.10212350e+00, -1.70722628e+00
            				, -3.63359165e+00, -5.47843218e-01, -2.64278913e+00, -1.77265847e+00
            				, 1.87043774e+00, -2.59650970e+00, -3.98941350e+00, 5.82017720e-01
            				, -4.96628731e-01, -1.93082964e+00, -4.30189103e-01, 5.18930550e+01
            				, -2.61194777e+00, -1.17626786e+00, -1.58202183e+00, 3.64995971e-02}, 
            			{-9.69619155e-02, -5.12354493e-01, -8.85208786e-01, -8.80032182e-02
        					, 9.35025871e-01, -3.58804727e+00, -1.55267048e+00, -3.07875252e+00
        					, -1.27512550e+00, -2.42984936e-01, -7.34318912e-01, 1.60132885e+00
        					, -1.91697097e+00, -6.53099358e-01, -1.40206575e+00, -2.91732121e+00
        					, -2.31197819e-01, 1.03301156e+00, 1.03511112e-02, -1.48652160e+00},
            			{-1.84504110e-02, -1.24706812e-01, 2.90883213e-01, 1.52441273e-02
    						, 1.17167962e+00, -1.29266536e+00, -1.99762106e-01, 2.16765374e-01
    						, 3.63908485e-02, 2.94799507e-01, 1.38599336e+00, -1.23361659e+00
    						, 3.28754008e-01, 1.74637663e+00, 1.26637459e-01, 2.13137746e+00
    						, -9.08087343e-02, -1.17502439e+00, 4.10417974e-01, 2.34661818e-01}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-1.72912514, 0.250779, 1.39243746, -0.20545378, 0.49574447, -0.0026062
            			, 0.54883987, -0.77348399, -1.72882771, 0.16587126, -0.10265815, -1.25308132
            			, -1.37723744, -0.5686906, -0.07748327, -0.52628398, 0.08051533, -0.38476497
            			, 0.42740151, -0.60700691};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.64693552,
            			0.58394831,
            			0.70651346,
            			-0.6493445,
            			1.9328717,
            			0.3769173,
            			1.65670037,
            			0.2796309,
            			-0.23469262,
            			0.32094133,
            			1.48277628,
            			-0.43512598,
            			0.11716673,
            			0.89837939,
            			0.01786352,
            			-2.74809551,
            			1.4944315,
            			1.04138279,
            			0.26964429,
            			0.19945292};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-0.5867644};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 5.1;
	}

	@Override
	public double getMaxWidth() {
		
		return 650.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 13306.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.35;
	}

	@Override
	public double getMinWidth() {
		
		return 237.0;
	}

	@Override
	public double getMinWeight() {
		
		return 2163.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
