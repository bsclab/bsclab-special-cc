package kr.ac.pusan.bsclab.bab.assembly.job;


import kr.ac.pusan.bsclab.bab.assembly.domain.info.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.Report;

public interface JobListener {
	
	public int save(Job job);
	
	public int updateByJobId(Job job);
	
	public int updateByNo(Job job);
	
	public int save(Report report);

}
