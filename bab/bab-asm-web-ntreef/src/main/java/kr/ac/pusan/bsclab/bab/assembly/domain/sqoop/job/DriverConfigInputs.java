package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job;

import java.util.ArrayList;
import java.util.List;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.InputsFactory;

public class DriverConfigInputs implements InputsFactory {

	
	/**
	 * args[0] : numExtractors
	 * args[1] : numLoaders
	 */
	@Override
	public Input[] createInputs(Object... args) {
		List<Input> inputs = new ArrayList<Input>();
		inputs.add(new Input(0, "ANY", "throttlingConfig.numExtractors", 36, false, "", "INTEGER", args[0], null));
		inputs.add(new Input(0, "ANY", "throttlingConfig.numLoaders", 37, false, "", "INTEGER", args[1], null));
		return inputs.toArray(new Input[0]);
	}

}
