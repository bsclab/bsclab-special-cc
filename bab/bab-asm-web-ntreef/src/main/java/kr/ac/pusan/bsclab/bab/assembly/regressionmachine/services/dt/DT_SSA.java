package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SSA implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("CRA1332")) return 30;
if(p.equals("PP12184"))
 if(w <= 48.99)
  if(we <= 147)
   if(we <= 77) return 90;
   if(we > 77) return 60;
  if(we > 147)
   if(we <= 164) return 20;
   if(we > 164) return 50;
 if(w > 48.99)
  if(we <= 168) return 60;
  if(we > 168)
   if(t <= 0.528) return 65;
   if(t > 0.528) return 51.9166666666667;
if(p.equals("PP12186")) return 60;
if(p.equals("PP12187")) return 60;
if(p.equals("PP12188"))
 if(we <= 78) return 60;
 if(we > 78)
  if(w <= 369) return 50;
  if(w > 369) return 60;
if(p.equals("PP12190")) return 80;
if(p.equals("PP12192"))
 if(t <= 0.49) return 60;
 if(t > 0.49) return 90;
if(p.equals("PP12193")) return 30;
if(p.equals("PP12194"))
 if(t <= 0.407)
  if(we <= 434) return 90;
  if(we > 434) return 50;
 if(t > 0.407) return 60;
if(p.equals("PP12195"))
 if(w <= 369) return 70;
 if(w > 369) return 60;
if(p.equals("PP12196"))
 if(w <= 341) return 10;
 if(w > 341) return 50;
if(p.equals("PP12201")) return 60;
if(p.equals("PP12222")) return 60;
if(p.equals("PP12229"))
 if(w <= 88)
  if(we <= 61)
   if(we <= 59) return 50;
   if(we > 59) return 45;
  if(we > 61)
   if(we <= 76) return 60;
   if(we > 76)
    if(w <= 85.5)
     if(t <= 1.21) return 70;
     if(t > 1.21) return 60;
    if(w > 85.5) return 60;
 if(w > 88)
  if(we <= 2096)
   if(we <= 1688) return 50;
   if(we > 1688) return 40;
  if(we > 2096) return 60;
if(p.equals("PP12230")) return 60;
if(p.equals("PP12232")) return 60;
if(p.equals("PP12234"))
 if(t <= 0.308)
  if(we <= 1567) return 60;
  if(we > 1567) return 90;
 if(t > 0.308)
  if(we <= 1414) return 30;
  if(we > 1414) return 60;
if(p.equals("PP12235")) return 30;
if(p.equals("PP12245")) return 60;
if(p.equals("PP12247")) return 30;
if(p.equals("PP12248"))
 if(we <= 59) return 50;
 if(we > 59) return 60;
if(p.equals("PP12251"))
 if(t <= 1.1)
  if(w <= 166) return 30;
  if(w > 166) return 75;
 if(t > 1.1)
  if(w <= 168) return 40;
  if(w > 168)
   if(w <= 195)
    if(w <= 179.37) return 60;
    if(w > 179.37) return 40;
   if(w > 195) return 30;
if(p.equals("PP12256"))
 if(w <= 130) return 110;
 if(w > 130)
  if(w <= 312)
   if(t <= 0.25)
    if(w <= 245) return 60;
    if(w > 245) return 50;
   if(t > 0.25) return 60;
  if(w > 312)
   if(w <= 369) return 80;
   if(w > 369) return 60;
if(p.equals("PP12263")) return 60;
if(p.equals("PP12266")) return 60;
if(p.equals("PP12273")) return 60;
if(p.equals("PP12282"))
 if(w <= 83)
  if(we <= 56.5) return 50;
  if(we > 56.5)
   if(t <= 1.21)
    if(w <= 82) return 40;
    if(w > 82) return 70;
   if(t > 1.21) return 60;
 if(w > 83)
  if(we <= 1040) return 40;
  if(we > 1040) return 60;
if(p.equals("PP12285")) return 60;
if(p.equals("PP12286"))
 if(w <= 162.5) return 30;
 if(w > 162.5)
  if(w <= 168) return 40;
  if(w > 168)
   if(w <= 181) return 30;
   if(w > 181) return 40;
if(p.equals("PP12296")) return 60;
if(p.equals("PP12297")) return 60;
if(p.equals("PP12298")) return 90;
if(p.equals("PP12306")) return 60;
if(p.equals("PP12307")) return 40;
if(p.equals("PP12326"))
 if(t <= 0.45) return 30;
 if(t > 0.45) return 60;
if(p.equals("PP12327"))
 if(w <= 116)
  if(we <= 320)
   if(we <= 314) return 50;
   if(we > 314) return 30;
  if(we > 320) return 40;
 if(w > 116)
  if(t <= 0.408)
   if(t <= 0.33)
    if(we <= 1458) return 60;
    if(we > 1458) return 70;
   if(t > 0.33)
    if(w <= 128) return 60;
    if(w > 128)
     if(t <= 0.3905) return 30;
     if(t > 0.3905) return 60;
  if(t > 0.408)
   if(we <= 2952) return 45;
   if(we > 2952) return 60;
if(p.equals("PP12328"))
 if(w <= 103)
  if(w <= 102.5) return 45;
  if(w > 102.5) return 60;
 if(w > 103)
  if(we <= 434)
   if(t <= 0.33) return 60;
   if(t > 0.33)
    if(w <= 148.5)
     if(we <= 389) return 50;
     if(we > 389) return 58;
    if(w > 148.5) return 20;
  if(we > 434)
   if(we <= 514)
    if(we <= 466) return 40;
    if(we > 466) return 30;
   if(we > 514)
    if(t <= 0.483)
     if(t <= 0.33) return 40;
     if(t > 0.33)
      if(w <= 144)
       if(we <= 887)
        if(we <= 770) return 60;
        if(we > 770) return 40;
       if(we > 887)
        if(w <= 139) return 70;
        if(w > 139) return 50;
      if(w > 144)
       if(we <= 936) return 40;
       if(we > 936) return 60;
    if(t > 0.483)
     if(we <= 4326)
      if(we <= 3879)
       if(w <= 361.5) return 40;
       if(w > 361.5) return 60;
      if(we > 3879) return 50;
     if(we > 4326) return 60;
if(p.equals("PP12329"))
 if(w <= 25) return 60;
 if(w > 25)
  if(w <= 32) return 90;
  if(w > 32) return 60;
if(p.equals("PP12330")) return 60;
if(p.equals("PP12332"))
 if(t <= 0.93)
  if(w <= 77) return 80;
  if(w > 77) return 90;
 if(t > 0.93) return 60;
if(p.equals("PP12342"))
 if(we <= 274) return 90;
 if(we > 274)
  if(w <= 72) return 80;
  if(w > 72) return 60;
if(p.equals("PP12344")) return 90;
if(p.equals("PP12349")) return 50;
if(p.equals("PP12350")) return 70;
if(p.equals("PP12351")) return 60;
if(p.equals("PP12352")) return 60;
if(p.equals("PP12353"))
 if(we <= 1252) return 30;
 if(we > 1252) return 60;
if(p.equals("PP12356")) return 60;
if(p.equals("PP12357"))
 if(we <= 727)
  if(w <= 103)
   if(we <= 658) return 50;
   if(we > 658) return 60;
  if(w > 103)
   if(w <= 108.5) return 60;
   if(w > 108.5) return 40;
 if(we > 727) return 60;
if(p.equals("PP12358")) return 60;
if(p.equals("PP12361"))
 if(w <= 103) return 90;
 if(w > 103)
  if(w <= 108.5) return 60;
  if(w > 108.5)
   if(w <= 369) return 30;
   if(w > 369) return 60;
if(p.equals("PP12362")) return 60;
if(p.equals("PP12363")) return 60;
if(p.equals("PP12364"))
 if(we <= 520) return 60;
 if(we > 520) return 90;
if(p.equals("PP12370")) return 90;
if(p.equals("PP12373")) return 90;
if(p.equals("PP12377"))
 if(t <= 0.725) return 60;
 if(t > 0.725) return 75;
if(p.equals("PP12378")) return 70;
if(p.equals("PP12399")) return 50;
if(p.equals("PP12414")) return 70;
if(p.equals("PP12422"))
 if(w <= 369) return 70;
 if(w > 369) return 60;
if(p.equals("PP12428"))
 if(we <= 3191) return 60;
 if(we > 3191) return 30;
if(p.equals("PP12429"))
 if(w <= 369) return 40;
 if(w > 369) return 60;
if(p.equals("PP12432")) return 60;
if(p.equals("PP12433"))
 if(w <= 126) return 60;
 if(w > 126)
  if(t <= 1.075) return 90;
  if(t > 1.075) return 60;
if(p.equals("PP12437")) return 60;
if(p.equals("PP12444"))
 if(w <= 369) return 90;
 if(w > 369) return 60;
if(p.equals("PP12460")) return 75;
if(p.equals("PP12464")) return 40;
if(p.equals("PP12466")) return 60;
if(p.equals("PP12468")) return 50;
if(p.equals("PP12479"))
 if(we <= 1090)
  if(w <= 162) return 75;
  if(w > 162) return 50;
 if(we > 1090)
  if(we <= 1096) return 60;
  if(we > 1096) return 45;
if(p.equals("PP12484"))
 if(w <= 369) return 20;
 if(w > 369) return 60;
if(p.equals("PP12491")) return 40;
if(p.equals("PP12521")) return 60;
if(p.equals("PP12525")) return 60;
if(p.equals("PP12526"))
 if(w <= 257)
  if(w <= 245) return 60;
  if(w > 245)
   if(we <= 1636) return 30;
   if(we > 1636) return 60;
 if(w > 257) return 40;
if(p.equals("PP12529")) return 60;
if(p.equals("PP12530")) return 50;
if(p.equals("PP12533"))
 if(w <= 369) return 40;
 if(w > 369) return 60;
if(p.equals("PP12544"))
 if(w <= 70) return 65;
 if(w > 70)
  if(w <= 90) return 60;
  if(w > 90) return 90;
if(p.equals("PP12558")) return 60;
if(p.equals("PP12565")) return 75;
if(p.equals("PP21909")) return 80;
if(p.equals("PP21918")) return 30;
if(p.equals("PP21923")) return 80;
if(p.equals("PP21926"))
 if(w <= 172)
  if(w <= 112) return 30;
  if(w > 112)
   if(t <= 1.225) return 45;
   if(t > 1.225) return 60;
 if(w > 172)
  if(t <= 0.345) return 60;
  if(t > 0.345) return 50;
if(p.equals("PP21927"))
 if(we <= 2050)
  if(we <= 1869) return 60;
  if(we > 1869) return 50;
 if(we > 2050) return 60;
if(p.equals("PP21931"))
 if(t <= 0.8) return 90;
 if(t > 0.8)
  if(we <= 395)
   if(t <= 0.8625) return 60;
   if(t > 0.8625)
    if(w <= 50.995) return 60;
    if(w > 50.995) return 50;
  if(we > 395)
   if(we <= 400) return 30;
   if(we > 400) return 40;
if(p.equals("PP21933")) return 75;
if(p.equals("PP21935")) return 60;
if(p.equals("PP21937")) return 60;
if(p.equals("PP21939")) return 60;
if(p.equals("PP21945"))
 if(w <= 369) return 100;
 if(w > 369) return 60;
if(p.equals("PP21957")) return 80;
if(p.equals("PP21973")) return 110;
if(p.equals("PP21976")) return 60;
if(p.equals("PP22001"))
 if(t <= 1.22)
  if(t <= 1.1) return 90;
  if(t > 1.1) return 60;
 if(t > 1.22)
  if(we <= 376) return 30;
  if(we > 376)
   if(t <= 1.26) return 60;
   if(t > 1.26) return 50;
if(p.equals("PP22003")) return 60;
if(p.equals("PP22014")) return 60;
if(p.equals("PP22035"))
 if(w <= 150) return 60;
 if(w > 150) return 40;
if(p.equals("PP22036")) return 60;
if(p.equals("PP22038")) return 80;
if(p.equals("PP22071"))
 if(we <= 142)
  if(we <= 137) return 70;
  if(we > 137) return 10;
 if(we > 142)
  if(w <= 341)
   if(we <= 1970) return 60;
   if(we > 1970) return 30;
  if(w > 341) return 60;
if(p.equals("PP22072")) return 65;
if(p.equals("PP22074"))
 if(w <= 369) return 20;
 if(w > 369) return 60;
if(p.equals("PP22080"))
 if(t <= 0.33)
  if(w <= 169) return 60;
  if(w > 169) return 90;
 if(t > 0.33)
  if(w <= 310)
   if(we <= 946)
    if(w <= 144)
     if(we <= 909)
      if(t <= 1.21) return 40;
      if(t > 1.21) return 60;
     if(we > 909) return 60;
    if(w > 144) return 60;
   if(we > 946)
    if(w <= 153)
     if(we <= 970) return 40;
     if(we > 970) return 50;
    if(w > 153) return 40;
  if(w > 310) return 60;
if(p.equals("PP22083"))
 if(w <= 37) return 50;
 if(w > 37) return 80;
if(p.equals("PP22093"))
 if(w <= 163.5)
  if(w <= 81)
   if(we <= 56.5) return 50;
   if(we > 56.5)
    if(w <= 80.5)
     if(t <= 1.21) return 50;
     if(t > 1.21) return 60;
    if(w > 80.5) return 40;
  if(w > 81) return 60;
 if(w > 163.5)
  if(we <= 1900) return 50;
  if(we > 1900) return 45;
if(p.equals("PP22094")) return 60;
if(p.equals("PP22111"))
 if(w <= 369) return 70;
 if(w > 369) return 60;
if(p.equals("PP22112")) return 60;
if(p.equals("PP22132")) return 60;
if(p.equals("PP22135")) return 40;
if(p.equals("PP22136")) return 60;
if(p.equals("PP22137"))
 if(t <= 1.095)
  if(w <= 369) return 30;
  if(w > 369) return 60;
 if(t > 1.095) return 60;
if(p.equals("PP22159"))
 if(t <= 0.975)
  if(w <= 374.5) return 40;
  if(w > 374.5) return 60;
 if(t > 0.975)
  if(w <= 93)
   if(w <= 77) return 60;
   if(w > 77) return 70;
  if(w > 93) return 60;
if(p.equals("PP22174"))
 if(we <= 75) return 60;
 if(we > 75)
  if(w <= 369) return 30;
  if(w > 369) return 60;
if(p.equals("PP22183")) return 60;
if(p.equals("PP22186")) return 45;
if(p.equals("PP22195"))
 if(w <= 369) return 110;
 if(w > 369) return 60;
if(p.equals("PP22202"))
 if(we <= 254) return 80;
 if(we > 254) return 60;
if(p.equals("PP22225"))
 if(w <= 369) return 70;
 if(w > 369) return 60;
if(p.equals("PP22230")) return 60;
if(p.equals("PP22231")) return 40;
if(p.equals("PP22256")) return 60;
if(p.equals("PP22261"))
 if(w <= 369) return 50;
 if(w > 369) return 60;
if(p.equals("PP22262"))
 if(we <= 4676) return 60;
 if(we > 4676)
  if(we <= 4748) return 30;
  if(we > 4748) return 45;
if(p.equals("PP22263"))
 if(w <= 369) return 45;
 if(w > 369) return 60;
if(p.equals("PP22264")) return 30;
if(p.equals("PP22267"))
 if(w <= 369) return 30;
 if(w > 369) return 60;
if(p.equals("PP22268"))
 if(w <= 93) return 90;
 if(w > 93) return 60;
if(p.equals("PP22297"))
 if(we <= 182)
  if(we <= 180) return 58.95;
  if(we > 180) return 50;
 if(we > 182)
  if(w <= 58)
   if(we <= 185) return 30;
   if(we > 185) return 60;
  if(w > 58)
   if(we <= 194) return 30;
   if(we > 194) return 80;
if(p.equals("PP22298"))
 if(t <= 0.7) return 60;
 if(t > 0.7)
  if(w <= 369) return 30;
  if(w > 369) return 60;
if(p.equals("PP22299")) return 90;
if(p.equals("PP22317"))
 if(w <= 227) return 35;
 if(w > 227) return 30;
if(p.equals("PP22345")) return 50;
if(p.equals("PP22350")) return 60;
if(p.equals("PP22351"))
 if(we <= 702) return 60;
 if(we > 702) return 30;
if(p.equals("PP22354"))
 if(w <= 32) return 50;
 if(w > 32) return 75;
if(p.equals("PP22355")) return 40;
if(p.equals("PP22361"))
 if(w <= 369) return 110;
 if(w > 369) return 60;
if(p.equals("PP22363")) return 60;
if(p.equals("PP22367")) return 60;
if(p.equals("PP22389")) return 60;
if(p.equals("PP22408")) return 60;
if(p.equals("PP22425"))
 if(we <= 167)
  if(t <= 0.73)
   if(we <= 163) return 55;
   if(we > 163) return 60;
  if(t > 0.73) return 60;
 if(we > 167)
  if(w <= 369) return 50;
  if(w > 369) return 60;
if(p.equals("PP22426"))
 if(we <= 1842)
  if(we <= 926) return 45;
  if(we > 926) return 30;
 if(we > 1842) return 45;
if(p.equals("PP22427")) return 30;
if(p.equals("PP22435"))
 if(w <= 369) return 40;
 if(w > 369) return 60;
if(p.equals("PP22448")) return 60;
if(p.equals("PP22456")) return 60;
if(p.equals("PP22457")) return 60;
if(p.equals("PP22460")) return 45;
if(p.equals("PP22468")) return 60;
if(p.equals("PP22470")) return 70;
if(p.equals("PP22482")) return 60;
if(p.equals("PP22487")) return 60;
if(p.equals("PP22489")) return 40;
if(p.equals("PP22492")) return 60;
if(p.equals("PP22499")) return 60;
if(p.equals("PP22500")) return 60;
if(p.equals("PP22502")) return 30;
if(p.equals("PP22512")) return 70;
if(p.equals("PP22540")) return 120;
if(p.equals("PP22541")) return 60;
if(p.equals("PP22550")) return 60;
if(p.equals("PP22553")) return 60;
if(p.equals("WS21238")) return 30;
if(p.equals("WS21267"))
 if(we <= 1326) return 40;
 if(we > 1326) return 35;
return 35.0;
}
}
