package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;

import java.sql.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import kr.ac.pusan.bsclab.bab.assembly.abs.AbstractController;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueue;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.JobType;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.SparkJobConfiguration;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.SqoopJobConfiguration;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.JobSubmission;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.ImportJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;

@Controller
public abstract class AbstractServiceController extends AbstractController {

	private static final Logger logger
		= LoggerFactory.getLogger(AbstractServiceController.class);
	
	
	
	
//	protected static JobQueue jobQueue;

//	@Bean
//	public static JobQueue getJobQueue() {
//		if (jobQueue == null) {
//			// jobQueue = new JobQueue(BabServer.CONFIG.BAB_QUEUE_SIZE);
//			jobQueue = new JobQueue(1);
//
//		}
//		return jobQueue;
//	}

	@Autowired
	protected RequestMappingHandlerMapping handlerMapping;
//	
//
//	public <T> Response<T> submitJob(String jobClass, String jobExtension, String workspaceId,
//			String datasetId, String repositoryId, String configuration, Class<T> responseClass) {
//		return submitJob(jobClass, jobExtension, workspaceId, datasetId, repositoryId, configuration, null,
//				responseClass);
//	}
//
//	public <T> Response<T> submitJob(String jobClass, String jobExtension, String workspaceId,
//			String datasetId, String repositoryId, String configuration, String resultPath, Class<T> responseClass) {
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			// SimpleAbstractTypeResolver resolver = new
//			// SimpleAbstractTypeResolver();
//			// resolver.addMapping(IRepository.class, BRepository.class);
//			SimpleModule module = new SimpleModule();
//			module.addAbstractTypeMapping(IRepository.class, BRepository.class);
//			mapper.registerModule(module);
//			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//			mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
//			String hash = DigestUtils.md5Hex(configuration);
//			JobSubmission submission = new JobSubmission(hp.getHome(), hash, jobClass, jobExtension, workspaceId, datasetId,
//					repositoryId, configuration);
//			if (!webHdfs.isExists(submission.getJobPath())) {
//				webHdfs.saveAsTextFile(submission.getJobPath(), mapper.writeValueAsString(submission));
//			}
//			if (resultPath != null) {
//				submission.setResultPath(resultPath);
//			}
//			String result = null;
//			T response = null;
//			if (webHdfs.isExists(submission.getResultPath())) {
//				result = webHdfs.openAsTextFile(submission.getResultPath());
//				response = mapper.readValue(result, responseClass);
//			} else {
//				getJobQueue().submitJob(submission);
//			}
//			Response<T> responseObj = new Response<T>(submission, response);
//			if (webHdfs.isExists(submission.getResultPath())) {
//				responseObj.setStatus(Response.STATUS_FINISHED);
//			} else if (getJobQueue().getRunning().containsKey(submission.getJobId())) {
//				responseObj.setStatus(Response.STATUS_RUNNING);
//			} else {
//				responseObj.setStatus(Response.STATUS_QUEUED);
//			}
//			return responseObj;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
//	public	<T> Response<T> submitJob(String jobClass, String jobExtension
//			, String workspaceId, String datasetId, String configuration, Timestamp sdt, Timestamp edt, Class<T> responseClass) {
//		return this.submitJob(jobClass, jobExtension, workspaceId, datasetId, configuration, sdt, edt, responseClass);
//	}
	
	public	<T1> Response<T1> submitJob(String jobClass, String jobExtension
			, String workspaceId, String datasetId, String repositoryId, String hash, String config
			, Timestamp sdt, Timestamp edt,  Class<T1> responseClass) {
		
		try {
//			ObjectMapper configOm = new ObjectMapper();
			ObjectMapper submissionOm = new ObjectMapper();
			SimpleModule submissionModule = new SimpleModule();
			submissionModule.addAbstractTypeMapping(IRepository.class, BRepository.class);
			submissionOm.registerModule(submissionModule);
			submissionOm.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			submissionOm.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			
			
//			String repositoryId = logDao.getChecksum(sdt, edt);
//			String hash = DigestUtils.md5Hex(config);
//			String resultPath = hp.getUserHome() + "/" + workspaceId + "/" + datasetId + "/" + repositoryId;
			
			JobSubmission mainJobSubmission = new JobSubmission(hp.getHome(), hash, jobClass, jobExtension
					, workspaceId, datasetId, repositoryId, config, sdt, edt);
			
			if (!webHdfs.isExists(mainJobSubmission.getJobPath()))
				webHdfs.saveAsTextFile(mainJobSubmission.getJobPath(), submissionOm.writeValueAsString(mainJobSubmission));
			
			
			String result = null;
			T1 response = null;
			
			logger.debug("check final file ={}", mainJobSubmission.getResultPath());
			/*
			if (ap.getTestMode() == 1) {
				if (webHdfs.isExists(mainJobSubmission.getResultPath())) {
					System.out.println("TESTMODE: DELETE " + mainJobSubmission.getResultPath());
					webHdfs.delete(mainJobSubmission.getResultPath());
				}
			}
			*/
			
			if (webHdfs.isExists(mainJobSubmission.getResultPath())) {
				result = webHdfs.openAsTextFile(mainJobSubmission.getResultPath());
				response = submissionOm.readValue(result,  responseClass);
			} else {
				
				JobQueue jobQueue = jobQueueFactory.createJobQueue(mainJobSubmission.getJobId());				
				
				if (webHdfs.listStatus(mainJobSubmission.getResourcePath()) == null) {
					Job<SqoopJobConfiguration> sqoopJob = jf.createJob(JobType.SQOOP
							, jcf.createSqoopJobConfiguration(
									sdt, edt, sqp.getServer()
									, ap.getLogTableName(), mainJobSubmission.getResourcePath()
									, ap.getJdbcDriver(), ap.getConnectionString(), ap.getDbUsername(), ap.getDbPassword()));
					jobQueue.add(sqoopJob);
				}
				
				if (!webHdfs.isExists(mainJobSubmission.getResultDir() + "/" + repositoryId + ".mrepo")) {
					ObjectMapper mrepoOm = new ObjectMapper();
					ImportJobConfiguration ijc = new ImportJobConfiguration();
					ijc.setRawPath(mainJobSubmission.getResourcePath());
					ijc.setRepositoryURI(mainJobSubmission.getResultDir() + "/" + repositoryId);
					JobSubmission mrepoJobSubmission = new JobSubmission(hp.getHome(), repositoryId, "RepositoryCsvImportJob", "mrepo"
							, workspaceId, datasetId, repositoryId, mrepoOm.writeValueAsString(ijc)
							, sdt, edt);
					
					if (!webHdfs.isExists(mrepoJobSubmission.getJobPath()))
						webHdfs.saveAsTextFile(mrepoJobSubmission.getJobPath(), submissionOm.writeValueAsString(mrepoJobSubmission));
					
					
					Job<SparkJobConfiguration> mrepoSparkJob = jf.createJob(JobType.SPARK
							, jcf.createSparkJobConfiguration(mrepoJobSubmission.getJobId()
									, mrepoJobSubmission.getJobClass()
									, mrepoJobSubmission.getJobPath(), ap.getName()));
					
					
					
					jobQueue.add(mrepoSparkJob);
				}

//				if (!webHdfs.isExists(mainJobSubmission.getResultDir() + "/" + repositoryId + ".brepo")) {
////					String irepoPath = mainJobSubmission.getResultDir() + "/" + repositoryId + ".irepo";
////					List<FileStatus> irepoStatus = webHdfs.listStatus(irepoPath);
//					
//					Map<String, String> _case = new HashMap<String, String>();
//					_case.put("coil_no", "CASE");
//					Map<String, String> activity = new HashMap<String, String>();
//					activity.put("prc_cd", "EVENT");
//					Map<String, String> timestamp = new HashMap<String, String>();
//					timestamp.put("sdt", "EVENT,yyyy-MM-dd HH:mm:ss.SSS");
//					timestamp.put("edt", "EVENT,yyyy-MM-dd HH:mm:ss.SSS");
//					Map<String, String> originator = new HashMap<String, String>();
//					originator.put("emg_nam", "EVENT");
//					
//					
//					Map<String, Map<String, String>> mapping
//						= new HashMap<String, Map<String, String>>();
//					mapping.put("CASE", _case);
//					mapping.put("ACTIVITY", activity);
//					mapping.put("TIMESTAMP", timestamp);
//					mapping.put("ORIGINATOR", originator);
//					
//					
//					MappingJobConfiguration mjc = new MappingJobConfiguration();
//					mjc.setDescription("description");
//					mjc.setMapping(mapping);
//					mjc.setName("repository");
//					
//					mjc.setRepositoryURI(mainJobSubmission.getResultDir() + "/" + repositoryId);
//					
//					ObjectMapper brepoOm = new ObjectMapper();
//
//					JobSubmission brepoJobSubmission = new JobSubmission(hp.getHome(), hash, "RepositoryMappingJob", "brepo"
//							, workspaceId, datasetId, repositoryId, brepoOm.writeValueAsString(mjc)
//							, sdt, edt);
//					
//					if (!webHdfs.isExists(brepoJobSubmission.getJobPath()))
//						webHdfs.saveAsTextFile(brepoJobSubmission.getJobPath(), submissionOm.writeValueAsString(brepoJobSubmission));
//					
//					
//					Job<SparkJobConfiguration> brepoSparkJob = jf.createJob(JobType.SPARK
//							, jcf.createSparkJobConfiguration(brepoJobSubmission.getJobId()
//									, brepoJobSubmission.getJobClass()
//									, brepoJobSubmission.getJobPath()
//									, ap.getName()));
//					
//					jobQueue.add(brepoSparkJob);
//				}
				
				Job<SparkJobConfiguration> mainSparkJob = jf.createJob(JobType.SPARK
						, jcf.createSparkJobConfiguration(mainJobSubmission.getJobId()
								, mainJobSubmission.getJobClass(), mainJobSubmission.getJobPath(), ap.getName()));
				
				jobQueue.add(mainSparkJob);
				jobManager.runJobQueue(jobQueue);
				
				
			}
			
			Response<T1> responseObj = new Response<T1>(mainJobSubmission, repositoryId, response);
						
			if (response != null) {
				responseObj.setStatus(Response.STATUS_FINISHED);
			} else {
				responseObj.setStatus(Response.STATUS_RUNNING);
			}
			return responseObj;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String resourceDataHash(Timestamp sdt, Timestamp edt) {
		return logDao.getChecksum(dc.getTimestamp(sdt), dc.getTimestamp(edt));
	}
	
}
