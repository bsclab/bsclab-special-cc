package kr.ac.pusan.bsclab.bab.assembly.ws.models;

//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//import java.util.Queue;
//import java.util.TreeMap;
//import java.util.concurrent.LinkedBlockingQueue;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//import kr.ac.pusan.bsclab.bab.assembly.domain.spark.SparkDriverStatus;
//import kr.ac.pusan.bsclab.bab.assembly.domain.spark.SparkSubmissionStatus;
//import kr.ac.pusan.bsclab.bab.assembly.utils.hdfs.WebHdfs;
//import kr.ac.pusan.bsclab.bab.assembly.utils.spark.SparkWeb;
//import kr.ac.pusan.bsclab.bab.assembly.utils.spark.SparkSubmission;
//import kr.ac.pusan.bsclab.bab.assembly.ws.controllers.BabWebServiceController;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.AbstractServiceModel;


public class JobQueue extends AbstractServiceModel {
	
	/*
	
	private final int size;
	private int submitted;
	private int processed;
	private Queue<JobSubmission> queue;
	private Map<String, JobSubmission> running;
	private List<JobSubmission> finished;
	
	@JsonIgnore
	@Autowired
	private WebHdfs webHdfs;
	
	@JsonIgnore
	@Autowired
	private SparkWeb sparkRest;
	
	public JobQueue(int size) {
		this.size = size;
	}
	
	public int getSize() {
		return size;
	}
	
	public int getSubmitted() {
		return submitted;
	}
	
	public int getProcessed() {
		return processed;
	}

	@JsonIgnore
	public WebHdfs getHdfs() {
//		if (webHdfs == null) {
//			webHdfs = new WebHdfs();
//		}
		return webHdfs;
	}
	
	@JsonIgnore
	public SparkWeb getSpark() {
//		if (sparkRest == null) {
//			sparkRest = new SparkRest();
//		}
		return sparkRest;
	}
	
	public Queue<JobSubmission> getQueue() {
		if (queue == null) {
			queue = new LinkedBlockingQueue<JobSubmission>();
		}
		return queue;
	}
	
	public  Map<String, JobSubmission> getRunning() {
		if (running == null) {
			running = new TreeMap<String, JobSubmission>();
		}
		return running;
	}
	
	public List<JobSubmission> getFinished() {
		if (finished == null) {
			finished = new ArrayList<JobSubmission>();
		}
		return finished;
	}
	
	public JobSubmission submitJob(JobSubmission submission) {
		submitted++;
		getQueue().add(submission);
		pull();
		return submission;
	}

	public JobSubmission pull() {
		List<String> failingJobs = new ArrayList<String>(); 
		for (String jobId : getRunning().keySet()) {
			JobSubmission job = getRunning().get(jobId);
			SparkDriverStatus status = getSpark().status(job.getSparkStatus());
			if (!status.driverState.equalsIgnoreCase("RUNNING") && !status.driverState.equalsIgnoreCase("WAITING")) {
				failingJobs.add(jobId);
			}
		}
		for (String failingJobId : failingJobs) {
			JobSubmission failingJob = getRunning().get(failingJobId);
			getSpark().kill(failingJob.getSparkStatus());
			getRunning().remove(failingJobId);
			SparkDriverStatus status = getSpark().status(failingJob.getSparkStatus());
			failingJob.setSparkStatus(status);
			getFinished().add(failingJob);
		}
		if (getRunning().size() < size) {
			JobSubmission job = getQueue().poll();
			if (job != null) {
				if ((!getHdfs().isExists(job.getResultPath()))) {
					processed++;
//					SparkSubmissionStatus status = getSpark().submit(new SparkSubmission(job.getJobId(), job.getJobClass(), job.getJobPath()));
					SparkSubmissionStatus status = getSpark().submit(new SparkSubmission(ap, hp, sp, job.getJobId(), job.getJobClass(), job.getJobPath(), ap.getWebServer()));
					job.setSparkStatus(status);
					job.setForceKillUrl(BabWebServiceController.BASE_URL + "/kill/" + job.getJobId());
					getRunning().put(job.getJobId(), job);
				} else {
					getFinished().add(job);
				}
				return job;
			}
		}
		return null;
	}
	
	public JobSubmission kill(String jobId) {
		if (getRunning().containsKey(jobId)) {
			JobSubmission job = getRunning().get(jobId);
			getSpark().kill(job.getSparkStatus());
			getRunning().remove(jobId);
			SparkDriverStatus status = getSpark().status(job.getSparkStatus());
			job.setSparkStatus(status);
			getFinished().add(job);
			pull();
			return job;
		}
		return null;
	}	
	
	public JobSubmission report(String jobId) {
		if (getRunning().containsKey(jobId)) {
			JobSubmission job = getRunning().get(jobId);
			getRunning().remove(jobId);
			SparkDriverStatus status = getSpark().status(job.getSparkStatus());
			job.setSparkStatus(status);
			getFinished().add(job);
			pull();
			return job;
		}
		return null;
	}
	
	*/

}
