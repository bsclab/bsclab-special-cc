package kr.ac.pusan.bsclab.bab.assembly.rserver.param;

/**
 * Class for defining output object
 * output object is path of graph resulting from RStudio server analysis
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */

public class GraphOutput {

	private String output;

	public GraphOutput() {
	}

	public GraphOutput(String string) {
		output = string;
	}

	/**
	 * @return output
	 */
	public String getOutput() {
		return output;
	}

	/**
	 * @param output for setting path url of graph
	 */
	public void setOutput(String output) {
		this.output = output;
	}
	
}
