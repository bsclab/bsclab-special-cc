
package kr.ac.pusan.bsclab.bab.assembly.job;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueue;
import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.JobQueueStatus;

@Component
public class JobManager {
	
	private Map<String, JobQueue> managedJobQueues;
	
	@Autowired
	private JobRunner jobRunner;
	
	JobManager() {
		managedJobQueues = new LinkedHashMap<String, JobQueue>();
	}
	
	public void runJobQueue(JobQueue jobQueue)
			throws JsonParseException, JsonMappingException, IOException, InterruptedException {
		jobQueue.setJobQueueStatus(JobQueueStatus.RUNNING);
		managedJobQueues.put(jobQueue.getJobQueueId(), jobQueue);
		jobRunner.run(jobQueue);
	}
	
	public void killJobQueue(JobQueue jobQueue) {
		
	}
	
	public boolean isRunning(String jobQueueId) {
		JobQueue jobQueue = managedJobQueues.get(jobQueueId);
		if (jobQueue.getJobQueueStatus().equals(JobQueueStatus.RUNNING))
			return true;
		else
			return false;
	}
	
	public boolean wasFinished(String jobQueueId) {
		
		JobQueue jobQueue = managedJobQueues.get(jobQueueId);
		if (jobQueue.getJobQueueStatus().equals(JobQueueStatus.FINISHED))
			return true;
		else
			return false;
	}
	
	public Map<String, JobQueue> getManagedJobQueues() {
		return managedJobQueues;
	}

	public JobRunner getJobRunner() {
		return jobRunner;
	}


	public void setManagedJobQueues(Map<String, JobQueue> managedJobQueues) {
		this.managedJobQueues = managedJobQueues;
	}

	public void setJobRunner(JobRunner jobRunner) {
		this.jobRunner = jobRunner;
	}

	
	
	
}
