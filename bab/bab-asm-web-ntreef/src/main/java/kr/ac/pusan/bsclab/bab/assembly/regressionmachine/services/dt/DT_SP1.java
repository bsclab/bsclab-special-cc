package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SP1 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("PP11010"))
 if(t <= 3.17)
  if(we <= 3652)
   if(we <= 3420)
    if(t <= 2.1) return 13;
    if(t > 2.1) return 17.5;
   if(we > 3420) return 20;
  if(we > 3652)
   if(w <= 338)
    if(w <= 266) return 14;
    if(w > 266) return 15;
   if(w > 338) return 13;
 if(t > 3.17)
  if(w <= 355)
   if(w <= 318)
    if(w <= 280) return 15;
    if(w > 280) return 17;
   if(w > 318)
    if(we <= 5223) return 15;
    if(we > 5223)
     if(we <= 5305) return 10;
     if(we > 5305) return 15;
  if(w > 355)
   if(t <= 3.24)
    if(we <= 6555) return 15;
    if(we > 6555) return 17;
   if(t > 3.24) return 13;
if(p.equals("PP11012")) return 30;
if(p.equals("PP11013")) return 16.5;
if(p.equals("PP11021"))
 if(we <= 3946)
  if(w <= 237) return 30;
  if(w > 237)
   if(t <= 3.225) return 10;
   if(t > 3.225) return 20;
 if(we > 3946)
  if(w <= 350)
   if(t <= 3.3) return 20;
   if(t > 3.3) return 15;
  if(w > 350)
   if(w <= 406)
    if(we <= 5297) return 29;
    if(we > 5297) return 16;
   if(w > 406) return 19;
if(p.equals("PP11036"))
 if(t <= 1.42)
  if(we <= 3468) return 22;
  if(we > 3468) return 19;
 if(t > 1.42)
  if(t <= 1.6) return 32;
  if(t > 1.6) return 16;
if(p.equals("PP11037"))
 if(t <= 1.09)
  if(t <= 1) return 41.3333333333333;
  if(t > 1)
   if(w <= 262) return 16;
   if(w > 262)
    if(w <= 319) return 12.5;
    if(w > 319) return 10;
 if(t > 1.09)
  if(we <= 3250)
   if(w <= 210)
    if(we <= 3105) return 13;
    if(we > 3105) return 14;
   if(w > 210)
    if(w <= 237) return 17;
    if(w > 237) return 15;
  if(we > 3250)
   if(t <= 1.2)
    if(w <= 232)
     if(w <= 222) return 22;
     if(w > 222) return 19;
    if(w > 232) return 22.5;
   if(t > 1.2)
    if(w <= 303) return 34;
    if(w > 303) return 26;
if(p.equals("PP11039")) return 21;
if(p.equals("PP11052")) return 55;
if(p.equals("PP11891"))
 if(t <= 3.3)
  if(w <= 303) return 14;
  if(w > 303)
   if(w <= 350) return 40;
   if(w > 350) return 16;
 if(t > 3.3) return 15;
if(p.equals("PP11892")) return 17;
if(p.equals("PP12011")) return 10.5;
if(p.equals("PP12040")) return 16;
if(p.equals("PP12152")) return 13.3333333333333;
if(p.equals("PP12226")) return 17.6666666666667;
if(p.equals("PP12524")) return 15.5;
if(p.equals("PP21429")) return 21.5;
if(p.equals("PP21601"))
 if(t <= 3.6) return 11;
 if(t > 3.6) return 10;
if(p.equals("PP21647")) return 17;
if(p.equals("PP21789")) return 28;
if(p.equals("PP22107")) return 25;
if(p.equals("PP22480")) return 40;
return 40.0;
}
}
