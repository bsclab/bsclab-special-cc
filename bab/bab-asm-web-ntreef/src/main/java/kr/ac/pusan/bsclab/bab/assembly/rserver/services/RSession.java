package kr.ac.pusan.bsclab.bab.assembly.rserver.services;

import kr.ac.pusan.bsclab.bab.assembly.rserver.conf.RConfiguration;
import java.util.UUID;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for handling R session
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */

public class RSession {

	/**
	 *  logging information to trace RStudio service process
	 */
	private static final Logger log = LoggerFactory.getLogger(RSession.class);
	
	
	/**
	 * @param requester
	 * @param libraryName
	 * @throws REXPMismatchException
	 * @throws REngineException
	 * @throws RSException
	 * 
	 * loading library in RStudio Server
	 */
	public static void loadLibrary(RConnection requester, String libraryName)
			throws REXPMismatchException, REngineException, RSException {

		String lib = "";
		// dplyr library resulting output massage, therefore it needs to suppress the massages
		if (!libraryName.equals("dplyr"))
			lib = "library(" + libraryName + ")";
		else
			lib = "suppressMessages(library(" + libraryName + "))"; 

		REXP r = requester.parseAndEval("try(" + lib + ", silent=TRUE)");

		// catch information if there is error while loading library
		if (r.inherits("try-error")) {
			throw new RSException("Error, there is no " + libraryName + " library");
		}
	}

	/**
	 * @param requester
	 * @param src
	 * @param scriptName
	 * @throws REXPMismatchException
	 * @throws REngineException
	 * @throws RSException
	 * 
	 * method for loading R-Script
	 */
	public static void loadRScript(RConnection requester, RConfiguration src, String scriptName)
			throws REXPMismatchException, REngineException, RSException {

		log.info("try(source(\"" + src.getScriptPath() +"/"+ scriptName + "\"), silent=TRUE)");
		REXP r = requester.parseAndEval("try(source(\"" + src.getScriptPath() +"/"+ scriptName + "\"), silent=TRUE)");
		
		// catch information if there is error while loading R-Script
		if (r.inherits("try-error")) {
			throw new RSException("Error in Rscript -> " + scriptName);
		}

	}

	/**
	 * @param requester
	 * @param src
	 * @param exeCore
	 * @param exeMemory
	 * @throws REXPMismatchException
	 * @throws REngineException
	 * @throws RSException
	 * 
	 * method for setting configuration variable in RStudio server in order to connect to Spark, Hadoop and Yarn.
	 */
	public static void sparkRConf(RConnection requester, RConfiguration src, int exeCore, String exeMemory)
			throws REXPMismatchException, REngineException, RSException {

		loadLibrary(requester, "sparklyr");
		requester.eval("Sys.setenv(SPARK_HOME = \"" + src.getSparkHome() + "\")");
		requester.eval("Sys.setenv(HADOOP_CONF_DIR=\"" + src.getHadoopDir() + "\")");
		requester.eval("Sys.setenv(YARN_CONF_DIR=\"" + src.getYarnDir() + "\")");
		requester.eval("config <- spark_config()");
		requester.eval("config$spark.executor.cores <- " + exeCore + "");
		requester.eval("config$spark.executor.memory <-\"" + exeMemory + "\"");
		requester.eval("config$spark.yarn.am.cores <- " + exeCore + "");
		requester.eval("config$spark.yarn.am.memory <-\"" + exeMemory + "\"");

		REXP r = requester.parseAndEval("try(sc <- spark_connect(master=\"yarn-client\", config=config, version=\'"
				+ src.getSparkVersion() + "\'), silent=TRUE)");
		
		// catch information if there is error while connecting to spark
		if (r.inherits("try-error")) {
			System.out.println("Error = " +r.asString());
			throw new RSException("Error in connecting to Spark");
		}
	}

	/**
	 * @param requester
	 * @param src
	 * @param docName
	 * @throws REngineException
	 * @throws REXPMismatchException
	 * @throws RSException
	 * 
	 * Read CSV file in HDFS via Spark
	 * 
	 */
	public static void sparklyrReadCSV(RConnection requester, RConfiguration src, String docName)
			throws REngineException, REXPMismatchException, RSException {

		String tblName = docName.substring(0, docName.length() - 4);
		String pathName = src.getHdfsUrl() + "/test/datatestcsv/" + docName;
		log.info("try(data <- spark_read_csv(sc, name=\"" + tblName + "\", path=\"" + pathName + "\", header=TRUE), silent=TRUE)");
		REXP rj = requester.parseAndEval(
				"try(data <- spark_read_csv(sc, name=\"" + tblName + "\", path=\"" + pathName + "\", header=TRUE), silent=TRUE)");
		
		// catch information if there is error while reading CSV file
		if (rj.inherits("try-error")) {
			throw new RSException("Error in reading CSV from Spark -> " + docName);
		}

	}
	
	/**
	 * @param requester
	 * @param src
	 * @param docName
	 * @throws REngineException
	 * @throws REXPMismatchException
	 * @throws RSException
	 * 
	 * Read CSV file in HDFS via Spark
	 * 
	 */
	public static void sparklyrReadJSON(RConnection requester, RConfiguration src, String resourceFile)
			throws REngineException, REXPMismatchException, RSException {

		String jsonName = resourceFile.substring(resourceFile.lastIndexOf("/") + 1);
		String pathName = src.getHdfsUrl() + resourceFile;
		
		log.info("try(data <- spark_read_json(sc, name=\"" + jsonName.substring(0,jsonName.lastIndexOf(".")) + "\", path=\"" + pathName + "\", options = list(), repartition = 0, memory = TRUE, overwrite=TRUE), silent=TRUE)");
		REXP rj = requester.parseAndEval(
				"try(data <- spark_read_json(sc, name=\"" + jsonName.substring(0,jsonName.lastIndexOf(".")) + "\", path=\"" + pathName + "\", options = list(), repartition = 0, memory = TRUE, overwrite=TRUE), silent=TRUE)");
		
		// catch information if there is error while reading JSON file
		if (rj.inherits("try-error")) {
			throw new RSException("Error in reading JSON File from Spark -> " + jsonName);
		}

	}
	
	/**
	 * @param requester
	 * @param src
	 * @param id
	 * @return
	 * @throws REngineException
	 * @throws REXPMismatchException
	 * @throws RSException
	 * 
	 * method to plot graph in R-Studio Server
	 */
	public static String plotGraph(RConnection requester, RConfiguration src, String id)
			throws REngineException, REXPMismatchException, RSException {
		UUID uuid = UUID.randomUUID();

		String fileName = id.concat("-"+uuid+".svg");
		log.info("svg(filename=\'" + src.getOutputPath()+"/"+fileName + "\', width=7, height=6)");
		requester.eval("svg(filename=\'" + src.getOutputPath()+"/"+fileName + "\', width=7, height=6)");
		return src.getOutputPath()+"/"+fileName;
	}

	/**
	 * @param xp
	 * @param requester
	 * @param src
	 * @param id
	 * @return
	 * @throws REngineException
	 * @throws REXPMismatchException
	 * @throws RSException
	 * 
	 * method for importing Graph
	 */
	public static String importGraph(REXP xp, RConnection requester, RConfiguration src, String id)
			throws REngineException, REXPMismatchException, RSException {
		UUID uuid = UUID.randomUUID();

		String fileName = id.concat("-"+uuid+".svg");
		log.info("svg(filename=\'" + src.getOutputPath()+"/"+fileName + "\', width=7, height=6)");
		requester.eval("svg(filename=\'" + src.getOutputPath()+"/"+fileName + "\', width=7, height=6)");
		return src.getOutputPath()+"/"+fileName;
	}
}
