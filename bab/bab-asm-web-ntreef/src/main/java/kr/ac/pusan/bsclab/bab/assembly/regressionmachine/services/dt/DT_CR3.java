package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_CR3 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10245"))
 if(t <= 5.12)
  if(t <= 4.7)
   if(t <= 4.6) return 35;
   if(t > 4.6)
    if(we <= 7568) return 40;
    if(we > 7568) return 36;
  if(t > 4.7)
   if(t <= 5.02)
    if(we <= 9057)
     if(t <= 4.77) return 32;
     if(t > 4.77) return 39;
    if(we > 9057) return 36;
   if(t > 5.02)
    if(w <= 419) return 25;
    if(w > 419) return 37.5;
 if(t > 5.12)
  if(t <= 5.3)
   if(w <= 421)
    if(w <= 419) return 30;
    if(w > 419)
     if(we <= 8155)
      if(we <= 7927) return 31.5;
      if(we > 7927) return 30.6666666666667;
     if(we > 8155)
      if(we <= 8329) return 27.4666666666667;
      if(we > 8329) return 27.8166666666667;
   if(w > 421)
    if(w <= 422) return 35.1;
    if(w > 422) return 26.9833333333333;
  if(t > 5.3)
   if(t <= 5.5)
    if(t <= 5.36) return 32;
    if(t > 5.36) return 37.9166666666667;
   if(t > 5.5)
    if(w <= 445) return 30;
    if(w > 445)
     if(w <= 449) return 21.0166666666667;
     if(w > 449) return 33;
if(p.equals("AN10252"))
 if(t <= 4.35) return 25;
 if(t > 4.35) return 29;
if(p.equals("AN10258"))
 if(t <= 4.6) return 30;
 if(t > 4.6) return 33;
if(p.equals("PP11988")) return 49;
if(p.equals("PP12095"))
 if(we <= 5080)
  if(w <= 331)
   if(w <= 330.06)
    if(t <= 0.795)
     if(we <= 4910)
      if(we <= 4695) return 39.15;
      if(we > 4695) return 60;
     if(we > 4910)
      if(w <= 327)
       if(we <= 4965) return 39;
       if(we > 4965) return 57.8166666666667;
      if(w > 327) return 35.2;
    if(t > 0.795)
     if(we <= 4761)
      if(we <= 4396)
       if(we <= 4024) return 51.6666666666667;
       if(we > 4024) return 35;
      if(we > 4396)
       if(we <= 4561) return 42;
       if(we > 4561) return 48.5333333333333;
     if(we > 4761)
      if(w <= 314)
       if(we <= 4799)
        if(we <= 4772) return 51;
        if(we > 4772) return 36.9833333333333;
       if(we > 4799) return 42;
      if(w > 314)
       if(w <= 317)
        if(we <= 4809) return 39;
        if(we > 4809) return 41.8666666666667;
       if(w > 317)
        if(w <= 323) return 54;
        if(w > 323) return 51;
   if(w > 330.06)
    if(we <= 4727)
     if(we <= 4587)
      if(we <= 4396)
       if(we <= 4080) return 35;
       if(we > 4080) return 43;
      if(we > 4396)
       if(we <= 4500) return 57;
       if(we > 4500) return 42;
     if(we > 4587)
      if(we <= 4654)
       if(we <= 4613) return 47;
       if(we > 4613) return 60;
      if(we > 4654)
       if(we <= 4675) return 49.9166666666667;
       if(we > 4675) return 45.3833333333333;
    if(we > 4727)
     if(we <= 4976)
      if(w <= 330.25)
       if(we <= 4886)
        if(w <= 330.17) return 60;
        if(w > 330.17) return 43;
       if(we > 4886) return 40;
      if(w > 330.25)
       if(we <= 4825)
        if(we <= 4765) return 45.3;
        if(we > 4765) return 54.4833333333333;
       if(we > 4825) return 50;
     if(we > 4976)
      if(we <= 5056)
       if(we <= 5025)
        if(we <= 5000) return 41.45;
        if(we > 5000) return 43;
       if(we > 5025) return 46;
      if(we > 5056)
       if(we <= 5071) return 51;
       if(we > 5071) return 43;
  if(w > 331)
   if(w <= 333)
    if(we <= 4772)
     if(w <= 332)
      if(we <= 4516)
       if(we <= 4340)
        if(we <= 4115) return 44;
        if(we > 4115) return 43.8333333333333;
       if(we > 4340)
        if(we <= 4437) return 41.9666666666667;
        if(we > 4437) return 43;
      if(we > 4516) return 55;
     if(w > 332)
      if(w <= 332.17)
       if(we <= 4727) return 48.1333333333333;
       if(we > 4727) return 45;
      if(w > 332.17) return 56.5166666666667;
    if(we > 4772)
     if(we <= 4983)
      if(we <= 4897)
       if(we <= 4825) return 45;
       if(we > 4825)
        if(we <= 4847) return 45.3166666666667;
        if(we > 4847) return 41.3833333333333;
      if(we > 4897)
       if(we <= 4946) return 39;
       if(we > 4946)
        if(we <= 4965) return 23.9166666666667;
        if(we > 4965) return 41;
     if(we > 4983)
      if(we <= 5038)
       if(we <= 5019)
        if(we <= 4994) return 38.05;
        if(we > 4994) return 44.5;
       if(we > 5019)
        if(we <= 5033) return 49;
        if(we > 5033) return 49.55;
      if(we > 5038)
       if(we <= 5062)
        if(we <= 5059) return 21.1833333333333;
        if(we > 5059) return 45;
       if(we > 5062)
        if(we <= 5069) return 46.55;
        if(we > 5069) return 39.7;
   if(w > 333)
    if(we <= 4783)
     if(we <= 4736) return 42;
     if(we > 4736)
      if(w <= 354) return 41;
      if(w > 354) return 15;
    if(we > 4783)
     if(we <= 4960)
      if(w <= 345.1)
       if(we <= 4897)
        if(we <= 4864) return 58;
        if(we > 4864) return 50;
       if(we > 4897)
        if(we <= 4948) return 50.7166666666667;
        if(we > 4948) return 60;
      if(w > 345.1)
       if(w <= 347)
        if(we <= 4910) return 44;
        if(we > 4910) return 60;
       if(w > 347) return 51.2666666666667;
     if(we > 4960)
      if(w <= 345.1)
       if(w <= 335) return 41.0666666666667;
       if(w > 335) return 43;
      if(w > 345.1)
       if(w <= 350) return 56.5;
       if(w > 350) return 57.2;
 if(we > 5080)
  if(w <= 355)
   if(w <= 345)
    if(w <= 332.17)
     if(w <= 331)
      if(t <= 0.795) return 50.85;
      if(t > 0.795)
       if(we <= 5117)
        if(we <= 5086) return 45.5;
        if(we > 5086) return 51;
       if(we > 5117)
        if(we <= 5139) return 46.5;
        if(we > 5139) return 47.5;
     if(w > 331)
      if(we <= 5143)
       if(we <= 5117) return 52;
       if(we > 5117) return 53;
      if(we > 5143)
       if(we <= 5165) return 51.5;
       if(we > 5165) return 48.5166666666667;
    if(w > 332.17)
     if(we <= 5263)
      if(w <= 344.26)
       if(w <= 334) return 39.8666666666667;
       if(w > 334)
        if(w <= 344.13) return 45;
        if(w > 344.13) return 83;
      if(w > 344.26)
       if(we <= 5240) return 43.1166666666667;
       if(we > 5240) return 43;
     if(we > 5263)
      if(we <= 5298)
       if(we <= 5278) return 45.1166666666667;
       if(we > 5278) return 46.5;
      if(we > 5298)
       if(we <= 5317) return 41.3166666666667;
       if(we > 5317)
        if(we <= 5375) return 40;
        if(we > 5375) return 41;
   if(w > 345)
    if(we <= 5185)
     if(w <= 346)
      if(we <= 5113) return 40;
      if(we > 5113)
       if(we <= 5139) return 45.5;
       if(we > 5139) return 53.5;
     if(w > 346)
      if(w <= 351) return 66.05;
      if(w > 351) return 25;
    if(we > 5185)
     if(we <= 5311) return 50;
     if(we > 5311)
      if(we <= 5368)
       if(we <= 5362) return 49;
       if(we > 5362) return 51.5833333333333;
      if(we > 5368)
       if(we <= 5377) return 55.2166666666667;
       if(we > 5377) return 46.25;
  if(w > 355)
   if(w <= 374.28)
    if(we <= 5490)
     if(we <= 5331)
      if(we <= 5294)
       if(we <= 5235) return 29.9666666666667;
       if(we > 5235)
        if(we <= 5286) return 39;
        if(we > 5286) return 55;
      if(we > 5294) return 47;
     if(we > 5331)
      if(w <= 373)
       if(we <= 5462) return 60;
       if(we > 5462) return 36;
      if(w > 373) return 43;
    if(we > 5490)
     if(we <= 5704)
      if(w <= 372)
       if(we <= 5557)
        if(we <= 5520) return 46.7666666666667;
        if(we > 5520) return 44;
       if(we > 5557)
        if(we <= 5628) return 43;
        if(we > 5628) return 44;
      if(w > 372) return 42;
     if(we > 5704)
      if(w <= 372)
       if(we <= 5772)
        if(we <= 5765) return 45.3833333333333;
        if(we > 5765) return 50;
       if(we > 5772)
        if(we <= 5781) return 57.5;
        if(we > 5781) return 52.2166666666667;
      if(w > 372)
       if(we <= 5749) return 30;
       if(we > 5749) return 60;
   if(w > 374.28)
    if(we <= 5662)
     if(we <= 5493) return 60;
     if(we > 5493)
      if(w <= 390)
       if(w <= 382) return 38.1;
       if(w > 382) return 47.5;
      if(w > 390)
       if(w <= 397) return 51.45;
       if(w > 397) return 31;
    if(we > 5662)
     if(t <= 0.795)
      if(w <= 501) return 45.25;
      if(w > 501)
       if(we <= 7309) return 44.3333333333333;
       if(we > 7309) return 58.2166666666667;
     if(t > 0.795)
      if(w <= 487)
       if(we <= 5881)
        if(we <= 5746)
         if(w <= 397)
          if(we <= 5685) return 44;
          if(we > 5685) return 41;
         if(w > 397) return 53;
        if(we > 5746)
         if(w <= 386) return 40;
         if(w > 386) return 57.5166666666667;
       if(we > 5881)
        if(w <= 438)
         if(w <= 390) return 43;
         if(w > 390)
          if(we <= 6043) return 42;
          if(we > 6043) return 53;
        if(w > 438)
         if(we <= 6824) return 43;
         if(we > 6824) return 44.7166666666667;
      if(w > 487)
       if(w <= 494.17)
        if(we <= 7001) return 42;
        if(we > 7001)
         if(we <= 7053) return 36.3;
         if(we > 7053) return 50.25;
       if(w > 494.17)
        if(we <= 7193) return 50;
        if(we > 7193)
         if(we <= 7257) return 55;
         if(we > 7257) return 41;
if(p.equals("PP12141"))
 if(t <= 0.91) return 48.8;
 if(t > 0.91) return 36;
if(p.equals("PP12146"))
 if(w <= 610)
  if(we <= 10289)
   if(t <= 0.76) return 61.7166666666667;
   if(t > 0.76) return 55;
  if(we > 10289)
   if(t <= 1.295) return 60;
   if(t > 1.295) return 49;
 if(w > 610)
  if(we <= 12059) return 56;
  if(we > 12059)
   if(t <= 1.36)
    if(w <= 612) return 57.5;
    if(w > 612) return 48.5;
   if(t > 1.36)
    if(we <= 12266) return 30.65;
    if(we > 12266) return 21.6;
if(p.equals("PP12231"))
 if(w <= 354)
  if(we <= 6580)
   if(we <= 6251) return 35;
   if(we > 6251) return 24;
  if(we > 6580) return 43;
 if(w > 354)
  if(t <= 1.33) return 40.35;
  if(t > 1.33)
   if(t <= 2.02) return 33;
   if(t > 2.02) return 52;
if(p.equals("PP12233")) return 56.3166666666667;
if(p.equals("PP12445"))
 if(w <= 332.17)
  if(w <= 322.5)
   if(we <= 4464) return 51;
   if(we > 4464) return 47;
  if(w > 322.5)
   if(w <= 331) return 52;
   if(w > 331)
    if(we <= 4710) return 43;
    if(we > 4710) return 47;
 if(w > 332.17)
  if(w <= 350) return 55;
  if(w > 350)
   if(we <= 5051) return 46;
   if(we > 5051) return 50;
if(p.equals("PP12472")) return 49.6166666666667;
if(p.equals("PP12483"))
 if(we <= 8249) return 38.5;
 if(we > 8249) return 35;
if(p.equals("PP12485")) return 50;
if(p.equals("PP12536"))
 if(t <= 1.65) return 35;
 if(t > 1.65) return 36;
if(p.equals("PP12537")) return 67;
if(p.equals("PP12563"))
 if(t <= 2.33)
  if(t <= 2.25)
   if(w <= 430) return 28.6666666666667;
   if(w > 430)
    if(w <= 547) return 25;
    if(w > 547) return 40;
  if(t > 2.25)
   if(we <= 8919)
    if(we <= 7354) return 22;
    if(we > 7354) return 35;
   if(we > 8919)
    if(we <= 9299) return 26;
    if(we > 9299) return 28;
 if(t > 2.33)
  if(we <= 8919)
   if(t <= 2.42) return 23;
   if(t > 2.42)
    if(t <= 2.53) return 12;
    if(t > 2.53) return 27;
  if(we > 8919)
   if(we <= 13646) return 50;
   if(we > 13646) return 43;
if(p.equals("PP21317")) return 42.5;
if(p.equals("PP21320"))
 if(w <= 448)
  if(w <= 436)
   if(w <= 384)
    if(t <= 2.45)
     if(t <= 2.32)
      if(we <= 6951)
       if(w <= 364) return 45;
       if(w > 364)
        if(w <= 375) return 34;
        if(w > 375) return 26.9333333333333;
      if(we > 6951)
       if(t <= 2.13) return 22.5;
       if(t > 2.13)
        if(we <= 7661) return 33;
        if(we > 7661) return 26.2;
     if(t > 2.32) return 50;
    if(t > 2.45)
     if(t <= 3.79)
      if(we <= 7051)
       if(w <= 353) return 38.2833333333333;
       if(w > 353) return 34;
      if(we > 7051) return 28.5;
     if(t > 3.79)
      if(w <= 353)
       if(we <= 7051) return 20;
       if(we > 7051) return 21.5;
      if(w > 353)
       if(we <= 7293) return 40;
       if(we > 7293) return 27;
   if(w > 384)
    if(t <= 2.37)
     if(w <= 417)
      if(t <= 2.3)
       if(w <= 386.16) return 24.6666666666667;
       if(w > 386.16) return 26;
      if(t > 2.3)
       if(w <= 386.16) return 24;
       if(w > 386.16) return 26.5666666666667;
     if(w > 417)
      if(we <= 8554)
       if(we <= 8276) return 25.1;
       if(we > 8276) return 23.4333333333333;
      if(we > 8554)
       if(we <= 8761) return 25.9833333333333;
       if(we > 8761) return 20;
    if(t > 2.37)
     if(t <= 2.44)
      if(we <= 8607)
       if(we <= 8494) return 31.8166666666667;
       if(we > 8494) return 29.0833333333333;
      if(we > 8607)
       if(we <= 8848) return 29.1833333333333;
       if(we > 8848) return 26;
     if(t > 2.44)
      if(t <= 2.55)
       if(we <= 7001) return 20;
       if(we > 7001) return 24;
      if(t > 2.55)
       if(t <= 2.63)
        if(we <= 7001) return 23.5;
        if(we > 7001) return 34.8;
       if(t > 2.63) return 23;
  if(w > 436)
   if(t <= 2.385)
    if(we <= 8796)
     if(we <= 8480)
      if(we <= 5286)
       if(we <= 4454) return 15.5;
       if(we > 4454) return 26;
      if(we > 5286) return 47;
     if(we > 8480)
      if(we <= 8580) return 25.7666666666667;
      if(we > 8580) return 58;
    if(we > 8796)
     if(we <= 9224) return 20;
     if(we > 9224)
      if(we <= 9320) return 26;
      if(we > 9320) return 26.2666666666667;
   if(t > 2.385)
    if(t <= 2.44)
     if(we <= 8890)
      if(we <= 8068)
       if(we <= 4454) return 20;
       if(we > 4454) return 22.5;
      if(we > 8068) return 39;
     if(we > 8890)
      if(we <= 9213)
       if(we <= 9026) return 34.65;
       if(we > 9026) return 37;
      if(we > 9213)
       if(we <= 9283) return 35;
       if(we > 9283)
        if(we <= 9433) return 31;
        if(we > 9433) return 40;
    if(t > 2.44)
     if(t <= 2.55) return 26.6666666666667;
     if(t > 2.55) return 27.4666666666667;
 if(w > 448)
  if(we <= 11375)
   if(we <= 9031)
    if(we <= 8524)
     if(t <= 2.385)
      if(w <= 590)
       if(t <= 2.13)
        if(t <= 1.92) return 34;
        if(t > 1.92)
         if(we <= 4541) return 19;
         if(we > 4541) return 33;
       if(t > 2.13)
        if(we <= 4547)
         if(we <= 4516) return 26;
         if(we > 4516) return 25;
        if(we > 4547)
         if(we <= 8402) return 32;
         if(we > 8402) return 32.5;
      if(w > 590)
       if(t <= 2.33)
        if(we <= 6534) return 16.5;
        if(we > 6534) return 22;
       if(t > 2.33) return 33;
     if(t > 2.385)
      if(we <= 6442) return 30;
      if(we > 6442)
       if(t <= 3.08)
        if(t <= 2.44) return 30;
        if(t > 2.44)
         if(w <= 648) return 25;
         if(w > 648) return 32.5;
       if(t > 3.08)
        if(t <= 3.64) return 31;
        if(t > 3.64) return 50;
    if(we > 8524)
     if(t <= 2.13)
      if(we <= 8802) return 28;
      if(we > 8802)
       if(we <= 8904) return 25;
       if(we > 8904) return 18;
     if(t > 2.13)
      if(we <= 8904)
       if(w <= 478)
        if(t <= 2.33)
         if(we <= 8687) return 30;
         if(we > 8687) return 27;
        if(t > 2.33) return 36;
       if(w > 478) return 36;
      if(we > 8904) return 29;
   if(we > 9031)
    if(t <= 2.25)
     if(we <= 9218)
      if(w <= 478)
       if(we <= 9062) return 26;
       if(we > 9062) return 42;
      if(w > 478)
       if(t <= 2.13) return 26.2333333333333;
       if(t > 2.13) return 33.6;
     if(we > 9218)
      if(t <= 2.13)
       if(w <= 481)
        if(we <= 9463)
         if(we <= 9391) return 25.1666666666667;
         if(we > 9391) return 25.3166666666667;
        if(we > 9463)
         if(we <= 9609) return 26.0833333333333;
         if(we > 9609) return 33.9166666666667;
       if(w > 481) return 33.1;
      if(t > 2.13)
       if(t <= 2.195)
        if(w <= 481)
         if(we <= 9408) return 49;
         if(we > 9408) return 40;
        if(w > 481) return 27.5;
       if(t > 2.195)
        if(w <= 525) return 51.8666666666667;
        if(w > 525) return 35;
    if(t > 2.25)
     if(t <= 2.93)
      if(t <= 2.498)
       if(w <= 474)
        if(w <= 456) return 26.9;
        if(w > 456) return 28;
       if(w > 474)
        if(w <= 514) return 44;
        if(w > 514) return 26.5;
      if(t > 2.498)
       if(w <= 502)
        if(t <= 2.55)
         if(w <= 497) return 27;
         if(w > 497) return 29;
        if(t > 2.55)
         if(w <= 466)
          if(w <= 456) return 38;
          if(w > 456) return 29;
         if(w > 466) return 32;
       if(w > 502)
        if(t <= 2.6)
         if(w <= 524) return 43;
         if(w > 524) return 36.5;
        if(t > 2.6) return 28;
     if(t > 2.93)
      if(t <= 3.34)
       if(t <= 3.08)
        if(w <= 545) return 25;
        if(w > 545)
         if(w <= 551) return 23.5;
         if(w > 551) return 14;
       if(t > 3.08)
        if(w <= 545)
         if(we <= 11114) return 35;
         if(we > 11114) return 32;
        if(w > 545)
         if(w <= 551) return 42.4166666666667;
         if(w > 551) return 29;
      if(t > 3.34)
       if(t <= 3.59)
        if(w <= 526)
         if(w <= 523) return 9;
         if(w > 523) return 15;
        if(w > 526)
         if(w <= 529) return 17.0333333333333;
         if(w > 529) return 22;
       if(t > 3.59)
        if(t <= 3.96)
         if(w <= 536)
          if(w <= 524) return 30;
          if(w > 524) return 35;
         if(w > 536) return 32.1666666666667;
        if(t > 3.96)
         if(w <= 526)
          if(w <= 523) return 33;
          if(w > 523) return 25;
         if(w > 526)
          if(w <= 529) return 33.5;
          if(w > 529) return 22;
  if(we > 11375)
   if(w <= 619)
    if(t <= 2.78)
     if(w <= 578)
      if(t <= 2.4) return 30;
      if(t > 2.4)
       if(t <= 2.6)
        if(we <= 11815) return 49;
        if(we > 11815) return 38;
       if(t > 2.6)
        if(w <= 575) return 45.5;
        if(w > 575) return 32.5;
     if(w > 578)
      if(t <= 2.195)
       if(t <= 2.13)
        if(w <= 589) return 47.3666666666667;
        if(w > 589) return 26.25;
       if(t > 2.13) return 39.9666666666667;
      if(t > 2.195)
       if(w <= 615) return 36.3333333333333;
       if(w > 615) return 27;
    if(t > 2.78)
     if(t <= 3.4) return 50;
     if(t > 3.4)
      if(t <= 3.74)
       if(we <= 11858) return 34.05;
       if(we > 11858) return 36;
      if(t > 3.74)
       if(we <= 11858) return 30;
       if(we > 11858) return 29.5;
   if(w > 619)
    if(t <= 2.35)
     if(we <= 13150)
      if(we <= 12684)
       if(we <= 12251) return 30;
       if(we > 12251)
        if(we <= 12603) return 27;
        if(we > 12603) return 22;
      if(we > 12684)
       if(we <= 12903) return 40.05;
       if(we > 12903)
        if(we <= 13052) return 30;
        if(we > 13052) return 40;
     if(we > 13150)
      if(we <= 13520)
       if(we <= 13202) return 29;
       if(we > 13202)
        if(we <= 13371) return 32;
        if(we > 13371) return 52;
      if(we > 13520)
       if(we <= 13613) return 28;
       if(we > 13613)
        if(we <= 13826) return 28.2666666666667;
        if(we > 13826) return 30;
    if(t > 2.35)
     if(t <= 2.44)
      if(we <= 13543)
       if(we <= 13013)
        if(we <= 12251) return 30;
        if(we > 12251) return 40;
       if(we > 13013)
        if(we <= 13191)
         if(we <= 13128) return 48.6666666666667;
         if(we > 13128) return 30;
        if(we > 13191)
         if(we <= 13303) return 34;
         if(we > 13303)
          if(we <= 13458) return 35;
          if(we > 13458) return 30;
      if(we > 13543)
       if(we <= 13681) return 31;
       if(we > 13681) return 37.4666666666667;
     if(t > 2.44)
      if(w <= 640)
       if(t <= 3.6)
        if(w <= 628) return 35;
        if(w > 628) return 24.5;
       if(t > 3.6) return 40;
      if(w > 640)
       if(t <= 2.55)
        if(we <= 13150) return 45;
        if(we > 13150) return 39;
       if(t > 2.55)
        if(we <= 13150) return 42.8166666666667;
        if(we > 13150) return 32;
if(p.equals("PP21326")) return 59;
if(p.equals("PP21328"))
 if(t <= 4.92)
  if(t <= 2.4)
   if(w <= 561)
    if(w <= 538) return 55;
    if(w > 538)
     if(we <= 11356) return 48;
     if(we > 11356) return 51;
   if(w > 561) return 44;
  if(t > 2.4)
   if(we <= 12349)
    if(t <= 2.7)
     if(we <= 12263)
      if(we <= 12129) return 26.5;
      if(we > 12129) return 27.4166666666667;
     if(we > 12263)
      if(we <= 12316) return 31;
      if(we > 12316) return 30;
    if(t > 2.7)
     if(t <= 4.04)
      if(w <= 456) return 20;
      if(w > 456) return 40;
     if(t > 4.04) return 26;
   if(we > 12349)
    if(w <= 597)
     if(we <= 12421) return 27.5;
     if(we > 12421)
      if(we <= 12489) return 27.1833333333333;
      if(we > 12489) return 26;
    if(w > 597) return 24.8833333333333;
 if(t > 4.92)
  if(t <= 5.56)
   if(w <= 613)
    if(t <= 4.97)
     if(w <= 539) return 23;
     if(w > 539) return 17;
    if(t > 4.97)
     if(t <= 5.24)
      if(w <= 557)
       if(we <= 10528) return 24;
       if(we > 10528) return 20;
      if(w > 557)
       if(w <= 572) return 22;
       if(w > 572)
        if(w <= 573) return 20;
        if(w > 573) return 15.2333333333333;
     if(t > 5.24)
      if(w <= 415) return 20;
      if(w > 415) return 23;
   if(w > 613)
    if(we <= 11573)
     if(w <= 625)
      if(t <= 4.97) return 32;
      if(t > 4.97) return 11;
     if(w > 625)
      if(we <= 11016)
       if(we <= 10886) return 22.5;
       if(we > 10886) return 21.5;
      if(we > 11016)
       if(we <= 11041) return 36;
       if(we > 11041) return 15;
    if(we > 11573)
     if(we <= 12146)
      if(we <= 11955)
       if(we <= 11810) return 40;
       if(we > 11810) return 20.8166666666667;
      if(we > 11955) return 18;
     if(we > 12146)
      if(t <= 4.97) return 30;
      if(t > 4.97) return 25;
  if(t > 5.56)
   if(t <= 5.76)
    if(we <= 7881)
     if(we <= 7730)
      if(we <= 7401) return 26;
      if(we > 7401) return 26.6666666666667;
     if(we > 7730)
      if(we <= 7753) return 21.0666666666667;
      if(we > 7753) return 26.3333333333333;
    if(we > 7881)
     if(we <= 7976)
      if(we <= 7899) return 24.75;
      if(we > 7899) return 16.6666666666667;
     if(we > 7976)
      if(we <= 8088) return 26.6666666666667;
      if(we > 8088) return 22.4333333333333;
   if(t > 5.76)
    if(we <= 8047)
     if(we <= 7518) return 23.9166666666667;
     if(we > 7518) return 17;
    if(we > 8047)
     if(we <= 8256) return 23.3333333333333;
     if(we > 8256) return 20.2;
if(p.equals("PP21329")) return 76;
if(p.equals("PP21330"))
 if(w <= 370)
  if(w <= 337) return 37.2;
  if(w > 337)
   if(t <= 1.15) return 39;
   if(t > 1.15) return 30;
 if(w > 370)
  if(we <= 6243) return 27;
  if(we > 6243) return 33.5;
if(p.equals("PP21331"))
 if(t <= 1.675)
  if(w <= 632) return 40;
  if(w > 632) return 33.6333333333333;
 if(t > 1.675)
  if(t <= 1.79) return 42;
  if(t > 1.79)
   if(w <= 632) return 34.0333333333333;
   if(w > 632) return 39;
if(p.equals("PP21334"))
 if(w <= 539)
  if(t <= 2.15) return 46;
  if(t > 2.15) return 35;
 if(w > 539)
  if(w <= 559) return 37;
  if(w > 559)
   if(t <= 1.53) return 36;
   if(t > 1.53) return 55;
if(p.equals("PP21335")) return 22;
if(p.equals("PP21336"))
 if(t <= 2.51)
  if(t <= 2.295)
   if(we <= 11910)
    if(we <= 11163)
     if(t <= 2.195)
      if(w <= 553)
       if(t <= 2.02) return 31.5;
       if(t > 2.02)
        if(we <= 10995) return 35.4833333333333;
        if(we > 10995) return 43.5;
      if(w > 553) return 50;
     if(t > 2.195)
      if(w <= 513)
       if(w <= 383)
        if(we <= 7718) return 32;
        if(we > 7718) return 50;
       if(w > 383) return 39;
      if(w > 513)
       if(we <= 10982) return 53;
       if(we > 10982)
        if(we <= 11031) return 59;
        if(we > 11031) return 37.5;
    if(we > 11163)
     if(w <= 551) return 39.5;
     if(w > 551)
      if(w <= 554) return 51;
      if(w > 554) return 34;
   if(we > 11910)
    if(we <= 12070)
     if(w <= 551)
      if(t <= 2.23)
       if(we <= 11995) return 45;
       if(we > 11995) return 39.5;
      if(t > 2.23)
       if(we <= 11995) return 41;
       if(we > 11995) return 38;
     if(w > 551)
      if(t <= 2.23)
       if(w <= 553)
        if(we <= 12011) return 48.05;
        if(we > 12011) return 38;
       if(w > 553) return 41;
      if(t > 2.23)
       if(w <= 553) return 36.3333333333333;
       if(w > 553) return 47;
    if(we > 12070)
     if(we <= 12110)
      if(t <= 2.23) return 42;
      if(t > 2.23)
       if(we <= 12090) return 44;
       if(we > 12090) return 47;
     if(we > 12110)
      if(we <= 12265)
       if(we <= 12167) return 50;
       if(we > 12167)
        if(t <= 2.23) return 43;
        if(t > 2.23) return 46;
      if(we > 12265) return 43.1;
  if(t > 2.295)
   if(w <= 613)
    if(w <= 550)
     if(t <= 2.45)
      if(w <= 490)
       if(we <= 7625) return 40.35;
       if(we > 7625) return 50;
      if(w > 490)
       if(we <= 10989) return 43;
       if(we > 10989) return 38.5;
     if(t > 2.45)
      if(w <= 392)
       if(w <= 376) return 56;
       if(w > 376) return 31;
      if(w > 392)
       if(w <= 464) return 51;
       if(w > 464) return 38;
    if(w > 550)
     if(w <= 555) return 42;
     if(w > 555)
      if(w <= 581) return 40;
      if(w > 581) return 36;
   if(w > 613)
    if(w <= 622)
     if(t <= 2.495) return 45;
     if(t > 2.495)
      if(we <= 12358) return 33.5;
      if(we > 12358)
       if(we <= 12370) return 35.5;
       if(we > 12370) return 29;
    if(w > 622)
     if(we <= 12331) return 32;
     if(we > 12331)
      if(we <= 12423) return 31;
      if(we > 12423) return 46.5;
 if(t > 2.51)
  if(t <= 4.01)
   if(t <= 3.01)
    if(t <= 2.895)
     if(t <= 2.74)
      if(w <= 553)
       if(t <= 2.57) return 60;
       if(t > 2.57) return 65;
      if(w > 553)
       if(w <= 612) return 35.0833333333333;
       if(w > 612) return 39;
     if(t > 2.74)
      if(t <= 2.795) return 40;
      if(t > 2.795) return 43.1833333333333;
    if(t > 2.895)
     if(w <= 486)
      if(t <= 2.995)
       if(t <= 2.94) return 53.7166666666667;
       if(t > 2.94) return 42.4333333333333;
      if(t > 2.995) return 47.4333333333333;
     if(w > 486)
      if(we <= 10335)
       if(w <= 615) return 36.2166666666667;
       if(w > 615) return 37.5;
      if(we > 10335)
       if(we <= 11038) return 55;
       if(we > 11038) return 42.85;
   if(t > 3.01)
    if(w <= 487)
     if(t <= 3.74)
      if(t <= 3.29) return 36.15;
      if(t > 3.29)
       if(w <= 483) return 42.5666666666667;
       if(w > 483) return 32.6666666666667;
     if(t > 3.74)
      if(t <= 3.9) return 30;
      if(t > 3.9)
       if(w <= 392) return 30.5333333333333;
       if(w > 392) return 27.8666666666667;
    if(w > 487)
     if(t <= 3.91)
      if(w <= 553)
       if(t <= 3.44) return 59;
       if(t > 3.44) return 34;
      if(w > 553)
       if(w <= 581) return 49;
       if(w > 581) return 29.1833333333333;
     if(t > 3.91)
      if(t <= 3.96) return 34.5;
      if(t > 3.96)
       if(w <= 545) return 35;
       if(w > 545) return 30.9;
  if(t > 4.01)
   if(t <= 4.445)
    if(t <= 4.02)
     if(w <= 364)
      if(we <= 7893) return 24.7333333333333;
      if(we > 7893) return 39.5;
     if(w > 364)
      if(w <= 366)
       if(we <= 7657) return 29.5;
       if(we > 7657) return 33.3333333333333;
      if(w > 366) return 31;
    if(t > 4.02)
     if(we <= 10504)
      if(w <= 379) return 22;
      if(w > 379) return 33;
     if(we > 10504)
      if(we <= 11364) return 30;
      if(we > 11364) return 32.65;
   if(t > 4.445)
    if(t <= 6.6)
     if(w <= 474)
      if(t <= 4.7)
       if(t <= 4.52) return 29;
       if(t > 4.52) return 33.5;
      if(t > 4.7)
       if(t <= 4.97) return 28.9666666666667;
       if(t > 4.97) return 27.5666666666667;
     if(w > 474)
      if(we <= 11769) return 28.5;
      if(we > 11769)
       if(w <= 622) return 30;
       if(w > 622) return 36;
    if(t > 6.6)
     if(w <= 599)
      if(we <= 11990) return 14.2333333333333;
      if(we > 11990)
       if(w <= 588) return 17.9;
       if(w > 588) return 17;
     if(w > 599)
      if(w <= 612) return 15;
      if(w > 612) return 27;
if(p.equals("PP21338")) return 45;
if(p.equals("PP21339")) return 25;
if(p.equals("PP21340")) return 43;
if(p.equals("PP21341"))
 if(t <= 2.51)
  if(w <= 551)
   if(w <= 550) return 30;
   if(w > 550) return 33.5;
  if(w > 551)
   if(we <= 11006) return 35;
   if(we > 11006) return 41;
 if(t > 2.51)
  if(w <= 368) return 43;
  if(w > 368) return 35;
if(p.equals("PP21344")) return 28.35;
if(p.equals("PP21345")) return 30.6833333333333;
if(p.equals("PP21346"))
 if(t <= 4.77)
  if(t <= 2.63)
   if(w <= 546)
    if(t <= 2.51)
     if(t <= 2.41) return 33.5;
     if(t > 2.41) return 31;
    if(t > 2.51)
     if(w <= 478)
      if(we <= 9062) return 40;
      if(we > 9062) return 37;
     if(w > 478)
      if(w <= 482) return 35.8666666666667;
      if(w > 482) return 50;
   if(w > 546)
    if(w <= 573)
     if(w <= 569)
      if(we <= 11611) return 41;
      if(we > 11611) return 40;
     if(w > 569) return 60;
    if(w > 573)
     if(w <= 583)
      if(w <= 582)
       if(w <= 577) return 38;
       if(w > 577) return 33;
      if(w > 582) return 41;
     if(w > 583)
      if(we <= 11405) return 37;
      if(we > 11405) return 40;
  if(t > 2.63)
   if(w <= 472)
    if(t <= 3.34)
     if(t <= 2.79) return 32;
     if(t > 2.79) return 31.2666666666667;
    if(t > 3.34)
     if(t <= 4.54) return 29.2;
     if(t > 4.54) return 31.5;
   if(w > 472)
    if(w <= 561)
     if(we <= 10861)
      if(t <= 3.4)
       if(w <= 518) return 39;
       if(w > 518)
        if(we <= 10673) return 55;
        if(we > 10673) return 28;
      if(t > 3.4) return 30;
     if(we > 10861)
      if(we <= 11041) return 31;
      if(we > 11041)
       if(t <= 3.88) return 40.25;
       if(t > 3.88) return 29;
    if(w > 561)
     if(t <= 4.53)
      if(we <= 12030) return 42;
      if(we > 12030) return 37;
     if(t > 4.53)
      if(we <= 11242) return 27;
      if(we > 11242) return 33;
 if(t > 4.77)
  if(w <= 590)
   if(t <= 5.02)
    if(w <= 512)
     if(w <= 405) return 25.35;
     if(w > 405) return 22;
    if(w > 512)
     if(w <= 555)
      if(w <= 525) return 25;
      if(w > 525) return 34;
     if(w > 555)
      if(we <= 11118) return 27;
      if(we > 11118) return 26;
   if(t > 5.02)
    if(t <= 6.04)
     if(we <= 9752) return 34;
     if(we > 9752)
      if(t <= 5.3)
       if(we <= 10852) return 35;
       if(we > 10852)
        if(w <= 522) return 36;
        if(w > 522) return 40;
      if(t > 5.3) return 23;
    if(t > 6.04) return 27;
  if(w > 590)
   if(w <= 598)
    if(t <= 5.56)
     if(we <= 12157)
      if(we <= 12051) return 33.05;
      if(we > 12051) return 31.55;
     if(we > 12157)
      if(we <= 12206) return 26;
      if(we > 12206) return 22;
    if(t > 5.56)
     if(we <= 11521) return 27;
     if(we > 11521) return 29;
   if(w > 598)
    if(t <= 5.56)
     if(w <= 613) return 35;
     if(w > 613)
      if(t <= 5.03) return 31.1666666666667;
      if(t > 5.03) return 35.7333333333333;
    if(t > 5.56)
     if(w <= 614)
      if(t <= 6.07) return 40;
      if(t > 6.07)
       if(we <= 12133) return 28;
       if(we > 12133) return 29.9833333333333;
     if(w > 614)
      if(w <= 617)
       if(we <= 12202) return 31.2666666666667;
       if(we > 12202) return 19;
      if(w > 617) return 40;
if(p.equals("PP21347")) return 30.45;
if(p.equals("PP21348")) return 29.5;
if(p.equals("PP21349")) return 42.5;
if(p.equals("PP21350")) return 29.3833333333333;
if(p.equals("PP21364"))
 if(t <= 2.8)
  if(we <= 11458) return 32;
  if(we > 11458) return 35;
 if(t > 2.8) return 34.3;
if(p.equals("PP21368")) return 16;
if(p.equals("PP21389"))
 if(w <= 519)
  if(t <= 3.79)
   if(we <= 10059)
    if(w <= 490) return 46.0333333333333;
    if(w > 490)
     if(we <= 9959) return 39.8666666666667;
     if(we > 9959) return 33.4;
   if(we > 10059)
    if(we <= 10174)
     if(we <= 10114) return 36.8666666666667;
     if(we > 10114) return 37.5833333333333;
    if(we > 10174)
     if(we <= 10248) return 43.6666666666667;
     if(we > 10248) return 37.45;
  if(t > 3.79)
   if(w <= 485)
    if(w <= 428)
     if(w <= 379)
      if(we <= 6778) return 22.6166666666667;
      if(we > 6778) return 35;
     if(w > 379)
      if(we <= 8314) return 22;
      if(we > 8314) return 24.6;
    if(w > 428)
     if(we <= 9402) return 35.15;
     if(we > 9402) return 24.6166666666667;
   if(w > 485)
    if(we <= 9633)
     if(we <= 9402)
      if(we <= 9276) return 26;
      if(we > 9276) return 31;
     if(we > 9402)
      if(we <= 9515) return 27;
      if(we > 9515)
       if(we <= 9598) return 25;
       if(we > 9598) return 29.3666666666667;
    if(we > 9633)
     if(w <= 492)
      if(we <= 9681)
       if(we <= 9652) return 31.0833333333333;
       if(we > 9652) return 24.5;
      if(we > 9681)
       if(we <= 10006) return 27;
       if(we > 10006) return 55.2833333333333;
     if(w > 492)
      if(t <= 4.6) return 26;
      if(t > 4.6) return 35;
 if(w > 519)
  if(we <= 11314)
   if(w <= 549)
    if(t <= 3.79)
     if(w <= 531)
      if(we <= 10612) return 52.2666666666667;
      if(we > 10612) return 30.5;
     if(w > 531) return 40;
    if(t > 3.79)
     if(w <= 529) return 30;
     if(w > 529)
      if(we <= 9959) return 29.5;
      if(we > 9959)
       if(we <= 10184) return 26.65;
       if(we > 10184) return 29.0333333333333;
   if(w > 549)
    if(t <= 4.1)
     if(w <= 560)
      if(we <= 10154) return 27.5;
      if(we > 10154) return 28.4666666666667;
     if(w > 560)
      if(w <= 592) return 33;
      if(w > 592) return 28;
    if(t > 4.1)
     if(t <= 4.54) return 38;
     if(t > 4.54)
      if(we <= 10931) return 29.85;
      if(we > 10931) return 23.05;
  if(we > 11314)
   if(we <= 12456)
    if(t <= 4.65)
     if(we <= 11616)
      if(w <= 557) return 29;
      if(w > 557) return 36;
     if(we > 11616) return 30;
    if(t > 4.65) return 26;
   if(we > 12456)
    if(we <= 12747)
     if(we <= 12593) return 33;
     if(we > 12593)
      if(we <= 12705) return 32.5166666666667;
      if(we > 12705) return 47;
    if(we > 12747)
     if(we <= 13234) return 40;
     if(we > 13234) return 33.25;
if(p.equals("PP21393"))
 if(t <= 3.69)
  if(t <= 2.98)
   if(t <= 2.51)
    if(w <= 511) return 30;
    if(w > 511)
     if(w <= 583) return 23;
     if(w > 583) return 27.5;
   if(t > 2.51)
    if(t <= 2.69)
     if(w <= 495)
      if(w <= 482) return 33.9333333333333;
      if(w > 482) return 45;
     if(w > 495)
      if(w <= 525) return 25;
      if(w > 525)
       if(w <= 562) return 47;
       if(w > 562) return 35;
    if(t > 2.69)
     if(t <= 2.795)
      if(t <= 2.75)
       if(w <= 461) return 42;
       if(w > 461) return 40;
      if(t > 2.75)
       if(we <= 11788) return 34;
       if(we > 11788) return 36;
     if(t > 2.795)
      if(t <= 2.87) return 29;
      if(t > 2.87) return 38;
  if(t > 2.98)
   if(t <= 3.05)
    if(we <= 10046)
     if(w <= 416)
      if(w <= 408) return 30;
      if(w > 408) return 35;
     if(w > 416)
      if(we <= 7822) return 33;
      if(we > 7822)
       if(we <= 9293) return 36;
       if(we > 9293) return 40;
    if(we > 10046)
     if(w <= 571)
      if(w <= 528) return 37.8666666666667;
      if(w > 528) return 32.5;
     if(w > 571)
      if(we <= 11975) return 30;
      if(we > 11975)
       if(w <= 611) return 31;
       if(w > 611) return 50;
   if(t > 3.05)
    if(we <= 10154)
     if(we <= 7001)
      if(w <= 444) return 21;
      if(w > 444)
       if(w <= 566) return 28.8833333333333;
       if(w > 566) return 22.0666666666667;
     if(we > 7001)
      if(w <= 518) return 35;
      if(w > 518) return 40;
    if(we > 10154)
     if(t <= 3.48)
      if(t <= 3.47)
       if(we <= 11038) return 42;
       if(we > 11038) return 36.5;
      if(t > 3.47) return 29.3166666666667;
     if(t > 3.48)
      if(t <= 3.5)
       if(w <= 614) return 29;
       if(w > 614) return 31.4666666666667;
      if(t > 3.5) return 30.15;
 if(t > 3.69)
  if(t <= 4.92)
   if(w <= 511)
    if(t <= 4.51)
     if(w <= 496)
      if(t <= 4.01)
       if(t <= 3.9) return 32;
       if(t > 3.9)
        if(w <= 377) return 20;
        if(w > 377) return 31;
      if(t > 4.01)
       if(t <= 4.03)
        if(w <= 410) return 40;
        if(w > 410) return 24;
       if(t > 4.03)
        if(w <= 339) return 33.5833333333333;
        if(w > 339) return 31;
     if(w > 496)
      if(t <= 4.25)
       if(w <= 498) return 29;
       if(w > 498)
        if(w <= 500) return 34;
        if(w > 500) return 22.3833333333333;
      if(t > 4.25) return 40;
    if(t > 4.51)
     if(w <= 478) return 27;
     if(w > 478)
      if(w <= 496) return 21.5;
      if(w > 496) return 33;
   if(w > 511)
    if(we <= 10725)
     if(w <= 573)
      if(we <= 9520) return 25;
      if(we > 9520)
       if(w <= 529)
        if(w <= 519) return 39;
        if(w > 519) return 25.5;
       if(w > 529)
        if(w <= 565) return 30;
        if(w > 565) return 27;
     if(w > 573)
      if(t <= 4.01)
       if(t <= 3.96) return 32;
       if(t > 3.96) return 15;
      if(t > 4.01)
       if(t <= 4.03) return 53;
       if(t > 4.03)
        if(t <= 4.25) return 25;
        if(t > 4.25) return 29;
    if(we > 10725)
     if(t <= 4.01)
      if(t <= 3.96) return 27;
      if(t > 3.96)
       if(w <= 590) return 29;
       if(w > 590) return 19;
     if(t > 4.01)
      if(t <= 4.51)
       if(we <= 11384) return 28;
       if(we > 11384)
        if(t <= 4.03) return 33;
        if(t > 4.03) return 26;
      if(t > 4.51) return 39;
  if(t > 4.92)
   if(we <= 11001)
    if(we <= 10588)
     if(w <= 442)
      if(t <= 5.03) return 25;
      if(t > 5.03) return 28;
     if(w > 442) return 45;
    if(we > 10588)
     if(t <= 5.02) return 25;
     if(t > 5.02)
      if(w <= 581)
       if(w <= 579) return 34;
       if(w > 579) return 19;
      if(w > 581)
       if(w <= 582) return 37;
       if(w > 582) return 31;
   if(we > 11001)
    if(w <= 587)
     if(t <= 5.02)
      if(t <= 5.01)
       if(w <= 555) return 30;
       if(w > 555) return 36.5;
      if(t > 5.01) return 34.2;
     if(t > 5.02)
      if(w <= 580)
       if(we <= 11247) return 20;
       if(we > 11247) return 48;
      if(w > 580)
       if(we <= 11987) return 27;
       if(we > 11987) return 26.5;
    if(w > 587)
     if(t <= 5.03)
      if(t <= 5.02) return 29;
      if(t > 5.02) return 24;
     if(t > 5.03)
      if(t <= 5.5) return 29.5;
      if(t > 5.5)
       if(we <= 12416) return 28;
       if(we > 12416) return 29;
if(p.equals("PP21394"))
 if(t <= 2.04)
  if(we <= 11080)
   if(w <= 515)
    if(we <= 6540)
     if(we <= 6182)
      if(we <= 6010)
       if(we <= 4601) return 32;
       if(we > 4601) return 29.5833333333333;
      if(we > 6010)
       if(we <= 6083) return 25;
       if(we > 6083) return 26.5;
     if(we > 6182)
      if(w <= 342)
       if(we <= 6479) return 23;
       if(we > 6479) return 26;
      if(w > 342) return 40.5;
    if(we > 6540)
     if(w <= 357)
      if(we <= 6831) return 45;
      if(we > 6831)
       if(we <= 6893) return 29;
       if(we > 6893) return 31;
     if(w > 357)
      if(t <= 1.95)
       if(we <= 10800) return 28;
       if(we > 10800) return 45;
      if(t > 1.95)
       if(w <= 368) return 27.5833333333333;
       if(w > 368) return 22;
   if(w > 515)
    if(w <= 536)
     if(w <= 525)
      if(w <= 522)
       if(t <= 1.8) return 42.7833333333333;
       if(t > 1.8) return 34;
      if(w > 522)
       if(we <= 9884) return 34;
       if(we > 9884) return 40;
     if(w > 525)
      if(w <= 534)
       if(t <= 1.75) return 42;
       if(t > 1.75)
        if(w <= 532) return 49;
        if(w > 532)
         if(we <= 10588) return 32;
         if(we > 10588) return 29;
      if(w > 534)
       if(we <= 10577)
        if(we <= 10522) return 36;
        if(we > 10522) return 40;
       if(we > 10577)
        if(we <= 10629) return 48.5;
        if(we > 10629) return 35.5;
    if(w > 536)
     if(we <= 10796)
      if(w <= 583)
       if(w <= 561) return 21.3166666666667;
       if(w > 561) return 36.5;
      if(w > 583)
       if(t <= 1.25) return 47;
       if(t > 1.25) return 45;
     if(we > 10796)
      if(we <= 10959)
       if(t <= 1.85) return 25;
       if(t > 1.85) return 38;
      if(we > 10959)
       if(w <= 581) return 46;
       if(w > 581) return 28;
  if(we > 11080)
   if(we <= 12063)
    if(we <= 11692)
     if(w <= 570)
      if(t <= 1.65) return 40;
      if(t > 1.65) return 52;
     if(w > 570) return 53;
    if(we > 11692)
     if(t <= 1.78)
      if(w <= 576) return 39.8166666666667;
      if(w > 576)
       if(w <= 588) return 45.5666666666667;
       if(w > 588) return 35;
     if(t > 1.78)
      if(we <= 11879)
       if(we <= 11709) return 37.3;
       if(we > 11709)
        if(we <= 11781) return 31.8166666666667;
        if(we > 11781) return 30;
      if(we > 11879)
       if(t <= 1.9)
        if(w <= 591) return 26;
        if(w > 591) return 26.15;
       if(t > 1.9) return 31.45;
   if(we > 12063)
    if(we <= 12375)
     if(w <= 598)
      if(t <= 1.7)
       if(w <= 588) return 36;
       if(w > 588) return 36.5;
      if(t > 1.7)
       if(t <= 1.95)
        if(w <= 588)
         if(we <= 12170)
          if(we <= 12121) return 40.5;
          if(we > 12121) return 29.25;
         if(we > 12170)
          if(we <= 12217) return 38;
          if(we > 12217) return 32.5;
        if(w > 588) return 40.5;
       if(t > 1.95) return 29.25;
     if(w > 598)
      if(t <= 1.7)
       if(we <= 12286) return 55;
       if(we > 12286) return 50;
      if(t > 1.7)
       if(w <= 602)
        if(t <= 1.9) return 29.5;
        if(t > 1.9) return 40;
       if(w > 602) return 36;
    if(we > 12375)
     if(w <= 609)
      if(t <= 1.9) return 29.0166666666667;
      if(t > 1.9) return 31.6666666666667;
     if(w > 609)
      if(w <= 619)
       if(we <= 13371) return 43;
       if(we > 13371) return 51;
      if(w > 619)
       if(w <= 623) return 71;
       if(w > 623) return 51;
 if(t > 2.04)
  if(t <= 2.15)
   if(we <= 10921)
    if(we <= 6893)
     if(we <= 6498)
      if(we <= 6083) return 50;
      if(we > 6083)
       if(we <= 6479) return 55;
       if(we > 6479) return 45.4166666666667;
     if(we > 6498)
      if(we <= 6661)
       if(w <= 342)
        if(we <= 6569) return 29;
        if(we > 6569) return 36.5166666666667;
       if(w > 342) return 42;
      if(we > 6661)
       if(we <= 6765) return 28;
       if(we > 6765)
        if(we <= 6831) return 32;
        if(we > 6831) return 60;
    if(we > 6893)
     if(w <= 534)
      if(t <= 2.1)
       if(w <= 368)
        if(w <= 352) return 36;
        if(w > 352) return 37.1833333333333;
       if(w > 368)
        if(we <= 9501) return 29.5333333333333;
        if(we > 9501) return 38;
      if(t > 2.1)
       if(we <= 10603) return 31.5;
       if(we > 10603) return 39;
     if(w > 534)
      if(w <= 538)
       if(we <= 10577)
        if(we <= 10522) return 37;
        if(we > 10522) return 35;
       if(we > 10577)
        if(we <= 10629) return 33;
        if(we > 10629) return 45;
      if(w > 538)
       if(t <= 2.13) return 38;
       if(t > 2.13) return 54;
   if(we > 10921)
    if(t <= 2.11)
     if(w <= 581)
      if(we <= 12146)
       if(we <= 11887) return 58;
       if(we > 11887) return 28;
      if(we > 12146)
       if(we <= 12198) return 45.35;
       if(we > 12198) return 37.5;
     if(w > 581)
      if(we <= 11692) return 36.5;
      if(we > 11692)
       if(we <= 11709) return 43.35;
       if(we > 11709) return 35.7;
    if(t > 2.11)
     if(t <= 2.12)
      if(we <= 11740)
       if(we <= 11170) return 40.5;
       if(we > 11170) return 44;
      if(we > 11740)
       if(we <= 11890) return 45.4166666666667;
       if(we > 11890) return 49.2666666666667;
     if(t > 2.12)
      if(t <= 2.13)
       if(w <= 599) return 38.5;
       if(w > 599) return 38.1833333333333;
      if(t > 2.13)
       if(we <= 11677) return 45.6166666666667;
       if(we > 11677) return 39;
  if(t > 2.15)
   if(t <= 2.44)
    if(t <= 2.385)
     if(t <= 2.35)
      if(t <= 2.25) return 42.35;
      if(t > 2.25) return 38;
     if(t > 2.35)
      if(w <= 599)
       if(w <= 598) return 34;
       if(w > 598)
        if(we <= 12295) return 36.5;
        if(we > 12295) return 34;
      if(w > 599) return 31.0333333333333;
    if(t > 2.385)
     if(w <= 524) return 33.5;
     if(w > 524)
      if(we <= 11322) return 30;
      if(we > 11322) return 23;
   if(t > 2.44)
    if(we <= 12106)
     if(t <= 2.63)
      if(we <= 11077) return 35;
      if(we > 11077) return 38;
     if(t > 2.63)
      if(t <= 2.78)
       if(we <= 10180) return 35;
       if(we > 10180) return 33;
      if(t > 2.78)
       if(t <= 2.84) return 29.7166666666667;
       if(t > 2.84) return 36.5;
    if(we > 12106)
     if(w <= 619)
      if(w <= 614)
       if(we <= 12286) return 25;
       if(we > 12286) return 34.5;
      if(w > 614)
       if(we <= 13371) return 33;
       if(we > 13371) return 26.3;
     if(w > 619)
      if(we <= 13271) return 31;
      if(we > 13271) return 30;
if(p.equals("PP21398"))
 if(t <= 1.995)
  if(w <= 484)
   if(w <= 358)
    if(w <= 335) return 41;
    if(w > 335)
     if(t <= 1.08)
      if(we <= 7832) return 10;
      if(we > 7832) return 62.3333333333333;
     if(t > 1.08) return 51.6666666666667;
   if(w > 358)
    if(t <= 1.08)
     if(we <= 7752)
      if(we <= 7600) return 64.3333333333333;
      if(we > 7600) return 59;
     if(we > 7752)
      if(we <= 7876) return 53.3333333333333;
      if(we > 7876) return 50;
    if(t > 1.08)
     if(t <= 1.68) return 53;
     if(t > 1.68)
      if(we <= 8310) return 35.5;
      if(we > 8310) return 40;
  if(w > 484)
   if(t <= 1.55)
    if(w <= 572)
     if(t <= 1.495)
      if(w <= 518) return 43;
      if(w > 518)
       if(t <= 1.27)
        if(we <= 6772) return 35;
        if(we > 6772) return 46;
       if(t > 1.27) return 43;
     if(t > 1.495)
      if(w <= 535) return 33.1833333333333;
      if(w > 535) return 39.5;
    if(w > 572)
     if(t <= 1.36) return 37;
     if(t > 1.36)
      if(t <= 1.495) return 60;
      if(t > 1.495) return 38.75;
   if(t > 1.55)
    if(t <= 1.63) return 55;
    if(t > 1.63)
     if(w <= 576)
      if(w <= 550) return 43.7333333333333;
      if(w > 550) return 45;
     if(w > 576)
      if(w <= 599) return 45.5;
      if(w > 599)
       if(we <= 12005) return 35;
       if(we > 12005) return 53;
 if(t > 1.995)
  if(w <= 591)
   if(we <= 11966)
    if(w <= 511)
     if(t <= 2.04) return 15.05;
     if(t > 2.04)
      if(we <= 8021)
       if(w <= 382) return 27.7333333333333;
       if(w > 382) return 40;
      if(we > 8021)
       if(we <= 8300) return 27;
       if(we > 8300) return 47.7166666666667;
    if(w > 511)
     if(w <= 585)
      if(w <= 517) return 32.9833333333333;
      if(w > 517)
       if(w <= 552) return 25.1666666666667;
       if(w > 552) return 34.9833333333333;
     if(w > 585)
      if(we <= 11484)
       if(we <= 11351) return 35.5;
       if(we > 11351) return 33;
      if(we > 11484) return 36;
   if(we > 11966)
    if(we <= 12430)
     if(w <= 586)
      if(we <= 12206)
       if(we <= 12167) return 41.8666666666667;
       if(we > 12167) return 46.8333333333333;
      if(we > 12206)
       if(we <= 12393) return 41.7333333333333;
       if(we > 12393) return 47;
     if(w > 586) return 37;
    if(we > 12430) return 42.5;
  if(w > 591)
   if(w <= 637)
    if(w <= 614)
     if(we <= 11139)
      if(we <= 10881) return 30;
      if(we > 10881) return 30.5;
     if(we > 11139)
      if(we <= 11803) return 39.5;
      if(we > 11803) return 28.5;
    if(w > 614)
     if(we <= 12058)
      if(we <= 11974)
       if(we <= 11269) return 42.5;
       if(we > 11269) return 40;
      if(we > 11974)
       if(we <= 12038) return 34.3333333333333;
       if(we > 12038) return 41.3166666666667;
     if(we > 12058)
      if(we <= 12172)
       if(we <= 12085) return 28.4166666666667;
       if(we > 12085) return 31.1833333333333;
      if(we > 12172)
       if(we <= 12286) return 34.1333333333333;
       if(we > 12286) return 35;
   if(w > 637)
    if(we <= 12082)
     if(t <= 2.04) return 52;
     if(t > 2.04)
      if(we <= 12029)
       if(we <= 11461) return 43.5166666666667;
       if(we > 11461) return 42.5166666666667;
      if(we > 12029)
       if(we <= 12065) return 37.4833333333333;
       if(we > 12065) return 36.85;
    if(we > 12082)
     if(we <= 12156)
      if(we <= 12116) return 40;
      if(we > 12116) return 36.7;
     if(we > 12156)
      if(we <= 12218) return 47.6333333333333;
      if(we > 12218) return 49.4;
if(p.equals("PP21423"))
 if(t <= 0.93)
  if(we <= 7327)
   if(w <= 430)
    if(we <= 5504)
     if(w <= 358) return 31.5;
     if(w > 358)
      if(w <= 389) return 34;
      if(w > 389) return 37.5;
    if(we > 5504)
     if(w <= 420) return 38;
     if(w > 420)
      if(we <= 5919) return 34;
      if(we > 5919) return 36;
   if(w > 430)
    if(w <= 442)
     if(we <= 6526) return 41;
     if(we > 6526) return 35;
    if(w > 442)
     if(w <= 466) return 31;
     if(w > 466) return 36;
  if(we > 7327)
   if(t <= 0.71)
    if(w <= 543) return 63;
    if(w > 543)
     if(we <= 8601) return 35;
     if(we > 8601) return 40.05;
   if(t > 0.71)
    if(t <= 0.85)
     if(w <= 613)
      if(we <= 7424) return 37;
      if(we > 7424) return 55;
     if(w > 613)
      if(w <= 619) return 36.8;
      if(w > 619) return 27;
    if(t > 0.85) return 54;
 if(t > 0.93)
  if(t <= 1.31)
   if(we <= 6442)
    if(t <= 1.03)
     if(we <= 5616)
      if(w <= 340) return 31;
      if(w > 340) return 42;
     if(we > 5616)
      if(we <= 5762) return 27.5;
      if(we > 5762) return 31;
    if(t > 1.03)
     if(w <= 419)
      if(w <= 358) return 2;
      if(w > 358)
       if(w <= 389) return 33;
       if(w > 389) return 36;
     if(w > 419)
      if(we <= 6161)
       if(w <= 423)
        if(we <= 5919) return 41;
        if(we > 5919) return 33;
       if(w > 423) return 42.5;
      if(we > 6161)
       if(w <= 420) return 37;
       if(w > 420)
        if(we <= 6362) return 43;
        if(we > 6362) return 41;
   if(we > 6442)
    if(we <= 8687)
     if(t <= 1.03)
      if(t <= 0.96) return 60;
      if(t > 0.96)
       if(we <= 8095) return 64;
       if(we > 8095) return 33;
     if(t > 1.03)
      if(w <= 464) return 56;
      if(w > 464)
       if(t <= 1.11)
        if(w <= 495) return 62;
        if(w > 495) return 29.5;
       if(t > 1.11) return 40;
    if(we > 8687)
     if(w <= 608)
      if(w <= 583) return 51;
      if(w > 583) return 32;
     if(w > 608)
      if(w <= 617) return 30;
      if(w > 617)
       if(we <= 9548) return 50;
       if(we > 9548) return 55;
  if(t > 1.31)
   if(w <= 562)
    if(t <= 1.47)
     if(w <= 387) return 32;
     if(w > 387) return 41;
    if(t > 1.47) return 35;
   if(w > 562)
    if(t <= 1.33)
     if(we <= 9119) return 37;
     if(we > 9119) return 38;
    if(t > 1.33)
     if(w <= 596) return 32.9333333333333;
     if(w > 596)
      if(we <= 8161) return 33.1333333333333;
      if(we > 8161) return 20.8666666666667;
if(p.equals("PP21456"))
 if(t <= 1.22)
  if(we <= 8800)
   if(w <= 579)
    if(we <= 7822) return 45.15;
    if(we > 7822)
     if(t <= 0.85) return 51;
     if(t > 0.85) return 50;
   if(w > 579) return 80;
  if(we > 8800)
   if(w <= 635) return 41;
   if(w > 635)
    if(t <= 0.91) return 59;
    if(t > 0.91) return 51;
 if(t > 1.22)
  if(w <= 571)
   if(we <= 7490)
    if(we <= 6159)
     if(t <= 2.42) return 24;
     if(t > 2.42) return 27.5;
    if(we > 6159) return 45;
   if(we > 7490)
    if(t <= 1.57) return 29.25;
    if(t > 1.57) return 33.05;
  if(w > 571)
   if(w <= 613)
    if(t <= 1.31) return 34.45;
    if(t > 1.31) return 33.3833333333333;
   if(w > 613)
    if(w <= 645) return 33;
    if(w > 645) return 33.1333333333333;
if(p.equals("PP21475"))
 if(w <= 492)
  if(t <= 1.21) return 37;
  if(t > 1.21) return 28.0666666666667;
 if(w > 492)
  if(we <= 8613) return 28.5;
  if(we > 8613) return 27;
if(p.equals("PP21476"))
 if(w <= 549)
  if(t <= 4.54)
   if(we <= 8239)
    if(t <= 3.22)
     if(t <= 2.42) return 37;
     if(t > 2.42)
      if(w <= 329) return 29.6666666666667;
      if(w > 329) return 41.35;
    if(t > 3.22)
     if(we <= 8185)
      if(w <= 491)
       if(w <= 408) return 25.5;
       if(w > 408) return 25;
      if(w > 491)
       if(w <= 503)
        if(we <= 7500) return 10.55;
        if(we > 7500) return 23.8166666666667;
       if(w > 503) return 23;
     if(we > 8185)
      if(w <= 548) return 22.5;
      if(w > 548) return 31;
   if(we > 8239)
    if(w <= 513)
     if(t <= 3.21)
      if(w <= 495)
       if(w <= 442) return 30;
       if(w > 442) return 33.5;
      if(w > 495)
       if(we <= 9909) return 37;
       if(we > 9909)
        if(we <= 10081) return 29.15;
        if(we > 10081) return 26;
     if(t > 3.21)
      if(t <= 3.34)
       if(w <= 481) return 34.9166666666667;
       if(w > 481)
        if(we <= 10015) return 35.5;
        if(we > 10015) return 50;
      if(t > 3.34)
       if(t <= 4.03) return 42.5;
       if(t > 4.03) return 27;
    if(w > 513)
     if(w <= 523) return 35;
     if(w > 523)
      if(t <= 3.64)
       if(t <= 3.22) return 35.9333333333333;
       if(t > 3.22) return 35;
      if(t > 3.64)
       if(t <= 4.01) return 25;
       if(t > 4.01)
        if(w <= 541) return 32.6;
        if(w > 541) return 26;
  if(t > 4.54)
   if(w <= 518)
    if(we <= 9606)
     if(we <= 7608)
      if(we <= 7555.5) return 20;
      if(we > 7555.5) return 25;
     if(we > 7608) return 30;
    if(we > 9606)
     if(t <= 4.55)
      if(w <= 497) return 35;
      if(w > 497)
       if(we <= 10006)
        if(we <= 9895) return 21;
        if(we > 9895) return 23;
       if(we > 10006)
        if(we <= 10040) return 30;
        if(we > 10040) return 23.7666666666667;
     if(t > 4.55)
      if(w <= 514) return 23;
      if(w > 514) return 26;
   if(w > 518)
    if(we <= 10679)
     if(t <= 4.55)
      if(we <= 10154) return 32.7;
      if(we > 10154) return 27;
     if(t > 4.55)
      if(we <= 10610) return 25.5;
      if(we > 10610) return 30;
    if(we > 10679)
     if(w <= 531)
      if(w <= 528) return 25.25;
      if(w > 528)
       if(we <= 10706) return 27.5;
       if(we > 10706) return 25;
     if(w > 531)
      if(we <= 10824) return 28.5;
      if(we > 10824) return 23;
 if(w > 549)
  if(t <= 2.84)
   if(w <= 614)
    if(we <= 12221)
     if(w <= 611)
      if(t <= 2.165) return 44;
      if(t > 2.165)
       if(w <= 610) return 35.0833333333333;
       if(w > 610) return 40;
     if(w > 611)
      if(we <= 11754)
       if(we <= 11194) return 55;
       if(we > 11194) return 47.5;
      if(we > 11754)
       if(we <= 11933) return 37;
       if(we > 11933)
        if(we <= 12198) return 47;
        if(we > 12198) return 40;
    if(we > 12221)
     if(we <= 12239)
      if(we <= 12231) return 37.5;
      if(we > 12231) return 30.5;
     if(we > 12239)
      if(we <= 12251) return 35.5833333333333;
      if(we > 12251) return 36;
   if(w > 614)
    if(w <= 617)
     if(w <= 615) return 43.5;
     if(w > 615)
      if(we <= 12017) return 35.9666666666667;
      if(we > 12017) return 34.25;
    if(w > 617) return 50;
  if(t > 2.84)
   if(t <= 4.25)
    if(we <= 11087)
     if(w <= 591) return 34.5;
     if(w > 591)
      if(t <= 3.6) return 36;
      if(t > 3.6) return 31;
    if(we > 11087)
     if(w <= 579)
      if(w <= 570) return 31;
      if(w > 570)
       if(w <= 577) return 45;
       if(w > 577) return 39.5;
     if(w > 579)
      if(w <= 586)
       if(t <= 3.74) return 30.5;
       if(t > 3.74) return 42;
      if(w > 586)
       if(we <= 11926) return 31;
       if(we > 11926) return 35;
   if(t > 4.25)
    if(t <= 4.55) return 40;
    if(t > 4.55)
     if(we <= 11344)
      if(we <= 11027) return 30;
      if(we > 11027) return 24;
     if(we > 11344) return 40;
if(p.equals("PP21487")) return 45;
if(p.equals("PP21504"))
 if(w <= 486) return 59;
 if(w > 486) return 55;
if(p.equals("PP21516")) return 20;
if(p.equals("PP21526"))
 if(w <= 485)
  if(t <= 1.53)
   if(t <= 1.35)
    if(t <= 1.15)
     if(t <= 0.93) return 85;
     if(t > 0.93) return 50;
    if(t > 1.15) return 46;
   if(t > 1.35)
    if(t <= 1.42) return 30;
    if(t > 1.42) return 35;
  if(t > 1.53)
   if(t <= 1.73)
    if(t <= 1.58) return 30.85;
    if(t > 1.58)
     if(we <= 7467) return 43;
     if(we > 7467) return 53.3333333333333;
   if(t > 1.73)
    if(t <= 1.95) return 68;
    if(t > 1.95) return 46;
 if(w > 485)
  if(we <= 8626)
   if(t <= 1.15)
    if(w <= 565) return 34;
    if(w > 565) return 43;
   if(t > 1.15)
    if(w <= 537)
     if(we <= 7796) return 42;
     if(we > 7796) return 56;
    if(w > 537) return 38;
  if(we > 8626)
   if(t <= 1.27)
    if(t <= 1.22) return 58;
    if(t > 1.22) return 19.7;
   if(t > 1.27)
    if(we <= 10224) return 66;
    if(we > 10224) return 30;
if(p.equals("PP21531")) return 31.5;
if(p.equals("PP21533")) return 35;
if(p.equals("PP21557"))
 if(w <= 467)
  if(we <= 5707) return 26;
  if(we > 5707) return 23;
 if(w > 467)
  if(t <= 2.55) return 33;
  if(t > 2.55) return 38;
if(p.equals("PP21577"))
 if(t <= 1.36)
  if(w <= 450)
   if(t <= 1.25)
    if(t <= 1.11) return 34.6666666666667;
    if(t > 1.11) return 38;
   if(t > 1.25)
    if(w <= 439) return 30.5;
    if(w > 439) return 50.5;
  if(w > 450) return 37;
 if(t > 1.36)
  if(we <= 10706)
   if(we <= 7600)
    if(t <= 1.88)
     if(t <= 1.66)
      if(w <= 395) return 39;
      if(w > 395) return 41;
     if(t > 1.66) return 40;
    if(t > 1.88)
     if(t <= 1.97) return 32;
     if(t > 1.97)
      if(t <= 2.02)
       if(w <= 412) return 37.5833333333333;
       if(w > 412) return 23;
      if(t > 2.02) return 24;
   if(we > 7600)
    if(we <= 8667)
     if(w <= 589) return 45;
     if(w > 589)
      if(t <= 1.62) return 35;
      if(t > 1.62) return 29;
    if(we > 8667)
     if(we <= 8802) return 30.7;
     if(we > 8802)
      if(w <= 561) return 39;
      if(w > 561) return 33.5;
  if(we > 10706)
   if(t <= 1.95)
    if(w <= 567)
     if(we <= 11686) return 40;
     if(we > 11686)
      if(we <= 11969) return 40.5;
      if(we > 11969) return 35;
    if(w > 567)
     if(w <= 615)
      if(w <= 588) return 32.4666666666667;
      if(w > 588) return 35;
     if(w > 615) return 44;
   if(t > 1.95)
    if(t <= 1.995)
     if(we <= 12245)
      if(we <= 11926) return 43;
      if(we > 11926) return 40;
     if(we > 12245)
      if(we <= 12537) return 33.3166666666667;
      if(we > 12537) return 41.1166666666667;
    if(t > 1.995)
     if(t <= 2.01) return 35.5;
     if(t > 2.01) return 45;
if(p.equals("PP21580"))
 if(t <= 1.7)
  if(w <= 469) return 31.1333333333333;
  if(w > 469) return 45;
 if(t > 1.7)
  if(t <= 1.85) return 28;
  if(t > 1.85) return 54.4833333333333;
if(p.equals("PP21592"))
 if(w <= 472)
  if(t <= 1.21)
   if(t <= 0.85)
    if(t <= 0.71) return 39;
    if(t > 0.71) return 50;
   if(t > 0.85)
    if(w <= 435) return 40;
    if(w > 435) return 47;
  if(t > 1.21)
   if(t <= 1.4) return 39.2666666666667;
   if(t > 1.4) return 30;
 if(w > 472)
  if(t <= 0.81)
   if(w <= 501) return 38;
   if(w > 501)
    if(w <= 562) return 52;
    if(w > 562) return 34;
  if(t > 0.81) return 53;
if(p.equals("PP21606"))
 if(we <= 9125) return 28;
 if(we > 9125) return 29.9166666666667;
if(p.equals("PP21677")) return 37.5;
if(p.equals("PP21684")) return 37;
if(p.equals("PP21690")) return 42;
if(p.equals("PP21694"))
 if(w <= 557) return 45;
 if(w > 557)
  if(we <= 11069) return 38;
  if(we > 11069) return 36;
if(p.equals("PP21726")) return 25;
if(p.equals("PP21752")) return 32;
if(p.equals("PP21753")) return 26.2833333333333;
if(p.equals("PP21754"))
 if(t <= 2.04)
  if(w <= 603) return 60;
  if(w > 603)
   if(w <= 612)
    if(we <= 11840) return 59.0666666666667;
    if(we > 11840) return 56.5;
   if(w > 612) return 62;
 if(t > 2.04)
  if(w <= 585)
   if(we <= 11950) return 38.5;
   if(we > 11950) return 33.5;
  if(w > 585) return 38;
if(p.equals("PP21762"))
 if(w <= 566) return 53.8833333333333;
 if(w > 566) return 36.35;
if(p.equals("PP21792")) return 44.1333333333333;
if(p.equals("PP21794"))
 if(t <= 1.8)
  if(we <= 5616) return 42.2166666666667;
  if(we > 5616) return 37.9666666666667;
 if(t > 1.8)
  if(we <= 5616) return 35.3166666666667;
  if(we > 5616) return 36.35;
if(p.equals("PP21798")) return 55;
if(p.equals("PP21802"))
 if(t <= 1)
  if(w <= 388)
   if(w <= 367) return 60.7833333333333;
   if(w > 367) return 45.5;
  if(w > 388)
   if(w <= 508) return 52;
   if(w > 508) return 40;
 if(t > 1)
  if(w <= 374) return 65;
  if(w > 374)
   if(w <= 410) return 57.5166666666667;
   if(w > 410) return 46.8333333333333;
if(p.equals("PP21804")) return 32;
if(p.equals("PP21805"))
 if(t <= 1.8)
  if(we <= 5496)
   if(we <= 5280)
    if(we <= 4970)
     if(we <= 4882) return 37.1666666666667;
     if(we > 4882) return 32.6666666666667;
    if(we > 4970)
     if(we <= 4983) return 26;
     if(we > 4983) return 30.6666666666667;
   if(we > 5280)
    if(we <= 5448)
     if(we <= 5313) return 33;
     if(we > 5313) return 31.6666666666667;
    if(we > 5448)
     if(we <= 5487) return 33.6666666666667;
     if(we > 5487) return 31.5;
  if(we > 5496)
   if(we <= 5546)
    if(we <= 5525) return 36.3333333333333;
    if(we > 5525)
     if(we <= 5534) return 31.3333333333333;
     if(we > 5534) return 41;
   if(we > 5546)
    if(we <= 5616)
     if(we <= 5584) return 34;
     if(we > 5584)
      if(we <= 5603) return 34.6666666666667;
      if(we > 5603) return 35.3333333333333;
    if(we > 5616)
     if(we <= 5664) return 37.3333333333333;
     if(we > 5664) return 40.6666666666667;
 if(t > 1.8)
  if(we <= 5511)
   if(we <= 5280)
    if(we <= 4970)
     if(we <= 4882) return 32.8333333333333;
     if(we > 4882) return 31;
    if(we > 4970)
     if(we <= 4983) return 29;
     if(we > 4983) return 31.3333333333333;
   if(we > 5280)
    if(we <= 5479)
     if(we <= 5337) return 31.5;
     if(we > 5337) return 28.6666666666667;
    if(we > 5479)
     if(we <= 5490) return 26;
     if(we > 5490)
      if(we <= 5496) return 41;
      if(we > 5496) return 32.6666666666667;
  if(we > 5511)
   if(we <= 5570)
    if(we <= 5546)
     if(we <= 5532) return 35.3333333333333;
     if(we > 5532) return 37;
    if(we > 5546)
     if(we <= 5560) return 38.1666666666667;
     if(we > 5560) return 34.75;
   if(we > 5570)
    if(we <= 5658) return 36.1666666666667;
    if(we > 5658) return 33.3333333333333;
if(p.equals("PP21809")) return 22.6166666666667;
if(p.equals("PP21814")) return 51.4833333333333;
if(p.equals("PP21855"))
 if(we <= 11684)
  if(t <= 2.23) return 32;
  if(t > 2.23) return 40;
 if(we > 11684)
  if(t <= 2.23) return 37.8166666666667;
  if(t > 2.23) return 41;
if(p.equals("PP21903"))
 if(t <= 3.91)
  if(t <= 3.5)
   if(t <= 3.29) return 42;
   if(t > 3.29) return 64;
  if(t > 3.5)
   if(t <= 3.74) return 38.1166666666667;
   if(t > 3.74) return 33.6166666666667;
 if(t > 3.91)
  if(w <= 348.13)
   if(we <= 6182) return 30;
   if(we > 6182) return 21;
  if(w > 348.13)
   if(w <= 359) return 33.3166666666667;
   if(w > 359) return 40;
if(p.equals("PP21908")) return 27.0166666666667;
if(p.equals("PP22018"))
 if(t <= 2.57)
  if(t <= 2.49)
   if(t <= 2.4) return 49;
   if(t > 2.4) return 36;
  if(t > 2.49) return 39;
 if(t > 2.57)
  if(t <= 2.83)
   if(w <= 543) return 31.5;
   if(w > 543)
    if(w <= 577) return 42.5;
    if(w > 577) return 40.5;
  if(t > 2.83)
   if(t <= 3.15)
    if(w <= 553) return 37.5;
    if(w > 553)
     if(we <= 11451) return 34;
     if(we > 11451) return 33.5;
   if(t > 3.15)
    if(t <= 4.1) return 25;
    if(t > 4.1) return 35;
if(p.equals("PP22049")) return 20;
if(p.equals("PP22060"))
 if(w <= 578)
  if(we <= 9606)
   if(t <= 3.43)
    if(we <= 8068) return 44.9166666666667;
    if(we > 8068) return 32;
   if(t > 3.43)
    if(t <= 4.03) return 28;
    if(t > 4.03) return 22.2333333333333;
  if(we > 9606)
   if(w <= 576)
    if(w <= 514) return 47;
    if(w > 514)
     if(w <= 533) return 37;
     if(w > 533)
      if(t <= 4.03) return 33;
      if(t > 4.03) return 20;
   if(w > 576)
    if(we <= 11521)
     if(we <= 11394) return 37.85;
     if(we > 11394) return 43;
    if(we > 11521)
     if(we <= 11670) return 36.5;
     if(we > 11670) return 30.2833333333333;
 if(w > 578)
  if(w <= 583)
   if(we <= 11499)
    if(we <= 11269) return 47.5;
    if(we > 11269) return 42;
   if(we > 11499)
    if(we <= 11551) return 32;
    if(we > 11551) return 32.5;
  if(w > 583)
   if(w <= 601)
    if(t <= 3.28) return 39;
    if(t > 3.28)
     if(w <= 592) return 45;
     if(w > 592) return 40;
   if(w > 601)
    if(t <= 4.25) return 33;
    if(t > 4.25) return 30;
if(p.equals("PP22069"))
 if(we <= 7647)
  if(t <= 1.31) return 45;
  if(t > 1.31)
   if(we <= 7270) return 26.4666666666667;
   if(we > 7270) return 28.7833333333333;
 if(we > 7647) return 26;
if(p.equals("PP22076")) return 62;
if(p.equals("PP22079"))
 if(t <= 1.63) return 55;
 if(t > 1.63) return 40.0666666666667;
if(p.equals("PP22085"))
 if(we <= 10948)
  if(w <= 532) return 40;
  if(w > 532)
   if(w <= 605) return 43;
   if(w > 605) return 52;
 if(we > 10948)
  if(t <= 1.92)
   if(w <= 545) return 27;
   if(w > 545) return 38;
  if(t > 1.92) return 36;
if(p.equals("PP22160"))
 if(w <= 569)
  if(t <= 4.3) return 15;
  if(t > 4.3)
   if(t <= 6.07) return 30.8666666666667;
   if(t > 6.07) return 24.7833333333333;
 if(w > 569)
  if(w <= 632)
   if(t <= 6.07) return 27.6833333333333;
   if(t > 6.07) return 17.95;
  if(w > 632) return 40;
if(p.equals("PP22171"))
 if(t <= 1.45)
  if(we <= 8310)
   if(t <= 0.85)
    if(w <= 515) return 50.8;
    if(w > 515) return 36;
   if(t > 0.85) return 57.5;
  if(we > 8310)
   if(t <= 1.11)
    if(w <= 510) return 59;
    if(w > 510) return 33.4166666666667;
   if(t > 1.11)
    if(we <= 8360) return 31;
    if(we > 8360) return 39;
 if(t > 1.45)
  if(w <= 500)
   if(t <= 2.6) return 60;
   if(t > 2.6) return 26;
  if(w > 500)
   if(w <= 510)
    if(we <= 8360) return 20;
    if(we > 8360) return 24;
   if(w > 510)
    if(w <= 565) return 35.5;
    if(w > 565)
     if(t <= 1.77) return 39.5833333333333;
     if(t > 1.77) return 28;
if(p.equals("PP22179"))
 if(t <= 3.48) return 35;
 if(t > 3.48) return 29;
if(p.equals("PP22180"))
 if(t <= 4.6)
  if(w <= 363) return 25.4333333333333;
  if(w > 363) return 31;
 if(t > 4.6)
  if(t <= 4.7)
   if(w <= 390) return 20;
   if(w > 390) return 25;
  if(t > 4.7) return 28;
if(p.equals("PP22181"))
 if(w <= 481) return 32;
 if(w > 481) return 50;
if(p.equals("PP22201")) return 39;
if(p.equals("PP22233")) return 40;
if(p.equals("PP22270"))
 if(t <= 2.98) return 32.7166666666667;
 if(t > 2.98) return 27.3833333333333;
if(p.equals("PP22272")) return 55;
if(p.equals("PP22282")) return 45;
if(p.equals("PP22285")) return 40;
if(p.equals("PP22288"))
 if(t <= 4.25)
  if(w <= 520) return 27.1333333333333;
  if(w > 520)
   if(w <= 599) return 27.3666666666667;
   if(w > 599) return 34;
 if(t > 4.25)
  if(t <= 4.8) return 24.6;
  if(t > 4.8) return 17;
if(p.equals("PP22289")) return 33;
if(p.equals("PP22290"))
 if(we <= 12102) return 55;
 if(we > 12102) return 50;
if(p.equals("PP22304"))
 if(t <= 4.85)
  if(t <= 4.2)
   if(w <= 501) return 22.25;
   if(w > 501) return 25;
  if(t > 4.2) return 35;
 if(t > 4.85)
  if(t <= 5.56)
   if(w <= 499) return 54;
   if(w > 499) return 40;
  if(t > 5.56) return 20;
if(p.equals("PP22305"))
 if(we <= 10725) return 34.5;
 if(we > 10725) return 27;
if(p.equals("PP22310")) return 57.55;
if(p.equals("PP22315"))
 if(we <= 9011) return 36.5833333333333;
 if(we > 9011) return 35;
if(p.equals("PP22324")) return 24.6;
if(p.equals("PP22334"))
 if(w <= 528)
  if(w <= 487) return 41.4833333333333;
  if(w > 487)
   if(we <= 10419) return 30;
   if(we > 10419) return 41.1833333333333;
 if(w > 528)
  if(t <= 1.9) return 39.2666666666667;
  if(t > 1.9)
   if(w <= 572) return 30.7166666666667;
   if(w > 572) return 23.1333333333333;
if(p.equals("PP22343")) return 40;
if(p.equals("PP22386")) return 29;
if(p.equals("PP22391")) return 37;
if(p.equals("PP22395"))
 if(t <= 1.35)
  if(we <= 7899) return 38;
  if(we > 7899)
   if(we <= 8150) return 29;
   if(we > 8150) return 28.5;
 if(t > 1.35)
  if(we <= 7961)
   if(t <= 1.42)
    if(we <= 7822) return 40.5;
    if(we > 7822) return 42;
   if(t > 1.42)
    if(we <= 7822) return 31.5;
    if(we > 7822) return 25;
  if(we > 7961)
   if(t <= 1.8)
    if(we <= 8321) return 40;
    if(we > 8321) return 51.5;
   if(t > 1.8) return 38.5;
if(p.equals("PP22412")) return 33.3166666666667;
if(p.equals("PP22413")) return 43.7666666666667;
if(p.equals("PP22419")) return 50;
if(p.equals("PP22423"))
 if(t <= 1.25) return 32;
 if(t > 1.25) return 23;
if(p.equals("PP22438")) return 35;
if(p.equals("PP22456")) return 32;
if(p.equals("PP22457")) return 33;
if(p.equals("PP22459")) return 37;
if(p.equals("PP22460")) return 42;
if(p.equals("PP22461")) return 43;
if(p.equals("PP22478")) return 35;
if(p.equals("PP22479")) return 27;
if(p.equals("PP22496")) return 50;
if(p.equals("PP22522")) return 29;
if(p.equals("PP22539"))
 if(we <= 11484) return 45;
 if(we > 11484) return 31.5;
if(p.equals("PP22553")) return 56;
if(p.equals("PP22561")) return 44;
if(p.equals("PP22604")) return 41;
return 41.0;
}
}
