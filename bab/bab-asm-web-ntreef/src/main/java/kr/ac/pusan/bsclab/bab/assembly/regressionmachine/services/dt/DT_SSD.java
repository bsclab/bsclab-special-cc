package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SSD implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("CRB0112")) return 35;
if(p.equals("CRB0120")) return 32.5;
if(p.equals("CRC1263"))
 if(we <= 526)
  if(we <= 512) return 40;
  if(we > 512)
   if(we <= 521) return 35;
   if(we > 521) return 45;
 if(we > 526)
  if(we <= 535) return 35;
  if(we > 535) return 30;
if(p.equals("PP12183")) return 40;
if(p.equals("PP12184"))
 if(t <= 0.66)
  if(t <= 0.428) return 55;
  if(t > 0.428)
   if(t <= 0.635) return 40;
   if(t > 0.635) return 50;
 if(t > 0.66)
  if(w <= 335)
   if(t <= 0.818)
    if(w <= 328)
     if(w <= 309) return 35;
     if(w > 309)
      if(we <= 3771) return 25;
      if(we > 3771) return 30;
    if(w > 328) return 35;
   if(t > 0.818)
    if(w <= 304) return 35;
    if(w > 304)
     if(we <= 4204) return 20;
     if(we > 4204) return 40;
  if(w > 335)
   if(t <= 0.725)
    if(w <= 358) return 30;
    if(w > 358) return 25;
   if(t > 0.725)
    if(w <= 343)
     if(we <= 4672) return 25;
     if(we > 4672) return 45;
    if(w > 343) return 15;
if(p.equals("PP12186"))
 if(t <= 1.1)
  if(w <= 164) return 15;
  if(w > 164)
   if(we <= 1082)
    if(we <= 906)
     if(we <= 680) return 45;
     if(we > 680) return 20;
    if(we > 906) return 10;
   if(we > 1082) return 20;
 if(t > 1.1)
  if(we <= 724) return 20;
  if(we > 724)
   if(we <= 1334)
    if(w <= 169) return 5;
    if(w > 169) return 10;
   if(we > 1334) return 15;
if(p.equals("PP12188"))
 if(w <= 163.5) return 30;
 if(w > 163.5) return 40;
if(p.equals("PP12190")) return 30;
if(p.equals("PP12192"))
 if(t <= 0.488) return 38;
 if(t > 0.488) return 35;
if(p.equals("PP12193"))
 if(w <= 220)
  if(t <= 1.1)
   if(we <= 1334) return 35;
   if(we > 1334) return 20;
  if(t > 1.1) return 15;
 if(w > 220)
  if(w <= 223)
   if(we <= 977) return 17.5;
   if(we > 977) return 25;
  if(w > 223)
   if(we <= 973) return 15;
   if(we > 973)
    if(w <= 235)
     if(w <= 233.5) return 20;
     if(w > 233.5) return 15;
    if(w > 235) return 20;
if(p.equals("PP12194"))
 if(t <= 0.407)
  if(t <= 0.308) return 50;
  if(t > 0.308) return 35;
 if(t > 0.407)
  if(w <= 315) return 25;
  if(w > 315)
   if(we <= 4296) return 40;
   if(we > 4296) return 45;
if(p.equals("PP12195"))
 if(we <= 4225) return 30;
 if(we > 4225) return 35;
if(p.equals("PP12196"))
 if(w <= 133)
  if(we <= 399) return 40;
  if(we > 399)
   if(we <= 410) return 35;
   if(we > 410) return 50;
 if(w > 133) return 60;
if(p.equals("PP12197"))
 if(w <= 307)
  if(w <= 114)
   if(w <= 77)
    if(we <= 398)
     if(we <= 386) return 20;
     if(we > 386)
      if(we <= 391) return 15;
      if(we > 391) return 50;
    if(we > 398)
     if(we <= 410)
      if(we <= 400) return 27.5;
      if(we > 400) return 37.5;
     if(we > 410)
      if(we <= 416) return 30;
      if(we > 416) return 37;
   if(w > 77)
    if(we <= 447) return 20;
    if(we > 447) return 30;
  if(w > 114)
   if(t <= 1.8)
    if(we <= 766)
     if(we <= 746) return 35;
     if(we > 746) return 55;
    if(we > 766) return 27;
   if(t > 1.8)
    if(w <= 237) return 30;
    if(w > 237)
     if(we <= 3330) return 40;
     if(we > 3330) return 25;
 if(w > 307)
  if(we <= 3940) return 15;
  if(we > 3940) return 20;
if(p.equals("PP12198"))
 if(t <= 1.22)
  if(w <= 105)
   if(w <= 103.5)
    if(we <= 429) return 40;
    if(we > 429) return 35;
   if(w > 103.5)
    if(we <= 409) return 75;
    if(we > 409) return 55;
  if(w > 105)
   if(we <= 478)
    if(we <= 470)
     if(we <= 344) return 20;
     if(we > 344) return 15;
    if(we > 470)
     if(we <= 472) return 37.15;
     if(we > 472) return 20;
   if(we > 478)
    if(we <= 500)
     if(we <= 482)
      if(w <= 110) return 10;
      if(w > 110) return 60;
     if(we > 482)
      if(we <= 494) return 15;
      if(we > 494) return 35;
    if(we > 500)
     if(w <= 114)
      if(we <= 503) return 10;
      if(we > 503) return 15;
     if(w > 114)
      if(we <= 516) return 20;
      if(we > 516) return 10;
 if(t > 1.22)
  if(t <= 1.6)
   if(t <= 1.55)
    if(w <= 372.5)
     if(w <= 274) return 30;
     if(w > 274)
      if(we <= 3582)
       if(we <= 3324) return 35;
       if(we > 3324) return 25;
      if(we > 3582) return 35;
    if(w > 372.5)
     if(we <= 3638)
      if(we <= 3487)
       if(we <= 3453) return 30;
       if(we > 3453) return 45;
      if(we > 3487) return 30;
     if(we > 3638)
      if(we <= 3850) return 20;
      if(we > 3850) return 25;
   if(t > 1.55)
    if(we <= 2020)
     if(we <= 1870)
      if(w <= 246)
       if(we <= 1161) return 25;
       if(we > 1161) return 45;
      if(w > 246)
       if(w <= 316)
        if(we <= 1747) return 45;
        if(we > 1747)
         if(we <= 1864) return 40;
         if(we > 1864) return 50;
       if(w > 316) return 25;
     if(we > 1870) return 30;
    if(we > 2020) return 35;
  if(t > 1.6)
   if(we <= 3379)
    if(we <= 3148)
     if(t <= 1.9)
      if(we <= 2568) return 10;
      if(we > 2568)
       if(we <= 3090)
        if(we <= 2896) return 20;
        if(we > 2896)
         if(we <= 3056) return 25;
         if(we > 3056) return 20;
       if(we > 3090) return 22.5;
     if(t > 1.9) return 25;
    if(we > 3148)
     if(w <= 343) return 20;
     if(w > 343)
      if(we <= 3242) return 45;
      if(we > 3242)
       if(we <= 3273) return 35;
       if(we > 3273) return 30;
   if(we > 3379)
    if(t <= 1.675) return 25;
    if(t > 1.675) return 20;
if(p.equals("PP12213")) return 30;
if(p.equals("PP12221"))
 if(t <= 0.45)
  if(t <= 0.388) return 45;
  if(t > 0.388) return 55;
 if(t > 0.45) return 115;
if(p.equals("PP12227")) return 45;
if(p.equals("PP12228"))
 if(t <= 1.15) return 45;
 if(t > 1.15) return 30;
if(p.equals("PP12229")) return 24;
if(p.equals("PP12230"))
 if(we <= 974) return 15;
 if(we > 974) return 20;
if(p.equals("PP12232")) return 30;
if(p.equals("PP12234"))
 if(t <= 0.53) return 50;
 if(t > 0.53) return 40;
if(p.equals("PP12238")) return 85;
if(p.equals("PP12248"))
 if(we <= 1974) return 25;
 if(we > 1974)
  if(we <= 2198)
   if(w <= 152.5) return 20;
   if(w > 152.5) return 35;
  if(we > 2198) return 40;
if(p.equals("PP12251"))
 if(t <= 1.1)
  if(we <= 1154)
   if(we <= 942) return 20;
   if(we > 942) return 15;
  if(we > 1154)
   if(we <= 1348) return 40;
   if(we > 1348) return 20;
 if(t > 1.1)
  if(we <= 876) return 30;
  if(we > 876)
   if(w <= 175) return 20;
   if(w > 175) return 10;
if(p.equals("PP12252"))
 if(w <= 237) return 20;
 if(w > 237) return 10;
if(p.equals("PP12253"))
 if(t <= 0.985)
  if(we <= 1238) return 15;
  if(we > 1238)
   if(w <= 247)
    if(we <= 1962) return 10;
    if(we > 1962)
     if(we <= 2052) return 25;
     if(we > 2052) return 15;
   if(w > 247)
    if(w <= 269)
     if(we <= 2294)
      if(we <= 2016)
       if(we <= 1978) return 20;
       if(we > 1978) return 25;
      if(we > 2016) return 30;
     if(we > 2294) return 20;
    if(w > 269)
     if(we <= 1584) return 30;
     if(we > 1584)
      if(we <= 2475) return 20;
      if(we > 2475) return 25;
 if(t > 0.985)
  if(w <= 247) return 40;
  if(w > 247)
   if(we <= 3128) return 25;
   if(we > 3128) return 30;
if(p.equals("PP12256"))
 if(t <= 0.25) return 100;
 if(t > 0.25) return 110;
if(p.equals("PP12257"))
 if(t <= 1.55)
  if(t <= 1.36)
   if(we <= 2184) return 60;
   if(we > 2184) return 45;
  if(t > 1.36)
   if(we <= 2181) return 25;
   if(we > 2181) return 32.5;
 if(t > 1.55)
  if(t <= 1.76)
   if(we <= 1756)
    if(w <= 205)
     if(we <= 1736) return 30;
     if(we > 1736) return 38.3333333333333;
    if(w > 205)
     if(t <= 1.675) return 30;
     if(t > 1.675) return 45;
   if(we > 1756)
    if(w <= 206)
     if(we <= 1882)
      if(we <= 1814)
       if(we <= 1776) return 25;
       if(we > 1776) return 30;
      if(we > 1814)
       if(we <= 1834) return 20;
       if(we > 1834) return 25;
     if(we > 1882)
      if(we <= 1907) return 26;
      if(we > 1907) return 15;
    if(w > 206)
     if(we <= 1896)
      if(we <= 1880) return 20;
      if(we > 1880) return 30;
     if(we > 1896)
      if(we <= 1980) return 30;
      if(we > 1980)
       if(we <= 2221)
        if(we <= 2044) return 25;
        if(we > 2044) return 30;
       if(we > 2221) return 20;
  if(t > 1.76) return 40;
if(p.equals("PP12262")) return 40;
if(p.equals("PP12263"))
 if(we <= 4123) return 25;
 if(we > 4123) return 40;
if(p.equals("PP12267"))
 if(w <= 160.5)
  if(we <= 1063)
   if(we <= 631)
    if(we <= 598) return 15;
    if(we > 598) return 20;
   if(we > 631) return 25;
  if(we > 1063) return 20;
 if(w > 160.5)
  if(t <= 0.825)
   if(we <= 1053) return 10;
   if(we > 1053) return 22;
  if(t > 0.825) return 25;
if(p.equals("PP12275"))
 if(t <= 1.4)
  if(t <= 1.35)
   if(we <= 2054) return 32.5;
   if(we > 2054)
    if(we <= 2120)
     if(we <= 2108)
      if(we <= 2097) return 30;
      if(we > 2097) return 25;
     if(we > 2108) return 20;
    if(we > 2120)
     if(we <= 2160)
      if(we <= 2137) return 30;
      if(we > 2137) return 37.5;
     if(we > 2160)
      if(we <= 2206) return 25;
      if(we > 2206) return 10;
  if(t > 1.35) return 25;
 if(t > 1.4)
  if(we <= 2575) return 30;
  if(we > 2575)
   if(we <= 2706) return 25;
   if(we > 2706)
    if(t <= 1.915) return 20;
    if(t > 1.915)
     if(we <= 2872)
      if(we <= 2832) return 25;
      if(we > 2832) return 20;
     if(we > 2872) return 25;
if(p.equals("PP12280")) return 15;
if(p.equals("PP12282")) return 45;
if(p.equals("PP12283"))
 if(t <= 1.3)
  if(w <= 103.5) return 110;
  if(w > 103.5)
   if(we <= 401.5) return 15;
   if(we > 401.5) return 25;
 if(t > 1.3)
  if(w <= 255)
   if(we <= 1077) return 50;
   if(we > 1077) return 21.6666666666667;
  if(w > 255) return 30;
if(p.equals("PP12284"))
 if(w <= 293.5) return 30;
 if(w > 293.5) return 5;
if(p.equals("PP12285")) return 40;
if(p.equals("PP12286"))
 if(w <= 187)
  if(we <= 1144) return 20;
  if(we > 1144) return 30;
 if(w > 187) return 15;
if(p.equals("PP12287")) return 20;
if(p.equals("PP12296")) return 65;
if(p.equals("PP12297")) return 30;
if(p.equals("PP12306")) return 100;
if(p.equals("PP12307")) return 10;
if(p.equals("PP12308")) return 30;
if(p.equals("PP12313"))
 if(t <= 1.495)
  if(t <= 1.15)
   if(we <= 456) return 40;
   if(we > 456) return 20;
  if(t > 1.15)
   if(we <= 2174)
    if(we <= 2086) return 30;
    if(we > 2086) return 20;
   if(we > 2174)
    if(we <= 2505) return 50;
    if(we > 2505) return 30;
 if(t > 1.495)
  if(t <= 1.675)
   if(we <= 2015) return 20;
   if(we > 2015)
    if(we <= 2453) return 25;
    if(we > 2453) return 30;
  if(t > 1.675)
   if(we <= 1846)
    if(we <= 1774) return 28;
    if(we > 1774) return 25;
   if(we > 1846) return 30;
if(p.equals("PP12319")) return 20;
if(p.equals("PP12324"))
 if(t <= 1.78)
  if(we <= 2293) return 25;
  if(we > 2293) return 30;
 if(t > 1.78)
  if(we <= 2362) return 35;
  if(we > 2362) return 30;
if(p.equals("PP12325"))
 if(w <= 142) return 25;
 if(w > 142) return 20;
if(p.equals("PP12326"))
 if(w <= 127)
  if(w <= 123) return 20;
  if(w > 123) return 15;
 if(w > 127) return 30;
if(p.equals("PP12330")) return 35;
if(p.equals("PP12336")) return 20;
if(p.equals("PP12340"))
 if(we <= 1606)
  if(we <= 1435)
   if(w <= 207)
    if(w <= 206)
     if(w <= 196)
      if(we <= 1252) return 15;
      if(we > 1252) return 20;
     if(w > 196) return 10;
    if(w > 206) return 15;
   if(w > 207) return 20;
  if(we > 1435)
   if(w <= 225)
    if(we <= 1472) return 10;
    if(we > 1472) return 25;
   if(w > 225) return 15;
 if(we > 1606)
  if(w <= 241) return 10;
  if(w > 241)
   if(w <= 251) return 30;
   if(w > 251) return 15;
if(p.equals("PP12344")) return 40;
if(p.equals("PP12349")) return 55;
if(p.equals("PP12350")) return 25;
if(p.equals("PP12351")) return 45;
if(p.equals("PP12354"))
 if(we <= 1622) return 55;
 if(we > 1622) return 10;
if(p.equals("PP12355")) return 40;
if(p.equals("PP12356"))
 if(t <= 0.9)
  if(we <= 3514) return 27.5;
  if(we > 3514) return 45;
 if(t > 0.9) return 40;
if(p.equals("PP12358"))
 if(we <= 1012) return 20;
 if(we > 1012) return 25;
if(p.equals("PP12360"))
 if(w <= 159) return 20;
 if(w > 159)
  if(we <= 1048) return 30;
  if(we > 1048) return 17;
if(p.equals("PP12362")) return 40;
if(p.equals("PP12364"))
 if(t <= 0.507) return 25;
 if(t > 0.507) return 35;
if(p.equals("PP12366"))
 if(t <= 0.73) return 20;
 if(t > 0.73) return 35;
if(p.equals("PP12369")) return 30;
if(p.equals("PP12376")) return 20;
if(p.equals("PP12377"))
 if(t <= 0.725) return 50;
 if(t > 0.725) return 25;
if(p.equals("PP12383")) return 25;
if(p.equals("PP12384"))
 if(t <= 0.73) return 20;
 if(t > 0.73) return 45;
if(p.equals("PP12388"))
 if(we <= 1000) return 40;
 if(we > 1000) return 60;
if(p.equals("PP12389"))
 if(we <= 1103) return 50;
 if(we > 1103)
  if(we <= 1270) return 45;
  if(we > 1270) return 35;
if(p.equals("PP12390"))
 if(we <= 1454) return 30;
 if(we > 1454)
  if(t <= 1.8) return 25;
  if(t > 1.8) return 35;
if(p.equals("PP12392")) return 35;
if(p.equals("PP12395")) return 55;
if(p.equals("PP12408")) return 13;
if(p.equals("PP12409")) return 15;
if(p.equals("PP12411")) return 25;
if(p.equals("PP12421")) return 30;
if(p.equals("PP12423")) return 30;
if(p.equals("PP12424")) return 75;
if(p.equals("PP12428"))
 if(t <= 1) return 40;
 if(t > 1) return 25;
if(p.equals("PP12429")) return 35;
if(p.equals("PP12430")) return 20;
if(p.equals("PP12431")) return 25;
if(p.equals("PP12432")) return 25;
if(p.equals("PP12435"))
 if(w <= 240)
  if(we <= 1945)
   if(we <= 1927) return 35;
   if(we > 1927) return 45;
  if(we > 1945)
   if(t <= 1.66) return 20;
   if(t > 1.66)
    if(t <= 1.915) return 25;
    if(t > 1.915) return 30;
 if(w > 240) return 20;
if(p.equals("PP12436"))
 if(w <= 361)
  if(w <= 77)
   if(we <= 665) return 50;
   if(we > 665)
    if(we <= 671) return 25.45;
    if(we > 671) return 35;
  if(w > 77)
   if(we <= 1026) return 30;
   if(we > 1026)
    if(we <= 1067) return 25;
    if(we > 1067) return 35;
 if(w > 361)
  if(we <= 3324) return 15;
  if(we > 3324) return 30;
if(p.equals("PP12437"))
 if(t <= 0.825) return 105;
 if(t > 0.825)
  if(we <= 1002)
   if(we <= 977) return 15;
   if(we > 977) return 80;
  if(we > 1002) return 50;
if(p.equals("PP12438")) return 40;
if(p.equals("PP12440"))
 if(t <= 1.54) return 70;
 if(t > 1.54) return 35;
if(p.equals("PP12443"))
 if(w <= 310)
  if(w <= 298) return 35;
  if(w > 298) return 40;
 if(w > 310) return 20;
if(p.equals("PP12444"))
 if(w <= 258) return 30;
 if(w > 258) return 28;
if(p.equals("PP12457")) return 20;
if(p.equals("PP12459")) return 25;
if(p.equals("PP12460")) return 30;
if(p.equals("PP12461"))
 if(w <= 220)
  if(t <= 1.72) return 25;
  if(t > 1.72) return 20;
 if(w > 220)
  if(we <= 2134) return 30;
  if(we > 2134) return 25;
if(p.equals("PP12462"))
 if(t <= 1.74)
  if(w <= 227)
   if(t <= 1.68)
    if(we <= 2490)
     if(we <= 1965) return 25;
     if(we > 1965)
      if(we <= 2142) return 20;
      if(we > 2142)
       if(we <= 2220) return 25;
       if(we > 2220)
        if(we <= 2398) return 35;
        if(we > 2398) return 45;
    if(we > 2490)
     if(we <= 2544) return 25;
     if(we > 2544) return 20;
   if(t > 1.68) return 40;
  if(w > 227)
   if(t <= 1.54)
    if(t <= 1.4)
     if(we <= 2040) return 25;
     if(we > 2040) return 20;
    if(t > 1.4)
     if(we <= 2256) return 25;
     if(we > 2256)
      if(we <= 2280) return 20;
      if(we > 2280) return 30;
   if(t > 1.54)
    if(we <= 2796)
     if(we <= 2659) return 25;
     if(we > 2659) return 30;
    if(we > 2796)
     if(w <= 232) return 20;
     if(w > 232) return 40;
 if(t > 1.74)
  if(w <= 236)
   if(we <= 2368)
    if(we <= 2362) return 20;
    if(we > 2362) return 22.5;
   if(we > 2368)
    if(we <= 2446) return 30;
    if(we > 2446) return 25;
  if(w > 236) return 30;
if(p.equals("PP12470"))
 if(we <= 2152) return 30;
 if(we > 2152) return 50;
if(p.equals("PP12484")) return 40;
if(p.equals("PP12488")) return 40;
if(p.equals("PP12491")) return 25;
if(p.equals("PP12500")) return 30;
if(p.equals("PP12501")) return 30;
if(p.equals("PP12508"))
 if(t <= 0.9) return 15;
 if(t > 0.9) return 20;
if(p.equals("PP12510")) return 25;
if(p.equals("PP12513"))
 if(we <= 2218) return 35;
 if(we > 2218) return 40;
if(p.equals("PP12519")) return 30;
if(p.equals("PP12521")) return 30;
if(p.equals("PP12526")) return 30;
if(p.equals("PP12528")) return 35;
if(p.equals("PP12538")) return 35;
if(p.equals("PP12545"))
 if(t <= 1.94) return 15;
 if(t > 1.94)
  if(we <= 1879) return 30;
  if(we > 1879) return 25;
if(p.equals("PP12546"))
 if(t <= 1.36) return 50;
 if(t > 1.36) return 30;
if(p.equals("PP12554")) return 45;
if(p.equals("PP12565")) return 20;
if(p.equals("PP12578")) return 10;
if(p.equals("PP21894"))
 if(we <= 1618)
  if(w <= 111.1)
   if(we <= 429) return 130;
   if(we > 429)
    if(w <= 105) return 50;
    if(w > 105) return 70;
  if(w > 111.1) return 55;
 if(we > 1618)
  if(w <= 218)
   if(we <= 1824)
    if(we <= 1716) return 25;
    if(we > 1716) return 30;
   if(we > 1824)
    if(we <= 1912)
     if(we <= 1830) return 22.5;
     if(we > 1830) return 25;
    if(we > 1912)
     if(we <= 2193)
      if(we <= 2120) return 30;
      if(we > 2120)
       if(we <= 2162) return 25;
       if(we > 2162) return 30;
     if(we > 2193)
      if(we <= 2231) return 35;
      if(we > 2231) return 30;
  if(w > 218)
   if(t <= 1.36)
    if(we <= 2130) return 25;
    if(we > 2130)
     if(we <= 2160) return 34;
     if(we > 2160) return 20;
   if(t > 1.36)
    if(w <= 227)
     if(we <= 2078)
      if(we <= 1845) return 40;
      if(we > 1845) return 25;
     if(we > 2078)
      if(we <= 2137)
       if(we <= 2110) return 15;
       if(we > 2110) return 40;
      if(we > 2137) return 30;
    if(w > 227) return 30;
if(p.equals("PP21896")) return 30;
if(p.equals("PP21901")) return 15;
if(p.equals("PP21904"))
 if(t <= 3)
  if(w <= 235) return 20;
  if(w > 235)
   if(we <= 2732) return 15;
   if(we > 2732) return 25;
 if(t > 3)
  if(w <= 279) return 30;
  if(w > 279)
   if(we <= 3030) return 22;
   if(we > 3030)
    if(we <= 3307) return 20;
    if(we > 3307) return 30;
if(p.equals("PP21906"))
 if(t <= 2)
  if(t <= 1.42)
   if(we <= 2553)
    if(we <= 2532) return 22.5;
    if(we > 2532) return 25;
   if(we > 2553) return 35;
  if(t > 1.42)
   if(t <= 1.75) return 20;
   if(t > 1.75)
    if(we <= 2430)
     if(we <= 2184) return 25;
     if(we > 2184) return 30;
    if(we > 2430)
     if(we <= 2510) return 32.5;
     if(we > 2510) return 25;
 if(t > 2)
  if(we <= 2883) return 40;
  if(we > 2883) return 30;
if(p.equals("PP21908")) return 50;
if(p.equals("PP21912"))
 if(t <= 1.4) return 30;
 if(t > 1.4)
  if(w <= 237)
   if(we <= 2142)
    if(we <= 1960) return 40;
    if(we > 1960) return 25;
   if(we > 2142)
    if(we <= 2544)
     if(we <= 2364)
      if(we <= 2237)
       if(we <= 2214)
        if(we <= 2206) return 24.3666666666667;
        if(we > 2206) return 30;
       if(we > 2214) return 20;
      if(we > 2237) return 25;
     if(we > 2364)
      if(we <= 2399) return 5;
      if(we > 2399) return 20;
    if(we > 2544)
     if(w <= 235.5) return 35;
     if(w > 235.5) return 30;
  if(w > 237)
   if(t <= 1.93) return 27.5;
   if(t > 1.93)
    if(we <= 2326) return 15;
    if(we > 2326)
     if(we <= 2789) return 25;
     if(we > 2789) return 20;
if(p.equals("PP21913"))
 if(we <= 1097)
  if(t <= 1.55)
   if(t <= 1.22)
    if(w <= 111.1) return 20;
    if(w > 111.1) return 150;
   if(t > 1.22) return 55;
  if(t > 1.55)
   if(we <= 1089)
    if(w <= 367.25) return 40;
    if(w > 367.25) return 60;
   if(we > 1089) return 20;
 if(we > 1097)
  if(we <= 2918)
   if(t <= 2.24)
    if(t <= 1.7)
     if(w <= 180)
      if(we <= 1608) return 25;
      if(we > 1608) return 35;
     if(w > 180)
      if(we <= 2820)
       if(we <= 1760)
        if(we <= 1176) return 45;
        if(we > 1176) return 50;
       if(we > 1760)
        if(w <= 316) return 35;
        if(w > 316) return 30;
      if(we > 2820)
       if(w <= 343)
        if(we <= 2892) return 20;
        if(we > 2892) return 25;
       if(w > 343) return 50;
    if(t > 1.7)
     if(we <= 1530) return 30;
     if(we > 1530)
      if(we <= 1552) return 20;
      if(we > 1552) return 25;
   if(t > 2.24)
    if(t <= 2.38)
     if(w <= 360) return 46;
     if(w > 360) return 45;
    if(t > 2.38) return 15;
  if(we > 2918)
   if(we <= 3436)
    if(we <= 3202)
     if(we <= 3068)
      if(we <= 3003) return 20;
      if(we > 3003)
       if(we <= 3040) return 40;
       if(we > 3040) return 20;
     if(we > 3068)
      if(w <= 343) return 40;
      if(w > 343)
       if(we <= 3140) return 30;
       if(we > 3140)
        if(t <= 1.675)
         if(we <= 3155) return 50;
         if(we > 3155) return 15;
        if(t > 1.675) return 15;
    if(we > 3202)
     if(t <= 1.55)
      if(we <= 3356) return 40;
      if(we > 3356) return 25;
     if(t > 1.55)
      if(we <= 3224) return 20;
      if(we > 3224) return 25;
   if(we > 3436)
    if(w <= 361)
     if(we <= 3774)
      if(we <= 3524) return 35;
      if(we > 3524) return 30;
     if(we > 3774)
      if(we <= 4006) return 35;
      if(we > 4006) return 20;
    if(w > 361) return 30;
if(p.equals("PP21915"))
 if(w <= 263) return 20;
 if(w > 263) return 25;
if(p.equals("PP21917")) return 80;
if(p.equals("PP21922")) return 40;
if(p.equals("PP21932")) return 20;
if(p.equals("PP21933"))
 if(w <= 169)
  if(we <= 1024) return 20;
  if(we > 1024) return 25;
 if(w > 169) return 20;
if(p.equals("PP21935")) return 30;
if(p.equals("PP21938")) return 40;
if(p.equals("PP21940")) return 55;
if(p.equals("PP21943"))
 if(we <= 1363) return 15;
 if(we > 1363) return 20;
if(p.equals("PP21945")) return 65;
if(p.equals("PP21958"))
 if(we <= 1823) return 20;
 if(we > 1823)
  if(we <= 2612) return 10;
  if(we > 2612) return 40;
if(p.equals("PP21965")) return 15;
if(p.equals("PP21972"))
 if(w <= 233.5) return 18;
 if(w > 233.5) return 20;
if(p.equals("PP21975"))
 if(w <= 224.5) return 25;
 if(w > 224.5)
  if(w <= 235)
   if(we <= 2544)
    if(t <= 2.6) return 20;
    if(t > 2.6)
     if(we <= 2512) return 25;
     if(we > 2512) return 35;
   if(we > 2544)
    if(we <= 2598) return 30;
    if(we > 2598) return 15;
  if(w > 235) return 30;
if(p.equals("PP21994"))
 if(we <= 2178)
  if(t <= 2.72) return 20;
  if(t > 2.72)
   if(t <= 2.78) return 30;
   if(t > 2.78) return 15;
 if(we > 2178)
  if(t <= 2.62)
   if(t <= 2.6) return 30;
   if(t > 2.6) return 47.5;
  if(t > 2.62)
   if(we <= 2622)
    if(we <= 2346)
     if(we <= 2275)
      if(we <= 2194) return 30;
      if(we > 2194) return 25;
     if(we > 2275)
      if(t <= 2.68) return 30;
      if(t > 2.68) return 15;
    if(we > 2346)
     if(w <= 240)
      if(we <= 2481)
       if(we <= 2370) return 20;
       if(we > 2370)
        if(we <= 2428) return 40;
        if(we > 2428) return 20;
      if(we > 2481) return 25;
     if(w > 240)
      if(we <= 2517) return 25;
      if(we > 2517) return 20;
   if(we > 2622) return 30;
if(p.equals("PP22003"))
 if(w <= 274)
  if(we <= 2493)
   if(we <= 2444) return 15;
   if(we > 2444) return 27;
  if(we > 2493)
   if(we <= 3517)
    if(w <= 243) return 25;
    if(w > 243)
     if(we <= 2926) return 25;
     if(we > 2926) return 30;
   if(we > 3517)
    if(t <= 1.41) return 25;
    if(t > 1.41) return 50;
 if(w > 274)
  if(t <= 2.65) return 20;
  if(t > 2.65) return 40;
if(p.equals("PP22005")) return 30;
if(p.equals("PP22009"))
 if(t <= 1.83)
  if(t <= 1.7)
   if(we <= 2353) return 30;
   if(we > 2353) return 15;
  if(t > 1.7) return 25;
 if(t > 1.83)
  if(we <= 1850) return 25;
  if(we > 1850)
   if(we <= 1908) return 15;
   if(we > 1908) return 20;
if(p.equals("PP22014")) return 30;
if(p.equals("PP22015")) return 30;
if(p.equals("PP22017"))
 if(t <= 2.48)
  if(we <= 1005)
   if(we <= 992) return 24;
   if(we > 992) return 37.5;
  if(we > 1005)
   if(we <= 1024) return 35;
   if(we > 1024) return 40;
 if(t > 2.48) return 75;
if(p.equals("PP22041")) return 35;
if(p.equals("PP22052"))
 if(w <= 169) return 20;
 if(w > 169)
  if(we <= 1852) return 35;
  if(we > 1852) return 55;
if(p.equals("PP22054"))
 if(t <= 2.88) return 15;
 if(t > 2.88) return 25;
if(p.equals("PP22056")) return 20;
if(p.equals("PP22067"))
 if(we <= 2475) return 25;
 if(we > 2475) return 20;
if(p.equals("PP22069"))
 if(we <= 1098)
  if(t <= 1.3) return 37;
  if(t > 1.3) return 20;
 if(we > 1098)
  if(w <= 246) return 40;
  if(w > 246) return 45;
if(p.equals("PP22071")) return 30;
if(p.equals("PP22074")) return 15;
if(p.equals("PP22086"))
 if(we <= 1660) return 60;
 if(we > 1660) return 45;
if(p.equals("PP22093")) return 25;
if(p.equals("PP22094")) return 25;
if(p.equals("PP22096")) return 30;
if(p.equals("PP22102")) return 30;
if(p.equals("PP22111")) return 40;
if(p.equals("PP22112"))
 if(t <= 0.507) return 85;
 if(t > 0.507)
  if(we <= 4358) return 30;
  if(we > 4358) return 25;
if(p.equals("PP22115"))
 if(w <= 212) return 30;
 if(w > 212)
  if(we <= 968) return 10;
  if(we > 968) return 15;
if(p.equals("PP22135")) return 50;
if(p.equals("PP22136")) return 65;
if(p.equals("PP22137")) return 35;
if(p.equals("PP22157"))
 if(w <= 317)
  if(t <= 1.3) return 45;
  if(t > 1.3) return 40;
 if(w > 317) return 60;
if(p.equals("PP22174"))
 if(w <= 220)
  if(we <= 2464) return 30;
  if(we > 2464) return 25;
 if(w > 220) return 30;
if(p.equals("PP22175")) return 20;
if(p.equals("PP22183")) return 30;
if(p.equals("PP22184")) return 20;
if(p.equals("PP22186")) return 45;
if(p.equals("PP22195")) return 30;
if(p.equals("PP22210")) return 40;
if(p.equals("PP22213"))
 if(we <= 1572) return 35;
 if(we > 1572) return 55;
if(p.equals("PP22215")) return 20;
if(p.equals("PP22225")) return 37.5;
if(p.equals("PP22229"))
 if(w <= 157)
  if(we <= 1040) return 20;
  if(we > 1040) return 35;
 if(w > 157) return 15;
if(p.equals("PP22236"))
 if(we <= 1556)
  if(w <= 255) return 20;
  if(w > 255) return 15;
 if(we > 1556)
  if(t <= 0.825) return 25;
  if(t > 0.825) return 20;
if(p.equals("PP22240")) return 60;
if(p.equals("PP22243"))
 if(t <= 0.73)
  if(we <= 884) return 32.5;
  if(we > 884)
   if(we <= 894) return 25;
   if(we > 894) return 20;
 if(t > 0.73)
  if(w <= 296) return 30;
  if(w > 296) return 40;
if(p.equals("PP22261")) return 35;
if(p.equals("PP22262"))
 if(t <= 0.73)
  if(we <= 1100)
   if(we <= 1095) return 65;
   if(we > 1095) return 50;
  if(we > 1100)
   if(we <= 1113) return 100;
   if(we > 1113) return 30;
 if(t > 0.73)
  if(we <= 4730) return 35;
  if(we > 4730) return 40;
if(p.equals("PP22263"))
 if(we <= 758) return 25;
 if(we > 758)
  if(we <= 1136) return 35;
  if(we > 1136)
   if(we <= 1500) return 45;
   if(we > 1500) return 32.5;
if(p.equals("PP22264")) return 25;
if(p.equals("PP22267")) return 35;
if(p.equals("PP22268")) return 30;
if(p.equals("PP22276"))
 if(we <= 838)
  if(we <= 798) return 25;
  if(we > 798) return 50;
 if(we > 838)
  if(we <= 916) return 30;
  if(we > 916) return 20;
if(p.equals("PP22277"))
 if(t <= 0.53)
  if(we <= 930) return 100;
  if(we > 930) return 25;
 if(t > 0.53)
  if(we <= 1033)
   if(w <= 158) return 20;
   if(w > 158) return 15;
  if(we > 1033)
   if(w <= 163.5) return 25;
   if(w > 163.5) return 19;
if(p.equals("PP22278"))
 if(we <= 3548) return 25;
 if(we > 3548) return 30;
if(p.equals("PP22284")) return 125;
if(p.equals("PP22292"))
 if(we <= 1341) return 67;
 if(we > 1341) return 20;
if(p.equals("PP22297"))
 if(we <= 3112)
  if(we <= 2370) return 20;
  if(we > 2370) return 25;
 if(we > 3112) return 30;
if(p.equals("PP22298")) return 20;
if(p.equals("PP22300")) return 55;
if(p.equals("PP22306")) return 30;
if(p.equals("PP22307"))
 if(w <= 217) return 10;
 if(w > 217) return 25;
if(p.equals("PP22312"))
 if(we <= 778)
  if(we <= 553) return 60;
  if(we > 553) return 50;
 if(we > 778)
  if(we <= 977) return 35;
  if(we > 977)
   if(we <= 1200) return 80;
   if(we > 1200) return 25;
if(p.equals("PP22316")) return 65;
if(p.equals("PP22318"))
 if(we <= 622) return 20;
 if(we > 622) return 25;
if(p.equals("PP22319")) return 120;
if(p.equals("PP22322"))
 if(w <= 211)
  if(we <= 2213) return 25;
  if(we > 2213) return 20;
 if(w > 211)
  if(we <= 2430) return 30;
  if(we > 2430) return 20;
if(p.equals("PP22341")) return 50;
if(p.equals("PP22342"))
 if(t <= 1.78) return 45;
 if(t > 1.78)
  if(we <= 1055)
   if(w <= 77) return 35;
   if(w > 77)
    if(we <= 1020) return 40;
    if(we > 1020) return 27.15;
  if(we > 1055)
   if(we <= 1074) return 50;
   if(we > 1074) return 25;
if(p.equals("PP22350")) return 30;
if(p.equals("PP22352"))
 if(w <= 202.5)
  if(we <= 2190)
   if(we <= 2112) return 14;
   if(we > 2112) return 40;
  if(we > 2190) return 30;
 if(w > 202.5)
  if(we <= 2296)
   if(we <= 2214) return 30;
   if(we > 2214)
    if(we <= 2264) return 15;
    if(we > 2264) return 30;
  if(we > 2296)
   if(we <= 2346)
    if(w <= 214) return 30;
    if(w > 214) return 25;
   if(we > 2346) return 30;
if(p.equals("PP22354")) return 35;
if(p.equals("PP22355"))
 if(we <= 1034) return 25;
 if(we > 1034)
  if(w <= 163) return 15;
  if(w > 163) return 30;
if(p.equals("PP22358"))
 if(t <= 1.79) return 30;
 if(t > 1.79)
  if(we <= 1672) return 30;
  if(we > 1672) return 35;
if(p.equals("PP22362"))
 if(t <= 0.73) return 25;
 if(t > 0.73) return 17;
if(p.equals("PP22366")) return 40;
if(p.equals("PP22367"))
 if(t <= 1.01) return 60;
 if(t > 1.01)
  if(t <= 1.3) return 25;
  if(t > 1.3) return 100;
if(p.equals("PP22380"))
 if(w <= 190)
  if(we <= 900) return 5;
  if(we > 900)
   if(we <= 1282) return 50;
   if(we > 1282) return 5;
 if(w > 190) return 40;
if(p.equals("PP22386")) return 40;
if(p.equals("PP22389")) return 35;
if(p.equals("PP22390"))
 if(we <= 804) return 25;
 if(we > 804)
  if(we <= 1502) return 20;
  if(we > 1502) return 15;
if(p.equals("PP22404")) return 30;
if(p.equals("PP22406")) return 30;
if(p.equals("PP22408"))
 if(t <= 0.458)
  if(w <= 159) return 30;
  if(w > 159)
   if(w <= 199) return 20;
   if(w > 199) return 17.5;
 if(t > 0.458)
  if(w <= 277) return 50;
  if(w > 277) return 30;
if(p.equals("PP22409"))
 if(w <= 293)
  if(we <= 1536)
   if(we <= 1520) return 10;
   if(we > 1520) return 20;
  if(we > 1536) return 15;
 if(w > 293) return 20;
if(p.equals("PP22410"))
 if(t <= 0.73)
  if(w <= 180) return 20;
  if(w > 180) return 10;
 if(t > 0.73) return 50;
if(p.equals("PP22416"))
 if(w <= 246)
  if(t <= 1.82)
   if(we <= 1806) return 40;
   if(we > 1806) return 30;
  if(t > 1.82)
   if(we <= 1048) return 30;
   if(we > 1048) return 25;
 if(w > 246) return 45;
if(p.equals("PP22417"))
 if(w <= 294)
  if(we <= 710)
   if(we <= 672)
    if(we <= 647) return 30;
    if(we > 647)
     if(we <= 650) return 19;
     if(we > 650)
      if(we <= 660) return 35;
      if(we > 660) return 27.5;
   if(we > 672)
    if(we <= 696) return 30;
    if(we > 696) return 22.5;
  if(we > 710)
   if(t <= 1.79)
    if(t <= 1.4)
     if(we <= 2140) return 40;
     if(we > 2140) return 20;
    if(t > 1.4)
     if(we <= 1437) return 50;
     if(we > 1437) return 30;
   if(t > 1.79)
    if(we <= 2945)
     if(w <= 77)
      if(we <= 714) return 25;
      if(we > 714) return 60;
     if(w > 77)
      if(we <= 2735) return 25;
      if(we > 2735) return 40;
    if(we > 2945)
     if(t <= 2) return 25;
     if(t > 2)
      if(w <= 264)
       if(we <= 3134) return 25;
       if(we > 3134) return 30;
      if(w > 264) return 30;
 if(w > 294) return 5;
if(p.equals("PP22425")) return 30;
if(p.equals("PP22431")) return 25;
if(p.equals("PP22438")) return 46;
if(p.equals("PP22448"))
 if(t <= 1.65) return 50;
 if(t > 1.65) return 60;
if(p.equals("PP22457")) return 95;
if(p.equals("PP22461")) return 70;
if(p.equals("PP22464")) return 30;
if(p.equals("PP22468")) return 30;
if(p.equals("PP22469"))
 if(we <= 1252) return 10;
 if(we > 1252) return 15;
if(p.equals("PP22470")) return 50;
if(p.equals("PP22471"))
 if(we <= 3105)
  if(we <= 615) return 15;
  if(we > 615)
   if(we <= 706) return 25;
   if(we > 706)
    if(we <= 713) return 35;
    if(we > 713) return 30;
 if(we > 3105) return 20;
if(p.equals("PP22481"))
 if(t <= 1.22)
  if(we <= 2077) return 70;
  if(we > 2077)
   if(we <= 2098) return 110;
   if(we > 2098) return 15;
 if(t > 1.22)
  if(we <= 1488) return 35;
  if(we > 1488) return 30;
if(p.equals("PP22482"))
 if(w <= 202) return 30;
 if(w > 202)
  if(we <= 1488) return 23;
  if(we > 1488) return 25;
if(p.equals("PP22484")) return 20;
if(p.equals("PP22485")) return 15;
if(p.equals("PP22487")) return 30;
if(p.equals("PP22488"))
 if(t <= 0.9)
  if(we <= 1766) return 20;
  if(we > 1766) return 15;
 if(t > 0.9) return 25;
if(p.equals("PP22489"))
 if(we <= 746) return 35;
 if(we > 746) return 20;
if(p.equals("PP22490")) return 80;
if(p.equals("PP22491")) return 35;
if(p.equals("PP22496")) return 20;
if(p.equals("PP22498"))
 if(we <= 516)
  if(we <= 511) return 20;
  if(we > 511) return 30;
 if(we > 516) return 15;
if(p.equals("PP22505"))
 if(t <= 1.6)
  if(we <= 1908)
   if(we <= 1872) return 35;
   if(we > 1872) return 30;
  if(we > 1908) return 35;
 if(t > 1.6)
  if(t <= 1.675) return 20;
  if(t > 1.675)
   if(we <= 1030)
    if(we <= 993)
     if(we <= 920) return 40;
     if(we > 920)
      if(we <= 965) return 30;
      if(we > 965) return 27.5;
    if(we > 993)
     if(we <= 1012) return 25;
     if(we > 1012) return 45;
   if(we > 1030)
    if(we <= 1070) return 30;
    if(we > 1070)
     if(we <= 1164)
      if(we <= 1094) return 40;
      if(we > 1094) return 20;
     if(we > 1164) return 30;
if(p.equals("PP22507"))
 if(t <= 1.63) return 35;
 if(t > 1.63) return 40;
if(p.equals("PP22508"))
 if(t <= 2.03) return 40;
 if(t > 2.03)
  if(t <= 2.33) return 60;
  if(t > 2.33)
   if(t <= 2.36) return 25;
   if(t > 2.36) return 30;
if(p.equals("PP22509"))
 if(t <= 0.825) return 120;
 if(t > 0.825) return 45;
if(p.equals("PP22510")) return 20;
if(p.equals("PP22512")) return 75;
if(p.equals("PP22514")) return 35;
if(p.equals("PP22515"))
 if(t <= 1.83)
  if(t <= 1.65) return 40;
  if(t > 1.65)
   if(w <= 178) return 25;
   if(w > 178) return 20;
 if(t > 1.83) return 30;
if(p.equals("PP22520")) return 15;
if(p.equals("PP22522")) return 40;
if(p.equals("PP22531"))
 if(t <= 2.52)
  if(we <= 2118)
   if(t <= 2.48) return 20;
   if(t > 2.48)
    if(we <= 2056) return 40;
    if(we > 2056)
     if(t <= 2.505) return 20;
     if(t > 2.505) return 27.5;
  if(we > 2118)
   if(we <= 2319) return 20;
   if(we > 2319) return 15;
 if(t > 2.52)
  if(we <= 2025)
   if(we <= 1948) return 15;
   if(we > 1948) return 10;
  if(we > 2025)
   if(t <= 2.88)
    if(t <= 2.84)
     if(t <= 2.68)
      if(t <= 2.58) return 25;
      if(t > 2.58) return 50;
     if(t > 2.68) return 35;
    if(t > 2.84)
     if(w <= 252.5) return 45;
     if(w > 252.5) return 30;
   if(t > 2.88) return 25;
if(p.equals("PP22534")) return 15;
if(p.equals("PP22540")) return 35;
if(p.equals("PP22542")) return 20;
if(p.equals("PP22553")) return 70;
if(p.equals("PP22570")) return 40;
if(p.equals("PP22571")) return 25;
if(p.equals("PP22574")) return 30;
if(p.equals("PP22581"))
 if(we <= 3030) return 20;
 if(we > 3030) return 30;
if(p.equals("PP22586"))
 if(we <= 1216) return 25;
 if(we > 1216) return 15;
if(p.equals("PP22595")) return 10;
if(p.equals("PP22600")) return 30;
if(p.equals("WS21265")) return 30;
if(p.equals("WSA1253")) return 20;
return 20.0;
}
}
