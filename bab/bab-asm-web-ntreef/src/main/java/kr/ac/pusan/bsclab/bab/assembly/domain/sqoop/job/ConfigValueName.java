package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job;

public enum ConfigValueName {
	
	fromJobConfig, toJobConfig, throttlingConfig

}
