package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SS2 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("OW11009")) return 50;
if(p.equals("PP11013")) return 50;
if(p.equals("PP11015"))
 if(we <= 1399) return 55;
 if(we > 1399) return 40;
if(p.equals("PP11037"))
 if(w <= 130)
  if(we <= 172)
   if(w <= 23.3)
    if(we <= 119)
     if(we <= 106) return 70;
     if(we > 106) return 100;
    if(we > 119)
     if(w <= 18.1) return 70;
     if(w > 18.1)
      if(we <= 135) return 50;
      if(we > 135) return 56;
   if(w > 23.3)
    if(w <= 58)
     if(t <= 1.13)
      if(we <= 154)
       if(we <= 144) return 78;
       if(we > 144) return 50;
      if(we > 154) return 65;
     if(t > 1.13)
      if(we <= 159) return 50;
      if(we > 159) return 55.7166666666667;
    if(w > 58)
     if(we <= 114)
      if(we <= 111)
       if(we <= 101) return 50;
       if(we > 101) return 100;
      if(we > 111) return 72;
     if(we > 114)
      if(we <= 121)
       if(we <= 117) return 64;
       if(we > 117) return 75.55;
      if(we > 121)
       if(we <= 125) return 70;
       if(we > 125) return 50;
  if(we > 172)
   if(w <= 28.7)
    if(we <= 190)
     if(we <= 186) return 60;
     if(we > 186) return 52.65;
    if(we > 190)
     if(t <= 1.13)
      if(we <= 199)
       if(we <= 195) return 82.75;
       if(we > 195) return 70;
      if(we > 199)
       if(we <= 204) return 70.45;
       if(we > 204) return 60;
     if(t > 1.13) return 80;
   if(w > 28.7)
    if(t <= 1.13)
     if(we <= 669) return 60;
     if(we > 669) return 110;
    if(t > 1.13) return 110;
 if(w > 130)
  if(w <= 357.5)
   if(w <= 273) return 50;
   if(w > 273)
    if(we <= 1926) return 55;
    if(we > 1926)
     if(we <= 2086) return 50;
     if(we > 2086) return 55;
  if(w > 357.5)
   if(w <= 380)
    if(we <= 2359) return 30;
    if(we > 2359) return 37.5;
   if(w > 380) return 50;
if(p.equals("PP11039"))
 if(we <= 1061)
  if(w <= 59)
   if(we <= 266) return 80;
   if(we > 266) return 90;
  if(w > 59)
   if(t <= 0.895)
    if(w <= 121.1)
     if(t <= 0.71) return 60;
     if(t > 0.71) return 55;
    if(w > 121.1) return 60;
   if(t > 0.895)
    if(t <= 0.9) return 80;
    if(t > 0.9) return 55;
 if(we > 1061)
  if(t <= 0.9) return 50;
  if(t > 0.9)
   if(we <= 1206) return 60;
   if(we > 1206) return 50;
if(p.equals("PP11041"))
 if(t <= 0.45) return 90;
 if(t > 0.45)
  if(t <= 0.505) return 100;
  if(t > 0.505) return 115;
if(p.equals("PP11046"))
 if(w <= 410)
  if(t <= 0.9)
   if(we <= 1776)
    if(w <= 238)
     if(w <= 197) return 40;
     if(w > 197) return 60;
    if(w > 238)
     if(we <= 1756)
      if(we <= 1684)
       if(we <= 1626) return 40;
       if(we > 1626) return 50;
      if(we > 1684)
       if(we <= 1704) return 42.5;
       if(we > 1704)
        if(we <= 1726) return 50;
        if(we > 1726) return 40;
     if(we > 1756) return 50;
   if(we > 1776)
    if(we <= 1886)
     if(we <= 1791) return 46.6666666666667;
     if(we > 1791) return 50;
    if(we > 1886)
     if(we <= 2299) return 40;
     if(we > 2299) return 50;
  if(t > 0.9)
   if(t <= 1.13)
    if(w <= 310) return 55;
    if(w > 310) return 60;
   if(t > 1.13) return 50;
 if(w > 410)
  if(we <= 2014)
   if(we <= 1977) return 50;
   if(we > 1977) return 36.6666666666667;
  if(we > 2014)
   if(we <= 2185)
    if(we <= 2050) return 90;
    if(we > 2050) return 50;
   if(we > 2185)
    if(we <= 2376) return 40;
    if(we > 2376) return 70;
if(p.equals("PP11048"))
 if(we <= 1514)
  if(we <= 1234)
   if(w <= 197)
    if(we <= 1129) return 60;
    if(we > 1129) return 50;
   if(w > 197) return 60;
  if(we > 1234)
   if(we <= 1323) return 70;
   if(we > 1323) return 50;
 if(we > 1514)
  if(we <= 2086)
   if(we <= 1799)
    if(w <= 238) return 45;
    if(w > 238)
     if(w <= 273)
      if(w <= 262) return 55;
      if(w > 262) return 50;
     if(w > 273) return 55;
   if(we > 1799) return 45;
  if(we > 2086) return 50;
if(p.equals("PP11052")) return 45;
if(p.equals("PP11058"))
 if(w <= 273)
  if(w <= 189)
   if(w <= 183.5)
    if(we <= 1000)
     if(we <= 945)
      if(we <= 883) return 45;
      if(we > 883)
       if(we <= 925) return 60;
       if(we > 925) return 40;
     if(we > 945) return 75;
    if(we > 1000)
     if(w <= 172)
      if(we <= 1049) return 50;
      if(we > 1049)
       if(we <= 1067) return 60;
       if(we > 1067)
        if(we <= 1145) return 50;
        if(we > 1145) return 60;
     if(w > 172)
      if(we <= 1202) return 60;
      if(we > 1202)
       if(we <= 1275)
        if(we <= 1227) return 50;
        if(we > 1227)
         if(we <= 1241) return 60;
         if(we > 1241) return 50;
       if(we > 1275) return 60;
   if(w > 183.5)
    if(we <= 1194)
     if(we <= 1136)
      if(we <= 1015) return 45;
      if(we > 1015) return 50;
     if(we > 1136) return 60;
    if(we > 1194)
     if(we <= 1305)
      if(we <= 1267)
       if(we <= 1212) return 55;
       if(we > 1212) return 50;
      if(we > 1267)
       if(we <= 1275) return 51.5833333333333;
       if(we > 1275) return 60;
     if(we > 1305)
      if(we <= 1377)
       if(we <= 1358) return 55;
       if(we > 1358) return 60;
      if(we > 1377) return 55;
  if(w > 189)
   if(t <= 0.81)
    if(we <= 1277)
     if(we <= 1177)
      if(w <= 197)
       if(w <= 194) return 60;
       if(w > 194) return 50;
      if(w > 197)
       if(we <= 1092) return 40;
       if(we > 1092) return 55;
     if(we > 1177) return 50;
    if(we > 1277)
     if(t <= 0.76)
      if(w <= 267)
       if(we <= 1677) return 60;
       if(we > 1677) return 50;
      if(w > 267) return 50;
     if(t > 0.76)
      if(we <= 1369)
       if(w <= 197)
        if(we <= 1289) return 56.6666666666667;
        if(we > 1289)
         if(we <= 1359) return 50;
         if(we > 1359) return 60;
       if(w > 197)
        if(we <= 1305) return 50;
        if(we > 1305)
         if(we <= 1343) return 60;
         if(we > 1343) return 40;
      if(we > 1369) return 50;
   if(t > 0.81) return 55;
 if(w > 273)
  if(we <= 2182)
   if(w <= 305.7) return 50;
   if(w > 305.7)
    if(t <= 0.795)
     if(we <= 2021) return 30;
     if(we > 2021)
      if(we <= 2103)
       if(we <= 2074) return 50;
       if(we > 2074) return 30;
      if(we > 2103)
       if(we <= 2141) return 52.5;
       if(we > 2141)
        if(we <= 2155) return 40;
        if(we > 2155) return 43.75;
    if(t > 0.795)
     if(t <= 1.01) return 50;
     if(t > 1.01) return 60;
  if(we > 2182)
   if(w <= 322.5) return 50;
   if(w > 322.5)
    if(w <= 340)
     if(we <= 2424) return 50;
     if(we > 2424)
      if(w <= 325)
       if(we <= 2447) return 55;
       if(we > 2447)
        if(we <= 2514) return 52.5;
        if(we > 2514) return 50;
      if(w > 325) return 60;
    if(w > 340)
     if(we <= 2641) return 50;
     if(we > 2641)
      if(t <= 0.895)
       if(we <= 2791)
        if(we <= 2684) return 50.8333333333333;
        if(we > 2684)
         if(we <= 2733) return 52.5;
         if(we > 2733) return 50;
       if(we > 2791) return 55;
      if(t > 0.895) return 40;
if(p.equals("PP11060")) return 40;
if(p.equals("PP11081"))
 if(w <= 347)
  if(t <= 0.71)
   if(w <= 242) return 55;
   if(w > 242) return 60;
  if(t > 0.71)
   if(t <= 0.9)
    if(we <= 1457)
     if(w <= 238) return 45;
     if(w > 238) return 50;
    if(we > 1457)
     if(we <= 1802)
      if(we <= 1600) return 55;
      if(we > 1600)
       if(we <= 1671) return 60;
       if(we > 1671) return 55;
     if(we > 1802)
      if(w <= 287.3) return 45;
      if(w > 287.3) return 50;
   if(t > 0.9) return 50;
 if(w > 347)
  if(w <= 380)
   if(we <= 2188) return 85;
   if(we > 2188) return 40;
  if(w > 380) return 55;
if(p.equals("PP11275")) return 50;
if(p.equals("PP11683"))
 if(we <= 449)
  if(we <= 395)
   if(w <= 54)
    if(w <= 45) return 115;
    if(w > 45)
     if(we <= 308)
      if(w <= 52.5)
       if(we <= 270) return 100;
       if(we > 270) return 80;
      if(w > 52.5) return 100;
     if(we > 308) return 80;
   if(w > 54)
    if(t <= 0.4)
     if(we <= 336) return 100;
     if(we > 336)
      if(w <= 70) return 55;
      if(w > 70)
       if(w <= 73) return 100;
       if(w > 73) return 50;
    if(t > 0.4)
     if(we <= 332) return 42.5;
     if(we > 332) return 100;
  if(we > 395)
   if(w <= 81)
    if(w <= 70)
     if(we <= 411) return 40;
     if(we > 411)
      if(w <= 65) return 80;
      if(w > 65) return 70;
    if(w > 70)
     if(w <= 73)
      if(we <= 422) return 50.7166666666667;
      if(we > 422) return 125;
     if(w > 73) return 120;
   if(w > 81)
    if(we <= 411) return 50;
    if(we > 411) return 100;
 if(we > 449)
  if(w <= 80)
   if(we <= 472)
    if(we <= 460) return 30;
    if(we > 460) return 55;
   if(we > 472)
    if(we <= 482) return 60;
    if(we > 482) return 130;
  if(w > 80)
   if(w <= 87)
    if(we <= 555)
     if(we <= 486)
      if(we <= 472) return 40;
      if(we > 472) return 42.5;
     if(we > 486) return 60;
    if(we > 555)
     if(we <= 603)
      if(t <= 0.4) return 40;
      if(t > 0.4)
       if(we <= 584) return 50;
       if(we > 584) return 40;
     if(we > 603) return 60;
   if(w > 87) return 115;
if(p.equals("PP11686")) return 55;
if(p.equals("PP11931"))
 if(we <= 417)
  if(we <= 332) return 65;
  if(we > 332) return 70;
 if(we > 417) return 50;
if(p.equals("PP11932"))
 if(t <= 1)
  if(we <= 222)
   if(we <= 182) return 135;
   if(we > 182) return 60;
  if(we > 222)
   if(t <= 0.81)
    if(we <= 332) return 50;
    if(we > 332) return 55;
   if(t > 0.81) return 60;
 if(t > 1)
  if(t <= 1.195)
   if(we <= 441)
    if(t <= 1.13) return 40;
    if(t > 1.13)
     if(we <= 281) return 55;
     if(we > 281)
      if(we <= 422)
       if(we <= 369)
        if(we <= 338) return 50;
        if(we > 338) return 55;
       if(we > 369)
        if(we <= 380) return 60;
        if(we > 380) return 50;
      if(we > 422)
       if(we <= 434) return 75;
       if(we > 434) return 45;
   if(we > 441) return 50;
  if(t > 1.195)
   if(we <= 455)
    if(w <= 31.5)
     if(we <= 411) return 50;
     if(we > 411)
      if(we <= 431) return 70;
      if(we > 431)
       if(we <= 448) return 58;
       if(we > 448) return 40;
    if(w > 31.5)
     if(we <= 413) return 40;
     if(we > 413)
      if(we <= 443) return 55;
      if(we > 443) return 45;
   if(we > 455)
    if(w <= 31.5)
     if(we <= 460) return 53.8833333333333;
     if(we > 460) return 55;
    if(w > 31.5) return 50;
if(p.equals("PP11991")) return 50;
if(p.equals("PP12002"))
 if(we <= 194)
  if(we <= 169) return 80.2333333333333;
  if(we > 169) return 80;
 if(we > 194) return 60;
if(p.equals("PP12047"))
 if(t <= 1.1)
  if(we <= 198) return 70;
  if(we > 198) return 65;
 if(t > 1.1)
  if(we <= 216) return 60;
  if(we > 216) return 72.5;
if(p.equals("PP12095"))
 if(t <= 0.795) return 20;
 if(t > 0.795) return 50;
if(p.equals("PP12226")) return 50;
if(p.equals("PP12239"))
 if(we <= 1377) return 42.5;
 if(we > 1377)
  if(w <= 221.2) return 50;
  if(w > 221.2)
   if(we <= 1645) return 40;
   if(we > 1645) return 55;
if(p.equals("PP12269"))
 if(we <= 1558) return 80;
 if(we > 1558) return 100;
if(p.equals("PP12289")) return 60;
if(p.equals("PP12413"))
 if(w <= 305.7) return 30;
 if(w > 305.7) return 45;
if(p.equals("PP12445"))
 if(w <= 313.9) return 55;
 if(w > 313.9)
  if(w <= 322.5)
   if(we <= 1969)
    if(we <= 1945) return 45;
    if(we > 1945) return 40;
   if(we > 1969) return 50;
  if(w > 322.5) return 50;
if(p.equals("PP12472")) return 60;
if(p.equals("PP12485")) return 65;
if(p.equals("PP21028")) return 47.5;
if(p.equals("PP21039"))
 if(w <= 238)
  if(we <= 1584) return 55;
  if(we > 1584) return 50;
 if(w > 238)
  if(we <= 1467) return 55;
  if(we > 1467) return 40;
if(p.equals("PP21090")) return 50;
if(p.equals("PP21092")) return 30;
if(p.equals("PP21185"))
 if(we <= 372)
  if(we <= 360) return 82.5;
  if(we > 360) return 70;
 if(we > 372) return 60;
if(p.equals("PP21187"))
 if(w <= 111.1)
  if(we <= 179) return 70;
  if(we > 179)
   if(we <= 184) return 65;
   if(we > 184) return 57.4333333333333;
 if(w > 111.1)
  if(w <= 250) return 42.5;
  if(w > 250) return 40;
if(p.equals("PP21292"))
 if(w <= 292.7)
  if(we <= 1579)
   if(w <= 224) return 55;
   if(w > 224) return 50;
  if(we > 1579) return 55;
 if(w > 292.7)
  if(w <= 378) return 60;
  if(w > 378) return 50;
if(p.equals("PP21330")) return 40;
if(p.equals("PP21423")) return 55;
if(p.equals("PP21529")) return 50;
if(p.equals("PP21702"))
 if(we <= 1603) return 30;
 if(we > 1603) return 40;
if(p.equals("PP21773")) return 40;
if(p.equals("PP21789")) return 40;
if(p.equals("PP21802"))
 if(we <= 116)
  if(we <= 84) return 120;
  if(we > 84)
   if(we <= 93) return 50;
   if(we > 93) return 45.3833333333333;
 if(we > 116)
  if(we <= 123) return 42.5;
  if(we > 123)
   if(we <= 130) return 50;
   if(we > 130) return 40;
if(p.equals("PP21804")) return 105;
if(p.equals("PP21814")) return 70;
if(p.equals("PP22107")) return 60;
if(p.equals("PP22272"))
 if(we <= 135) return 55;
 if(we > 135) return 58.3333333333333;
if(p.equals("PP22325")) return 50;
if(p.equals("PP22326")) return 60;
if(p.equals("PP22412")) return 85;
if(p.equals("PP22413")) return 80;
if(p.equals("PP22423"))
 if(t <= 1.13) return 70;
 if(t > 1.13) return 75;
if(p.equals("PP22459"))
 if(we <= 177) return 70;
 if(we > 177) return 80;
if(p.equals("WS21007"))
 if(w <= 172)
  if(t <= 0.8)
   if(w <= 65) return 80;
   if(w > 65) return 85;
  if(t > 0.8) return 60;
 if(w > 172)
  if(t <= 0.9)
   if(t <= 0.71) return 55;
   if(t > 0.71)
    if(w <= 273)
     if(we <= 2095)
      if(we <= 1915) return 45;
      if(we > 1915) return 50;
     if(we > 2095) return 45;
    if(w > 273) return 50;
  if(t > 0.9) return 35;
if(p.equals("WS31014"))
 if(w <= 197)
  if(we <= 477) return 50;
  if(we > 477) return 40;
 if(w > 197) return 80;
return 80.0;
}
}
