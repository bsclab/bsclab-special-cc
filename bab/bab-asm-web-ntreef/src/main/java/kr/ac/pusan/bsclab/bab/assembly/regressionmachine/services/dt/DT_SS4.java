package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SS4 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("OW11009")) return 40;
if(p.equals("PP11036"))
 if(t <= 1.3)
  if(t <= 1.2) return 60;
  if(t > 1.2) return 35;
 if(t > 1.3)
  if(we <= 1212) return 50;
  if(we > 1212) return 40;
if(p.equals("PP11037")) return 45;
if(p.equals("PP11045")) return 40;
if(p.equals("PP11058"))
 if(w <= 313.8)
  if(we <= 1903)
   if(w <= 248)
    if(we <= 1120) return 50;
    if(we > 1120) return 60;
   if(w > 248) return 50;
  if(we > 1903)
   if(we <= 2115)
    if(we <= 1958) return 55;
    if(we > 1958) return 40;
   if(we > 2115) return 35;
 if(w > 313.8)
  if(we <= 2137)
   if(w <= 322.5)
    if(we <= 2027)
     if(we <= 1070) return 50;
     if(we > 1070) return 40;
    if(we > 2027)
     if(we <= 2080) return 50;
     if(we > 2080) return 40;
   if(w > 322.5)
    if(we <= 2065) return 50;
    if(we > 2065) return 45;
  if(we > 2137)
   if(w <= 322.5) return 45;
   if(w > 322.5)
    if(w <= 330)
     if(we <= 2203) return 46.6666666666667;
     if(we > 2203) return 50;
    if(w > 330)
     if(we <= 2243) return 50;
     if(we > 2243) return 45;
if(p.equals("PP11683")) return 45;
if(p.equals("PP11884")) return 50;
if(p.equals("PP11932")) return 30;
if(p.equals("PP11982"))
 if(we <= 636)
  if(t <= 1.1)
   if(we <= 540)
    if(we <= 480) return 45;
    if(we > 480) return 50;
   if(we > 540)
    if(we <= 571)
     if(we <= 553) return 43.0833333333333;
     if(we > 553) return 40;
    if(we > 571) return 45;
  if(t > 1.1)
   if(we <= 565)
    if(we <= 520)
     if(we <= 424) return 45;
     if(we > 424) return 25;
    if(we > 520) return 45;
   if(we > 565)
    if(we <= 601)
     if(we <= 588) return 30;
     if(we > 588) return 37.5;
    if(we > 601) return 50;
 if(we > 636)
  if(we <= 673)
   if(we <= 662) return 30;
   if(we > 662)
    if(we <= 668) return 37;
    if(we > 668) return 45;
  if(we > 673)
   if(w <= 594)
    if(w <= 71.4) return 50;
    if(w > 71.4) return 40;
   if(w > 594) return 40;
if(p.equals("PP11984"))
 if(t <= 1.63)
  if(t <= 1.3) return 50;
  if(t > 1.3) return 45;
 if(t > 1.63)
  if(t <= 1.795) return 25;
  if(t > 1.795) return 40;
if(p.equals("PP11985")) return 40;
if(p.equals("PP11988")) return 90;
if(p.equals("PP11991")) return 50;
if(p.equals("PP12002"))
 if(we <= 198)
  if(we <= 188) return 46.6666666666667;
  if(we > 188) return 55;
 if(we > 198) return 45;
if(p.equals("PP12011")) return 35;
if(p.equals("PP12026"))
 if(t <= 1.7) return 50;
 if(t > 1.7)
  if(we <= 1807) return 20;
  if(we > 1807) return 35;
if(p.equals("PP12035"))
 if(t <= 0.86) return 60;
 if(t > 0.86)
  if(w <= 77) return 30;
  if(w > 77)
   if(w <= 95.5) return 70;
   if(w > 95.5) return 50;
if(p.equals("PP12037"))
 if(t <= 0.975) return 55;
 if(t > 0.975) return 60;
if(p.equals("PP12047"))
 if(w <= 28)
  if(we <= 185)
   if(we <= 167) return 55;
   if(we > 167) return 40;
  if(we > 185) return 45;
 if(w > 28)
  if(w <= 82) return 60;
  if(w > 82) return 70;
if(p.equals("PP12095"))
 if(w <= 302.6)
  if(w <= 171.8)
   if(t <= 0.795) return 45;
   if(t > 0.795)
    if(we <= 1086) return 55;
    if(we > 1086)
     if(we <= 1096) return 54.3666666666667;
     if(we > 1096)
      if(we <= 1160) return 60;
      if(we > 1160) return 35;
  if(w > 171.8)
   if(w <= 183.5)
    if(we <= 1145)
     if(we <= 1120) return 40;
     if(we > 1120) return 41.6666666666667;
    if(we > 1145)
     if(we <= 1223)
      if(we <= 1200)
       if(we <= 1174)
        if(we <= 1156) return 35;
        if(we > 1156) return 60;
       if(we > 1174) return 52;
      if(we > 1200)
       if(we <= 1213) return 40;
       if(we > 1213) return 35;
     if(we > 1223)
      if(we <= 1264) return 46;
      if(we > 1264) return 40;
   if(w > 183.5)
    if(we <= 1245)
     if(w <= 189) return 50;
     if(w > 189)
      if(we <= 1160) return 50;
      if(we > 1160) return 55;
    if(we > 1245)
     if(we <= 1308)
      if(w <= 189)
       if(t <= 0.795) return 60;
       if(t > 0.795)
        if(we <= 1260) return 50;
        if(we > 1260) return 70;
      if(w > 189)
       if(we <= 1260) return 41.6666666666667;
       if(we > 1260)
        if(we <= 1283) return 50;
        if(we > 1283) return 52.5;
     if(we > 1308)
      if(w <= 189) return 35;
      if(w > 189) return 50;
 if(w > 302.6)
  if(we <= 2167)
   if(w <= 322.5)
    if(w <= 312)
     if(we <= 1948)
      if(we <= 1937) return 40;
      if(we > 1937) return 47.5;
     if(we > 1948)
      if(we <= 2042) return 60;
      if(we > 2042)
       if(we <= 2104) return 53.1833333333333;
       if(we > 2104)
        if(we <= 2143) return 40;
        if(we > 2143) return 45;
    if(w > 312)
     if(we <= 2101)
      if(t <= 0.795) return 50;
      if(t > 0.795)
       if(we <= 1999) return 50;
       if(we > 1999) return 40;
     if(we > 2101) return 60;
   if(w > 322.5)
    if(we <= 2116)
     if(w <= 336.5)
      if(we <= 2072)
       if(w <= 330)
        if(we <= 1400) return 40;
        if(we > 1400)
         if(t <= 0.6975) return 50;
         if(t > 0.6975)
          if(w <= 327)
           if(t <= 1.2) return 37.5;
           if(t > 1.2) return 40;
          if(w > 327) return 40;
       if(w > 330) return 45;
      if(we > 2072) return 42.5;
     if(w > 336.5) return 40;
    if(we > 2116)
     if(w <= 350)
      if(we <= 2162)
       if(w <= 323.6) return 45;
       if(w > 323.6)
        if(w <= 336.5) return 50;
        if(w > 336.5) return 45;
      if(we > 2162) return 37.5;
     if(w > 350) return 40;
  if(we > 2167)
   if(w <= 330)
    if(we <= 2297)
     if(w <= 322.5)
      if(w <= 320)
       if(t <= 0.795)
        if(we <= 2278)
         if(we <= 2199) return 45;
         if(we > 2199)
          if(we <= 2230) return 40;
          if(we > 2230)
           if(we <= 2247) return 60;
           if(we > 2247) return 40;
        if(we > 2278) return 41.6666666666667;
       if(t > 0.795)
        if(we <= 2222)
         if(we <= 2206) return 40;
         if(we > 2206) return 45;
        if(we > 2222)
         if(we <= 2234) return 50;
         if(we > 2234) return 40;
      if(w > 320)
       if(we <= 2229)
        if(we <= 2192) return 45;
        if(we > 2192) return 40;
       if(we > 2229) return 48.75;
     if(w > 322.5)
      if(we <= 2283) return 50;
      if(we > 2283) return 49;
    if(we > 2297)
     if(we <= 2333)
      if(t <= 0.795) return 43;
      if(t > 0.795)
       if(we <= 2314) return 45.25;
       if(we > 2314) return 47.5;
     if(we > 2333) return 45;
   if(w > 330)
    if(w <= 350)
     if(w <= 336.5)
      if(we <= 2286)
       if(we <= 2227) return 40;
       if(we > 2227)
        if(we <= 2260) return 47.5;
        if(we > 2260) return 41.25;
      if(we > 2286)
       if(we <= 2357)
        if(we <= 2311) return 46.8833333333333;
        if(we > 2311) return 45;
       if(we > 2357)
        if(we <= 2418) return 40;
        if(we > 2418) return 50;
     if(w > 336.5)
      if(we <= 2374)
       if(we <= 2293) return 50;
       if(we > 2293)
        if(we <= 2319) return 47.5;
        if(we > 2319)
         if(we <= 2359) return 45;
         if(we > 2359) return 47.5;
      if(we > 2374)
       if(we <= 2473)
        if(we <= 2396) return 40;
        if(we > 2396) return 50;
       if(we > 2473) return 45;
    if(w > 350)
     if(we <= 2635)
      if(we <= 2458)
       if(we <= 2359)
        if(we <= 2339) return 50;
        if(we > 2339) return 55;
       if(we > 2359)
        if(we <= 2434) return 40;
        if(we > 2434) return 49;
      if(we > 2458)
       if(we <= 2505)
        if(we <= 2477) return 43.75;
        if(we > 2477) return 45.55;
       if(we > 2505)
        if(we <= 2561) return 40;
        if(we > 2561)
         if(we <= 2606) return 50;
         if(we > 2606) return 40;
     if(we > 2635)
      if(we <= 2787) return 45;
      if(we > 2787) return 55;
if(p.equals("PP12129")) return 50;
if(p.equals("PP12141")) return 45;
if(p.equals("PP12146"))
 if(t <= 0.9)
  if(w <= 230) return 65;
  if(w > 230)
   if(t <= 0.76) return 50;
   if(t > 0.76) return 60;
 if(t > 0.9)
  if(t <= 1.1)
   if(we <= 1827)
    if(w <= 309.5) return 75;
    if(w > 309.5) return 50;
   if(we > 1827) return 70;
  if(t > 1.1)
   if(t <= 1.595)
    if(we <= 2694) return 60;
    if(we > 2694) return 80;
   if(t > 1.595)
    if(t <= 1.91) return 90;
    if(t > 1.91)
     if(we <= 2640) return 100;
     if(we > 2640) return 90;
if(p.equals("PP12158"))
 if(we <= 671)
  if(w <= 85) return 40;
  if(w > 85) return 30;
 if(we > 671) return 50;
if(p.equals("PP12178"))
 if(we <= 1693)
  if(we <= 1500) return 40;
  if(we > 1500)
   if(we <= 1580)
    if(we <= 1520) return 52.5;
    if(we > 1520)
     if(we <= 1533) return 60;
     if(we > 1533) return 42.85;
   if(we > 1580)
    if(w <= 337) return 40;
    if(w > 337)
     if(we <= 1622) return 40;
     if(we > 1622) return 45;
 if(we > 1693)
  if(w <= 337) return 50;
  if(w > 337)
   if(we <= 1811) return 42.5;
   if(we > 1811) return 35;
if(p.equals("PP12231"))
 if(t <= 1.61)
  if(t <= 1.21) return 135;
  if(t > 1.21)
   if(we <= 2092) return 80;
   if(we > 2092) return 75;
 if(t > 1.61)
  if(w <= 339.8)
   if(w <= 254) return 60;
   if(w > 254) return 50;
  if(w > 339.8) return 60;
if(p.equals("PP12233")) return 60;
if(p.equals("PP12315")) return 60;
if(p.equals("PP12384")) return 40;
if(p.equals("PP12394"))
 if(t <= 1.675) return 50;
 if(t > 1.675) return 25;
if(p.equals("PP12401"))
 if(we <= 716) return 45;
 if(we > 716)
  if(we <= 843) return 60;
  if(we > 843) return 90;
if(p.equals("PP12403"))
 if(w <= 85)
  if(w <= 59) return 70;
  if(w > 59)
   if(w <= 73) return 60;
   if(w > 73) return 70;
 if(w > 85)
  if(we <= 738) return 60;
  if(we > 738)
   if(we <= 760) return 65;
   if(we > 760) return 40;
if(p.equals("PP12452"))
 if(t <= 2.015) return 50;
 if(t > 2.015) return 30;
if(p.equals("PP12483")) return 60;
if(p.equals("PP12536"))
 if(t <= 1.71)
  if(t <= 1.51) return 85;
  if(t > 1.51)
   if(t <= 1.58) return 75;
   if(t > 1.58) return 85;
 if(t > 1.71)
  if(t <= 1.78) return 90;
  if(t > 1.78) return 75;
if(p.equals("PP12537"))
 if(w <= 245) return 100;
 if(w > 245) return 70;
if(p.equals("PP12559")) return 43.3333333333333;
if(p.equals("PP12563")) return 60;
if(p.equals("PP21092")) return 25;
if(p.equals("PP21306")) return 20;
if(p.equals("PP21317"))
 if(we <= 1785) return 20;
 if(we > 1785) return 65;
if(p.equals("PP21320"))
 if(t <= 1.55) return 80;
 if(t > 1.55) return 50;
if(p.equals("PP21323"))
 if(t <= 2.015) return 70;
 if(t > 2.015) return 50;
if(p.equals("PP21328"))
 if(we <= 1283) return 70;
 if(we > 1283)
  if(we <= 1551) return 50;
  if(we > 1551) return 80;
if(p.equals("PP21330"))
 if(t <= 0.71) return 50;
 if(t > 0.71)
  if(we <= 456)
   if(we <= 440) return 50;
   if(we > 440) return 45;
  if(we > 456) return 55;
if(p.equals("PP21331")) return 80;
if(p.equals("PP21334"))
 if(w <= 276)
  if(t <= 1.57)
   if(we <= 2595) return 80;
   if(we > 2595) return 95;
  if(t > 1.57) return 85;
 if(w > 276)
  if(t <= 1.595) return 65;
  if(t > 1.595) return 50;
if(p.equals("PP21336"))
 if(w <= 106)
  if(we <= 456) return 60;
  if(we > 456)
   if(we <= 508) return 90;
   if(we > 508) return 53.3333333333333;
 if(w > 106)
  if(t <= 2.015)
   if(we <= 538) return 66.25;
   if(we > 538)
    if(we <= 567)
     if(w <= 147.5) return 55;
     if(w > 147.5) return 100;
    if(we > 567) return 40;
  if(t > 2.015) return 80;
if(p.equals("PP21345")) return 60;
if(p.equals("PP21394"))
 if(w <= 330)
  if(we <= 1638)
   if(w <= 184)
    if(t <= 1.61) return 85;
    if(t > 1.61)
     if(we <= 1516)
      if(w <= 166.7) return 85;
      if(w > 166.7) return 80;
     if(we > 1516)
      if(t <= 1.8)
       if(w <= 166.7) return 80;
       if(w > 166.7) return 90;
      if(t > 1.8)
       if(t <= 1.95) return 75;
       if(t > 1.95)
        if(we <= 1601) return 60;
        if(we > 1601) return 90;
   if(w > 184)
    if(t <= 1.9)
     if(we <= 1590)
      if(we <= 1407) return 60;
      if(we > 1407)
       if(we <= 1497) return 80;
       if(we > 1497)
        if(we <= 1543) return 60;
        if(we > 1543) return 69.2833333333333;
     if(we > 1590)
      if(we <= 1627) return 85;
      if(we > 1627) return 72.9;
    if(t > 1.9)
     if(w <= 282.1) return 85;
     if(w > 282.1) return 70;
  if(we > 1638)
   if(w <= 190.6)
    if(we <= 1720)
     if(t <= 1.725) return 65;
     if(t > 1.725)
      if(we <= 1679)
       if(t <= 1.825) return 60;
       if(t > 1.825) return 50;
      if(we > 1679)
       if(we <= 1703) return 66.6666666666667;
       if(we > 1703)
        if(we <= 1718) return 70;
        if(we > 1718) return 75;
    if(we > 1720)
     if(w <= 186)
      if(w <= 184.2) return 90;
      if(w > 184.2) return 60;
     if(w > 186)
      if(we <= 1795)
       if(we <= 1764)
        if(t <= 1.9) return 75;
        if(t > 1.9) return 85;
       if(we > 1764) return 80;
      if(we > 1795)
       if(we <= 1849) return 100;
       if(we > 1849)
        if(we <= 1874) return 85;
        if(we > 1874) return 90;
   if(w > 190.6)
    if(w <= 292)
     if(t <= 1.9)
      if(t <= 1.45)
       if(w <= 286) return 85;
       if(w > 286)
        if(we <= 2415) return 95;
        if(we > 2415) return 70;
      if(t > 1.45)
       if(w <= 197)
        if(we <= 1763) return 75;
        if(we > 1763)
         if(we <= 1835) return 80;
         if(we > 1835) return 70;
       if(w > 197)
        if(we <= 2405)
         if(we <= 2061)
          if(we <= 1975) return 80;
          if(we > 1975)
           if(we <= 2027) return 90;
           if(we > 2027) return 80;
         if(we > 2061)
          if(w <= 273)
           if(we <= 2384)
            if(w <= 260)
             if(t <= 1.7)
              if(we <= 2313) return 85;
              if(we > 2313) return 60;
             if(t > 1.7) return 60;
            if(w > 260)
             if(we <= 2326) return 60;
             if(we > 2326) return 70;
           if(we > 2384) return 82.5;
          if(w > 273) return 80;
        if(we > 2405)
         if(w <= 273)
          if(w <= 265) return 77.5;
          if(w > 265)
           if(we <= 2598) return 75;
           if(we > 2598) return 77.5;
         if(w > 273) return 80;
     if(t > 1.9) return 70;
    if(w > 292)
     if(we <= 2860)
      if(t <= 1.1) return 60;
      if(t > 1.1)
       if(w <= 325)
        if(w <= 302.6)
         if(we <= 2749)
          if(we <= 2654) return 75;
          if(we > 2654) return 65;
         if(we > 2749)
          if(we <= 2830) return 70;
          if(we > 2830) return 60;
        if(w > 302.6)
         if(we <= 2716)
          if(t <= 1.35) return 50;
          if(t > 1.35) return 70;
         if(we > 2716) return 75;
       if(w > 325) return 65;
     if(we > 2860)
      if(we <= 2979) return 90;
      if(we > 2979)
       if(w <= 322.5)
        if(t <= 1.7) return 77.5;
        if(t > 1.7) return 70;
       if(w > 322.5)
        if(we <= 3092)
         if(we <= 3000) return 80;
         if(we > 3000)
          if(we <= 3054) return 75;
          if(we > 3054) return 80;
        if(we > 3092) return 60;
 if(w > 330)
  if(w <= 339.8)
   if(we <= 2778)
    if(we <= 2655) return 45;
    if(we > 2655)
     if(we <= 2734) return 55;
     if(we > 2734) return 50;
   if(we > 2778)
    if(we <= 2837) return 45;
    if(we > 2837)
     if(we <= 3045) return 50;
     if(we > 3045) return 45;
  if(w > 339.8)
   if(we <= 3121) return 50;
   if(we > 3121) return 90;
if(p.equals("PP21398"))
 if(t <= 2.015)
  if(w <= 210)
   if(w <= 127.5)
    if(t <= 2.0075) return 60;
    if(t > 2.0075) return 70;
   if(w > 127.5)
    if(t <= 1.725)
     if(t <= 1.519) return 85;
     if(t > 1.519)
      if(t <= 1.63)
       if(w <= 141)
        if(t <= 1.58) return 75;
        if(t > 1.58) return 90;
       if(w > 141)
        if(we <= 1416)
         if(w <= 149) return 35;
         if(w > 149) return 70;
        if(we > 1416)
         if(we <= 1440) return 73.3333333333333;
         if(we > 1440) return 80;
      if(t > 1.63)
       if(w <= 190.6)
        if(we <= 1747) return 85;
        if(we > 1747) return 77.5;
       if(w > 190.6) return 70;
    if(t > 1.725)
     if(we <= 1246) return 70;
     if(we > 1246)
      if(t <= 1.91)
       if(w <= 190.6)
        if(t <= 1.825) return 60;
        if(t > 1.825)
         if(we <= 1785)
          if(we <= 1684) return 75;
          if(we > 1684) return 85;
         if(we > 1785) return 80;
       if(w > 190.6)
        if(we <= 1772) return 70;
        if(we > 1772) return 60;
      if(t > 1.91) return 70;
  if(w > 210)
   if(t <= 1.05)
    if(we <= 3469)
     if(we <= 3278) return 60;
     if(we > 3278) return 50;
    if(we > 3469) return 60;
   if(t > 1.05)
    if(t <= 1.91)
     if(w <= 240) return 30;
     if(w > 240) return 70;
    if(t > 1.91) return 110;
 if(t > 2.015)
  if(we <= 2481)
   if(we <= 1789)
    if(we <= 1626)
     if(we <= 1467) return 60;
     if(we > 1467)
      if(we <= 1602) return 70;
      if(we > 1602) return 60;
    if(we > 1626)
     if(we <= 1716)
      if(we <= 1693) return 80;
      if(we > 1693) return 71.7833333333333;
     if(we > 1716)
      if(we <= 1772)
       if(we <= 1751) return 70;
       if(we > 1751) return 74.45;
      if(we > 1772) return 60;
   if(we > 1789)
    if(w <= 251)
     if(we <= 1822)
      if(we <= 1803) return 75;
      if(we > 1803) return 73.3333333333333;
     if(we > 1822) return 70;
    if(w > 251)
     if(we <= 2275)
      if(we <= 2203)
       if(we <= 2003) return 40;
       if(we > 2003)
        if(we <= 2129) return 60;
        if(we > 2129) return 80;
      if(we > 2203) return 75;
     if(we > 2275)
      if(w <= 332)
       if(we <= 2406) return 55;
       if(we > 2406) return 100;
      if(w > 332) return 70;
  if(we > 2481)
   if(we <= 2655)
    if(w <= 353.4) return 30;
    if(w > 353.4) return 70;
   if(we > 2655)
    if(we <= 2808)
     if(we <= 2773)
      if(we <= 2691) return 25;
      if(we > 2691) return 80;
     if(we > 2773) return 25;
    if(we > 2808) return 80;
if(p.equals("PP21426")) return 70;
if(p.equals("PP21427")) return 70;
if(p.equals("PP21428")) return 90;
if(p.equals("PP21429")) return 70;
if(p.equals("PP21430")) return 70;
if(p.equals("PP21431")) return 90;
if(p.equals("PP21434")) return 60;
if(p.equals("PP21436")) return 75;
if(p.equals("PP21439")) return 80;
if(p.equals("PP21440")) return 90;
if(p.equals("PP21441")) return 60;
if(p.equals("PP21442")) return 60;
if(p.equals("PP21443")) return 55;
if(p.equals("PP21444")) return 45;
if(p.equals("PP21445")) return 45;
if(p.equals("PP21446")) return 55;
if(p.equals("PP21447")) return 60;
if(p.equals("PP21448")) return 45;
if(p.equals("PP21449")) return 66.6666666666667;
if(p.equals("PP21450")) return 45;
if(p.equals("PP21451")) return 70;
if(p.equals("PP21452")) return 55;
if(p.equals("PP21453")) return 60;
if(p.equals("PP21454")) return 55;
if(p.equals("PP21455")) return 60;
if(p.equals("PP21456"))
 if(w <= 308.4)
  if(t <= 1.55)
   if(t <= 0.605)
    if(we <= 366) return 70;
    if(we > 366) return 80;
   if(t > 0.605)
    if(t <= 0.9)
     if(t <= 0.81)
      if(t <= 0.71)
       if(w <= 294) return 70;
       if(w > 294) return 60;
      if(t > 0.71) return 60;
     if(t > 0.81)
      if(w <= 200)
       if(we <= 646) return 70;
       if(we > 646) return 50;
      if(w > 200) return 105;
    if(t > 0.9)
     if(t <= 1.25) return 80;
     if(t > 1.25) return 60;
  if(t > 1.55)
   if(t <= 1.78) return 55;
   if(t > 1.78)
    if(we <= 474) return 85;
    if(we > 474) return 70;
 if(w > 308.4)
  if(we <= 1533) return 75;
  if(we > 1533) return 50;
if(p.equals("PP21457")) return 60;
if(p.equals("PP21458")) return 60;
if(p.equals("PP21459")) return 50;
if(p.equals("PP21460")) return 45;
if(p.equals("PP21461")) return 50;
if(p.equals("PP21462")) return 45;
if(p.equals("PP21463")) return 45;
if(p.equals("PP21464")) return 45;
if(p.equals("PP21465")) return 60;
if(p.equals("PP21466")) return 55;
if(p.equals("PP21467")) return 55;
if(p.equals("PP21468")) return 60;
if(p.equals("PP21469")) return 60;
if(p.equals("PP21470")) return 45;
if(p.equals("PP21471")) return 60;
if(p.equals("PP21472")) return 60;
if(p.equals("PP21473")) return 50;
if(p.equals("PP21474")) return 45;
if(p.equals("PP21475"))
 if(we <= 1237) return 80;
 if(we > 1237) return 65;
if(p.equals("PP21476")) return 85;
if(p.equals("PP21477")) return 50;
if(p.equals("PP21478")) return 50;
if(p.equals("PP21479")) return 40;
if(p.equals("PP21480")) return 45;
if(p.equals("PP21481")) return 40;
if(p.equals("PP21482")) return 45;
if(p.equals("PP21483")) return 90;
if(p.equals("PP21484"))
 if(t <= 1.63)
  if(t <= 1.21) return 40;
  if(t > 1.21)
   if(we <= 429) return 40;
   if(we > 429)
    if(we <= 474) return 70;
    if(we > 474)
     if(we <= 655) return 50;
     if(we > 655) return 45;
 if(t > 1.63)
  if(t <= 1.9) return 80;
  if(t > 1.9)
   if(we <= 421)
    if(w <= 48)
     if(we <= 284) return 70;
     if(we > 284)
      if(we <= 386)
       if(we <= 304) return 65;
       if(we > 304) return 60;
      if(we > 386) return 90;
    if(w > 48)
     if(w <= 53)
      if(we <= 320) return 75;
      if(we > 320)
       if(we <= 336) return 80;
       if(we > 336) return 85;
     if(w > 53)
      if(w <= 59)
       if(we <= 333)
        if(we <= 304) return 110;
        if(we > 304) return 78.1833333333333;
       if(we > 333)
        if(we <= 370) return 65;
        if(we > 370) return 55;
      if(w > 59)
       if(we <= 388) return 70;
       if(we > 388) return 75;
   if(we > 421) return 70;
if(p.equals("PP21485")) return 50;
if(p.equals("PP21486")) return 50;
if(p.equals("PP21487"))
 if(we <= 1260)
  if(w <= 141) return 50;
  if(w > 141) return 80;
 if(we > 1260)
  if(we <= 1397) return 70;
  if(we > 1397) return 50;
if(p.equals("PP21488")) return 40;
if(p.equals("PP21489")) return 66.6666666666667;
if(p.equals("PP21490")) return 45;
if(p.equals("PP21491")) return 46.6666666666667;
if(p.equals("PP21492")) return 60;
if(p.equals("PP21493")) return 50;
if(p.equals("PP21494")) return 40;
if(p.equals("PP21495")) return 50;
if(p.equals("PP21496")) return 60;
if(p.equals("PP21497")) return 50;
if(p.equals("PP21498")) return 60;
if(p.equals("PP21499")) return 50;
if(p.equals("PP21500")) return 60;
if(p.equals("PP21501")) return 60;
if(p.equals("PP21502")) return 60;
if(p.equals("PP21503")) return 60;
if(p.equals("PP21504"))
 if(w <= 242) return 90;
 if(w > 242) return 60;
if(p.equals("PP21505")) return 60;
if(p.equals("PP21506")) return 60;
if(p.equals("PP21507")) return 65;
if(p.equals("PP21508")) return 60;
if(p.equals("PP21509")) return 50;
if(p.equals("PP21510")) return 60;
if(p.equals("PP21511")) return 60;
if(p.equals("PP21512")) return 60;
if(p.equals("PP21513")) return 70;
if(p.equals("PP21514")) return 60;
if(p.equals("PP21515")) return 60;
if(p.equals("PP21516")) return 60;
if(p.equals("PP21517")) return 50;
if(p.equals("PP21518")) return 50;
if(p.equals("PP21519")) return 60;
if(p.equals("PP21520")) return 65;
if(p.equals("PP21521")) return 60;
if(p.equals("PP21522")) return 70;
if(p.equals("PP21523")) return 70;
if(p.equals("PP21524")) return 70;
if(p.equals("PP21525")) return 50;
if(p.equals("PP21526"))
 if(t <= 1.21)
  if(t <= 1.1)
   if(t <= 0.9)
    if(t <= 0.795)
     if(we <= 2264) return 60;
     if(we > 2264) return 50;
    if(t > 0.795) return 90;
   if(t > 0.9)
    if(w <= 110.3) return 70;
    if(w > 110.3)
     if(we <= 710) return 60;
     if(we > 710)
      if(we <= 754) return 80;
      if(we > 754)
       if(we <= 794) return 55;
       if(we > 794) return 35;
  if(t > 1.1)
   if(w <= 155)
    if(w <= 101.1) return 60;
    if(w > 101.1)
     if(w <= 111.1) return 65;
     if(w > 111.1)
      if(we <= 832)
       if(we <= 735) return 65;
       if(we > 735) return 75;
      if(we > 832) return 60;
   if(w > 155)
    if(w <= 193) return 30;
    if(w > 193)
     if(we <= 1747) return 65;
     if(we > 1747) return 60;
 if(t > 1.21)
  if(w <= 337)
   if(w <= 224)
    if(t <= 1.275) return 50;
    if(t > 1.275)
     if(w <= 179)
      if(w <= 111.1) return 70;
      if(w > 111.1) return 90;
     if(w > 179)
      if(w <= 202)
       if(we <= 1349) return 45;
       if(we > 1349) return 80;
      if(w > 202) return 70;
   if(w > 224)
    if(w <= 248)
     if(w <= 240) return 60;
     if(w > 240) return 35;
    if(w > 248)
     if(we <= 2049)
      if(w <= 260) return 40;
      if(w > 260)
       if(t <= 1.45) return 70;
       if(t > 1.45) return 30;
     if(we > 2049)
      if(we <= 2509)
       if(t <= 1.45) return 50;
       if(t > 1.45) return 60;
      if(we > 2509)
       if(w <= 313.8) return 40;
       if(w > 313.8)
        if(t <= 1.95) return 70;
        if(t > 1.95) return 40;
  if(w > 337)
   if(we <= 2253)
    if(we <= 2128)
     if(we <= 694) return 70;
     if(we > 694) return 50;
    if(we > 2128) return 55;
   if(we > 2253)
    if(we <= 2299) return 35;
    if(we > 2299) return 50;
if(p.equals("PP21527")) return 60;
if(p.equals("PP21528")) return 50;
if(p.equals("PP21529")) return 50;
if(p.equals("PP21530")) return 50;
if(p.equals("PP21531")) return 70;
if(p.equals("PP21532")) return 50;
if(p.equals("PP21533"))
 if(w <= 127.5)
  if(we <= 815) return 60;
  if(we > 815) return 50;
 if(w > 127.5) return 60;
if(p.equals("PP21534")) return 80;
if(p.equals("PP21535")) return 60;
if(p.equals("PP21536")) return 50;
if(p.equals("PP21537")) return 40;
if(p.equals("PP21538")) return 40;
if(p.equals("PP21539")) return 60;
if(p.equals("PP21540")) return 60;
if(p.equals("PP21541")) return 60;
if(p.equals("PP21542")) return 50;
if(p.equals("PP21543")) return 50;
if(p.equals("PP21544")) return 70;
if(p.equals("PP21545")) return 35;
if(p.equals("PP21546")) return 45;
if(p.equals("PP21547")) return 50;
if(p.equals("PP21548")) return 40;
if(p.equals("PP21549")) return 45;
if(p.equals("PP21550")) return 45;
if(p.equals("PP21551"))
 if(we <= 621) return 100;
 if(we > 621) return 90;
if(p.equals("PP21552")) return 40;
if(p.equals("PP21553")) return 48.3333333333333;
if(p.equals("PP21554")) return 58.3333333333333;
if(p.equals("PP21555")) return 58.3333333333333;
if(p.equals("PP21556")) return 45;
if(p.equals("PP21557")) return 80;
if(p.equals("PP21558")) return 80;
if(p.equals("PP21563"))
 if(w <= 53) return 60;
 if(w > 53) return 70;
if(p.equals("PP21576"))
 if(w <= 68) return 70;
 if(w > 68) return 110;
if(p.equals("PP21577"))
 if(w <= 322.5)
  if(t <= 1.91)
   if(w <= 93)
    if(t <= 1.795)
     if(t <= 1.3)
      if(t <= 1.1) return 60;
      if(t > 1.1) return 75;
     if(t > 1.3) return 80;
    if(t > 1.795)
     if(w <= 75) return 80;
     if(w > 75)
      if(we <= 701) return 70;
      if(we > 701)
       if(we <= 765) return 90;
       if(we > 765)
        if(we <= 803) return 68.8833333333333;
        if(we > 803) return 55;
   if(w > 93)
    if(t <= 1.81)
     if(we <= 1143)
      if(t <= 1.4)
       if(t <= 1.25)
        if(t <= 1.2) return 65;
        if(t > 1.2) return 50;
       if(t > 1.25) return 60;
      if(t > 1.4)
       if(we <= 1101)
        if(we <= 1081)
         if(we <= 938) return 50;
         if(we > 938) return 80;
        if(we > 1081) return 75;
       if(we > 1101) return 53.3333333333333;
     if(we > 1143)
      if(we <= 1200)
       if(we <= 1169) return 70;
       if(we > 1169)
        if(we <= 1191) return 80;
        if(we > 1191) return 77.5;
      if(we > 1200)
       if(we <= 1223) return 95;
       if(we > 1223)
        if(t <= 1.1)
         if(we <= 2080) return 60;
         if(we > 2080) return 80;
        if(t > 1.1)
         if(t <= 1.55)
          if(t <= 1.45) return 70;
          if(t > 1.45) return 50;
         if(t > 1.55)
          if(t <= 1.7) return 80;
          if(t > 1.7)
           if(w <= 242) return 70;
           if(w > 242) return 60;
    if(t > 1.81)
     if(t <= 1.86) return 50;
     if(t > 1.86)
      if(we <= 536) return 50;
      if(we > 536) return 95;
  if(t > 1.91)
   if(w <= 154)
    if(t <= 1.98)
     if(t <= 1.97)
      if(we <= 350)
       if(we <= 338) return 100;
       if(we > 338) return 60;
      if(we > 350)
       if(we <= 420) return 80;
       if(we > 420) return 70;
     if(t > 1.97)
      if(we <= 561) return 80;
      if(we > 561) return 100;
    if(t > 1.98)
     if(w <= 106)
      if(we <= 326) return 60;
      if(we > 326) return 100;
     if(w > 106) return 100;
   if(w > 154)
    if(t <= 2)
     if(w <= 246) return 90;
     if(w > 246) return 75;
    if(t > 2)
     if(t <= 2.075) return 70;
     if(t > 2.075) return 50;
 if(w > 322.5)
  if(t <= 1.275)
   if(w <= 400)
    if(t <= 1.1) return 50;
    if(t > 1.1) return 40;
   if(w > 400)
    if(t <= 1.21) return 65;
    if(t > 1.21)
     if(we <= 1948)
      if(we <= 1926)
       if(we <= 1601) return 55;
       if(we > 1601) return 70;
      if(we > 1926) return 65;
     if(we > 1948) return 55;
  if(t > 1.275)
   if(w <= 347) return 75;
   if(w > 347)
    if(we <= 1397)
     if(t <= 1.7) return 40;
     if(t > 1.7) return 35;
    if(we > 1397) return 40;
if(p.equals("PP21580"))
 if(w <= 71.5)
  if(we <= 348) return 50;
  if(we > 348) return 60;
 if(w > 71.5)
  if(we <= 595)
   if(t <= 1.58)
    if(we <= 296) return 75;
    if(we > 296)
     if(we <= 342) return 60;
     if(we > 342) return 75;
   if(t > 1.58)
    if(t <= 1.63)
     if(we <= 553)
      if(we <= 285) return 90;
      if(we > 285)
       if(w <= 106) return 70;
       if(w > 106)
        if(we <= 522) return 90;
        if(we > 522) return 70;
     if(we > 553) return 80;
    if(t > 1.63)
     if(t <= 1.675)
      if(w <= 106)
       if(we <= 294) return 85;
       if(we > 294) return 90;
      if(w > 106)
       if(we <= 538) return 85;
       if(we > 538) return 90;
     if(t > 1.675) return 80;
  if(we > 595)
   if(we <= 648)
    if(we <= 618) return 60;
    if(we > 618) return 85;
   if(we > 648) return 75;
if(p.equals("PP21592"))
 if(t <= 0.71)
  if(t <= 0.6) return 48.3333333333333;
  if(t > 0.6)
   if(w <= 225) return 80;
   if(w > 225) return 70;
 if(t > 0.71)
  if(w <= 404)
   if(we <= 1660) return 50;
   if(we > 1660)
    if(w <= 302.6) return 60;
    if(w > 302.6)
     if(w <= 368) return 40;
     if(w > 368) return 60;
  if(w > 404) return 50;
if(p.equals("PP21628")) return 80;
if(p.equals("PP21684")) return 70;
if(p.equals("PP21690")) return 70;
if(p.equals("PP21694"))
 if(t <= 1.519)
  if(we <= 640)
   if(we <= 601) return 70;
   if(we > 601)
    if(we <= 628) return 80;
    if(we > 628) return 90;
  if(we > 640) return 70;
 if(t > 1.519)
  if(t <= 1.71) return 75;
  if(t > 1.71) return 90;
if(p.equals("PP21752")) return 70;
if(p.equals("PP21754"))
 if(w <= 330)
  if(w <= 194) return 80;
  if(w > 194)
   if(we <= 1801) return 90;
   if(we > 1801)
    if(t <= 0.9) return 80;
    if(t > 0.9) return 85;
 if(w > 330) return 60;
if(p.equals("PP21762"))
 if(w <= 273) return 70;
 if(w > 273) return 90;
if(p.equals("PP21794"))
 if(we <= 4066) return 50;
 if(we > 4066)
  if(we <= 4900) return 30;
  if(we > 4900) return 35;
if(p.equals("PP21798")) return 55;
if(p.equals("PP21802"))
 if(we <= 1711) return 60;
 if(we > 1711) return 50;
if(p.equals("PP21805")) return 50;
if(p.equals("PP21814")) return 30;
if(p.equals("PP21855"))
 if(t <= 1.63) return 70;
 if(t > 1.63)
  if(t <= 1.725) return 90;
  if(t > 1.725) return 70;
if(p.equals("PP22069"))
 if(w <= 566)
  if(we <= 6743) return 50;
  if(we > 6743) return 45;
 if(w > 566) return 40;
if(p.equals("PP22076")) return 50;
if(p.equals("PP22079"))
 if(w <= 72) return 100;
 if(w > 72) return 70;
if(p.equals("PP22085"))
 if(t <= 1.795)
  if(t <= 1.4) return 60;
  if(t > 1.4) return 80;
 if(t > 1.795)
  if(t <= 1.9)
   if(we <= 670)
    if(we <= 434.5) return 70;
    if(we > 434.5)
     if(w <= 66.5) return 70;
     if(w > 66.5) return 60;
   if(we > 670) return 80;
  if(t > 1.9) return 70;
if(p.equals("PP22171"))
 if(t <= 1.1)
  if(t <= 0.81)
   if(we <= 3610) return 50;
   if(we > 3610) return 70;
  if(t > 0.81)
   if(t <= 1.01)
    if(we <= 1890) return 50;
    if(we > 1890) return 60;
   if(t > 1.01)
    if(we <= 3581) return 60;
    if(we > 3581) return 50;
 if(t > 1.1)
  if(t <= 1.57) return 55;
  if(t > 1.57)
   if(we <= 1523) return 80;
   if(we > 1523) return 40;
if(p.equals("PP22234")) return 40;
if(p.equals("PP22259")) return 60;
if(p.equals("PP22272")) return 70;
if(p.equals("PP22273"))
 if(w <= 224) return 70;
 if(w > 224)
  if(we <= 2357)
   if(t <= 1.78) return 50;
   if(t > 1.78) return 40;
  if(we > 2357)
   if(we <= 2606) return 45;
   if(we > 2606) return 55;
if(p.equals("PP22274")) return 40;
if(p.equals("PP22279")) return 35;
if(p.equals("PP22302")) return 35;
if(p.equals("PP22334"))
 if(w <= 127.5)
  if(w <= 59)
   if(we <= 372)
    if(we <= 344)
     if(we <= 311) return 60;
     if(we > 311) return 70;
    if(we > 344) return 90;
   if(we > 372) return 70;
  if(w > 59) return 90;
 if(w > 127.5)
  if(we <= 1727) return 80;
  if(we > 1727) return 70;
if(p.equals("PP22343")) return 70;
if(p.equals("PP22376"))
 if(w <= 80)
  if(we <= 372) return 75;
  if(we > 372) return 70;
 if(w > 80)
  if(we <= 512) return 75;
  if(we > 512) return 70;
if(p.equals("PP22391")) return 60;
if(p.equals("PP22395"))
 if(we <= 1703) return 60;
 if(we > 1703)
  if(we <= 1735) return 55;
  if(we > 1735)
   if(we <= 1874) return 70;
   if(we > 1874)
    if(we <= 2101) return 60;
    if(we > 2101) return 55;
if(p.equals("PP22423")) return 40;
if(p.equals("PP22438")) return 40;
if(p.equals("PP22457")) return 60;
if(p.equals("PP22460")) return 40;
if(p.equals("PP22461")) return 40;
if(p.equals("PP22483")) return 70;
if(p.equals("PP22496")) return 60;
if(p.equals("PP22517"))
 if(we <= 2660) return 30;
 if(we > 2660) return 19.9;
if(p.equals("PP22522")) return 50;
if(p.equals("PP22537"))
 if(w <= 60) return 50;
 if(w > 60) return 55;
if(p.equals("PP22553")) return 40;
if(p.equals("PP22567"))
 if(w <= 80)
  if(we <= 400)
   if(we <= 384) return 80;
   if(we > 384)
    if(we <= 392) return 79.1833333333333;
    if(we > 392) return 110;
  if(we > 400)
   if(we <= 423)
    if(we <= 410) return 84.7833333333333;
    if(we > 410) return 70;
   if(we > 423)
    if(we <= 433) return 60;
    if(we > 433) return 80;
 if(w > 80)
  if(we <= 530) return 80;
  if(we > 530)
   if(we <= 583) return 110;
   if(we > 583) return 80;
if(p.equals("WS21007")) return 40;
if(p.equals("WS21143"))
 if(t <= 0.51) return 50;
 if(t > 0.51)
  if(we <= 397)
   if(we <= 389) return 35;
   if(we > 389) return 40;
  if(we > 397) return 65;
return 65.0;
}
}
