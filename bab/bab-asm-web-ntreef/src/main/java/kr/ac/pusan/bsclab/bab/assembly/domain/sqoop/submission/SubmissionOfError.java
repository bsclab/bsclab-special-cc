package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.submission;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubmissionOfError extends Submission {
	
	@JsonProperty("error-details")
	String errorDetails;
	
	@JsonProperty("error-summary")
	String errorSummary;
	
	@JsonProperty("to-schema")
	Map<String, Object> toSchema;
	
	@JsonProperty("from-schema")
	Map<String, Object> fromSchema;
	
	
}
