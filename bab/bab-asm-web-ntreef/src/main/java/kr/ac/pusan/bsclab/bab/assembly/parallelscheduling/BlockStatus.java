package kr.ac.pusan.bsclab.bab.assembly.parallelscheduling;

public class BlockStatus {
	public String tp_name;
	public double tp_departure_time;
	public double tp_block_pickup_time;
	public double tp_block_delivered_time;
	public double tp_duration;
}
