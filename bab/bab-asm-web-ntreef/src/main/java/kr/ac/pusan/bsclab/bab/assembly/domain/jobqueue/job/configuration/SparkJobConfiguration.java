package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration;

public class SparkJobConfiguration implements JobConfiguration {
	
	private String jobId;
	private String jobClass;
	private String jobPath;
	private String userId;
	
	
	public SparkJobConfiguration(String jobId, String jobClass, String jobPath, String userId) {
		this.jobId = jobId;
		this.jobClass = jobClass;
		this.jobPath = jobPath;
		this.userId = userId;
	}
	public String getJobId() {
		return jobId;
	}
	public String getJobClass() {
		return jobClass;
	}
	public String getJobPath() {
		return jobPath;
	}
	public String getUserId() {
		return userId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}
	public void setJobPath(String jobPath) {
		this.jobPath = jobPath;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	

}
