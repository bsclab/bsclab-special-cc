package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.hadoop.fs.FileStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.assembly.ws.BabWebService;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.BabServiceInfo;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;
import kr.ac.pusan.bsclab.bab.ws.api.repository.fl.FilteringJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ls.LogSummary;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ls.LogSummaryJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.map.MappingJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;

@Controller
public class RepositoryController extends AbstractServiceController {

	public static final String BASE_URL = BabWebService.BASE_URL + "/repository";

	private static final Logger logger = LoggerFactory.getLogger(RepositoryController.class);

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.POST }, path = BASE_URL
//			+ "/import/{workspaceId}/{datasetId}/{repositoryId}/{format}/{tmpName}")
//	public @ResponseBody Response<ImportJobResult> getImport(@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId,  
//			@PathVariable(value = "sdt") String sdt,
//			@PathVariable(value = "edt") String edt,@PathVariable(value = "format") String format,
//			@RequestParam("file") MultipartFile file, ModelMap modelMap, HttpServletRequest request,
//			HttpServletResponse response) {
//		try {
//			String rootPath = System.getProperty("catalina.home");
//			File uploadDir = new File(rootPath + File.separator + "uploads");
//			if (!uploadDir.exists())
//				uploadDir.mkdirs();
//			if (!file.isEmpty()) {
//				String rawPath = webHdfs.getPathOnly(
//						"/" + workspaceId + datasetId + repositoryId + "." + format.toLowerCase(), WebHdfs.PATH_TEMP);
//				byte[] bytes = file.getBytes();
//				File serverFile = new File(uploadDir.getAbsolutePath() + File.separator + file.getOriginalFilename());
//				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
//				stream.write(bytes);
//				stream.close();
//				this.webHdfs.copyFromLocal(serverFile.getPath(), rawPath);
//
//				ObjectMapper mapper = new ObjectMapper();
//				ImportJobConfiguration config = new ImportJobConfiguration();
//				config.setRawPath(rawPath);
//				config.setRepositoryURI(
//						webHdfs.getPathOnly("/" + workspaceId + "/" + datasetId + "/" + repositoryId));
//				String configJson = mapper.writeValueAsString(config);
//				Response<ImportJobResult> babResponse = this.submitJob(
//						"Repository" + format.substring(0, 1).toUpperCase() + format.substring(1).toLowerCase()
//								+ "ImportJob",
//						"mrepo", workspaceId, datasetId, configJson
//						, dc.getTimestamp(sdt), dc.getTimestamp(edt),
//						webHdfs.getRepositoryPathOnly(workspaceId, datasetId, logDao.getChecksum(dc.getTimestamp(sdt), dc.getTimestamp(edt))) + ".mrepo",
//						ImportJobResult.class);
//				return babResponse;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.POST }, path = BASE_URL + "/map/{workspaceId}/{datasetId}/{repositoryId}")
//	public @ResponseBody Response<BRepository> postMap(@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId,
//			@PathVariable(value = "repositoryId") String repositoryId,  
//			@PathVariable(value = "sdt") String sdt,
//			@PathVariable(value = "edt") String edt,MappingJobConfiguration config,
//			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//		try {
//			if (config.getRepositoryURI() == null) {
//				config.setRepositoryURI(webHdfs.getRepositoryPathOnly(workspaceId, datasetId, repositoryId));
//			}
//			ObjectMapper mapper = new ObjectMapper();
//			String configJson = mapper.writeValueAsString(config);
//			Response<BRepository> babResponse = this.submitJob( "RepositoryMappingJob", "brepo", workspaceId,
//					datasetId, configJson
//					, dc.getTimestamp(sdt), dc.getTimestamp(edt),
//					webHdfs.getRepositoryPathOnly(workspaceId, datasetId, repositoryId + "_" + config.getName())
//							+ ".brepo",
//					BRepository.class);
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/workspaces")
//	public @ResponseBody Response<List<BWorkspace>> getWorkspaces(ModelMap modelMap, HttpServletRequest request,
//			HttpServletResponse response) {
//		try {
//			List<FileStatus> fileStatuses = webHdfs.listStatus(webHdfs.getPathOnly("", WebHdfs.PATH_WORKSPACES));
//			List<BWorkspace> workspaces = new ArrayList<BWorkspace>();
//			for (FileStatus fs : fileStatuses) {
//				if (fs.isDirectory()) {
//					BWorkspace ws = new BWorkspace();
//					ws.setId(fs.getPath().getName());
//					ws.setName(ws.getId());
//					ws.setDescription(ws.getId());
//					ws.setUri(BASE_URL + "/datasets/" + ws.getId());
//					workspaces.add(ws);
//				}
//			}
//			Response<List<BWorkspace>> babResponse = new Response<List<BWorkspace>>(null, workspaces);
//			babResponse.setStatus(Response.STATUS_FINISHED);
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/datasets/{workspaceId}")
//	public @ResponseBody Response<List<BDataset>> getDatasets(@PathVariable(value = "workspaceId") String workspaceId,
//			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//		try {
//			List<FileStatus> fileStatuses = webHdfs
//					.listStatus(webHdfs.getPathOnly("/" + workspaceId, WebHdfs.PATH_WORKSPACES));
//			List<BDataset> datasets = new ArrayList<BDataset>();
//			for (FileStatus fs : fileStatuses) {
//				if (fs.isDirectory()) {
//					BDataset ds = new BDataset();
//					ds.setId(fs.getPath().getName());
//					ds.setName(ds.getId());
//					ds.setDescription(ds.getId());
//					ds.setMappingUri(BASE_URL + "/mappings/" + workspaceId + "/" + ds.getId());
//					ds.setUri(BASE_URL + "/repositories/" + workspaceId + "/" + ds.getId());
//					datasets.add(ds);
//				}
//			}
//			Response<List<BDataset>> babResponse = new Response<List<BDataset>>(null, datasets);
//			babResponse.setStatus(Response.STATUS_FINISHED);
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/mappings/{workspaceId}/{datasetId}")
//	public @ResponseBody Response<List<ImportJobResult>> getMappings(
//			@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId, ModelMap modelMap, HttpServletRequest request,
//			HttpServletResponse response) {
//		try {
//			List<FileStatus> fileStatuses = webHdfs
//					.listStatus(webHdfs.getPathOnly("/" + workspaceId + "/" + datasetId, WebHdfs.PATH_WORKSPACES));
//			List<ImportJobResult> mappings = new ArrayList<ImportJobResult>();
//			ObjectMapper mapper = new ObjectMapper();
//			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//			for (FileStatus fs : fileStatuses) {
//				if (!fs.isDirectory() && fs.getPath().getName().endsWith(".mrepo")) {
//					ImportJobResult mr = new ImportJobResult();
//					mr.setId(fs.getPath().getName().substring(0, fs.getPath().getName().length() - 6));
//					mr.setName(mr.getId());
//					mr.setDescription(mr.getId());
//					mr.setUri(BASE_URL + "/viewmap/" + workspaceId + "/" + datasetId + "/" + mr.getId());
//					mappings.add(mr);
//				}
//			}
//			Response<List<ImportJobResult>> babResponse = new Response<List<ImportJobResult>>(null, mappings);
//			babResponse.setStatus(Response.STATUS_FINISHED);
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL
			+ "/repositories/{workspaceId}/{datasetId}/{sdt}/{edt}")
	@BabServiceInfo(
		title = "Get List of Repositories", 
		uri = BASE_URL + "/repositories/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<List<BRepository>> getRepositories(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
//			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
//					, dc.getTimestamp(edt));
			
			List<FileStatus> fileStatuses = webHdfs
					.listStatus(hp.getUserHome() + "/" + workspaceId
							+ "/" + datasetId);
			List<BRepository> repositories = new ArrayList<BRepository>();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			for (FileStatus fs : fileStatuses) {
				BRepository br = new BRepository();
				br.setId(fs.getPath().getName());
				br.setName(br.getId());
				br.setDescription(sdt + "~" + edt);
				br.setUri(BASE_URL + "/view/" + workspaceId
						+ "/" + datasetId + "/" + sdt + "/" + edt);
				String hdfsPath = hp.getUserHome() + "/" + workspaceId + "/" + datasetId + "/" + br.getId() + "/" + br.getId() + ".brepo";
				if (webHdfs.isExists(hdfsPath)) {
					BRepository brl = mapper.readValue(webHdfs.openAsTextFile(hdfsPath), BRepository.class);
					br.setName(brl.getName());
					br.setDescription(brl.getDescription());
					br.setUri(brl.getUri());
				}
				repositories.add(br);
			}
			Response<List<BRepository>> babResponse = new Response<List<BRepository>>(null, repositories);
			babResponse.setStatus(Response.STATUS_FINISHED);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/filter/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<BRepository> postFilter(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			FilteringJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
			
		try {
			Response<BRepository> babResponse = null;
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			if (config != null) {
	//				if (config.getRepositoryURI() == null) {
	//				config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, logDao.getChecksum(dc.getTimestamp(sdt), dc.getTimestamp(edt))));
	//			}
				ObjectMapper om = new ObjectMapper();
				String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
				config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
						+ "/" + hash);
				String jobConfig = om.writeValueAsString(config);
				babResponse = this.submitJob("RepositoryFilteringJob", "brepo", workspaceId,
						datasetId, resourceDataHash, hash, jobConfig
						, dc.getTimestamp(sdt), dc.getTimestamp(edt), BRepository.class);
			}
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/repositories/{workspaceId}/{datasetId}/{sdt}/{edt}")
//	public @ResponseBody Response<List<BRepository>> getRepositories(
//			@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId
//			, @PathVariable(value = "sdt") String sdt
//			, @PathVariable(value = "edt") String edt, ModelMap modelMap, HttpServletRequest request,
//			HttpServletResponse response) {
//		return getRepositories(workspaceId, datasetId, sdt, edt, null, modelMap, request, response);
//	}

//	@CrossOrigin
//	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/viewmap/{workspaceId}/{datasetId}/{mappingId}")
//	public @ResponseBody Response<ImportJobResult> getViewMapping(
//			@PathVariable(value = "workspaceId") String workspaceId,
//			@PathVariable(value = "datasetId") String datasetId, @PathVariable(value = "mappingId") String mappingId,
//			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//			ImportJobResult mapping = null;
//			String hdfsPath = webHdfs.getRepositoryPathOnly(workspaceId, datasetId, mappingId) + ".mrepo";
//			Response<ImportJobResult> babResponse = new Response<ImportJobResult>(null, null);
//			if (webHdfs.isExists(hdfsPath)) {
//				mapping = mapper.readValue(webHdfs.openAsTextFile(hdfsPath), ImportJobResult.class);
//				List<FileStatus> fileStatuses = webHdfs.listStatus(
//						webHdfs.getPathOnly("/" + workspaceId + "/" + datasetId, WebHdfs.PATH_WORKSPACES));
//				for (FileStatus fs : fileStatuses) {
//					if (!fs.isDirectory() && fs.getPath().getName().startsWith(mappingId + "_")
//							&& fs.getPath().getName().endsWith(".brepo")) {
//						BRepository br = new BRepository();
//						br.setId(fs.getPath().getName());
//						br.setName(br.getId());
//						br.setDescription(br.getId());
//						br.setUri(BASE_URL + "/view/" + workspaceId + "/" + datasetId + "/"
//								+ br.getId().substring(0, br.getId().length() - 6));
//						mapping.getRepositories().add(br);
//					}
//				}
//				babResponse = new Response<ImportJobResult>(null, mapping);
//				babResponse.setStatus(Response.STATUS_FINISHED);
//			}
//			return babResponse;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/view/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<BRepository> getView(
			@PathVariable(value = "workspaceId") String workspaceId
			, @PathVariable(value = "datasetId") String datasetId
			, @PathVariable(value = "sdt") String sdt
			, @PathVariable(value = "edt") String edt
			, ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//		logger.debug("called getView()");
		try {
			String resourceDataHash = this.resourceDataHash(
					dc.getTimestamp(sdt), dc.getTimestamp(edt));
//			resourceDataHash = (sdt + edt);
			
			/*
			 * this is for menual mapping configuration
			 */
			Map<String, String> _case = new HashMap<String, String>();
			_case.put("coil_no", "CASE");
			Map<String, String> activity = new HashMap<String, String>();
			activity.put("prc_cd1", "EVENT");
			Map<String, String> timestamp = new HashMap<String, String>();
			timestamp.put("sdt", "EVENT,yyyy-MM-dd HH:mm:ss.SSS");
			timestamp.put("edt", "EVENT,yyyy-MM-dd HH:mm:ss.SSS");
			Map<String, String> originator = new HashMap<String, String>();
			originator.put("prc_cd_short", "EVENT");
			Map<String, String> resource = new HashMap<String, String>();
			resource.put("prc_cd", "EVENT");
			
			
			Map<String, Map<String, String>> mapping
				= new HashMap<String, Map<String, String>>();
			mapping.put("CASE", _case);
			mapping.put("ACTIVITY", activity);
			mapping.put("TIMESTAMP", timestamp);
			mapping.put("ORIGINATOR", originator);
			mapping.put("RESOURCE", resource);
			
			MappingJobConfiguration mjc = new MappingJobConfiguration();
			mjc.setDescription(sdt + "~" + edt + " hash@" + resourceDataHash.substring(0,5));
			mjc.setMapping(mapping);
			mjc.setName(resourceDataHash);
			
			ObjectMapper om = new ObjectMapper();
//			String hash = DigestUtils.md5Hex(om.writeValueAsString(mjc));
			mjc.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + resourceDataHash);
			String jobConfig = om.writeValueAsString(mjc);
			Response<BRepository> babResponse = this.submitJob("RepositoryMappingJob", "brepo"
					, workspaceId, datasetId, resourceDataHash, resourceDataHash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), BRepository.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET }, path = BASE_URL + "/view/{workspaceId}/{datasetId}/{repositoryId}")
	public @ResponseBody Response<BRepository> getView(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId,
			ModelMap modelMap, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			BRepository repository = null;
			
			String resourceDataHash = repositoryId; 
			String hdfsPath = webHdfs.getFilePath(workspaceId, datasetId
					, resourceDataHash, resourceDataHash, "brepo");
			Response<BRepository> babResponse = new Response<BRepository>(null
					, resourceDataHash, null);
			babResponse.setStatus(Response.STATUS_RUNNING);
			if (webHdfs.isExists(hdfsPath)) {
				repository = mapper.readValue(webHdfs.openAsTextFile(hdfsPath), BRepository.class);
//				babResponse = new Response<BRepository>(null, resourceDataHash, repository);
				babResponse.setResponse(repository);
				babResponse.setStatus(Response.STATUS_FINISHED);
			}
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@CrossOrigin
	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST }, path = BASE_URL
			+ "/summary/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody Response<LogSummary> postLogSummary(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, LogSummaryJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpServletResponse response) {
//		logger.debug("called getLogSummary()");
		try {
			String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt)
					, dc.getTimestamp(edt));
			logger.debug("resourceDataHash={}", resourceDataHash);
			if (config == null) {
				config = new LogSummaryJobConfiguration();
			}
//			if (config.getRepositoryURI() == null) {
//				config.setRepositoryURI(webHdfs.getRepositoryPath(
//						workspaceId, datasetId, resourceDataHash));
//			}
			
			ObjectMapper om = new ObjectMapper();
			String hash = DigestUtils.md5Hex(om.writeValueAsString(config));
			config.setRepositoryURI(webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash)
					+ "/" + hash);
			String jobConfig = om.writeValueAsString(config);
			Response<LogSummary> babResponse = this.submitJob( "RepositoryLogSummary", "blsum", workspaceId,
					datasetId, resourceDataHash, hash, jobConfig
					, dc.getTimestamp(sdt), dc.getTimestamp(edt), LogSummary.class);
			return babResponse;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/*
	 * /
	 * 
	 * @RequestMapping(method = { RequestMethod.POST }, path = BASE_URL +
	 * "/mapping/{workspaceId}/{datasetId}/{repositoryId}") public @ResponseBody
	 * Response postMapping(
	 * 
	 * @PathVariable(value = "workspaceId") String workspaceId,
	 * 
	 * @PathVariable(value = "datasetId") String datasetId,
	 * 
	 * @PathVariable(value = "repositoryId") String repositoryId, ModelMap
	 * modelMap, HttpServletRequest request, HttpServletResponse response) {
	 * return null; } //
	 */
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = BASE_URL + "/upload")
	public @ResponseBody void postUpload(
			@RequestParam("uploadFile")MultipartFile multipartFile) throws IOException, InterruptedException {
		
		babService.csvToDB(multipartFile);
	}

}