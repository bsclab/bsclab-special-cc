package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job;

import java.util.Date;

import org.springframework.stereotype.Component;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.SqoopElement;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link.Link;

@Component
public class SqoopJobFactory {
	
	public Job createJob(int id
			, String name
			, Link fromLink
			, Link toLink
			, String creationUser
			, Date creationDate
			, String updateUser
			, Date updateDate
			, boolean enabled
			, ConfigValue[] fromConfigValues
			, ConfigValue[] toConfigValues
			, ConfigValue[] driverConfigValues) {
		return new Job(id
				, name
				, fromLink.getConnectorId()
				, fromLink.getId()
				, toLink.getConnectorId()
				, toLink.getId()
				, creationUser
				, creationDate
				, updateUser
				, updateDate
				, enabled
				, fromConfigValues
				, toConfigValues
				, driverConfigValues);
	}
	
	public ConfigValue[] createConfigValues(int id
			, ConfigValueName configValueName
			, SqoopElement sqoopElement
			, Input[] inputs) {
		return new ConfigValue[]{new ConfigValue(id
				, configValueName
				, sqoopElement
				, inputs)};
	}
}
