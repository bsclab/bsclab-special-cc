package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.submission;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("submission")
public class Submission {
	
	Integer job;
	
	Integer progress;
	
	SubmissionStatus status;
	
	@JsonProperty("creation-date")
	Date creationDate;

	@JsonProperty("last-update-date")
	Date lastUpdateDate;
	
	@JsonInclude(Include.NON_DEFAULT)
	@JsonProperty("external-id")
	String externalId;
	
	@JsonInclude(Include.NON_DEFAULT)
	@JsonProperty("external-link")
	String externalLink;
	
	

	/**
	 * 
	 */
	public Submission() {
	}

	/**
	 * @param job
	 * @param progress
	 * @param status
	 * @param creationDate
	 * @param lastUpdateDate
	 * @param externalId
	 * @param externalLink
	 */
	public Submission(Integer job, Integer progress, SubmissionStatus status, Date creationDate, Date lastUpdateDate,
			String externalId, String externalLink) {
		this.job = job;
		this.progress = progress;
		this.status = status;
		this.creationDate = creationDate;
		this.lastUpdateDate = lastUpdateDate;
		this.externalId = externalId;
		this.externalLink = externalLink;
	}

	public Integer getJob() {
		return job;
	}

	public void setJob(Integer job) {
		this.job = job;
	}

	public Integer getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	public SubmissionStatus getStatus() {
		return status;
	}

	public void setStatus(SubmissionStatus status) {
		this.status = status;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}
	
	
}