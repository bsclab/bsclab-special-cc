package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SS3 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-8.01519006e-02, -4.73342091e-01, 1.22598000e+01, -1.17669851e-01,
            				7.63642311e+00, -6.09862924e-01, -1.37455493e-01, 3.36414516e-01,
            				9.13231468e+00, 1.24281025e+01, -1.03190923e+00, -1.01915345e-01,
            				-1.03409600e+00, 1.39607496e+01, -1.14812684e+00, -9.50954854e-01,
            				7.79918253e-01, -1.42964327e+00, 9.59117651e-01, 3.39287519e-02}, 
            			{-1.09575677e+00, -1.11409354e+00, -6.64264619e-01, 1.16410226e-01,
        					1.62866682e-01, 4.55484986e-01, -3.60305041e-01, 9.71577466e-01,
        					-1.28569293e+00, -2.39033866e+00, 2.48395514e+00, 2.03625345e+00,
        					2.05516696e-01, -2.83093542e-01, -1.71006012e+00, -1.46268892e+00,
        					-1.18884385e+00, 1.54353714e+00, -6.74616158e-01, -8.87647569e-01},
            			{5.28437078e-01, -6.53721213e-01, -4.10838187e-01, -1.08783102e+00,
    						4.35380191e-01, -1.04902875e+00, 1.93083510e-01, -1.63411033e+00,
    						-8.33280206e-01, -8.44335377e-01, -1.37044996e-01, 5.69518983e-01,
    						-3.98605019e-02, 6.42618716e-01, -7.14272680e-03, -1.32668149e+00,
    						-2.06876469e+00, 2.99052484e-02, -9.11880016e-01, 6.56361699e-01}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-1.04986525e+00, -3.95602882e-01, 2.48040080e+00, -1.02384254e-01
            			, 1.04877961e+00, -1.58192357e-03, -1.88263029e-01, -1.35428572e+00
            			, 6.34032369e-01, 6.98523283e-01, -5.40403724e-01, -4.75431561e-01
            			, -6.99972272e-01, 8.07066023e-01, -9.73312557e-01, -5.48621356e-01
            			, -3.20456445e-01, -2.29100561e+00, -4.91476238e-01, -1.50644386e+00};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {0.12726426,
            			0.19155794,
            			0.88886613,
            			0.96968019,
            			0.54458481,
            			0.35154301,
            			0.43658662,
            			0.71248132,
            			0.43557489,
            			0.91211355,
            			0.37281924,
            			0.65256226,
            			-0.45262176,
            			1.51715779,
            			0.60681349,
            			0.2931906,
            			-0.70627958,
            			0.69614422,
            			-0.15012771,
            			-0.58707452};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-0.78752846};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 10.0;
	}

	@Override
	public double getMaxWidth() {
		
		return 600.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 6898.0;
	}

	@Override
	public double getMinThick() {
		
		return 1.48;
	}

	@Override
	public double getMinWidth() {
		
		return 54.0;
	}

	@Override
	public double getMinWeight() {
		
		return 261.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
