package kr.ac.pusan.bsclab.bab.assembly.conf.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application-yarn.properties")
@ConfigurationProperties(prefix="yarn")
public class YarnProperties {
	
	private String server;
	private String apiPort;
	private String apiUri;
	
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getApiPort() {
		return apiPort;
	}
	public void setApiPort(String apiPort) {
		this.apiPort = apiPort;
	}
	public String getApiUri() {
		return apiUri;
	}
	public void setApiUri(String apiUri) {
		this.apiUri = apiUri;
	}
	
	
}
