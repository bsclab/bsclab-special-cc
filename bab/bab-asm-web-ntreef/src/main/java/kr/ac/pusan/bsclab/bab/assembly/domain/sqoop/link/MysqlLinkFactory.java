package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link;

import java.util.ArrayList;
import java.util.List;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.InputsFactory;

public class MysqlLinkFactory extends LinkFactory implements InputsFactory {
	
	/**
	 * args[0] : jdbcDriver
	 * args[1] : connectionString
	 * args[2] : username
	 * args[3] : password
	 * args[4] : jdbcProperties
	 */
	@Override
	public Input[] createInputs(Object... args) {
		List<Input> inputs = new ArrayList<Input>();
		inputs.add(new Input(128, "ANY", "linkConfig.jdbcDriver", 1, false, "", "STRING", args[0], null));
		inputs.add(new Input(128, "ANY", "linkConfig.connectionString", 2, false, "", "STRING", args[1], null));
		inputs.add(new Input(40, "ANY", "linkConfig.username", 3, false, "", "STRING", args[2], null));
		inputs.add(new Input(40, "ANY", "linkConfig.password", 4, true, "", "STRING", args[3], null));
		inputs.add(new Input(0, "ANY", "linkConfig.jdbcProperties", 5, false, "", "MAP", args[4], null));
		return inputs.toArray(new Input[0]);
	}
	

}
