package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.assembly.abs.AbstractController;
import kr.ac.pusan.bsclab.bab.assembly.conf.ApiManager;
import kr.ac.pusan.bsclab.bab.assembly.web.models.JsonResponse;
import kr.ac.pusan.bsclab.bab.assembly.ws.models.Response;

@Controller
public abstract class AbstractWebController extends AbstractController {
//	public final BabConfiguration config = new BabConfiguration();
//	public final Map<String, Map<String, String>> APIURI = config.APIURI; 
//	public final Map<String, Map<String, String>> WEBURI = config.WEBURI; 
	
	@Autowired
	protected ApiManager apiManager;
	
	public <K, T> Response<T> callBabService(String uri, K config, Class<T> responseClass) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			MultiValueMap<String, Object> request = new LinkedMultiValueMap<String, Object>();
			request.add("config", mapper.writeValueAsString(config));
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			HttpEntity<MultiValueMap<String, Object>> entity
				= new HttpEntity<MultiValueMap<String, Object>>(request, headers);
			ResponseEntity<String> json = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
			JsonResponse immResult = mapper.readValue(json.getBody(), JsonResponse.class);
			String responseJson = mapper.writeValueAsString(immResult.getResponse());
			T response = mapper.readValue(responseJson, responseClass);
			Response<T> result = new Response<T>(immResult.getRequest(), response);
			result.setId(immResult.getId());
			result.setStatus(immResult.getStatus());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Map<String, String> buildJSONWorkspaceDatasetRepository(String workspaceId, String datasetId, String sdt, String edt){
		Map<String, String> jsonData= new HashMap<String, String>();
        jsonData.put("workspaceId", workspaceId);
        jsonData.put("datasetId", datasetId);
        jsonData.put("sdt", sdt);
        jsonData.put("edt", edt);
        jsonData.put("wd", workspaceId+"/"+datasetId);
        jsonData.put("wdr", workspaceId+"/"+datasetId+"/"+sdt+"/"+edt);
        
        return jsonData;
	}

}
