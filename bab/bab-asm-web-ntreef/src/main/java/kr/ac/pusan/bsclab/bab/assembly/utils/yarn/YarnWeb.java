package kr.ac.pusan.bsclab.bab.assembly.utils.yarn;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.YarnProperties;
import kr.ac.pusan.bsclab.bab.assembly.domain.yarn.App;
import kr.ac.pusan.bsclab.bab.assembly.domain.yarn.State;
import kr.ac.pusan.bsclab.bab.assembly.rest.RestRequest;

@Component
public class YarnWeb {

	@Autowired
	YarnProperties yp;
	
	@Autowired
	RestRequest restRequest;
	
	@Transactional
	public App getApp(String appId)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
		om.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		String uri = yp.getApiUri() + "/apps/" + appId;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);
		return om.readValue(result.getBody(), App.class);
	}
	
	@Transactional
	public State getAppState(String appId)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(DeserializationFeature.UNWRAP_ROOT_VALUE, true);
		String uri = yp.getApiUri() + "/apps/" + appId + "/state";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<String> result = restRequest.exchange(uri, HttpMethod.GET, entity, String.class);

		return om.readValue(result.getBody(), State.class);
	}

}
