package kr.ac.pusan.bsclab.bab.assembly.rserver.param;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Class for setting parameters, refers to all of fields of event logs
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */
public class Parameter {

	private String coil_no;
	private String prc_cd;
	private String thk;
	private String wdt;
	private String wgt;
	private String sdt;
	private String edt;

	/**
	 * @return coil number
	 */
	@JsonInclude(Include.NON_NULL)
	public String getCoil_no() {
		return coil_no;
	}

	/**
	 * @param coil_no
	 */
	public void setCoil_no(String coil_no) {
		this.coil_no = coil_no;
	}

	/**
	 * @return prc_cd
	 */
	@JsonInclude(Include.NON_NULL)
	public String getPrc_cd() {
		return prc_cd;
	}

	/**
	 * @param prc_cd
	 */
	public void setPrc_cd(String prc_cd) {
		this.prc_cd = prc_cd;
	}

	/**
	 * @return coil thick
	 */
	@JsonInclude(Include.NON_NULL)
	public String getThk() {
		return thk;
	}

	/**
	 * @param coil thick
	 */
	public void setThk(String thk) {
		this.thk = thk;
	}

	/**
	 * @return coil width
	 */
	@JsonInclude(Include.NON_NULL)
	public String getWdt() {
		return wdt;
	}

	/**
	 * @param coil width
	 */
	public void setWdt(String wdt) {
		this.wdt = wdt;
	}

	/**
	 * @return coil weight
	 */
	@JsonInclude(Include.NON_NULL)
	public String getWgt() {
		return wgt;
	}

	/**
	 * @param coil weight
	 */
	public void setWgt(String wgt) {
		this.wgt = wgt;
	}

	/**
	 * @return start time
	 */
	@JsonInclude(Include.NON_NULL)
	public String getSdt() {
		return sdt;
	}

	/**
	 * @param start time
	 */
	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	/**
	 * @return end time
	 */
	@JsonInclude(Include.NON_NULL)
	public String getEdt() {
		return edt;
	}

	/**
	 * @param end time
	 */
	public void setEdt(String edt) {
		this.edt = edt;
	}

}
