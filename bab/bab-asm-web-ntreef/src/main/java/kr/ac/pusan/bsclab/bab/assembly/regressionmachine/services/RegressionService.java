package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services;

import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann.*;
import kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt.*;

import java.util.HashMap;
import java.util.Map;

import org.nd4j.linalg.activations.Activation;

public class RegressionService {
	ANN_base an;
	String type, machine;
	
	Map<String, Double> mean_val = new HashMap<String, Double>();
	
	public RegressionService(String type, String machine) {
		this.type = type;
		this.machine = machine;
		
		mean_val.put("AN1", 3448.5415);
		mean_val.put("ANA", 2377.5327);
		mean_val.put("CR1", 46.1105);
		mean_val.put("CR2", 38.9565);
		mean_val.put("CR3", 42.6033);
		mean_val.put("CRA", 51.7305);
		mean_val.put("CRB", 139.2985);
		mean_val.put("CRC", 54.7965);
		mean_val.put("PP1", 40.0247);
		mean_val.put("PP2", 29.5907);
		
		mean_val.put("RC1", 35.2588);
		mean_val.put("RC2", 63.5096);
		mean_val.put("RS1", 28.3718);
		mean_val.put("RS2", 48.7199);
		mean_val.put("RS3", 52.7025);
		mean_val.put("RS4", 48.4433);
		mean_val.put("RSA", 32.0131);
		mean_val.put("RSB", 38.6871);
		mean_val.put("RSD", 29.2401);
		mean_val.put("RW1", 44.1397);
		mean_val.put("RW2", 47.0137);
		mean_val.put("RW3", 37.9202);
		mean_val.put("RW4", 49.3070);
		mean_val.put("TW1", 201.9272);
		mean_val.put("TW2", 150.3128);
		
		mean_val.put("SP1", 21.5191);
		mean_val.put("SP2", 24.0109);
		mean_val.put("SP3", 27.4056);
		mean_val.put("SP9", 36.4171);
		mean_val.put("SPA", 18.0569);
		mean_val.put("SPB", 32.9668);
		mean_val.put("SS1", 37.6986);
		mean_val.put("SS2", 60.1513);
		mean_val.put("SS3", 56.6426);
		mean_val.put("SS4", 63.2057);
		mean_val.put("SSA", 57.9279);
		mean_val.put("SSB", 54.0939);
		mean_val.put("SSC", 30.4347);
		mean_val.put("SSD", 29.7473);
		mean_val.put("WS1", 52.1146);
		mean_val.put("WS2", 53.2928);
		mean_val.put("WS3", 61.8574);
		mean_val.put("WS4", 47.2544);
		mean_val.put("WSA", 54.642);
		mean_val.put("PK1", 0.0);
		mean_val.put("PK2", 0.0);
		mean_val.put("PK3", 0.0);
		mean_val.put("PK4", 0.0);
		mean_val.put("PK5", 0.0);
		mean_val.put("PK6", 0.0);
		mean_val.put("PK7", 0.0);
		mean_val.put("PK8", 0.0);
		mean_val.put("PKA", 0.0);
		mean_val.put("PKB", 0.0);
		mean_val.put("PKC", 0.0);
		mean_val.put("PKD", 0.0);
		mean_val.put("PKE", 0.0);
		mean_val.put("PKF", 0.0);
		mean_val.put("PKG", 0.0);
		mean_val.put("PKH", 0.0);
		mean_val.put("PKI", 0.0);
		mean_val.put("PKJ", 0.0);
		
		if(type.equals("NN"))
		{
			MultiLayerNetwork net = new MultiLayerNetwork(new NeuralNetConfiguration.Builder()
	                .weightInit(WeightInit.XAVIER)
	                .list()
	                .layer(0, new DenseLayer.Builder().nIn(3).nOut(20)
	                        .activation(Activation.RELU)
	                        .build())
	                .layer(1, new OutputLayer.Builder(LossFunctions.LossFunction.MSE)
	                        .activation(Activation.RELU)
	                        .nIn(20).nOut(1).build()).build()	
	        );
			net.init();
			setWeightNN(machine, net);
		}
		else { // DT directly set in method
			
		}
	}
	
    public void setWeightNN(String machine, MultiLayerNetwork net){
    	if(machine.equals("AN1")) {
    		an = new ANN_AN1();
    	}
    	else if(machine.equals("ANA")) {
    		an = new ANN_ANA();
    	}
    	else if(machine.equals("CR1")) {
    		an = new ANN_CR1();
    	}
    	else if(machine.equals("CR2")) {
    		an = new ANN_CR2();
    	}
    	else if(machine.equals("CR3")) {
    		an = new ANN_CR3();
    	}
    	else if(machine.equals("CRA")) {
    		an = new ANN_CRA();
    	}
    	else if(machine.equals("CRB")) {
    		an = new ANN_CRB();
    	}
    	else if(machine.equals("CRC")) {
    		an = new ANN_CRC();
    	}
    	else if(machine.equals("PP1")) {
    		an = new ANN_PP1();
    	}
    	else if(machine.equals("PP2")) {
    		an = new ANN_PP2();
    	}
    	else if(machine.equals("SP1")) {
    		an = new ANN_SP1();
    	}
    	else if(machine.equals("SP2")) {
    		an = new ANN_SP2();
    	}
    	else if(machine.equals("SP3")) {
    		an = new ANN_SP3();
    	}
    	else if(machine.equals("SP9")) {
    		an = new ANN_SP9();
    	}
    	else if(machine.equals("SPA")) {
    		an = new ANN_SP1();
    	}
    	else if(machine.equals("SPB")) {
    		an = new ANN_SP2();
    	}
    	else if(machine.equals("SS1")) {
    		an = new ANN_SS1();
    	}
    	else if(machine.equals("SS2")) {
    		an = new ANN_SS2();
    	}
    	else if(machine.equals("SS3")) {
    		an = new ANN_SS3();
    	}
    	else if(machine.equals("SS4")) {
    		an = new ANN_SS4();
    	}
    	else if(machine.equals("SSA")) {
    		an = new ANN_SSA();
    	}
    	else if(machine.equals("SSB")) {
    		an = new ANN_SSB();
    	}
    	else if(machine.equals("SSC")) {
    		an = new ANN_SSC();
    	}
    	else if(machine.equals("SSD")) {
    		an = new ANN_SSD();
    	}
    	else if(machine.equals("WS1")) {
    		an = new ANN_WS1();
    	}
    	else if(machine.equals("WS2")) {
    		an = new ANN_WS2();
    	}
    	else if(machine.equals("WS3")) {
    		an = new ANN_WS3();
    	}
    	else if(machine.equals("WS4")) {
    		an = new ANN_WS4();
    	}
    	else if(machine.equals("WSA")) {
    		an = new ANN_WSA();
    	}
    	else if(machine.startsWith("PK")) {
    		an = new ANN_PK_Others();
    	}
    	else an = null;
    	
    	try{
    		an.setWeight(net);
    	}
    	catch(NullPointerException e) {
    		
    	}
    }
    
    private double getDurationFromNN(double thick, double width, double weight) {
    	if(an == null)
    		return getDurationFromMean(machine);
    	
    	return an.getOutput(thick, width, weight);
    	
    }
    
    private double getDurationFromMean(String machine) {
    	//System.out.println(machine);
    	return mean_val.get(machine);
    }
    
    private double getDurationFromDT(double thick, double width, double weight, String plancode) {
    	DT_base dt = null;
    	
    	if(machine.equals("AN1")) {
    		dt = new DT_AN1();
    	}
    	else if(machine.equals("ANA")) {
    		dt = new DT_ANA();
    	}
    	else if(machine.equals("CR1")) {
    		dt = new DT_CR1();
    	}
    	else if(machine.equals("CR2")) {
    		dt = new DT_CR2();
    	}
    	else if(machine.equals("CR3")) {
    		dt = new DT_CR3();
    	}
    	else if(machine.equals("CRA")) {
    		dt = new DT_CRA();
    	}
    	else if(machine.equals("CRB")) {
    		dt = new DT_CRB();
    	}
    	else if(machine.equals("CRC")) {
    		dt = new DT_CRC();
    	}
    	else if(machine.equals("PP1")) {
    		dt = new DT_PP1();
    	}
    	else if(machine.equals("PP2")) {
    		dt = new DT_PP2();
    	}
    	else if(machine.equals("SP1")) {
    		dt = new DT_SP1();
    	}
    	else if(machine.equals("SP2")) {
    		dt = new DT_SP2();
    	}
    	else if(machine.equals("SP3")) {
    		dt = new DT_SP3();
    	}
    	else if(machine.equals("SP9")) {
    		dt = new DT_SP9();
    	}
    	else if(machine.equals("SPA")) { // data related with this machine is not yet mined
    		dt = new DT_SP1(); // -> trained with 
    	}
    	else if(machine.equals("SPB")) { // data related with this machine is not yet mined
    		dt = new DT_SP2();
    	}
    	else if(machine.equals("SS1")) {
    		dt = new DT_SS1();
    	}
    	else if(machine.equals("SS2")) {
    		dt = new DT_SS2();
    	}
    	else if(machine.equals("SS3")) {
    		dt = new DT_SS3();
    	}
    	else if(machine.equals("SS4")) {
    		dt = new DT_SS4();
    	}
    	else if(machine.equals("SSA")) {
    		dt = new DT_SSA();
    	}
    	else if(machine.equals("SSB")) {
    		dt = new DT_SSB();
    	}
    	else if(machine.equals("SSC")) {
    		dt = new DT_SSC();
    	}
    	else if(machine.equals("SSD")) {
    		dt = new DT_SSD();
    	}
    	else if(machine.equals("WS1")) {
    		dt = new DT_WS1();
    	}
    	else if(machine.equals("WS2")) {
    		dt = new DT_WS2();
    	}
    	else if(machine.equals("WS3")) {
    		dt = new DT_WS3();
    	}
    	else if(machine.equals("WS4")) {
    		dt = new DT_WS4();
    	}
    	else if(machine.equals("WSA")) {
    		dt = new DT_WSA();
    	}
    	else if(machine.startsWith("PK")) {
    		dt = new DT_PK_Others();
    	}
    	else
    		return getDurationFromMean(machine);
    	
    	return dt.getDuration(thick, width, weight, plancode);
    }
    
    public double getDuration(double thick, double width, double weight, String plancode) {
    	if(type.equals("NN"))
    	{
    		double dur = getDurationFromNN(thick, width, weight);
    		return (dur > getDurationFromMean(machine)*2 || dur < getDurationFromMean(machine)/2) ? getDurationFromMean(machine) : dur;
    	}
    	else if(type.equals("MEAN"))
    		return getDurationFromMean(machine);
    	else
    		return getDurationFromDT(thick, width, weight, plancode);
    }
}
