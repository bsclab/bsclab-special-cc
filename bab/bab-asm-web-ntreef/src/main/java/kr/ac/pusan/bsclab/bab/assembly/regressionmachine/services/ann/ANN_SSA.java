package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SSA implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-1.40160215e+00, -2.78799176e+00, -1.16136777e+00, -7.42433429e-01
            				, 8.24790001e-01, -5.09390783e+00, 1.43759041e+01, 1.75495744e+00
            				, -6.07242107e+00, -4.44548225e+00, -6.56956005e+00, -1.21468055e+00
            				, -5.12078905e+00, 1.75965548e-01, 1.59182966e+00, -1.42717683e+00
            				, 2.68886452e+01, -1.08224952e+00, -1.03002796e+01, -2.47292089e+00}, 
            			{4.74538475e-01, 6.31848991e-01, 1.42885840e+00, -1.24713933e+00
        					, -2.25617319e-01, 2.19026065e+00, -2.77772522e+00, 1.84466124e+00
        					, 2.07814738e-01, -1.37399030e+00, -4.88668889e-01, -4.45291884e-02
        					, 2.81148285e-01, -7.41587400e-01, 1.95251405e-02, -1.28212702e+00
        					, 1.29186988e+00, -1.51899964e-01, 5.44043072e-02, -3.45719004e+00},
            			{-4.88057464e-01, 1.74787629e+00, -1.34837818e+00, -8.10756862e-01
    						, -2.05581069e-01, -1.10686088e+00, -5.13874531e-01, -1.04159462e+00
    						, 2.37347126e+00, 2.01947019e-01, -6.78623378e-01, 6.46818578e-01
    						, -9.58829939e-01, -1.27735762e-02, -1.57789290e+00, -1.73871064e+00
    						, 2.52079487e+00, -1.60058141e+00, 1.05089319e+00, -3.30881071e+00}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-0.56629968, -0.26903951, -0.01318006, -0.17051864, -0.50918853, 0.3310895
            			, 0.37459198, -1.44932866, 1.12972641, 1.04910982, 0.77482677, -1.84999084
            			, 0.91021359, -0.48649099, -0.90485865, -0.45383847, 0.17413171, -0.63023478
            			, 1.04716563, 0.74742013};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.10936847,
            			0.48586249,
            			0.01295781,
            			0.64343572,
            			1.68579841,
            			0.49736589,
            			-0.91906351,
            			-0.63949853,
            			0.479846 ,
            			0.16568321,
            			0.72758281,
            			0.12921278,
            			0.25680307,
            			-1.20492232,
            			0.6511302,
            			0.07863989,
            			-1.61907268,
            			-0.20823798,
            			2.65952277,
            			0.19852668};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.16282181};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 1.4;
	}

	@Override
	public double getMaxWidth() {
		
		return 400.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 4932.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.2;
	}

	@Override
	public double getMinWidth() {
		
		return 10.0;
	}

	@Override
	public double getMinWeight() {
		
		return 50.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
