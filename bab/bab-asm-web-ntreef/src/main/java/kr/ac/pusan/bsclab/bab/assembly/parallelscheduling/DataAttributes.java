package kr.ac.pusan.bsclab.bab.assembly.parallelscheduling;

public class DataAttributes {
	public String coil;
	public String histcoino;
	public double thick;
	public double width;
	public double weight;
	public String linemap;
	public String spec_code;
	public String grd_cd;
    public String prdknd_cd;
    public String stl_cd;
    public String cus_id;
    public String usr_id;
	public String drtcoi_no;
	public String prc_cd;

	public String jobseq;
}
