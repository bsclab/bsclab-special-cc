package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_CR1 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-1.23997343, -0.304367661, -5.17273998, 0.781651080, 0.553373098, 0.607971072, -1.89000261, 2.44783235, 1.33540237, -53.0120239, -0.503065944e-01, -0.0750994012, -0.704315662, -0.679816067, -1.75450134, -30.47586403, -2.78032827, -0.514331341, -40.99878349, -40.43862381}, 
            			{-2.03303194, -0.163092673, 0.134819016, 0.0753057599, -0.390199959, -2.51925993, 0.166626647, 0.427960724, -0.192150027, -1.38147867, 0.116607703, -1.06050587, 0.420867890, 0.00318015390, -3.02866149, -2.10021305, -0.198434874, -0.258450568, -3.80063677, -2.46003675},
            			{0.961682200, -1.59313166, 0.672818542, -1.63706112, -4.03126287, -1.09095562, 0.252639532, -1.04796588, -0.510770202, 0.0686821416, -0.362098366, -0.811231852, -0.287739396, 0.329945445, -0.190976813, 0.516195118, -1.10247040, -0.926347017, -0.562044501, 1.14719093}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-1.12800264, -0.94694918, 1.45045614, 0.206058, 1.10569394, -0.11864313, -0.26274437, -0.58096099, -1.79246902, 0.4728168, -0.39302334, -0.26633281, -0.17725985, -1.28480983, -0.940763, 0.88709629, -1.44705606, -0.31643945, 1.34355903, -0.11802685};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.83835453, -0.48607892, 0.02710414, -0.46968365, -0.15011621, -1.07070065, 0.13708459, -0.91333264, 0.38638097, 2.03842139, 0.25848615, 0.2621077, 0.96784461, 0.88556969, 0.49006289, 0.66911864, 1.61227071, 0.27004978, 1.07758236, 1.4576478};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.46324793};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 5.05;
	}

	@Override
	public double getMaxWidth() {
		
		return 450.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 7042.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.41;
	}

	@Override
	public double getMinWidth() {
		
		return 200.0;
	}

	@Override
	public double getMinWeight() {
		
		return 1583.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
