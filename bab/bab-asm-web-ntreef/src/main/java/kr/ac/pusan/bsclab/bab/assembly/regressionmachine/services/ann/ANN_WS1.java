package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_WS1 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{3.77808309e+00, 6.37110853e+00, 5.28174686e+00, -5.15395224e-01
            				, 1.40191710e+00, -9.31245983e-01, -9.01669998e+01, 1.16235733e+00
            				, -1.24904320e-01, 1.38517618e+00, 9.38522053e+00, 1.01912558e+00
            				, 3.97242457e-01, 2.95388281e-01, -9.01973322e-02, -1.52919674e+00
            				, 1.08634758e+01, 7.49312162e+00, -2.76218541e-02, 1.08498707e+01}, 
            			{-1.62619889e+00, -1.58008194e+00, 2.72688580e+00, -4.30050761e-01
        					, 3.56154814e-02, 1.37645662e+00, -1.94875860e+00, 8.49345744e-01
        					, 2.84750849e-01, -8.84959102e-01, 5.61334908e-01, 2.61389112e+00
        					, -1.23344171e+00, 1.32142067e+00, 1.51107490e-01, -1.45432723e+00
        					, -2.66497761e-01, -4.56770509e-01, 6.24454627e-03, 3.61139059e-01},
            			{-5.31365834e-02, 8.07781100e-01, 2.82194710e+00, -1.70272803e+00
    						, 2.91706808e-02, 1.11690044e+00, 4.67583704e+00, 6.64024413e-01
    						, 1.66855723e-01, -6.06918335e-01, 4.71864372e-01, 9.45971757e-02
    						, -1.27710211e+00, -1.72514105e+00, -1.29930869e-01, -1.36009896e+00
    						, -5.84768713e-01, 6.60471261e-01, 6.40941441e-01, 7.02957809e-02}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {0.38589984, 0.46351373, -0.42694405, -1.27645874, -0.049389, 0.57195896
            			, 0.32164252, -1.92602992, -1.61606693, -1.52231443, 0.32812238, -0.50854963
            			, -0.47738773, -1.26982081, -0.31828871, 0.04035624, 1.84299016, 0.23194209
            			, -0.85831261, 0.2801142};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {1.78451225e-01,
            			6.30848825e-01,
            			1.51955056e+00,
            			1.01646769e+00,
            			-9.71157029e-02,
            			-2.64946808e-04,
            			-1.14976215e+00,
            			4.42763895e-01,
            			1.00752175e+00,
            			-2.61161506e-01,
            			1.01952791e+00,
            			6.55305326e-01,
            			-8.58297050e-01,
            			-9.57002789e-02,
            			-7.70403802e-01,
            			-4.66087729e-01,
            			1.37451756e+00,
            			6.40691817e-01,
            			3.93842280e-01,
            			1.11467290e+00};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {1.16634583};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 6.2;
	}

	@Override
	public double getMaxWidth() {
		
		return 1250.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 22500.00;
	}

	@Override
	public double getMinThick() {
		
		return 1.76;
	}

	@Override
	public double getMinWidth() {
		
		return 65.0;
	}

	@Override
	public double getMinWeight() {
		
		return 599.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
