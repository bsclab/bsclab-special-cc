package kr.ac.pusan.bsclab.bab.assembly.domain.spark;

public class SparkSubmissionStatus extends SparkRestResult {
    public String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
    
    
}
