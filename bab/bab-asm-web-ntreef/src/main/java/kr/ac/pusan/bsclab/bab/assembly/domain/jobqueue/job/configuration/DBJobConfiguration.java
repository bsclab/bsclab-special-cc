package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration;

public class DBJobConfiguration implements JobConfiguration {
	
	String tmpTableName;
	String csvFilePath;
	
	
	
	public DBJobConfiguration(String tmpTableName, String csvFilePath) {
		super();
		this.tmpTableName = tmpTableName;
		this.csvFilePath = csvFilePath;
	}
	
	public String getTmpTableName() {
		return tmpTableName;
	}
	public String getCsvFilePath() {
		return csvFilePath;
	}
	public void setTmpTableName(String tmpTableName) {
		this.tmpTableName = tmpTableName;
	}
	public void setCsvFilePath(String csvFilePath) {
		this.csvFilePath = csvFilePath;
	}
	
	
	
	
	

}
