package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link;

import com.fasterxml.jackson.annotation.JsonProperty;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.SqoopElement;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;

public class LinkConfigValue {
	
	@JsonProperty("inputs")
	Input[] input;
	String name;
	int id;
	SqoopElement type;
	
	
	
	/**
	 * @param input
	 * @param name
	 * @param id
	 * @param type
	 */
	public LinkConfigValue(Input[] input, String name, int id, SqoopElement type) {
		this.input = input;
		this.name = name;
		this.id = id;
		this.type = type;
	}
	public Input[] getInput() {
		return input;
	}
	public void setInput(Input[] input) {
		this.input = input;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public SqoopElement getType() {
		return type;
	}
	public void setType(SqoopElement type) {
		this.type = type;
	}
	
	
}