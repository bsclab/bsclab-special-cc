package kr.ac.pusan.bsclab.bab.assembly.rserver.param;
import java.util.List;

/**
 * Class for defining entities for response to requester
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */
public class ResponseEntity {

	private String id;
	private String functionName;
	Parameter parameters;
	List<GraphOutput> output;

	/**
	 * @return user id (requester id)
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id (requester id)
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return funtionName, name of function of R-analysis, 
	 * eg. Histogram, correlation so on
	 */
	public String getFunctionName() {
		return functionName;
	}

	/**
	 * @param functionName, name of function of R-analysis, 
	 * eg. Histogram, correlation so on
	 */
	public void setFunctionName(String name) {
		this.functionName = name;
	}

	/**
	 * @return parameters, based on Parameter.java
	 */
	public Parameter getParameters() {
		return parameters;
	}

	/**
	 * @param parameters
	 */
	public void setParameters(Parameter parameters) {
		this.parameters = parameters;
	}

	/**
	 * @return output contains name and directory of graph
	 */
	public List<GraphOutput> getOutput() {
		return output;
	}

	/**
	 * @param output
	 */
	public void setOutput(List<GraphOutput> output) {
		this.output = output;
	}
}
