package kr.ac.pusan.bsclab.bab.assembly.utils.sqoop;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kr.ac.pusan.bsclab.bab.assembly.conf.properties.AppProperties;
import kr.ac.pusan.bsclab.bab.assembly.conf.properties.SqoopProperties;

@Component
public class SqoopCmdMaker {
	
	@Autowired
	SqoopProperties sp;
	
	@Autowired
	AppProperties ap;
	
	public String sqoopCmdMaker(String queueName, String dbDriver, String dbConnStr
			, String dbUser, String dbPw, String query, String outputHdfsDir) {
		
		if (queueName.equals(null)) queueName="default";
		
		String cmd = sp.getSqoopCmd()
				.replace("${QUEUE_NAME}", queueName)
				.replace("${DB_DRIVER}", dbDriver)
				.replace("${CONNECTION_STR}", dbConnStr)
				.replace("${DB_USER}", dbUser)
				.replace("${DB_PW}", dbPw)
				.replace("${QUERY}", query)				
				.replace("${OUTPUT_HDFS_DIR}", outputHdfsDir);
										
		
		return cmd;
	}
	
	public String dbToHdfsQueryBuilder(String resourceTable
			, Timestamp sdt, Timestamp edt) {
		return sp.getDbToHdfsQuery()
						.replace("${SDT}", sdt.toString())
						.replace("${EDT}", edt.toString())
						.replace("${RESOURCETABLE}", ap.getLogTableName());
				
	}

}


//sqoop import \
//-D mapreduce.job.queuename=default \
//--driver 'com.mysql.jdbc.Driver' \
//--connect 'jdbc:mysql://192.168.44.241:3306/test' \
//--table 'user' \
//--username 'root' \
//--password 'dlfdk@75661!' \
//--fields-terminated-by ',' \
//--lines-terminated-by '\n' \
//--optionally-enclosed-by '\"' \
//--num-mappers 1 \
//--enclosed-by "\"" \
//--target-dir /user/hadoop/woojs_workspace/test \
