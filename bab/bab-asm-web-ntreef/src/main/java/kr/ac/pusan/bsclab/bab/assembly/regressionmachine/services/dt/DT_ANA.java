package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;

public class DT_ANA implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("CRA1332")) return 1930;
if(p.equals("CRC1263"))
 if(we <= 2378) return 1872;
 if(we > 2378) return 1860;
if(p.equals("PP12183"))
 if(we <= 4865) return 2915;
 if(we > 4865) return 2732.5;
if(p.equals("PP12184"))
 if(t <= 1)
  if(w <= 322)
   if(t <= 0.91) return 1951;
   if(t > 0.91)
    if(we <= 4264)
     if(t <= 0.95) return 1861;
     if(t > 0.95) return 2215;
    if(we > 4264)
     if(we <= 4330) return 1974;
     if(we > 4330) return 1860;
  if(w > 322)
   if(we <= 4812)
    if(t <= 0.95) return 4650;
    if(t > 0.95)
     if(w <= 343)
      if(w <= 342) return 1912;
      if(w > 342) return 2115;
     if(w > 343) return 2040;
   if(we > 4812)
    if(w <= 357) return 1974;
    if(w > 357) return 2220;
 if(t > 1)
  if(w <= 318)
   if(t <= 1.2)
    if(w <= 307) return 2020;
    if(w > 307) return 1893;
   if(t > 1.2) return 2325;
  if(w > 318)
   if(t <= 1.15) return 1860;
   if(t > 1.15) return 2006;
if(p.equals("PP12186"))
 if(w <= 310)
  if(t <= 1.1)
   if(w <= 215) return 1921;
   if(w > 215)
    if(we <= 4239) return 1997;
    if(we > 4239) return 2153;
  if(t > 1.1) return 1890;
 if(w > 310)
  if(t <= 1.1)
   if(w <= 328)
    if(we <= 4322) return 2195;
    if(we > 4322) return 1963;
   if(w > 328)
    if(w <= 339) return 2120;
    if(w > 339) return 1890;
  if(t > 1.1) return 1860;
if(p.equals("PP12187")) return 1920;
if(p.equals("PP12188"))
 if(w <= 322) return 2010;
 if(w > 322) return 1701;
if(p.equals("PP12190")) return 1836;
if(p.equals("PP12192"))
 if(t <= 0.91)
  if(w <= 315) return 2130;
  if(w > 315) return 1970;
 if(t > 0.91)
  if(w <= 315) return 1895;
  if(w > 315) return 1710;
if(p.equals("PP12193"))
 if(w <= 230)
  if(t <= 1.1)
   if(w <= 220)
    if(we <= 3082.5) return 2076;
    if(we > 3082.5) return 1924;
   if(w > 220) return 1924;
  if(t > 1.1) return 1713;
 if(w > 230)
  if(w <= 241)
   if(we <= 2704) return 1997;
   if(we > 2704) return 2640;
  if(w > 241) return 2360;
if(p.equals("PP12194"))
 if(w <= 316)
  if(w <= 315)
   if(t <= 0.8) return 1990;
   if(t > 0.8) return 1950;
  if(w > 315)
   if(t <= 0.9)
    if(t <= 0.63)
     if(we <= 3726) return 2043;
     if(we > 3726) return 1739;
    if(t > 0.63) return 1880;
   if(t > 0.9)
    if(we <= 3726) return 2040;
    if(we > 3726) return 1985;
 if(w > 316)
  if(t <= 0.8) return 2220;
  if(t > 0.8) return 2280.15;
if(p.equals("PP12195"))
 if(we <= 4388) return 2407;
 if(we > 4388) return 1860;
if(p.equals("PP12196"))
 if(t <= 1.45)
  if(t <= 1.02)
   if(we <= 5148)
    if(we <= 3676) return 1881;
    if(we > 3676) return 1874;
   if(we > 5148) return 2150;
  if(t > 1.02) return 2101;
 if(t > 1.45)
  if(w <= 274) return 1931;
  if(w > 274)
   if(we <= 3495) return 2028;
   if(we > 3495) return 2437;
if(p.equals("PP12197"))
 if(w <= 298)
  if(w <= 237)
   if(we <= 2772) return 2250;
   if(we > 2772) return 2039;
  if(w > 237) return 2198;
 if(w > 298)
  if(w <= 307) return 2495;
  if(w > 307)
   if(w <= 339)
    if(w <= 322) return 2640;
    if(w > 322) return 2076;
   if(w > 339) return 2130;
if(p.equals("PP12198"))
 if(w <= 354)
  if(t <= 1.62)
   if(t <= 1.31)
    if(w <= 338)
     if(w <= 323)
      if(we <= 4186) return 1902;
      if(we > 4186) return 1865;
     if(w > 323)
      if(w <= 326) return 1735;
      if(w > 326) return 1856;
    if(w > 338)
     if(w <= 348) return 1710;
     if(w > 348)
      if(we <= 4735) return 1769;
      if(we > 4735) return 1908;
   if(t > 1.31)
    if(w <= 325)
     if(w <= 316)
      if(we <= 4109) return 1995;
      if(we > 4109) return 1723;
     if(w > 316)
      if(we <= 4289)
       if(we <= 4242) return 2700;
       if(we > 4242) return 2163;
      if(we > 4289) return 2072;
    if(w > 325)
     if(w <= 334)
      if(w <= 333)
       if(we <= 4422) return 1995;
       if(we > 4422) return 1984;
      if(w > 333) return 1935;
     if(w > 334)
      if(we <= 4500) return 1985;
      if(we > 4500) return 1965;
  if(t > 1.62)
   if(w <= 341)
    if(w <= 338)
     if(w <= 324) return 2155;
     if(w > 324) return 1840;
    if(w > 338)
     if(we <= 3515) return 2315;
     if(we > 3515) return 1394;
   if(w > 341)
    if(we <= 3184) return 2155;
    if(we > 3184)
     if(we <= 3352) return 3335;
     if(we > 3352)
      if(we <= 3372) return 1861;
      if(we > 3372)
       if(we <= 3466) return 2100;
       if(we > 3466) return 2910;
 if(w > 354)
  if(w <= 378)
   if(w <= 360)
    if(w <= 358)
     if(we <= 3565)
      if(we <= 3469) return 2058;
      if(we > 3469) return 2155;
     if(we > 3565)
      if(we <= 3774) return 2130;
      if(we > 3774) return 2046;
    if(w > 358)
     if(we <= 3555)
      if(we <= 3446)
       if(we <= 3329) return 1937;
       if(we > 3329) return 2018;
      if(we > 3446)
       if(we <= 3512) return 2230;
       if(we > 3512) return 2018;
     if(we > 3555)
      if(we <= 3988) return 2340;
      if(we > 3988)
       if(we <= 4250) return 2317;
       if(we > 4250) return 2420;
   if(w > 360)
    if(w <= 361)
     if(we <= 3524)
      if(we <= 3368) return 2370;
      if(we > 3368) return 2594;
     if(we > 3524) return 2838;
    if(w > 361)
     if(t <= 1.62)
      if(we <= 3631)
       if(we <= 3565) return 2175;
       if(we > 3565) return 2315;
      if(we > 3631) return 2340;
     if(t > 1.62)
      if(we <= 3676) return 8789;
      if(we > 3676) return 2550;
  if(w > 378)
   if(we <= 3885)
    if(w <= 386)
     if(w <= 379) return 2190;
     if(w > 379)
      if(we <= 3668) return 2056;
      if(we > 3668) return 2169;
    if(w > 386)
     if(w <= 393)
      if(we <= 3778)
       if(we <= 3641) return 1927;
       if(we > 3641) return 3184;
      if(we > 3778)
       if(we <= 3803) return 2206;
       if(we > 3803) return 1942;
     if(w > 393)
      if(we <= 3693) return 2056;
      if(we > 3693) return 1861;
   if(we > 3885)
    if(t <= 1.57)
     if(w <= 393)
      if(we <= 5385) return 2073;
      if(we > 5385) return 2138;
     if(w > 393)
      if(we <= 3922) return 2220;
      if(we > 3922) return 2022;
    if(t > 1.57)
     if(t <= 1.92) return 2160;
     if(t > 1.92) return 2520;
if(p.equals("PP12199"))
 if(t <= 1.5)
  if(w <= 335) return 1990;
  if(w > 335) return 1902;
 if(t > 1.5)
  if(t <= 2.04) return 1900;
  if(t > 2.04) return 1955;
if(p.equals("PP12202"))
 if(t <= 1.82)
  if(we <= 3323) return 2195;
  if(we > 3323) return 1895;
 if(t > 1.82)
  if(t <= 2.07)
   if(w <= 268) return 2145;
   if(w > 268) return 2250;
  if(t > 2.07) return 1977;
if(p.equals("PP12204"))
 if(t <= 2.7) return 1885;
 if(t > 2.7) return 2460;
if(p.equals("PP12205"))
 if(t <= 2.58) return 2016;
 if(t > 2.58) return 2460;
if(p.equals("PP12211"))
 if(t <= 4.35)
  if(we <= 3782)
   if(we <= 3592) return 2716;
   if(we > 3592)
    if(we <= 3722) return 2498.33333333333;
    if(we > 3722) return 2365;
  if(we > 3782)
   if(we <= 5216) return 2460;
   if(we > 5216)
    if(we <= 5262) return 2482.5;
    if(we > 5262) return 2871;
 if(t > 4.35)
  if(we <= 4388)
   if(we <= 3655)
    if(we <= 3542) return 2537.5;
    if(we > 3542) return 2520;
   if(we > 3655)
    if(we <= 3782) return 2610;
    if(we > 3782) return 2491;
  if(we > 4388)
   if(we <= 5262)
    if(we <= 5089) return 2440;
    if(we > 5089)
     if(we <= 5216) return 2790;
     if(we > 5216) return 2730;
   if(we > 5262) return 2472;
if(p.equals("PP12213"))
 if(we <= 4078) return 1739;
 if(we > 4078) return 1738;
if(p.equals("PP12221"))
 if(we <= 3392)
  if(t <= 0.66) return 1865;
  if(t > 0.66) return 2072;
 if(we > 3392)
  if(t <= 0.63) return 2129;
  if(t > 0.63) return 1997;
if(p.equals("PP12222"))
 if(t <= 0.9) return 1792;
 if(t > 0.9) return 1713;
if(p.equals("PP12229"))
 if(w <= 335) return 1741;
 if(w > 335)
  if(we <= 4437) return 1951;
  if(we > 4437)
   if(we <= 4671) return 1865;
   if(we > 4671) return 1773;
if(p.equals("PP12230"))
 if(t <= 1.5)
  if(w <= 329)
   if(we <= 4342) return 2215;
   if(we > 4342) return 2009;
  if(w > 329) return 2100;
 if(t > 1.5)
  if(we <= 4420)
   if(we <= 4140) return 2375;
   if(we > 4140) return 2460;
  if(we > 4420) return 2435;
if(p.equals("PP12232"))
 if(t <= 1.5)
  if(w <= 323) return 2105;
  if(w > 323) return 2045;
 if(t > 1.5) return 2250;
if(p.equals("PP12234"))
 if(t <= 0.8)
  if(w <= 252) return 1890;
  if(w > 252)
   if(w <= 260) return 2125;
   if(w > 260) return 1905;
 if(t > 0.8)
  if(w <= 504) return 1659;
  if(w > 504)
   if(w <= 519) return 1906;
   if(w > 519) return 2220;
if(p.equals("PP12235"))
 if(t <= 0.87) return 1896;
 if(t > 0.87) return 1735;
if(p.equals("PP12237")) return 1836;
if(p.equals("PP12238"))
 if(t <= 2.24)
  if(w <= 357) return 1882;
  if(w > 357) return 1862;
 if(t > 2.24) return 2049;
if(p.equals("PP12245"))
 if(t <= 1.1)
  if(w <= 315)
   if(we <= 4345) return 2035;
   if(we > 4345) return 2134;
  if(w > 315)
   if(w <= 344) return 1746;
   if(w > 344) return 1755;
 if(t > 1.1)
  if(we <= 4293)
   if(we <= 4245) return 2014;
   if(we > 4245) return 1790;
  if(we > 4293)
   if(we <= 4345) return 1895;
   if(we > 4345) return 2142;
if(p.equals("PP12247")) return 2105;
if(p.equals("PP12248"))
 if(t <= 1)
  if(w <= 314)
   if(we <= 4243) return 1768;
   if(we > 4243) return 1880;
  if(w > 314)
   if(we <= 4101) return 13800;
   if(we > 4101) return 1936;
 if(t > 1)
  if(we <= 4155) return 1968;
  if(we > 4155) return 1865;
if(p.equals("PP12249"))
 if(w <= 222) return 2090;
 if(w > 222)
  if(t <= 1.45) return 2260;
  if(t > 1.45) return 1710;
if(p.equals("PP12251"))
 if(w <= 343)
  if(t <= 1.1) return 2177;
  if(t > 1.1)
   if(we <= 3783) return 2320;
   if(we > 3783) return 1741;
 if(w > 343)
  if(we <= 4808) return 1817;
  if(we > 4808)
   if(t <= 1.1) return 2100;
   if(t > 1.1) return 1773;
if(p.equals("PP12252"))
 if(w <= 482) return 1835;
 if(w > 482) return 1730;
if(p.equals("PP12253"))
 if(w <= 269)
  if(w <= 247)
   if(we <= 3104) return 2105;
   if(we > 3104) return 1810;
  if(w > 247)
   if(we <= 3541) return 1910;
   if(we > 3541) return 1896;
 if(w > 269)
  if(we <= 3783) return 1657.61666666667;
  if(we > 3783) return 1761.31666666667;
if(p.equals("PP12256"))
 if(t <= 0.53)
  if(t <= 0.35)
   if(t <= 0.26)
    if(t <= 0.205) return 2120;
    if(t > 0.205)
     if(t <= 0.21) return 1851;
     if(t > 0.21) return 2077;
   if(t > 0.26)
    if(t <= 0.3)
     if(we <= 3010) return 1895;
     if(we > 3010) return 1785;
    if(t > 0.3) return 1695;
  if(t > 0.35)
   if(t <= 0.46)
    if(w <= 313) return 1739;
    if(w > 313) return 1970;
   if(t > 0.46)
    if(we <= 4033)
     if(w <= 313) return 1896;
     if(w > 313) return 1834;
    if(we > 4033)
     if(w <= 312) return 1862;
     if(w > 312) return 2040;
 if(t > 0.53)
  if(t <= 0.95)
   if(t <= 0.8) return 2013;
   if(t > 0.8)
    if(w <= 313) return 1735;
    if(w > 313) return 1960;
  if(t > 0.95)
   if(we <= 4033) return 1959;
   if(we > 4033)
    if(w <= 309) return 1838;
    if(w > 309)
     if(we <= 4312) return 1790;
     if(we > 4312) return 2250;
if(p.equals("PP12257"))
 if(w <= 232)
  if(w <= 209)
   if(w <= 207) return 2500;
   if(w > 207)
    if(we <= 1987)
     if(we <= 1945) return 2765;
     if(we > 1945) return 1861;
    if(we > 1987)
     if(we <= 2112) return 2145;
     if(we > 2112) return 2119;
  if(w > 209)
   if(w <= 214)
    if(we <= 2027) return 2265;
    if(we > 2027)
     if(we <= 2058)
      if(we <= 2039) return 2175.5;
      if(we > 2039)
       if(we <= 2045) return 2145;
       if(we > 2045) return 2056;
     if(we > 2058)
      if(we <= 2084) return 2598;
      if(we > 2084) return 2145;
   if(w > 214)
    if(t <= 1.69)
     if(t <= 1.34) return 1885;
     if(t > 1.34)
      if(we <= 2182) return 2018;
      if(we > 2182) return 2056;
    if(t > 1.69)
     if(w <= 219)
      if(we <= 2144) return 2101;
      if(we > 2144) return 1815;
     if(w > 219) return 2244;
 if(w > 232)
  if(t <= 1.69)
   if(t <= 1.45)
    if(t <= 1.36)
     if(w <= 242) return 2272;
     if(w > 242) return 2320;
    if(t > 1.36)
     if(w <= 235)
      if(we <= 2351) return 2480;
      if(we > 2351) return 2190;
     if(w > 235)
      if(we <= 2232) return 2145;
      if(we > 2232) return 1955;
   if(t > 1.45)
    if(t <= 1.62)
     if(w <= 304) return 1908;
     if(w > 304) return 1770;
    if(t > 1.62)
     if(w <= 233)
      if(we <= 2298)
       if(we <= 2156) return 2068;
       if(we > 2156)
        if(we <= 2225) return 2176;
        if(we > 2225) return 2272;
      if(we > 2298) return 2132;
     if(w > 233)
      if(we <= 2290) return 2010;
      if(we > 2290)
       if(we <= 2450) return 2068;
       if(we > 2450) return 2010;
  if(t > 1.69)
   if(w <= 241)
    if(we <= 2827)
     if(we <= 2368) return 1942;
     if(we > 2368)
      if(we <= 2572) return 2317;
      if(we > 2572) return 2280;
    if(we > 2827) return 2760;
   if(w > 241)
    if(t <= 1.87)
     if(w <= 282) return 1862;
     if(w > 282) return 2028;
    if(t > 1.87)
     if(we <= 2411) return 2160;
     if(we > 2411) return 2340;
if(p.equals("PP12262")) return 2120;
if(p.equals("PP12263"))
 if(t <= 1.02)
  if(t <= 0.85) return 1930;
  if(t > 0.85) return 2129;
 if(t > 1.02)
  if(we <= 4125) return 3254;
  if(we > 4125) return 1706;
if(p.equals("PP12266"))
 if(we <= 3896)
  if(w <= 253) return 1835;
  if(w > 253) return 1849;
 if(we > 3896) return 1975;
if(p.equals("PP12267"))
 if(t <= 0.95)
  if(t <= 0.86)
   if(w <= 327) return 1760;
   if(w > 327) return 1775;
  if(t > 0.86)
   if(w <= 313) return 2640;
   if(w > 313) return 1823;
 if(t > 0.95)
  if(t <= 1.3)
   if(w <= 327) return 2021;
   if(w > 327) return 1899;
  if(t > 1.3)
   if(w <= 313) return 2371;
   if(w > 313) return 1867;
if(p.equals("PP12273")) return 2142;
if(p.equals("PP12275"))
 if(w <= 238)
  if(t <= 1.54)
   if(we <= 2426)
    if(we <= 2194) return 6675;
    if(we > 2194)
     if(we <= 2356) return 2400;
     if(we > 2356) return 2010;
   if(we > 2426)
    if(we <= 2490) return 2160;
    if(we > 2490) return 2075;
  if(t > 1.54) return 2326;
 if(w > 238)
  if(t <= 1.65)
   if(we <= 2345)
    if(we <= 2338) return 2340;
    if(we > 2338) return 2288;
   if(we > 2345)
    if(we <= 2416) return 2146;
    if(we > 2416)
     if(we <= 2450) return 2340;
     if(we > 2450) return 2280;
  if(t > 1.65)
   if(we <= 2832) return 2569;
   if(we > 2832) return 2592;
if(p.equals("PP12280"))
 if(t <= 1.61) return 1893;
 if(t > 1.61) return 2305;
if(p.equals("PP12282"))
 if(t <= 1)
  if(we <= 4185) return 1885;
  if(we > 4185) return 2180;
 if(t > 1)
  if(t <= 1.6) return 2098;
  if(t > 1.6)
   if(w <= 315) return 2460;
   if(w > 315) return 2255;
if(p.equals("PP12283"))
 if(t <= 1.87)
  if(t <= 1.31)
   if(w <= 344)
    if(we <= 4372) return 1989;
    if(we > 4372) return 1908;
   if(w > 344) return 1895;
  if(t > 1.31)
   if(w <= 357) return 1891;
   if(w > 357) return 1871;
 if(t > 1.87)
  if(we <= 4340)
   if(w <= 331) return 2213;
   if(w > 331) return 2099;
  if(we > 4340)
   if(t <= 2.3)
    if(w <= 344) return 2320;
    if(w > 344) return 2375;
   if(t > 2.3) return 2460;
if(p.equals("PP12284"))
 if(t <= 2.63)
  if(w <= 293.5) return 2207;
  if(w > 293.5) return 2211;
 if(t > 2.63) return 2088;
if(p.equals("PP12285"))
 if(t <= 1.61)
  if(w <= 340) return 2006;
  if(w > 340) return 1895;
 if(t > 1.61)
  if(w <= 340) return 2135;
  if(w > 340) return 2520;
if(p.equals("PP12286"))
 if(t <= 1.61)
  if(w <= 331)
   if(w <= 263) return 1910;
   if(w > 263) return 1865;
  if(w > 331)
   if(w <= 342) return 2285;
   if(w > 342) return 1895;
 if(t > 1.61)
  if(w <= 331) return 2250;
  if(w > 331)
   if(w <= 342) return 2335;
   if(w > 342) return 2520;
if(p.equals("PP12287"))
 if(t <= 1.7) return 1985;
 if(t > 1.7) return 2610;
if(p.equals("PP12296")) return 1908;
if(p.equals("PP12297"))
 if(t <= 1.55) return 1989;
 if(t > 1.55) return 2255;
if(p.equals("PP12298")) return 1860;
if(p.equals("PP12302"))
 if(t <= 2.16)
  if(t <= 1.62) return 1882.91666666667;
  if(t > 1.62) return 2018;
 if(t > 2.16)
  if(t <= 2.9) return 2345;
  if(t > 2.9) return 2019;
if(p.equals("PP12306")) return 2190;
if(p.equals("PP12307")) return 1755;
if(p.equals("PP12308")) return 2176;
if(p.equals("PP12313"))
 if(we <= 2360)
  if(t <= 1.69)
   if(t <= 1.5) return 2340;
   if(t > 1.5)
    if(we <= 2254)
     if(we <= 2196) return 2125;
     if(we > 2196) return 2097;
    if(we > 2254)
     if(we <= 2289) return 2237;
     if(we > 2289) return 2166;
  if(t > 1.69)
   if(we <= 2006) return 2160;
   if(we > 2006) return 2340;
 if(we > 2360)
  if(t <= 1.5)
   if(we <= 2817)
    if(we <= 2450) return 2070;
    if(we > 2450) return 2279;
   if(we > 2817)
    if(t <= 1.17) return 2138;
    if(t > 1.17) return 2045;
  if(t > 1.5)
   if(t <= 1.69)
    if(w <= 232) return 2175;
    if(w > 232) return 2400;
   if(t > 1.69)
    if(we <= 2442) return 2400;
    if(we > 2442) return 2279;
if(p.equals("PP12319")) return 1926;
if(p.equals("PP12324"))
 if(t <= 1.8) return 2243.5;
 if(t > 1.8)
  if(we <= 2536) return 2480;
  if(we > 2536) return 2160;
if(p.equals("PP12325"))
 if(w <= 285) return 1826;
 if(w > 285)
  if(t <= 1.01) return 12340;
  if(t > 1.01) return 1853;
if(p.equals("PP12326"))
 if(t <= 0.91)
  if(we <= 3351)
   if(w <= 237) return 2155;
   if(w > 237) return 13865;
  if(we > 3351) return 2100;
 if(t > 0.91)
  if(t <= 1.1)
   if(w <= 233) return 2087;
   if(w > 233) return 1845;
  if(t > 1.1)
   if(w <= 249) return 1849;
   if(w > 249) return 1826;
if(p.equals("PP12327"))
 if(t <= 1.02)
  if(t <= 0.66)
   if(w <= 241)
    if(we <= 2993) return 1950;
    if(we > 2993) return 2109;
   if(w > 241)
    if(w <= 248) return 1985;
    if(w > 248) return 1940;
  if(t > 0.66)
   if(w <= 243) return 1905;
   if(w > 243)
    if(w <= 249) return 1920;
    if(w > 249)
     if(w <= 256) return 2220;
     if(w > 256) return 1920;
 if(t > 1.02)
  if(w <= 249)
   if(w <= 243) return 1840;
   if(w > 243) return 1730;
  if(w > 249)
   if(w <= 256) return 1899;
   if(w > 256) return 1773;
if(p.equals("PP12328"))
 if(t <= 1.02)
  if(t <= 0.66)
   if(we <= 4267)
    if(w <= 309) return 1886;
    if(w > 309)
     if(w <= 321) return 1940;
     if(w > 321) return 1950;
   if(we > 4267)
    if(t <= 0.46) return 1841;
    if(t > 0.46)
     if(we <= 4350) return 1816;
     if(we > 4350) return 2490;
  if(t > 0.66)
   if(we <= 4360)
    if(w <= 322) return 13865;
    if(w > 322) return 1760;
   if(we > 4360)
    if(we <= 4631) return 12340;
    if(we > 4631) return 1760;
 if(t > 1.02)
  if(we <= 4360)
   if(w <= 316) return 1890;
   if(w > 316)
    if(we <= 4183) return 1906;
    if(we > 4183) return 1891;
  if(we > 4360)
   if(we <= 4631) return 1741;
   if(we > 4631) return 1663;
if(p.equals("PP12329"))
 if(t <= 0.66) return 1910;
 if(t > 0.66)
  if(t <= 1.1) return 1675;
  if(t > 1.1) return 1941;
if(p.equals("PP12330")) return 1975;
if(p.equals("PP12332"))
 if(t <= 1.1)
  if(t <= 0.95)
   if(w <= 339) return 1730;
   if(w > 339) return 1860;
  if(t > 0.95) return 2063;
 if(t > 1.1)
  if(t <= 1.25)
   if(w <= 305) return 2090;
   if(w > 305) return 1963;
  if(t > 1.25) return 2181;
if(p.equals("PP12336")) return 2240;
if(p.equals("PP12337"))
 if(t <= 2.8)
  if(w <= 249)
   if(t <= 2.56)
    if(we <= 3170) return 2158;
    if(we > 3170) return 2390;
   if(t > 2.56) return 1920;
  if(w > 249)
   if(w <= 368)
    if(w <= 310) return 2157;
    if(w > 310) return 2211;
   if(w > 368) return 2200;
 if(t > 2.8)
  if(t <= 3.29)
   if(w <= 319) return 2198;
   if(w > 319) return 3705;
  if(t > 3.29)
   if(t <= 3.35) return 2087.5;
   if(t > 3.35) return 2105;
if(p.equals("PP12340"))
 if(w <= 225)
  if(w <= 206) return 2180;
  if(w > 206)
   if(w <= 223)
    if(we <= 2578) return 1950;
    if(we > 2578) return 2300;
   if(w > 223) return 1963;
 if(w > 225)
  if(w <= 243)
   if(w <= 241) return 2098;
   if(w > 241)
    if(we <= 3315) return 1865;
    if(we > 3315) return 2054;
  if(w > 243)
   if(w <= 317) return 1727;
   if(w > 317) return 2180;
if(p.equals("PP12342"))
 if(t <= 1.1) return 1860;
 if(t > 1.1) return 1964;
if(p.equals("PP12344")) return 1900;
if(p.equals("PP12349"))
 if(t <= 0.61) return 2025;
 if(t > 0.61) return 2040;
if(p.equals("PP12350")) return 1999;
if(p.equals("PP12351")) return 2640;
if(p.equals("PP12352"))
 if(t <= 0.9)
  if(t <= 0.53) return 1969;
  if(t > 0.53) return 1791;
 if(t > 0.9)
  if(we <= 3023) return 1950;
  if(we > 3023) return 2080;
if(p.equals("PP12353")) return 2138;
if(p.equals("PP12354")) return 2250;
if(p.equals("PP12355")) return 2027;
if(p.equals("PP12356")) return 3343;
if(p.equals("PP12357"))
 if(t <= 1.52)
  if(w <= 330) return 2190;
  if(w > 330) return 2125;
 if(t > 1.52)
  if(we <= 5105) return 1906;
  if(we > 5105) return 2125;
if(p.equals("PP12358"))
 if(w <= 325)
  if(t <= 0.53) return 1832;
  if(t > 0.53)
   if(t <= 0.95) return 2080;
   if(t > 0.95) return 4680;
 if(w > 325) return 1836;
if(p.equals("PP12359"))
 if(t <= 1.99)
  if(t <= 1.97) return 2407;
  if(t > 1.97) return 1900;
 if(t > 1.99) return 2395;
if(p.equals("PP12360"))
 if(t <= 1) return 1905;
 if(t > 1)
  if(t <= 1.22) return 1913;
  if(t > 1.22) return 1853;
if(p.equals("PP12361"))
 if(w <= 335) return 2099;
 if(w > 335) return 1893;
if(p.equals("PP12362")) return 1713;
if(p.equals("PP12363"))
 if(t <= 1)
  if(w <= 222) return 1805;
  if(w > 222) return 2130;
 if(t > 1)
  if(w <= 222) return 1755;
  if(w > 222) return 1746;
if(p.equals("PP12364"))
 if(t <= 0.91) return 1899;
 if(t > 0.91) return 2030;
if(p.equals("PP12366")) return 1970;
if(p.equals("PP12369"))
 if(t <= 2.02)
  if(t <= 1.57)
   if(w <= 371) return 2331;
   if(w > 371) return 2196;
  if(t > 1.57)
   if(w <= 370)
    if(we <= 3577) return 2160;
    if(we > 3577) return 2008;
   if(w > 370) return 2220;
 if(t > 2.02)
  if(w <= 368) return 2199;
  if(w > 368) return 2580;
if(p.equals("PP12370")) return 1861;
if(p.equals("PP12373")) return 2700;
if(p.equals("PP12376")) return 1768;
if(p.equals("PP12377"))
 if(w <= 320) return 1880;
 if(w > 320)
  if(w <= 338) return 2105;
  if(w > 338) return 2220;
if(p.equals("PP12378")) return 2700;
if(p.equals("PP12383")) return 2130;
if(p.equals("PP12384")) return 1861;
if(p.equals("PP12388"))
 if(t <= 1.25) return 2140;
 if(t > 1.25)
  if(t <= 1.87) return 1659;
  if(t > 1.87) return 2528.31666666667;
if(p.equals("PP12389"))
 if(t <= 1.87)
  if(w <= 278) return 2230.28333333333;
  if(w > 278) return 4333;
 if(t > 1.87)
  if(w <= 278) return 4571;
  if(w > 278) return 1720;
if(p.equals("PP12390")) return 2124;
if(p.equals("PP12392")) return 2199;
if(p.equals("PP12393")) return 1862;
if(p.equals("PP12395")) return 1950;
if(p.equals("PP12399"))
 if(w <= 220)
  if(t <= 0.95) return 1834;
  if(t > 0.95) return 2139;
 if(w > 220) return 1856;
if(p.equals("PP12408")) return 1975;
if(p.equals("PP12409")) return 1960;
if(p.equals("PP12410"))
 if(w <= 406)
  if(t <= 2.84)
   if(we <= 3308) return 1836;
   if(we > 3308) return 1870;
  if(t > 2.84)
   if(we <= 3308) return 2129;
   if(we > 3308) return 2186;
 if(w > 406)
  if(t <= 2.56) return 1859;
  if(t > 2.56) return 2081.5;
if(p.equals("PP12411"))
 if(t <= 1.81) return 1935;
 if(t > 1.81) return 2400;
if(p.equals("PP12414")) return 1748;
if(p.equals("PP12421"))
 if(t <= 1.72) return 1867;
 if(t > 1.72) return 1727;
if(p.equals("PP12422"))
 if(we <= 4570) return 1838;
 if(we > 4570) return 2042;
if(p.equals("PP12423")) return 1862;
if(p.equals("PP12424")) return 2087;
if(p.equals("PP12428"))
 if(we <= 3308) return 1960;
 if(we > 3308) return 1975;
if(p.equals("PP12429"))
 if(we <= 3550)
  if(t <= 0.4) return 1995;
  if(t > 0.4) return 1927;
 if(we > 3550) return 1800;
if(p.equals("PP12430")) return 2160;
if(p.equals("PP12431")) return 2580;
if(p.equals("PP12432")) return 2100;
if(p.equals("PP12433"))
 if(w <= 230) return 2008;
 if(w > 230) return 2360;
if(p.equals("PP12435"))
 if(t <= 1.8) return 2066;
 if(t > 1.8)
  if(w <= 240) return 1965;
  if(w > 240) return 2125;
if(p.equals("PP12436"))
 if(t <= 1.99)
  if(t <= 1.7)
   if(t <= 1.57)
    if(we <= 4567) return 2190;
    if(we > 4567) return 2323;
   if(t > 1.57) return 1950;
  if(t > 1.7)
   if(t <= 1.985) return 2075;
   if(t > 1.985) return 2540;
 if(t > 1.99)
  if(t <= 2.25)
   if(w <= 407)
    if(w <= 343)
     if(we <= 3308) return 2470;
     if(we > 3308) return 2416;
    if(w > 343) return 2610;
   if(w > 407)
    if(we <= 4508) return 2460;
    if(we > 4508) return 2393;
  if(t > 2.25)
   if(t <= 2.32)
    if(w <= 367) return 2000;
    if(w > 367)
     if(we <= 4907) return 1865;
     if(we > 4907) return 1980;
   if(t > 2.32) return 2130;
if(p.equals("PP12437"))
 if(t <= 0.95)
  if(w <= 357)
   if(we <= 3949)
    if(w <= 306)
     if(we <= 2383) return 2130;
     if(we > 2383) return 1920;
    if(w > 306)
     if(t <= 0.61) return 1960;
     if(t > 0.61) return 1891;
   if(we > 3949)
    if(we <= 3999) return 1780;
    if(we > 3999)
     if(we <= 4088)
      if(we <= 4041) return 2300;
      if(we > 4041) return 1970;
     if(we > 4088) return 1891;
  if(w > 357)
   if(we <= 5436)
    if(we <= 5174) return 2275;
    if(we > 5174) return 2160;
   if(we > 5436)
    if(t <= 0.86) return 1711;
    if(t > 0.86)
     if(we <= 5507) return 2678;
     if(we > 5507) return 2490;
 if(t > 0.95)
  if(t <= 1.25)
   if(we <= 3964)
    if(t <= 1.1)
     if(w <= 306)
      if(we <= 2383) return 1867;
      if(we > 2383) return 2153;
     if(w > 306) return 2010;
    if(t > 1.1) return 1836;
   if(we > 3964)
    if(w <= 306)
     if(we <= 3986) return 2220;
     if(we > 3986)
      if(we <= 4072) return 1870;
      if(we > 4072) return 1867;
    if(w > 306)
     if(we <= 3986) return 1870;
     if(we > 3986)
      if(we <= 4088) return 1862;
      if(we > 4088) return 1990;
  if(t > 1.25)
   if(we <= 5487)
    if(we <= 5174)
     if(we <= 5062) return 1874;
     if(we > 5062) return 2285;
    if(we > 5174)
     if(we <= 5340) return 1867;
     if(we > 5340) return 2285;
   if(we > 5487)
    if(we <= 5512) return 1870;
    if(we > 5512) return 1865;
if(p.equals("PP12438")) return 2073;
if(p.equals("PP12439")) return 1917;
if(p.equals("PP12440"))
 if(t <= 1.42)
  if(we <= 5332) return 2010;
  if(we > 5332) return 2355;
 if(t > 1.42) return 1905;
if(p.equals("PP12443"))
 if(t <= 1.1) return 1865;
 if(t > 1.1)
  if(w <= 309)
   if(w <= 300) return 1965;
   if(w > 300) return 1723;
  if(w > 309) return 1849;
if(p.equals("PP12444"))
 if(w <= 258)
  if(we <= 3580) return 1698;
  if(we > 3580) return 1804;
 if(w > 258)
  if(t <= 0.4) return 1970;
  if(t > 0.4)
   if(t <= 0.75) return 2300;
   if(t > 0.75) return 1929;
if(p.equals("PP12446"))
 if(w <= 334)
  if(w <= 265) return 1922;
  if(w > 265) return 2030;
 if(w > 334)
  if(t <= 2.51) return 1698;
  if(t > 2.51)
   if(w <= 350) return 2160;
   if(w > 350) return 2370;
if(p.equals("PP12447")) return 1950;
if(p.equals("PP12449"))
 if(t <= 1.82) return 1893;
 if(t > 1.82) return 2105;
if(p.equals("PP12450")) return 2337;
if(p.equals("PP12457")) return 2053.5;
if(p.equals("PP12459")) return 2355;
if(p.equals("PP12460")) return 1865;
if(p.equals("PP12461"))
 if(t <= 1.55)
  if(t <= 1.36) return 2190;
  if(t > 1.36)
   if(we <= 2825) return 1955;
   if(we > 2825)
    if(we <= 2839) return 2322;
    if(we > 2839) return 2150;
 if(t > 1.55)
  if(t <= 1.72)
   if(w <= 209) return 2150;
   if(w > 209) return 2246;
  if(t > 1.72)
   if(t <= 1.85) return 2340;
   if(t > 1.85)
    if(we <= 2401) return 2075;
    if(we > 2401) return 2150;
if(p.equals("PP12462"))
 if(t <= 1.7)
  if(w <= 232)
   if(we <= 2660) return 2525;
   if(we > 2660) return 2235;
  if(w > 232)
   if(we <= 2254)
    if(we <= 2220) return 2310;
    if(we > 2220) return 2225;
   if(we > 2254)
    if(t <= 1.54) return 2060;
    if(t > 1.54)
     if(we <= 2555) return 2215;
     if(we > 2555) return 2340;
 if(t > 1.7)
  if(w <= 240)
   if(we <= 2386)
    if(we <= 2370) return 2340;
    if(we > 2370) return 2206;
   if(we > 2386) return 2165;
  if(w > 240)
   if(t <= 1.93)
    if(we <= 2665) return 2345;
    if(we > 2665)
     if(we <= 2679) return 2310;
     if(we > 2679) return 1919.5;
   if(t > 1.93) return 2674;
if(p.equals("PP12464")) return 1985;
if(p.equals("PP12466")) return 2550;
if(p.equals("PP12468")) return 1711;
if(p.equals("PP12469"))
 if(t <= 2.02) return 1893;
 if(t > 2.02)
  if(t <= 2.35) return 2460;
  if(t > 2.35) return 1805;
if(p.equals("PP12470"))
 if(we <= 2152) return 1814;
 if(we > 2152) return 1975;
if(p.equals("PP12471")) return 2520;
if(p.equals("PP12477")) return 7440;
if(p.equals("PP12478"))
 if(t <= 2.48) return 2424;
 if(t > 2.48) return 3060;
if(p.equals("PP12479"))
 if(t <= 0.8)
  if(w <= 328) return 1893;
  if(w > 328) return 1950;
 if(t > 0.8)
  if(w <= 328) return 2171;
  if(w > 328) return 7850;
if(p.equals("PP12480"))
 if(w <= 337)
  if(t <= 3.24) return 1940;
  if(t > 3.24) return 2460;
 if(w > 337)
  if(t <= 2.63) return 2291;
  if(t > 2.63) return 2236;
if(p.equals("PP12481")) return 2085;
if(p.equals("PP12484")) return 1761;
if(p.equals("PP12486"))
 if(t <= 2.61)
  if(t <= 2.02)
   if(we <= 1784) return 2123.5;
   if(we > 1784) return 1940;
  if(t > 2.02) return 2370;
 if(t > 2.61)
  if(t <= 3.24)
   if(we <= 1784) return 2260;
   if(we > 1784) return 2025;
  if(t > 3.24)
   if(we <= 3449) return 2100;
   if(we > 3449) return 2495;
if(p.equals("PP12488")) return 2640;
if(p.equals("PP12491")) return 2171;
if(p.equals("PP12499"))
 if(t <= 2.74) return 2035;
 if(t > 2.74) return 2320;
if(p.equals("PP12500"))
 if(w <= 240) return 1701;
 if(w > 240)
  if(t <= 2.1) return 2200;
  if(t > 2.1) return 2180;
if(p.equals("PP12501")) return 2105;
if(p.equals("PP12506")) return 2690;
if(p.equals("PP12508"))
 if(t <= 1.57) return 1965;
 if(t > 1.57) return 2051;
if(p.equals("PP12510"))
 if(t <= 2.19) return 1891;
 if(t > 2.19) return 2280;
if(p.equals("PP12513"))
 if(t <= 3.15) return 1982;
 if(t > 3.15)
  if(we <= 4923) return 2520;
  if(we > 4923) return 2148;
if(p.equals("PP12514"))
 if(t <= 3.24) return 2080;
 if(t > 3.24)
  if(w <= 263) return 2099;
  if(w > 263) return 2280;
if(p.equals("PP12519"))
 if(t <= 1.01) return 1865;
 if(t > 1.01) return 1875;
if(p.equals("PP12521")) return 1989;
if(p.equals("PP12525")) return 1963;
if(p.equals("PP12526"))
 if(t <= 1.62)
  if(w <= 252.5) return 2035;
  if(w > 252.5)
   if(t <= 1.1) return 3076;
   if(t > 1.1) return 2175;
 if(t > 1.62)
  if(w <= 258) return 2335;
  if(w > 258) return 2126;
if(p.equals("PP12528"))
 if(t <= 1.6) return 2063;
 if(t > 1.6) return 2425;
if(p.equals("PP12529"))
 if(t <= 1) return 3240;
 if(t > 1)
  if(t <= 1.6) return 1963;
  if(t > 1.6) return 2255;
if(p.equals("PP12530"))
 if(t <= 0.95) return 1936;
 if(t > 0.95)
  if(t <= 1.6) return 1875;
  if(t > 1.6) return 2175;
if(p.equals("PP12531")) return 1980;
if(p.equals("PP12532")) return 2035;
if(p.equals("PP12533")) return 1910;
if(p.equals("PP12538")) return 2880;
if(p.equals("PP12544"))
 if(w <= 338)
  if(w <= 310) return 2255;
  if(w > 310) return 2505.36666666667;
 if(w > 338) return 2119;
if(p.equals("PP12545"))
 if(t <= 2.46)
  if(t <= 1.94) return 2640;
  if(t > 1.94)
   if(we <= 1868) return 2058;
   if(we > 1868) return 2455;
 if(t > 2.46)
  if(w <= 398)
   if(we <= 3756) return 2018;
   if(we > 3756) return 2224;
  if(w > 398) return 2087;
if(p.equals("PP12549")) return 1972.9;
if(p.equals("PP12552")) return 2096;
if(p.equals("PP12553")) return 2281;
if(p.equals("PP12554")) return 1911;
if(p.equals("PP12556")) return 2143;
if(p.equals("PP12557")) return 2725;
if(p.equals("PP12558")) return 2087;
if(p.equals("PP12559")) return 2087;
if(p.equals("PP12560")) return 2640;
if(p.equals("PP12561")) return 2030;
if(p.equals("PP12562")) return 2640;
if(p.equals("PP12564")) return 2579;
if(p.equals("PP12565")) return 2030;
if(p.equals("PP12566")) return 2135;
if(p.equals("PP12567")) return 2907.5;
if(p.equals("PP12568")) return 2115;
if(p.equals("PP12571")) return 2135;
if(p.equals("PP12581")) return 2323.25;
if(p.equals("PP21894"))
 if(t <= 1.87)
  if(we <= 2240)
   if(w <= 214)
    if(we <= 2057)
     if(we <= 1968) return 2340;
     if(we > 1968) return 2160;
    if(we > 2057)
     if(we <= 2088)
      if(w <= 209) return 2275;
      if(w > 209) return 2469;
     if(we > 2088)
      if(we <= 2146)
       if(we <= 2126) return 2171;
       if(we > 2126) return 2220;
      if(we > 2146)
       if(we <= 2156) return 2190;
       if(we > 2156) return 2160;
   if(w > 214)
    if(t <= 1.7)
     if(t <= 1.54)
      if(t <= 1.36) return 2106;
      if(t > 1.36) return 1929;
     if(t > 1.54)
      if(w <= 232)
       if(we <= 2099) return 2106;
       if(we > 2099) return 2390;
      if(w > 232) return 2265;
    if(t > 1.7)
     if(w <= 218) return 8789;
     if(w > 218)
      if(we <= 2134) return 2075;
      if(we > 2134) return 2260;
  if(we > 2240)
   if(w <= 234)
    if(t <= 1.69)
     if(t <= 1.34)
      if(w <= 215) return 1723;
      if(w > 215) return 2025;
     if(t > 1.34)
      if(w <= 232) return 2500;
      if(w > 232)
       if(we <= 2416) return 1927;
       if(we > 2416)
        if(we <= 2486) return 2010;
        if(we > 2486) return 1990;
    if(t > 1.69)
     if(w <= 209)
      if(w <= 208) return 2275;
      if(w > 208) return 2217;
     if(w > 209)
      if(we <= 2468)
       if(we <= 2460) return 2063;
       if(we > 2460) return 2505;
      if(we > 2468) return 2063;
   if(w > 234)
    if(w <= 235)
     if(we <= 2416)
      if(we <= 2326) return 2383;
      if(we > 2326)
       if(we <= 2342) return 2225;
       if(we > 2342)
        if(we <= 2350) return 1928;
        if(we > 2350) return 1927;
     if(we > 2416)
      if(we <= 2735)
       if(we <= 2694) return 2047;
       if(we > 2694) return 2063;
      if(we > 2735) return 2130;
    if(w > 235)
     if(w <= 241)
      if(t <= 1.36) return 1800;
      if(t > 1.36) return 2036;
     if(w > 241)
      if(t <= 1.42)
       if(we <= 2453)
        if(we <= 2400) return 2225;
        if(we > 2400)
         if(we <= 2426) return 2345;
         if(we > 2426) return 2225;
       if(we > 2453) return 2345;
      if(t > 1.42) return 1965;
 if(t > 1.87)
  if(we <= 2282)
   if(w <= 225)
    if(w <= 214)
     if(w <= 209)
      if(we <= 2064) return 2180;
      if(we > 2064) return 1956;
     if(w > 209)
      if(t <= 2.84)
       if(we <= 2104)
        if(we <= 2051) return 2280;
        if(we > 2051)
         if(we <= 2088) return 3446;
         if(we > 2088) return 2190;
       if(we > 2104)
        if(we <= 2127)
         if(we <= 2115) return 2057.5;
         if(we > 2115) return 2250;
        if(we > 2127)
         if(we <= 2156) return 3724.51666666667;
         if(we > 2156) return 2280;
      if(t > 2.84)
       if(we <= 1984) return 2099;
       if(we > 1984) return 3030;
    if(w > 214)
     if(w <= 218)
      if(we <= 2134) return 2394;
      if(we > 2134) return 2220.53333333333;
     if(w > 218)
      if(we <= 2134)
       if(we <= 2042) return 2079;
       if(we > 2042) return 2525;
      if(we > 2134) return 2283;
   if(w > 225)
    if(t <= 2.84)
     if(w <= 234)
      if(w <= 232) return 2193;
      if(w > 232)
       if(we <= 2221) return 2700;
       if(we > 2221) return 2280;
     if(w > 234)
      if(w <= 238)
       if(w <= 235) return 2370;
       if(w > 235) return 2425;
      if(w > 238)
       if(we <= 2216) return 2280;
       if(we > 2216) return 2220;
    if(t > 2.84) return 2340;
  if(we > 2282)
   if(we <= 2608)
    if(w <= 232)
     if(t <= 2.84) return 2370;
     if(t > 2.84) return 2300;
    if(w > 232)
     if(t <= 2.84)
      if(w <= 238)
       if(we <= 2326)
        if(we <= 2308)
         if(we <= 2298) return 2107;
         if(we > 2298) return 2151;
        if(we > 2308)
         if(we <= 2314) return 2140;
         if(we > 2314) return 2220;
       if(we > 2326)
        if(w <= 234) return 1998;
        if(w > 234)
         if(we <= 2350)
          if(we <= 2337) return 2107;
          if(we > 2337) return 2140;
         if(we > 2350) return 2195;
      if(w > 238)
       if(we <= 2453)
        if(we <= 2426) return 2370;
        if(we > 2426) return 2140;
       if(we > 2453) return 2099;
     if(t > 2.84)
      if(we <= 2338) return 2340;
      if(we > 2338) return 3446;
   if(we > 2608)
    if(w <= 235)
     if(w <= 228) return 2210;
     if(w > 228) return 2820;
    if(w > 235)
     if(t <= 2.31)
      if(t <= 2.15) return 2108;
      if(t > 2.15) return 2294;
     if(t > 2.31)
      if(w <= 253) return 2465;
      if(w > 253) return 2395;
if(p.equals("PP21895"))
 if(we <= 4404)
  if(t <= 3.2)
   if(t <= 2.25)
    if(w <= 388)
     if(w <= 313)
      if(w <= 312) return 2220;
      if(w > 312) return 2160;
     if(w > 313)
      if(w <= 315) return 1997;
      if(w > 315) return 2025;
    if(w > 388)
     if(w <= 413) return 2124;
     if(w > 413) return 2160;
   if(t > 2.25)
    if(w <= 313)
     if(w <= 312) return 2013;
     if(w > 312) return 2127;
    if(w > 313)
     if(t <= 2.7) return 2100;
     if(t > 2.7)
      if(w <= 315) return 1997;
      if(w > 315)
       if(w <= 430) return 2460;
       if(w > 430) return 2130;
  if(t > 3.2)
   if(t <= 4.25)
    if(w <= 312) return 2339;
    if(w > 312)
     if(t <= 3.74) return 2032;
     if(t > 3.74)
      if(w <= 315)
       if(we <= 3524) return 2040;
       if(we > 3524) return 2032;
      if(w > 315) return 2545;
   if(t > 4.25)
    if(w <= 377) return 2118;
    if(w > 377) return 2234;
 if(we > 4404)
  if(t <= 3.44)
   if(t <= 2.45)
    if(we <= 4710) return 2039;
    if(we > 4710) return 1981;
   if(t > 2.45) return 2187;
  if(t > 3.44)
   if(t <= 4.25) return 2400;
   if(t > 4.25) return 2400.08333333333;
if(p.equals("PP21901")) return 2075;
if(p.equals("PP21904"))
 if(t <= 3.94)
  if(w <= 283)
   if(t <= 2.93)
    if(t <= 2.78)
     if(w <= 232)
      if(t <= 2.63)
       if(t <= 2.41) return 2250;
       if(t > 2.41) return 1790;
      if(t > 2.63)
       if(t <= 2.7) return 2160;
       if(t > 2.7) return 1870;
     if(w > 232)
      if(t <= 2.68)
       if(we <= 2536) return 1895;
       if(we > 2536) return 1950;
      if(t > 2.68) return 2280;
    if(t > 2.78)
     if(w <= 255)
      if(t <= 2.84) return 7480;
      if(t > 2.84)
       if(w <= 247)
        if(we <= 2787) return 1740;
        if(we > 2787) return 1986;
       if(w > 247) return 2110;
     if(w > 255)
      if(w <= 260)
       if(we <= 2404) return 2030.13333333333;
       if(we > 2404) return 1980;
      if(w > 260)
       if(w <= 271)
        if(t <= 2.88) return 2010;
        if(t > 2.88)
         if(we <= 2847) return 2010;
         if(we > 2847) return 1870;
       if(w > 271) return 2026;
   if(t > 2.93)
    if(t <= 3.14)
     if(we <= 2932)
      if(w <= 241) return 1869;
      if(w > 241) return 2010;
     if(we > 2932)
      if(we <= 3088)
       if(we <= 3031) return 1870;
       if(we > 3031) return 1802;
      if(we > 3088)
       if(we <= 3280) return 1802;
       if(we > 3280) return 1925;
    if(t > 3.14)
     if(w <= 272)
      if(t <= 3.34)
       if(we <= 3263) return 2160;
       if(we > 3263) return 1770;
      if(t > 3.34) return 2162;
     if(w > 272) return 2700;
  if(w > 283)
   if(we <= 3480)
    if(w <= 290)
     if(we <= 3166)
      if(t <= 3.14)
       if(we <= 3005) return 2205;
       if(we > 3005) return 2100;
      if(t > 3.14)
       if(we <= 3026) return 1800;
       if(we > 3026) return 1823;
     if(we > 3166) return 1909;
    if(w > 290)
     if(w <= 301)
      if(t <= 3.14)
       if(t <= 3) return 2017;
       if(t > 3) return 1841.66666666667;
      if(t > 3.14)
       if(t <= 3.24)
        if(we <= 3099) return 2552;
        if(we > 3099) return 1947;
       if(t > 3.24) return 2190;
     if(w > 301)
      if(t <= 3.59) return 2083;
      if(t > 3.59) return 2010;
   if(we > 3480)
    if(w <= 323)
     if(w <= 301)
      if(w <= 290) return 1857;
      if(w > 290) return 1925;
     if(w > 301)
      if(w <= 309) return 1805;
      if(w > 309)
       if(t <= 3.46) return 2006.95;
       if(t > 3.46) return 1727;
    if(w > 323)
     if(w <= 358)
      if(w <= 339)
       if(we <= 3960) return 2220;
       if(we > 3960)
        if(w <= 328) return 2280;
        if(w > 328) return 1802;
      if(w > 339) return 2130;
     if(w > 358)
      if(t <= 3.77)
       if(we <= 4328) return 1962.5;
       if(we > 4328) return 1828;
      if(t > 3.77)
       if(we <= 4016) return 1836;
       if(we > 4016)
        if(we <= 4467) return 1770;
        if(we > 4467) return 2220;
 if(t > 3.94)
  if(t <= 4.7)
   if(w <= 283)
    if(t <= 4.25)
     if(we <= 2636)
      if(we <= 2404) return 2090;
      if(we > 2404) return 2621;
     if(we > 2636) return 2400;
    if(t > 4.25)
     if(w <= 251)
      if(w <= 237)
       if(w <= 221) return 2280;
       if(w > 221) return 2290;
      if(w > 237)
       if(w <= 243) return 2055;
       if(w > 243)
        if(we <= 3352) return 2149;
        if(we > 3352) return 2193;
     if(w > 251)
      if(w <= 265)
       if(w <= 261) return 2234;
       if(w > 261) return 2621;
      if(w > 265)
       if(w <= 278) return 2057;
       if(w > 278) return 2310;
   if(w > 283)
    if(t <= 4.04)
     if(we <= 3235)
      if(we <= 3116)
       if(we <= 3010) return 2352.5;
       if(we > 3010) return 2210;
      if(we > 3116) return 2640;
     if(we > 3235) return 2051;
    if(t > 4.04)
     if(we <= 3308)
      if(we <= 3214)
       if(we <= 2957) return 2018;
       if(we > 2957)
        if(we <= 3082)
         if(w <= 290) return 2202;
         if(w > 290) return 2520;
        if(we > 3082) return 2202;
      if(we > 3214) return 2292.66666666667;
     if(we > 3308)
      if(t <= 4.35)
       if(we <= 3728) return 1857;
       if(we > 3728) return 2190;
      if(t > 4.35)
       if(w <= 290)
        if(we <= 3371) return 2090;
        if(we > 3371) return 2141;
       if(w > 290)
        if(w <= 295) return 2112;
        if(w > 295) return 2115;
  if(t > 4.7)
   if(w <= 301)
    if(we <= 3247)
     if(we <= 2908)
      if(w <= 232)
       if(w <= 223) return 2788.5;
       if(w > 223) return 2375;
      if(w > 232)
       if(w <= 237)
        if(we <= 2536) return 2580;
        if(we > 2536) return 4088;
       if(w > 237)
        if(w <= 268) return 2220;
        if(w > 268) return 2090;
     if(we > 2908)
      if(w <= 268)
       if(w <= 263)
        if(w <= 246) return 2350;
        if(w > 246) return 2070;
       if(w > 263)
        if(we <= 2936) return 2580;
        if(we > 2936)
         if(we <= 3088) return 4088;
         if(we > 3088) return 2070;
      if(w > 268)
       if(w <= 281) return 2160;
       if(w > 281) return 2090;
    if(we > 3247)
     if(we <= 3366)
      if(w <= 272) return 2460;
      if(w > 272) return 2220;
     if(we > 3366) return 2400;
   if(w > 301)
    if(w <= 343)
     if(w <= 328)
      if(w <= 318)
       if(we <= 3424)
        if(we <= 3299) return 2164;
        if(we > 3299) return 2095;
       if(we > 3424)
        if(we <= 3604) return 2875.2;
        if(we > 3604) return 2410;
      if(w > 318)
       if(we <= 3992) return 2452;
       if(we > 3992) return 2164;
     if(w > 328)
      if(w <= 335)
       if(w <= 331) return 2070;
       if(w > 331) return 2019;
      if(w > 335) return 2124;
    if(w > 343)
     if(w <= 358) return 6780;
     if(w > 358)
      if(we <= 4291) return 2486;
      if(we > 4291) return 2164;
if(p.equals("PP21906"))
 if(t <= 2.46)
  if(t <= 1.65) return 5403;
  if(t > 1.65) return 2640;
 if(t > 2.46)
  if(we <= 5735)
   if(we <= 5691) return 2440;
   if(we > 5691) return 2300;
  if(we > 5735)
   if(we <= 5786)
    if(w <= 482)
     if(we <= 5760) return 2700;
     if(we > 5760) return 2162;
    if(w > 482) return 2700;
   if(we > 5786) return 2400;
if(p.equals("PP21909")) return 1890;
if(p.equals("PP21911"))
 if(t <= 3.46)
  if(t <= 3)
   if(t <= 2.25)
    if(w <= 255)
     if(w <= 240) return 2140;
     if(w > 240)
      if(t <= 1.98) return 2080;
      if(t > 1.98) return 2495;
    if(w > 255)
     if(t <= 1.72) return 1865;
     if(t > 1.72)
      if(we <= 2912) return 2245;
      if(we > 2912) return 2186;
   if(t > 2.25)
    if(t <= 2.69)
     if(t <= 2.41) return 2400;
     if(t > 2.41) return 2560;
    if(t > 2.69)
     if(w <= 244) return 2193;
     if(w > 244) return 1882;
  if(t > 3)
   if(t <= 3.24)
    if(t <= 3.2) return 1945;
    if(t > 3.2)
     if(w <= 259) return 2820;
     if(w > 259)
      if(we <= 3290) return 2359;
      if(we > 3290) return 2407;
   if(t > 3.24)
    if(we <= 5232)
     if(w <= 319) return 2362.5;
     if(w > 319)
      if(we <= 4996) return 2734;
      if(we > 4996) return 2653.41666666667;
    if(we > 5232)
     if(we <= 5270) return 2691;
     if(we > 5270) return 2579.41666666667;
 if(t > 3.46)
  if(w <= 326)
   if(t <= 3.74)
    if(w <= 253)
     if(w <= 240) return 2235;
     if(w > 240) return 2070;
    if(w > 253) return 2099;
   if(t > 3.74) return 2400;
  if(w > 326)
   if(t <= 5.2)
    if(t <= 4.5)
     if(we <= 4638) return 1912.66666666667;
     if(we > 4638) return 1891;
    if(t > 4.5)
     if(we <= 4638) return 2240;
     if(we > 4638) return 2131;
   if(t > 5.2)
    if(we <= 5232) return 2401.33333333333;
    if(we > 5232)
     if(we <= 5270) return 2551;
     if(we > 5270) return 2580;
if(p.equals("PP21912"))
 if(t <= 2.46)
  if(w <= 241)
   if(t <= 1.67)
    if(t <= 1.36)
     if(we <= 2274) return 2012;
     if(we > 2274) return 2029.75;
    if(t > 1.36)
     if(we <= 2786) return 2036;
     if(we > 2786) return 2265;
   if(t > 1.67) return 3210;
  if(w > 241)
   if(w <= 253)
    if(we <= 2412)
     if(we <= 2326) return 2070;
     if(we > 2326) return 2130;
    if(we > 2412) return 2065;
   if(w > 253) return 2730;
 if(t > 2.46)
  if(t <= 2.84)
   if(we <= 4669) return 2138.5;
   if(we > 4669) return 2505;
  if(t > 2.84)
   if(w <= 482) return 2460;
   if(w > 482)
    if(we <= 5060)
     if(we <= 4752) return 2099;
     if(we > 4752) return 2138;
    if(we > 5060)
     if(we <= 5997) return 2310;
     if(we > 5997) return 2302;
if(p.equals("PP21913"))
 if(t <= 2.15)
  if(w <= 357)
   if(we <= 3557)
    if(w <= 341)
     if(we <= 3328)
      if(we <= 3092) return 2277;
      if(we > 3092) return 2555;
     if(we > 3328)
      if(t <= 1.72)
       if(we <= 3487) return 2009;
       if(we > 3487) return 1394;
      if(t > 1.72) return 2080;
    if(w > 341)
     if(we <= 3398)
      if(w <= 342) return 2100;
      if(w > 342) return 2324;
     if(we > 3398)
      if(we <= 3515)
       if(we <= 3454) return 2097;
       if(we > 3454) return 2530;
      if(we > 3515) return 2091;
   if(we > 3557)
    if(t <= 1.62)
     if(w <= 331)
      if(t <= 1.31) return 1900;
      if(t > 1.31)
       if(we <= 3976) return 2313;
       if(we > 3976) return 1984;
     if(w > 331)
      if(w <= 334) return 2003;
      if(w > 334) return 2100;
    if(t > 1.62)
     if(t <= 1.67)
      if(w <= 335) return 1840;
      if(w > 335) return 2090;
     if(t > 1.67)
      if(w <= 329) return 1723;
      if(w > 329) return 2540;
  if(w > 357)
   if(w <= 380)
    if(we <= 3908)
     if(t <= 1.7)
      if(w <= 358) return 1975;
      if(w > 358)
       if(t <= 1.57)
        if(w <= 370) return 2107;
        if(w > 370) return 2400;
       if(t > 1.57)
        if(we <= 3512)
         if(w <= 359) return 3065;
         if(w > 359) return 2169;
        if(we > 3512)
         if(we <= 3666)
          if(we <= 3566) return 2083;
          if(we > 3566) return 2400;
         if(we > 3666) return 2166;
     if(t > 1.7)
      if(we <= 3553) return 2395;
      if(we > 3553)
       if(we <= 3592) return 2175;
       if(we > 3592) return 2163;
    if(we > 3908)
     if(t <= 1.62)
      if(w <= 379)
       if(w <= 373)
        if(t <= 1.52) return 1738;
        if(t > 1.52) return 1910;
       if(w > 373) return 1786;
      if(w > 379)
       if(we <= 4770) return 2225;
       if(we > 4770) return 2352;
     if(t > 1.62)
      if(we <= 4439)
       if(t <= 1.7)
        if(w <= 359)
         if(we <= 4182) return 2400;
         if(we > 4182) return 2171;
        if(w > 359) return 2037;
       if(t > 1.7) return 2400;
      if(we > 4439) return 2057;
   if(w > 380)
    if(w <= 393)
     if(we <= 3918)
      if(w <= 387) return 2022;
      if(w > 387)
       if(we <= 3789)
        if(we <= 3752) return 1937;
        if(we > 3752) return 1927;
       if(we > 3789) return 2149;
     if(we > 3918)
      if(we <= 3980)
       if(we <= 3950)
        if(we <= 3938) return 2171;
        if(we > 3938) return 2340;
       if(we > 3950) return 2171;
      if(we > 3980)
       if(we <= 4010) return 1929;
       if(we > 4010) return 2002;
    if(w > 393)
     if(t <= 1.67)
      if(we <= 3756) return 2079;
      if(we > 3756) return 2056;
     if(t > 1.67)
      if(w <= 428) return 2260;
      if(w > 428)
       if(we <= 5003) return 2346.5;
       if(we > 5003) return 2335;
 if(t > 2.15)
  if(t <= 2.74)
   if(t <= 2.63)
    if(t <= 2.31)
     if(t <= 2.25)
      if(w <= 319)
       if(we <= 3619) return 2580;
       if(we > 3619)
        if(we <= 3734) return 2530;
        if(we > 3734) return 2305;
      if(w > 319)
       if(w <= 382) return 3045;
       if(w > 382)
        if(w <= 407) return 2411;
        if(w > 407) return 2640;
     if(t > 2.25) return 2235;
    if(t > 2.31)
     if(t <= 2.56)
      if(we <= 5316) return 1795;
      if(we > 5316) return 1720;
     if(t > 2.56)
      if(w <= 335)
       if(w <= 331) return 2343;
       if(w > 331) return 2164;
      if(w > 335)
       if(we <= 4996)
        if(w <= 350) return 2445;
        if(w > 350) return 2320;
       if(we > 4996) return 2313;
   if(t > 2.63)
    if(w <= 358)
     if(we <= 3352)
      if(w <= 341)
       if(we <= 3160) return 2099;
       if(we > 3160) return 2370;
      if(w > 341)
       if(w <= 342)
        if(we <= 3223) return 2044;
        if(we > 3223) return 2350;
       if(w > 342) return 2099;
     if(we > 3352)
      if(we <= 3619)
       if(w <= 341)
        if(we <= 3439)
         if(we <= 3418) return 2151;
         if(we > 3418) return 2140;
        if(we > 3439)
         if(we <= 3476) return 2126;
         if(we > 3476) return 2099;
       if(w > 341)
        if(w <= 342)
         if(we <= 3472)
          if(we <= 3434) return 2460;
          if(we > 3434) return 2303;
         if(we > 3472)
          if(we <= 3498) return 2107;
          if(we > 3498) return 1998;
        if(w > 342)
         if(we <= 3577)
          if(we <= 3550) return 2126;
          if(we > 3550) return 2460;
         if(we > 3577)
          if(we <= 3601) return 2099;
          if(we > 3601) return 2126;
      if(we > 3619)
       if(we <= 3912)
        if(we <= 3650) return 2520;
        if(we > 3650) return 2099;
       if(we > 3912)
        if(we <= 4182) return 2300;
        if(we > 4182) return 2286;
    if(w > 358)
     if(w <= 380)
      if(w <= 369)
       if(w <= 360)
        if(we <= 3527) return 2524;
        if(we > 3527)
         if(we <= 3566) return 2250;
         if(we > 3566) return 1956;
       if(w > 360)
        if(we <= 3622) return 2151;
        if(we > 3622) return 2220;
      if(w > 369) return 2520;
     if(w > 380)
      if(we <= 3928)
       if(w <= 393)
        if(w <= 387) return 2079;
        if(w > 387)
         if(we <= 3789)
          if(we <= 3752) return 2323;
          if(we > 3752) return 1956;
         if(we > 3789)
          if(we <= 3880) return 2490;
          if(we > 3880) return 2088.26666666667;
       if(w > 393)
        if(we <= 3695) return 2191;
        if(we > 3695) return 2417;
      if(we > 3928)
       if(we <= 3990)
        if(we <= 3950) return 2015;
        if(we > 3950)
         if(we <= 3970) return 2284.5;
         if(we > 3970) return 2250;
       if(we > 3990)
        if(w <= 393)
         if(we <= 4010) return 2283;
         if(we > 4010) return 2107;
        if(w > 393) return 2417;
  if(t > 2.74)
   if(t <= 3.4)
    if(t <= 3.09)
     if(w <= 369)
      if(w <= 350) return 2134;
      if(w > 350)
       if(w <= 359)
        if(we <= 4145) return 2138;
        if(we > 4145) return 2302;
       if(w > 359) return 2040;
     if(w > 369)
      if(we <= 3592) return 2698;
      if(we > 3592) return 1940;
    if(t > 3.09)
     if(we <= 3860)
      if(w <= 324) return 2320;
      if(w > 324) return 2387.5;
     if(we > 3860)
      if(we <= 4770) return 2520;
      if(we > 4770) return 2360;
   if(t > 3.4)
    if(t <= 3.74)
     if(w <= 340)
      if(w <= 306) return 2520;
      if(w > 306) return 2730.5;
     if(w > 340)
      if(w <= 393) return 2044;
      if(w > 393)
       if(w <= 428)
        if(we <= 4970) return 2220;
        if(we > 4970) return 2520;
       if(w > 428) return 2223;
    if(t > 3.74)
     if(w <= 345)
      if(w <= 317) return 2258;
      if(w > 317)
       if(we <= 3734) return 2190;
       if(we > 3734) return 2315;
     if(w > 345)
      if(w <= 405) return 2107;
      if(w > 405)
       if(w <= 418) return 2126;
       if(w > 418) return 2190;
if(p.equals("PP21914"))
 if(t <= 2.36)
  if(t <= 2.33)
   if(t <= 2.21)
    if(t <= 2.18)
     if(w <= 389)
      if(t <= 2.07)
       if(we <= 3574)
        if(w <= 367) return 2280;
        if(w > 367) return 2424;
       if(we > 3574) return 2960;
      if(t > 2.07)
       if(w <= 379) return 2940;
       if(w > 379) return 2047;
     if(w > 389)
      if(t <= 2.07) return 2130;
      if(t > 2.07)
       if(we <= 4051)
        if(we <= 3878) return 2511;
        if(we > 3878) return 2087;
       if(we > 4051)
        if(we <= 4130) return 2280;
        if(we > 4130) return 2130;
    if(t > 2.18)
     if(we <= 3571) return 2340;
     if(we > 3571) return 2661;
   if(t > 2.21)
    if(w <= 396)
     if(w <= 387)
      if(w <= 378) return 2460;
      if(w > 378)
       if(we <= 3580) return 2051;
       if(we > 3580) return 2823;
     if(w > 387)
      if(t <= 2.3) return 3150;
      if(t > 2.3) return 2047;
    if(w > 396)
     if(t <= 2.28)
      if(w <= 397) return 2154;
      if(w > 397)
       if(w <= 416) return 2162;
       if(w > 416) return 2020;
     if(t > 2.28)
      if(we <= 3840) return 2578;
      if(we > 3840)
       if(we <= 4155)
        if(we <= 4000) return 2794;
        if(we > 4000) return 2470;
       if(we > 4155) return 2794;
  if(t > 2.33)
   if(w <= 403)
    if(we <= 3566) return 2215;
    if(we > 3566) return 2170;
   if(w > 403)
    if(w <= 404)
     if(we <= 3930)
      if(we <= 3541)
       if(we <= 3434) return 2220;
       if(we > 3434) return 2210;
      if(we > 3541)
       if(we <= 3681) return 2200;
       if(we > 3681) return 2342;
     if(we > 3930)
      if(we <= 4104) return 11369;
      if(we > 4104) return 5302;
    if(w > 404) return 2223;
 if(t > 2.36)
  if(w <= 406)
   if(w <= 402)
    if(t <= 2.41)
     if(w <= 399)
      if(we <= 3789)
       if(we <= 3606) return 2190;
       if(we > 3606) return 2177;
      if(we > 3789) return 2190;
     if(w > 399) return 2460;
    if(t > 2.41)
     if(t <= 2.51)
      if(t <= 2.43) return 2169;
      if(t > 2.43) return 2342;
     if(t > 2.51)
      if(we <= 3586) return 2765;
      if(we > 3586)
       if(w <= 401) return 2465;
       if(w > 401) return 2220;
   if(w > 402)
    if(we <= 3818)
     if(we <= 3645)
      if(we <= 3436)
       if(we <= 3380) return 2765;
       if(we > 3380) return 2681;
      if(we > 3436) return 2760;
     if(we > 3645)
      if(we <= 3787)
       if(we <= 3735)
        if(we <= 3671) return 2790;
        if(we > 3671) return 9460;
       if(we > 3735) return 2900.06666666667;
      if(we > 3787) return 2790;
    if(we > 3818)
     if(we <= 3996)
      if(we <= 3893)
       if(we <= 3867) return 2970;
       if(we > 3867) return 2310;
      if(we > 3893)
       if(we <= 3930) return 2820;
       if(we > 3930) return 3030;
     if(we > 3996)
      if(w <= 405) return 2681;
      if(w > 405) return 2791;
  if(w > 406)
   if(w <= 432)
    if(w <= 414)
     if(w <= 410)
      if(w <= 407) return 2830;
      if(w > 407) return 3420;
     if(w > 410)
      if(w <= 413) return 2465;
      if(w > 413) return 2276;
    if(w > 414)
     if(t <= 2.45)
      if(w <= 417) return 2632.3;
      if(w > 417)
       if(t <= 2.4) return 2830;
       if(t > 2.4) return 2280;
     if(t > 2.45) return 2830;
   if(w > 432)
    if(t <= 2.5)
     if(we <= 4330)
      if(t <= 2.45) return 2020;
      if(t > 2.45)
       if(we <= 4264)
        if(we <= 4132) return 2020;
        if(we > 4132) return 2100;
       if(we > 4264) return 2774;
     if(we > 4330)
      if(we <= 4515) return 2389;
      if(we > 4515) return 2465;
    if(t > 2.5)
     if(t <= 2.6)
      if(w <= 435) return 2823;
      if(w > 435) return 2880;
     if(t > 2.6) return 2105;
if(p.equals("PP21915"))
 if(w <= 251)
  if(w <= 217) return 2320;
  if(w > 217)
   if(t <= 2.73)
    if(t <= 2.63)
     if(w <= 234) return 2280;
     if(w > 234) return 2162;
    if(t > 2.63) return 2190;
   if(t > 2.73) return 2460;
 if(w > 251)
  if(w <= 264.5)
   if(t <= 2.7)
    if(we <= 2820) return 2023;
    if(we > 2820)
     if(we <= 2902) return 2534;
     if(we > 2902) return 2400;
   if(t > 2.7)
    if(t <= 2.78) return 2550;
    if(t > 2.78) return 2878;
  if(w > 264.5)
   if(t <= 2.93)
    if(we <= 2860) return 2310;
    if(we > 2860) return 2193;
   if(t > 2.93) return 2400;
if(p.equals("PP21917"))
 if(t <= 3.3)
  if(w <= 355)
   if(w <= 314)
    if(t <= 3.24) return 2193;
    if(t > 3.24)
     if(we <= 3155) return 2552;
     if(we > 3155) return 1899;
   if(w > 314)
    if(t <= 2.28)
     if(w <= 349) return 1830;
     if(w > 349) return 1813;
    if(t > 2.28)
     if(t <= 2.91) return 2190;
     if(t > 2.91)
      if(we <= 3636) return 1877.5;
      if(we > 3636) return 2049;
  if(w > 355)
   if(w <= 399)
    if(w <= 393)
     if(t <= 2.1) return 2550;
     if(t > 2.1)
      if(we <= 4912) return 2400;
      if(we > 4912) return 2640;
    if(w > 393)
     if(we <= 4601) return 1828;
     if(we > 4601) return 1986;
   if(w > 399)
    if(we <= 5174) return 1740;
    if(we > 5174)
     if(t <= 2.7) return 1808;
     if(t > 2.7) return 1950;
 if(t > 3.3)
  if(t <= 3.77)
   if(w <= 374)
    if(w <= 309) return 2072;
    if(w > 309)
     if(we <= 3741) return 1895;
     if(we > 3741) return 2006.95;
   if(w > 374)
    if(w <= 399) return 2365;
    if(w > 399) return 2074;
  if(t > 3.77)
   if(we <= 3378)
    if(w <= 302) return 2290;
    if(w > 302) return 2520;
   if(we > 3378)
    if(w <= 313) return 2310;
    if(w > 313)
     if(w <= 364)
      if(we <= 3676)
       if(we <= 3612) return 2465;
       if(we > 3612) return 2343;
      if(we > 3676)
       if(we <= 3904) return 3090;
       if(we > 3904) return 2141;
     if(w > 364)
      if(t <= 4.25) return 2400;
      if(t > 4.25)
       if(w <= 429) return 2331;
       if(w > 429) return 2465;
if(p.equals("PP21918")) return 2045;
if(p.equals("PP21920"))
 if(t <= 3.22)
  if(t <= 2.41)
   if(t <= 1.87)
    if(t <= 1.5)
     if(t <= 1.45) return 2371;
     if(t > 1.45) return 1872;
    if(t > 1.5)
     if(w <= 270) return 1723;
     if(w > 270) return 1890;
   if(t > 1.87)
    if(t <= 2.25) return 2285;
    if(t > 2.25) return 2297;
  if(t > 2.41)
   if(t <= 2.56)
    if(w <= 236)
     if(t <= 2.52)
      if(we <= 2988) return 2153;
      if(we > 2988) return 2163;
     if(t > 2.52) return 2122;
    if(w > 236)
     if(we <= 3483)
      if(w <= 243) return 1940;
      if(w > 243) return 1907;
     if(we > 3483)
      if(w <= 310) return 2008;
      if(w > 310) return 2072;
   if(t > 2.56)
    if(w <= 261)
     if(t <= 2.61) return 2343;
     if(t > 2.61)
      if(w <= 249) return 1765;
      if(w > 249) return 2585;
    if(w > 261)
     if(t <= 2.61) return 2198;
     if(t > 2.61)
      if(t <= 2.88)
       if(w <= 280) return 1932;
       if(w > 280) return 1727;
      if(t > 2.88) return 2026;
 if(t > 3.22)
  if(t <= 4.35)
   if(t <= 3.4)
    if(w <= 225)
     if(t <= 3.27) return 2006;
     if(t > 3.27) return 2080;
    if(w > 225)
     if(t <= 3.34) return 2140;
     if(t > 3.34) return 2277;
   if(t > 3.4)
    if(t <= 3.79)
     if(w <= 229)
      if(we <= 2786) return 1840;
      if(we > 2786) return 2310;
     if(w > 229) return 2420;
    if(t > 3.79)
     if(t <= 4.04)
      if(w <= 238) return 2310;
      if(w > 238)
       if(w <= 280) return 2250;
       if(w > 280)
        if(w <= 292.5) return 2265;
        if(w > 292.5) return 2460;
     if(t > 4.04) return 1880;
  if(t > 4.35)
   if(w <= 235)
    if(w <= 224.5)
     if(w <= 214) return 2553;
     if(w > 214) return 2117;
    if(w > 224.5)
     if(w <= 229)
      if(w <= 225) return 2400;
      if(w > 225) return 2279;
     if(w > 229) return 2303;
   if(w > 235)
    if(t <= 4.7)
     if(w <= 257)
      if(we <= 2904) return 2430;
      if(we > 2904)
       if(w <= 244) return 2006;
       if(w > 244) return 2190;
     if(w > 257)
      if(we <= 3634)
       if(w <= 280) return 2280;
       if(w > 280) return 2313;
      if(we > 3634)
       if(w <= 300) return 2430;
       if(w > 300) return 2097;
    if(t > 4.7)
     if(w <= 327) return 2220;
     if(w > 327) return 2313;
if(p.equals("PP21922"))
 if(t <= 3.2)
  if(w <= 259)
   if(we <= 3074)
    if(we <= 2568)
     if(t <= 2.45) return 2028;
     if(t > 2.45) return 2316;
    if(we > 2568)
     if(w <= 231) return 1970;
     if(w > 231) return 2196;
   if(we > 3074)
    if(t <= 2.46)
     if(w <= 249) return 2026;
     if(w > 249) return 1955;
    if(t > 2.46)
     if(w <= 249) return 1939;
     if(w > 249) return 1935;
  if(w > 259)
   if(w <= 266)
    if(t <= 2.45)
     if(we <= 2935) return 2025;
     if(we > 2935) return 2250;
    if(t > 2.45)
     if(we <= 2952) return 2076;
     if(we > 2952) return 2893;
   if(w > 266)
    if(t <= 2.1)
     if(w <= 310) return 1836;
     if(w > 310) return 2431;
    if(t > 2.1)
     if(t <= 2.56)
      if(w <= 370) return 1950;
      if(w > 370) return 1895;
     if(t > 2.56)
      if(w <= 310) return 1829;
      if(w > 310)
       if(we <= 3396) return 2025;
       if(we > 3396) return 2100;
 if(t > 3.2)
  if(t <= 4.25)
   if(t <= 3.74)
    if(w <= 370) return 1965;
    if(w > 370) return 1795;
   if(t > 3.74)
    if(w <= 310)
     if(we <= 3101)
      if(we <= 2935) return 2177;
      if(we > 2935) return 2012;
     if(we > 3101) return 2590;
    if(w > 310) return 2445;
  if(t > 4.25)
   if(t <= 5.2)
    if(w <= 249) return 2472;
    if(w > 249)
     if(w <= 523.5) return 2019;
     if(w > 523.5) return 2640;
   if(t > 5.2)
    if(w <= 370) return 2286;
    if(w > 370) return 2141;
if(p.equals("PP21923"))
 if(w <= 343)
  if(t <= 3.48)
   if(w <= 314)
    if(w <= 301)
     if(t <= 2.36) return 1972.9;
     if(t > 2.36) return 2131;
    if(w > 301)
     if(t <= 1.86) return 2371;
     if(t > 1.86)
      if(t <= 2.41) return 2910;
      if(t > 2.41) return 1883;
   if(w > 314)
    if(t <= 3.09)
     if(t <= 2.8) return 2130;
     if(t > 2.8) return 1965;
    if(t > 3.09)
     if(t <= 3.27)
      if(w <= 333) return 2154.5;
      if(w > 333)
       if(w <= 334) return 1936;
       if(w > 334)
        if(we <= 4604) return 2100;
        if(we > 4604) return 1936;
     if(t > 3.27) return 2190;
  if(t > 3.48)
   if(w <= 330)
    if(t <= 4.25)
     if(w <= 327) return 2325;
     if(w > 327) return 2250;
    if(t > 4.25)
     if(w <= 313)
      if(we <= 3962) return 2095;
      if(we > 3962) return 2089;
     if(w > 313) return 2235;
   if(w > 330)
    if(w <= 337)
     if(we <= 4212) return 2171;
     if(we > 4212)
      if(w <= 335) return 2293;
      if(w > 335) return 2400;
    if(w > 337)
     if(t <= 4) return 1995;
     if(t > 4) return 2430;
 if(w > 343)
  if(we <= 4712)
   if(t <= 3.77)
    if(t <= 3.5)
     if(t <= 3.24) return 2585;
     if(t > 3.24) return 2422.5;
    if(t > 3.5)
     if(w <= 370) return 2380;
     if(w > 370) return 2039;
   if(t > 3.77)
    if(t <= 4.25) return 2250;
    if(t > 4.25) return 2205;
  if(we > 4712)
   if(w <= 355)
    if(we <= 4761) return 2580;
    if(we > 4761)
     if(w <= 351)
      if(we <= 4863)
       if(t <= 3.55) return 1770;
       if(t > 3.55) return 2400;
      if(we > 4863)
       if(we <= 4893) return 1880;
       if(we > 4893) return 2580;
     if(w > 351)
      if(t <= 3.35) return 2109;
      if(t > 3.35) return 2611;
   if(w > 355)
    if(w <= 408)
     if(w <= 389)
      if(t <= 2.45) return 1861;
      if(t > 2.45) return 2518;
     if(w > 389)
      if(t <= 3) return 1935;
      if(t > 3) return 2580;
    if(w > 408)
     if(t <= 2.84)
      if(t <= 2.02) return 2250;
      if(t > 2.02) return 2160;
     if(t > 2.84)
      if(we <= 5847) return 2246;
      if(we > 5847) return 2445;
if(p.equals("PP21926"))
 if(t <= 1.02)
  if(t <= 0.61) return 2074;
  if(t > 0.61) return 1740;
 if(t > 1.02)
  if(t <= 1.65) return 1886;
  if(t > 1.65) return 2460;
if(p.equals("PP21927"))
 if(t <= 1.78)
  if(w <= 326) return 2165;
  if(w > 326) return 2125;
 if(t > 1.78)
  if(w <= 326)
   if(we <= 4488)
    if(we <= 4264) return 2220;
    if(we > 4264) return 2235;
   if(we > 4488) return 2191;
  if(w > 326) return 2910;
if(p.equals("PP21931"))
 if(t <= 1.25)
  if(t <= 1) return 1860;
  if(t > 1)
   if(w <= 336) return 1783;
   if(w > 336) return 2165;
 if(t > 1.25)
  if(t <= 1.75)
   if(w <= 336) return 1775;
   if(w > 336) return 1808;
  if(t > 1.75)
   if(we <= 5212) return 2450;
   if(we > 5212) return 2154;
if(p.equals("PP21932"))
 if(w <= 330)
  if(t <= 0.75) return 1916;
  if(t > 0.75) return 1768;
 if(w > 330)
  if(t <= 0.4) return 1836;
  if(t > 0.4)
   if(t <= 0.75) return 1748;
   if(t > 0.75) return 1939;
if(p.equals("PP21933"))
 if(t <= 1.61)
  if(we <= 4866) return 1738;
  if(we > 4866) return 1727;
 if(t > 1.61)
  if(we <= 4866) return 2340;
  if(we > 4866) return 2258;
if(p.equals("PP21935"))
 if(w <= 311) return 1913;
 if(w > 311) return 2109;
if(p.equals("PP21936"))
 if(w <= 295)
  if(t <= 2.63) return 1880;
  if(t > 2.63) return 2265.5;
 if(w > 295) return 2190;
if(p.equals("PP21937"))
 if(we <= 4375) return 2405;
 if(we > 4375)
  if(we <= 4879) return 1895;
  if(we > 4879) return 2870;
if(p.equals("PP21938")) return 2320;
if(p.equals("PP21939"))
 if(w <= 295)
  if(t <= 1.76) return 1775;
  if(t > 1.76) return 2154;
 if(w > 295) return 1838;
if(p.equals("PP21940"))
 if(t <= 2.48)
  if(we <= 4474)
   if(t <= 2.21)
    if(w <= 339)
     if(t <= 1.85)
      if(w <= 338) return 2460;
      if(w > 338)
       if(t <= 1.77) return 2525;
       if(t > 1.77) return 2410;
     if(t > 1.85) return 2330;
    if(w > 339)
     if(w <= 386)
      if(t <= 2.03) return 2010;
      if(t > 2.03) return 3790;
     if(w > 386)
      if(we <= 3059) return 2089.5;
      if(we > 3059) return 2185;
   if(t > 2.21)
    if(t <= 2.3)
     if(we <= 4308)
      if(we <= 4257) return 2307;
      if(we > 4257) return 2410;
     if(we > 4308)
      if(we <= 4381) return 2307;
      if(we > 4381) return 2400;
    if(t > 2.3) return 1895;
  if(we > 4474)
   if(we <= 4873)
    if(w <= 357)
     if(w <= 356)
      if(t <= 1.85) return 2238;
      if(t > 1.85)
       if(t <= 2.18) return 2640;
       if(t > 2.18) return 2115;
     if(w > 356) return 2407;
    if(w > 357)
     if(w <= 374)
      if(we <= 4788) return 2520;
      if(we > 4788) return 2576.23333333333;
     if(w > 374)
      if(t <= 2.25) return 2080;
      if(t > 2.25)
       if(t <= 2.32) return 2085;
       if(t > 2.32) return 2006;
   if(we > 4873)
    if(t <= 2.14)
     if(we <= 5073)
      if(t <= 2.03)
       if(t <= 1.87) return 2576.23333333333;
       if(t > 1.87) return 2540;
      if(t > 2.03) return 2760;
     if(we > 5073) return 2855;
    if(t > 2.14)
     if(w <= 362)
      if(w <= 359) return 2640;
      if(w > 359)
       if(we <= 4952) return 2470;
       if(we > 4952) return 2542;
     if(w > 362)
      if(w <= 414)
       if(we <= 5406) return 2700;
       if(we > 5406) return 2510;
      if(w > 414) return 2640;
 if(t > 2.48)
  if(w <= 370)
   if(w <= 356)
    if(w <= 338)
     if(w <= 328)
      if(we <= 4274) return 2140;
      if(we > 4274) return 2095;
     if(w > 328)
      if(t <= 3.09) return 2290;
      if(t > 3.09)
       if(we <= 4580) return 2166;
       if(we > 4580) return 2445;
    if(w > 338)
     if(w <= 339)
      if(we <= 2618) return 2150.75;
      if(we > 2618) return 2460;
     if(w > 339) return 2267;
   if(w > 356)
    if(we <= 4845)
     if(w <= 359)
      if(we <= 4788) return 2520;
      if(we > 4788) return 2034;
     if(w > 359)
      if(we <= 4308)
       if(we <= 4264) return 2400;
       if(we > 4264) return 2055;
      if(we > 4308)
       if(we <= 4368) return 2455;
       if(we > 4368) return 2118;
    if(we > 4845)
     if(we <= 4960)
      if(t <= 3) return 2400;
      if(t > 3)
       if(w <= 360)
        if(w <= 358)
         if(we <= 4868) return 2410;
         if(we > 4868) return 2196;
        if(w > 358)
         if(we <= 4937) return 2129;
         if(we > 4937) return 2410;
       if(w > 360) return 2164;
     if(we > 4960)
      if(t <= 3.35)
       if(w <= 358)
        if(w <= 357) return 2196;
        if(w > 357) return 2875.2;
       if(w > 358) return 2280;
      if(t > 3.35) return 2280;
  if(w > 370)
   if(w <= 390)
    if(t <= 3.74)
     if(w <= 387) return 2534;
     if(w > 387) return 2310;
    if(t > 3.74) return 2234;
   if(w > 390)
    if(we <= 5041)
     if(w <= 396)
      if(we <= 3059) return 2019;
      if(we > 3059) return 2272;
     if(w > 396) return 2267;
    if(we > 5041)
     if(w <= 414) return 2250;
     if(w > 414) return 2149;
if(p.equals("PP21942")) return 2115;
if(p.equals("PP21943"))
 if(t <= 1.61) return 1990;
 if(t > 1.61) return 2250;
if(p.equals("PP21945")) return 1883.5;
if(p.equals("PP21956"))
 if(w <= 317) return 1901;
 if(w > 317)
  if(t <= 2.74) return 1977;
  if(t > 2.74) return 2460;
if(p.equals("PP21957")) return 1920;
if(p.equals("PP21958"))
 if(t <= 1.6) return 1970;
 if(t > 1.6) return 2258;
if(p.equals("PP21965"))
 if(t <= 1.76) return 2100;
 if(t > 1.76) return 2134;
if(p.equals("PP21972"))
 if(t <= 1.75) return 2139;
 if(t > 1.75) return 2405;
if(p.equals("PP21973"))
 if(t <= 1.1) return 1872;
 if(t > 1.1) return 1827;
if(p.equals("PP21975"))
 if(t <= 2.63)
  if(t <= 2.53)
   if(t <= 2.43) return 2031;
   if(t > 2.43)
    if(we <= 2221) return 2310;
    if(we > 2221) return 2047;
  if(t > 2.53)
   if(t <= 2.6)
    if(we <= 2296) return 2400;
    if(we > 2296) return 2940;
   if(t > 2.6)
    if(w <= 225) return 2310;
    if(w > 225)
     if(we <= 2576) return 2530;
     if(we > 2576) return 2400;
 if(t > 2.63)
  if(t <= 2.7) return 2164;
  if(t > 2.7)
   if(we <= 2386) return 2091;
   if(we > 2386) return 2190;
if(p.equals("PP21976")) return 1772;
if(p.equals("PP21994"))
 if(we <= 3032)
  if(t <= 2.62)
   if(t <= 2.52)
    if(we <= 2228)
     if(we <= 2088) return 2640;
     if(we > 2088) return 1865;
    if(we > 2228)
     if(t <= 2.48) return 1710;
     if(t > 2.48)
      if(we <= 2542) return 1904;
      if(we > 2542) return 1845;
   if(t > 2.52)
    if(w <= 234)
     if(t <= 2.56) return 2400;
     if(t > 2.56) return 2660;
    if(w > 234) return 1970;
  if(t > 2.62)
   if(w <= 231)
    if(t <= 2.7)
     if(t <= 2.63)
      if(w <= 225) return 2102;
      if(w > 225) return 2006;
     if(t > 2.63) return 2075;
    if(t > 2.7)
     if(we <= 2346)
      if(we <= 2120) return 2290;
      if(we > 2120) return 1885;
     if(we > 2346)
      if(t <= 2.74)
       if(we <= 2490) return 2045;
       if(we > 2490)
        if(we <= 2536) return 1915;
        if(we > 2536) return 1904;
      if(t > 2.74) return 1925;
   if(w > 231)
    if(w <= 243)
     if(w <= 237) return 1840;
     if(w > 237)
      if(t <= 2.84) return 1786;
      if(t > 2.84) return 2015;
    if(w > 243)
     if(w <= 250)
      if(we <= 2770) return 1886;
      if(we > 2770) return 2006;
     if(w > 250)
      if(t <= 2.88) return 2180;
      if(t > 2.88) return 2010;
 if(we > 3032)
  if(t <= 3.74)
   if(w <= 459)
    if(w <= 457)
     if(t <= 3.35) return 2700;
     if(t > 3.35)
      if(we <= 4539) return 2225;
      if(we > 4539) return 2440;
    if(w > 457)
     if(we <= 4977) return 2349;
     if(we > 4977)
      if(we <= 5039) return 2400;
      if(we > 5039) return 2197;
   if(w > 459)
    if(w <= 488)
     if(w <= 462) return 2100;
     if(w > 462)
      if(w <= 475) return 2070;
      if(w > 475) return 2450;
    if(w > 488)
     if(we <= 5130)
      if(we <= 4708) return 2310;
      if(we > 4708) return 2100;
     if(we > 5130)
      if(we <= 5760) return 2369.6;
      if(we > 5760) return 2089;
  if(t > 3.74)
   if(t <= 4.25)
    if(we <= 4973)
     if(we <= 4578) return 2135;
     if(we > 4578)
      if(w <= 457) return 2159;
      if(w > 457) return 2015;
    if(we > 4973)
     if(we <= 5885)
      if(we <= 5174) return 2350;
      if(we > 5174)
       if(w <= 457) return 2329.73333333333;
       if(w > 457) return 2069;
     if(we > 5885) return 2331;
   if(t > 4.25)
    if(w <= 480) return 2131;
    if(w > 480)
     if(w <= 511)
      if(we <= 5505) return 2015;
      if(we > 5505) return 2157;
     if(w > 511) return 2640;
if(p.equals("PP21996")) return 2108;
if(p.equals("PP22001"))
 if(t <= 2.93)
  if(t <= 2.1)
   if(t <= 1.66)
    if(t <= 1.25)
     if(w <= 389) return 1881;
     if(w > 389) return 1862;
    if(t > 1.25)
     if(w <= 377)
      if(we <= 4840) return 2087;
      if(we > 4840) return 1657.61666666667;
     if(w > 377) return 1944;
   if(t > 1.66)
    if(t <= 1.82)
     if(w <= 374)
      if(w <= 328) return 1852;
      if(w > 328) return 1951;
     if(w > 374) return 1895;
    if(t > 1.82)
     if(t <= 1.93) return 1813;
     if(t > 1.93)
      if(w <= 340)
       if(w <= 310) return 2305;
       if(w > 310) return 2119;
      if(w > 340) return 2220;
  if(t > 2.1)
   if(w <= 257)
    if(w <= 219)
     if(t <= 2.7) return 1895;
     if(t > 2.7)
      if(w <= 217)
       if(we <= 2570) return 2352;
       if(we > 2570) return 2305;
      if(w > 217)
       if(we <= 2416) return 1830;
       if(we > 2416) return 2554;
    if(w > 219)
     if(t <= 2.7)
      if(t <= 2.41)
       if(w <= 240) return 1770;
       if(w > 240) return 2130;
      if(t > 2.41)
       if(w <= 233) return 2145;
       if(w > 233) return 2018;
     if(t > 2.7)
      if(w <= 252.5)
       if(w <= 235) return 2460;
       if(w > 235)
        if(w <= 251) return 2145;
        if(w > 251) return 2420;
      if(w > 252.5)
       if(we <= 2965) return 2114;
       if(we > 2965) return 2460;
   if(w > 257)
    if(w <= 366)
     if(w <= 299)
      if(w <= 259)
       if(we <= 2891) return 2039;
       if(we > 2891) return 2200;
      if(w > 259)
       if(w <= 279) return 1965;
       if(w > 279)
        if(we <= 3248) return 2070;
        if(we > 3248) return 2280;
     if(w > 299)
      if(we <= 4353)
       if(t <= 2.51) return 4333;
       if(t > 2.51) return 2114;
      if(we > 4353)
       if(t <= 2.41) return 1965;
       if(t > 2.41) return 2200;
    if(w > 366)
     if(w <= 402)
      if(w <= 401) return 1814;
      if(w > 401) return 2220;
     if(w > 402)
      if(we <= 6060) return 2055;
      if(we > 6060) return 1830;
 if(t > 2.93)
  if(w <= 279)
   if(w <= 242)
    if(w <= 228)
     if(t <= 3.48) return 2460;
     if(t > 3.48) return 2730;
    if(w > 228)
     if(t <= 3.09)
      if(w <= 237)
       if(w <= 229) return 2250;
       if(w > 229) return 2368;
      if(w > 237)
       if(w <= 240) return 2072;
       if(w > 240) return 2060;
     if(t > 3.09) return 2247;
   if(w > 242)
    if(w <= 260)
     if(w <= 245)
      if(w <= 243) return 2280;
      if(w > 243) return 2220;
     if(w > 245)
      if(w <= 247) return 1997;
      if(w > 247)
       if(w <= 253)
        if(we <= 2976) return 2280;
        if(we > 2976) return 1951;
       if(w > 253) return 1997;
    if(w > 260)
     if(w <= 278)
      if(t <= 3.48)
       if(w <= 277)
        if(we <= 3288) return 2145;
        if(we > 3288) return 2250;
       if(w > 277) return 2204;
      if(t > 3.48)
       if(w <= 262) return 2045;
       if(w > 262) return 1710;
     if(w > 278)
      if(we <= 3133)
       if(we <= 2793) return 2062;
       if(we > 2793) return 2140;
      if(we > 3133) return 2230;
  if(w > 279)
   if(t <= 3)
    if(w <= 339)
     if(we <= 3472)
      if(w <= 286)
       if(we <= 3292) return 1928;
       if(we > 3292) return 2280;
      if(w > 286)
       if(w <= 310)
        if(w <= 290) return 2220;
        if(w > 290) return 1873;
       if(w > 310)
        if(we <= 3247) return 2220;
        if(we > 3247) return 2033;
     if(we > 3472)
      if(we <= 3779)
       if(w <= 312) return 2352;
       if(w > 312) return 2149;
      if(we > 3779)
       if(we <= 3880) return 1951;
       if(we > 3880) return 2230.28333333333;
    if(w > 339)
     if(w <= 364)
      if(w <= 341) return 2205;
      if(w > 341)
       if(we <= 3812) return 2010;
       if(we > 3812) return 2460;
     if(w > 364)
      if(w <= 378) return 2160;
      if(w > 378)
       if(w <= 395) return 2100;
       if(w > 395) return 2220;
   if(t > 3)
    if(t <= 3.59)
     if(t <= 3.14) return 1813;
     if(t > 3.14) return 2008;
    if(t > 3.59)
     if(t <= 4.15)
      if(w <= 338) return 2225;
      if(w > 338) return 4554;
     if(t > 4.15)
      if(w <= 289) return 2039;
      if(w > 289) return 2247;
if(p.equals("PP22003"))
 if(t <= 2.74)
  if(t <= 1.92)
   if(t <= 1.41) return 1843;
   if(t > 1.41) return 1847;
  if(t > 1.92)
   if(w <= 276) return 1810;
   if(w > 276) return 2270;
 if(t > 2.74)
  if(w <= 245) return 2220;
  if(w > 245)
   if(w <= 262) return 1928;
   if(w > 262) return 2370;
if(p.equals("PP22005")) return 2242;
if(p.equals("PP22007"))
 if(w <= 303)
  if(we <= 3594) return 1740;
  if(we > 3594) return 2325;
 if(w > 303)
  if(we <= 3640) return 2178;
  if(we > 3640) return 4571;
if(p.equals("PP22009"))
 if(t <= 2.46)
  if(t <= 1.85)
   if(w <= 186)
    if(w <= 173)
     if(we <= 1665)
      if(w <= 161)
       if(we <= 1549) return 2979;
       if(we > 1549) return 2594;
      if(w > 161)
       if(we <= 1572) return 2425;
       if(we > 1572) return 2979;
     if(we > 1665)
      if(w <= 161) return 2097;
      if(w > 161) return 2037;
    if(w > 173)
     if(t <= 1.65) return 2037;
     if(t > 1.65)
      if(t <= 1.715) return 2180;
      if(t > 1.715) return 2395;
   if(w > 186)
    if(w <= 214)
     if(w <= 208)
      if(t <= 1.7)
       if(t <= 1.6) return 2091;
       if(t > 1.6) return 2180;
      if(t > 1.7)
       if(we <= 2019)
        if(we <= 1996) return 2340;
        if(we > 1996) return 2009;
       if(we > 2019) return 2310;
     if(w > 208)
      if(we <= 2045) return 2091;
      if(we > 2045)
       if(we <= 2064) return 2047;
       if(we > 2064) return 2140;
    if(w > 214) return 2074;
  if(t > 1.85)
   if(we <= 1825)
    if(w <= 186)
     if(we <= 1650)
      if(t <= 1.99) return 2455;
      if(t > 1.99) return 2505;
     if(we > 1650)
      if(w <= 174)
       if(we <= 1700) return 2260;
       if(we > 1700) return 2105;
      if(w > 174)
       if(w <= 184) return 2143;
       if(w > 184) return 2260;
    if(w > 186) return 2760;
   if(we > 1825)
    if(t <= 2.08)
     if(we <= 1907)
      if(w <= 184) return 2060;
      if(w > 184) return 2130;
     if(we > 1907) return 3184;
    if(t > 2.08) return 2130;
 if(t > 2.46)
  if(t <= 2.84)
   if(we <= 3937)
    if(w <= 386)
     if(w <= 356)
      if(we <= 3280) return 2760;
      if(we > 3280) return 2023;
     if(w > 356)
      if(we <= 3814) return 2135;
      if(we > 3814) return 2020;
    if(w > 386)
     if(we <= 3890)
      if(we <= 3625) return 2235;
      if(we > 3625) return 2074;
     if(we > 3890) return 2159;
   if(we > 3937)
    if(we <= 4163)
     if(we <= 4155) return 2250;
     if(we > 4155) return 2157;
    if(we > 4163) return 2505;
  if(t > 2.84)
   if(w <= 374)
    if(t <= 3.24)
     if(w <= 364)
      if(w <= 359) return 3313;
      if(w > 359) return 2490;
     if(w > 364)
      if(we <= 3644)
       if(we <= 3392) return 2195;
       if(we > 3392) return 2225;
      if(we > 3644) return 2550;
    if(t > 3.24)
     if(w <= 326)
      if(w <= 311) return 2286;
      if(w > 311) return 2195;
     if(w > 326)
      if(we <= 3178) return 2022;
      if(we > 3178) return 2074;
   if(w > 374)
    if(w <= 387)
     if(t <= 3.24)
      if(w <= 381) return 2142;
      if(w > 381) return 3060;
     if(t > 3.24) return 2310;
    if(w > 387)
     if(we <= 3641) return 2012;
     if(we > 3641) return 2460;
if(p.equals("PP22014"))
 if(t <= 0.66) return 1805;
 if(t > 0.66) return 1944;
if(p.equals("PP22015"))
 if(w <= 307) return 2131;
 if(w > 307)
  if(t <= 2.84) return 2058;
  if(t > 2.84) return 2079;
if(p.equals("PP22017"))
 if(t <= 2.67)
  if(we <= 4412) return 2050.5;
  if(we > 4412) return 2163;
 if(t > 2.67)
  if(t <= 3.74)
   if(we <= 4545) return 2066;
   if(we > 4545) return 2198;
  if(t > 3.74)
   if(w <= 406) return 2590;
   if(w > 406) return 2520;
if(p.equals("PP22035"))
 if(t <= 1.61)
  if(w <= 245) return 1659;
  if(w > 245) return 1972.9;
 if(t > 1.61)
  if(t <= 2.15) return 2465;
  if(t > 2.15) return 2297;
if(p.equals("PP22036"))
 if(w <= 282)
  if(w <= 220) return 1980;
  if(w > 220)
   if(w <= 257.5) return 1772;
   if(w > 257.5) return 2089;
 if(w > 282)
  if(w <= 294) return 1970;
  if(w > 294)
   if(we <= 4273) return 2025;
   if(we > 4273) return 1859;
if(p.equals("PP22038"))
 if(t <= 0.85) return 1945;
 if(t > 0.85)
  if(t <= 1.2) return 1853;
  if(t > 1.2) return 2059;
if(p.equals("PP22041")) return 2400;
if(p.equals("PP22052"))
 if(t <= 2.62)
  if(t <= 1.87)
   if(we <= 1773) return 2230;
   if(we > 1773) return 2106;
  if(t > 1.87) return 2113.5;
 if(t > 2.62)
  if(t <= 3.35)
   if(t <= 3.09) return 2134;
   if(t > 3.09) return 2510;
  if(t > 3.35)
   if(t <= 4)
    if(t <= 3.415) return 2105;
    if(t > 3.415) return 2089;
   if(t > 4) return 2149;
if(p.equals("PP22054"))
 if(t <= 3.5)
  if(t <= 2.88) return 2115;
  if(t > 2.88) return 1878;
 if(t > 3.5)
  if(t <= 4.25) return 2069;
  if(t > 4.25) return 2234;
if(p.equals("PP22056"))
 if(t <= 1.5) return 1874;
 if(t > 1.5) return 2370;
if(p.equals("PP22066"))
 if(t <= 2.45) return 2420;
 if(t > 2.45) return 2140;
if(p.equals("PP22067"))
 if(t <= 2.45)
  if(w <= 430) return 2340;
  if(w > 430) return 2190;
 if(t > 2.45)
  if(w <= 430) return 2352;
  if(w > 430) return 1925;
if(p.equals("PP22070"))
 if(t <= 2.58) return 2018;
 if(t > 2.58) return 2640;
if(p.equals("PP22071"))
 if(t <= 0.86)
  if(w <= 358)
   if(w <= 326) return 10950;
   if(w > 326) return 1734;
  if(w > 358)
   if(we <= 5301) return 2040;
   if(we > 5301) return 2000;
 if(t > 0.86)
  if(w <= 365) return 1859;
  if(w > 365)
   if(we <= 4895) return 1999;
   if(we > 4895) return 1730;
if(p.equals("PP22072")) return 1891;
if(p.equals("PP22074")) return 1851;
if(p.equals("PP22080"))
 if(t <= 1.02)
  if(t <= 0.66)
   if(w <= 318) return 1940;
   if(w > 318) return 1865;
  if(t > 0.66) return 2080;
 if(t > 1.02)
  if(t <= 1.65) return 1950;
  if(t > 1.65) return 2126;
if(p.equals("PP22083"))
 if(w <= 358)
  if(t <= 1.1) return 1815.45;
  if(t > 1.1) return 2140;
 if(w > 358)
  if(t <= 1.1) return 1867;
  if(t > 1.1) return 2072;
if(p.equals("PP22086"))
 if(we <= 3552) return 1753;
 if(we > 3552) return 1975;
if(p.equals("PP22093"))
 if(t <= 1.61)
  if(w <= 327) return 1895;
  if(w > 327)
   if(w <= 334) return 1996;
   if(w > 334) return 1890;
 if(t > 1.61)
  if(t <= 2.15)
   if(we <= 4323) return 2340;
   if(we > 4323) return 2126;
  if(t > 2.15)
   if(w <= 334) return 2270;
   if(w > 334) return 2220;
if(p.equals("PP22094")) return 2153;
if(p.equals("PP22096")) return 2316;
if(p.equals("PP22102"))
 if(t <= 1.15) return 1970;
 if(t > 1.15)
  if(t <= 1.85) return 2005;
  if(t > 1.85) return 2430.33333333333;
if(p.equals("PP22111")) return 1960;
if(p.equals("PP22112"))
 if(t <= 1.5)
  if(w <= 312)
   if(we <= 4356) return 1917;
   if(we > 4356) return 1997;
  if(w > 312)
   if(t <= 0.95) return 1975;
   if(t > 0.95) return 2432;
 if(t > 1.5)
  if(w <= 312)
   if(we <= 4348) return 2210;
   if(we > 4348) return 11661;
  if(w > 312)
   if(w <= 343) return 2108;
   if(w > 343) return 2216;
if(p.equals("PP22115"))
 if(w <= 212) return 2126;
 if(w > 212)
  if(t <= 1.76) return 2025;
  if(t > 1.76)
   if(we <= 3043) return 2044;
   if(we > 3043) return 2191;
if(p.equals("PP22132"))
 if(t <= 0.4)
  if(we <= 3144) return 2012;
  if(we > 3144) return 2155;
 if(t > 0.4)
  if(t <= 0.75) return 1955.5;
  if(t > 0.75)
   if(t <= 1.5) return 2082.5;
   if(t > 1.5) return 2233.5;
if(p.equals("PP22135")) return 1965;
if(p.equals("PP22136")) return 1890;
if(p.equals("PP22137")) return 1859;
if(p.equals("PP22157"))
 if(t <= 1.31) return 1970;
 if(t > 1.31)
  if(t <= 1.62)
   if(w <= 325)
    if(we <= 4250) return 2159;
    if(we > 4250) return 2055;
   if(w > 325) return 2159;
  if(t > 1.62) return 2160;
if(p.equals("PP22159"))
 if(w <= 223)
  if(we <= 2566) return 1972;
  if(we > 2566) return 2120;
 if(w > 223)
  if(t <= 0.87) return 2815;
  if(t > 0.87) return 2290;
if(p.equals("PP22174"))
 if(w <= 416) return 2127;
 if(w > 416)
  if(t <= 1.61) return 1760;
  if(t > 1.61)
   if(t <= 2.74) return 2207;
   if(t > 2.74) return 2090;
if(p.equals("PP22175")) return 1841;
if(p.equals("PP22183"))
 if(t <= 1.76) return 2220;
 if(t > 1.76) return 2216;
if(p.equals("PP22184"))
 if(t <= 2.62) return 2185;
 if(t > 2.62) return 2488;
if(p.equals("PP22186")) return 1795;
if(p.equals("PP22195"))
 if(t <= 2.4)
  if(w <= 312)
   if(t <= 1.45) return 1974;
   if(t > 1.45) return 1970;
  if(w > 312) return 1870;
 if(t > 2.4)
  if(t <= 4.25)
   if(w <= 312) return 1995;
   if(w > 312) return 2362.5;
  if(t > 4.25) return 2620;
if(p.equals("PP22202")) return 2815;
if(p.equals("PP22209"))
 if(w <= 422)
  if(t <= 3.32) return 1829;
  if(t > 3.32) return 2590;
 if(w > 422)
  if(t <= 3.09) return 1955;
  if(t > 3.09) return 2302;
if(p.equals("PP22210")) return 1985.5;
if(p.equals("PP22213"))
 if(t <= 2.03)
  if(w <= 509) return 1917;
  if(w > 509) return 2020;
 if(t > 2.03)
  if(w <= 509) return 2220;
  if(w > 509) return 2460;
if(p.equals("PP22215")) return 2120;
if(p.equals("PP22225"))
 if(w <= 329) return 1734;
 if(w > 329) return 1960;
if(p.equals("PP22229"))
 if(t <= 1.5)
  if(we <= 3427) return 2195;
  if(we > 3427) return 2120;
 if(t > 1.5) return 2465;
if(p.equals("PP22230"))
 if(t <= 0.75) return 1779;
 if(t > 0.75)
  if(t <= 1.5) return 1856;
  if(t > 1.5) return 2300;
if(p.equals("PP22231")) return 1900;
if(p.equals("PP22240")) return 1825;
if(p.equals("PP22243"))
 if(t <= 1)
  if(w <= 296) return 1979;
  if(w > 296) return 2290;
 if(t > 1)
  if(w <= 436) return 2215;
  if(w > 436) return 2142;
if(p.equals("PP22246"))
 if(w <= 249)
  if(t <= 4) return 1765;
  if(t > 4) return 2230;
 if(w > 249)
  if(t <= 3.77) return 1999;
  if(t > 3.77) return 2111;
if(p.equals("PP22250")) return 2008;
if(p.equals("PP22256")) return 2400;
if(p.equals("PP22261"))
 if(t <= 1.61) return 1965;
 if(t > 1.61) return 2220;
if(p.equals("PP22262"))
 if(t <= 1.45)
  if(we <= 4779) return 2270;
  if(we > 4779) return 1951;
 if(t > 1.45)
  if(we <= 4832) return 2395;
  if(we > 4832)
   if(we <= 4846) return 2380;
   if(we > 4846) return 2310;
if(p.equals("PP22263"))
 if(w <= 367)
  if(t <= 0.66) return 1747;
  if(t > 0.66) return 2200;
 if(w > 367)
  if(we <= 4844) return 2185;
  if(we > 4844) return 2310;
if(p.equals("PP22264")) return 1972;
if(p.equals("PP22267")) return 1891;
if(p.equals("PP22268")) return 1860;
if(p.equals("PP22276"))
 if(t <= 1)
  if(w <= 419) return 2130;
  if(w > 419) return 2077;
 if(t > 1)
  if(w <= 419)
   if(we <= 3910) return 2040;
   if(we > 3910) return 2700;
  if(w > 419) return 4050;
if(p.equals("PP22277"))
 if(t <= 0.9)
  if(t <= 0.63) return 1792;
  if(t > 0.63)
   if(we <= 4394) return 1972;
   if(we > 4394) return 1985;
 if(t > 0.9)
  if(t <= 1.1) return 2269;
  if(t > 1.1)
   if(we <= 4394) return 2040;
   if(we > 4394) return 1768;
if(p.equals("PP22278"))
 if(t <= 2.56) return 2024;
 if(t > 2.56)
  if(we <= 7083) return 2310;
  if(we > 7083) return 2225;
if(p.equals("PP22284")) return 1810;
if(p.equals("PP22292"))
 if(we <= 5442) return 2049;
 if(we > 5442) return 2095;
if(p.equals("PP22293"))
 if(t <= 2.46) return 2140;
 if(t > 2.46) return 2074;
if(p.equals("PP22295"))
 if(w <= 417) return 2470;
 if(w > 417) return 3030;
if(p.equals("PP22297"))
 if(t <= 1.5)
  if(w <= 233)
   if(we <= 3096) return 1800;
   if(we > 3096) return 2025;
  if(w > 233)
   if(we <= 3256) return 1856;
   if(we > 3256) return 1882;
 if(t > 1.5)
  if(w <= 233) return 2540;
  if(w > 233)
   if(we <= 3256) return 2220;
   if(we > 3256) return 2262;
if(p.equals("PP22298"))
 if(w <= 313) return 1912;
 if(w > 313) return 1738;
if(p.equals("PP22299")) return 2480;
if(p.equals("PP22300")) return 1706;
if(p.equals("PP22303"))
 if(t <= 2.23)
  if(we <= 2105) return 1979;
  if(we > 2105) return 2190;
 if(t > 2.23)
  if(we <= 2105) return 2190;
  if(we > 2105) return 2008;
if(p.equals("PP22306")) return 1975;
if(p.equals("PP22307"))
 if(t <= 2.7)
  if(t <= 2.1) return 2140;
  if(t > 2.1) return 2100;
 if(t > 2.7) return 2360;
if(p.equals("PP22312"))
 if(t <= 0.95)
  if(t <= 0.53) return 2120;
  if(t > 0.53) return 1792;
 if(t > 0.95)
  if(t <= 1.6) return 1706;
  if(t > 1.6) return 2108;
if(p.equals("PP22316")) return 1939;
if(p.equals("PP22317"))
 if(t <= 2) return 1836;
 if(t > 2) return 2184;
if(p.equals("PP22318"))
 if(t <= 3.09) return 2416;
 if(t > 3.09) return 2520;
if(p.equals("PP22319"))
 if(t <= 3.27) return 1810;
 if(t > 3.27) return 2400;
if(p.equals("PP22321"))
 if(t <= 2.45)
  if(we <= 3426) return 2010;
  if(we > 3426)
   if(we <= 3626) return 1968.11666666667;
   if(we > 3626) return 2130;
 if(t > 2.45)
  if(we <= 3359) return 1965;
  if(we > 3359) return 1999;
if(p.equals("PP22322"))
 if(t <= 2.45)
  if(w <= 430)
   if(we <= 4994)
    if(we <= 4592)
     if(we <= 4440) return 2010;
     if(we > 4440) return 1968.11666666667;
    if(we > 4592)
     if(we <= 4790) return 2310;
     if(we > 4790) return 2010;
   if(we > 4994) return 2160;
  if(w > 430)
   if(we <= 5204)
    if(we <= 4630) return 2280;
    if(we > 4630)
     if(we <= 4996)
      if(we <= 4790) return 2160;
      if(we > 4790) return 2300;
     if(we > 4996) return 2160;
   if(we > 5204) return 1975;
 if(t > 2.45)
  if(w <= 430)
   if(we <= 4440) return 2098;
   if(we > 4440) return 2340;
  if(w > 430)
   if(we <= 4836)
    if(we <= 4630) return 2163;
    if(we > 4630) return 2190;
   if(we > 4836) return 1968.11666666667;
if(p.equals("PP22336"))
 if(t <= 2.45)
  if(we <= 3444)
   if(w <= 303) return 2340;
   if(w > 303) return 2181;
  if(we > 3444)
   if(we <= 3595)
    if(we <= 3564) return 1931;
    if(we > 3564) return 2089;
   if(we > 3595)
    if(we <= 3622) return 2017;
    if(we > 3622) return 1931;
 if(t > 2.45)
  if(we <= 3444)
   if(w <= 303) return 1995;
   if(w > 303)
    if(we <= 3198)
     if(we <= 3168) return 2460;
     if(we > 3168) return 2092;
    if(we > 3198)
     if(we <= 3290) return 2130;
     if(we > 3290)
      if(we <= 3359) return 2945;
      if(we > 3359) return 2130;
  if(we > 3444) return 2534;
if(p.equals("PP22339"))
 if(t <= 3.64)
  if(w <= 241) return 2030;
  if(w > 241) return 1727;
 if(t > 3.64)
  if(w <= 241) return 2310;
  if(w > 241) return 2097;
if(p.equals("PP22340")) return 2030;
if(p.equals("PP22341"))
 if(t <= 2.74)
  if(w <= 250) return 2035;
  if(w > 250) return 1704;
 if(t > 2.74) return 2310;
if(p.equals("PP22342"))
 if(t <= 3.35)
  if(t <= 2.02)
   if(t <= 1.7) return 2305;
   if(t > 1.7) return 1977;
  if(t > 2.02)
   if(we <= 3804)
    if(w <= 323)
     if(w <= 318) return 2475;
     if(w > 318) return 2700;
    if(w > 323)
     if(we <= 3636) return 2400;
     if(we > 3636) return 2416;
   if(we > 3804)
    if(w <= 400) return 2460;
    if(w > 400)
     if(w <= 422)
      if(w <= 421) return 2490;
      if(w > 421) return 2525;
     if(w > 422) return 2400;
 if(t > 3.35)
  if(t <= 3.77)
   if(w <= 328)
    if(we <= 3702) return 2267;
    if(we > 3702) return 2229;
   if(w > 328)
    if(w <= 423)
     if(we <= 4799) return 2490;
     if(we > 4799) return 2280;
    if(w > 423) return 2229;
  if(t > 3.77)
   if(w <= 318)
    if(we <= 3755) return 2160;
    if(we > 3755) return 2565;
   if(w > 318) return 2345;
if(p.equals("PP22345")) return 1798;
if(p.equals("PP22350"))
 if(t <= 1) return 2400;
 if(t > 1)
  if(w <= 304) return 1921;
  if(w > 304) return 2122;
if(p.equals("PP22351"))
 if(w <= 328)
  if(w <= 308) return 2297;
  if(w > 308) return 2225;
 if(w > 328) return 2111;
if(p.equals("PP22352"))
 if(t <= 2.45)
  if(w <= 430)
   if(we <= 4554)
    if(w <= 414) return 2280;
    if(w > 414)
     if(we <= 4420) return 2300;
     if(we > 4420) return 2970;
   if(we > 4554)
    if(we <= 4720)
     if(we <= 4667) return 2065;
     if(we > 4667) return 1950;
    if(we > 4720)
     if(we <= 4890) return 2160;
     if(we > 4890) return 2460;
  if(w > 430)
   if(we <= 4770)
    if(we <= 4580)
     if(we <= 4511) return 2601;
     if(we > 4511) return 2110;
    if(we > 4580) return 2232;
   if(we > 4770)
    if(we <= 5112) return 2116;
    if(we > 5112) return 2160;
 if(t > 2.45)
  if(we <= 4731)
   if(we <= 4474)
    if(w <= 414) return 2754;
    if(w > 414)
     if(we <= 4413) return 2078;
     if(we > 4413) return 2315;
   if(we > 4474)
    if(w <= 430)
     if(we <= 4667)
      if(we <= 4554) return 2092;
      if(we > 4554) return 2218;
     if(we > 4667)
      if(we <= 4720) return 2003;
      if(we > 4720) return 2098;
    if(w > 430)
     if(we <= 4580)
      if(we <= 4540) return 2092;
      if(we > 4540) return 2524;
     if(we > 4580)
      if(we <= 4599) return 2003;
      if(we > 4599) return 2078;
  if(we > 4731)
   if(w <= 430)
    if(we <= 4922)
     if(we <= 4832) return 2172;
     if(we > 4832) return 2401;
    if(we > 4922) return 2130;
   if(w > 430)
    if(we <= 4923)
     if(we <= 4803) return 2149;
     if(we > 4803)
      if(we <= 4850) return 2092;
      if(we > 4850) return 2129.33333333333;
    if(we > 4923)
     if(we <= 5089) return 2218;
     if(we > 5089)
      if(we <= 5130) return 1871;
      if(we > 5130) return 2130;
if(p.equals("PP22354"))
 if(t <= 1.1)
  if(w <= 250) return 1779;
  if(w > 250) return 1806.5;
 if(t > 1.1) return 1772;
if(p.equals("PP22355"))
 if(t <= 1.01)
  if(t <= 0.81)
   if(w <= 326) return 1975;
   if(w > 326) return 2203;
  if(t > 0.81) return 2025;
 if(t > 1.01)
  if(w <= 315) return 2183;
  if(w > 315)
   if(w <= 326) return 1899;
   if(w > 326) return 2164;
if(p.equals("PP22356"))
 if(t <= 3.35)
  if(w <= 320) return 2185;
  if(w > 320) return 1853;
 if(t > 3.35)
  if(we <= 3923) return 2097;
  if(we > 3923) return 2179;
if(p.equals("PP22357"))
 if(t <= 2.36)
  if(we <= 3804) return 11369;
  if(we > 3804) return 11545;
 if(t > 2.36) return 3001;
if(p.equals("PP22358"))
 if(w <= 289)
  if(t <= 1.78) return 2230;
  if(t > 1.78)
   if(we <= 1800) return 2230;
   if(we > 1800) return 2700;
 if(w > 289)
  if(t <= 3.24) return 2117;
  if(t > 3.24) return 2280;
if(p.equals("PP22361")) return 1798;
if(p.equals("PP22362"))
 if(w <= 315) return 2007;
 if(w > 315) return 2395;
if(p.equals("PP22363")) return 2360;
if(p.equals("PP22365")) return 2300;
if(p.equals("PP22366")) return 2042;
if(p.equals("PP22367"))
 if(w <= 408)
  if(t <= 1.32) return 2054;
  if(t > 1.32) return 2082;
 if(w > 408)
  if(t <= 1.17)
   if(we <= 5320) return 1865;
   if(we > 5320) return 1882.91666666667;
  if(t > 1.17) return 1935;
if(p.equals("PP22369")) return 1810;
if(p.equals("PP22380")) return 1594;
if(p.equals("PP22387")) return 2880;
if(p.equals("PP22406")) return 1865;
if(p.equals("PP22408"))
 if(t <= 0.66)
  if(w <= 277) return 1780;
  if(w > 277) return 1865;
 if(t > 0.66) return 2145;
if(p.equals("PP22410")) return 1944;
if(p.equals("PP22415"))
 if(t <= 2.56) return 1940;
 if(t > 2.56) return 2045;
if(p.equals("PP22416"))
 if(t <= 2.08)
  if(we <= 2576)
   if(we <= 2487) return 2269;
   if(we > 2487) return 2368;
  if(we > 2576)
   if(t <= 1.81) return 2030.13333333333;
   if(t > 1.81) return 3076;
 if(t > 2.08)
  if(we <= 3570)
   if(we <= 3526) return 2430;
   if(we > 3526) return 2860;
  if(we > 3570) return 2430;
if(p.equals("PP22417"))
 if(w <= 263)
  if(we <= 2894)
   if(t <= 1.82) return 2220;
   if(t > 1.82)
    if(we <= 2737) return 2270;
    if(we > 2737) return 2425;
  if(we > 2894)
   if(we <= 3134)
    if(we <= 2934) return 2860;
    if(we > 2934) return 2580;
   if(we > 3134) return 2520;
 if(w > 263)
  if(we <= 3625)
   if(t <= 2.02) return 2035;
   if(t > 2.02)
    if(w <= 264) return 2245;
    if(w > 264)
     if(w <= 284)
      if(we <= 3116) return 2893;
      if(we > 3116) return 2681.46666666667;
     if(w > 284) return 2245;
  if(we > 3625) return 2730;
if(p.equals("PP22420")) return 2640;
if(p.equals("PP22421")) return 2395;
if(p.equals("PP22422")) return 1828;
if(p.equals("PP22424"))
 if(t <= 3.98)
  if(we <= 3152) return 2400;
  if(we > 3152) return 2700;
 if(t > 3.98)
  if(we <= 3166) return 2197;
  if(we > 3166) return 2580;
if(p.equals("PP22425"))
 if(we <= 4828) return 2640;
 if(we > 4828) return 2195;
if(p.equals("PP22426"))
 if(we <= 2840) return 2171;
 if(we > 2840) return 2420.5;
if(p.equals("PP22427")) return 2646;
if(p.equals("PP22431"))
 if(t <= 0.7) return 2220;
 if(t > 0.7) return 1800;
if(p.equals("PP22435")) return 1990;
if(p.equals("PP22437")) return 2490;
if(p.equals("PP22447"))
 if(t <= 1.87) return 1761;
 if(t > 1.87) return 2250;
if(p.equals("PP22448"))
 if(t <= 1.86)
  if(w <= 356)
   if(t <= 0.75) return 1895;
   if(t > 0.75) return 1853;
  if(w > 356)
   if(t <= 1.67) return 1990;
   if(t > 1.67)
    if(we <= 5605) return 1952;
    if(we > 5605) return 1915;
 if(t > 1.86)
  if(t <= 3.64)
   if(w <= 406)
    if(t <= 2.4) return 2280;
    if(t > 2.4) return 2190;
   if(w > 406)
    if(t <= 2.74) return 2123;
    if(t > 2.74) return 2260;
  if(t > 3.64)
   if(we <= 5535)
    if(we <= 5458) return 2213;
    if(we > 5458) return 2280;
   if(we > 5535)
    if(we <= 5605) return 2118;
    if(we > 5605) return 2142;
if(p.equals("PP22456")) return 1910;
if(p.equals("PP22457")) return 1895;
if(p.equals("PP22460")) return 2016;
if(p.equals("PP22461")) return 1854;
if(p.equals("PP22464"))
 if(w <= 383)
  if(t <= 2.63) return 2400;
  if(t > 2.63) return 2520;
 if(w > 383) return 2400;
if(p.equals("PP22468")) return 2120;
if(p.equals("PP22469"))
 if(t <= 1.6) return 1810;
 if(t > 1.6) return 2345;
if(p.equals("PP22470")) return 1882.91666666667;
if(p.equals("PP22471"))
 if(t <= 3.09)
  if(we <= 3105) return 2390;
  if(we > 3105) return 2580;
 if(t > 3.09)
  if(we <= 6194) return 2160;
  if(we > 6194) return 2350;
if(p.equals("PP22473"))
 if(t <= 2.88) return 2140;
 if(t > 2.88) return 1940;
if(p.equals("PP22474"))
 if(t <= 3.4) return 2256;
 if(t > 3.4) return 1940;
if(p.equals("PP22481"))
 if(t <= 1.25)
  if(we <= 4398) return 2203;
  if(we > 4398) return 2037;
 if(t > 1.25) return 2400;
if(p.equals("PP22482"))
 if(t <= 1.01)
  if(t <= 0.86)
   if(we <= 3496) return 2203;
   if(we > 3496) return 1926;
  if(t > 0.86) return 2520;
 if(t > 1.01) return 2211;
if(p.equals("PP22484")) return 1739;
if(p.equals("PP22485")) return 1854;
if(p.equals("PP22486"))
 if(t <= 2.84)
  if(t <= 2.25) return 2399;
  if(t > 2.25) return 1852;
 if(t > 2.84)
  if(t <= 4.2) return 2579;
  if(t > 4.2)
   if(w <= 291) return 1998;
   if(w > 291) return 2280;
if(p.equals("PP22487"))
 if(t <= 1) return 1985;
 if(t > 1)
  if(t <= 1.6) return 1915;
  if(t > 1.6) return 2280;
if(p.equals("PP22488"))
 if(t <= 1.57)
  if(we <= 3617) return 2014;
  if(we > 3617) return 2045;
 if(t > 1.57) return 2280;
if(p.equals("PP22489"))
 if(t <= 1.61)
  if(we <= 4522) return 1699;
  if(we > 4522) return 2025;
 if(t > 1.61)
  if(we <= 4522) return 2280;
  if(we > 4522) return 2320;
if(p.equals("PP22490")) return 1810;
if(p.equals("PP22491"))
 if(t <= 1.76) return 2120;
 if(t > 1.76) return 2600.78333333333;
if(p.equals("PP22492")) return 2087;
if(p.equals("PP22498"))
 if(t <= 2.56)
  if(w <= 356)
   if(t <= 2.25)
    if(t <= 1.87) return 2210;
    if(t > 1.87)
     if(we <= 3311) return 2550;
     if(we > 3311) return 2310;
   if(t > 2.25)
    if(t <= 2.51) return 2079;
    if(t > 2.51)
     if(w <= 294) return 2210;
     if(w > 294) return 2078;
  if(w > 356)
   if(t <= 2.32)
    if(t <= 1.87) return 2241;
    if(t > 1.87) return 1765;
   if(t > 2.32)
    if(w <= 362) return 2321;
    if(w > 362) return 2212;
 if(t > 2.56)
  if(w <= 332)
   if(we <= 3672) return 2324;
   if(we > 3672)
    if(t <= 2.8) return 2135;
    if(t > 2.8) return 2175;
  if(w > 332)
   if(w <= 352) return 2385;
   if(w > 352)
    if(w <= 386) return 2520;
    if(w > 386) return 2324;
if(p.equals("PP22499"))
 if(t <= 1) return 2180;
 if(t > 1) return 1760;
if(p.equals("PP22500")) return 1773;
if(p.equals("PP22502")) return 1893;
if(p.equals("PP22503"))
 if(t <= 3.24) return 3371;
 if(t > 3.24) return 2217;
if(p.equals("PP22504")) return 1704;
if(p.equals("PP22505"))
 if(t <= 2.02)
  if(t <= 1.57)
   if(t <= 1.27) return 2440;
   if(t > 1.27)
    if(t <= 1.42) return 2400;
    if(t > 1.42) return 2027;
  if(t > 1.57)
   if(w <= 338)
    if(t <= 1.72)
     if(we <= 4162) return 2321;
     if(we > 4162) return 2390;
    if(t > 1.72) return 2000;
   if(w > 338)
    if(t <= 1.7)
     if(w <= 350) return 2058;
     if(w > 350) return 2269;
    if(t > 1.7) return 2105;
 if(t > 2.02)
  if(we <= 3430)
   if(we <= 3312)
    if(we <= 3233) return 2425;
    if(we > 3233) return 2270;
   if(we > 3312)
    if(we <= 3346) return 2460;
    if(we > 3346) return 2690;
  if(we > 3430)
   if(w <= 368)
    if(we <= 3750) return 2720;
    if(we > 3750) return 2520;
   if(w > 368)
    if(we <= 4837) return 3642.5;
    if(we > 4837) return 2163;
if(p.equals("PP22507"))
 if(t <= 2.46)
  if(w <= 391)
   if(we <= 3923)
    if(t <= 1.65) return 2001;
    if(t > 1.65) return 2251;
   if(we > 3923) return 2460;
  if(w > 391) return 1997;
 if(t > 2.46)
  if(t <= 3.24)
   if(we <= 3799) return 2022;
   if(we > 3799) return 2640;
  if(t > 3.24) return 2255;
if(p.equals("PP22508"))
 if(w <= 397)
  if(w <= 371)
   if(t <= 2.02) return 2940;
   if(t > 2.02) return 2448;
  if(w > 371)
   if(we <= 3565) return 1765;
   if(we > 3565) return 2390;
 if(w > 397)
  if(t <= 2.43)
   if(t <= 2.16) return 2190;
   if(t > 2.16) return 1698;
  if(t > 2.43) return 2065;
if(p.equals("PP22509"))
 if(t <= 1.1) return 1885;
 if(t > 1.1)
  if(we <= 5326) return 1935;
  if(we > 5326) return 1882.91666666667;
if(p.equals("PP22510"))
 if(t <= 1.86) return 2557;
 if(t > 1.86) return 2089;
if(p.equals("PP22512")) return 1865;
if(p.equals("PP22513"))
 if(w <= 235) return 2030.13333333333;
 if(w > 235)
  if(t <= 1.95) return 1990;
  if(t > 1.95) return 2157;
if(p.equals("PP22514"))
 if(t <= 2.36)
  if(w <= 403)
   if(we <= 4118) return 2278;
   if(we > 4118)
    if(we <= 4148)
     if(we <= 4137) return 2243;
     if(we > 4137) return 2460;
    if(we > 4148) return 2160;
  if(w > 403)
   if(we <= 3827)
    if(we <= 3721) return 2490;
    if(we > 3721) return 2460;
   if(we > 3827)
    if(we <= 4000) return 2452;
    if(we > 4000) return 2280;
 if(t > 2.36)
  if(w <= 402)
   if(w <= 399) return 2495;
   if(w > 399)
    if(we <= 4098) return 2525;
    if(we > 4098) return 2440;
  if(w > 402)
   if(w <= 405) return 2399;
   if(w > 405) return 2090;
if(p.equals("PP22515"))
 if(t <= 2.32)
  if(t <= 1.85)
   if(t <= 1.65)
    if(w <= 196) return 2191.5;
    if(w > 196) return 2427.5;
   if(t > 1.65)
    if(w <= 174) return 2340;
    if(w > 174) return 2045;
  if(t > 1.85)
   if(w <= 174) return 2345;
   if(w > 174)
    if(w <= 196) return 2130;
    if(w > 196) return 2211;
 if(t > 2.32)
  if(w <= 381)
   if(w <= 356)
    if(we <= 3366) return 2447;
    if(we > 3366) return 2090;
   if(w > 356)
    if(we <= 3683) return 2445;
    if(we > 3683) return 2460;
  if(w > 381)
   if(t <= 2.84) return 2010;
   if(t > 2.84)
    if(w <= 383) return 2340;
    if(w > 383)
     if(we <= 4010) return 2525;
     if(we > 4010) return 2742;
if(p.equals("PP22520"))
 if(t <= 2.72) return 2545;
 if(t > 2.72) return 2014;
if(p.equals("PP22521")) return 1980;
if(p.equals("PP22522")) return 1927;
if(p.equals("PP22530"))
 if(we <= 3591) return 2530;
 if(we > 3591) return 2035;
if(p.equals("PP22531"))
 if(w <= 259)
  if(w <= 226)
   if(w <= 225)
    if(we <= 2198)
     if(we <= 2105) return 2160;
     if(we > 2105) return 1985;
    if(we > 2198)
     if(we <= 2321) return 2208;
     if(we > 2321) return 2294;
   if(w > 225)
    if(t <= 2.68) return 2016;
    if(t > 2.68)
     if(we <= 2130) return 1830.5;
     if(we > 2130) return 1676;
  if(w > 226)
   if(t <= 2.7)
    if(w <= 234) return 2110;
    if(w > 234) return 2219;
   if(t > 2.7)
    if(t <= 2.84) return 2533;
    if(t > 2.84)
     if(w <= 242) return 1698;
     if(w > 242)
      if(we <= 2412) return 1980;
      if(we > 2412) return 1895;
 if(w > 259)
  if(t <= 3.47)
   if(w <= 265)
    if(t <= 2.9)
     if(t <= 2.84) return 1960;
     if(t > 2.84) return 2490;
    if(t > 2.9)
     if(t <= 3) return 2113;
     if(t > 3) return 2380;
   if(w > 265)
    if(we <= 2514) return 2380;
    if(we > 2514)
     if(we <= 3070) return 2349;
     if(we > 3070) return 2113;
  if(t > 3.47)
   if(t <= 3.5)
    if(we <= 3292) return 2575;
    if(we > 3292)
     if(we <= 3434) return 2058;
     if(we > 3434) return 2542;
   if(t > 3.5)
    if(we <= 3273) return 2016;
    if(we > 3273) return 2405;
if(p.equals("PP22535"))
 if(w <= 271)
  if(w <= 248)
   if(t <= 2.74)
    if(t <= 2.52) return 2528;
    if(t > 2.52) return 2455;
   if(t > 2.74)
    if(w <= 236)
     if(we <= 2542) return 2175;
     if(we > 2542) return 1996;
    if(w > 236)
     if(w <= 245)
      if(we <= 2848) return 2036;
      if(we > 2848) return 2520;
     if(w > 245) return 2175;
  if(w > 248)
   if(we <= 3155) return 2676;
   if(we > 3155) return 2343.5;
 if(w > 271)
  if(we <= 3023)
   if(w <= 333) return 2105;
   if(w > 333) return 2310;
  if(we > 3023)
   if(t <= 2.74) return 2460;
   if(t > 2.74)
    if(t <= 3.09) return 2691;
    if(t > 3.09) return 2143;
if(p.equals("PP22540")) return 2285;
if(p.equals("PP22541")) return 2185;
if(p.equals("PP22542")) return 2037;
if(p.equals("PP22546"))
 if(t <= 2.84) return 2429;
 if(t > 2.84)
  if(t <= 4.35) return 2163;
  if(t > 4.35) return 2220;
if(p.equals("PP22550")) return 1860;
if(p.equals("PP22553")) return 2043;
if(p.equals("PP22558")) return 2533;
if(p.equals("PP22563"))
 if(t <= 2.29) return 2445;
 if(t > 2.29)
  if(t <= 2.58) return 2535;
  if(t > 2.58) return 2580;
if(p.equals("PP22566"))
 if(t <= 2.83) return 2880;
 if(t > 2.83) return 2110;
if(p.equals("PP22568")) return 2179;
if(p.equals("PP22570")) return 2206;
if(p.equals("PP22571")) return 2216.5;
if(p.equals("PP22574")) return 2036;
if(p.equals("PP22581")) return 2250;
if(p.equals("PP22586")) return 2020;
if(p.equals("PP22590"))
 if(t <= 2.1) return 2027;
 if(t > 2.1) return 2101;
if(p.equals("PP22595")) return 2115;
if(p.equals("PP22600"))
 if(w <= 235) return 2105;
 if(w > 235)
  if(t <= 2.51) return 1935;
  if(t > 2.51) return 2085;
if(p.equals("PP22601"))
 if(t <= 2.58)
  if(we <= 2760) return 2073;
  if(we > 2760) return 2014;
 if(t > 2.58)
  if(we <= 2760) return 2085;
  if(we > 2760) return 2158;
if(p.equals("WSA1253")) return 1960;
return 1960.0;
}
}
