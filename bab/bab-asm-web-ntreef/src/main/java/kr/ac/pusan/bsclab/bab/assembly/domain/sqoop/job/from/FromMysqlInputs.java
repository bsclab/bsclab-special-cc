package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.from;

import java.util.ArrayList;
import java.util.List;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.InputsFactory;

public class FromMysqlInputs implements InputsFactory {

	/**
	 * args[0] : schemaName
	 * args[1] : tableName
	 * args[2] : sql
	 * args[3] : columns
	 * args[4] : partitionColumn
	 * args[5] : allowNullValueInPartitionColumn
	 * args[6] : boundryQuery
	 */ 
	@Override
	public Input[] createInputs(Object... args) {
		List<Input> inputs = new ArrayList<Input>();
		inputs.add(new Input(50, "ANY", "fromJobConfig.schemaName", 6, false, "", "STRING", args[0], null));
		inputs.add(new Input(50, "ANY", "fromJobConfig.tableName", 7, false, "", "STRING", args[1], null));
		inputs.add(new Input(2000, "ANY", "fromJobConfig.sql", 8, false, "", "STRING", args[2], null));
		inputs.add(new Input(50, "ANY", "fromJobConfig.columns", 9, false, "", "STRING", args[3], null));
		inputs.add(new Input(50, "ANY", "fromJobConfig.partitionColumn", 10, false, "", "STRING", args[4], null));
		inputs.add(new Input(0, "ANY", "fromJobConfig.allowNullValueInPartitionColumn", 11, false, "", "BOOLEAN", args[5], null));
		inputs.add(new Input(50, "ANY", "fromJobConfig.boundaryQuery", 12, false, "", "STRING", args[6], null));
		return inputs.toArray(new Input[0]);
	}
}
