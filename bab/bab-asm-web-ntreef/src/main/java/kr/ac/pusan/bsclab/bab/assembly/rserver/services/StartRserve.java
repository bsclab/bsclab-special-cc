package kr.ac.pusan.bsclab.bab.assembly.rserver.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.rosuda.REngine.Rserve.RConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class StreamHog extends Thread {
	
	InputStream is;
	boolean capture;
	String installPath;
	
	StreamHog(InputStream is, boolean capture) {
		this.is = is;
		this.capture = capture;
		start();
	}

	public String getInstallPath() {
		return installPath;
	}

	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = br.readLine()) != null) {
				if (capture) { // we are supposed to capture the output from REG
								// command
					int i = line.indexOf("InstallPath");
					if (i >= 0) {
						String s = line.substring(i + 11).trim();
						int j = s.indexOf("REG_SZ");
						if (j >= 0)
							s = s.substring(j + 6).trim();
						installPath = s;
						System.out.println("R InstallPath = " + s);
					}
				} else
					System.out.println("Rserve>" + line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}


public class StartRserve {
	private static final Logger log = LoggerFactory.getLogger(StartRserve.class);
	public static boolean launchRserve(String cmd) {
		return launchRserve(cmd, "--no-save --slave", "--no-save --slave", false);
	}

	
	public static boolean launchRserve(String cmd, String rargs, String rsrvargs, boolean debug) {
		try {
			Process p;
			boolean isWindows = false;
			String osname = System.getProperty("os.name");
			if (osname != null && osname.length() >= 7 && osname.substring(0, 7).equals("Windows")) {
				isWindows = true; /* Windows */
				p = Runtime.getRuntime().exec("\"" + cmd + "\" -e \"library(Rserve);Rserve("
						+ (debug ? "TRUE" : "FALSE") + ",args='" + rsrvargs + "')\" " + rargs);
			} else /* unix  */
				p = Runtime.getRuntime().exec(new String[] { "/bin/sh", "-c", "echo 'library(Rserve);Rserve("
						+ (debug ? "TRUE" : "FALSE") + ",args=\"" + rsrvargs + "\")'|" + cmd + " " + rargs });
			System.out.println("waiting for Rserve to start ... (" + p + ")");
			// we need to fetch the output - some platforms will die if you
			// don't ...
//			StreamHog errorHog = new StreamHog(p.getErrorStream(), false);
//			StreamHog outputHog = new StreamHog(p.getInputStream(), false);
			if (!isWindows) 
				p.waitFor();
			System.out.println("call terminated, let us try to connect ...");
		} catch (Exception x) {
			System.out.println("failed to start Rserve process with " + x.getMessage());
			return false;
		}
		int attempts = 5; 
		while (attempts > 0) {
			try {
				RConnection c = new RConnection();
				System.out.println("Rserve is running.");
				c.close();
				return true;
			} catch (Exception e2) {
				System.out.println("Try failed with: " + e2.getMessage());
			}
			/*
			 * a safety sleep just in case the start up is delayed or
			 * asynchronous
			 */
			try {
				Thread.sleep(500);
			} catch (InterruptedException ix) {
			}
			;
			attempts--;
		}
		return false;
	}

	public static boolean checkLocalRserve() {
		if (isRserveRunning())
			return true;
		String osname = System.getProperty("os.name");
		if (osname != null && osname.length() >= 7 && osname.substring(0, 7).equals("Windows")) {
			System.out.println("Windows: query registry to find where R is installed ...");
			String installPath = null;
			try {
				Process rp = Runtime.getRuntime().exec("reg query HKLM\\Software\\R-core\\R");
				StreamHog regHog = new StreamHog(rp.getInputStream(), true);
				rp.waitFor();
				regHog.join();
				installPath = regHog.getInstallPath();
			} catch (Exception rge) {
				System.out.println("ERROR: unable to run REG to find the location of R: " + rge);
				return false;
			}
			if (installPath == null) {
				System.out.println(
						"ERROR: canot find path to R. Make sure reg is available and R was installed with registry settings.");
				return false;
			}
			return launchRserve(installPath + "\\bin\\R.exe");
		}
		return (launchRserve("R") || /* try some common unix locations of R */
				((new File("/Library/Frameworks/R.framework/Resources/bin/R")).exists()
						&& launchRserve("/Library/Frameworks/R.framework/Resources/bin/R"))
				|| ((new File("/usr/lib64/R/library/R")).exists() && launchRserve("/usr/lib64/R/library/R"))
				|| ((new File("/usr/local/lib/R/bin/R")).exists() && launchRserve("/usr/local/lib/R/bin/R"))
				|| ((new File("/usr/lib/R/bin/R")).exists() && launchRserve("/usr/lib/R/bin/R"))
				|| ((new File("/usr/local/bin/R")).exists() && launchRserve("/usr/local/bin/R"))
				|| ((new File("/sw/bin/R")).exists() && launchRserve("/sw/bin/R"))
				|| ((new File("/usr/common/bin/R")).exists() && launchRserve("/usr/common/bin/R"))
				|| ((new File("/opt/bin/R")).exists() && launchRserve("/opt/bin/R")));
	}

	
	public static boolean isRserveRunning() {
		try {
			RConnection c = new RConnection();
			System.out.println("Rserve is running.");
			c.close();
			return true;
		} catch (Throwable  e) {
			System.out.println("First connect try failed with: " + e.getMessage());
		} 
		return false;
		
	}
	public static boolean isRserveRunning(String ip, int port) {
		try {
			RConnection c = new RConnection(ip, port);
			log.info("Rserve is running.");
			c.close();
			return true;
		} catch (Throwable  e) {
			System.out.println("R-Serve demon is not running, with error : " + e.getMessage());
		} 
		return false;
		
	}

	/** just a demo  */
	/*public static void main(String[] args) {
		System.out.println("result=" + checkLocalRserve());
		try {
			RConnection c = new RConnection();
			c.shutdown();
		} catch (Exception x) {
		}
		;
	}*/
}
