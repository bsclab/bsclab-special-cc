package kr.ac.pusan.bsclab.bab.assembly.rserver.param;

import java.util.List;
import org.springframework.stereotype.Component;

/**
 * Class for wrapping request parameters that defined by Parameter class and
 * binding it with user request id
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */
@Component
public class RequestWrapper {

	private String id;
	List<ResponseEntity> entities;

	/**
	 * @return entities
	 */
	public List<ResponseEntity> getEntities() {
		return entities;
	}

	/**
	 * @param entities
	 */
	public void setEntities(List<ResponseEntity> entities) {
		this.entities = entities;
	}

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
}
