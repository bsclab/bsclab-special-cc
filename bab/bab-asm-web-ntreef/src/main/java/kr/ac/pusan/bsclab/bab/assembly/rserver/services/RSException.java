package kr.ac.pusan.bsclab.bab.assembly.rserver.services;

/**
 * Class for handling R-Service exception
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */

public class RSException extends Exception{

	private static final long serialVersionUID = 1L;

	public RSException(String msg){
		super(msg);
	}
}
