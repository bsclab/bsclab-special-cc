package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SP9 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10245"))
 if(w <= 372) return 12;
 if(w > 372) return 27;
if(p.equals("AN10252")) return 37;
if(p.equals("AN10258")) return 29;
if(p.equals("PP11010"))
 if(t <= 2.6)
  if(t <= 2.475)
   if(w <= 414) return 32;
   if(w > 414)
    if(t <= 2.2) return 36.5;
    if(t > 2.2) return 31;
  if(t > 2.475)
   if(w <= 392) return 37;
   if(w > 392) return 23;
 if(t > 2.6)
  if(we <= 5605)
   if(t <= 3.04)
    if(w <= 273) return 14;
    if(w > 273) return 18.5;
   if(t > 3.04)
    if(t <= 3.7)
     if(we <= 4979) return 35;
     if(we > 4979) return 20;
    if(t > 3.7) return 21.5;
  if(we > 5605)
   if(w <= 416) return 28;
   if(w > 416)
    if(w <= 422) return 30;
    if(w > 422) return 23;
if(p.equals("PP11015")) return 46;
if(p.equals("PP11021"))
 if(w <= 344) return 25;
 if(w > 344)
  if(t <= 3.7) return 28;
  if(t > 3.7) return 22;
if(p.equals("PP11036"))
 if(t <= 1.4)
  if(w <= 310) return 45;
  if(w > 310) return 22;
 if(t > 1.4)
  if(t <= 1.66) return 33;
  if(t > 1.66) return 26.5;
if(p.equals("PP11037"))
 if(t <= 1)
  if(w <= 344)
   if(we <= 4648) return 38;
   if(we > 4648) return 32;
  if(w > 344)
   if(w <= 395)
    if(we <= 5058) return 45;
    if(we > 5058) return 41;
   if(w > 395) return 28;
 if(t > 1)
  if(t <= 1.01)
   if(we <= 5152) return 41;
   if(we > 5152) return 42;
  if(t > 1.01)
   if(t <= 1.13)
    if(w <= 319) return 39;
    if(w > 319)
     if(we <= 4421) return 34.5;
     if(we > 4421) return 45;
   if(t > 1.13) return 31;
if(p.equals("PP11038")) return 39;
if(p.equals("PP11039"))
 if(t <= 0.8)
  if(w <= 326) return 44;
  if(w > 326) return 35;
 if(t > 0.8)
  if(t <= 0.9) return 27;
  if(t > 0.9) return 30;
if(p.equals("PP11041")) return 42;
if(p.equals("PP11988")) return 46;
if(p.equals("PP12035"))
 if(w <= 306) return 41;
 if(w > 306) return 29;
if(p.equals("PP12037")) return 31;
if(p.equals("PP12069")) return 72;
if(p.equals("PP12141"))
 if(t <= 0.9) return 33;
 if(t > 0.9) return 29;
if(p.equals("PP12158"))
 if(t <= 2.075) return 30;
 if(t > 2.075) return 29;
if(p.equals("PP12254")) return 42;
if(p.equals("PP12269")) return 45;
if(p.equals("PP12289"))
 if(w <= 350) return 32.5;
 if(w > 350) return 32;
if(p.equals("PP12315")) return 38;
if(p.equals("PP12394"))
 if(t <= 1.82) return 28;
 if(t > 1.82) return 24;
if(p.equals("PP12401"))
 if(w <= 535) return 36;
 if(w > 535)
  if(we <= 7927) return 46;
  if(we > 7927) return 38;
if(p.equals("PP12403"))
 if(w <= 498) return 42;
 if(w > 498)
  if(we <= 8094) return 32;
  if(we > 8094) return 37;
if(p.equals("PP12413")) return 36.5;
if(p.equals("PP12452"))
 if(t <= 2.6)
  if(t <= 2.25)
   if(t <= 2.01) return 36.5;
   if(t > 2.01) return 38;
  if(t > 2.25)
   if(we <= 11194) return 50;
   if(we > 11194) return 42;
 if(t > 2.6)
  if(t <= 3.04) return 46;
  if(t > 3.04)
   if(w <= 614) return 36;
   if(w > 614) return 33;
if(p.equals("PP12536"))
 if(t <= 1.63) return 48;
 if(t > 1.63) return 41;
if(p.equals("PP12539")) return 26.5;
if(p.equals("PP21345")) return 20;
if(p.equals("PP21346"))
 if(w <= 530)
  if(t <= 2.75)
   if(t <= 2.6)
    if(w <= 482)
     if(t <= 2.48)
      if(we <= 8501) return 35.5;
      if(we > 8501) return 19;
     if(t > 2.48)
      if(w <= 478)
       if(we <= 9060) return 35;
       if(we > 9060) return 27;
      if(w > 478) return 35.5;
    if(w > 482) return 40;
   if(t > 2.6)
    if(w <= 518) return 34;
    if(w > 518)
     if(we <= 10668) return 19;
     if(we > 10668) return 29;
  if(t > 2.75)
   if(w <= 424)
    if(t <= 4.65)
     if(t <= 4.49)
      if(w <= 342) return 28;
      if(w > 342) return 30;
     if(t > 4.49) return 23;
    if(t > 4.65)
     if(t <= 4.98) return 32;
     if(t > 4.98)
      if(we <= 8291) return 32.5;
      if(we > 8291) return 30;
   if(w > 424)
    if(we <= 10588)
     if(w <= 488) return 32;
     if(w > 488)
      if(w <= 522)
       if(we <= 10390) return 37;
       if(we > 10390) return 29.5;
      if(w > 522) return 23;
    if(we > 10588)
     if(t <= 4.98) return 34;
     if(t > 4.98)
      if(w <= 522) return 25;
      if(w > 522) return 24;
 if(w > 530)
  if(we <= 11322)
   if(w <= 559)
    if(t <= 2.95)
     if(we <= 10961) return 42;
     if(we > 10961) return 35;
    if(t > 2.95)
     if(w <= 555) return 28;
     if(w > 555)
      if(w <= 556) return 35;
      if(w > 556) return 21;
   if(w > 559)
    if(we <= 11048)
     if(we <= 9664) return 27;
     if(we > 9664) return 28;
    if(we > 11048)
     if(we <= 11217) return 52;
     if(we > 11217) return 38;
  if(we > 11322)
   if(we <= 11724)
    if(w <= 572)
     if(t <= 3.48)
      if(w <= 568)
       if(we <= 11611) return 32;
       if(we > 11611)
        if(we <= 11675) return 24;
        if(we > 11675) return 29;
      if(w > 568) return 23;
     if(t > 3.48)
      if(t <= 4.65) return 39;
      if(t > 4.65) return 32;
    if(w > 572)
     if(w <= 604)
      if(w <= 579) return 26;
      if(w > 579) return 41;
     if(w > 604) return 35;
   if(we > 11724)
    if(t <= 4.98)
     if(t <= 2.5)
      if(w <= 576) return 40;
      if(w > 576)
       if(w <= 583) return 36;
       if(w > 583) return 30;
     if(t > 2.5)
      if(t <= 4.65)
       if(we <= 12285) return 33;
       if(we > 12285) return 27;
      if(t > 4.65) return 36;
    if(t > 4.98) return 30;
if(p.equals("PP21389"))
 if(t <= 3.7)
  if(we <= 10491)
   if(w <= 503)
    if(we <= 9962)
     if(w <= 490) return 38;
     if(w > 490) return 34.5;
    if(we > 9962)
     if(we <= 10114)
      if(we <= 10058) return 30;
      if(we > 10058) return 37;
     if(we > 10114) return 39.5;
   if(w > 503)
    if(w <= 515) return 29;
    if(w > 515)
     if(we <= 7413) return 28;
     if(we > 7413) return 32;
  if(we > 10491)
   if(w <= 591)
    if(t <= 3.24) return 31;
    if(t > 3.24) return 37;
   if(w > 591) return 49;
 if(t > 3.7)
  if(w <= 521)
   if(we <= 9444)
    if(w <= 484)
     if(we <= 7518) return 27;
     if(we > 7518) return 25;
    if(w > 484)
     if(we <= 9276)
      if(we <= 8616) return 30;
      if(we > 8616) return 21;
     if(we > 9276)
      if(we <= 9336) return 32;
      if(we > 9336) return 41;
   if(we > 9444)
    if(w <= 492)
     if(w <= 480) return 34;
     if(w > 480)
      if(we <= 9652) return 28;
      if(we > 9652)
       if(we <= 10022) return 33;
       if(we > 10022) return 37;
    if(w > 492)
     if(we <= 10444) return 23;
     if(we > 10444) return 25;
  if(w > 521)
   if(w <= 580)
    if(w <= 558)
     if(t <= 4.2)
      if(w <= 543)
       if(we <= 9962) return 31;
       if(we > 9962)
        if(we <= 10180) return 27;
        if(we > 10180) return 28.5;
      if(w > 543)
       if(we <= 10154) return 31;
       if(we > 10154) return 29;
     if(t > 4.2) return 24;
    if(w > 558)
     if(w <= 564) return 24;
     if(w > 564) return 39;
   if(w > 580)
    if(w <= 620)
     if(w <= 591)
      if(w <= 588)
       if(we <= 11451) return 37;
       if(we > 11451) return 40;
      if(w > 588)
       if(we <= 11065) return 28;
       if(we > 11065) return 32;
     if(w > 591)
      if(w <= 597) return 35;
      if(w > 597) return 26;
    if(w > 620)
     if(we <= 12759)
      if(we <= 12285)
       if(we <= 11397) return 31;
       if(we > 11397) return 28;
      if(we > 12285)
       if(we <= 12705)
        if(we <= 12433) return 33;
        if(we > 12433) return 31;
       if(we > 12705) return 33;
     if(we > 12759)
      if(we <= 13005) return 25;
      if(we > 13005)
       if(we <= 13250) return 32;
       if(we > 13250) return 31;
if(p.equals("PP21393"))
 if(t <= 3.04)
  if(w <= 496)
   if(t <= 2.99)
    if(we <= 9187)
     if(w <= 402) return 32;
     if(w > 402)
      if(t <= 2.6) return 35;
      if(t > 2.6) return 27;
    if(we > 9187)
     if(t <= 2.6) return 31;
     if(t > 2.6) return 38;
   if(t > 2.99)
    if(w <= 413)
     if(we <= 8136) return 28;
     if(we > 8136)
      if(we <= 8317) return 30;
      if(we > 8317) return 44;
    if(w > 413)
     if(w <= 416) return 26.5;
     if(w > 416) return 35;
  if(w > 496)
   if(t <= 2.6)
    if(t <= 2.48)
     if(w <= 583)
      if(w <= 511) return 16;
      if(w > 511) return 35;
     if(w > 583) return 29;
    if(t > 2.48) return 25;
   if(t > 2.6)
    if(w <= 598)
     if(t <= 2.75) return 39;
     if(t > 2.75)
      if(t <= 2.98) return 22;
      if(t > 2.98) return 32;
    if(w > 598)
     if(t <= 2.995)
      if(t <= 2.72) return 20;
      if(t > 2.72) return 40.5;
     if(t > 2.995)
      if(w <= 611) return 25;
      if(w > 611) return 34;
 if(t > 3.04)
  if(t <= 4.46)
   if(t <= 3.5)
    if(t <= 3.435)
     if(w <= 599)
      if(we <= 5783) return 29.5;
      if(we > 5783)
       if(t <= 3.225) return 21;
       if(t > 3.225)
        if(we <= 10785) return 44;
        if(we > 10785) return 33;
     if(w > 599)
      if(w <= 600) return 28.5;
      if(w > 600)
       if(we <= 10012) return 42;
       if(we > 10012) return 19;
    if(t > 3.435)
     if(we <= 9772)
      if(t <= 3.46)
       if(w <= 616) return 30;
       if(w > 616) return 38;
      if(t > 3.46) return 38;
     if(we > 9772)
      if(w <= 614) return 33;
      if(w > 614) return 34;
   if(t > 3.5)
    if(we <= 9830)
     if(t <= 3.97)
      if(we <= 8545)
       if(t <= 3.7) return 26;
       if(t > 3.7)
        if(w <= 376) return 35;
        if(w > 376) return 37;
      if(we > 8545) return 40;
     if(t > 3.97)
      if(we <= 7874)
       if(w <= 359) return 30;
       if(w > 359)
        if(w <= 394) return 34.3333333333333;
        if(w > 394) return 27;
      if(we > 7874)
       if(t <= 3.995) return 39;
       if(t > 3.995)
        if(t <= 4.2)
         if(w <= 543)
          if(w <= 498) return 35.5;
          if(w > 498) return 48;
         if(w > 543) return 22;
        if(t > 4.2) return 29;
    if(we > 9830)
     if(we <= 10825)
      if(t <= 3.97)
       if(t <= 3.92)
        if(t <= 3.83) return 23.5;
        if(t > 3.83) return 33;
       if(t > 3.92) return 30;
      if(t > 3.97)
       if(w <= 518)
        if(w <= 503) return 25.5;
        if(w > 503) return 28;
       if(w > 518)
        if(we <= 10540) return 27.5;
        if(we > 10540) return 25;
     if(we > 10825)
      if(t <= 3.97)
       if(t <= 3.83) return 27.5;
       if(t > 3.83)
        if(w <= 598) return 32;
        if(w > 598) return 23;
      if(t > 3.97)
       if(t <= 3.98)
        if(w <= 579) return 38.5;
        if(w > 579) return 37;
       if(t > 3.98) return 35;
  if(t > 4.46)
   if(t <= 4.97)
    if(we <= 9937)
     if(we <= 8702)
      if(t <= 4.48) return 34;
      if(t > 4.48)
       if(we <= 7309) return 25;
       if(we > 7309) return 32;
     if(we > 8702)
      if(t <= 4.87)
       if(we <= 9660) return 30;
       if(we > 9660)
        if(t <= 4.48) return 25;
        if(t > 4.48) return 40;
      if(t > 4.87)
       if(w <= 496) return 28;
       if(w > 496) return 34;
    if(we > 9937)
     if(t <= 4.87)
      if(w <= 551)
       if(w <= 535)
        if(t <= 4.65) return 33;
        if(t > 4.65) return 27;
       if(w > 535) return 27;
      if(w > 551)
       if(we <= 12106) return 37;
       if(we > 12106) return 33;
     if(t > 4.87)
      if(w <= 555)
       if(w <= 542) return 23;
       if(w > 542) return 35;
      if(w > 555)
       if(t <= 4.95) return 23.5;
       if(t > 4.95) return 25;
   if(t > 4.97)
    if(w <= 582)
     if(w <= 581)
      if(w <= 510) return 24;
      if(w > 510) return 27;
     if(w > 581)
      if(we <= 11910)
       if(we <= 10747) return 24;
       if(we > 10747) return 28.5;
      if(we > 11910)
       if(we <= 12039) return 33;
       if(we > 12039) return 24;
    if(w > 582)
     if(t <= 4.98)
      if(we <= 12106) return 36;
      if(we > 12106) return 32;
     if(t > 4.98)
      if(we <= 11556) return 30;
      if(we > 11556) return 32;
if(p.equals("PP21456"))
 if(w <= 599)
  if(t <= 1.2) return 62;
  if(t > 1.2)
   if(t <= 1.76) return 31;
   if(t > 1.76) return 31.5;
 if(w > 599)
  if(t <= 0.9)
   if(w <= 626)
    if(t <= 0.7) return 45;
    if(t > 0.7) return 49;
   if(w > 626)
    if(w <= 640) return 41;
    if(w > 640) return 52;
  if(t > 0.9)
   if(w <= 613) return 44;
   if(w > 613) return 37;
if(p.equals("PP21475"))
 if(w <= 492) return 32.5;
 if(w > 492) return 20.5;
if(p.equals("PP21476"))
 if(w <= 510)
  if(we <= 8402)
   if(t <= 3.97)
    if(w <= 330)
     if(t <= 2.3) return 24.3333333333333;
     if(t > 2.3) return 31;
    if(w > 330)
     if(w <= 494)
      if(t <= 3.3) return 25;
      if(t > 3.3) return 31;
     if(w > 494) return 18;
   if(t > 3.97)
    if(w <= 373) return 43;
    if(w > 373)
     if(w <= 435) return 32;
     if(w > 435)
      if(we <= 7479) return 31;
      if(we > 7479) return 29;
  if(we > 8402)
   if(t <= 4.48)
    if(t <= 3.16)
     if(w <= 500)
      if(w <= 470) return 27;
      if(w > 470)
       if(we <= 9509) return 19;
       if(we > 9509) return 33;
     if(w > 500)
      if(w <= 509) return 40;
      if(w > 509) return 26;
    if(t > 3.16) return 33;
   if(t > 4.48)
    if(we <= 9985)
     if(we <= 9366) return 29;
     if(we > 9366)
      if(we <= 9816) return 27;
      if(we > 9816) return 26.5;
    if(we > 9985) return 31;
 if(w > 510)
  if(w <= 591)
   if(w <= 520)
    if(t <= 3.555) return 30;
    if(t > 3.555)
     if(we <= 10495) return 27;
     if(we > 10495)
      if(w <= 516) return 38;
      if(w > 516)
       if(t <= 4.49) return 27.5;
       if(t > 4.49) return 25;
   if(w > 520)
    if(t <= 3.97) return 30;
    if(t > 3.97)
     if(w <= 542)
      if(w <= 528)
       if(w <= 524)
        if(t <= 4.3) return 37.5;
        if(t > 4.3) return 22;
       if(w > 524)
        if(we <= 10615) return 28;
        if(we > 10615)
         if(we <= 10629) return 29;
         if(we > 10629) return 36;
      if(w > 528)
       if(w <= 533)
        if(w <= 530) return 33;
        if(w > 530) return 28;
       if(w > 533)
        if(we <= 10599) return 36;
        if(we > 10599) return 38;
     if(w > 542)
      if(t <= 3.98)
       if(w <= 548)
        if(w <= 547) return 25;
        if(w > 547) return 22.5;
       if(w > 548)
        if(we <= 11619) return 29;
        if(we > 11619) return 32.5;
      if(t > 3.98)
       if(t <= 4.49)
        if(t <= 4.48) return 30;
        if(t > 4.48) return 39;
       if(t > 4.49)
        if(t <= 4.5) return 25;
        if(t > 4.5)
         if(w <= 553) return 27;
         if(w > 553) return 23;
  if(w > 591)
   if(we <= 11719)
    if(we <= 11113)
     if(we <= 9561)
      if(w <= 602) return 27;
      if(w > 602) return 26;
     if(we > 9561) return 42;
    if(we > 11113)
     if(we <= 11633)
      if(t <= 2.805) return 37;
      if(t > 2.805) return 31.5;
     if(we > 11633)
      if(w <= 613) return 40;
      if(w > 613) return 33;
   if(we > 11719)
    if(w <= 613)
     if(we <= 12157)
      if(we <= 11910) return 44;
      if(we > 11910) return 35;
     if(we > 12157)
      if(we <= 12235)
       if(we <= 12191) return 36.5;
       if(we > 12191)
        if(we <= 12221) return 32.5;
        if(we > 12221) return 30.5;
      if(we > 12235)
       if(we <= 12251) return 35.6666666666667;
       if(we > 12251) return 32;
    if(w > 613)
     if(we <= 11919) return 33.5;
     if(we > 11919) return 27;
if(p.equals("PP21487"))
 if(t <= 1.63) return 29;
 if(t > 1.63) return 32;
if(p.equals("PP21504")) return 55;
if(p.equals("PP21516")) return 38;
if(p.equals("PP21533"))
 if(w <= 410) return 30;
 if(w > 410) return 27;
if(p.equals("PP21557"))
 if(t <= 2.6) return 31;
 if(t > 2.6)
  if(t <= 3.04) return 29;
  if(t > 3.04) return 36;
if(p.equals("PP21577"))
 if(w <= 600)
  if(we <= 7244)
   if(t <= 1.63)
    if(t <= 1.3)
     if(t <= 1.09) return 25;
     if(t > 1.09) return 32;
    if(t > 1.3)
     if(t <= 1.51) return 20.5;
     if(t > 1.51) return 26;
   if(t > 1.63)
    if(t <= 1.9)
     if(t <= 1.82) return 29;
     if(t > 1.82) return 38.5;
    if(t > 1.9) return 35;
  if(we > 7244)
   if(w <= 567)
    if(t <= 1.86)
     if(w <= 559)
      if(we <= 8359)
       if(t <= 1.51) return 33;
       if(t > 1.51) return 30;
      if(we > 8359)
       if(w <= 450)
        if(we <= 8449) return 38;
        if(we > 8449) return 54;
       if(w > 450)
        if(t <= 1.6) return 30;
        if(t > 1.6) return 34;
     if(w > 559)
      if(we <= 11686) return 37.5;
      if(we > 11686)
       if(we <= 11966) return 33;
       if(we > 11966) return 31;
    if(t > 1.86) return 35;
   if(w > 567)
    if(we <= 11945) return 35;
    if(we > 11945) return 38;
 if(w > 600)
  if(t <= 1.795)
   if(t <= 1.3)
    if(w <= 616) return 33;
    if(w > 616)
     if(we <= 9053) return 44;
     if(we > 9053) return 48;
   if(t > 1.3) return 34;
  if(t > 1.795)
   if(t <= 1.91)
    if(we <= 10869) return 42;
    if(we > 10869) return 37;
   if(t > 1.91)
    if(t <= 1.98)
     if(we <= 12537) return 75;
     if(we > 12537) return 50;
    if(t > 1.98) return 18;
if(p.equals("PP21580"))
 if(t <= 1.63)
  if(w <= 590) return 36;
  if(w > 590) return 37;
 if(t > 1.63)
  if(t <= 1.66)
   if(we <= 11084) return 38;
   if(we > 11084) return 33;
  if(t > 1.66)
   if(t <= 1.82) return 27;
   if(t > 1.82) return 28;
if(p.equals("PP21601")) return 20;
if(p.equals("PP21632")) return 38;
if(p.equals("PP21677")) return 34;
if(p.equals("PP21684")) return 82;
if(p.equals("PP21690")) return 29;
if(p.equals("PP21694"))
 if(w <= 556) return 35;
 if(w > 556)
  if(t <= 1.51) return 37;
  if(t > 1.51) return 32;
if(p.equals("PP21752")) return 37;
if(p.equals("PP21753")) return 32;
if(p.equals("PP21762")) return 111.5;
if(p.equals("PP21792")) return 29;
if(p.equals("PP21809")) return 31;
if(p.equals("PP21855"))
 if(t <= 1.66) return 35;
 if(t > 1.66) return 28;
if(p.equals("PP21903")) return 40;
if(p.equals("PP21908")) return 38;
if(p.equals("PP22018"))
 if(we <= 10681)
  if(we <= 7546)
   if(w <= 425) return 26;
   if(w > 425) return 27;
  if(we > 7546)
   if(t <= 2.8) return 22.5;
   if(t > 2.8)
    if(t <= 3.04) return 15;
    if(t > 3.04) return 31;
 if(we > 10681)
  if(w <= 567)
   if(w <= 553) return 40;
   if(w > 553) return 35.5;
  if(w > 567)
   if(w <= 576) return 21.5;
   if(w > 576) return 32;
if(p.equals("PP22060"))
 if(we <= 11463)
  if(w <= 576)
   if(t <= 3.6)
    if(t <= 3.18)
     if(we <= 9866) return 20;
     if(we > 9866) return 30;
    if(t > 3.18)
     if(t <= 3.355)
      if(w <= 425) return 31;
      if(w > 425) return 33;
     if(t > 3.355) return 34;
   if(t > 3.6)
    if(w <= 528)
     if(we <= 9524)
      if(w <= 425) return 22;
      if(w > 425)
       if(we <= 8695) return 28;
       if(we > 8695) return 47;
     if(we > 9524) return 32;
    if(w > 528)
     if(w <= 547) return 31;
     if(w > 547) return 38;
  if(w > 576)
   if(we <= 11017)
    if(w <= 583) return 30;
    if(w > 583)
     if(t <= 3.6) return 160;
     if(t > 3.6) return 28;
   if(we > 11017)
    if(w <= 578) return 37;
    if(w > 578) return 45;
 if(we > 11463)
  if(w <= 583)
   if(w <= 578)
    if(we <= 11667)
     if(we <= 11520) return 41.5;
     if(we > 11520) return 37;
    if(we > 11667)
     if(we <= 11736) return 35;
     if(we > 11736) return 33.5;
   if(w > 578)
    if(we <= 11539) return 29;
    if(we > 11539)
     if(we <= 11625) return 29.5;
     if(we > 11625) return 28.5;
  if(w > 583)
   if(w <= 598)
    if(w <= 592) return 26;
    if(w > 592) return 19;
   if(w > 598)
    if(t <= 4.2) return 108;
    if(t > 4.2) return 17;
if(p.equals("PP22069"))
 if(t <= 1.3) return 26;
 if(t > 1.3)
  if(we <= 7518)
   if(we <= 7257) return 35;
   if(we > 7257) return 36;
  if(we > 7518)
   if(w <= 594)
    if(we <= 7697) return 19;
    if(we > 7697) return 29;
   if(w > 594) return 22;
if(p.equals("PP22076")) return 55;
if(p.equals("PP22079"))
 if(t <= 1.61) return 35;
 if(t > 1.61) return 27;
if(p.equals("PP22085"))
 if(w <= 561)
  if(t <= 1.795)
   if(we <= 11171) return 29;
   if(we > 11171) return 31;
  if(t > 1.795) return 20;
 if(w > 561)
  if(t <= 1.9)
   if(w <= 626) return 34;
   if(w > 626) return 40;
  if(t > 1.9) return 31;
if(p.equals("PP22107")) return 38;
if(p.equals("PP22180")) return 23;
if(p.equals("PP22181")) return 23;
if(p.equals("PP22201")) return 35;
if(p.equals("PP22232"))
 if(we <= 7309)
  if(w <= 376) return 36.5;
  if(w > 376)
   if(we <= 6860)
    if(we <= 6717) return 33.6666666666667;
    if(we > 6717) return 31;
   if(we > 6860)
    if(we <= 7153) return 36.3333333333333;
    if(we > 7153) return 39.6666666666667;
 if(we > 7309)
  if(w <= 380)
   if(we <= 7471) return 37;
   if(we > 7471)
    if(we <= 7546) return 35;
    if(we > 7546) return 29;
  if(w > 380)
   if(w <= 425) return 27;
   if(w > 425) return 23;
if(p.equals("PP22259")) return 41;
if(p.equals("PP22269"))
 if(w <= 599) return 34;
 if(w > 599) return 34.5;
if(p.equals("PP22270")) return 32;
if(p.equals("PP22273"))
 if(we <= 7843)
  if(we <= 4782) return 29;
  if(we > 4782) return 36;
 if(we > 7843)
  if(t <= 1.6) return 38;
  if(t > 1.6) return 37;
if(p.equals("PP22274")) return 32;
if(p.equals("PP22285")) return 33;
if(p.equals("PP22288"))
 if(t <= 4.2)
  if(w <= 520) return 39;
  if(w > 520)
   if(w <= 599) return 33.5;
   if(w > 599) return 28;
 if(t > 4.2)
  if(t <= 4.65) return 31;
  if(t > 4.65) return 32;
if(p.equals("PP22289")) return 28;
if(p.equals("PP22304"))
 if(t <= 4)
  if(w <= 501) return 30;
  if(w > 501) return 26.5;
 if(t > 4)
  if(w <= 514) return 25;
  if(w > 514) return 37;
if(p.equals("PP22305"))
 if(we <= 10599) return 27.5;
 if(we > 10599) return 31;
if(p.equals("PP22310")) return 50;
if(p.equals("PP22315"))
 if(we <= 9008) return 110;
 if(we > 9008) return 28;
if(p.equals("PP22324")) return 51;
if(p.equals("PP22386")) return 29;
if(p.equals("PP22419")) return 25;
if(p.equals("PP22423"))
 if(t <= 1.21) return 36;
 if(t > 1.21) return 45;
if(p.equals("PP22438")) return 41;
if(p.equals("PP22459")) return 32;
if(p.equals("PP22478")) return 42;
if(p.equals("PP22479")) return 31;
if(p.equals("PP22496")) return 32;
if(p.equals("PP22539"))
 if(we <= 11483) return 28;
 if(we > 11483) return 31.5;
return 31.5;
}
}
