package kr.ac.pusan.bsclab.bab.assembly.domain.info;


@Deprecated
public enum JobType {
	
	SAVE_CSV
	, SAVE_CSV_START
	, SAVE_CSV_DONE
	, SAVE_CSV_FAIL
	, DB_TO_HDFS
	, DB_TO_HDFS_START
	, DB_TO_HDFS_DONE
	, DB_TO_HDFS_FAIL

}
