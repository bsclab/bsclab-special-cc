package kr.ac.pusan.bsclab.bab.assembly.domain.factory;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="sf_oper_det")
public class OperDet {
	
	@Id
	@Column(name="BUS_CD")
	String busCd;

	@Column(name="COIL_NO")
	String coilNo;

	@Column(name="PRC_CD")
	String prcCd;

	@Column(name="SEQ")
	String seq;

	@Column(name="LOT_NO")
	String lotNo;

	@Column(name="BASIC_COIL_NO")
	String basicCoilNo;

	@Column(name="OSTWDT_LEN")
	String ostwdtLen;

	@Column(name="OSTSKP_QTY")
	String ostskpQty;

	@Column(name="OSTDIV_QTY")
	String ostdivQty;

	@Column(name="OSTSTD_QTY")
	String oststdQty;

	@Column(name="OSTSTS_CD")
	String oststsCd;

	@Column(name="TAT_LEN")
	String tatLen;

	@Column(name="THK_RAN")
	String thkRan;

	@Column(name="PSS_RAT")
	String pssRat;

	@Column(name="ANNPNT_CD")
	String annpntCd;

	@Column(name="ANNCYC_VAL")
	String anncycVal;

	@Column(name="SPN_CLS")
	String spnCls;

	@Column(name="SPM_YN")
	String spmYn;

	@Column(name="CRX_YN")
	String crxYn;

	@Column(name="EDGGRT_YN")
	String edggrtYn;

	@Column(name="COAMIN_WGT")
	String coaminWgt;

	@Column(name="COAAVG_WGT")
	String coaavgWgt;

	@Column(name="COAWGT_CD")
	String coawgtCd;

	@Column(name="DIV_RAT")
	String divRat;

	@Column(name="DIV_GRPNO")
	String divGrpno;

	@Column(name="WRKTRM_YMD")
	String wrktrmYmd;

	@Column(name="SPL_REM")
	String splRem;

	@Column(name="SAMPLEORDER_NO")
	String sampleorderNo;

	@Column(name="SILCOIL_NO")
	String silcoilNo;

	@Column(name="CRT_DT")
	String crtDt;

	@Column(name="CRTCHR_NO")
	String crtchrNo;

	@Column(name="UPD_DT")
	String updDt;

	@Column(name="UPDCHR_NO")
	String updchrNo;

	@Column(name="KEYSAMPLE_NO")
	String keysampleNo;

	@Column(name="TWL_THK_MAX")
	String twlThkMax;

	@Column(name="TWL_THK_MIN")
	String twlThkMin;

	@Column(name="TWL_HRD_MAX")
	String twlHrdMax;

	@Column(name="TWL_HRD_MIN")
	String twlHrdMin;

	@Column(name="TWL_STRENGTH_STD")
	String twlStrengthStd;

	@Column(name="TWL_MARKING_YN")
	String twlMarkingYn;

	@Column(name="TWL_OUDMAX_LEN")
	String twlOudmaxLen;

	@Column(name="TWL_OUDMIN_LEN")
	String twlOudminLen;

	@Column(name="TWL_IND_CD")
	String twlIndCd;

	@Column(name="TWL_MAX_WGT")
	String twlMaxWgt;

	@Column(name="TWL_MIN_WGT")
	String twlMinWgt;

	@Column(name="TWL_SAMPLE_YN")
	String twlSampleYn;

	@Column(name="TWL_SLEEVE_CD")
	String twlSleeveCd;

	@Column(name="TWL_STRENGTH_SHP")
	String twlStrengthShp;

	@Column(name="ZON_CD")
	String zonCd;


}

