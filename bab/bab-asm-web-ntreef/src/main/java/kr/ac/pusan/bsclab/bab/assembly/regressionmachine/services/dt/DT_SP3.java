package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_SP3 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10245"))
 if(w <= 421)
  if(t <= 5.1)
   if(w <= 371) return 41.0166666666667;
   if(w > 371) return 25;
  if(t > 5.1)
   if(w <= 419)
    if(t <= 5.36)
     if(we <= 8250) return 50;
     if(we > 8250) return 30;
    if(t > 5.36) return 20;
   if(w > 419)
    if(we <= 8155)
     if(we <= 7927) return 17;
     if(we > 7927) return 20;
    if(we > 8155)
     if(we <= 8329) return 25;
     if(we > 8329) return 21;
 if(w > 421)
  if(t <= 5.2)
   if(t <= 4.96)
    if(t <= 4.75) return 22;
    if(t > 4.75) return 36.2833333333333;
   if(t > 4.96)
    if(w <= 422) return 25.0333333333333;
    if(w > 422)
     if(we <= 8329) return 23.3333333333333;
     if(we > 8329) return 30;
  if(t > 5.2)
   if(we <= 8589)
    if(t <= 5.7) return 19;
    if(t > 5.7)
     if(w <= 439) return 45;
     if(w > 439) return 25;
   if(we > 8589) return 23;
if(p.equals("PP21346"))
 if(w <= 601)
  if(w <= 556) return 27;
  if(w > 556)
   if(we <= 11743)
    if(we <= 10611) return 20;
    if(we > 10611)
     if(t <= 6) return 45;
     if(t > 6) return 16;
   if(we > 11743)
    if(w <= 596) return 35;
    if(w > 596) return 30;
 if(w > 601)
  if(we <= 12136)
   if(w <= 615)
    if(we <= 11980) return 17;
    if(we > 11980) return 29;
   if(w > 615) return 50;
  if(we > 12136)
   if(we <= 12340) return 46;
   if(we > 12340) return 28;
if(p.equals("PP21389"))
 if(we <= 10565) return 42.1833333333333;
 if(we > 10565)
  if(t <= 5.3) return 25;
  if(t > 5.3) return 30;
if(p.equals("PP21393"))
 if(w <= 615) return 40;
 if(w > 615) return 50;
if(p.equals("PP21903"))
 if(w <= 358)
  if(t <= 3.9)
   if(w <= 325) return 20;
   if(w > 325)
    if(we <= 6213) return 35;
    if(we > 6213) return 21;
  if(t > 3.9) return 17.7166666666667;
 if(w > 358)
  if(t <= 3.65)
   if(t <= 3.3)
    if(t <= 2.95) return 19;
    if(t > 2.95) return 22;
   if(t > 3.3) return 24.1833333333333;
  if(t > 3.65)
   if(t <= 3.84) return 19.8166666666667;
   if(t > 3.84)
    if(t <= 3.9) return 40;
    if(t > 3.9) return 50;
if(p.equals("PP22049")) return 18;
if(p.equals("PP22060")) return 22.2166666666667;
if(p.equals("PP22179")) return 21;
if(p.equals("PP22180"))
 if(t <= 4.5) return 25;
 if(t > 4.5)
  if(t <= 4.65) return 20;
  if(t > 4.65) return 22;
if(p.equals("PP22181")) return 25;
if(p.equals("PP22233"))
 if(w <= 516) return 19;
 if(w > 516) return 28.4166666666667;
if(p.equals("PP22304"))
 if(t <= 5.36) return 24;
 if(t > 5.36) return 17;
return 17.0;
}
}
