package kr.ac.pusan.bsclab.bab.assembly.parallelscheduling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann.ANN_AN1;
import kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann.ANN_ANA;

@SuppressWarnings({"unchecked","rawtypes"})
public class SchedulingSAFSA {
	long start_time_exec, duration_exec;
	long cycle_check_time = 10*1000; // 10 seconds
	
	public SyncObj syncobj = new SyncObj();
	
	List<String> init_sol_1D;
	
	HashMap<String, HashSet<String>> list_group_opr = new HashMap<String, HashSet<String>>(); // list of true job:coil_no in each of raw material+seq 
	
	boolean is_running = false;
	
	int cycle = 0;
	
	HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail;
	
	
	
	
	LinkedHashSet<String> list_of_coils; HashMap<String, List<DataAttributes>> list_of_jobs;
	
	public SchedulingSAFSA(HashMap list_group_opr, List<String> pre_solution_1D, HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail,             LinkedHashSet<String> list_of_coils, HashMap<String, List<DataAttributes>> list_of_jobs) {
		this.list_group_opr = list_group_opr;
		this.init_sol_1D = pre_solution_1D;
		syncobj.bb_sol_1D = new ArrayList<String>(this.init_sol_1D);
		this.jobopr_per_seq_detail = jobopr_per_seq_detail;
		
		
		this.list_of_coils = list_of_coils;
		this.list_of_jobs = list_of_jobs;
	}
	
	public void setDuration(long duration_exec) {
		start_time_exec = System.currentTimeMillis();
		this.duration_exec = duration_exec;
	}
	
	public void runProcess() {
		is_running = true;
		
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				try {
					ObjectMapper mapper = new ObjectMapper();
					String json = mapper.writeValueAsString(jobopr_per_seq_detail);
					TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
					HashMap<String, SchedulingDataPerOperation> new_jobopr_per_seq_detail = mapper.readValue(json, typeRef);
					doSA(new HashMap<String, SchedulingDataPerOperation>(new_jobopr_per_seq_detail));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				try {
					ObjectMapper mapper = new ObjectMapper();
					String json = mapper.writeValueAsString(jobopr_per_seq_detail);
					TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
					HashMap<String, SchedulingDataPerOperation> new_jobopr_per_seq_detail = mapper.readValue(json, typeRef);
					doFSA(new HashMap<String, SchedulingDataPerOperation>(new_jobopr_per_seq_detail));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		t1.start();
		t2.start();
		
		while(System.currentTimeMillis  () < start_time_exec + duration_exec) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		is_running = false;
	}
	
	private List<String> swapWithConsiderConstraint(List<String> cur_sol_1D, Random random, HashMap<String, SchedulingDataPerOperation> mmsch) {
		List<String> newlist = new ArrayList<String>(cur_sol_1D);
		
		int firstindex = random.nextInt(newlist.size());
		int secondindex = random.nextInt(newlist.size());
		int st_loop = 0, lt_loop = 0;
		
		boolean clear = true;
		String v_previndex = null, v_nextindex = null;
		String par_nextindex = null;
		
		if(firstindex != secondindex) {
			
			if(firstindex < secondindex) {
				v_previndex = newlist.get(firstindex);
				v_nextindex = newlist.get(secondindex);
				st_loop = firstindex; lt_loop = secondindex;
			}
			else {
				v_previndex = newlist.get(secondindex);
				v_nextindex = newlist.get(firstindex);
				st_loop = secondindex; lt_loop = firstindex;
			}
			
			List<String> sublist1 = newlist.subList(0, lt_loop); 
			String seqjob_nextindex = v_nextindex+"-"+Collections.frequency(sublist1, v_nextindex);
			par_nextindex = mmsch.get(seqjob_nextindex).parent_mat;
			
			List<String> sublist2 = newlist.subList(st_loop, lt_loop);
			
			if(par_nextindex != null && sublist2.contains(par_nextindex))
				clear = false;
			else
				for(int x=st_loop; x<=lt_loop; x++) {
					String target = newlist.get(x);
					List<String> sublist3 = newlist.subList(0, x);
					String seqjob_target = target+"-"+Collections.frequency(sublist3, target);
					String par_target = mmsch.get(seqjob_target).parent_mat;
					
					if(par_target != null && par_target.equals(v_previndex)) {
						clear = false;
						break;
					}
				}
		}
		else
			clear = false;
		
		if(clear) {
			Collections.swap(newlist, firstindex, secondindex);
		}
		return newlist;
	}
	
	public class SyncObj {
		public double bb_sol;
		public HashMap<String, List<String>> bb_jobs_per_machine;
		
		public List<String> bb_sol_1D;
		public HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail;
	}
	
	public void doSA(HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail_sa) {
		Random randnum = new Random(1);
		ObjectMapper mapper = new ObjectMapper();
		
		long flag_step_time = start_time_exec;
		boolean is_first_loop = true;
		
		List<String> cur_sol_1D = new ArrayList<String>(this.init_sol_1D);
		List<String> best_sol_1D = new ArrayList<String>(this.init_sol_1D);
		
		List<Object> receiv_data = calculateMakeSpan(cur_sol_1D, jobopr_per_seq_detail_sa, "SA");
		
		double makespan = (Double)receiv_data.get(0); // current makespan calculation
		
		double curr_sol = makespan; // current makespan from current solution choosed
		double best_sol = curr_sol;
		
		HashMap<String, List<String>> cur_jobs_per_machine = (HashMap)receiv_data.get(1); // machine to list of jobs+seq
		HashMap<String, List<String>> best_jobs_per_machine = cur_jobs_per_machine; // machine to list of jobs+seq
		
		int iter_per_temperature = 30, curr_iter_per_temperature = 1;
//		int curr_global_iter = 1;
		
		double init_temperature = 100, temperature = init_temperature;
		double alpha = 0.9;
		
		while(is_running) {
			List<String> tmp_sol_1D = cur_sol_1D;
			
			if(!is_first_loop) {
				cur_sol_1D = swapWithConsiderConstraint(cur_sol_1D, randnum, jobopr_per_seq_detail_sa);
				receiv_data = calculateMakeSpan(cur_sol_1D, jobopr_per_seq_detail_sa, "SA");		
				
				makespan = (Double)receiv_data.get(0);
			} else {
				synchronized(syncobj) {
					if(syncobj.bb_jobs_per_machine == null) {
						System.out.println("Dipanggil SA");
						syncobj.bb_sol = best_sol;
						syncobj.bb_sol_1D = new ArrayList<String>(best_sol_1D);
						try {
							String json1 = mapper.writeValueAsString(best_jobs_per_machine);
							TypeReference<HashMap<String, List<String>>> typeRef1 = new TypeReference<HashMap<String,List<String>>>() {};
							syncobj.bb_jobs_per_machine = mapper.readValue(json1, typeRef1);
							String json2 = mapper.writeValueAsString(jobopr_per_seq_detail_sa);
							TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef2 = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
							syncobj.jobopr_per_seq_detail = mapper.readValue(json2, typeRef2);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			
			if(makespan < curr_sol) {
				curr_sol = makespan;
				cur_jobs_per_machine = (HashMap)receiv_data.get(1);
				
				best_sol = curr_sol;
				best_jobs_per_machine = cur_jobs_per_machine;
				best_sol_1D = (List)receiv_data.get(2);
			}
			else if(!is_first_loop)
			{
				double delta = makespan - curr_sol;
				
				//double r = Math.random();
				double r = randnum.nextFloat();
				
				if(r <= Math.exp(-1*delta/temperature)) { // accept
					curr_sol = makespan;
					cur_jobs_per_machine = (HashMap)receiv_data.get(1);
				}
				else
					cur_sol_1D = tmp_sol_1D;
			}
			
			long nowtime = System.currentTimeMillis();
			
			if(nowtime-flag_step_time >= cycle_check_time) {
				synchronized(syncobj) {
					flag_step_time = nowtime;
					
					if(syncobj.bb_jobs_per_machine == null || best_sol < syncobj.bb_sol)
					{	
						syncobj.bb_sol = best_sol;
						syncobj.bb_sol_1D = new ArrayList<String>(best_sol_1D);
						try {
							String json1 = mapper.writeValueAsString(best_jobs_per_machine);
							TypeReference<HashMap<String, List<String>>> typeRef1 = new TypeReference<HashMap<String,List<String>>>() {};
							syncobj.bb_jobs_per_machine = mapper.readValue(json1, typeRef1);
							String json2 = mapper.writeValueAsString(jobopr_per_seq_detail_sa);
							TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef2 = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
							syncobj.jobopr_per_seq_detail = mapper.readValue(json2, typeRef2);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else if(best_sol > syncobj.bb_sol){
						best_sol = syncobj.bb_sol;
						try {
							String json1 = mapper.writeValueAsString(syncobj.bb_jobs_per_machine);
							TypeReference<HashMap<String, List<String>>> typeRef1 = new TypeReference<HashMap<String,List<String>>>() {};
							best_jobs_per_machine = mapper.readValue(json1, typeRef1);
							String json2 = mapper.writeValueAsString(syncobj.jobopr_per_seq_detail);
							TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef2 = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
							jobopr_per_seq_detail_sa = mapper.readValue(json2, typeRef2);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					System.out.println("DoSA "+syncobj.bb_sol);
				}
			}
			
			if(curr_iter_per_temperature > iter_per_temperature){
				temperature = temperature*alpha; // cooling mechanism Simulated Annealing , alpha geometric cooling
				//curr_global_iter++;
				curr_iter_per_temperature = 0;
			}
			
			is_first_loop = false;
			
			curr_iter_per_temperature++;
		}
	}
	
	public void doFSA(HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail_fsa) {
		Random randnum = new Random(1);
		ObjectMapper mapper = new ObjectMapper();
		
		long flag_step_time = start_time_exec;			
		boolean is_first_loop = true;
		
		List<String> cur_sol_1D = new ArrayList(this.init_sol_1D);
		List<String> best_sol_1D = new ArrayList<String>(this.init_sol_1D);
		
		List<Object> receiv_data = calculateMakeSpan(cur_sol_1D,  jobopr_per_seq_detail_fsa, "FSA");
		
		double makespan = (Double)receiv_data.get(0); // current makespan calculation
		
		double curr_sol = makespan; // current makespan from current solution choosed
		double best_sol = curr_sol;
		
		HashMap<String, List<String>> cur_jobs_per_machine = (HashMap)receiv_data.get(1); // machine to list of jobs+seq
		HashMap<String, List<String>> best_jobs_per_machine = cur_jobs_per_machine; // machine to list of jobs+seq
		
		int iter_per_temperature = 30, curr_iter_per_temperature = 1, curr_global_iter = 1;
		
		double init_temperature = 30, temperature = init_temperature;
		
		while(is_running) {
			List<String> tmp_sol_1D = cur_sol_1D;
			
			if(!is_first_loop) {
				cur_sol_1D = swapWithConsiderConstraint(cur_sol_1D, randnum, jobopr_per_seq_detail_fsa);
				receiv_data = calculateMakeSpan(cur_sol_1D, jobopr_per_seq_detail_fsa, "FSA");
				makespan = (Double)receiv_data.get(0);
			} else {
				synchronized(syncobj) {
					if(syncobj.bb_jobs_per_machine == null) {
						System.out.println("Dipanggil FSA");
						syncobj.bb_sol = best_sol;
						syncobj.bb_sol_1D = new ArrayList<String>(best_sol_1D);
						syncobj.bb_sol = best_sol;
						syncobj.bb_sol_1D = new ArrayList<String>(best_sol_1D);
						try {
							String json1 = mapper.writeValueAsString(best_jobs_per_machine);
							TypeReference<HashMap<String, List<String>>> typeRef1 = new TypeReference<HashMap<String,List<String>>>() {};
							syncobj.bb_jobs_per_machine = mapper.readValue(json1, typeRef1);
							String json2 = mapper.writeValueAsString(jobopr_per_seq_detail_fsa);
							TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef2 = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
							syncobj.jobopr_per_seq_detail = mapper.readValue(json2, typeRef2);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
			
			if(makespan < curr_sol) {
				curr_sol = makespan;
				cur_jobs_per_machine = (HashMap)receiv_data.get(1);
				
				best_sol = curr_sol;
				best_jobs_per_machine = cur_jobs_per_machine;
				best_sol_1D = (List)receiv_data.get(2);
			}
			else if(!is_first_loop){
				double delta = makespan - curr_sol;
				
				double r = randnum.nextFloat();
				
				if(r <= temperature/Math.sqrt((Math.pow(delta, 2))+(Math.pow(temperature, 2)))) { // accept
					curr_sol = makespan;
					cur_jobs_per_machine = (HashMap)receiv_data.get(1);
				}
				else
					cur_sol_1D = tmp_sol_1D;
			}
			
			long nowtime = System.currentTimeMillis();
			
			if(nowtime-flag_step_time >= cycle_check_time) {
				synchronized(syncobj) {
					flag_step_time = nowtime;

					if(syncobj.bb_jobs_per_machine == null || best_sol < syncobj.bb_sol)
					{	
						syncobj.bb_sol = best_sol;
						syncobj.bb_sol_1D = new ArrayList<String>(best_sol_1D);
						try {
							String json1 = mapper.writeValueAsString(best_jobs_per_machine);
							TypeReference<HashMap<String, List<String>>> typeRef1 = new TypeReference<HashMap<String,List<String>>>() {};
							syncobj.bb_jobs_per_machine = mapper.readValue(json1, typeRef1);
							String json2 = mapper.writeValueAsString(jobopr_per_seq_detail_fsa);
							TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef2 = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
							syncobj.jobopr_per_seq_detail = mapper.readValue(json2, typeRef2);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else if(best_sol > syncobj.bb_sol){
						best_sol = syncobj.bb_sol;
						try {
							String json1 = mapper.writeValueAsString(syncobj.bb_jobs_per_machine);
							TypeReference<HashMap<String, List<String>>> typeRef1 = new TypeReference<HashMap<String,List<String>>>() {};
							best_jobs_per_machine = mapper.readValue(json1, typeRef1);
							String json2 = mapper.writeValueAsString(syncobj.jobopr_per_seq_detail);
							TypeReference<HashMap<String, SchedulingDataPerOperation>> typeRef2 = new TypeReference<HashMap<String,SchedulingDataPerOperation>>() {};
							jobopr_per_seq_detail_fsa = mapper.readValue(json2, typeRef2);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					System.out.println("DoFSA "+syncobj.bb_sol);
				}
			}

			if(curr_iter_per_temperature > iter_per_temperature){
				temperature = init_temperature/curr_global_iter; // cooling mechanism Fast Simulated Annealing, inversely linear cooling
				curr_global_iter++;
				curr_iter_per_temperature = 0;
			}
			
			is_first_loop = false;
			
			curr_iter_per_temperature++;
		}
	}
	
	public List<Object> calculateMakeSpan(List<String> sol_1D, HashMap<String, SchedulingDataPerOperation> jobopr_per_seq_detail, String worker) {
		double local_makespan = 0;
		HashMap<String, Long> ed_group_opr = new HashMap<String, Long>(); // end time for each raw material (key is job:hiscoi_no job of raw material)
		HashMap<String, Integer> nmb_seq_in_raw_mat = new HashMap<String, Integer>();
		HashMap<String, List<String>> jobs_per_machine = new HashMap<String, List<String>>(); // machine to list of jobs+seq
		
		for(String jobid : sol_1D) {
			SchedulingDataPerOperation dsd;
			String jobid_seq;
			String mach;
			double dur;
			long st_time; 
			long ed_time; 
			
			if(nmb_seq_in_raw_mat.get(jobid) == null)
				nmb_seq_in_raw_mat.put(jobid, 0);
			
			int seqjob = nmb_seq_in_raw_mat.get(jobid);
			jobid_seq = jobid+"-"+seqjob;
			dsd = jobopr_per_seq_detail.get(jobid_seq);
			
			dur = dsd.duration;
			
			if(seqjob != 0) {
				st_time = jobopr_per_seq_detail.get(jobid+"-"+(seqjob-1)).ed_time; // precedence constraint, starting time is dependent of previous operation in the same job
			}
			else {
				if(dsd.parent_mat != null)
				{ 
					st_time = ed_group_opr.get(dsd.parent_mat) == null ? 0 : ed_group_opr.get(dsd.parent_mat);
				}
				else
					st_time = 0; // st_time if no other job is precede in this machine
			}
			
			nmb_seq_in_raw_mat.put(jobid, seqjob+1);
			
			mach = dsd.machine;
			
			if(jobs_per_machine.get(mach) == null && !mach.startsWith("AN"))
    			jobs_per_machine.put(mach, new ArrayList<String>());
    		
			List<String> jobseq_in_mach = null;
			
			if(mach.startsWith("AN")) {
				long st_time_tmp = st_time;
				
				String mach_ann = null;
				
				int maxbase = mach.equals("AN1") ? ANN_AN1.maxBase() : ANN_ANA.maxBase();
				outerloop:
				for(int x=0; x < maxbase; x++) {
					mach_ann = mach+x;
					jobseq_in_mach = jobs_per_machine.get(mach_ann);
					
					if(jobseq_in_mach == null) {
						jobseq_in_mach = new ArrayList<String>();
						st_time_tmp = st_time;
						break;
					}
					
					Collections.sort(jobseq_in_mach, new Comparator<String>() {
			            @Override
						public int compare(String o1, String o2) {
							long stTimeO1 = jobopr_per_seq_detail.get(o1).st_time;
				            long stTimeO2 = jobopr_per_seq_detail.get(o2).st_time;
				            return Long.compare(stTimeO1, stTimeO2);
						}
			        });
					
					for(String jobseq_mach : jobseq_in_mach) { // each job in every machine, st_time that we get previously, will be compared with this one
						SchedulingDataPerOperation dsc_jm = jobopr_per_seq_detail.get(jobseq_mach);
						
						if(dur <= dsc_jm.st_time - st_time) // durasi operasi yg mau diassign lebih kecil dari jeda waktu antara starting scheduling dan operasi pertama di mesin ini, bisa langsung di assign
						{
							st_time_tmp = st_time;
							break outerloop;
						}
						else
							st_time_tmp = st_time < dsc_jm.ed_time ? dsc_jm.ed_time : st_time;	
					}
				}
				
				st_time = st_time_tmp;
				
				if(jobs_per_machine.get(mach_ann) == null)
					jobs_per_machine.put(mach_ann, jobseq_in_mach);
				
				jobseq_in_mach.add(jobid_seq);
			} 
			else {
				jobseq_in_mach = jobs_per_machine.get(mach);
				
				Collections.sort(jobseq_in_mach,new Comparator<String>() {
		            @Override
					public int compare(String o1, String o2) {
						long stTimeO1 = jobopr_per_seq_detail.get(o1).st_time;
			            long stTimeO2 = jobopr_per_seq_detail.get(o2).st_time;
			            return Long.compare(stTimeO1, stTimeO2);
					}
		        });	
				
				for(String jobseq_mach : jobseq_in_mach) { // each job in every machine, st_time that we get previously, will be compared with this one
					SchedulingDataPerOperation dsc_jm = jobopr_per_seq_detail.get(jobseq_mach);
					
					if(dur <= dsc_jm.st_time - st_time) // durasi operasi yg mau diassign lebih kecil dari jeda waktu antara starting scheduling dan operasi pertama di mesin ini, bisa langsung di assign
						break;
					else
						st_time = st_time < dsc_jm.ed_time ? dsc_jm.ed_time : st_time;	
				}
				jobseq_in_mach.add(jobid_seq);
			}
    		
			ed_time = st_time + (int)dur;
			
			if(ed_time > local_makespan)
				local_makespan = ed_time;
			
			dsd.st_time = st_time;
			dsd.ed_time = ed_time;
			
			Long ed_job = ed_group_opr.get(jobid);
			if(ed_job == null || ed_time > ed_job)
				ed_group_opr.put(jobid, ed_time);
		}
		
		List<Object> send_data = new ArrayList<Object>();
		send_data.add(new Double(local_makespan));
		send_data.add(jobs_per_machine);
		send_data.add(sol_1D);
		
		return send_data;
	}
	
	public double getBestMakespan() {
		return this.syncobj.bb_sol;
	}
	
	public HashMap<String, SchedulingDataPerOperation> getJobOprPerSeqDetail() {
		return syncobj.jobopr_per_seq_detail;
	}
}
