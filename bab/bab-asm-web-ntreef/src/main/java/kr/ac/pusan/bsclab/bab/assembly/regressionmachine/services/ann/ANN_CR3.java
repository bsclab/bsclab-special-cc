package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_CR3 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{0.655351222, 0.00851221662e-03, 0.780609965, -20.63047924, -0.782549441, -0.799689174, -1.77318120, -0.708292186, 1.08291519, -10.73384476, -20.66967258, 0.470278561, 1.64065027, -10.67963676, -1.33592772, -0.711690903, 1.70926857, -0.654535770, -30.13445892, 20.84774284}, 
            			{0.930659771, 1.00514328, 0.601580441, -0.152440056, 0.0235351399, -0.836549997, 0.640183806, 0.596863091, -0.361679077, -1.17738581, -0.850143135, -0.0121326828, -0.613780439, -0.127390295, -0.440858275, 0.0104066683, -1.00915968, -0.938543439, 1.53671944, -1.67039979},
            			{1.06035233, -0.270776629, -0.225996137, -0.469770074, -1.08008683, -0.652137697, 1.01858735, -0.110853598, 0.303300709, -0.234991372, 1.39029646, 0.0669851229, 0.0145948119, -1.07963777, -0.632995605, -1.59138525, -0.471603453, -0.379769802, -1.07340157, 0.549818218}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-2.04041028, -0.76965964, -0.01951564, 0.95923728, -0.44955194, -0.38533396, -1.46287107, -0.40898356, -0.74409598, 1.47238493, 0.62557757, -0.78118646, -0.91390198, 0.79273152, -0.7055015, -1.38818038, -0.47228855, -0.37726963, 1.09094596, 1.11324441};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {0.44543165, -0.22196895, 0.62243706, 1.30752933, 0.27987313, -1.79785585, -1.9462837, 0.92099118, 0.4401015 , 0.44432008, 2.49002743, 0.06417399, -0.06687578, 0.77958727, -0.64379168, -0.10823229, -0.42363212, -0.44472206, 1.72670114, -0.59156412};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-1.4777683};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 10.0;
	}

	@Override
	public double getMaxWidth() {
		
		return 650.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 14033.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.36;
	}

	@Override
	public double getMinWidth() {
		
		return 290.0;
	}

	@Override
	public double getMinWeight() {
		
		return 1168.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
