package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_CRC implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{3.82416964, -0.331723541e-01, -4.37821001e-01, -1.00538135e-01
            				, 8.55516434e-01, 6.97509229e-01, -3.67988676e-01, -6.27763569e-02
            				, -1.44062960e+00, -1.11408007e+00, -7.96503127e-01, 3.13842118e-01
            				, 1.53427684e+00, -4.66702193e-01, -2.24782491e+00, 1.84045887e+00
            				, -9.63502169e-01, 2.63244081e+00, -4.85349670e-02, -1.24900687e+00}, 
            			{1.23621118e+00, -3.35768461e-01, 1.09179493e-03, -5.13459325e-01
            				, 6.92295849e-01, -8.24182093e-01, -8.41260910e-01, -1.69444990e+00
            				, 7.29598105e-01, -3.68114799e-01, 3.45256537e-01, -1.45284414e-01
            				, 6.17434025e-01, -1.33578551e+00, 2.28812918e-01, 1.87902200e+00
            				, 1.17881894e+00, 1.48543656e+00, -1.34752747e-02, 3.27554554e-01},
            			{1.60929278e-01, -1.18397403e+00, 8.02531183e-01, -7.80737340e-01
            				, 1.20489269e-01, 7.33317494e-01, -6.35620773e-01, -1.40856540e+00
            				, -4.51418549e-01, 7.84986198e-01, 1.69994220e-01, -4.93965596e-01
            				, 1.59210312e+00, -1.06167638e+00, -1.51372105e-01, 9.18662071e-01
            				, -1.31700611e+00, 8.97783399e-01, -7.85472631e-01, -5.22366524e-01}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {1.51875818, -0.09507027, -1.87905669, 0.51822263, -1.0416925, 0.77202076
            			, 0.70347565, 0.44788677, -0.10286196, 0.20757549, 1.18613708, -0.33918539
            			, 2.40238309, -0.08648832, -0.91265815, 1.44030762, 0.30864802, 1.79238665
            			, 1.23718655, -1.26252639};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-1.30814004,
            			-0.37509602,
            			1.13413835,
            			0.13717881,
            			-0.05983163,
            			-3.55580783,
            			0.32024699,
            			-0.24510144,
            			-0.24891159,
            			0.75935549,
            			-1.44321179,
            			-0.22555485,
            			-1.19941139,
            			0.59132987,
            			0.06688222,
            			-0.99762714,
            			0.0878227,
            			-1.40183198,
            			0.58034629,
            			-0.16217192};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-1.89288032};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 3.64;
	}

	@Override
	public double getMaxWidth() {
		
		return 588.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 8172.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.21;
	}

	@Override
	public double getMinWidth() {
		
		return 274.0;
	}

	@Override
	public double getMinWeight() {
		
		return 937.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
