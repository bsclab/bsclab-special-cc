package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

@Component
public class JobConfigurationFactory {
	
	
	public DBJobConfiguration createDBJobConfiguration(
			String tmpTableName, String csvFilePath) {
		return new DBJobConfiguration(tmpTableName, csvFilePath);
	}
	
	public SqoopJobConfiguration createSqoopJobConfiguration(
			Timestamp sdt, Timestamp edt
			, String serverIp, String resourceTable
			, String outputDir, String jdbcDriver, String connetionString
			, String dbUser, String dbPw) {
		return new SqoopJobConfiguration(sdt, edt, serverIp, resourceTable
				, outputDir, jdbcDriver, connetionString
				, dbUser, dbPw);
	}
	
	public SparkJobConfiguration createSparkJobConfiguration(
			String jobId, String jobClass, String jobPath
			, String userId) {
		return new SparkJobConfiguration(jobId, jobClass, jobPath
				, userId);
	}
	
	
	
	
	// is it possible?
//	public <T extends JobConfiguration> T createJobConfiguration(
//			String tmpTableName
//			, String csvFilePath
//			, Class<T> jobConfigurationClass) {
//		
//		
//		
//		
//	}

}
