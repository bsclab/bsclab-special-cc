package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_CR2 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-7.13202477, 90.27735596, -9.95854092, -10.00339003, -2.27939343, 0.867090821, 0.118622854, -1.01422870, -100.22685524, -1.68030083, -10.31828156, 2.21087050, -10.22064066, 0.561024666, -10.24796915, -1.97905338, -0.495713860, 20.85996799, -0.0227773171, 0.539714515}, 
            			{0.825979888, -0.0687741116, -0.0288301688, 0.231718823, -1.32866859, 1.24832547, 0.213765368, 0.302718341, 4.41853189, -0.0253073387, -1.42408526, -0.466472387, 2.70593953, 0.521482766, -0.837458849, -1.76167524, -1.57632351, 1.37292159, -1.59773850, -0.980847895},
            			{0.0345504321, 0.481634885, -0.0509188548, 0.973526657, 0.616555393, -0.392677665, -0.502650619, 0.622453451, -1.35250783, -0.267562985, -0.745960236, -0.246883795, 1.65239656, -0.474745274, 0.147006184, 0.688859940, -0.800658286, -1.11998856, -0.534862339, 0.0240281206}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {0.52705431, 0.52018207, 0.80820721, 0.41876608, -0.73143774, -0.75059235, -0.77115595, -1.37199354, 1.11330938, -0.75127792, 1.56301761, -1.28550386, 0.58469898, -0.14942105, 0.36917481, -0.58430433, -0.62246335, 0.65105641, -0.30966234, -1.11020732};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {0.45478675, -1.43740165, 0.72348744, 0.96222299, -0.75526851, -0.6024037, -1.15678298, -0.23350142, 1.42903161, 0.01927817, 1.30480826, 0.08297952, 1.35609949, 0.33379871, 1.25094056, 0.17046453, 0.60984433, -1.28012741, -2.81184983, -1.09383929};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-1.83680689};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 7.25;
	}

	@Override
	public double getMaxWidth() {
		
		return 1298.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 25400.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.36;
	}

	@Override
	public double getMinWidth() {
		
		return 700.0;
	}

	@Override
	public double getMinWeight() {
		
		return 3516.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
