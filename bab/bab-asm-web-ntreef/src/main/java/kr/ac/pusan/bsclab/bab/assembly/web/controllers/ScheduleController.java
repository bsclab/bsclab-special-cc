package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;

@Controller
public class ScheduleController extends AbstractWebController {

	public static final String BASE_URL = BabWeb.BASE_URL + "/schedule";
	
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL
			+ "/schedule/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getSchedule(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			HttpServletRequest request, HttpSession session) {
		
		ModelAndView view = new ModelAndView("schedule/schedule");
		
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
		jsonData.put("datasetId", datasetId);
		view.addObject("apiURI", apiManager.getAPIURI());
		view.addObject("jsonData", jsonData);
		
		return view;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = BASE_URL
			+ "/simulate/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public ModelAndView getSimulate(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt,
			HttpServletRequest request, HttpSession session) {
		ModelAndView view = new ModelAndView("schedule/simulate");
		
		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("sdt", sdt);
		jsonData.put("edt", edt);
		jsonData.put("datasetId", datasetId);
		view.addObject("apiURI", apiManager.getAPIURI());
		view.addObject("jsonData", jsonData);
		
		return view;
	}
}
