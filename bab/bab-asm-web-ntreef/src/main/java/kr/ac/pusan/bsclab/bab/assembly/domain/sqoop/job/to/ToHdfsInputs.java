package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job.to;

import java.util.ArrayList;
import java.util.List;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.InputsFactory;

public class ToHdfsInputs implements InputsFactory {

	/**
	 * args[0] : overrideNullValue
	 * args[1] : nullValue
	 * args[2] : outputFormat
	 * args[3] : compression
	 * args[4] : customCompression
	 * args[5] : outputDirectory
	 */
	@Override
	public Input[] createInputs(Object... args) {
		List<Input> inputs = new ArrayList<Input>();
		inputs.add(new Input(0, "ANY", "toJobConfig.overrideNullValue", 27, false, "", "BOOLEAN", args[0], null));
		inputs.add(new Input(255, "ANY", "toJobConfig.nullValue", 28, false, "", "STRING", args[1], null));
		inputs.add(new Input(0, "ANY", "toJobConfig.outputFormat", 29, false, "", "ENUM", args[2], ToHdfsOutputFormat.getAll()));
		inputs.add(new Input(0, "ANY", "toJobConfig.compression", 30, false, "", "ENUM", args[3], ToHdfsCompression.getAll()));
		inputs.add(new Input(255, "ANY", "toJobConfig.customCompression", 31, false, "", "STRING", args[4], null));
		inputs.add(new Input(255, "ANY", "toJobConfig.outputDirectory", 32, false, "", "STRING", args[5], null));
		return inputs.toArray(new Input[0]);
	}
}
