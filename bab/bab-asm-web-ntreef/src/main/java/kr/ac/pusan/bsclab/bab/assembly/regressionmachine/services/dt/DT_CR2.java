package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_CR2 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("AN10244"))
 if(we <= 24750) return 22.9;
 if(we > 24750) return 30;
if(p.equals("CR21056"))
 if(we <= 24560) return 23.7833333333333;
 if(we > 24560) return 31.95;
if(p.equals("CR31001"))
 if(we <= 20830) return 17.9833333333333;
 if(we > 20830)
  if(we <= 21950) return 15.7;
  if(we > 21950) return 16.2;
if(p.equals("PP11260"))
 if(w <= 1132)
  if(t <= 2.57) return 30;
  if(t > 2.57)
   if(we <= 23540)
    if(we <= 23410) return 25;
    if(we > 23410) return 24.6;
   if(we > 23540)
    if(t <= 2.965) return 30;
    if(t > 2.965) return 20;
 if(w > 1132)
  if(w <= 1189)
   if(t <= 2.45) return 40;
   if(t > 2.45)
    if(t <= 3.28) return 30;
    if(t > 3.28) return 35;
  if(w > 1189)
   if(we <= 24490) return 30;
   if(we > 24490) return 50;
if(p.equals("PP11261"))
 if(t <= 3.5)
  if(t <= 1.35)
   if(we <= 21100) return 60;
   if(we > 21100) return 25.6333333333333;
  if(t > 1.35)
   if(we <= 6605) return 20;
   if(we > 6605) return 30;
 if(t > 3.5)
  if(we <= 21000)
   if(t <= 4.03) return 30;
   if(t > 4.03) return 32.8;
  if(we > 21000) return 20;
if(p.equals("PP11264"))
 if(t <= 2.34)
  if(w <= 1184)
   if(t <= 2.12)
    if(t <= 1.92)
     if(we <= 20210)
      if(we <= 15461) return 40;
      if(we > 15461)
       if(we <= 19700) return 30;
       if(we > 19700) return 28.4;
     if(we > 20210) return 30;
    if(t > 1.92) return 40;
   if(t > 2.12)
    if(w <= 1108) return 50;
    if(w > 1108)
     if(we <= 20430) return 25;
     if(we > 20430)
      if(we <= 21030) return 30;
      if(we > 21030) return 18.7333333333333;
  if(w > 1184)
   if(t <= 1.61)
    if(t <= 1.11)
     if(w <= 1236) return 34.5666666666667;
     if(w > 1236) return 32.1;
    if(t > 1.11)
     if(t <= 1.25) return 50;
     if(t > 1.25)
      if(w <= 1223) return 30;
      if(w > 1223) return 50;
   if(t > 1.61)
    if(t <= 1.83)
     if(we <= 12770)
      if(we <= 12359)
       if(we <= 12280) return 26.0166666666667;
       if(we > 12280) return 23.1833333333333;
      if(we > 12359)
       if(we <= 12590) return 42.3;
       if(we > 12590) return 20;
     if(we > 12770)
      if(w <= 1190)
       if(we <= 12920) return 30;
       if(we > 12920) return 19.7833333333333;
      if(w > 1190)
       if(w <= 1223) return 21.75;
       if(w > 1223) return 31.4333333333333;
    if(t > 1.83)
     if(t <= 2.12)
      if(we <= 16770) return 20;
      if(we > 16770) return 40;
     if(t > 2.12) return 30;
 if(t > 2.34)
  if(t <= 2.63)
   if(we <= 24030)
    if(w <= 1208) return 30;
    if(w > 1208) return 35;
   if(we > 24030)
    if(we <= 24790) return 20;
    if(we > 24790)
     if(we <= 24900) return 15;
     if(we > 24900) return 35;
  if(t > 2.63)
   if(w <= 1200)
    if(w <= 1074)
     if(we <= 19410)
      if(we <= 19130) return 40;
      if(we > 19130) return 50;
     if(we > 19410) return 40;
    if(w > 1074) return 30;
   if(w > 1200)
    if(we <= 23960)
     if(t <= 3.1)
      if(we <= 22700) return 16.95;
      if(we > 22700) return 30;
     if(t > 3.1)
      if(w <= 1219) return 50;
      if(w > 1219) return 30.8833333333333;
    if(we > 23960)
     if(we <= 24090) return 40;
     if(we > 24090)
      if(we <= 24980)
       if(we <= 24200) return 35;
       if(we > 24200) return 30;
      if(we > 24980) return 50;
if(p.equals("PP11266"))
 if(we <= 19920)
  if(w <= 1066.5)
   if(t <= 1.25)
    if(t <= 0.91)
     if(we <= 18340)
      if(we <= 17710) return 40;
      if(we > 17710)
       if(we <= 17910) return 60;
       if(we > 17910)
        if(we <= 18040) return 50;
        if(we > 18040) return 90;
     if(we > 18340) return 40;
    if(t > 0.91)
     if(we <= 18920) return 50;
     if(we > 18920) return 50.8666666666667;
   if(t > 1.25)
    if(t <= 1.5)
     if(w <= 988)
      if(we <= 18890)
       if(we <= 18690) return 42.2;
       if(we > 18690) return 50;
      if(we > 18890)
       if(we <= 19770) return 60;
       if(we > 19770) return 55.2166666666667;
     if(w > 988) return 50;
    if(t > 1.5)
     if(w <= 1020) return 55;
     if(w > 1020) return 75;
  if(w > 1066.5)
   if(w <= 1126)
    if(t <= 2.3)
     if(we <= 10104) return 25;
     if(we > 10104)
      if(t <= 1.4)
       if(we <= 15800) return 70;
       if(we > 15800) return 50;
      if(t > 1.4) return 50;
    if(t > 2.3)
     if(w <= 1078) return 20;
     if(w > 1078) return 25;
   if(w > 1126)
    if(t <= 1.4)
     if(we <= 16190) return 50;
     if(we > 16190) return 60;
    if(t > 1.4) return 40;
 if(we > 19920)
  if(t <= 1.07)
   if(w <= 1130)
    if(w <= 1078)
     if(we <= 21010)
      if(we <= 20070) return 55;
      if(we > 20070) return 65;
     if(we > 21010)
      if(we <= 21200) return 30;
      if(we > 21200)
       if(we <= 21220) return 85;
       if(we > 21220) return 45;
    if(w > 1078)
     if(we <= 22390)
      if(we <= 22210) return 62.4666666666667;
      if(we > 22210) return 60;
     if(we > 22390)
      if(we <= 22500) return 49.8333333333333;
      if(we > 22500) return 50;
   if(w > 1130)
    if(we <= 22450)
     if(we <= 21420) return 50;
     if(we > 21420) return 53.9333333333333;
    if(we > 22450)
     if(we <= 22530) return 61.5333333333333;
     if(we > 22530) return 66.4166666666667;
  if(t > 1.07)
   if(t <= 2)
    if(we <= 20690)
     if(we <= 20420)
      if(t <= 1.5)
       if(we <= 19990) return 60;
       if(we > 19990) return 35;
      if(t > 1.5)
       if(w <= 1042)
        if(we <= 20320) return 55;
        if(we > 20320) return 50;
       if(w > 1042)
        if(we <= 20300) return 60;
        if(we > 20300) return 35;
     if(we > 20420)
      if(we <= 20650) return 50;
      if(we > 20650) return 20;
    if(we > 20690)
     if(w <= 1078)
      if(w <= 1042)
       if(t <= 1.5)
        if(we <= 21110) return 30;
        if(we > 21110) return 50;
       if(t > 1.5) return 55;
      if(w > 1042)
       if(we <= 22000)
        if(we <= 21650) return 64.5833333333333;
        if(we > 21650) return 45.3;
       if(we > 22000)
        if(we <= 22060) return 56.6666666666667;
        if(we > 22060) return 30;
     if(w > 1078)
      if(w <= 1112) return 30;
      if(w > 1112)
       if(we <= 20820) return 30;
       if(we > 20820) return 50;
   if(t > 2)
    if(w <= 1078)
     if(we <= 20840) return 20;
     if(we > 20840) return 21.4333333333333;
    if(w > 1078)
     if(w <= 1118) return 45;
     if(w > 1118) return 22.7166666666667;
if(p.equals("PP11275"))
 if(t <= 1.45) return 30;
 if(t > 1.45) return 25;
if(p.equals("PP11287"))
 if(t <= 1.215)
  if(we <= 18920) return 50;
  if(we > 18920) return 45;
 if(t > 1.215) return 30;
if(p.equals("PP11288")) return 30;
if(p.equals("PP11312"))
 if(we <= 20080)
  if(t <= 1.65)
   if(t <= 1.07) return 39.7333333333333;
   if(t > 1.07) return 30;
  if(t > 1.65)
   if(t <= 1.995) return 25;
   if(t > 1.995) return 50;
 if(we > 20080)
  if(we <= 20990)
   if(t <= 1.5) return 45;
   if(t > 1.5) return 50;
  if(we > 20990)
   if(w <= 1087) return 30;
   if(w > 1087) return 45;
if(p.equals("PP11325")) return 30;
if(p.equals("PP11403"))
 if(t <= 1.15) return 50;
 if(t > 1.15)
  if(we <= 16800) return 30;
  if(we > 16800) return 35;
if(p.equals("PP11435"))
 if(we <= 22130) return 17.3166666666667;
 if(we > 22130) return 30;
if(p.equals("PP11443")) return 24.3833333333333;
if(p.equals("PP11539")) return 20;
if(p.equals("PP11607"))
 if(we <= 24980) return 27.6833333333333;
 if(we > 24980) return 40;
if(p.equals("PP11683"))
 if(t <= 0.41)
  if(we <= 16230)
   if(we <= 16040) return 60;
   if(we > 16040) return 63.8333333333333;
  if(we > 16230) return 85;
 if(t > 0.41)
  if(we <= 17760)
   if(we <= 16130) return 60;
   if(we > 16130) return 85;
  if(we > 17760)
   if(we <= 19980) return 70;
   if(we > 19980) return 80;
if(p.equals("PP11894"))
 if(we <= 20480) return 60;
 if(we > 20480) return 50;
if(p.equals("PP11910")) return 20;
if(p.equals("PP11912"))
 if(we <= 20900)
  if(w <= 1177)
   if(t <= 2.72) return 30;
   if(t > 2.72) return 20;
  if(w > 1177) return 40;
 if(we > 20900)
  if(we <= 21510)
   if(we <= 21170) return 25;
   if(we > 21170)
    if(t <= 2.815) return 30.35;
    if(t > 2.815) return 33.1666666666667;
  if(we > 21510)
   if(we <= 21660) return 14.5833333333333;
   if(we > 21660)
    if(we <= 21670) return 25;
    if(we > 21670) return 20;
if(p.equals("PP11917")) return 25;
if(p.equals("PP11931"))
 if(w <= 1126)
  if(t <= 1.65)
   if(t <= 1.38)
    if(we <= 16900)
     if(we <= 16560) return 23.85;
     if(we > 16560) return 40;
    if(we > 16900)
     if(t <= 1.23) return 50;
     if(t > 1.23) return 35;
   if(t > 1.38)
    if(we <= 15790)
     if(we <= 14720) return 30;
     if(we > 14720)
      if(we <= 14830) return 19.7;
      if(we > 14830) return 21.5666666666667;
    if(we > 15790)
     if(we <= 16330) return 35;
     if(we > 16330)
      if(we <= 23715) return 30;
      if(we > 23715) return 35;
  if(t > 1.65)
   if(w <= 1087)
    if(t <= 2.1) return 30;
    if(t > 2.1)
     if(t <= 2.35)
      if(we <= 16820) return 35.4666666666667;
      if(we > 16820) return 25;
     if(t > 2.35)
      if(we <= 16900) return 7.2;
      if(we > 16900) return 23.2;
   if(w > 1087)
    if(we <= 14860)
     if(we <= 14720) return 30;
     if(we > 14720) return 20;
    if(we > 14860)
     if(we <= 17110)
      if(we <= 16960)
       if(we <= 16330)
        if(we <= 15790) return 25;
        if(we > 15790) return 30;
       if(we > 16330)
        if(we <= 16750)
         if(we <= 16460) return 20;
         if(we > 16460) return 40;
        if(we > 16750) return 20;
      if(we > 16960)
       if(we <= 17050)
        if(we <= 17000) return 27.5;
        if(we > 17000) return 30;
       if(we > 17050)
        if(we <= 17070) return 40;
        if(we > 17070) return 20;
     if(we > 17110)
      if(we <= 22950)
       if(we <= 17140) return 20;
       if(we > 17140) return 30;
      if(we > 22950) return 50;
 if(w > 1126)
  if(t <= 1.75)
   if(we <= 17610)
    if(we <= 17400)
     if(we <= 16179) return 30;
     if(we > 16179) return 35;
    if(we > 17400)
     if(we <= 17570)
      if(we <= 17480) return 42.2833333333333;
      if(we > 17480) return 25;
     if(we > 17570) return 22.75;
   if(we > 17610)
    if(we <= 18120)
     if(we <= 17810)
      if(we <= 17680) return 23.5666666666667;
      if(we > 17680) return 24.6;
     if(we > 17810)
      if(we <= 17950) return 30;
      if(we > 17950) return 40;
    if(we > 18120)
     if(we <= 18160) return 35;
     if(we > 18160)
      if(we <= 21090) return 30;
      if(we > 21090) return 43.9;
  if(t > 1.75)
   if(we <= 17650)
    if(we <= 16320)
     if(we <= 14900) return 17.5666666666667;
     if(we > 14900)
      if(we <= 15100) return 40;
      if(we > 15100) return 22.6166666666667;
    if(we > 16320)
     if(we <= 17600) return 30;
     if(we > 17600) return 23.1833333333333;
   if(we > 17650)
    if(we <= 18080)
     if(we <= 17810)
      if(we <= 17710) return 23;
      if(we > 17710) return 29.0166666666667;
     if(we > 17810)
      if(we <= 17870) return 20.7166666666667;
      if(we > 17870) return 23.8;
    if(we > 18080)
     if(we <= 18140) return 45;
     if(we > 18140) return 30;
if(p.equals("PP11932"))
 if(t <= 1.195)
  if(t <= 1.05)
   if(we <= 16950)
    if(we <= 15200) return 65;
    if(we > 15200) return 60;
   if(we > 16950)
    if(t <= 0.85) return 50;
    if(t > 0.85) return 10;
  if(t > 1.05) return 30;
 if(t > 1.195)
  if(t <= 1.62)
   if(we <= 15170)
    if(t <= 1.35) return 25;
    if(t > 1.35) return 40;
   if(we > 15170)
    if(we <= 16980)
     if(t <= 1.35)
      if(we <= 16860)
       if(we <= 15530) return 30;
       if(we > 15530) return 45;
      if(we > 16860)
       if(we <= 16920) return 35;
       if(we > 16920) return 24.7166666666667;
     if(t > 1.35) return 25;
    if(we > 16980)
     if(t <= 1.35) return 40;
     if(t > 1.35) return 30;
  if(t > 1.62) return 30;
if(p.equals("PP11954"))
 if(we <= 20820) return 70;
 if(we > 20820)
  if(t <= 0.79) return 65;
  if(t > 0.79) return 50;
if(p.equals("PP11982"))
 if(t <= 1.55) return 40;
 if(t > 1.55)
  if(t <= 2.14) return 30;
  if(t > 2.14)
   if(we <= 12620)
    if(we <= 11560) return 15;
    if(we > 11560)
     if(we <= 11730) return 30;
     if(we > 11730) return 20;
   if(we > 12620)
    if(we <= 12920) return 25;
    if(we > 12920)
     if(we <= 13110)
      if(we <= 12950) return 30;
      if(we > 12950) return 20;
     if(we > 13110)
      if(we <= 13180) return 30;
      if(we > 13180) return 25;
if(p.equals("PP11991")) return 40;
if(p.equals("PP12002"))
 if(t <= 1.25)
  if(we <= 16720) return 40;
  if(we > 16720) return 34.45;
 if(t > 1.25) return 30;
if(p.equals("PP12047"))
 if(w <= 1114)
  if(t <= 1.1)
   if(we <= 17070)
    if(we <= 16280) return 29.4666666666667;
    if(we > 16280) return 35;
   if(we > 17070) return 40;
  if(t > 1.1) return 30;
 if(w > 1114)
  if(t <= 0.79)
   if(we <= 17610) return 20;
   if(we > 17610) return 25;
  if(t > 0.79)
   if(t <= 0.81) return 60;
   if(t > 0.81) return 45;
if(p.equals("PP12167")) return 80;
if(p.equals("PP12218")) return 80;
if(p.equals("PP12331")) return 30;
if(p.equals("PP12401"))
 if(t <= 1.99)
  if(we <= 15600) return 15;
  if(we > 15600) return 20;
 if(t > 1.99) return 30;
if(p.equals("PP12403"))
 if(t <= 0.985) return 25;
 if(t > 0.985)
  if(t <= 1.07) return 35;
  if(t > 1.07) return 30;
if(p.equals("PP12452"))
 if(t <= 2.49)
  if(we <= 22440)
   if(t <= 2.085) return 20;
   if(t > 2.085) return 30;
  if(we > 22440) return 20;
 if(t > 2.49)
  if(t <= 3.24)
   if(t <= 2.52)
    if(we <= 23940) return 21.85;
    if(we > 23940)
     if(we <= 24200) return 35;
     if(we > 24200) return 30;
   if(t > 2.52) return 30;
  if(t > 3.24)
   if(we <= 22110)
    if(t <= 3.4) return 25;
    if(t > 3.4)
     if(t <= 3.63) return 30;
     if(t > 3.63) return 35;
   if(we > 22110)
    if(we <= 23770)
     if(t <= 3.6) return 40;
     if(t > 3.6) return 45;
    if(we > 23770) return 45;
if(p.equals("PP12539")) return 13.0333333333333;
if(p.equals("PP12541")) return 75;
if(p.equals("PP12598")) return 30;
if(p.equals("PP21089"))
 if(t <= 1.92)
  if(we <= 21240)
   if(we <= 20650) return 50;
   if(we > 20650)
    if(t <= 1.49)
     if(we <= 20980) return 40;
     if(we > 20980) return 55;
    if(t > 1.49) return 22.5333333333333;
  if(we > 21240)
   if(t <= 1.54)
    if(t <= 1.45) return 80;
    if(t > 1.45)
     if(w <= 1170) return 35.3666666666667;
     if(w > 1170)
      if(we <= 24140) return 60;
      if(we > 24140) return 53.35;
   if(t > 1.54)
    if(t <= 1.65) return 20.95;
    if(t > 1.65) return 30;
 if(t > 1.92)
  if(we <= 20910) return 30;
  if(we > 20910)
   if(we <= 22660)
    if(w <= 1014) return 30.4666666666667;
    if(w > 1014) return 40;
   if(we > 22660)
    if(t <= 2.05) return 29.2166666666667;
    if(t > 2.05)
     if(t <= 2.2) return 35;
     if(t > 2.2)
      if(w <= 1190) return 50;
      if(w > 1190) return 26.6166666666667;
if(p.equals("PP21090")) return 40.25;
if(p.equals("PP21092"))
 if(t <= 2.095)
  if(t <= 1.44)
   if(w <= 1036)
    if(w <= 1022)
     if(we <= 20640)
      if(we <= 19720)
       if(we <= 18290) return 50;
       if(we > 18290)
        if(we <= 19610) return 45;
        if(we > 19610) return 35;
      if(we > 19720)
       if(we <= 20550) return 50;
       if(we > 20550) return 45;
     if(we > 20640)
      if(we <= 20920)
       if(we <= 20770) return 48.0333333333333;
       if(we > 20770) return 45.9333333333333;
      if(we > 20920)
       if(we <= 21030) return 30;
       if(we > 21030) return 40.05;
    if(w > 1022)
     if(we <= 20680)
      if(we <= 18640) return 50;
      if(we > 18640) return 40;
     if(we > 20680)
      if(we <= 20740)
       if(we <= 20710) return 60;
       if(we > 20710) return 65.8666666666667;
      if(we > 20740) return 50;
   if(w > 1036)
    if(w <= 1152)
     if(w <= 1076) return 45;
     if(w > 1076)
      if(we <= 20800)
       if(we <= 19600)
        if(we <= 19560) return 42.7333333333333;
        if(we > 19560) return 80;
       if(we > 19600)
        if(we <= 19880) return 50;
        if(we > 19880) return 65.6166666666667;
      if(we > 20800)
       if(we <= 22950)
        if(we <= 21490)
         if(t <= 1.07) return 55.9833333333333;
         if(t > 1.07) return 30;
        if(we > 21490) return 45;
       if(we > 22950)
        if(we <= 23460)
         if(we <= 23040) return 40.8166666666667;
         if(we > 23040) return 30;
        if(we > 23460)
         if(we <= 23600) return 60;
         if(we > 23600) return 50;
    if(w > 1152)
     if(we <= 24580) return 60;
     if(we > 24580)
      if(we <= 24820) return 44.15;
      if(we > 24820) return 40;
  if(t > 1.44)
   if(w <= 1087)
    if(t <= 1.695)
     if(we <= 15650)
      if(w <= 1070) return 35;
      if(w > 1070) return 30;
     if(we > 15650)
      if(w <= 1051)
       if(we <= 15930)
        if(we <= 15740)
         if(we <= 15670) return 25;
         if(we > 15670)
          if(we <= 15690) return 30;
          if(we > 15690) return 35;
        if(we > 15740)
         if(we <= 15790)
          if(we <= 15760) return 22.9166666666667;
          if(we > 15760) return 26.3666666666667;
         if(we > 15790)
          if(we <= 15820) return 28.5666666666667;
          if(we > 15820) return 25;
       if(we > 15930)
        if(we <= 16000) return 32.5;
        if(we > 16000)
         if(we <= 16050) return 25;
         if(we > 16050) return 30;
      if(w > 1051) return 30;
    if(t > 1.695)
     if(we <= 21410)
      if(we <= 20570)
       if(w <= 995) return 28.0166666666667;
       if(w > 995)
        if(w <= 1022) return 30;
        if(w > 1022)
         if(we <= 20440) return 35;
         if(we > 20440) return 30;
      if(we > 20570) return 30;
     if(we > 21410)
      if(w <= 1040) return 25;
      if(w > 1040) return 26.6333333333333;
   if(w > 1087)
    if(w <= 1195)
     if(w <= 1156)
      if(we <= 23230) return 30;
      if(we > 23230)
       if(we <= 24320) return 40;
       if(we > 24320) return 35;
     if(w > 1156)
      if(we <= 23650)
       if(t <= 1.735) return 40.3166666666667;
       if(t > 1.735) return 28.4333333333333;
      if(we > 23650) return 30;
    if(w > 1195)
     if(we <= 24460)
      if(we <= 23810)
       if(t <= 2.02)
        if(we <= 22330)
         if(we <= 21450) return 65;
         if(we > 21450) return 34.4;
        if(we > 22330)
         if(t <= 1.695) return 40;
         if(t > 1.695)
          if(t <= 1.795)
           if(w <= 1223) return 45;
           if(w > 1223) return 30;
          if(t > 1.795) return 30;
       if(t > 2.02) return 50;
      if(we > 23810)
       if(t <= 1.695)
        if(we <= 24210)
         if(we <= 23950) return 34.05;
         if(we > 23950) return 35;
        if(we > 24210)
         if(we <= 24300) return 45;
         if(we > 24300)
          if(we <= 24390) return 36.3666666666667;
          if(we > 24390) return 40;
       if(t > 1.695) return 50;
     if(we > 24460)
      if(we <= 24570)
       if(we <= 24500) return 35;
       if(we > 24500) return 40;
      if(we > 24570)
       if(t <= 1.84)
        if(we <= 24640)
         if(we <= 24600) return 45;
         if(we > 24600) return 30;
        if(we > 24640) return 35;
       if(t > 1.84)
        if(we <= 24880) return 30;
        if(we > 24880) return 70;
 if(t > 2.095)
  if(w <= 1057)
   if(t <= 2.34)
    if(we <= 20600)
     if(we <= 19990)
      if(we <= 18510)
       if(w <= 1048) return 25;
       if(w > 1048)
        if(we <= 16780)
         if(we <= 16660) return 30;
         if(we > 16660) return 20;
        if(we > 16780)
         if(we <= 16920) return 25;
         if(we > 16920) return 30;
      if(we > 18510)
       if(w <= 1011)
        if(we <= 19380) return 25;
        if(we > 19380) return 28.4833333333333;
       if(w > 1011)
        if(we <= 19430)
         if(we <= 18710) return 42.3166666666667;
         if(we > 18710)
          if(we <= 19110) return 45;
          if(we > 19110) return 26.5166666666667;
        if(we > 19430)
         if(we <= 19800)
          if(we <= 19650) return 24.8;
          if(we > 19650) return 22.95;
         if(we > 19800) return 25;
     if(we > 19990)
      if(we <= 20180)
       if(t <= 2.195)
        if(we <= 20080) return 21.25;
        if(we > 20080) return 20;
       if(t > 2.195) return 26;
      if(we > 20180)
       if(w <= 1048)
        if(we <= 20580) return 30;
        if(we > 20580) return 25;
       if(w > 1048) return 35;
    if(we > 20600) return 30;
   if(t > 2.34) return 30;
  if(w > 1057)
   if(t <= 2.495)
    if(t <= 2.15)
     if(t <= 2.1) return 30;
     if(t > 2.1)
      if(we <= 24600) return 33.7333333333333;
      if(we > 24600) return 20;
    if(t > 2.15)
     if(we <= 24430)
      if(t <= 2.34)
       if(we <= 22920)
        if(w <= 1087) return 20;
        if(w > 1087) return 30;
       if(we > 22920)
        if(we <= 23890)
         if(t <= 2.24) return 30;
         if(t > 2.24) return 35;
        if(we > 23890)
         if(t <= 2.24)
          if(we <= 24190) return 50;
          if(we > 24190) return 35;
         if(t > 2.24)
          if(we <= 24200) return 40;
          if(we > 24200) return 20;
      if(t > 2.34)
       if(t <= 2.44) return 30;
       if(t > 2.44)
        if(we <= 23320) return 35;
        if(we > 23320) return 30;
     if(we > 24430)
      if(t <= 2.385)
       if(w <= 1181) return 24.3833333333333;
       if(w > 1181) return 30;
      if(t > 2.385)
       if(we <= 24600) return 33;
       if(we > 24600) return 40;
   if(t > 2.495)
    if(t <= 3.4)
     if(w <= 1080) return 30;
     if(w > 1080)
      if(w <= 1090)
       if(we <= 22420)
        if(we <= 21420) return 30.2;
        if(we > 21420) return 30;
       if(we > 22420) return 40;
      if(w > 1090)
       if(w <= 1211)
        if(t <= 2.74) return 30;
        if(t > 2.74) return 20;
       if(w > 1211)
        if(w <= 1240) return 50;
        if(w > 1240)
         if(we <= 24680)
          if(we <= 24160)
           if(we <= 19590) return 25;
           if(we > 19590) return 30;
          if(we > 24160) return 50;
         if(we > 24680) return 40;
    if(t > 3.4)
     if(t <= 3.87)
      if(t <= 3.54)
       if(we <= 23890) return 30;
       if(we > 23890)
        if(we <= 24550) return 20;
        if(we > 24550) return 25;
      if(t > 3.54)
       if(w <= 1169)
        if(we <= 24590)
         if(we <= 24450)
          if(we <= 23750) return 20;
          if(we > 23750) return 30;
         if(we > 24450) return 25;
        if(we > 24590)
         if(we <= 24680) return 25.4666666666667;
         if(we > 24680) return 35;
       if(w > 1169) return 25;
     if(t > 3.87)
      if(t <= 4.015)
       if(w <= 1090) return 28.7;
       if(w > 1090)
        if(w <= 1127) return 20;
        if(w > 1127) return 30;
      if(t > 4.015)
       if(t <= 4.04) return 25;
       if(t > 4.04) return 15;
if(p.equals("PP21105"))
 if(we <= 16840)
  if(t <= 1.05)
   if(w <= 1131) return 29.6333333333333;
   if(w > 1131) return 25;
  if(t > 1.05) return 30;
 if(we > 16840)
  if(t <= 2.15)
   if(t <= 0.3875) return 80;
   if(t > 0.3875)
    if(t <= 2.145) return 40;
    if(t > 2.145) return 30;
  if(t > 2.15) return 30.1833333333333;
if(p.equals("PP21107"))
 if(w <= 1113)
  if(w <= 1109)
   if(t <= 3.1)
    if(w <= 1032) return 30;
    if(w > 1032)
     if(we <= 22900)
      if(w <= 1084)
       if(w <= 1058)
        if(we <= 19730) return 40;
        if(we > 19730) return 45;
       if(w > 1058) return 35;
      if(w > 1084)
       if(w <= 1095)
        if(we <= 22810) return 23.8;
        if(we > 22810) return 40;
       if(w > 1095) return 40;
     if(we > 22900)
      if(we <= 23140) return 30;
      if(we > 23140) return 35;
   if(t > 3.1)
    if(w <= 1057)
     if(we <= 19960) return 33.0833333333333;
     if(we > 19960) return 30;
    if(w > 1057)
     if(t <= 3.9) return 24.9833333333333;
     if(t > 3.9) return 20;
  if(w > 1109)
   if(we <= 22840)
    if(t <= 2.5)
     if(we <= 21190) return 24.9833333333333;
     if(we > 21190) return 29.6666666666667;
    if(t > 2.5)
     if(we <= 22550)
      if(w <= 1111) return 20;
      if(w > 1111)
       if(we <= 20300) return 20.2333333333333;
       if(we > 20300) return 30;
     if(we > 22550)
      if(we <= 22810) return 24.0166666666667;
      if(we > 22810) return 20;
   if(we > 22840)
    if(we <= 22930)
     if(we <= 22910) return 40;
     if(we > 22910) return 27.6666666666667;
    if(we > 22930)
     if(we <= 23000)
      if(we <= 22970) return 17.6;
      if(we > 22970) return 25;
     if(we > 23000)
      if(we <= 23030) return 26.6666666666667;
      if(we > 23030) return 19.3833333333333;
 if(w > 1113) return 30;
if(p.equals("PP21108"))
 if(t <= 0.91)
  if(we <= 20370)
   if(we <= 17910) return 40;
   if(we > 17910)
    if(w <= 1208) return 50;
    if(w > 1208) return 70;
  if(we > 20370) return 60;
 if(t > 0.91)
  if(we <= 20040)
   if(w <= 1156) return 30;
   if(w > 1156)
    if(w <= 1208)
     if(t <= 1.23) return 50;
     if(t > 1.23) return 25.5166666666667;
    if(w > 1208) return 24.9;
  if(we > 20040)
   if(w <= 1174)
    if(t <= 1.65) return 40;
    if(t > 1.65) return 30;
   if(w > 1174) return 30;
if(p.equals("PP21116"))
 if(t <= 1.34)
  if(we <= 14710) return 30;
  if(we > 14710) return 40;
 if(t > 1.34)
  if(we <= 14790) return 25;
  if(we > 14790) return 30;
if(p.equals("PP21173")) return 35;
if(p.equals("PP21180"))
 if(w <= 1021)
  if(w <= 1006)
   if(we <= 20250)
    if(we <= 19780) return 30;
    if(we > 19780) return 25;
   if(we > 20250) return 30;
  if(w > 1006)
   if(we <= 20300)
    if(we <= 19150)
     if(we <= 18890) return 50;
     if(we > 18890) return 18.5;
    if(we > 19150)
     if(we <= 20030) return 30;
     if(we > 20030) return 16.7666666666667;
   if(we > 20300)
    if(we <= 20490) return 35;
    if(we > 20490) return 30;
 if(w > 1021)
  if(we <= 20210)
   if(t <= 4.25) return 20;
   if(t > 4.25) return 16.7333333333333;
  if(we > 20210)
   if(we <= 21320)
    if(we <= 20630) return 30;
    if(we > 20630)
     if(we <= 20740)
      if(we <= 20670) return 20.6;
      if(we > 20670) return 19.6833333333333;
     if(we > 20740)
      if(we <= 20890) return 20.05;
      if(we > 20890) return 25;
   if(we > 21320)
    if(w <= 1050)
     if(we <= 21450) return 35.5166666666667;
     if(we > 21450) return 20;
    if(w > 1050)
     if(we <= 22330) return 32.4;
     if(we > 22330) return 30;
if(p.equals("PP21185")) return 35;
if(p.equals("PP21187"))
 if(w <= 1138)
  if(t <= 1.15) return 34.15;
  if(t > 1.15) return 30;
 if(w > 1138) return 50;
if(p.equals("PP21188"))
 if(t <= 2.4)
  if(we <= 25130) return 40;
  if(we > 25130)
   if(t <= 1.65) return 30.7666666666667;
   if(t > 1.65) return 30;
 if(t > 2.4)
  if(we <= 21750) return 20;
  if(we > 21750) return 13.6833333333333;
if(p.equals("PP21190")) return 35;
if(p.equals("PP21208"))
 if(t <= 1.82)
  if(t <= 1.79)
   if(w <= 1171)
    if(t <= 1.55)
     if(w <= 1022)
      if(we <= 20600)
       if(t <= 1.3) return 44.5;
       if(t > 1.3) return 30;
      if(we > 20600)
       if(t <= 1.45)
        if(w <= 1000) return 35;
        if(w > 1000) return 40;
       if(t > 1.45) return 56.5833333333333;
     if(w > 1022)
      if(t <= 1.45)
       if(we <= 17990)
        if(we <= 17880) return 45;
        if(we > 17880)
         if(we <= 17930) return 20;
         if(we > 17930) return 40;
       if(we > 17990)
        if(t <= 1.35)
         if(we <= 23740) return 50;
         if(we > 23740) return 25.9;
        if(t > 1.35)
         if(we <= 18100) return 30;
         if(we > 18100) return 60;
      if(t > 1.45) return 40;
    if(t > 1.55)
     if(w <= 1064)
      if(w <= 1014)
       if(w <= 990) return 25;
       if(w > 990) return 29.85;
      if(w > 1014)
       if(t <= 1.65)
        if(we <= 22010) return 35;
        if(we > 22010)
         if(we <= 22290) return 40;
         if(we > 22290) return 30;
       if(t > 1.65) return 40;
     if(w > 1064)
      if(we <= 21950)
       if(we <= 21400)
        if(we <= 20550) return 50;
        if(we > 20550) return 35;
       if(we > 21400)
        if(we <= 21520) return 36.0833333333333;
        if(we > 21520)
         if(we <= 21580) return 50;
         if(we > 21580) return 48.5;
      if(we > 21950)
       if(we <= 23820)
        if(we <= 22790) return 20;
        if(we > 22790)
         if(w <= 1132) return 30;
         if(w > 1132) return 40;
       if(we > 23820)
        if(we <= 24520)
         if(we <= 24110) return 50;
         if(we > 24110)
          if(w <= 1128) return 35;
          if(w > 1128) return 45;
        if(we > 24520)
         if(we <= 24590) return 28.3166666666667;
         if(we > 24590) return 25;
   if(w > 1171)
    if(w <= 1201)
     if(t <= 1.65)
      if(t <= 1.55)
       if(w <= 1187)
        if(w <= 1173) return 45;
        if(w > 1173)
         if(we <= 23850) return 20;
         if(we > 23850) return 30;
       if(w > 1187) return 25;
      if(t > 1.55)
       if(we <= 22760) return 25.4833333333333;
       if(we > 22760) return 25;
     if(t > 1.65)
      if(we <= 24050)
       if(t <= 1.77)
        if(we <= 21970) return 23;
        if(we > 21970) return 35.8666666666667;
       if(t > 1.77) return 30;
      if(we > 24050)
       if(we <= 24800) return 40;
       if(we > 24800)
        if(we <= 24920) return 30;
        if(we > 24920) return 34.1333333333333;
    if(w > 1201)
     if(w <= 1205)
      if(we <= 24830)
       if(we <= 20670)
        if(we <= 17790) return 12.25;
        if(we > 17790) return 24.1166666666667;
       if(we > 20670) return 35;
      if(we > 24830) return 70;
     if(w > 1205)
      if(w <= 1223)
       if(w <= 1208)
        if(we <= 24850)
         if(we <= 24720) return 25;
         if(we > 24720) return 50;
        if(we > 24850) return 30;
       if(w > 1208)
        if(t <= 0.91) return 75;
        if(t > 0.91)
         if(we <= 23180)
          if(we <= 22480)
           if(w <= 1213)
            if(we <= 22320) return 25;
            if(we > 22320) return 15.8;
           if(w > 1213) return 30;
          if(we > 22480) return 45;
         if(we > 23180)
          if(t <= 1.63) return 30;
          if(t > 1.63) return 40;
      if(w > 1223)
       if(we <= 24630)
        if(we <= 23840)
         if(t <= 0.4425) return 80;
         if(t > 0.4425) return 20;
        if(we > 23840) return 30.8666666666667;
       if(we > 24630)
        if(we <= 24770) return 27.4666666666667;
        if(we > 24770)
         if(we <= 24970) return 25;
         if(we > 24970) return 41.3333333333333;
  if(t > 1.79)
   if(we <= 20860)
    if(we <= 20570)
     if(we <= 20190)
      if(we <= 18790) return 25;
      if(we > 18790)
       if(w <= 1000)
        if(we <= 19970)
         if(we <= 19080) return 17.9666666666667;
         if(we > 19080) return 25;
        if(we > 19970)
         if(we <= 20010) return 15;
         if(we > 20010) return 20;
       if(w > 1000)
        if(we <= 19510)
         if(we <= 19340) return 20;
         if(we > 19340) return 15;
        if(we > 19510) return 22.6333333333333;
     if(we > 20190)
      if(we <= 20360)
       if(w <= 1000) return 20;
       if(w > 1000)
        if(we <= 20310) return 35;
        if(we > 20310) return 22.8833333333333;
      if(we > 20360)
       if(w <= 1000)
        if(we <= 20520) return 25;
        if(we > 20520) return 20;
       if(w > 1000)
        if(we <= 20380) return 25;
        if(we > 20380) return 20;
    if(we > 20570)
     if(w <= 1000)
      if(we <= 20660) return 30;
      if(we > 20660) return 22.9;
     if(w > 1000) return 25;
   if(we > 20860)
    if(we <= 23100)
     if(we <= 21000)
      if(we <= 20960) return 20;
      if(we > 20960) return 24.0333333333333;
     if(we > 21000)
      if(w <= 1150)
       if(we <= 21350)
        if(we <= 21060) return 25;
        if(we > 21060)
         if(we <= 21120) return 30;
         if(we > 21120) return 20;
       if(we > 21350)
        if(we <= 21680) return 20;
        if(we > 21680) return 25;
      if(w > 1150)
       if(w <= 1172) return 20;
       if(w > 1172) return 30;
    if(we > 23100)
     if(we <= 24440)
      if(we <= 23820)
       if(w <= 1172)
        if(w <= 1150) return 45;
        if(w > 1150) return 35;
       if(w > 1172)
        if(we <= 23510) return 25.25;
        if(we > 23510) return 40;
      if(we > 23820) return 40;
     if(we > 24440)
      if(we <= 24700) return 20;
      if(we > 24700) return 45;
 if(t > 1.82)
  if(t <= 2.295)
   if(w <= 1124)
    if(we <= 23270)
     if(we <= 21051)
      if(t <= 2.055) return 25;
      if(t > 2.055) return 30;
     if(we > 21051) return 30;
    if(we > 23270)
     if(we <= 23520)
      if(w <= 1119) return 40;
      if(w > 1119)
       if(we <= 23420) return 30;
       if(we > 23420) return 23.6333333333333;
     if(we > 23520)
      if(we <= 23580) return 50;
      if(we > 23580) return 40;
   if(w > 1124)
    if(we <= 24510)
     if(t <= 1.89)
      if(we <= 22460) return 50;
      if(we > 22460)
       if(w <= 1131)
        if(we <= 22650) return 40;
        if(we > 22650) return 30;
       if(w > 1131) return 30;
     if(t > 1.89)
      if(w <= 1173)
       if(t <= 2.055)
        if(t <= 1.95)
         if(w <= 1162) return 35;
         if(w > 1162) return 30;
        if(t > 1.95) return 25;
       if(t > 2.055) return 30;
      if(w > 1173)
       if(w <= 1223)
        if(t <= 1.92)
         if(we <= 23720) return 30;
         if(we > 23720) return 35;
        if(t > 1.92)
         if(we <= 21890) return 40;
         if(we > 21890) return 35;
       if(w > 1223)
        if(we <= 23510) return 14.1333333333333;
        if(we > 23510) return 30;
    if(we > 24510)
     if(we <= 24840)
      if(w <= 1194)
       if(t <= 2.055)
        if(t <= 1.89)
         if(we <= 24560) return 40;
         if(we > 24560) return 30;
        if(t > 1.89) return 28.3333333333333;
       if(t > 2.055)
        if(we <= 24630) return 60;
        if(we > 24630)
         if(we <= 24720) return 33.65;
         if(we > 24720) return 55;
      if(w > 1194) return 40;
     if(we > 24840)
      if(w <= 1220) return 30;
      if(w > 1220)
       if(t <= 2) return 35;
       if(t > 2)
        if(we <= 24980) return 32.6166666666667;
        if(we > 24980) return 28.2333333333333;
  if(t > 2.295)
   if(t <= 2.93)
    if(t <= 2.57)
     if(t <= 2.45) return 30;
     if(t > 2.45)
      if(w <= 1154) return 20;
      if(w > 1154) return 37.7;
    if(t > 2.57)
     if(t <= 2.7)
      if(w <= 1219)
       if(we <= 24800)
        if(t <= 2.65)
         if(w <= 1119)
          if(we <= 20690) return 20;
          if(we > 20690) return 40;
         if(w > 1119)
          if(we <= 24460) return 30;
          if(we > 24460) return 40;
        if(t > 2.65) return 30;
       if(we > 24800)
        if(we <= 24985) return 35;
        if(we > 24985) return 30;
      if(w > 1219)
       if(we <= 24490) return 41.4666666666667;
       if(we > 24490) return 30;
     if(t > 2.7)
      if(w <= 1074)
       if(we <= 20770)
        if(w <= 1061) return 35;
        if(w > 1061) return 17.7;
       if(we > 20770)
        if(t <= 2.83)
         if(we <= 22070)
          if(we <= 21570) return 21.3333333333333;
          if(we > 21570) return 19.2166666666667;
         if(we > 22070)
          if(we <= 24100) return 35;
          if(we > 24100) return 30;
        if(t > 2.83)
         if(t <= 2.91) return 30;
         if(t > 2.91) return 40;
      if(w > 1074)
       if(we <= 23630)
        if(we <= 22490) return 30;
        if(we > 22490)
         if(w <= 1205.5) return 25;
         if(w > 1205.5) return 30;
       if(we > 23630)
        if(we <= 24100)
         if(t <= 2.77)
          if(w <= 1173) return 20;
          if(w > 1173) return 30;
         if(t > 2.77) return 35;
        if(we > 24100) return 30;
   if(t > 2.93)
    if(we <= 23140)
     if(t <= 3.05)
      if(we <= 21990)
       if(w <= 1099)
        if(w <= 1068) return 30.4333333333333;
        if(w > 1068)
         if(we <= 20820) return 35;
         if(we > 20820)
          if(we <= 21520) return 30;
          if(we > 21520) return 20;
       if(w > 1099) return 25;
      if(we > 21990)
       if(w <= 1119)
        if(we <= 22250) return 25;
        if(we > 22250) return 24.75;
       if(w > 1119)
        if(we <= 22210)
         if(we <= 22090) return 23;
         if(we > 22090) return 13.9833333333333;
        if(we > 22210)
         if(we <= 22730) return 14.3833333333333;
         if(we > 22730) return 14.25;
     if(t > 3.05) return 30;
    if(we > 23140)
     if(we <= 23480)
      if(we <= 23310)
       if(we <= 23170) return 17.7166666666667;
       if(we > 23170) return 18.5833333333333;
      if(we > 23310)
       if(w <= 1119) return 15;
       if(w > 1119) return 12.6666666666667;
     if(we > 23480)
      if(we <= 23850)
       if(we <= 23610) return 30;
       if(we > 23610)
        if(t <= 3.1) return 16.5333333333333;
        if(t > 3.1) return 20;
      if(we > 23850)
       if(t <= 3.1) return 23.7;
       if(t > 3.1)
        if(w <= 1173) return 40;
        if(w > 1173)
         if(t <= 3.38) return 30;
         if(t > 3.38) return 40;
if(p.equals("PP21209")) return 30;
if(p.equals("PP21252")) return 25;
if(p.equals("PP21288")) return 32.1333333333333;
if(p.equals("PP21292"))
 if(we <= 17890)
  if(t <= 0.91) return 38;
  if(t > 0.91) return 25.7;
 if(we > 17890)
  if(we <= 18390) return 50;
  if(we > 18390) return 30;
if(p.equals("PP21306"))
 if(w <= 1162) return 29;
 if(w > 1162)
  if(we <= 24400) return 50;
  if(we > 24400) return 30;
if(p.equals("PP21312"))
 if(t <= 1.15) return 30;
 if(t > 1.15) return 50;
if(p.equals("PP21379"))
 if(w <= 980) return 35;
 if(w > 980) return 28;
if(p.equals("PP21391"))
 if(we <= 20060)
  if(t <= 0.9)
   if(t <= 0.71)
    if(we <= 15100)
     if(we <= 14760) return 60;
     if(we > 14760) return 45;
    if(we > 15100)
     if(t <= 0.3875) return 80;
     if(t > 0.3875) return 90;
   if(t > 0.71)
    if(we <= 14740)
     if(we <= 14300) return 20;
     if(we > 14300)
      if(we <= 14420) return 30;
      if(we > 14420) return 70;
    if(we > 14740)
     if(w <= 1034) return 73.5333333333333;
     if(w > 1034) return 65;
  if(t > 0.9)
   if(t <= 1.05)
    if(we <= 17960)
     if(we <= 17920) return 45;
     if(we > 17920) return 43.7666666666667;
    if(we > 17960)
     if(we <= 18160) return 50;
     if(we > 18160) return 40;
   if(t > 1.05)
    if(we <= 18600)
     if(we <= 18540)
      if(we <= 18090) return 50;
      if(we > 18090) return 60;
     if(we > 18540) return 40;
    if(we > 18600)
     if(we <= 18890)
      if(we <= 18710) return 30;
      if(we > 18710) return 55.9833333333333;
     if(we > 18890)
      if(we <= 19800) return 80;
      if(we > 19800) return 50;
 if(we > 20060)
  if(w <= 1090)
   if(w <= 1009)
    if(t <= 0.79)
     if(we <= 20763) return 65;
     if(we > 20763)
      if(we <= 21240) return 60;
      if(we > 21240) return 65;
    if(t > 0.79)
     if(t <= 0.91) return 40;
     if(t > 0.91)
      if(we <= 20160) return 55;
      if(we > 20160) return 60;
   if(w > 1009) return 60;
  if(w > 1090)
   if(w <= 1130)
    if(we <= 20910)
     if(we <= 20870) return 60;
     if(we > 20870) return 49.0833333333333;
    if(we > 20910)
     if(we <= 22110) return 65;
     if(we > 22110)
      if(we <= 22390) return 75;
      if(we > 22390) return 55;
   if(w > 1130)
    if(we <= 21770) return 62.4333333333333;
    if(we > 21770)
     if(we <= 23380)
      if(we <= 23000)
       if(we <= 21820) return 70;
       if(we > 21820) return 40;
      if(we > 23000)
       if(we <= 23270) return 60;
       if(we > 23270) return 70;
     if(we > 23380) return 65;
if(p.equals("PP21405"))
 if(t <= 5.03) return 30;
 if(t > 5.03)
  if(w <= 1105) return 34.8;
  if(w > 1105)
   if(we <= 21720) return 25.3666666666667;
   if(we > 21720) return 24.1166666666667;
if(p.equals("PP21414"))
 if(we <= 19350)
  if(we <= 19200) return 30;
  if(we > 19200) return 20.2;
 if(we > 19350)
  if(we <= 19580) return 35;
  if(we > 19580) return 30;
if(p.equals("PP21416"))
 if(we <= 23600) return 20;
 if(we > 23600) return 15;
if(p.equals("PP21419"))
 if(we <= 17640) return 25;
 if(we > 17640) return 30;
if(p.equals("PP21433"))
 if(we <= 21990) return 40;
 if(we > 21990)
  if(we <= 23610) return 30;
  if(we > 23610) return 50;
if(p.equals("PP21469")) return 35;
if(p.equals("PP21478")) return 20;
if(p.equals("PP21484"))
 if(t <= 1.84)
  if(t <= 1.23)
   if(we <= 11670)
    if(we <= 11400) return 40;
    if(we > 11400)
     if(we <= 11530) return 35;
     if(we > 11530) return 50;
   if(we > 11670)
    if(we <= 12740)
     if(we <= 11870) return 45;
     if(we > 11870) return 40;
    if(we > 12740) return 30;
  if(t > 1.23)
   if(we <= 16550) return 39.7833333333333;
   if(we > 16550)
    if(t <= 1.65) return 50;
    if(t > 1.65)
     if(we <= 22760)
      if(we <= 22100) return 30;
      if(we > 22100) return 40;
     if(we > 22760) return 50;
 if(t > 1.84)
  if(t <= 2.1)
   if(we <= 11870)
    if(we <= 11480) return 15;
    if(we > 11480) return 20;
   if(we > 11870)
    if(we <= 21730)
     if(we <= 12040) return 40;
     if(we > 12040)
      if(t <= 1.92)
       if(we <= 13310) return 30;
       if(we > 13310) return 20;
      if(t > 1.92) return 25;
    if(we > 21730) return 15;
  if(t > 2.1)
   if(t <= 2.31) return 30;
   if(t > 2.31)
    if(we <= 22270)
     if(t <= 2.52) return 30;
     if(t > 2.52) return 45;
    if(we > 22270) return 40;
if(p.equals("PP21486"))
 if(we <= 22520) return 40;
 if(we > 22520) return 50;
if(p.equals("PP21489")) return 50;
if(p.equals("PP21508")) return 30;
if(p.equals("PP21524")) return 30;
if(p.equals("PP21551")) return 25;
if(p.equals("PP21563"))
 if(t <= 0.85) return 25;
 if(t > 0.85) return 35.4166666666667;
if(p.equals("PP21576")) return 27.45;
if(p.equals("PP21578"))
 if(t <= 2.65)
  if(we <= 21670) return 20;
  if(we > 21670)
   if(we <= 22920) return 25;
   if(we > 22920) return 20;
 if(t > 2.65)
  if(we <= 21670) return 25;
  if(we > 21670) return 30;
if(p.equals("PP21597")) return 25.0833333333333;
if(p.equals("PP21620"))
 if(w <= 1042)
  if(t <= 0.91)
   if(we <= 16980) return 30;
   if(we > 16980) return 47.7;
  if(t > 0.91)
   if(we <= 16980) return 38.0333333333333;
   if(we > 16980) return 29.8333333333333;
 if(w > 1042)
  if(t <= 2.5)
   if(we <= 22020) return 40;
   if(we > 22020) return 31.1833333333333;
  if(t > 2.5) return 30;
if(p.equals("PP21628")) return 30;
if(p.equals("PP21667"))
 if(t <= 0.85) return 50;
 if(t > 0.85) return 40;
if(p.equals("PP21689")) return 30;
if(p.equals("PP21701"))
 if(w <= 1110) return 35;
 if(w > 1110)
  if(we <= 13340) return 25;
  if(we > 13340) return 30;
if(p.equals("PP21702"))
 if(w <= 1141)
  if(w <= 1042)
   if(we <= 16920) return 84.3833333333333;
   if(we > 16920) return 70;
  if(w > 1042) return 55;
 if(w > 1141)
  if(t <= 1.38) return 50;
  if(t > 1.38)
   if(t <= 1.895) return 60;
   if(t > 1.895) return 35;
if(p.equals("PP21730"))
 if(we <= 15400)
  if(we <= 9986) return 45;
  if(we > 9986)
   if(we <= 12010) return 40;
   if(we > 12010) return 50;
 if(we > 15400)
  if(we <= 18370) return 25;
  if(we > 18370) return 40.8333333333333;
if(p.equals("PP21756")) return 35;
if(p.equals("PP21777")) return 40;
if(p.equals("PP21791")) return 35;
if(p.equals("PP21811")) return 20;
if(p.equals("PP21831")) return 30;
if(p.equals("PP21833")) return 35;
if(p.equals("PP21880"))
 if(t <= 1.82)
  if(w <= 980) return 25;
  if(w > 980) return 20;
 if(t > 1.82)
  if(w <= 1090)
   if(w <= 980) return 60;
   if(w > 980) return 38.0666666666667;
  if(w > 1090)
   if(we <= 13110) return 19.8666666666667;
   if(we > 13110) return 25.5333333333333;
if(p.equals("PP21884"))
 if(t <= 1.55) return 25;
 if(t > 1.55)
  if(w <= 1126) return 30;
  if(w > 1126) return 60;
if(p.equals("PP21886")) return 35;
if(p.equals("PP21890")) return 35;
if(p.equals("PP21901")) return 30;
if(p.equals("PP22010")) return 40;
if(p.equals("PP22014"))
 if(we <= 13790) return 21;
 if(we > 13790) return 30;
if(p.equals("PP22020"))
 if(t <= 2.75) return 40;
 if(t > 2.75)
  if(we <= 22560) return 35;
  if(we > 22560) return 30;
if(p.equals("PP22026"))
 if(we <= 20900)
  if(we <= 20790) return 12.0333333333333;
  if(we > 20790) return 15;
 if(we > 20900)
  if(we <= 21720) return 30;
  if(we > 21720)
   if(we <= 22160) return 20;
   if(we > 22160) return 25;
if(p.equals("PP22104")) return 30;
if(p.equals("PP22175")) return 30;
if(p.equals("PP22178"))
 if(we <= 23260) return 40;
 if(we > 23260) return 50;
if(p.equals("PP22186")) return 30;
if(p.equals("PP22204")) return 28.4;
if(p.equals("PP22232"))
 if(we <= 23620)
  if(we <= 22540)
   if(we <= 22040) return 60;
   if(we > 22040) return 51.25;
  if(we > 22540) return 80;
 if(we > 23620) return 50;
if(p.equals("PP22236")) return 30;
if(p.equals("PP22269"))
 if(we <= 24110)
  if(t <= 3.5)
   if(t <= 3.4) return 20;
   if(t > 3.4) return 25;
  if(t > 3.5)
   if(t <= 3.9) return 13.15;
   if(t > 3.9) return 40;
 if(we > 24110)
  if(w <= 1223) return 15;
  if(w > 1223) return 26.75;
if(p.equals("PP22271"))
 if(t <= 1.58)
  if(we <= 17500)
   if(we <= 15990) return 33.1833333333333;
   if(we > 15990) return 30;
  if(we > 17500)
   if(we <= 17960)
    if(we <= 17860) return 25.4333333333333;
    if(we > 17860) return 45;
   if(we > 17960)
    if(we <= 18020) return 40;
    if(we > 18020) return 25;
 if(t > 1.58)
  if(t <= 2.02)
   if(we <= 17190) return 35;
   if(we > 17190) return 30;
  if(t > 2.02) return 30;
if(p.equals("PP22273"))
 if(w <= 1051)
  if(t <= 1.99) return 20;
  if(t > 1.99)
   if(we <= 15270) return 25;
   if(we > 15270) return 30;
 if(w > 1051) return 30;
if(p.equals("PP22274")) return 20;
if(p.equals("PP22291"))
 if(we <= 20210)
  if(we <= 19610) return 30;
  if(we > 19610) return 60;
 if(we > 20210)
  if(we <= 20380) return 40;
  if(we > 20380)
   if(we <= 20420) return 25;
   if(we > 20420) return 26.9666666666667;
if(p.equals("PP22296"))
 if(t <= 2.8)
  if(we <= 25030) return 50;
  if(we > 25030)
   if(we <= 25130) return 30;
   if(we > 25130) return 40;
 if(t > 2.8)
  if(we <= 23130)
   if(we <= 21051)
    if(we <= 20950) return 15;
    if(we > 20950) return 8.41666666666667;
   if(we > 21051)
    if(w <= 1099) return 35;
    if(w > 1099) return 30;
  if(we > 23130)
   if(w <= 1127)
    if(we <= 23190) return 50;
    if(we > 23190) return 45;
   if(w > 1127) return 50;
if(p.equals("PP22302")) return 30;
if(p.equals("PP22323"))
 if(t <= 1.895)
  if(we <= 17060) return 30;
  if(we > 17060) return 50;
 if(t > 1.895) return 30;
if(p.equals("PP22334"))
 if(we <= 24600) return 30;
 if(we > 24600) return 30.45;
if(p.equals("PP22374")) return 25;
if(p.equals("PP22376"))
 if(we <= 25040) return 30;
 if(we > 25040) return 45;
if(p.equals("PP22378")) return 54.7333333333333;
if(p.equals("PP22380")) return 26.8166666666667;
if(p.equals("PP22389")) return 45;
if(p.equals("PP22390")) return 25;
if(p.equals("PP22399")) return 29.5166666666667;
if(p.equals("PP22404")) return 31.6666666666667;
if(p.equals("PP22406")) return 30;
if(p.equals("PP22408"))
 if(t <= 1.42) return 25;
 if(t > 1.42) return 21;
if(p.equals("PP22409"))
 if(t <= 1.42) return 20;
 if(t > 1.42) return 20.2833333333333;
if(p.equals("PP22410")) return 20;
if(p.equals("PP22411")) return 40;
if(p.equals("PP22435")) return 30;
if(p.equals("PP22483")) return 20;
if(p.equals("PP22484")) return 40;
if(p.equals("PP22485")) return 25.2333333333333;
if(p.equals("PP22493")) return 50;
if(p.equals("PP22494")) return 50;
if(p.equals("PP22517"))
 if(w <= 1284)
  if(w <= 1246) return 30;
  if(w > 1246)
   if(we <= 23850) return 55;
   if(we > 23850) return 30;
 if(w > 1284)
  if(we <= 23250) return 25.1666666666667;
  if(we > 23250) return 28.2333333333333;
if(p.equals("PP22537"))
 if(t <= 0.85) return 30;
 if(t > 0.85) return 25;
if(p.equals("PP22540")) return 50;
if(p.equals("PP22541")) return 50;
if(p.equals("PP22542")) return 50;
if(p.equals("PP22567"))
 if(we <= 24920) return 35.1;
 if(we > 24920)
  if(we <= 25150) return 25;
  if(we > 25150) return 29.9;
if(p.equals("PP22576")) return 60;
if(p.equals("WS11039")) return 30;
if(p.equals("WS21130")) return 50;
if(p.equals("WS21271")) return 30;
return 30.0;
}
}
