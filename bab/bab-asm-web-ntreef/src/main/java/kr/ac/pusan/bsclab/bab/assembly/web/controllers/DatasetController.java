package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;
import kr.ac.pusan.bsclab.bab.ws.api.repository.map.MappingJobConfiguration;

@Controller
public class DatasetController extends AbstractWebController {

	public static final String BASE_URL = BabWeb.BASE_URL + "/dataset";

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/{workspaceId}/{datasetId}")
	public ModelAndView getIndex(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId, HttpServletRequest request, HttpSession session) {
		return getIndex(workspaceId, datasetId, null, request, session);
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/{workspaceId}/{datasetId}/{repositoryId}")
	public ModelAndView getIndex(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId, HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("dataset/dataset");

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("repositoryId", repositoryId);
		jsonData.put("sdt", (String) session.getAttribute("sdt"));
		jsonData.put("edt", (String) session.getAttribute("edt"));

		view.addObject("jsonData", jsonData);
		// view.addObject("apiURI", this.APIURI);
		 view.addObject("apiURI", apiManager.getAPIURI());

		return view;
	}

	@RequestMapping(method = RequestMethod.POST, path = BASE_URL + "/{workspaceId}/{datasetId}/{repositoryId}")
	public ModelAndView postIndex(@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "repositoryId") String repositoryId, MappingJobConfiguration config,
			ModelMap modelMap, HttpServletRequest request, HttpSession session) {
		ModelAndView view = new ModelAndView("dataset/dataset");

//		Response<BRepository> result = callBabService(APIURI.get("repository").get("mapping"), config,
//				BRepository.class);
//		Response<BRepository> result = callBabService(apiManager.getAPIURI().get("repository").get("mapping"), config, BRepository.class);
		

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
		jsonData.put("datasetId", datasetId);
		jsonData.put("repositoryId", repositoryId);
		jsonData.put("sdt", (String) session.getAttribute("sdt"));
		jsonData.put("edt", (String) session.getAttribute("edt"));

		view.addObject("jsonData", jsonData);
//		view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());


		return view;
	}
}
