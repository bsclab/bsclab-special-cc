package kr.ac.pusan.bsclab.bab.assembly.parallelscheduling;

public class SchedulingDataPerOperation {
	public String job;
	public String machine;
	public String machine_ann;
	public String jobseq;
	public String parent_mat;
	public double duration;
	public double duration_fix;
	public long st_time;
	public long ed_time;
	
	public String humanize_st_time;
	public String humanize_ed_time;

	
	public String realjob;
	public String real_histcoi;
	public double thick;
	public int width;
	public int weight;
	public String spec_code;
	public String grd_cd;
    public String prdknd_cd;
    public String prc_cd;
    public String stl_cd;
    public String cus_id;
    public String usr_id;
    public String line_map;
    public String drtcoi_no;
    
    public int seq_in_realjob;
    
    public String color_drtcoi;
}
