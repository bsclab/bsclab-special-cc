package kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job;

import kr.ac.pusan.bsclab.bab.assembly.domain.jobqueue.job.configuration.JobConfiguration;

public class Job<T extends JobConfiguration>{
	
	private JobType jobType;
	private T jobConfiguration;
	
	
	Job(JobType jobType, T jobConfiguration) {
		this.setJobType(jobType);
		this.setJobConfiguration(jobConfiguration);
	}
	
	public JobType getJobType() {
		return jobType;
	}
	public JobConfiguration getJobConfiguration() {
		return jobConfiguration;
	}
	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}
	public void setJobConfiguration(T jobConfiguration) {
		this.jobConfiguration = jobConfiguration;
	}
	

	
	

}
