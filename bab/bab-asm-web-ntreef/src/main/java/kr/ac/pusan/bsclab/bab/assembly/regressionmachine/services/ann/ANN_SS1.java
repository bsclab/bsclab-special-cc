package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SS1 implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-1.85872245, -2.98933744, 8.13344383, 0.01283515, -0.38070986, 0.29435593
            				, -1.56714618, 1.36736238, -0.02257391, 0.55523676, -0.02364658, -0.44641945
            				, 0.13744642, -2.95278358, -1.93843532, -0.72682613, 0.42746639, 0.19565518
            				, 0.52536052, 0.7595787}, 
            			{-0.25867894, 0.09877475, 1.16200387, -0.14383216, 0.14223349, 0.29997888
        					, -0.26587692, -3.24207759, 3.43682218, -0.62083977, 1.08886027, -0.50231344
        					, -1.84275091, 0.77923471, 0.78415191, 1.09201896, 1.49598074, -1.29918063
        					, -0.75261074, -2.18278456},
            			{1.68962657, 0.85117733, -0.45469508, -0.76057601, 0.78306603, 0.06251518
    						, -0.02165141, -1.24424112, -0.33388844, -0.33508056, 0.11590931, 1.51047897
    						, 0.33935139, -1.11819398, -1.37169814, -0.25589168, -0.77734852, -2.23321342
    						, -0.39063594, -1.5133872}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-2.15781546, -0.65408266, 1.55506516, -0.21312244, -0.7946108, -0.68814081
            			, -0.11866862, -0.61729813, -0.33374727, -1.32398367, -1.18102872, -1.46899247
            			, -0.55157971, -0.5856117, -0.6798355, -1.08126104, -0.55650532, -0.32706842
            			, -0.63679987, 0.01320212};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-1.44366789,
            			0.84594613,
            			2.63965082,
            			-0.75568783,
            			1.76458943,
            			0.75114232,
            			-0.51057029,
            			-0.56920004,
            			0.12010618,
            			-0.5337888,
            			-1.14151347,
            			1.07810271,
            			0.74909347,
            			-0.3139044,
            			-0.10598455,
            			1.39974439,
            			-2.33955669,
            			-0.43650636,
            			-1.4329015,
            			0.32696187};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-0.37935019};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 4.5;
	}

	@Override
	public double getMaxWidth() {
		
		return 440.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 5124.0;
	}

	@Override
	public double getMinThick() {
		
		return 1.0;
	}

	@Override
	public double getMinWidth() {
		
		return 23.0;
	}

	@Override
	public double getMinWeight() {
		
		return 86.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
