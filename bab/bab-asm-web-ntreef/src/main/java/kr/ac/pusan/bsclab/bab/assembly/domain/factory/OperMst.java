package kr.ac.pusan.bsclab.bab.assembly.domain.factory;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="sf_oper_mst")
public class OperMst {
	
	@Id
	@Column(name="BUS_CD")
	String busCd;

	@Column(name="COIL_NO")
	String coilNo;

	@Column(name="LOT_NO")
	String lotNo;

	@Column(name="ORD_NO")
	String ordNo;

	@Column(name="ORD_SEQ")
	String ordSeq;

	@Column(name="HOTCOI_NO")
	String hotcoiNo;

	@Column(name="EMG_CD")
	String emgCd;

	@Column(name="SCHFRM_YN")
	String schfrmYn;

	@Column(name="SCHFRM_DT")
	String schfrmDt;

	@Column(name="SCHDCH_NO")
	String schdchNo;

	@Column(name="PRDPLN_WGT")
	String prdplnWgt;

	@Column(name="PLNPRC_CD")
	String plnprcCd;

	@Column(name="RAWSTL_CLS")
	String rawstlCls;

	@Column(name="RAWSPC_ID")
	String rawspcId;

	@Column(name="RAWTHK_LEN")
	String rawthkLen;

	@Column(name="RAWWDT_LEN")
	String rawwdtLen;

	@Column(name="RAWLEN_LEN")
	String rawlenLen;

	@Column(name="RAWWGT_WGT")
	String rawwgtWgt;

	@Column(name="PURCOM_ID")
	String purcomId;

	@Column(name="SPL_REM")
	String splRem;

	@Column(name="SALBRN_CD")
	String salbrnCd;

	@Column(name="CUS_ID")
	String cusId;

	@Column(name="USR_ID")
	String usrId;

	@Column(name="ORD_CD")
	String ordCd;

	@Column(name="SPC_NO")
	String spcNo;

	@Column(name="PRDKND_CD")
	String prdkndCd;

	@Column(name="STL_CD")
	String stlCd;

	@Column(name="STLDTL_CD")
	String stldtlCd;

	@Column(name="PRD_CD")
	String prdCd;

	@Column(name="PRDSPC_ID")
	String prdspcId;

	@Column(name="ORDTHK_LEN")
	String ordthkLen;

	@Column(name="ORDWDT_LEN")
	String ordwdtLen;

	@Column(name="ORDLEN_LEN")
	String ordlenLen;

	@Column(name="ORD_WGT")
	String ordWgt;

	@Column(name="ORDIND_CD")
	String ordindCd;

	@Column(name="ORDMAT_LEN")
	String ordmatLen;

	@Column(name="ORDMIT_LEN")
	String ordmitLen;

	@Column(name="ORDMAO_LEN")
	String ordmaoLen;

	@Column(name="ORDMIO_LEN")
	String ordmioLen;

	@Column(name="ORDMAW_LEN")
	String ordmawLen;

	@Column(name="ORDMIW_LEN")
	String ordmiwLen;

	@Column(name="HRDMTH_CD")
	String hrdmthCd;

	@Column(name="HRDMAX_VAL")
	String hrdmaxVal;

	@Column(name="HRDMIN_VAL")
	String hrdminVal;

	@Column(name="OIL_CD")
	String oilCd;

	@Column(name="PRDUNT_WGT")
	String prduntWgt;

	@Column(name="PRUMAX_WGT")
	String prumaxWgt;

	@Column(name="PRUMIN_WGT")
	String pruminWgt;

	@Column(name="PKGUNT_WGT")
	String pkguntWgt;

	@Column(name="PKUMAX_WGT")
	String pkumaxWgt;

	@Column(name="PKUMIN_WGT")
	String pkuminWgt;

	@Column(name="PKGMTH_CD")
	String pkgmthCd;

	@Column(name="PKG_CD")
	String pkgCd;

	@Column(name="WGTDCS_CLS")
	String wgtdcsCls;

	@Column(name="SURSTS_CD")
	String surstsCd;

	@Column(name="USG_CD")
	String usgCd;

	@Column(name="DLV_YMD")
	String dlvYmd;

	@Column(name="LOTFRM_DT")
	String lotfrmDt;

	@Column(name="LOTFRC_NO")
	String lotfrcNo;

	@Column(name="CRT_DT")
	String crtDt;

	@Column(name="CRTCHR_NO")
	String crtchrNo;

	@Column(name="UPD_DT")
	String updDt;

	@Column(name="UPDCHR_NO")
	String updchrNo;

	@Column(name="ZON_CD")
	String zonCd;

	
}
