package kr.ac.pusan.bsclab.bab.assembly.domain.info.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.mysql.jdbc.Statement;

import kr.ac.pusan.bsclab.bab.assembly.domain.info.Job;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.JobStatus;


@Repository
public class JobDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Job> findAll() {
		return jdbcTemplate.query("select * from job"
				+ " order by find_in_set(jobStatus, \"RUNNING\") desc"
				+ ", start desc"
				+ ", no desc"
				, new JobRowMapper());
	}
	
	public int save(Job job) {
		
		String sql = "INSERT INTO job(jobId"
				+ ", jobStatus"
				+ ", job"
//				+ ", userNo"
				+ ", start"
				+ ", progress"
				+ ", reportNo) VALUES(?, ?, ?, ?, ?, ?)";
		
		GeneratedKeyHolder gkh = new GeneratedKeyHolder();
		
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection conn)
					throws SQLException {
				PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, job.getJobId());
				ps.setString(2,  job.getJobStatus().name());
				ps.setString(3, job.getJob());
//				ps.setInt(4, job);
				ps.setTimestamp(4, job.getStart());
				ps.setInt(5, job.getProgress());;
				ps.setInt(6, job.getReportNo());
				return ps;
			}
		}, gkh);
		return gkh.getKey().intValue();
	}
	
	public int updateByJobId(Job job) {
		String sql = "UPDATE job SET"
				+ " jobStatus=?"
				+ ", job=?"
				+ ", end=?"
				+ ", reportNo=?"
				+ " WHERE jobId=?";
		return jdbcTemplate.update(sql, new Object[] {
				job.getJobStatus().name()
				, job.getJob()
				, job.getEnd()
				, job.getReportNo()
				, job.getJobId()
		});
	}
	
	public int updateByNo(Job job) {
		String sql = "UPDATE job SET"
				+ " jobStatus=?"
				+ ", job=?"
				+ ", end=?"
				+ ", reportNo=?"
				+ " WHERE no=?";
		return jdbcTemplate.update(sql, new Object[] {
				job.getJobStatus().name()
				, job.getJob()
				, job.getEnd()
				, job.getReportNo()
				, job.getNo()	
		});
	}

}

class JobRowMapper implements RowMapper<Job>{
	@Override
	public Job mapRow(ResultSet rs, int rowNum) throws SQLException {
		Job job = new Job();
		job.setNo(rs.getInt("no"));
		job.setJobId(rs.getString("jobId"));
		job.setJob(rs.getString("job"));
		job.setJobStatus(JobStatus.valueOf(rs.getString("jobStatus")));
		job.setStart(rs.getTimestamp("start"));
		job.setEnd(rs.getTimestamp("end"));
		job.setProgress(rs.getInt("progress"));
		job.setReportNo(rs.getInt("reportNo"));
		return job;
	} 
}
