package kr.ac.pusan.bsclab.bab.assembly.web.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.pusan.bsclab.bab.assembly.web.BabWeb;

@Controller
public class BabWebController extends AbstractWebController {

	public static final String BASE_URL = BabWeb.BASE_URL;

	@RequestMapping(method = RequestMethod.GET, path = "/")
	public @ResponseBody ModelAndView getRootIndex(HttpSession session) {
		return new ModelAndView("redirect:/bab/dashboard/"
				+ session.getAttribute("workspaceId")
				+ "/" + session.getAttribute("datasetId")
				+ "/" + session.getAttribute("sdt")
				+ "/" + session.getAttribute("edt"));
	}

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL + "/home/{workspaceId}")
	public ModelAndView getIndex(@PathVariable(value = "workspaceId") String workspaceId, HttpServletRequest request,
			HttpSession session) {
		ModelAndView view = new ModelAndView("home/home");

		String message = this.getClass().getName();

		String[] vehicles = { "KIA", "Hyundai", "Daewoo", "Samsung" };

		Map<String, String> jsonData = new HashMap<String, String>();
		jsonData.put("workspaceId", workspaceId);
//		jsonData.put("importUri", this.APIURI.get("repository").get("import"));
		jsonData.put("importUri", apiManager.getAPIURI().get("repository").get("import"));
		jsonData.put("sdt", (String) session.getAttribute("sdt"));
		jsonData.put("edt", (String) session.getAttribute("edt"));

		session.setAttribute("workspaceId", workspaceId);

		view.addObject("jsonData", jsonData);
		view.addObject("message", message);
		view.addObject("vehicles", vehicles);
		// view.addObject("apiURI", this.APIURI);
		view.addObject("apiURI", apiManager.getAPIURI());

		return view;
	};

	@RequestMapping(method = RequestMethod.GET, path = BASE_URL)
	public ModelAndView getIndex(HttpServletRequest request, HttpSession session) {
		return new ModelAndView("redirect:/bab/home/dongkuk");
	}

	@RequestMapping(method = RequestMethod.POST, path = BASE_URL + "/home/{workspaceId}")
	public ModelAndView getIndex(@RequestParam("files") MultipartFile[] files) {
		ModelAndView view = new ModelAndView("bab/index");

		String message = this.getClass().getName();
		view.addObject("message", message);

		String rootPath = System.getProperty("catalina.home");
		File uploadDir = new File(rootPath + File.separator + "uploads");
		if (!uploadDir.exists())
			uploadDir.mkdirs();

		for (MultipartFile file : files) {
			if (!file.isEmpty()) {
				try {
					byte[] bytes = file.getBytes();
					File serverFile = new File(
							uploadDir.getAbsolutePath() + File.separator + file.getOriginalFilename());
					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
					stream.write(bytes);
					stream.close();
					webHdfs.copyFromLocal(serverFile.getPath(), "/test/test.txt");
					webHdfs.listStatus("/test");
					webHdfs.isExists("/test/test.txt");
					webHdfs.openAsTextFile("/test/test.txt");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return view;
	}
}
