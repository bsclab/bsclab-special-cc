package kr.ac.pusan.bsclab.bab.assembly.conf;


import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import kr.ac.pusan.bsclab.bab.assembly.BabServer;
import kr.ac.pusan.bsclab.bab.assembly.domain.factory.dao.LogDao;
import kr.ac.pusan.bsclab.bab.assembly.domain.user.dao.UserDao;
import kr.ac.pusan.bsclab.bab.assembly.utils.DateConvertor;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

//	@Autowired
//	private UserDetailsService userDetailsService;

	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private LogDao logDao;
	
	@Autowired
	DateConvertor dc;
	
	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
//		auth.userDetailsService(userDetailsService).passwordEncoder(passwordencoder());
		auth.jdbcAuthentication().dataSource(dataSource).passwordEncoder(passwordEncoder)
				.usersByUsernameQuery("select username, password, enabled from user where username=?")
				.authoritiesByUsernameQuery("select user.username, user_roles.role from user, user_roles where user.no = user_roles.userNo and user.username=?");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.antMatchers(BabServer.BASE_URL + "/modules/**"
					, BabServer.BASE_URL + "/api/**"
					, BabServer.BASE_URL + "/images/**"
					, "/bower_components/**").permitAll();
//					, "/vue-components/**"
		
		http.authorizeRequests()
			.antMatchers(BabServer.BASE_URL + "/login"
					, BabServer.BASE_URL + "/h2-console/**").permitAll();
		
		http.authorizeRequests()
			.anyRequest().hasAuthority("ROLE_ADMIN");
		
		http.formLogin()
			.loginPage(BabServer.BASE_URL + "/login")
			.loginProcessingUrl(BabServer.BASE_URL + "/login")
			.failureUrl(BabServer.BASE_URL + "/login?code=1")
			.usernameParameter("username")
			.passwordParameter("password")
			.successHandler(new AuthenticationSuccessHandler() {
				@Override
				public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
						throws IOException, ServletException {
					HttpSession session = req.getSession();
					
					kr.ac.pusan.bsclab.bab.assembly.domain.user.User user = userDao.findOneByUsername(((User) auth.getPrincipal()).getUsername());
					
					session.setAttribute("user", userDao.findOneByUsername(
							((User) auth.getPrincipal()).getUsername()));
					
//					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Calendar sdtCal = Calendar.getInstance();
					sdtCal.add(Calendar.MONTH, -1);
					
					String sdt = dc.getDateStr(sdtCal.getTime());
					String edt = dc.getDateStr(new Date());
					
//					session.setAttribute("sdt", sdt));
//					session.setAttribute("edt", edt);
//					session.setAttribute("resourceDataHash", logDao.getChecksum(
//							new Timestamp(sdtCal.getTimeInMillis())
//							, new Timestamp(new Date().getTime())));

					sdt = "2016-01-01";
					edt = "2016-01-31";
					
					session.setAttribute("sdt", sdt);
					session.setAttribute("edt", edt);
					session.setAttribute("workspaceId", user.getWorkspace());
					session.setAttribute("datasetId", user.getDataset());
					try {
						session.setAttribute("resourceDataHash", logDao.getChecksum(
								new Timestamp(dc.getTimestamp(sdt).getTime())
								, new Timestamp(dc.getTimestamp(edt).getTime())));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
					res.sendRedirect(BabServer.BASE_URL + "/dashboard"
					+ "/" + user.getWorkspace()
					+ "/" + user.getDataset()
					+ "/" + sdt
					+ "/" + edt);
				}
			});
			
		http.logout()
			.logoutSuccessUrl(BabServer.BASE_URL + "/login?code=2");
		
		http.csrf().disable();
		
		http.headers().frameOptions().disable();
		
//		http.authorizeRequests().anyRequest().permitAll();
	}

//	@Autowired
//	CustomUserDetailsService customUserDetailsService;
//	
//	@Override
//	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		
//	}

	@Bean(name = "passwordEncoder")
	public PasswordEncoder passwordencoder() {
		return new BCryptPasswordEncoder();
	}
}
