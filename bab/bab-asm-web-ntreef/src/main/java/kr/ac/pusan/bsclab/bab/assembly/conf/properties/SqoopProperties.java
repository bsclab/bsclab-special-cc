package kr.ac.pusan.bsclab.bab.assembly.conf.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("application-sqoop.properties")
@ConfigurationProperties(prefix="sqoop")
public class SqoopProperties {
	
	private String server;
	private String apiPort;
	private String apiUri;
	private String sqoopCmd;
	private String dbToHdfsQuery;
	
	public String getServer() {
		return server;
	}
	public String getApiPort() {
		return apiPort;
	}
	public String getApiUri() {
		return apiUri;
	}
	public String getSqoopCmd() {
		return sqoopCmd;
	}
	public String getDbToHdfsQuery() {
		return dbToHdfsQuery;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public void setApiPort(String apiPort) {
		this.apiPort = apiPort;
	}
	public void setApiUri(String apiUri) {
		this.apiUri = apiUri;
	}
	public void setSqoopCmd(String sqoopCmd) {
		this.sqoopCmd = sqoopCmd;
	}
	public void setDbToHdfsQuery(String dbToHdfsQuery) {
		this.dbToHdfsQuery = dbToHdfsQuery;
	}
	

}
