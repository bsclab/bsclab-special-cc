package kr.ac.pusan.bsclab.bab.assembly.ws.models;

public @interface BabServiceInfo {
	String title();
	String uri();
	String description() default "";
	String author() default "BSCLab";
	String version() default "2.2.0";
	String[] parameters() default {};
}
