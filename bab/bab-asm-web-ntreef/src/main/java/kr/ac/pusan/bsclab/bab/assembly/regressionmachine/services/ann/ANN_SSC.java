package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SSC implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{-6.61136091e-01, -3.17741007e-01, -1.79835513e-01, 4.53830004e+00
            				, 1.90927792e+01, -2.42275238e+00, -1.28140116e+00, 6.01575971e-01
            				, -1.13260579e+00, -1.18965912e+00, 1.13736260e+00, -6.98427439e-01
            				, -8.56969476e-01, -7.01670587e-01, -1.50101376e+00, -3.69665694e+00
            				, -6.41679764e-01, 1.27174175e+00, 3.76807237e+00, -5.17487377e-02}, 
            			{-2.11976767e-01, -1.78969860e+00, -7.01459169e-01, 1.36362231e+00
        					, 8.66181374e-01, -1.15197396e+00, -4.52327698e-01, 3.55795920e-02
        					, 1.11008859e+00, -5.97836912e-01, -1.04075086e+00, -6.92713916e-01
        					, 1.25627673e+00, -6.65150106e-01, -1.27324212e+00, -9.34138969e-02
        					, 6.25058532e-01, 1.14263901e-02, -5.96348867e-02, 1.02042067e+00},
            			{-2.50591058e-02, -7.01324582e-01, -1.58282757e-01, 1.14261413e+00
    						, 6.82116002e-02, 7.79783607e-01, -7.26130068e-01, -1.89195335e-01
    						, -3.24802101e-01, -1.15584302e+00, 5.19545853e-01, -7.41530776e-01
    						, -1.04153621e+00, 3.19994092e-01, -1.48821127e+00, 1.83052385e+00
    						, -1.09157073e+00, -3.91618907e-01, 2.57116914e-01, -2.10946822e+00}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-1.13056982, 0.28549671, -0.28926992, 0.35481077, 1.00009263, 1.21731222
            			, 0.97294569, -0.64336878, 1.30782795, -0.40671626, -0.63360739, -0.26363185
            			, -0.20679849, 1.07955158, 0.44158772, 0.83625513, 0.81595266, -1.06149852
            			, 0.07359286, 0.85579634};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.19909847,
            			1.12209916,
            			0.09435201,
            			-1.01883578,
            			-0.98119318,
            			1.232005  ,
            			1.21587431,
            			-1.08046174,
            			0.41548851,
            			-1.0118463 ,
            			-0.07515831,
            			-1.75189543,
            			0.35785478,
            			0.26625079,
            			0.85611612,
            			1.36923802,
            			0.88172293,
            			0.72197622,
            			-0.38991055,
            			-0.48381934,};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-0.05948742};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 1.2;
	}

	@Override
	public double getMaxWidth() {
		
		return 25.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 104.0;
	}

	@Override
	public double getMinThick() {
		
		return 0.23;
	}

	@Override
	public double getMinWidth() {
		
		return 6.0;
	}

	@Override
	public double getMinWeight() {
		
		return 5.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
