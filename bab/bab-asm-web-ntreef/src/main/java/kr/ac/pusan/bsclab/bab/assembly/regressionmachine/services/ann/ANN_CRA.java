package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_CRA implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{1.00946891, -20.567291830, -20.171970370, 1.188281060, 0.7687387470, -20.20154724, -1.88177252, 0.194351718, -2.44093919, 0.101811633, -20.00239429, -20.07886448, -0.193055570, 1.06256998, 0.586747169, -0.0320967808, 10.30827465, -4.54193497, -100.45715012, 0.146003604}, 
            			{-1.68019354, -0.222517759, 0.628388703, -0.842124879, -0.157018259, -1.31835198, -2.18645167, 0.730237007, -0.529635847, -1.76221251, -0.134953275, -1.20144916, 0.812195778, 0.649131417, -1.66958320, -0.981966615, -1.74221611, 0.984809697, -1.87826729, 0.899124384},
            			{-0.596781373, -0.742308736, 1.62189758, -0.231639966, 1.61458278, 0.0853037760, -1.71959364, -0.617977083, -0.333494008, -0.132230803, -0.0525016598, -0.338132739, -1.77899694, -1.09098947, 0.545233786, -0.382380337, 1.13066828, 0.977579415, -0.109187640, -0.933619380}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {-0.88328803, 0.76632702, -0.10286772, -1.36246109, -1.61071289, 0.25582156, 0.36044508, -0.0775611, -0.35668904, -0.27501121, 0.57585949, 0.70145661, 0.15293679, -1.1998924, -1.25410306, 0.1027367, -0.0916137, -0.51078904, 0.30584824, -0.90412998};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {0.74101001, 1.73333418, 1.95490181, -0.8418864, 0.43979016, 1.35954726, -0.69141185, -0.94173807, -0.04700637, -0.73064333, 1.29400349, 1.07595313, -0.05476782, -0.91210425, 0.56402504, -0.60269094, -0.28233078, 0.29338226, 5.04764891, 0.61844349};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {-0.4966169};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 4.35;
	}

	@Override
	public double getMaxWidth() {
		
		return 426.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 5926.00;
	}

	@Override
	public double getMinThick() {
		
		return 0.3;
	}

	@Override
	public double getMinWidth() {
		
		return 180.0;
	}

	@Override
	public double getMinWeight() {
		
		return 931.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
