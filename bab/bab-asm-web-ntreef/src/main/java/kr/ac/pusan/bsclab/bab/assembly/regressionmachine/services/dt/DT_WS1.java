package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;
public class DT_WS1 implements DT_base {
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("PP11009"))
 if(we <= 4509) return 60;
 if(we > 4509)
  if(w <= 311) return 60;
  if(w > 311) return 40;
if(p.equals("PP11010"))
 if(we <= 3651)
  if(t <= 4.8) return 70;
  if(t > 4.8) return 60;
 if(we > 3651)
  if(t <= 5.4)
   if(w <= 339) return 60;
   if(w > 339)
    if(we <= 5280)
     if(we <= 4863) return 50;
     if(we > 4863) return 40;
    if(we > 5280) return 60;
  if(t > 5.4) return 60;
if(p.equals("PP11013")) return 80;
if(p.equals("PP11015")) return 80;
if(p.equals("PP11017")) return 60;
if(p.equals("PP11018")) return 60;
if(p.equals("PP11021"))
 if(w <= 424)
  if(t <= 5.02)
   if(w <= 313)
    if(t <= 3.1) return 40;
    if(t > 3.1) return 60;
   if(w > 313)
    if(t <= 4.2)
     if(we <= 4033) return 40;
     if(we > 4033) return 50;
    if(t > 4.2) return 50;
  if(t > 5.02)
   if(we <= 5213)
    if(w <= 347)
     if(we <= 3493) return 50;
     if(we > 3493) return 60;
    if(w > 347) return 40;
   if(we > 5213) return 60;
 if(w > 424) return 40;
if(p.equals("PP11029"))
 if(w <= 392) return 60;
 if(w > 392) return 40;
if(p.equals("PP11030")) return 40;
if(p.equals("PP11032")) return 60;
if(p.equals("PP11033"))
 if(w <= 152) return 90;
 if(w > 152)
  if(t <= 3.017) return 40;
  if(t > 3.017) return 60;
if(p.equals("PP11050")) return 50;
if(p.equals("PP11064"))
 if(t <= 3.593)
  if(t <= 3.017) return 40;
  if(t > 3.017) return 35;
 if(t > 3.593) return 40;
if(p.equals("PP11065"))
 if(t <= 4.6) return 60;
 if(t > 4.6)
  if(w <= 174) return 60;
  if(w > 174) return 80;
if(p.equals("PP11325"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 80;
if(p.equals("PP11435"))
 if(t <= 4.5)
  if(t <= 3.017) return 40;
  if(t > 3.017) return 60;
 if(t > 4.5) return 90;
if(p.equals("PP11671")) return 40;
if(p.equals("PP11884")) return 60;
if(p.equals("PP11891"))
 if(we <= 3376) return 50;
 if(we > 3376)
  if(we <= 3582) return 70;
  if(we > 3582) return 90;
if(p.equals("PP11984")) return 50;
if(p.equals("PP11985")) return 60;
if(p.equals("PP12040")) return 50;
if(p.equals("PP12069")) return 60;
if(p.equals("PP12073"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 60;
if(p.equals("PP12087")) return 80;
if(p.equals("PP12152")) return 60;
if(p.equals("PP12178")) return 40;
if(p.equals("PP12184")) return 60;
if(p.equals("PP12194")) return 50;
if(p.equals("PP12196"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 30;
if(p.equals("PP12197")) return 50;
if(p.equals("PP12198"))
 if(t <= 3.034) return 40;
 if(t > 3.034) return 60;
if(p.equals("PP12201")) return 50;
if(p.equals("PP12202")) return 50;
if(p.equals("PP12211"))
 if(we <= 4448) return 70;
 if(we > 4448)
  if(we <= 5359) return 40;
  if(we > 5359) return 60;
if(p.equals("PP12221")) return 50;
if(p.equals("PP12238"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 50;
if(p.equals("PP12254")) return 60;
if(p.equals("PP12257"))
 if(we <= 2964) return 40;
 if(we > 2964)
  if(t <= 3.3) return 50;
  if(t > 3.3) return 60;
if(p.equals("PP12263")) return 50;
if(p.equals("PP12275"))
 if(we <= 5905) return 40;
 if(we > 5905) return 50;
if(p.equals("PP12284")) return 40;
if(p.equals("PP12289")) return 60;
if(p.equals("PP12302")) return 50;
if(p.equals("PP12324")) return 20;
if(p.equals("PP12331")) return 60;
if(p.equals("PP12337"))
 if(t <= 5.02)
  if(t <= 4.2) return 60;
  if(t > 4.2)
   if(w <= 274) return 40;
   if(w > 274)
    if(we <= 4343) return 60;
    if(we > 4343)
     if(t <= 5.01) return 40;
     if(t > 5.01) return 60;
 if(t > 5.02) return 70;
if(p.equals("PP12359")) return 60;
if(p.equals("PP12369")) return 60;
if(p.equals("PP12392")) return 60;
if(p.equals("PP12393")) return 60;
if(p.equals("PP12410")) return 40;
if(p.equals("PP12436"))
 if(t <= 3.034)
  if(we <= 3849) return 50;
  if(we > 3849) return 40;
 if(t > 3.034) return 60;
if(p.equals("PP12439"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 50;
if(p.equals("PP12446"))
 if(t <= 4.2) return 60;
 if(t > 4.2)
  if(we <= 4291) return 40;
  if(we > 4291) return 50;
if(p.equals("PP12447")) return 40;
if(p.equals("PP12449")) return 50;
if(p.equals("PP12450")) return 50;
if(p.equals("PP12461")) return 40;
if(p.equals("PP12462"))
 if(we <= 4541) return 50;
 if(we > 4541) return 40;
if(p.equals("PP12469"))
 if(we <= 4281) return 40;
 if(we > 4281) return 60;
if(p.equals("PP12470")) return 75;
if(p.equals("PP12477")) return 40;
if(p.equals("PP12478")) return 40;
if(p.equals("PP12480")) return 60;
if(p.equals("PP12481")) return 60;
if(p.equals("PP12486")) return 60;
if(p.equals("PP12487")) return 60;
if(p.equals("PP12499")) return 50;
if(p.equals("PP12501")) return 40;
if(p.equals("PP12513"))
 if(we <= 5009) return 50;
 if(we > 5009) return 60;
if(p.equals("PP12514"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 50;
if(p.equals("PP12531")) return 60;
if(p.equals("PP12532")) return 60;
if(p.equals("PP12539")) return 60;
if(p.equals("PP12545")) return 40;
if(p.equals("PP21078")) return 70;
if(p.equals("PP21092")) return 60;
if(p.equals("PP21107")) return 60;
if(p.equals("PP21114")) return 60;
if(p.equals("PP21288")) return 60;
if(p.equals("PP21405"))
 if(w <= 186)
  if(t <= 4.97) return 70;
  if(t > 4.97) return 90;
 if(w > 186)
  if(t <= 4.97) return 60;
  if(t > 4.97)
   if(w <= 222) return 60;
   if(w > 222) return 80;
if(p.equals("PP21409"))
 if(t <= 5.5) return 70;
 if(t > 5.5)
  if(t <= 6)
   if(w <= 180)
    if(we <= 1648) return 60;
    if(we > 1648) return 90;
   if(w > 180)
    if(we <= 1814) return 60;
    if(we > 1814) return 73.3333333333333;
  if(t > 6)
   if(we <= 1296) return 60;
   if(we > 1296) return 90;
if(p.equals("PP21414"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 60;
if(p.equals("PP21416"))
 if(we <= 1544)
  if(we <= 1318) return 90;
  if(we > 1318)
   if(we <= 1478) return 80;
   if(we > 1478) return 90;
 if(we > 1544) return 60;
if(p.equals("PP21420"))
 if(we <= 13420)
  if(w <= 1025)
   if(t <= 5.4) return 80;
   if(t > 5.4)
    if(we <= 9175) return 60;
    if(we > 9175)
     if(we <= 9688) return 40;
     if(we > 9688) return 70;
  if(w > 1025) return 60;
 if(we > 13420)
  if(we <= 18893)
   if(we <= 18297)
    if(we <= 17110) return 40;
    if(we > 17110)
     if(we <= 17206) return 45;
     if(we > 17206)
      if(we <= 18098) return 40;
      if(we > 18098) return 45;
   if(we > 18297)
    if(we <= 18749) return 30;
    if(we > 18749) return 20;
  if(we > 18893)
   if(we <= 19578) return 50;
   if(we > 19578) return 40;
if(p.equals("PP21429")) return 30;
if(p.equals("PP21433")) return 60;
if(p.equals("PP21478"))
 if(t <= 4.98) return 60;
 if(t > 4.98) return 100;
if(p.equals("PP21485"))
 if(w <= 587) return 90;
 if(w > 587) return 5;
if(p.equals("PP21529")) return 30;
if(p.equals("PP21557")) return 60;
if(p.equals("PP21644")) return 60;
if(p.equals("PP21671")) return 15;
if(p.equals("PP21689")) return 50;
if(p.equals("PP21756")) return 60;
if(p.equals("PP21773")) return 45;
if(p.equals("PP21880"))
 if(we <= 11780) return 60;
 if(we > 11780) return 40;
if(p.equals("PP22010"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 70;
if(p.equals("PP22026")) return 60;
if(p.equals("PP22066"))
 if(we <= 3431) return 60;
 if(we > 3431) return 40;
if(p.equals("PP22067")) return 40;
if(p.equals("PP22104"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 60;
if(p.equals("PP22234")) return 30;
if(p.equals("PP22261")) return 40;
if(p.equals("PP22291")) return 60;
if(p.equals("PP22321")) return 40;
if(p.equals("PP22322")) return 40;
if(p.equals("PP22333")) return 60;
if(p.equals("PP22336"))
 if(t <= 4.2) return 50;
 if(t > 4.2)
  if(w <= 303) return 50;
  if(w > 303)
   if(we <= 2695) return 60;
   if(we > 2695) return 40;
if(p.equals("PP22339"))
 if(w <= 241) return 50;
 if(w > 241) return 40;
if(p.equals("PP22340")) return 40;
if(p.equals("PP22341")) return 60;
if(p.equals("PP22342"))
 if(t <= 3.593)
  if(t <= 3.3) return 60;
  if(t > 3.3)
   if(we <= 3777) return 30;
   if(we > 3777) return 40;
 if(t > 3.593) return 40;
if(p.equals("PP22348")) return 50;
if(p.equals("PP22352"))
 if(t <= 4.2) return 50;
 if(t > 4.2) return 40;
if(p.equals("PP22356"))
 if(we <= 3990) return 60;
 if(we > 3990) return 50;
if(p.equals("PP22357"))
 if(t <= 3.2) return 40;
 if(t > 3.2)
  if(we <= 3665) return 30;
  if(we > 3665)
   if(we <= 4033) return 40;
   if(we > 4033) return 50;
if(p.equals("PP22358")) return 40;
if(p.equals("PP22416"))
 if(t <= 3.593) return 50;
 if(t > 3.593)
  if(we <= 3634) return 40;
  if(we > 3634)
   if(w <= 263.5) return 60;
   if(w > 263.5) return 50;
if(p.equals("PP22417"))
 if(w <= 527)
  if(we <= 6004) return 40;
  if(we > 6004) return 50;
 if(w > 527)
  if(we <= 6004) return 30;
  if(we > 6004)
   if(we <= 6555)
    if(w <= 528) return 40;
    if(w > 528) return 50;
   if(we > 6555) return 40;
if(p.equals("PP22424")) return 60;
if(p.equals("PP22425")) return 70;
if(p.equals("PP22442")) return 50;
if(p.equals("PP22464")) return 60;
if(p.equals("PP22471")) return 40;
if(p.equals("PP22473")) return 50;
if(p.equals("PP22474")) return 50;
if(p.equals("PP22481"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 30;
if(p.equals("PP22482"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 30;
if(p.equals("PP22487")) return 40;
if(p.equals("PP22488")) return 40;
if(p.equals("PP22489")) return 40;
if(p.equals("PP22498"))
 if(w <= 364)
  if(t <= 3.593)
   if(we <= 4582) return 40;
   if(we > 4582) return 30;
  if(t > 3.593)
   if(w <= 307) return 60;
   if(w > 307)
    if(we <= 4433) return 40;
    if(we > 4433) return 60;
 if(w > 364) return 50;
if(p.equals("PP22503"))
 if(w <= 246) return 40;
 if(w > 246) return 50;
if(p.equals("PP22504")) return 50;
if(p.equals("PP22505"))
 if(we <= 3522) return 40;
 if(we > 3522)
  if(w <= 368)
   if(t <= 5.2) return 50;
   if(t > 5.2) return 60;
  if(w > 368)
   if(we <= 4605) return 40;
   if(we > 4605) return 50;
if(p.equals("PP22507")) return 40;
if(p.equals("PP22508"))
 if(t <= 3.593) return 60;
 if(t > 3.593) return 30;
if(p.equals("PP22510"))
 if(t <= 3.017) return 40;
 if(t > 3.017) return 30;
if(p.equals("PP22513")) return 50;
if(p.equals("PP22514"))
 if(w <= 405)
  if(we <= 5856.5) return 40;
  if(we > 5856.5) return 60;
 if(w > 405)
  if(t <= 2.8) return 50;
  if(t > 2.8) return 30;
if(p.equals("PP22515"))
 if(t <= 2.8) return 50;
 if(t > 2.8)
  if(we <= 3796) return 40;
  if(we > 3796) return 30;
if(p.equals("PP22520")) return 40;
if(p.equals("PP22521")) return 50;
if(p.equals("PP22530"))
 if(t <= 4.2) return 50;
 if(t > 4.2) return 40;
if(p.equals("PP22531"))
 if(we <= 4630) return 40;
 if(we > 4630)
  if(t <= 3.2) return 50;
  if(t > 3.2)
   if(w <= 535) return 60;
   if(w > 535) return 40;
if(p.equals("PP22534"))
 if(t <= 4.7)
  if(we <= 2687) return 40;
  if(we > 2687) return 60;
 if(t > 4.7) return 50;
if(p.equals("PP22535"))
 if(t <= 4.8)
  if(w <= 288)
   if(w <= 239)
    if(we <= 2616) return 40;
    if(we > 2616) return 60;
   if(w > 239) return 40;
  if(w > 288) return 60;
 if(t > 4.8) return 50;
if(p.equals("PP22546")) return 60;
if(p.equals("PP22550")) return 40;
if(p.equals("PP22556")) return 30;
if(p.equals("PP22558")) return 50;
if(p.equals("PP22563")) return 40;
if(p.equals("PP22569")) return 50;
if(p.equals("PP22570")) return 60;
if(p.equals("PP22573")) return 40;
if(p.equals("PP22574")) return 50;
if(p.equals("PP22576")) return 60;
if(p.equals("PP22582")) return 60;
if(p.equals("PP22586")) return 60;
if(p.equals("PP22595")) return 70;
if(p.equals("PP22600")) return 60;
if(p.equals("PP22601")) return 60;
if(p.equals("WS11007")) return 60;
if(p.equals("WS11039")) return 50;
if(p.equals("WS11165")) return 70;
return 70.0;
}
}
