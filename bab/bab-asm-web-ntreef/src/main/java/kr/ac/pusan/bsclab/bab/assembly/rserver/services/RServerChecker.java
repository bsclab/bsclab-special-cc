package kr.ac.pusan.bsclab.bab.assembly.rserver.services;

/**
 * Class for checking RStudio Server and Rserve is running or not
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */

import org.rosuda.REngine.Rserve.RConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RServerChecker {
	private static final Logger log = LoggerFactory.getLogger(RServerChecker.class);

	
	/**
	 * check Rserver running or not by using IP localhost and default port (6311)
	 * @return boolean, true is running otherwise false
	 */
	public static boolean isRserveRunning() {
		try {
			RConnection c = new RConnection();
			System.out.println("Rserve is running.");
			c.close();
			return true;
		} catch (Throwable  e) {
			System.out.println("First connect try failed with: " + e.getMessage());
		} 
		return false;
		
	}
	/**
	 * @param ip address of RStudio server
	 * @param port of Rserve (default 6331)
	 * @return boolean, true is running otherwise false
	 */
	public static boolean isRserveRunning(String ip, int port) {
		try {
			RConnection c = new RConnection(ip, port);
			log.info("Rserve is running.");
			c.close();
			return true;
		} catch (Throwable  e) {
			System.out.println("R-Serve demon is not running, with error : " + e.getMessage());
		} 
		return false;
		
	}
}
