package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.dt;

public class DT_CR1 implements DT_base {
	
@Override
public double getDuration(double t, double w, double we, String p) {
if(p.equals("PP11008"))
 if(w <= 310)
  if(we <= 4494)
   if(we <= 2581)
    if(w <= 223) return 12.5;
    if(w > 223) return 10;
   if(we > 2581)
    if(w <= 308) return 13.5;
    if(w > 308) return 22.5;
  if(we > 4494)
   if(we <= 4607)
    if(we <= 4544) return 15;
    if(we > 4544) return 29.25;
   if(we > 4607)
    if(we <= 4752) return 17;
    if(we > 4752)
     if(we <= 4813) return 19.5;
     if(we > 4813) return 29.5;
 if(w > 310)
  if(t <= 2.94) return 18;
  if(t > 2.94)
   if(w <= 396)
    if(w <= 312) return 27.5;
    if(w > 312) return 17;
   if(w > 396) return 22;
if(p.equals("PP11009"))
 if(w <= 346)
  if(t <= 2.4)
   if(t <= 1.82) return 27;
   if(t > 1.82) return 16;
  if(t > 2.4)
   if(t <= 3.01) return 27.25;
   if(t > 3.01) return 13.5;
 if(w > 346)
  if(t <= 2.75)
   if(w <= 358) return 18;
   if(w > 358) return 21;
  if(t > 2.75)
   if(t <= 3.05) return 24;
   if(t > 3.05) return 25;
if(p.equals("PP11010"))
 if(we <= 5267)
  if(we <= 3564)
   if(w <= 240)
    if(we <= 3383)
     if(we <= 3250) return 24;
     if(we > 3250) return 26;
    if(we > 3383) return 40;
   if(w > 240) return 20;
  if(we > 3564)
   if(w <= 318)
    if(w <= 268)
     if(w <= 257) return 31.6666666666667;
     if(w > 257) return 25;
    if(w > 268)
     if(w <= 298) return 35;
     if(w > 298)
      if(t <= 3.515) return 15;
      if(t > 3.515) return 52.5;
   if(w > 318)
    if(t <= 3.715)
     if(we <= 4765)
      if(t <= 2.33) return 22.5;
      if(t > 2.33) return 24;
     if(we > 4765) return 30;
    if(t > 3.715)
     if(t <= 4.02) return 24;
     if(t > 4.02) return 17;
 if(we > 5267)
  if(w <= 389)
   if(t <= 3.26)
    if(t <= 2.83) return 31;
    if(t > 2.83) return 22;
   if(t > 3.26)
    if(t <= 3.3) return 38;
    if(t > 3.3) return 25;
  if(w > 389)
   if(t <= 3.64)
    if(w <= 422)
     if(t <= 2.75) return 29;
     if(t > 2.75) return 39;
    if(w > 422) return 35;
   if(t > 3.64) return 19;
if(p.equals("PP11012")) return 25;
if(p.equals("PP11013")) return 22.5;
if(p.equals("PP11015")) return 28.5;
if(p.equals("PP11017"))
 if(t <= 2.63) return 41;
 if(t > 2.63) return 15;
if(p.equals("PP11021"))
 if(t <= 3.26)
  if(we <= 4057)
   if(t <= 2.49) return 24;
   if(t > 2.49)
    if(t <= 2.75) return 32;
    if(t > 2.75) return 52;
  if(we > 4057) return 35;
 if(t > 3.26)
  if(t <= 4.02)
   if(t <= 4.01)
    if(w <= 271)
     if(t <= 3.915) return 15;
     if(t > 3.915) return 32.5;
    if(w > 271)
     if(w <= 337) return 25;
     if(w > 337) return 30;
   if(t > 4.01)
    if(w <= 349)
     if(w <= 343) return 39;
     if(w > 343) return 28.5;
    if(w > 349)
     if(w <= 388) return 196;
     if(w > 388) return 21;
  if(t > 4.02)
   if(w <= 375)
    if(t <= 4.18) return 36;
    if(t > 4.18)
     if(w <= 306) return 15;
     if(w > 306) return 29;
   if(w > 375)
    if(t <= 4.04) return 50;
    if(t > 4.04) return 33;
if(p.equals("PP11029"))
 if(t <= 2.94)
  if(w <= 392) return 29;
  if(w > 392)
   if(t <= 2.595) return 38;
   if(t > 2.595) return 105;
 if(t > 2.94)
  if(w <= 294)
   if(w <= 255) return 16.5;
   if(w > 255) return 18;
  if(w > 294) return 45;
if(p.equals("PP11036"))
 if(t <= 1.48)
  if(t <= 1.27) return 38;
  if(t > 1.27) return 44.6666666666667;
 if(t > 1.48)
  if(t <= 1.9)
   if(t <= 1.52) return 34;
   if(t > 1.52) return 39;
  if(t > 1.9) return 25;
if(p.equals("PP11037"))
 if(t <= 1.02)
  if(t <= 0.91)
   if(we <= 4645)
    if(we <= 4612) return 40;
    if(we > 4612) return 39;
   if(we > 4645)
    if(we <= 4661) return 56;
    if(we > 4661)
     if(we <= 4686) return 119;
     if(we > 4686) return 60.5;
  if(t > 0.91)
   if(w <= 367) return 48.3333333333333;
   if(w > 367) return 38.3333333333333;
 if(t > 1.02)
  if(t <= 1.14)
   if(w <= 319)
    if(w <= 263)
     if(w <= 262) return 29;
     if(w > 262) return 36.5;
    if(w > 263) return 115;
   if(w > 319)
    if(t <= 1.07) return 36;
    if(t > 1.07)
     if(w <= 320) return 44.5;
     if(w > 320)
      if(w <= 348) return 46;
      if(w > 348) return 18;
  if(t > 1.14)
   if(w <= 236)
    if(w <= 210.09)
     if(w <= 209) return 29;
     if(w > 209) return 42.3333333333333;
    if(w > 210.09)
     if(w <= 223) return 28;
     if(w > 223) return 35;
   if(w > 236) return 36;
if(p.equals("PP11038"))
 if(t <= 1.82) return 30;
 if(t > 1.82) return 38;
if(p.equals("PP11039"))
 if(w <= 373)
  if(t <= 1.23)
   if(t <= 0.8) return 40;
   if(t > 0.8) return 45;
  if(t > 1.23)
   if(w <= 326) return 21;
   if(w > 326) return 30;
 if(w > 373)
  if(w <= 420)
   if(t <= 1.2) return 37;
   if(t > 1.2) return 40.5;
  if(w > 420) return 54;
if(p.equals("PP11041"))
 if(w <= 324) return 29;
 if(w > 324)
  if(w <= 395) return 44;
  if(w > 395) return 35;
if(p.equals("PP11045"))
 if(t <= 1.7) return 32;
 if(t > 1.7) return 25;
if(p.equals("PP11046"))
 if(we <= 4013)
  if(we <= 3764)
   if(t <= 0.95)
    if(we <= 2701)
     if(we <= 2505) return 48;
     if(we > 2505) return 44.5;
    if(we > 2701)
     if(we <= 2755) return 47;
     if(we > 2755) return 45;
   if(t > 0.95) return 36;
  if(we > 3764)
   if(we <= 3933) return 66;
   if(we > 3933)
    if(we <= 3955) return 54;
    if(we > 3955) return 62.75;
 if(we > 4013)
  if(w <= 420)
   if(t <= 0.9)
    if(we <= 4041) return 61.5;
    if(we > 4041) return 55;
   if(t > 0.9) return 24;
  if(w > 420)
   if(we <= 6369)
    if(we <= 5355) return 37.5;
    if(we > 5355) return 40;
   if(we > 6369)
    if(we <= 6798) return 56;
    if(we > 6798) return 35;
if(p.equals("PP11047"))
 if(t <= 2.25) return 35;
 if(t > 2.25) return 21;
if(p.equals("PP11048"))
 if(t <= 1.33)
  if(we <= 4850)
   if(we <= 3543)
    if(we <= 3146) return 23.6666666666667;
    if(we > 3146) return 25;
   if(we > 3543)
    if(w <= 286) return 35;
    if(w > 286)
     if(w <= 388) return 25;
     if(w > 388) return 32;
  if(we > 4850)
   if(w <= 397)
    if(we <= 5688) return 26;
    if(we > 5688) return 22.5;
   if(w > 397)
    if(t <= 1.14) return 25;
    if(t > 1.14) return 22;
 if(t > 1.33)
  if(w <= 335)
   if(t <= 1.38)
    if(w <= 242) return 30;
    if(w > 242) return 26;
   if(t > 1.38) return 38.5;
  if(w > 335)
   if(w <= 387) return 21;
   if(w > 387)
    if(w <= 389) return 28.5;
    if(w > 389) return 20;
if(p.equals("PP11052")) return 25;
if(p.equals("PP11057")) return 18;
if(p.equals("PP11058"))
 if(w <= 326.16)
  if(we <= 4509)
   if(we <= 4070)
    if(w <= 299.17)
     if(t <= 0.76)
      if(t <= 0.71) return 59.5;
      if(t > 0.71)
       if(w <= 280) return 34;
       if(w > 280) return 33;
     if(t > 0.76)
      if(w <= 234)
       if(w <= 218)
        if(w <= 210) return 50;
        if(w > 210) return 46;
       if(w > 218) return 63;
      if(w > 234)
       if(w <= 255)
        if(we <= 2930) return 64;
        if(we > 2930) return 50.5;
       if(w > 255) return 65;
    if(w > 299.17)
     if(we <= 3948)
      if(t <= 0.75) return 58;
      if(t > 0.75) return 45;
     if(we > 3948)
      if(w <= 307)
       if(we <= 4004) return 43;
       if(we > 4004) return 48;
      if(w > 307) return 50;
   if(we > 4070)
    if(we <= 4352)
     if(we <= 4253)
      if(we <= 4177)
       if(we <= 4096) return 52;
       if(we > 4096)
        if(we <= 4117) return 195;
        if(we > 4117) return 48;
      if(we > 4177)
       if(we <= 4242)
        if(we <= 4235) return 47.6666666666667;
        if(we > 4235) return 45;
       if(we > 4242)
        if(we <= 4247) return 55;
        if(we > 4247) return 60;
     if(we > 4253)
      if(w <= 307)
       if(we <= 4287) return 48;
       if(we > 4287) return 45;
      if(w > 307) return 44;
    if(we > 4352)
     if(w <= 300)
      if(we <= 4446)
       if(we <= 4396) return 35;
       if(we > 4396)
        if(we <= 4417)
         if(we <= 4405) return 48;
         if(we > 4405) return 53;
        if(we > 4417) return 47;
      if(we > 4446)
       if(w <= 299.17)
        if(we <= 4489) return 45;
        if(we > 4489) return 51;
       if(w > 299.17)
        if(we <= 4479) return 70;
        if(we > 4479)
         if(we <= 4497) return 35;
         if(we > 4497) return 50.5;
     if(w > 300)
      if(we <= 4417)
       if(we <= 4382) return 52;
       if(we > 4382) return 40;
      if(we > 4417)
       if(w <= 315) return 45;
       if(w > 315) return 63;
  if(we > 4509)
   if(w <= 300)
    if(we <= 4580)
     if(we <= 4542)
      if(we <= 4528) return 45;
      if(we > 4528)
       if(we <= 4537) return 47;
       if(we > 4537) return 48;
     if(we > 4542) return 50;
    if(we > 4580)
     if(we <= 4623)
      if(we <= 4602)
       if(we <= 4590) return 52;
       if(we > 4590)
        if(we <= 4595) return 50;
        if(we > 4595) return 53.5;
      if(we > 4602)
       if(w <= 299.17) return 50;
       if(w > 299.17) return 60;
     if(we > 4623)
      if(we <= 4638)
       if(we <= 4626) return 180;
       if(we > 4626) return 51;
      if(we > 4638)
       if(we <= 4673) return 45;
       if(we > 4673) return 47;
   if(w > 300)
    if(we <= 4789)
     if(we <= 4742)
      if(we <= 4590)
       if(we <= 4532)
        if(w <= 307) return 51;
        if(w > 307) return 42;
       if(we > 4532) return 48;
      if(we > 4590)
       if(we <= 4719)
        if(w <= 307) return 50;
        if(w > 307) return 56;
       if(we > 4719)
        if(w <= 314)
         if(we <= 4730) return 51;
         if(we > 4730) return 50;
        if(w > 314) return 48;
     if(we > 4742)
      if(w <= 314.19)
       if(we <= 4762) return 48;
       if(we > 4762)
        if(we <= 4777) return 47;
        if(we > 4777) return 63;
      if(w > 314.19)
       if(w <= 317) return 61;
       if(w > 317) return 60;
    if(we > 4789)
     if(w <= 314)
      if(we <= 4809) return 45;
      if(we > 4809)
       if(we <= 4838) return 50;
       if(we > 4838) return 49;
     if(w > 314)
      if(t <= 0.795) return 35;
      if(t > 0.795)
       if(w <= 316) return 120;
       if(w > 316) return 127.5;
 if(w > 326.16)
  if(w <= 337)
   if(we <= 5010)
    if(we <= 4881)
     if(t <= 0.795)
      if(we <= 4608)
       if(we <= 4449)
        if(we <= 4352) return 109;
        if(we > 4352) return 55;
       if(we > 4449) return 50;
      if(we > 4608)
       if(we <= 4821) return 60;
       if(we > 4821)
        if(we <= 4874) return 50;
        if(we > 4874) return 59;
     if(t > 0.795)
      if(w <= 331.21)
       if(we <= 4278)
        if(w <= 329) return 51;
        if(w > 329)
         if(we <= 4111) return 45;
         if(we > 4111)
          if(we <= 4205) return 42;
          if(we > 4205) return 60;
       if(we > 4278) return 53;
      if(w > 331.21)
       if(we <= 4591)
        if(we <= 4261)
         if(we <= 4158)
          if(we <= 4108) return 45;
          if(we > 4108) return 47;
         if(we > 4158)
          if(we <= 4199) return 48;
          if(we > 4199) return 103;
        if(we > 4261)
         if(we <= 4439) return 52;
         if(we > 4439)
          if(we <= 4538)
           if(we <= 4508) return 46;
           if(we > 4508) return 51;
          if(we > 4538)
           if(w <= 333) return 45;
           if(w > 333) return 46;
       if(we > 4591)
        if(we <= 4799) return 46;
        if(we > 4799)
         if(we <= 4862) return 48;
         if(we > 4862)
          if(we <= 4874) return 44;
          if(we > 4874) return 43;
    if(we > 4881)
     if(w <= 331)
      if(t <= 0.795)
       if(we <= 4973)
        if(we <= 4915)
         if(we <= 4900) return 45;
         if(we > 4900)
          if(we <= 4904) return 65;
          if(we > 4904) return 100;
        if(we > 4915)
         if(we <= 4945)
          if(we <= 4933) return 47;
          if(we > 4933) return 46;
         if(we > 4945) return 52;
       if(we > 4973)
        if(we <= 4990) return 170;
        if(we > 4990) return 50;
      if(t > 0.795)
       if(we <= 4926)
        if(we <= 4900)
         if(we <= 4894) return 48;
         if(we > 4894) return 46;
        if(we > 4900)
         if(we <= 4910) return 41;
         if(we > 4910) return 45;
       if(we > 4926)
        if(we <= 4964) return 50;
        if(we > 4964)
         if(we <= 4986) return 48;
         if(we > 4986)
          if(we <= 4992) return 100;
          if(we > 4992) return 50;
     if(w > 331)
      if(we <= 4986)
       if(w <= 332.11)
        if(we <= 4942)
         if(we <= 4915) return 51;
         if(we > 4915)
          if(we <= 4925) return 53;
          if(we > 4925) return 55;
        if(we > 4942) return 64;
       if(w > 332.11)
        if(we <= 4941) return 43;
        if(we > 4941) return 42;
      if(we > 4986)
       if(we <= 5006)
        if(we <= 4996) return 53;
        if(we > 4996) return 51;
       if(we > 5006)
        if(we <= 5007) return 46;
        if(we > 5007) return 50;
   if(we > 5010)
    if(w <= 331.17)
     if(we <= 5063)
      if(t <= 0.795)
       if(we <= 5025) return 49;
       if(we > 5025) return 41;
      if(t > 0.795)
       if(we <= 5039) return 46;
       if(we > 5039)
        if(we <= 5051) return 55;
        if(we > 5051)
         if(we <= 5061) return 52.5;
         if(we > 5061) return 49;
     if(we > 5063)
      if(w <= 330.23)
       if(t <= 0.795) return 47;
       if(t > 0.795) return 45;
      if(w > 330.23)
       if(we <= 5076) return 45;
       if(we > 5076) return 65;
    if(w > 331.17)
     if(we <= 5106)
      if(we <= 5068)
       if(we <= 5044)
        if(we <= 5038)
         if(we <= 5025) return 50;
         if(we > 5025) return 48;
        if(we > 5038)
         if(we <= 5039) return 40;
         if(we > 5039) return 43;
       if(we > 5044)
        if(w <= 333)
         if(we <= 5051) return 51;
         if(we > 5051) return 71;
        if(w > 333) return 45;
      if(we > 5068)
       if(w <= 332) return 50;
       if(w > 332)
        if(w <= 334) return 44;
        if(w > 334) return 140;
     if(we > 5106)
      if(we <= 5140)
       if(we <= 5123)
        if(w <= 333) return 45;
        if(w > 333) return 53;
       if(we > 5123)
        if(we <= 5133) return 52;
        if(we > 5133) return 47;
      if(we > 5140) return 48;
  if(w > 337)
   if(w <= 368)
    if(we <= 5181)
     if(we <= 4884)
      if(w <= 345)
       if(we <= 4659)
        if(we <= 4278) return 46;
        if(we > 4278) return 44;
       if(we > 4659) return 50;
      if(w > 345)
       if(w <= 346)
        if(we <= 4679)
         if(we <= 4526) return 56;
         if(we > 4526) return 66;
        if(we > 4679)
         if(we <= 4842) return 49;
         if(we > 4842) return 185;
       if(w > 346)
        if(w <= 356) return 50;
        if(w > 356) return 31;
     if(we > 4884)
      if(we <= 5085)
       if(w <= 345)
        if(we <= 5001)
         if(we <= 4907) return 40;
         if(we > 4907) return 42.5;
        if(we > 5001)
         if(w <= 344.19) return 49;
         if(w > 344.19) return 79;
       if(w > 345)
        if(we <= 4925) return 48;
        if(we > 4925)
         if(w <= 347)
          if(we <= 5009) return 47.5;
          if(we > 5009) return 45;
         if(w > 347)
          if(w <= 361) return 46;
          if(w > 361) return 51;
      if(we > 5085)
       if(w <= 344.19) return 47.5;
       if(w > 344.19) return 50;
    if(we > 5181)
     if(we <= 5239)
      if(we <= 5211)
       if(we <= 5196)
        if(we <= 5189) return 48;
        if(we > 5189) return 112;
       if(we > 5196)
        if(we <= 5200) return 250;
        if(we > 5200) return 51;
      if(we > 5211)
       if(w <= 345)
        if(we <= 5230) return 61;
        if(we > 5230) return 50;
       if(w > 345) return 41;
     if(we > 5239)
      if(w <= 345)
       if(we <= 5277)
        if(we <= 5257)
         if(we <= 5242) return 46.5;
         if(we > 5242) return 58;
        if(we > 5257)
         if(we <= 5269) return 120;
         if(we > 5269) return 52;
       if(we > 5277)
        if(we <= 5312)
         if(we <= 5293) return 115;
         if(we > 5293) return 145;
        if(we > 5312)
         if(we <= 5343) return 43;
         if(we > 5343) return 47;
      if(w > 345)
       if(we <= 5333) return 63;
       if(we > 5333)
        if(w <= 362) return 52;
        if(w > 362)
         if(we <= 5437) return 44;
         if(we > 5437) return 45;
   if(w > 368)
    if(we <= 5386)
     if(w <= 381)
      if(we <= 5058)
       if(w <= 374)
        if(we <= 4813) return 50;
        if(we > 4813)
         if(we <= 4952) return 46;
         if(we > 4952) return 49;
       if(w > 374)
        if(we <= 4814) return 41;
        if(we > 4814) return 48;
      if(we > 5058)
       if(w <= 375.21)
        if(w <= 373) return 45;
        if(w > 373) return 210;
       if(w > 375.21)
        if(we <= 5196) return 40;
        if(we > 5196) return 43;
     if(w > 381)
      if(w <= 398)
       if(we <= 5005)
        if(we <= 4858) return 41;
        if(we > 4858) return 68;
       if(we > 5005)
        if(we <= 5232) return 51;
        if(we > 5232) return 49;
      if(w > 398)
       if(w <= 417) return 74;
       if(w > 417) return 79;
    if(we > 5386)
     if(we <= 5674)
      if(we <= 5549)
       if(we <= 5521)
        if(we <= 5449) return 59;
        if(we > 5449) return 47;
       if(we > 5521) return 42;
      if(we > 5549)
       if(w <= 373)
        if(w <= 371.23) return 47;
        if(w > 371.23) return 50;
       if(w > 373)
        if(we <= 5635) return 48;
        if(we > 5635)
         if(w <= 376) return 47;
         if(w > 376) return 50;
     if(we > 5674)
      if(w <= 381)
       if(we <= 5722) return 48;
       if(we > 5722)
        if(w <= 375)
         if(we <= 5756) return 100;
         if(we > 5756) return 48;
        if(w > 375) return 50;
      if(w > 381)
       if(w <= 392)
        if(w <= 387) return 40;
        if(w > 387)
         if(we <= 5835) return 49;
         if(we > 5835) return 50;
       if(w > 392)
        if(w <= 397)
         if(we <= 5855) return 60;
         if(we > 5855) return 52;
        if(w > 397) return 41;
if(p.equals("PP11059")) return 30;
if(p.equals("PP11060")) return 19;
if(p.equals("PP11081"))
 if(t <= 1.34)
  if(t <= 1.03)
   if(w <= 265)
    if(t <= 0.75) return 30;
    if(t > 0.75) return 35;
   if(w > 265)
    if(w <= 390)
     if(w <= 357) return 37;
     if(w > 357) return 25;
    if(w > 390) return 50;
  if(t > 1.03)
   if(w <= 253)
    if(t <= 1.2)
     if(we <= 2835) return 50;
     if(we > 2835)
      if(w <= 232) return 30;
      if(w > 232) return 25;
    if(t > 1.2) return 37;
   if(w > 253)
    if(w <= 420)
     if(we <= 3200)
      if(w <= 255) return 20;
      if(w > 255) return 33;
     if(we > 3200)
      if(w <= 390) return 28;
      if(w > 390) return 20;
    if(w > 420)
     if(w <= 431) return 28;
     if(w > 431) return 25;
 if(t > 1.34)
  if(w <= 357)
   if(t <= 1.4) return 22;
   if(t > 1.4) return 25;
  if(w > 357)
   if(w <= 420) return 24;
   if(w > 420)
    if(we <= 5156) return 25;
    if(we > 5156) return 20;
if(p.equals("PP11294"))
 if(t <= 2.19)
  if(w <= 224) return 16;
  if(w > 224) return 24;
 if(t > 2.19) return 20;
if(p.equals("PP11351"))
 if(t <= 2) return 30.25;
 if(t > 2) return 18;
if(p.equals("PP11671"))
 if(we <= 4686) return 30;
 if(we > 4686) return 25;
if(p.equals("PP11686")) return 44;
if(p.equals("PP11884"))
 if(t <= 1.52)
  if(t <= 1.38) return 25.5;
  if(t > 1.38)
   if(we <= 5580) return 47.5;
   if(we > 5580) return 46;
 if(t > 1.52)
  if(t <= 1.82) return 30;
  if(t > 1.82) return 37;
if(p.equals("PP11891"))
 if(w <= 348) return 25;
 if(w > 348)
  if(w <= 359) return 30.5;
  if(w > 359) return 34;
if(p.equals("PP11892")) return 15;
if(p.equals("PP11984"))
 if(t <= 1.44)
  if(we <= 4968) return 57;
  if(we > 4968) return 43;
 if(t > 1.44)
  if(t <= 1.62) return 49;
  if(t > 1.62)
   if(t <= 1.795) return 115;
   if(t > 1.795) return 113;
if(p.equals("PP11985")) return 41.5;
if(p.equals("PP12011")) return 29;
if(p.equals("PP12026"))
 if(w <= 377) return 33;
 if(w > 377) return 20;
if(p.equals("PP12035"))
 if(w <= 307)
  if(t <= 1.11) return 36;
  if(t > 1.11) return 28;
 if(w > 307)
  if(w <= 335) return 32;
  if(w > 335) return 24;
if(p.equals("PP12037"))
 if(we <= 6053) return 30;
 if(we > 6053) return 50;
if(p.equals("PP12040")) return 26.8;
if(p.equals("PP12069")) return 20;
if(p.equals("PP12073")) return 40;
if(p.equals("PP12129")) return 45;
if(p.equals("PP12130"))
 if(we <= 4474)
  if(we <= 3621) return 15;
  if(we > 3621) return 17.5;
 if(we > 4474)
  if(we <= 4765) return 27.5;
  if(we > 4765)
   if(we <= 4794) return 19;
   if(we > 4794) return 18.5;
if(p.equals("PP12152")) return 15;
if(p.equals("PP12158"))
 if(w <= 349)
  if(we <= 2728) return 50;
  if(we > 2728) return 33;
 if(w > 349)
  if(we <= 3241) return 32;
  if(we > 3241) return 30.5;
if(p.equals("PP12164"))
 if(we <= 4375) return 15;
 if(we > 4375) return 16.6666666666667;
if(p.equals("PP12178"))
 if(we <= 5273)
  if(t <= 1.52) return 30;
  if(t > 1.52)
   if(w <= 356)
    if(we <= 5164) return 43;
    if(we > 5164) return 49.3333333333333;
   if(w > 356) return 45;
 if(we > 5273)
  if(we <= 5372)
   if(we <= 5337) return 42.6666666666667;
   if(we > 5337) return 39;
  if(we > 5372)
   if(we <= 5434) return 35;
   if(we > 5434) return 66.6666666666667;
if(p.equals("PP12226")) return 26;
if(p.equals("PP12239"))
 if(we <= 3593) return 47;
 if(we > 3593) return 66.6666666666667;
if(p.equals("PP12254")) return 20;
if(p.equals("PP12269"))
 if(w <= 266) return 30;
 if(w > 266) return 35;
if(p.equals("PP12289"))
 if(t <= 2.33)
  if(t <= 1.5) return 36;
  if(t > 1.5) return 20;
 if(t > 2.33) return 25;
if(p.equals("PP12315")) return 54;
if(p.equals("PP12394"))
 if(we <= 3208)
  if(w <= 360)
   if(t <= 2.19) return 17;
   if(t > 2.19) return 23;
  if(w > 360) return 15;
 if(we > 3208)
  if(w <= 290) return 55;
  if(w > 290) return 30;
if(p.equals("PP12413"))
 if(t <= 0.81) return 32;
 if(t > 0.81) return 25;
if(p.equals("PP12524")) return 32;
if(p.equals("PP12575")) return 32;
if(p.equals("PP21028")) return 37.5;
if(p.equals("PP21039"))
 if(w <= 247)
  if(we <= 3657) return 36;
  if(we > 3657) return 35;
 if(w > 247)
  if(t <= 0.93)
   if(we <= 3543) return 31;
   if(we > 3543)
    if(we <= 3605) return 36.3333333333333;
    if(we > 3605) return 37;
  if(t > 0.93)
   if(t <= 1.26)
    if(we <= 3709) return 50;
    if(we > 3709) return 64.5;
   if(t > 1.26) return 46.2;
if(p.equals("PP21045"))
 if(t <= 2.75)
  if(w <= 334) return 21;
  if(w > 334) return 18.6666666666667;
 if(t > 2.75)
  if(w <= 311) return 21.5;
  if(w > 311) return 20;
if(p.equals("PP21082"))
 if(w <= 309) return 14.5;
 if(w > 309) return 20;
if(p.equals("PP21114")) return 21;
if(p.equals("PP21429")) return 35;
if(p.equals("PP21524")) return 20;
if(p.equals("PP21529"))
 if(t <= 0.93)
  if(we <= 3408) return 45;
  if(we > 3408) return 29;
 if(t > 0.93)
  if(we <= 3687) return 58;
  if(we > 3687) return 46;
if(p.equals("PP21599")) return 20;
if(p.equals("PP21601"))
 if(t <= 3.54) return 20;
 if(t > 3.54)
  if(t <= 4.18) return 16;
  if(t > 4.18) return 15;
if(p.equals("PP21632")) return 43;
if(p.equals("PP21647")) return 15;
if(p.equals("PP21769"))
 if(w <= 284) return 14;
 if(w > 284) return 25;
if(p.equals("PP21773")) return 32.5;
if(p.equals("PP21777"))
 if(we <= 3543)
  if(we <= 3339) return 22.8333333333333;
  if(we > 3339)
   if(we <= 3492) return 25;
   if(we > 3492) return 26.3;
 if(we > 3543)
  if(we <= 3644) return 24.0666666666667;
  if(we > 3644)
   if(we <= 3712) return 25.5666666666667;
   if(we > 3712) return 24.2166666666667;
if(p.equals("PP21779"))
 if(w <= 241)
  if(t <= 2.53)
   if(we <= 2101) return 16;
   if(we > 2101)
    if(we <= 2407) return 15;
    if(we > 2407) return 20;
  if(t > 2.53)
   if(we <= 2407) return 14;
   if(we > 2407) return 21;
 if(w > 241)
  if(t <= 2.14) return 17;
  if(t > 2.14) return 20;
if(p.equals("PP21789")) return 36.5;
if(p.equals("PP22016")) return 25;
if(p.equals("PP22107"))
 if(w <= 303)
  if(t <= 0.81) return 40;
  if(t > 0.81) return 20;
 if(w > 303) return 25;
if(p.equals("PP22234")) return 22.5;
if(p.equals("PP22259")) return 28;
if(p.equals("PP22279")) return 28;
if(p.equals("PP22302")) return 15;
if(p.equals("PP22325")) return 30;
if(p.equals("PP22326")) return 20;
if(p.equals("PP22480")) return 55;
if(p.equals("WS11165")) return 16.5;
return 16.5;
}
}
