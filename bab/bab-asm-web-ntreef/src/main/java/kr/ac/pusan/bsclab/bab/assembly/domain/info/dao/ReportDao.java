package kr.ac.pusan.bsclab.bab.assembly.domain.info.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import kr.ac.pusan.bsclab.bab.assembly.domain.info.Report;
import kr.ac.pusan.bsclab.bab.assembly.domain.info.ReportType;

@Repository
public class ReportDao {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public Report findOneByJobNo(int jobId) {
		
		String sql = "select * from report where jobNo=?";
		return jdbcTemplate.queryForObject(sql, new ReportMapper());
		
	}
	
	public int save(Report report) {
		GeneratedKeyHolder gkh = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			
			String sql = "INSERT INTO report(jobNo, type, report)"
					+ "VALUES(?, ?, ?)";
			
			@Override
			public PreparedStatement createPreparedStatement(Connection conn)
					throws SQLException {
				PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, report.getJobNo());
				ps.setObject(2, report.getReportType().name());
				ps.setObject(3, report.getReport());
				return ps;
			}
		}, gkh);
		
		return gkh.getKey().intValue();
	}
}

class ReportMapper implements RowMapper<Report> {

	@Override
	public Report mapRow(ResultSet rs, int rowNum) throws SQLException {
		Report report = new Report();
		report.setNo(rs.getInt("no"));
		report.setJobNo(rs.getInt("jobNo"));
		report.setReportType(ReportType.valueOf(rs.getString("type")));
		report.setReport(rs.getString("report"));
		return report;
	}
	
	
}
