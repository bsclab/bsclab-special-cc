package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Input {

	@JsonInclude(Include.NON_DEFAULT)
	Integer size;
	
	String editable;
	
	String name;
	
	Integer id;
	
	Boolean sensitive;
	
	String overrides;
	
	String type;
	
	@JsonInclude(Include.NON_EMPTY)
	Object value;
	
	@JsonInclude(Include.NON_EMPTY)
	Object values;
	
	
	

	/**
	 * 
	 */
	public Input() {
	}

	/**
	 * @param size
	 * @param editable
	 * @param name
	 * @param id
	 * @param sensitive
	 * @param overrides
	 * @param type
	 * @param value
	 * @param values
	 */
	public Input(Integer size, String editable, String name, Integer id, Boolean sensitive, String overrides,
			String type, Object value, Object values) {
		this.size = size;
		this.editable = editable;
		this.name = name;
		this.id = id;
		this.sensitive = sensitive;
		this.overrides = overrides;
		this.type = type;
		this.value = value;
		this.values = values;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public String getEditable() {
		return editable;
	}

	public void setEditable(String editable) {
		this.editable = editable;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getSensitive() {
		return sensitive;
	}

	public void setSensitive(Boolean sensitive) {
		this.sensitive = sensitive;
	}

	public String getOverrides() {
		return overrides;
	}

	public void setOverrides(String overrides) {
		this.overrides = overrides;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Object getValues() {
		return values;
	}

	public void setValues(Object values) {
		this.values = values;
	}
	
	
}