package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

public interface ANN_base {
	public void setWeight(MultiLayerNetwork net);
	public double getMaxThick();
	public double getMaxWidth();
	public double getMaxWeight();
	public double getMinThick();
	public double getMinWidth();
	public double getMinWeight();
	public double getOutput(double thick, double width, double weight);
}
