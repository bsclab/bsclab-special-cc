package kr.ac.pusan.bsclab.bab.assembly.domain.yarn;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("state")
public enum State {
	
	NEW
	, NEW_SAVING
	, SUBMITTED
	, ACCEPTED
	, RUNNING
	, FINISHED
	, FAILED
	, KILLED
}
