package kr.ac.pusan.bsclab.bab.assembly.domain.info;

public enum ReportType {
	
	UNEXPECTED_EXCEPTION
	, OVERLAP_DATA_OR_NULL_VALUE
	, FILE_EXISTS
	, YARN_FAIL
	, FILE_IS_EMPTY

}
