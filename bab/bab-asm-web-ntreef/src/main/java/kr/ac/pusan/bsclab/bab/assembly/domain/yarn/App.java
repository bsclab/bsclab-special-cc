package kr.ac.pusan.bsclab.bab.assembly.domain.yarn;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("app")
public class App {

	String id; // The application id
	String user; // The user who started the application
	String name; // The application name
	String applicationType; // The application type
	String applicationTags;
	String queue; // The queue the application was submitted to
	String state; // The application state according to the ResourceManager -
					// valid values are members of the YarnApplicationState
					// enum: NEW, NEW_SAVING, SUBMITTED, ACCEPTED, RUNNING,
					// FINISHED, FAILED, KILLED
	FinalStatus finalStatus; // The final status of the application if finished
								// - reported by the application itself - valid
								// values are: UNDEFINED, SUCCEEDED, FAILED,
								// KILLED
	float progress; // The progress of the application as a percent
	String trackingUI; // Where the tracking url is currently pointing - History
						// (for history server) or ApplicationMaster
	String trackingUrl; // The web URL that can be used to track the application
	String diagnostics; // Detailed diagnostics information
	long clusterId; // The cluster id
	long startedTime; // The time in which application started (in ms since
						// epoch)
	long finishedTime; // The time in which the application finished (in ms
						// since epoch)
	long elapsedTime; // The elapsed time since the application started (in ms)
	String amContainerLogs; // The URL of the application master container logs
	String amHostHttpAddress; // The nodes http address of the application
								// master
	int allocatedMB; // The sum of memory in MB allocated to the applications
						// running containers
	int allocatedVCores; // The sum of virtual cores allocated to the
							// applications running containers
	int runningContainers; // The number of containers currently running for the
							// application
	long memorySeconds; // The amount of memory the application has allocated
						// (megabyte-seconds)
	long vcoreSeconds; // The amount of CPU resources the application has
						// allocated (virtual core-seconds)

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getApplicationTags() {
		return applicationTags;
	}

	public void setApplicationTags(String applicationTags) {
		this.applicationTags = applicationTags;
	}

	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public FinalStatus getFinalStatus() {
		return finalStatus;
	}

	public void setFinalStatus(FinalStatus finalStatus) {
		this.finalStatus = finalStatus;
	}

	public float getProgress() {
		return progress;
	}

	public void setProgress(float progress) {
		this.progress = progress;
	}

	public String getTrackingUI() {
		return trackingUI;
	}

	public void setTrackingUI(String trackingUI) {
		this.trackingUI = trackingUI;
	}

	public String getTrackingUrl() {
		return trackingUrl;
	}

	public void setTrackingUrl(String trackingUrl) {
		this.trackingUrl = trackingUrl;
	}

	public String getDiagnostics() {
		return diagnostics;
	}

	public void setDiagnostics(String diagnostics) {
		this.diagnostics = diagnostics;
	}

	public long getClusterId() {
		return clusterId;
	}

	public void setClusterId(long clusterId) {
		this.clusterId = clusterId;
	}

	public long getStartedTime() {
		return startedTime;
	}

	public void setStartedTime(long startedTime) {
		this.startedTime = startedTime;
	}

	public long getFinishedTime() {
		return finishedTime;
	}

	public void setFinishedTime(long finishedTime) {
		this.finishedTime = finishedTime;
	}

	public long getElapsedTime() {
		return elapsedTime;
	}

	public void setElapsedTime(long elapsedTime) {
		this.elapsedTime = elapsedTime;
	}

	public String getAmContainerLogs() {
		return amContainerLogs;
	}

	public void setAmContainerLogs(String amContainerLogs) {
		this.amContainerLogs = amContainerLogs;
	}

	public String getAmHostHttpAddress() {
		return amHostHttpAddress;
	}

	public void setAmHostHttpAddress(String amHostHttpAddress) {
		this.amHostHttpAddress = amHostHttpAddress;
	}

	public int getAllocatedMB() {
		return allocatedMB;
	}

	public void setAllocatedMB(int allocatedMB) {
		this.allocatedMB = allocatedMB;
	}

	public int getAllocatedVCores() {
		return allocatedVCores;
	}

	public void setAllocatedVCores(int allocatedVCores) {
		this.allocatedVCores = allocatedVCores;
	}

	public int getRunningContainers() {
		return runningContainers;
	}

	public void setRunningContainers(int runningContainers) {
		this.runningContainers = runningContainers;
	}

	public long getMemorySeconds() {
		return memorySeconds;
	}

	public void setMemorySeconds(long memorySeconds) {
		this.memorySeconds = memorySeconds;
	}

	public long getVcoreSeconds() {
		return vcoreSeconds;
	}

	public void setVcoreSeconds(long vcoreSeconds) {
		this.vcoreSeconds = vcoreSeconds;
	}

}
