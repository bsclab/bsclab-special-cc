package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.link;

import java.util.Date;

import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.SqoopElement;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.Input;
import kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.input.InputsFactory;

//@Component
public abstract class LinkFactory implements InputsFactory {

	public Link createLink(int id, String name, int connectorId, boolean enabled, String creationUser,
			Date creationDate, String updateUser, Date updateDate, LinkConfigValue[] linkConfigValues) {
		return new Link(linkConfigValues, creationUser, name, creationDate, id, updateDate, enabled, updateUser, connectorId);
	}

	public LinkConfigValue[] createLinkConfigValues(String name, int id, SqoopElement sqoopElement, Input[] inputs) {
		return new LinkConfigValue[]{new LinkConfigValue(inputs, name, id, sqoopElement)};
	}
}
