package kr.ac.pusan.bsclab.bab.assembly.domain.sqoop.job;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("job")
public class Job {
	
	int id;
	
	String name;
	
	@JsonProperty("from-connector-id")
	int fromConnectorId;
	
	@JsonProperty("from-link-id")
	int fromLinkId;
	
	@JsonProperty("to-connector-id")
	int toConnectorId;
	
	@JsonProperty("to-link-id")
	int toLinkId;
	
	@JsonProperty("creation-user")
	String creationUser;
	
	@JsonProperty("creation-date")
	Date creationDate;
	
	@JsonProperty("update-user")
	String updateUser;
	
	@JsonProperty("update-date")
	Date updateDate;
	
	boolean enabled;
	
	@JsonProperty("from-config-values")
	ConfigValue[] fromConfigValues;
	
	@JsonProperty("to-config-values")
	ConfigValue[] toConfigValues;
	
	@JsonProperty("driver-config-values")
	ConfigValue[] driverConfigValues;

	/**
	 * @param id
	 * @param name
	 * @param fromConnectorId
	 * @param fromLinkId
	 * @param toConnectorId
	 * @param toLinkId
	 * @param creationUser
	 * @param creationDate
	 * @param updateUser
	 * @param updateDate
	 * @param enabled
	 * @param fromConfigValues
	 * @param toConfigValues
	 * @param driverConfigValues
	 */
	public Job(int id, String name, int fromConnectorId, int fromLinkId, int toConnectorId, int toLinkId,
			String creationUser, Date creationDate, String updateUser, Date updateDate, boolean enabled,
			ConfigValue[] fromConfigValues, ConfigValue[] toConfigValues, ConfigValue[] driverConfigValues) {
		this.id = id;
		this.name = name;
		this.fromConnectorId = fromConnectorId;
		this.fromLinkId = fromLinkId;
		this.toConnectorId = toConnectorId;
		this.toLinkId = toLinkId;
		this.creationUser = creationUser;
		this.creationDate = creationDate;
		this.updateUser = updateUser;
		this.updateDate = updateDate;
		this.enabled = enabled;
		this.fromConfigValues = fromConfigValues;
		this.toConfigValues = toConfigValues;
		this.driverConfigValues = driverConfigValues;
	}



	public Job() {
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getFromConnectorId() {
		return fromConnectorId;
	}

	public void setFromConnectorId(int fromConnectorId) {
		this.fromConnectorId = fromConnectorId;
	}

	public int getFromLinkId() {
		return fromLinkId;
	}

	public void setFromLinkId(int fromLinkId) {
		this.fromLinkId = fromLinkId;
	}

	public int getToConnectorId() {
		return toConnectorId;
	}

	public void setToConnectorId(int toConnectorId) {
		this.toConnectorId = toConnectorId;
	}

	public int getToLinkId() {
		return toLinkId;
	}

	public void setToLinkId(int toLinkId) {
		this.toLinkId = toLinkId;
	}

	public String getCreationUser() {
		return creationUser;
	}

	public void setCreationUser(String creationUser) {
		this.creationUser = creationUser;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public ConfigValue[] getFromConfigValues() {
		return fromConfigValues;
	}

	public void setFromConfigValues(ConfigValue[] fromConfigValues) {
		this.fromConfigValues = fromConfigValues;
	}

	public ConfigValue[] getToConfigValues() {
		return toConfigValues;
	}

	public void setToConfigValues(ConfigValue[] toConfigValues) {
		this.toConfigValues = toConfigValues;
	}

	public ConfigValue[] getDriverConfigValues() {
		return driverConfigValues;
	}

	public void setDriverConfigValues(ConfigValue[] driverConfigValues) {
		this.driverConfigValues = driverConfigValues;
	}
	
	
}
