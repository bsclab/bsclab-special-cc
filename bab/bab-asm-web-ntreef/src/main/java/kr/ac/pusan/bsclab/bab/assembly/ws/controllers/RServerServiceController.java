package kr.ac.pusan.bsclab.bab.assembly.ws.controllers;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;

import kr.ac.pusan.bsclab.bab.assembly.rserver.conf.RConfiguration;
import kr.ac.pusan.bsclab.bab.assembly.rserver.exceptions.ErrorResponse;
import kr.ac.pusan.bsclab.bab.assembly.rserver.exceptions.RServeException;
import kr.ac.pusan.bsclab.bab.assembly.rserver.param.GraphOutput;
import kr.ac.pusan.bsclab.bab.assembly.rserver.param.RequestWrapper;
import kr.ac.pusan.bsclab.bab.assembly.rserver.services.ConvertBufferImg;
import kr.ac.pusan.bsclab.bab.assembly.rserver.services.RSException;
import kr.ac.pusan.bsclab.bab.assembly.rserver.services.RServerChecker;
import kr.ac.pusan.bsclab.bab.assembly.rserver.services.RSession;
import kr.ac.pusan.bsclab.bab.assembly.ws.BabWebService;

/**
 * RServer service controller, REST API request by using JSON format and
 * response format either
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */

@Controller
public class RServerServiceController extends AbstractServiceController {

	private String id;
	private String functionName;
	private String sourceFile;
	
	// logging information
	private static final Logger log = LoggerFactory.getLogger(RServerServiceController.class);
	
	// create object rs to check Rserver is running or not
	static RServerChecker rs = new RServerChecker();

	@Autowired
	private RConfiguration src;

	public static final String BASE_URL = BabWebService.BASE_URL + "/rserver";

	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST, path = BASE_URL
			+ "/rservices/{workspaceId}/{datasetId}/{sdt}/{edt}")
	public @ResponseBody ResponseEntity<RequestWrapper> postRServices(
			@PathVariable(value = "workspaceId") String workspaceId,
			@PathVariable(value = "datasetId") String datasetId,
			@PathVariable(value = "sdt") String sdt,
			@PathVariable(value = "edt") String edt, HttpServletRequest request, HttpSession session,
			@RequestBody RequestWrapper entities) throws RServeException, RserveException, JsonProcessingException, ParseException {
		String resourceDataHash = this.resourceDataHash(dc.getTimestamp(sdt), dc.getTimestamp(edt));

		sourceFile = webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash) +"/"+ resourceDataHash+".irepo";
		sourceFile.toString();
	
		//System.out.println("ini resourceDataHashim = " + webHdfs.getRepositoryPath(workspaceId, datasetId, resourceDataHash) +"/"+ resourceDataHash+".irepo");
		if (RServerChecker.isRserveRunning(src.getIpRserver(), src.getPortRserve())) {
			int n = entities.getEntities().size();
			
			for (int i = 0; i < n; i++) {
				
				functionName = entities.getEntities().get(i).getFunctionName();
				id = entities.getId() + functionName;

				switch (functionName) {
					case "histogram": // just for demo only
						RConnection cn = null;
						entities.getEntities().get(i).setId(id);
	
						try {
							String device = "CairoJPEG";
							cn = new RConnection(src.getIpRserver(), src.getPortRserve());
	
							RSession.loadLibrary(cn, "Cairo");
							RSession.loadRScript(cn, src, "my_rscript6.R");

							REXP xp = cn.parseAndEval("try(" + device + "('test.jpg',quality=100))");
							if (xp.inherits("try-error")) {
								System.err.println("Can't open " + device + " graphics device:\n" + xp.asString());
							}
	
							cn.eval("z <- dataRnorm(" + entities.getEntities().get(i).getParameters().getCoil_no() + ")");
							cn.parseAndEval("hist(z, col=\"lightblue\");dev.off()");

							xp = cn.parseAndEval("r=readBin('test.jpg','raw',1024*1024); unlink('test.jpg'); r");

							Image img = Toolkit.getDefaultToolkit().createImage(xp.asBytes());
							String imgName = id +"-"+i+System.currentTimeMillis()+ ".jpg";
							if(createTempImg(imgName, img)){
								ArrayList<GraphOutput> output = new ArrayList<GraphOutput>();
								output.add(new GraphOutput(src.getGraphUrl()+"/"+ imgName));
								entities.getEntities().get(i).setOutput(output);
							}
							entities.getEntities().get(i).setId(id);
	
						} catch (RSException e) {
							throw new RServeException(e.getMessage());
						} catch (RserveException e) {
							throw new RServeException(e.getMessage());
						} catch (REXPMismatchException e) {
							throw new RServeException(e.getMessage());
						} catch (REngineException e) {
							throw new RServeException(e.getMessage());
						} finally {
							cn.close();
						}
						break;
						
					case "correlation":
						RConnection cn2 = null;
						entities.getEntities().get(i).setId(id);

						try {
							String device = "CairoJPEG";
							cn2 = new RConnection(src.getIpRserver(), src.getPortRserve());
							RSession.loadLibrary(cn2, "Cairo");
							//RSession.sparklyrReadJSON(cn2,src,sourceFile);

							RSession.sparkRConf(cn2, src, 4, "4G");
							RSession.sparklyrReadCSV(cn2, src, "TestData.csv");
							RSession.loadLibrary(cn2, "dplyr");
							RSession.loadRScript(cn2, src, "TimeGapCalculator.R");
							RSession.loadRScript(cn2, src, "Calculator.R");
	
							REXP xp = cn2.parseAndEval("try(" + device + "('test.jpg',quality=100))");
							if (xp.inherits("try-error")) {
								System.err.println("Can't open " + device + " graphics device:\n" + xp.asString());
							}
	
							cn2.eval("x <- data.frame(data)");
							cn2.eval("plot(correlation(x));dev.off()");
	
							xp = cn2.parseAndEval("r=readBin('test.jpg','raw',1024*1024); unlink('test.jpg'); r");

							Image img = Toolkit.getDefaultToolkit().createImage(xp.asBytes());
							String imgName = id +"-"+i+System.currentTimeMillis()+".jpg";
							if(createTempImg( imgName, img)){
								ArrayList<GraphOutput> output = new ArrayList<GraphOutput>();
								output.add(new GraphOutput(src.getGraphUrl()+"/" +imgName));
								entities.getEntities().get(i).setOutput(output);
							}
							entities.getEntities().get(i).setId(id);
							
						} catch (RSException e) {
							throw new RServeException(e.getMessage());
						} catch (RserveException e) {
							throw new RServeException(e.getMessage());
						} catch (REXPMismatchException e) {
							throw new RServeException(e.getMessage());
						} catch (REngineException e) {
							throw new RServeException(e.getMessage());
						} finally {
							cn2.close();
						}
	
						break;
					default:
						throw new RServeException("Invalid requested function");
					}
			}
		} else {
			log.info("Error connecting to R-Server");
			throw new RServeException("Error connecting to R-Server");
		}
		return new ResponseEntity<RequestWrapper>(entities, HttpStatus.OK);
	}
	
	
	/**
	 * @param id
	 * @param img
	 * @return true if img is successfully created otherwise false
	 */
	public boolean createTempImg(String id, Image img){
		try {
			// write graph in local server
			BufferedImage img2 = ConvertBufferImg.toBufferedImage(img);
		    File outputfile = new File(id);
		    ImageIO.write(img2, "png", outputfile);
		    
		    // copy from local to HDFS
		    this.webHdfs.copyFromLocal(outputfile.getAbsolutePath(), "/"+src.getOutputHdfsPath()+"/" +id);
			outputfile.delete(); // delete file after copy to HDFS
			return true;

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
	}

	/**
	 * @param ex
	 * @return error response
	 */
	@ExceptionHandler(RServeException.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
		error.setMessage(ex.getMessage());
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
	}
}
