package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services.ann;

import java.util.Iterator;
import java.util.Map;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class ANN_SSB implements ANN_base {
	MultiLayerNetwork net;
	
	@Override
	public void setWeight(MultiLayerNetwork net) {
		
		this.net = net;
		
    	Iterator<Map.Entry<String, INDArray>> paramap_iterator = net.paramTable().entrySet().iterator();
    	
        while(paramap_iterator.hasNext()) {
            Map.Entry<String, INDArray> me = (Map.Entry<String, INDArray>) paramap_iterator.next();
            INDArray indarr;
            if(me.getKey().equals("0_W")) {
            	double[][] doubarr = {{7.44480312e-01, -1.16928173e-02, -1.82394123e+01, 4.53813612e-01
            				, 7.14838982e-01, -4.41008538e-01, 1.14019585e+00, 1.41171682e+00
            				, 1.06599176e+00, 4.55576986e-01, -1.66597784e-01, 2.09453061e-01
            				, 8.29048347e+00, -7.69712210e-01, 3.10116500e-01, -1.17877378e+01
            				, 1.27350822e-01, -1.13358955e+01, 1.42490375e+00, 3.28321934e-01}, 
            			{-9.91732895e-01, -5.26799083e-01, 2.49035072e+00, 3.92580897e-01
        					, 3.85112345e-01, -1.20100148e-01, -8.87933552e-01, 6.72159940e-02
        					, -1.50091922e+00, -8.60489964e-01, 2.04995554e-02, -8.45353365e-01
        					, -2.69577980e-01, -1.71151495e+00, -1.39365196e+00, 1.72471035e+00
        					, -6.68339610e-01, 2.92116809e+00, -5.55735826e-01, -1.70787767e-01},
            			{-2.46759582e+00, -5.28962016e-01, -1.33097064e+00, -8.00113022e-01
    						, -9.67269242e-01, 4.70613420e-01, -1.22459149e+00, -4.66347963e-01
    						, 3.38190824e-01, -1.81117570e+00, -2.91266012e+00, 4.50608671e-01
    						, 2.99645877e+00, -6.67439103e-01, -1.61081207e+00, 3.94992262e-01
    						, -2.59472609e+00, 7.70148218e-01, -2.38387734e-01, -9.48766351e-01}};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("0_b")) {
            	double[] doubarr = {0.02184395, -0.50454867, 1.52195859, -1.84900558, -0.82666087, -0.49417037
            			, -0.65328997, -0.85580599, -0.81579727, -0.14701405, -1.31342936, -0.88386053
            			, -0.206966, -0.50345427, -0.22137043, 0.69762665, -0.0419559, 0.57030112
            			, -0.68627453, -1.40754426};
            	indarr = Nd4j.create(doubarr);
            }
            else if(me.getKey().equals("1_W")) {
            	double[] doubarr = {-0.36965105,
            			-0.0108426,
            			1.24469793,
            			1.26182091,
            			-1.13270688,
            			-0.17747205,
            			0.56966567,
            			0.37388706,
            			-0.76262701,
            			-0.08432196,
            			1.38556838,
            			1.09398007,
            			-0.38448513,
            			-0.51698118,
            			0.02374252,
            			0.61509973,
            			-0.41102049,
            			1.1437968,
            			-0.19096269,
            			-0.76434302};
            	indarr = Nd4j.create(doubarr);
            }
            else // if(me.getKey().equals("1_b") 
            {
            	double[] doubarr = {0.94217974};
            	indarr = Nd4j.create(doubarr);
            }
            net.setParam(me.getKey(), indarr);
            
            //System.out.println(me.getKey());
            //System.out.println(Arrays.toString(me.getValue().shape()));
            //System.out.println(me.getValue());
        }	
	}
	
	@Override
	public double getMaxThick() {
		
		return 4.6;
	}

	@Override
	public double getMaxWidth() {
		
		return 419.0;
	}

	@Override
	public double getMaxWeight() {
		
		return 4374.00;
	}

	@Override
	public double getMinThick() {
		
		return 1.29;
	}

	@Override
	public double getMinWidth() {
		
		return 12.0;
	}

	@Override
	public double getMinWeight() {
		
		return 36.0;
	}
	
	public double getOutput(double thick, double width, double weight) {
		thick = Math.abs((thick - getMinThick()))/Math.abs((getMaxThick()-getMinThick()));
		width = Math.abs((width - getMinWidth()))/Math.abs((getMaxWidth()-getMinWidth()));
		weight = Math.abs((weight - getMinWeight()))/Math.abs((getMaxWeight()-getMinWeight()));
		
		final INDArray input = Nd4j.create(new double[] { thick, width, weight }, new int[] { 1, 3 });
        INDArray out = net.output(input, false);
        return Math.exp(out.getDouble(0));
	}
}
