package kr.ac.pusan.bsclab.bab.assembly.regressionmachine.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class ParserDT {
	public ParserDT() {
		
	}
	
	/*public static void main(String[] args) {
		ParserDT pd = new ParserDT();
		pd.getAndWrite();
	}
	*/
	
	public void getAndWrite() {
		try {
			PrintWriter writer = new PrintWriter("DT_ANA.java", "UTF-8");
			BufferedReader in = new BufferedReader(new FileReader("ANA.txt"));
			String str;
			double def_val = 0;
			writer.println("public class DT_ANA {");
			writer.println("public double getDuration(double t, double w, double we, String p) {");
			while((str=in.readLine())!=null && str.length()!=0) {
				String[] split = str.split(" ");
				String line = "";
				boolean prev_stop = false;
				int flag = 0;
				boolean already_if = false;
				boolean is_planCode = false;
				
				for(String val : split) {
					if(val.startsWith("("))
						continue;
					
					if(val.equals("|") || val.equals(""))
					{
						if(val.equals("|"))
							flag++;
					}
					else
					{
						if(!already_if) {
							if(flag > 0) {
								for(int i=0; i<flag; i++)
									line += " ";
							}
							if(val.equals("thickness"))
								val = "t";
							else if(val.equals("width"))
								val = "w";
							else if(val.equals("weight"))
								val = "we";
							if(val.equals("planCode")) {
								val = "p";
								is_planCode = true;
							}
							
							line += "if("+val;
							
							already_if = true;
						}
						else {
							if(val.equals("="))
								val = " == ";
							else if(val.equals("<=") || val.equals(">=") || val.equals("<") || val.equals(">"))
								val = " "+val+" ";
							else if(val.endsWith(":")) {
								if(is_planCode)
									val = "\""+val+"\"";
								val = val.replace(":", "")+")";
								
								prev_stop = true;
							}
							else {
								if(prev_stop) {
									def_val = Double.parseDouble(val);
									val = " return "+val+";";
								}
								else {
									if(is_planCode)
										val = "\""+val+"\"";
									val = val+")";
								}
							}
							line += val;
						}
							
					}
				}
				writer.println(line);
			}
			in.close();
			
			writer.println("return "+def_val+";");
			writer.println("}");
			writer.println("}");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
