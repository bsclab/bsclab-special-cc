/*
 * author @taufiknuradi (taufik.nur.adi@gmail.com)
 * */

new Vue({
	el: '#app', components:{
		loader:LoaderComponent
	},
	data:{
	},
	ready: function(){
		//this.defaultGraph();
		//this.defaultResultSegment();
	},
	methods: {
		defaultResultSegment:function(event){
			var graphAreaSegment = d3.select("#graphAreaSegment");
			var yoySegment = d3.select("#yoySegment");
			var momSegment = d3.select("#momSegment");
			
			graphAreaSegment.attr("style","display:none");
			yoySegment.attr("style","display:none");
			momSegment.attr("style","display:none");
		},
		
		runResult:function(event){
			var graphAreaSegment = d3.select("#graphAreaSegment");
			var yoySegment = d3.select("#yoySegment");
			var momSegment = d3.select("#momSegment");
			
			var mainClassifier = document.getElementById("mainClassifier");
			var subClassifier = document.getElementById("subClassifier");
			var analysis = document.getElementById("analysis");
			var resultDisplay = document.getElementById("resultDisplay");
			
			var valMainClassifier = mainClassifier.value;
			var valSubClassifier = subClassifier.value;
			var valAnalysis = analysis.value;
			var valResultDisplay = resultDisplay.value; 			
						
			if(valMainClassifier != "" && valAnalysis !=""  && valResultDisplay != ""){
				if(valResultDisplay == "table"){				
					if(valAnalysis == "yoy"){
						this.defaultResultSegment();
						yoySegment.attr("style","display:yes");
						var titleMainClassifierYear = document.getElementById("titleMainClassifierYear");
						titleMainClassifierYear.innerHTML = valMainClassifier;
					}else if(valAnalysis == "mom"){
						this.defaultResultSegment();
						momSegment.attr("style","display:yes");
						var titleMainClassifierMonth = document.getElementById("titleMainClassifierMonth");
						titleMainClassifierMonth.innerHTML = valMainClassifier;
					}
				}else if(valResultDisplay == "graph"){					
					this.defaultResultSegment();
					graphAreaSegment.attr("style","display:yes");
					
					var graphArea = document.getElementById("graphArea");
					graphArea.innerHTML = "";
					
					var titleGraph = document.getElementById("titleGraph");
					var titleVal;
					
					if(valAnalysis == "mom"){titleVal = "Graph: Month over Month";}else{titleVal = "Graph: Year over Year";}
					
					titleGraph.innerHTML = "<i class=\"signal icon\"></i>"+titleVal;
					this.defaultGraph();
				}
			}else{
				alert("Check Main Classifier, Sub Classifier, Analysis and Result!");
			}
			
		},
		
		defaultGraph:function(event){
			var n = 11, // number of samples
		        m = 3; // number of series

			var data = d3.range(m).map(function() { return d3.range(n).map(Math.random); });
	
			var margin = {top: 20, right: 30, bottom: 30, left: 40},
			    width = 960 - margin.left - margin.right,
			    height = 500 - margin.top - margin.bottom;
	
			var y = d3.scale.linear()
			    .domain([0, 1])
			    .range([height, 0]);
	
			var x0 = d3.scale.ordinal()
			    .domain(d3.range(n))
			    .rangeBands([0, width], .2);
	
			var x1 = d3.scale.ordinal()
			    .domain(d3.range(m))
			    .rangeBands([0, x0.rangeBand()]);
	
			var z = d3.scale.category10();
	
			var xAxis = d3.svg.axis()
			    .scale(x0)
			    .orient("bottom");
	
			var yAxis = d3.svg.axis()
			    .scale(y)
			    .orient("left");
	
			var svg = d3.select("#graphArea").append("svg")
			    .attr("width", width + margin.left + margin.right)
			    .attr("height", height + margin.top + margin.bottom)
			  .append("svg:g")
			    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
			svg.append("g")
			    .attr("class", "y axis")
			    .call(yAxis);
	
			svg.append("g")
			    .attr("class", "x axis")
			    .attr("transform", "translate(0," + height + ")")
			    .call(xAxis);
	
			svg.append("g").selectAll("g")
			    .data(data)
			  .enter().append("g")
			    .style("fill", function(d, i) { return z(i); })
			    .attr("transform", function(d, i) { return "translate(" + x1(i) + ",0)"; })
			  .selectAll("rect")
			    .data(function(d) { return d; })
			  .enter().append("rect")
			    .attr("width", x1.rangeBand())
			    .attr("height", y)
			    .attr("x", function(d, i) { return x0(i); })
			    .attr("y", function(d) { return height - y(d); 
			});
		}
	}
})