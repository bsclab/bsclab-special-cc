new Vue({
  el: '#app',
  components: {
	'processmodel': ProcessModelInServerComponent
  },
  data: {
	lrData: undefined,
    pmData: {
    	config: config,
		options: {
	        is3D: false,
	        rankdir: "TB",
	        arctype: 'bundle',
	        isLogReplay: false,
	        type: 'heuristic',
	        renderer: 'dagred3'
	      }
    },
    apiURI: {
    	processModelInServer: webURI.processvisualizer.renderheuristic + jsonData.wdr,
    	repositoryView: apiURI.repository.view+ jsonData.workspaceId + '/' + jsonData.datasetId+ '/' + jsonData.sdt + '/' + jsonData.edt,
    	processModel: apiURI.model.heuristic + jsonData.workspaceId + '/' + jsonData.datasetId+ '/' + jsonData.sdt + '/' + jsonData.edt
    }
  }
});