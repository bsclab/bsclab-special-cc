/**
 * 
 * @version 1.10 07 July 2017
 * @author Imam Mustafa Kamal
 *
 */

// Id user generator
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}
// Vue js for handling process
new Vue({
	el: '#app',
	components: {
		'loader': LoaderComponent
	},
	data: {
		isProcessing: false
	},
	created: function(e) {
		this.isProcessing = true;
		/*var functionName = $('#function_name').val();
		var coilNumber = $('#coil_number').val();
		var activity = $('#activity').val();
		var startTime = $('#start_time').val();
		var endTime = $('#end_time').val();*/
		var sdt = $('#sdtVal').val();
		var edt = $('#edtVal').val();
		var URI = apiURI.rservices.rservices + jsonData.workspaceId + '/' + jsonData.datasetId +'/'+ jsonData.sdt + '/' + jsonData.edt;
		var json ={
				"id": guid(),
				"entities": [
					{
						"functionName": "correlation"
						/*"parameters":
							{
								"sdt" : sdt,
								"edt" : edt
							}*/
					}
				]
		};
	    $.ajax({
	    	url: URI,
	        data: JSON.stringify(json),
	        type: 'post',
	        dataType: 'json',
	        crossDomain: true,
	        beforeSend: function(xhr) {
	            xhr.setRequestHeader("Accept", "application/json");
	            xhr.setRequestHeader("Content-Type", "application/json");
	        }.bind(this),
	        success: function(data) {
	        	var imgUrl = data.entities[0].output[0]+'?op=OPEN';
	        	$('#output').css('background-image', "url(" + imgUrl + ")");
	        	this.isProcessing = false;
	        }.bind(this),
	        error: function (XMLHttpRequest, textStatus, errorThrown) {
	        	this.isProcessing = false;
	        	console.log("Error in " +XMLHttpRequest.status);
	        }.bind(this),
	        
	    });
	    return false;
	}
});