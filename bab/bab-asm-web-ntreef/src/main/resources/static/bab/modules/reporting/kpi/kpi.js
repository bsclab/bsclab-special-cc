/*
 * author @taufiknuradi (taufik.nur.adi@gmail.com)
 * */

/* Own Function for all Graph Function Below */
function getTextWidth(text, fontSize, fontName) {
  c = document.createElement("canvas");
  ctx = c.getContext("2d");
  ctx.font = fontSize + ' ' + fontName;
  return ctx.measureText(text).width;
}

new Vue({
	el: '#app', components:{
		loader:LoaderComponent
	},
	data:{
		optRadioValue : "",
		isProcessing: false,
		tempDashboardURL : ""
	},
	ready: function(){
		
		if(isModelNull == "true"){
			this.isProcessing = true;			
		}else{
			this.isProcessing = false;
		}
	},
	methods: {		
		runResult:function(event){
			var graphAreaSegment = d3.select("#graphAreaSegment");
			var yoySegment = d3.select("#yoySegment");
			var yoyTransposeSegment = d3.select("#yoyTransposeSegment");
			var yoyUtilizationSegment = d3.select("#yoyUtilizationSegment");
			var yoyUtilizationTransposeSegment = d3.select("#yoyUtilizationTransposeSegment");
			
			var momSegment = d3.select("#momSegment");
			var momTransposeSegment = d3.select("#momTransposeSegment");
			var momUtilizationSegment = d3.select("#momUtilizationSegment");
			var momUtilizationTransposeSegment = d3.select("#momUtilizationTransposeSegment");
			
			var utilizationReferenceYUS = d3.select("#utilizationReferenceYUS");
			var utilizationReferenceYUTS = d3.select("#utilizationReferenceYUTS");
			var utilizationReferenceMUS = d3.select("#utilizationReferenceMUS");
			var utilizationReferenceMUTS = d3.select("#utilizationReferenceMUTS");
			var utilizationMsg = "<div class='header'>Machine Capacity Reference</div>" +
					"<b>PP1:</b> 300,000 tons/year;&nbsp;&nbsp;<b>PP2:</b> 300,000 tons/year;&nbsp;&nbsp;" +
					"<b>WS1:</b> 140,000 tons/year;&nbsp;&nbsp;<b>WS2:</b> 120,000 tons/year;&nbsp;&nbsp;" +
					"<b>WS3:</b> 150,000 tons/year;&nbsp;&nbsp;<b>WS4:</b> 190,000 tons/year;&nbsp;&nbsp;" +
					"<br/><b>CR1:</b> 80,000 tons/year;&nbsp;&nbsp;<b>CR2:</b> 270,000 tons/year;&nbsp;&nbsp;" +
					"<b>CR3:</b> 150,000 tons/year;&nbsp;&nbsp;<b>SS1:</b> 86,000 tons/year;&nbsp;&nbsp;" +
					"<b>SS2:</b> 43,000 tons/year;&nbsp;&nbsp;<b>SS3:</b> 100,000 tons/year;&nbsp;&nbsp;" +
					"<b>SS4:</b> 50,000 tons/year;";
			
			var mainClassifier = document.getElementById("mainClassifier");
			var subClassifier = document.getElementById("subClassifier");
			var kpiOption = document.getElementById("kpiOption");
			var analysis = document.getElementById("analysis");
			var resultDisplay = document.getElementById("resultDisplay");
			
			var valMainClassifier = mainClassifier.value;
			var valSubClassifier = subClassifier.value;
			var valKpiOption = kpiOption.value;
			var valAnalysis = analysis.value;
			var valResultDisplay = resultDisplay.value; 			
					
			if(valMainClassifier != "" && valKpiOption != "" && valAnalysis !=""  && valResultDisplay != ""){
				if(valKpiOption == "throughput"){
					if(valResultDisplay == "table"){				
						if(valAnalysis == "yoy"){
							this.defaultResultSegment();
							if(this.optRadioValue == "default"){
								//https://stackoverflow.com/questions/29283128/how-to-enable-overflow-scrolling-within-a-semantic-ui-grid
								yoySegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
								
								var titleMainClassifierYear = document.getElementById("titleMainClassifierYear");
								titleMainClassifierYear.innerHTML = valMainClassifier;
								
								var normalYoyKpiTitle = document.getElementById("normalYoyKpiTitle");
								normalYoyKpiTitle.innerHTML = "";
								normalYoyKpiTitle.innerHTML = "<i class='calendar outline icon'></i>Year over Year : "+valKpiOption;
							}else if(this.optRadioValue == "transpose"){
								yoyTransposeSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
								
								var titleMainClassifierYearTranspose = document.getElementById("titleMainClassifierYearTranspose");
								titleMainClassifierYearTranspose.innerHTML = valMainClassifier;
								
								var transposeYoyKpiTitle = document.getElementById("transposeYoyKpiTitle");
								transposeYoyKpiTitle.innerHTML = "";
								transposeYoyKpiTitle.innerHTML = "<i class='calendar outline icon'></i>Year over Year : Transpose - "+valKpiOption;
							}					
						}else if(valAnalysis == "mom"){
							this.defaultResultSegment();
							if(this.optRadioValue == "default"){
								momSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
								
								var titleMainClassifierMonth = document.getElementById("titleMainClassifierMonth");
								titleMainClassifierMonth.innerHTML = valMainClassifier;
								
								var normalMomKpiTitle = document.getElementById("normalMomKpiTitle");
								normalMomKpiTitle.innerHTML = "";
								normalMomKpiTitle.innerHTML = "<i class='calendar icon'></i></i>Month over Month : "+valKpiOption;
							}else if(this.optRadioValue == "transpose"){
								momTransposeSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
								
								var titleMainClassifierMonth = document.getElementById("titleMainClassifierMonthTranspose");
								titleMainClassifierMonthTranspose.innerHTML = valMainClassifier;
								
								var transposeMomKpiTitle = document.getElementById("transposeMomKpiTitle");
								transposeMomKpiTitle.innerHTML = "";
								transposeMomKpiTitle.innerHTML = "<i class='calendar icon'></i></i>Month over Month : Transpose - "+valKpiOption;
							}
						}
					}else if(valResultDisplay == "graph"){					
						this.defaultResultSegment();
						graphAreaSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
						
						var graphArea = document.getElementById("graphArea");
						graphArea.innerHTML = "";
						
						var titleGraph = document.getElementById("titleGraph");
						var titleVal;
						
						if(valAnalysis == "mom"){
							titleVal = "Graph: Month over Month";
							if(this.optRadioValue == "default"){
								this.defaultMomGraph();
							}else if(this.optRadioValue == "flip"){
								this.flipMomGraph();
							}else if(this.optRadioValue == "transpose"){
								
							}
						}else if(valAnalysis == "yoy"){
							titleVal = "Graph: Year over Year";
							if(this.optRadioValue == "default"){
								this.defaultYoyGraph();
							}else if(this.optRadioValue == "flip"){
								this.flipYoyGraph();
							}else if(this.optRadioValue == "transpose"){
								this.transposeYoyGraph();
							}
						}
						
						titleGraph.innerHTML = "<i class=\"signal icon\"></i>"+titleVal;
						this.defaultGraph();
					}
				}else if(valKpiOption == "utilization"){
					if(valResultDisplay == "table"){				
						if(valAnalysis == "yoy"){
							this.defaultResultSegment();							
							if(this.optRadioValue == "default"){
								yoyUtilizationSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
								
								utilizationReferenceYUS.attr("class","ui yellow message");
								utilizationReferenceYUS.html(utilizationMsg);
								
								var titleMainClassifierYearUtilization = document.getElementById("titleMainClassifierYearUtilization");
								titleMainClassifierYearUtilization.innerHTML = valMainClassifier;
							}else if(this.optRadioValue == "transpose"){
								yoyUtilizationTransposeSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
								
								utilizationReferenceYUTS.attr("class","ui yellow message");
								utilizationReferenceYUTS.html(utilizationMsg);
								
								var titleMainClassifierYearUtilizationTranspose = document.getElementById("titleMainClassifierYearUtilizationTranspose");
								titleMainClassifierYearUtilizationTranspose.innerHTML = valMainClassifier;
							}
						}else if(valAnalysis == "mom"){
							this.defaultResultSegment();
							if(this.optRadioValue == "default"){
								momUtilizationSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
								
								utilizationReferenceMUS.attr("class","ui yellow message");
								utilizationReferenceMUS.html(utilizationMsg);
								
								var titleMainClassifierMonthUtilization = document.getElementById("titleMainClassifierMonthUtilization");
								titleMainClassifierMonthUtilization.innerHTML = valMainClassifier;
							}else if(this.optRadioValue == "transpose"){
								momUtilizationTransposeSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
								
								utilizationReferenceMUTS.attr("class","ui yellow message");
								utilizationReferenceMUTS.html(utilizationMsg);
								
								var titleMainClassifierMonthUtilizationTranspose = document.getElementById("titleMainClassifierMonthUtilizationTranspose");
								titleMainClassifierMonthUtilizationTranspose.innerHTML = valMainClassifier;
							}
						}
					}else if(valResultDisplay == "graph"){
						this.defaultResultSegment();
						graphAreaSegment.attr("style","display:yes;overflow-y:auto;white-space:nowrap;");
						
						var graphArea = document.getElementById("graphArea");
						graphArea.innerHTML = "";
						
						var titleGraph = document.getElementById("titleGraph");
						var titleVal;
						
						if(valAnalysis == "mom"){
							titleVal = "Graph: Month over Month - Utilization";
							if(this.optRadioValue == "default"){
								this.defaultMomUtilizationGraph();
							}
						}else if(valAnalysis == "yoy"){
							titleVal = "Graph: Year over Year - Utilization";
							if(this.optRadioValue == "default"){
								this.defaultYoyUtilizationGraph();
							}
						}
						
						titleGraph.innerHTML = "<i class=\"signal icon\"></i>"+titleVal;
						this.defaultGraph();
					}
				}
			}else{
				alert("Check Main Classifier, KPI, Analysis and Result!");
			}
		},
		
		changeResultOpt:function(event){
			var eventId = event.target.id;
			var eventValue = event.target.value;			
			
			var allOptRadio = d3.select("#allOptRadio");
			var flipOptRadio = d3.select("#flipOptRadio");
			var transposeOptRadio = d3.select("#transposeOptRadio");
			
			var resultDisplay = document.getElementById("resultDisplay");
			
			var kpiOption = document.getElementById("kpiOption");
			var valKpiOption = kpiOption.value;
			
			this.defaultResultOpt();
			allOptRadio.attr("style","display:yes");
			
			if(eventValue == "table" || resultDisplay.value == "table"){
				transposeOptRadio.attr("style","display:yes");
				//alert("Transpose activated");
			}else if(eventValue == "graph" || resultDisplay.value == "graph"){
				flipOptRadio.attr("style","display:yes");
				//alert("Flip activated");
				/*if(valKpiOption == "throughput"){
					flipOptRadio.attr("style","display:yes");
				}*/
			}
		},
		
		defaultResultOpt:function(event){
			var allOptRadio = d3.select("#allOptRadio");
			var flipOptRadio = d3.select("#flipOptRadio");
			var transposeOptRadio = d3.select("#transposeOptRadio");
			
			allOptRadio.attr("style","display:none");
			flipOptRadio.attr("style","display:none");
			transposeOptRadio.attr("style","display:none");
		},
		
		defaultResultSegment:function(event){
			var graphAreaSegment = d3.select("#graphAreaSegment");
			var yoySegment = d3.select("#yoySegment");
			var yoyTransposeSegment = d3.select("#yoyTransposeSegment");
			var yoyUtilizationSegment = d3.select("#yoyUtilizationSegment");
			var yoyUtilizationTransposeSegment = d3.select("#yoyUtilizationTransposeSegment"); 
			
			var momSegment = d3.select("#momSegment");
			var momTransposeSegment = d3.select("#momTransposeSegment");
			var momUtilizationSegment = d3.select("#momUtilizationSegment");
			var momUtilizationTransposeSegment = d3.select("#momUtilizationTransposeSegment");			
			
			/* Clearing Utilization Reference DIV */
			
			var utilizationReferenceYUS = d3.select("#utilizationReferenceYUS");
			var utilizationReferenceYUTS = d3.select("#utilizationReferenceYUTS");
			var utilizationReferenceMUS = d3.select("#utilizationReferenceMUS");
			var utilizationReferenceMUTS = d3.select("#utilizationReferenceMUTS");
			
			utilizationReferenceYUS.attr("class","");
			utilizationReferenceYUS.html("");
			
			utilizationReferenceYUTS.attr("class","");
			utilizationReferenceYUTS.html("");
			
			utilizationReferenceMUS.attr("class","");
			utilizationReferenceMUS.html("");
			
			utilizationReferenceMUTS.attr("class","");
			utilizationReferenceMUTS.html("");
			
			/* End of Clearing Utilization Reference DIV */
			
			graphAreaSegment.attr("style","display:none");
			yoySegment.attr("style","display:none");
			yoyTransposeSegment.attr("style","display:none");
			yoyUtilizationSegment.attr("style","display:none");
			yoyUtilizationTransposeSegment.attr("style","display:none");
			
			momSegment.attr("style","display:none");
			momTransposeSegment.attr("style","display:none");
			momUtilizationSegment.attr("style","display:none");
			momUtilizationTransposeSegment.attr("style","display:none");
		},
		
		kpiProgressBar:function(event){
			$(document).ready(function(){
				var $progress = $('.kpi.progress .progress'), updateEvent;
				
				clearInterval(window.fakeProgress);
				window.fakeProgress = setInterval(function(){
					$progress.progress('increment');
					if($progress.progress('is complete')){

					}
				}, 100);
			});
		},
		
		defaultGraph:function(event){		
			
		},
		
		defaultYoyGraph:function(event){
			Data = JSON.parse(jsonGraphYear);
			
			/*var margin = { top: 60, right: 30, bottom: 60, left: 60 },
			    width = (countAllYearData*numberOfOriginatorYear*40) - margin.left - margin.right,
			    height = 700 - margin.top - margin.bottom;*/
			
			/* Revision 2017.08.10
			 * Consider if the number of Year and Originator is 1
			 * 
			 */
			
			var margin = { top: 60, right: 30, bottom: 60, left: 60 };
			var height = 700 - margin.top - margin.bottom;
			
			var requiredWidth = countAllYearData*numberOfOriginatorYear*40;
			var rightPlusLeft = margin.right+margin.left;
			var width = 0;
			if(requiredWidth > rightPlusLeft){
				width = requiredWidth - margin.left - margin.right;
			}else if(requiredWidth <= rightPlusLeft){
				width = requiredWidth + margin.left + margin.right;
			}
				
			var textWidthHolder = 0;
			var Originators = new Array();
			        
			// Extension method declaration
			Originators.pro

			var Data;
			var ageNames;

			var x0 = d3.scale.ordinal().rangeRoundBands([0, width], .1);
			//var XLine = d3.scale.ordinal().rangeRoundPoints([0, width], .1);
			var x1 = d3.scale.ordinal();

			var y = d3.scale.linear().range([height, 0]);

			//var color = d3.scale.ordinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
			var color = d3.scale.ordinal().range(["#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9",
				"#6C9", "#9C9", "#CC9", "#FC9", "#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC",
				"#9CC", "#CCC", "#FCC", "#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF",
				"#CCF", "#FCF", "#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0",
				"#FF0", "#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
				"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6", "#069"]);
			
			var xAxis = d3.svg.axis()
			.scale(x0)
			.orient("bottom");

			var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickFormat(d3.format(".2s"));

			var svg = d3.select("#graphArea").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");


			// Bar Data categories
			Data.forEach(function (d) {
			  d.Originators.forEach(function (b) {
			    if (Originators.findIndex(function (c) { return c.Name===b.Name}) == -1) {
			      b.Type = "bar";
			      Originators.push(b)
			    }
			  })
			});        

			x0.domain(Data.map(function (d) { return d.Year; }));
			//XLine.domain(Data.map(function (d) { return d.Year; }));
			x1.domain(Originators.filter(function (d) { return d.Type == "bar" }).map(function (d) { return d.Name})).rangeRoundBands([0, x0.rangeBand()]);
			y.domain([0, d3.max(Data, function (d) { return d3.max(d.Originators, function (d) { return d.Value; }); })]);

			svg.append("g")
			  .attr("class", "x axis")
			  .attr("transform", "translate(0," + height + ")")
			  .call(xAxis);

			svg.append("g")
			  .attr("class", "y axis")
			  .call(yAxis)
			  .append("text")
			  .attr("transform", "rotate(-90)")
			  .attr("y", -margin.left)
			  .attr("dy", ".71em")
			  .style("text-anchor", "end")
			  .text("Tons");

			var state = svg.selectAll(".state")
			.data(Data)
			.enter().append("g")
			.attr("class", "state")
			.attr("transform", function (d) { return "translate(" + x0(d.Year) + ",0)"; });

			state.selectAll("rect")
			  .data(function (d) { return d.Originators; })
			  .enter().append("rect")
			  .attr("width", x1.rangeBand())
			  .attr("x", function (d) { return x1(d.Name); })
			  .attr("y", function (d) { return y(d.Value); })
			  .attr("height", function (d) { return height - y(d.Value); })
			  .style("fill", function (d) { return color(d.Name); })
			  .transition().delay(500).attrTween("height", function (d) {
			  var i = d3.interpolate(0, height - y(d.Value));
			  return function (t)
			  {
			    return i(t);
			  }
			});

			state.selectAll("text")
			.data(function (d) { return d.Originators; })
			.enter().append("text")
			.text(function(d){
			  return d3.format(",")(d.Value);
			})
			.attr("transform","rotate(-90)")
			.attr("x",function(d,i){
			  //return x1(d.Name)+(x1.rangeBand()/3);
				return -(y(d.Value))+5;
			})
			.attr("y", function(d){
			  //return y(d.Value)-2;
				return x1(d.Name)+(x1.rangeBand()/3)+5;
			});
			
			// Legends
			var defRectWidth = 18;
	    	var rectAndTextSpace = 10;
	    	var labelSpace = 15;
			
			var LegendHolder = svg.append("g").attr("class", "legendHolder");
			var legend = LegendHolder.selectAll(".legend")
			.data(Originators.map(function (d) { return {"Name":d.Name,"Type":d.Type}}))
			.enter().append("g")
			.attr("class", "legend")
			.attr("transform", function (d, i) { return "translate(0," +( height+ margin.bottom/2 )+ ")"; })
			.each(function (d,i) {			
			
			//  Legend Symbols
			d3.select(this).append("rect")
			    .attr("width", function () { return 18 })
			    .attr("x", function (b) {
			    	
		    	//var x = 0;		    	
		        //if(i > 0){ x = 20;
		        //}else{ x = 5; }
			    //left = (i+1) * 15 + i * 18 + i * x + textWidthHolder;			    
			    	
		    	left = (i*defRectWidth + i*rectAndTextSpace + i*labelSpace) + textWidthHolder;
			    return left;
			  })
			    .attr("y", function (b) { return b.Type == 'bar'?0:7})
			    .attr("height", function (b) { return b.Type== 'bar'? 18:5 })
			    .style("fill", function (b) { return b.Type == 'bar' ? color(d.Name) : LineColor(d.Name) });

			  //  Legend Text
			  d3.select(this).append("text")
			    .attr("x", function (b) {

		    	//var x = 0;
		        //if(i > 0){ x = 13;
		        //}else{ x = 5; }			    	
			    //left = (i+1) * 15 + (i+1) * 18 + (i + 1) * x + textWidthHolder;
			    
			    if(i > 0){
			    	left = (i*labelSpace + (i+1)*defRectWidth + (i+1)*rectAndTextSpace) + textWidthHolder;
			    }else{
			    	left = defRectWidth + rectAndTextSpace + textWidthHolder;
			    }
			    
			    return left;
			  })
			    .attr("y", 9)
			    .attr("dy", ".35em")
			    .style("text-anchor", "start")
			    .text(d.Name);

			  textWidthHolder += getTextWidth(d.Name, "15px", "calibri");	
			});


			// Legend Placing
			d3.select(".legendHolder").attr("transform", function (d) {
			  thisWidth = d3.select(this).node().getBBox().width;
			  return "translate("+(margin.left/2)+",0)"; // changed from: return "translate(" + ((width) / 2 - thisWidth / 2) + ",0)";
			});
		},
		
		flipYoyGraph:function(event){
			Data = JSON.parse(jsonGraphYear);
			
			/*var margin = { top: 60, right: 0, bottom: 20, left: 80 },
			    width = (countAllYearData*numberOfOriginatorYear*40) - margin.left - margin.right,
			    height = 700 - margin.top - margin.bottom; //CHANGE TOP & LEFT   
			*/
			
			/* Revision 2017.08.10
			 * Consider if the number of Year and Originator is 1
			 * 
			 */
			
			var margin = { top: 60, right: 0, bottom: 80, left: 80 };
			var height = 700 - margin.top - margin.bottom;
			
			var requiredWidth = countAllYearData*numberOfOriginatorYear*40;
			var rightPlusLeft = margin.right+margin.left;
			var width = 0;
			if(requiredWidth > rightPlusLeft){
				width = requiredWidth - margin.left - margin.right;
			}else if(requiredWidth <= rightPlusLeft){
				width = requiredWidth + margin.left + margin.right;
			}
			
			var textWidthHolder = 0;
			var Originators = new Array();
			        
			// Extension method declaration
			Originators.pro

			var Data;
			var ageNames;

			var x0 = d3.scale.ordinal().rangeRoundBands([0, width], .1);
			var XLine = d3.scale.ordinal().rangeRoundPoints([0, width], .1);
			var x1 = d3.scale.ordinal();

			var y = d3.scale.linear().range([0,height]); //CHANGE FROM .range([height,0])

			//var color = d3.scale.ordinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
			var color = d3.scale.ordinal().range(["#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9",
				"#6C9", "#9C9", "#CC9", "#FC9", "#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC",
				"#9CC", "#CCC", "#FCC", "#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF",
				"#CCF", "#FCF", "#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0",
				"#FF0", "#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
				"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6", "#069"]);
			
			var xAxis = d3.svg.axis()
			.scale(x0)
			.orient("top"); //CHANGE from bottom

			var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickFormat(d3.format(".2s"));

			var svg = d3.select("#graphArea").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");


			// Bar Data categories
			Data.forEach(function (d) {
			  d.Originators.forEach(function (b) {
			    if (Originators.findIndex(function (c) { return c.Name===b.Name}) == -1) {
			      b.Type = "bar";
			      Originators.push(b)
			    }
			  })
			});        

			x0.domain(Data.map(function (d) { return d.Year; }));
			XLine.domain(Data.map(function (d) { return d.Year; }));
			x1.domain(Originators.filter(function (d) { return d.Type == "bar" }).map(function (d) { return d.Name})).rangeRoundBands([0, x0.rangeBand()]);
			y.domain([0, d3.max(Data, function (d) { return d3.max(d.Originators, function (d) { return d.Value; }); })]);

			svg.append("g")
			  .attr("class", "x axis") //CHANGE deleted .attr("transform", "translate(0," + height + ")")			  
			  .call(xAxis);

			svg.append("g")
			  .attr("class", "y axis")
			  .call(yAxis)
			  .append("text")
			  .attr("transform", "rotate(90)")
			  .attr("y", 40) //CHANGE y to 40 from 6, ADD X set to 50
			  .attr("x", 50)
			  .attr("dy", ".71em")
			  .style("text-anchor", "end")
			  .text("Tons");

			var state = svg.selectAll(".state")
			.data(Data)
			.enter().append("g")
			.attr("class", "state")
			.attr("transform", function (d) { return "translate(" + x0(d.Year) + ",0)"; });

			state.selectAll("rect")
			  .data(function (d) { return d.Originators; })
			  .enter().append("rect")
			  .attr("width", x1.rangeBand())
			  .attr("x", function (d) { return x1(d.Name); })
			  .attr("y", 0) //CHANGE function (d) { return y(d.Value); }
			  .attr("height", function (d) { return y(d.Value); }) //CHANGE height - y(d.Value);
			  .style("fill", function (d) { return color(d.Name); })
			  .transition().delay(500).attrTween("height", function (d) {
			  var i = d3.interpolate(0, y(d.Value)); //CHANGE height - y(d.Value);
			  return function (t)
			  {
			    return i(t);
			  }
			});

			state.selectAll("text")
			.data(function (d) { return d.Originators; })
			.enter().append("text")
			.text(function(d){
			  return d3.format(",")(d.Value);
			})
			.attr("transform","rotate(-90)")
			.attr("x",function(d,i){
			  //return x1(d.Name)+(x1.rangeBand()/3);
			  return -(y(d.Value))-40; //CHANGE
			})
			.attr("y", function(d){
			  //return y(d.Value)+13; //CHANGE y(d.Value)-2
			  return x1(d.Name)+(x1.rangeBand()/2)+5; //CHANGE
			});
			
			// Legends
			var defRectWidth = 18;
	    	var rectAndTextSpace = 10;
	    	var labelSpace = 15;
			
			var LegendHolder = svg.append("g").attr("class", "legendHolder");
			var legend = LegendHolder.selectAll(".legend")
			.data(Originators.map(function (d) { return {"Name":d.Name,"Type":d.Type}}))
			.enter().append("g")
			.attr("class", "legend")
			.attr("transform", function (d, i) { return "translate(0," +( height+ margin.bottom/2 )+ ")"; })
			.each(function (d,i) {

			//  Legend Symbols
			d3.select(this).append("rect")
			    .attr("width", function () { return 18 })
			    .attr("x", function (b) {
			    	
		    	/*var x = 0;
		        if(i > 0){ x = 20;
		        }else{ x = 5; }

			    left = (i+1) * 15 + i * 18 + i * x + textWidthHolder;*/
			    	
			    left = (i*defRectWidth + i*rectAndTextSpace + i*labelSpace) + textWidthHolder;
			    return left;
			  })
			    .attr("y", function (b) { return b.Type == 'bar'?0:7})
			    .attr("height", function (b) { return b.Type== 'bar'? 18:5 })
			    .style("fill", function (b) { return b.Type == 'bar' ? color(d.Name) : LineColor(d.Name) });

			  //  Legend Text
			  d3.select(this).append("text")
			    .attr("x", function (b) {

		    	/*var x = 0;
		        if(i > 0){ x = 13;
		        }else{ x = 5; }
			    	
			    left = (i+1) * 15 + (i+1) * 18 + (i + 1) * x + textWidthHolder;*/
			    
		    	if(i > 0){
			    	left = (i*labelSpace + (i+1)*defRectWidth + (i+1)*rectAndTextSpace) + textWidthHolder;
			    }else{
			    	left = defRectWidth + rectAndTextSpace + textWidthHolder;
			    }

			    return left;
			  })
			    .attr("y", 9)
			    .attr("dy", ".35em")
			    .style("text-anchor", "start")
			    .text(d.Name);

			  textWidthHolder += getTextWidth(d.Name, "15px", "calibri");
			});


			// Legend Placing
			d3.select(".legendHolder").attr("transform", function (d) {
			  thisWidth = d3.select(this).node().getBBox().width;
			  return "translate(" + ((width) / 2 - thisWidth / 2) + ","+(0-(height+margin.top))+")"; //CHANGE translate(" + ((width) / 2 - thisWidth / 2) + ",0)
			});
		},
		
		transposeYoyGraph:function(event){
			
		},
		
		defaultYoyUtilizationGraph:function(event){
			Data = JSON.parse(jsonGraphUtilizationYear);
			
			/*var margin = { top: 80, right: 30, bottom: 60, left: 40 },
			    width = (countAllYearData*numberOfOriginatorYear*40) - margin.left - margin.right,
			    height = 700 - margin.top - margin.bottom;*/

			/* Revision 2017.08.10
			 * Consider if the number of Year and Originator is 1
			 * 
			 */
			
			var margin = { top: 80, right: 30, bottom: 60, left: 40 };
			var height = 700 - margin.top - margin.bottom;
			
			var requiredWidth = countAllYearData*numberOfOriginatorYear*40;
			var rightPlusLeft = margin.right+margin.left;
			var width = 0;
			if(requiredWidth > rightPlusLeft){
				width = requiredWidth - margin.left - margin.right;
			}else if(requiredWidth <= rightPlusLeft){
				width = requiredWidth + margin.left + margin.right;
			}
			
			var textWidthHolder = 0;
			var Originators = new Array();
			        
			// Extension method declaration
			Originators.pro

			var Data;
			var ageNames;

			var x0 = d3.scale.ordinal().rangeRoundBands([0, width], .1);			
			var x1 = d3.scale.ordinal();
			var y = d3.scale.linear().range([height, 0]);
			
			var color = d3.scale.ordinal().range(["#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9",
				"#6C9", "#9C9", "#CC9", "#FC9", "#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC",
				"#9CC", "#CCC", "#FCC", "#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF",
				"#CCF", "#FCF", "#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0",
				"#FF0", "#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
				"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6", "#069"]);
			
			var xAxis = d3.svg.axis()
			.scale(x0)
			.orient("bottom");

			var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickFormat(d3.format(".2s"));

			var svg = d3.select("#graphArea").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			// Bar Data categories
			Data.forEach(function (d) {
			  d.Originators.forEach(function (b) {
			    if (Originators.findIndex(function (c) { return c.Name===b.Name}) == -1) {
			      b.Type = "bar";
			      Originators.push(b)
			    }
			  })
			});        

			x0.domain(Data.map(function (d) { return d.Year; }));
			x1.domain(Originators.filter(function (d) { return d.Type == "bar" }).map(function (d) { return d.Name})).rangeRoundBands([0, x0.rangeBand()]);
			y.domain([0, d3.max(Data, function (d) { return d3.max(d.Originators, function (d) { return d.Value; }); })]);

			svg.append("g")
			  .attr("class", "x axis")
			  .attr("transform", "translate(0," + height + ")")
			  .call(xAxis);

			svg.append("g")
			  .attr("class", "y axis")
			  .call(yAxis)
			  .append("text")
			  .attr("transform", "rotate(-90)")
			  .attr("y", -margin.left)
			  .attr("dy", ".71em")
			  .style("text-anchor", "end")
			  .text("Utilization (%)");

			var state = svg.selectAll(".state")
			.data(Data)
			.enter().append("g")
			.attr("class", "state")
			.attr("transform", function (d) { return "translate(" + x0(d.Year) + ",0)"; });

			state.selectAll("rect")
			  .data(function (d) { return d.Originators; })
			  .enter().append("rect")
			  .attr("width", x1.rangeBand())
			  .attr("x", function (d) { return x1(d.Name); })
			  .attr("y", function (d) { return y(d.Value); })
			  .attr("height", function (d) { return height - y(d.Value); })
			  .style("fill", function (d) { return color(d.Name); })
			  .transition().delay(500).attrTween("height", function (d) {
				  var i = d3.interpolate(0, height - y(d.Value));
				  return function (t)
				  {
				    return i(t);
				  }
			  });

			state.selectAll("text")
			.data(function (d) { return d.Originators; })
			.enter().append("text")
			.text(function(d){
			  return d3.format(",")(d.Value);
			  //return "("+d.Name+") "+d3.format(",")(d.Value);
			})
			.attr("transform","rotate(-90)")
			.attr("x",function(d,i){
				return -(y(d.Value))+5;
				//return -(y(0))+5;
			})
			.attr("y", function(d){
				return x1(d.Name)+(x1.rangeBand()/3)+5;
			});
			
			// Legends
			var defRectWidth = 18;
	    	var rectAndTextSpace = 10;
	    	var labelSpace = 15;
			
			var LegendHolder = svg.append("g").attr("class", "legendHolder");
			var legend = LegendHolder.selectAll(".legend")
			.data(Originators.map(function (d) { return {"Name":d.Name,"Type":d.Type}}))
			.enter().append("g")
			.attr("class", "legend")
			.attr("transform", function (d, i) { return "translate(0," +( height+ margin.bottom/2 )+ ")"; })
			.each(function (d,i) {			
			
			//  Legend Symbols
			d3.select(this).append("rect")
			    .attr("width", function () { return 18 })
			    .attr("x", function (b) {		    
			    	
		    	left = (i*defRectWidth + i*rectAndTextSpace + i*labelSpace) + textWidthHolder;
			    return left;
			  })
			    .attr("y", function (b) { return b.Type == 'bar'?0:7})
			    .attr("height", function (b) { return b.Type== 'bar'? 18:5 })
			    .style("fill", function (b) { return b.Type == 'bar' ? color(d.Name) : LineColor(d.Name) });

			  //  Legend Text
			  d3.select(this).append("text")
			    .attr("x", function (b) {
			    
			    if(i > 0){
			    	left = (i*labelSpace + (i+1)*defRectWidth + (i+1)*rectAndTextSpace) + textWidthHolder;
			    }else{
			    	left = defRectWidth + rectAndTextSpace + textWidthHolder;
			    }
			    
			    return left;
			  })
			    .attr("y", 9)
			    .attr("dy", ".35em")
			    .style("text-anchor", "start")
			    .text(d.Name);

			  textWidthHolder += getTextWidth(d.Name, "15px", "calibri");	
			});


			// Legend Placing
			d3.select(".legendHolder").attr("transform", function (d) {
			  thisWidth = d3.select(this).node().getBBox().width;
			  return "translate("+(margin.left/2)+",0)"; // changed from: return "translate(" + ((width) / 2 - thisWidth / 2) + ",0)";
			});
		},
		
		defaultMomGraph:function(event){
			Data = JSON.parse(jsonGraphMonth);

			/*var margin = { top: 60, right: 30, bottom: 160, left: 60 },
			    width = (countAllMonthData*numberOfOriginatorMonth*40) - margin.left - margin.right,
			    height = 700 - margin.top - margin.bottom;*/

			/* Revision 2017.08.10
			 * Consider if the number of Year and Originator is 1
			 * 
			 */
			
			var margin = { top: 60, right: 30, bottom: 160, left: 60 };
			var height = 700 - margin.top - margin.bottom;
			
			var requiredWidth = countAllYearData*numberOfOriginatorYear*40;
			var rightPlusLeft = margin.right+margin.left;
			var width = 0;
			if(requiredWidth > rightPlusLeft){
				width = requiredWidth - margin.left - margin.right;
			}else if(requiredWidth <= rightPlusLeft){
				width = requiredWidth + margin.left + margin.right;
			}
			
			var textWidthHolder = 0;
			var Originators = new Array();
			
			Originators.pro

			var Data;
			var ageNames;

			var x0 = d3.scale.ordinal().rangeRoundBands([0, width], .1);
			var XLine = d3.scale.ordinal().rangeRoundPoints([0, width], .1);
			var x1 = d3.scale.ordinal();

			var y = d3.scale.linear().range([height, 0]);

			//var color = d3.scale.ordinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
			var color = d3.scale.ordinal().range(["#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9",
				"#6C9", "#9C9", "#CC9", "#FC9", "#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC",
				"#9CC", "#CCC", "#FCC", "#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF",
				"#CCF", "#FCF", "#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0",
				"#FF0", "#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
				"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6", "#069"]);
			
			var xAxis = d3.svg.axis()
			.scale(x0)
			.orient("bottom");
			
			var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickFormat(d3.format(".2s"));
			
			var svg = d3.select("#graphArea").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
			
			// Bar Data categories
			Data.forEach(function (d) {
			  d.Originators.forEach(function (b) {
			    if (Originators.findIndex(function (c) { return c.Name===b.Name}) == -1) {
			      b.Type = "bar";
			      Originators.push(b)
			    }
			  })
			});
			
			x0.domain(Data.map(function (d) { return d.Year; }));
			XLine.domain(Data.map(function (d) { return d.Year; }));
			x1.domain(Originators.filter(function (d) { return d.Type == "bar" }).map(function (d) { return d.Name})).rangeRoundBands([0, x0.rangeBand()]);
			y.domain([0, d3.max(Data, function (d) { return d3.max(d.Originators, function (d) { return d.Value; }); })]);
			
			svg.append("g")
			  .attr("class", "x axis")
			  .attr("transform", "translate(0," + height + ")")
			  .call(xAxis)
			  .selectAll("text")
			  .style("text-anchor","end")
			  .attr("dx","-.8em")
			  .attr("dy","-.15em")
			  .attr("transform", function(d){return "rotate(-65)"; });
			
			svg.append("g")
			  .attr("class", "y axis")
			  .call(yAxis)
			  .append("text")
			  .attr("transform", "rotate(-90)")
			  .attr("y", -margin.left)
			  .attr("dy", ".71em")
			  .style("text-anchor", "end")
			  .text("Tons");
			
			var state = svg.selectAll(".state")
			.data(Data)
			.enter().append("g")
			.attr("class", "state")
			.attr("transform", function (d) { return "translate(" + x0(d.Year) + ",0)"; });
			
			state.selectAll("rect")
			  .data(function (d) { return d.Originators; })
			  .enter().append("rect")
			  .attr("width", x1.rangeBand())
			  .attr("x", function (d) { return x1(d.Name); })
			  .attr("y", function (d) { return y(d.Value); })
			  .attr("height", function (d) { return height - y(d.Value); })
			  .style("fill", function (d) { return color(d.Name); })
			  .transition().delay(500).attrTween("height", function (d) {
			  var i = d3.interpolate(0, height - y(d.Value));
			  return function (t)
			  {
			    return i(t);
			  }
			});
			
			state.selectAll("text")
			.data(function (d) { return d.Originators; })
			.enter().append("text")
			.text(function(d){
			  return d3.format(",")(d.Value);
			})
			.attr("transform","rotate(-90)")
			.attr("x",function(d){
			  return -(y(d.Value))+5;
			})
			.attr("y", function(d){
			  return x1(d.Name)+(x1.rangeBand()/2)+5;
			});
			
			// Legends
			var defRectWidth = 18;
	    	var rectAndTextSpace = 10;
	    	var labelSpace = 15;
	    	
			var LegendHolder = svg.append("g").attr("class", "legendHolder");
			var legend = LegendHolder.selectAll(".legend")
			.data(Originators.map(function (d) { return {"Name":d.Name,"Type":d.Type}}))
			.enter().append("g")
			.attr("class", "legend")
			.attr("transform", function (d, i) { return "translate(0," +( height+ margin.bottom/2 )+ ")"; })
			.each(function (d,i) {

			//  Legend Symbols
			d3.select(this).append("rect")
			    .attr("width", function () { return 18 })
			    .attr("x", function (b) {

			    /*var x = 0;
			    if(i > 0){
			      x = 20;   
			    }else{
			      x = 5; 
			    }
			  
			    left = (i+1) * 15 + i * 18 + i * x + textWidthHolder;*/
			    
			    left = (i*defRectWidth + i*rectAndTextSpace + i*labelSpace) + textWidthHolder;
			    return left;
			  })
			    .attr("y", function (b) { return b.Type == 'bar'?0:7})
			    .attr("height", function (b) { return b.Type== 'bar'? 18:5 })
			    .style("fill", function (b) { return b.Type == 'bar' ? color(d.Name) : LineColor(d.Name) });

			  //  Legend Text

			  d3.select(this).append("text")
			    .attr("x", function (b) {

			   /* var x = 0;
			    if(i > 0){
			      x = 13;   
			    }else{
			      x = 5; 
			    }
			    
			    left = (i+1) * 15 + (i+1) * 18 + (i + 1) * x + textWidthHolder;*/
			    	
		    	if(i > 0){
			    	left = (i*labelSpace + (i+1)*defRectWidth + (i+1)*rectAndTextSpace) + textWidthHolder;
			    }else{
			    	left = defRectWidth + rectAndTextSpace + textWidthHolder;
			    }	
			    return left;
			  })
			    .attr("y", 9)
			    .attr("dy", ".35em")
			    .style("text-anchor", "start")
			    .text(d.Name);

			  textWidthHolder += getTextWidth(d.Name, "15px", "calibri");
			});


			// Legend Placing

			d3.select(".legendHolder").attr("transform", function (d) {
			  thisWidth = d3.select(this).node().getBBox().width;
			  //return "translate(" + ((width) / 2 - thisWidth / 2) + ",0)";
			  return "translate("+(margin.left/2)+",0)";
			})
		},
		
		flipMomGraph:function(event){
			Data = JSON.parse(jsonGraphMonth);

			/*var margin = { top: 160, right: 0, bottom: 60, left: 80 }, //CHANGE TOP & LEFT
			    width = (countAllMonthData*numberOfOriginatorMonth*40) - margin.left - margin.right,
			    height = 700 - margin.top - margin.bottom;*/

			/* Revision 2017.08.10
			 * Consider if the number of Year and Originator is 1
			 * 
			 */
			
			var margin = { top: 150, right: 0, bottom: 60, left: 80 };
			var height = 700 - margin.top - margin.bottom;
			
			var requiredWidth = countAllYearData*numberOfOriginatorYear*40;
			var rightPlusLeft = margin.right+margin.left;
			var width = 0;
			if(requiredWidth > rightPlusLeft){
				width = requiredWidth - margin.left - margin.right;
			}else if(requiredWidth <= rightPlusLeft){
				width = requiredWidth + margin.left + margin.right;
			}
			
			var textWidthHolder = 0;
			var Originators = new Array();

			// Extension method declaration
			Originators.pro

			var Data;
			var ageNames;

			var x0 = d3.scale.ordinal().rangeRoundBands([0, width], .1);
			var XLine = d3.scale.ordinal().rangeRoundPoints([0, width], .1);
			var x1 = d3.scale.ordinal();

			var y = d3.scale.linear().range([0,height]);
			//var y = d3.scale.linear().range([height, 0]); CHANGE

			//var color = d3.scale.ordinal().range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
			var color = d3.scale.ordinal().range(["#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9",
				"#6C9", "#9C9", "#CC9", "#FC9", "#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC",
				"#9CC", "#CCC", "#FCC", "#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF",
				"#CCF", "#FCF", "#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0",
				"#FF0", "#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
				"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6", "#069"]);
			
			var xAxis = d3.svg.axis()
			.scale(x0)
			.orient("top"); // CHANGE

			var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickFormat(d3.format(".2s"));

			var svg = d3.select("#graphArea").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			// Bar Data categories
			Data.forEach(function (d) {
			  d.Originators.forEach(function (b) {
			    if (Originators.findIndex(function (c) { return c.Name===b.Name}) == -1) {
			      b.Type = "bar";
			      Originators.push(b)
			    }
			  })
			});
			
			x0.domain(Data.map(function (d) { return d.Year; }));
			XLine.domain(Data.map(function (d) { return d.Year; }));
			x1.domain(Originators.filter(function (d) { return d.Type == "bar" }).map(function (d) { return d.Name})).rangeRoundBands([0, x0.rangeBand()]);
			y.domain([0, d3.max(Data, function (d) { return d3.max(d.Originators, function (d) { return d.Value; }); })]);

			svg.append("g")
			  .attr("class", "x axis")
			  //.attr("transform", "translate(0," + height + ")") CHANGE
			  .call(xAxis)
			  .selectAll("text")
			  .style("text-anchor","end")
			  .attr("dx","4em")
			  .attr("dy","-.25em")
			  .attr("transform", function(d){return "rotate(-65)"; });

			svg.append("g")
			  .attr("class", "y axis")
			  .call(yAxis)
			  .append("text")
			  .attr("transform", "rotate(90)")
			  .attr("y", 40)
			  .attr("x", 50)//CHANGE X & Y
			  .attr("dy", ".71em")
			  .style("text-anchor", "end")
			  .text("Tons");


			var state = svg.selectAll(".state")
			.data(Data)
			.enter().append("g")
			.attr("class", "state")
			.attr("transform", function (d) { return "translate(" + x0(d.Year) + ",0)"; });

			state.selectAll("rect")
			  .data(function (d) { return d.Originators; })
			  .enter().append("rect")
			  .attr("width", x1.rangeBand())
			  .attr("x", function (d) { return x1(d.Name); })
			  .attr("y", 0) //CHANGE
			  .attr("height", function (d) { return y(d.Value); }) //CHANGE
			  .style("fill", function (d) { return color(d.Name); })
			  .transition().delay(500).attrTween("height", function (d) {
			  var i = d3.interpolate(0, y(d.Value)); //CHANGE
			  return function (t)
			  {
			    return i(t);
			  }
			});
			
			state.selectAll("text")
			.data(function (d) { return d.Originators; })
			.enter().append("text")
			.text(function(d){
			  return d3.format(",")(d.Value);
			})
			.attr("transform","rotate(-90)")
			.attr("x",function(d){
			  return -(y(d.Value))-40; //CHANGE
			})
			.attr("y", function(d){
			  return x1(d.Name)+(x1.rangeBand()/2)+5; //CHANGE
			});

			// Legends
			var defRectWidth = 18;
	    	var rectAndTextSpace = 10;
	    	var labelSpace = 15;
	    	
			var LegendHolder = svg.append("g").attr("class", "legendHolder");
			var legend = LegendHolder.selectAll(".legend")
			.data(Originators.map(function (d) { return {"Name":d.Name,"Type":d.Type}}))
			.enter().append("g")
			.attr("class", "legend")
			.attr("transform", function (d, i) { return "translate(0," +( height+ margin.bottom/2 )+ ")"; })
			.each(function (d,i) {

			//  Legend Symbols
			d3.select(this).append("rect")
			    .attr("width", function () { return 18 })
			    .attr("x", function (b) {

			    /*var x = 0;
			    if(i > 0){
			      x = 20;   
			    }else{
			      x = 5; 
			    }
			  
			    left = (i+1) * 15 + i * 18 + i * x + textWidthHolder;*/
			    left = (i*defRectWidth + i*rectAndTextSpace + i*labelSpace) + textWidthHolder;
			    return left;
			  })
			    .attr("y", function (b) { return b.Type == 'bar'?0:7})
			    .attr("height", function (b) { return b.Type== 'bar'? 18:5 })
			    .style("fill", function (b) { return b.Type == 'bar' ? color(d.Name) : LineColor(d.Name) });

			  //  Legend Text
			  d3.select(this).append("text")
			    .attr("x", function (b) {

			   /* var x = 0;
			    if(i > 0){
			      x = 13;   
			    }else{
			      x = 5; 
			    }
			    
			    left = (i+1) * 15 + (i+1) * 18 + (i + 1) * x + textWidthHolder;*/
			    	
			    if(i > 0){
			    	left = (i*labelSpace + (i+1)*defRectWidth + (i+1)*rectAndTextSpace) + textWidthHolder;
			    }else{
			    	left = defRectWidth + rectAndTextSpace + textWidthHolder;
			    }	

			    return left;
			  })
			    .attr("y", 9)
			    .attr("dy", ".35em")
			    .style("text-anchor", "start")
			    .text(d.Name);

			  textWidthHolder += getTextWidth(d.Name, "15px", "calibri");
			});


			// Legend Placing
			d3.select(".legendHolder").attr("transform", function (d) {
			  thisWidth = d3.select(this).node().getBBox().width;
			  //return "translate(" + ((width) / 2 - thisWidth / 2) + ","+(0-(height+margin.top))+")"; //CHANGE
			  return "translate(" + (margin.left/2) + ","+(0-(height+margin.top))+")"; //CHANGE
			})
		},
		
		transposeMomGraph:function(event){
			
		},
		
		defaultMomUtilizationGraph:function(event){
			Data = JSON.parse(jsonGraphUtilizationMonth);

			/*var margin = { top: 80, right: 30, bottom: 160, left: 80 },
			    width = (countAllMonthData*numberOfOriginatorMonth*40) - margin.left - margin.right,
			    height = 700 - margin.top - margin.bottom;*/

			/* Revision 2017.08.10
			 * Consider if the number of Year and Originator is 1
			 * 
			 */
			
			var margin = { top: 80, right: 30, bottom: 160, left: 80 };
			var height = 700 - margin.top - margin.bottom;
			
			var requiredWidth = countAllYearData*numberOfOriginatorYear*40;
			var rightPlusLeft = margin.right+margin.left;
			var width = 0;
			if(requiredWidth > rightPlusLeft){
				width = requiredWidth - margin.left - margin.right;
			}else if(requiredWidth <= rightPlusLeft){
				width = requiredWidth + margin.left + margin.right;
			}
			
			var textWidthHolder = 0;
			var Originators = new Array();
			
			Originators.pro

			var Data;
			var ageNames;

			var x0 = d3.scale.ordinal().rangeRoundBands([0, width], .1);
			var XLine = d3.scale.ordinal().rangeRoundPoints([0, width], .1);
			var x1 = d3.scale.ordinal();

			var y = d3.scale.linear().range([height, 0]);

			var color = d3.scale.ordinal().range(["#039", "#339", "#639", "#939", "#C39", "#F39", "#0C9", "#3C9",
				"#6C9", "#9C9", "#CC9", "#FC9", "#03C", "#33C", "#63C", "#93C", "#C3C", "#F3C", "#0CC", "#3CC", "#6CC",
				"#9CC", "#CCC", "#FCC", "#03F", "#33F", "#63F", "#93F", "#C3F", "#F3F", "#0CF", "#3CF", "#6CF", "#9CF",
				"#CCF", "#FCF", "#060", "#360", "#660", "#960", "#C60", "#F60", "#0F0", "#3F0", "#6F0", "#9F0", "#CF0",
				"#FF0", "#063", "#363", "#663", "#963", "#C63", "#F63", "#0F3", "#3F3", "#6F3", "#9F3", "#CF3", "#FF3",
				"#066", "#366", "#666", "#966", "#C66", "#F66", "#0F6", "#3F6", "#6F6", "#9F6", "#CF6", "#FF6", "#069"]);
			
			var xAxis = d3.svg.axis()
			.scale(x0)
			.orient("bottom");
			
			var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.tickFormat(d3.format(".2s"));
			
			var svg = d3.select("#graphArea").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
			
			// Bar Data categories
			Data.forEach(function (d) {
			  d.Originators.forEach(function (b) {
			    if (Originators.findIndex(function (c) { return c.Name===b.Name}) == -1) {
			      b.Type = "bar";
			      Originators.push(b)
			    }
			  })
			});
			
			x0.domain(Data.map(function (d) { return d.Year; }));
			XLine.domain(Data.map(function (d) { return d.Year; }));
			x1.domain(Originators.filter(function (d) { return d.Type == "bar" }).map(function (d) { return d.Name})).rangeRoundBands([0, x0.rangeBand()]);
			y.domain([0, d3.max(Data, function (d) { return d3.max(d.Originators, function (d) { return d.Value; }); })]);
			
			svg.append("g")
			  .attr("class", "x axis")
			  .attr("transform", "translate(0," + height + ")")
			  .call(xAxis)
			  .selectAll("text")
			  .style("text-anchor","end")
			  .attr("dx","-.8em")
			  .attr("dy","-.15em")
			  .attr("transform", function(d){return "rotate(-65)"; });
			
			svg.append("g")
			  .attr("class", "y axis")
			  .call(yAxis)
			  .append("text")
			  .attr("transform", "rotate(-90)")
			  .attr("y", -margin.left)
			  .attr("dy", ".71em")
			  .style("text-anchor", "end")
			  .text("Utilization (%)");
			
			var state = svg.selectAll(".state")
			.data(Data)
			.enter().append("g")
			.attr("class", "state")
			.attr("transform", function (d) { return "translate(" + x0(d.Year) + ",0)"; });
			
			state.selectAll("rect")
			  .data(function (d) { return d.Originators; })
			  .enter().append("rect")
			  .attr("width", x1.rangeBand())
			  .attr("x", function (d) { return x1(d.Name); })
			  .attr("y", function (d) { return y(d.Value); })
			  .attr("height", function (d) { return height - y(d.Value); })
			  .style("fill", function (d) { return color(d.Name); })
			  .transition().delay(500).attrTween("height", function (d) {
			  var i = d3.interpolate(0, height - y(d.Value));
			  return function (t)
			  {
			    return i(t);
			  }
			});
			
			state.selectAll("text")
			.data(function (d) { return d.Originators; })
			.enter().append("text")
			.text(function(d){
			  return d3.format(",")(d.Value);
			})
			.attr("transform","rotate(-90)")
			.attr("x",function(d){
			  return -(y(d.Value))+5;
			})
			.attr("y", function(d){
			  return x1(d.Name)+(x1.rangeBand()/2)+5;
			});
			
			// Legends
			var defRectWidth = 18;
	    	var rectAndTextSpace = 10;
	    	var labelSpace = 15;
	    	
			var LegendHolder = svg.append("g").attr("class", "legendHolder");
			var legend = LegendHolder.selectAll(".legend")
			.data(Originators.map(function (d) { return {"Name":d.Name,"Type":d.Type}}))
			.enter().append("g")
			.attr("class", "legend")
			.attr("transform", function (d, i) { return "translate(0," +( height+ margin.bottom/2 )+ ")"; })
			.each(function (d,i) {

			//  Legend Symbols
			d3.select(this).append("rect")
			    .attr("width", function () { return 18 })
			    .attr("x", function (b) {

			    left = (i*defRectWidth + i*rectAndTextSpace + i*labelSpace) + textWidthHolder;
			    return left;
			  })
			    .attr("y", function (b) { return b.Type == 'bar'?0:7})
			    .attr("height", function (b) { return b.Type== 'bar'? 18:5 })
			    .style("fill", function (b) { return b.Type == 'bar' ? color(d.Name) : LineColor(d.Name) });

			  //  Legend Text

			  d3.select(this).append("text")
			    .attr("x", function (b) {
			    	
		    	if(i > 0){
			    	left = (i*labelSpace + (i+1)*defRectWidth + (i+1)*rectAndTextSpace) + textWidthHolder;
			    }else{
			    	left = defRectWidth + rectAndTextSpace + textWidthHolder;
			    }	
			    return left;
			  })
			    .attr("y", 9)
			    .attr("dy", ".35em")
			    .style("text-anchor", "start")
			    .text(d.Name);

			  textWidthHolder += getTextWidth(d.Name, "15px", "calibri");
			});


			// Legend Placing

			d3.select(".legendHolder").attr("transform", function (d) {
			  thisWidth = d3.select(this).node().getBBox().width;
			  return "translate("+(margin.left/2)+",0)";
			})
		}
	}
})