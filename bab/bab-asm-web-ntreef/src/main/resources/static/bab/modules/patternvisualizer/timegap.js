new Vue({
  el: '#app',
  components: {
	  'loader': LoaderComponent,
	  'processmodel': TimegapComponent  
  },
  data: {
	isProcessing: false,
	lrData: undefined,
    pmData: {
		options: {
	        is3D: false,
	        rankdir: "TB",
	        arctype: 'bundle',
	        isLogReplay: false,
	        type: 'timegap',
	        renderer: 'dagred3'
	      }
    },
    apiURI: {
    	processModel: apiURI.analysis.timegap+jsonData.workspaceId+'/'+jsonData.datasetId
    	+'/'+jsonData.sdt+'/'+jsonData.edt
    }
  }
});