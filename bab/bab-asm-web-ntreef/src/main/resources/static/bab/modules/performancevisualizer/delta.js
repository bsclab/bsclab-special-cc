/* author @superpikar */

var DELAY = 500;
var INTERVALDURATION = 3000;
var intervalDuration = 3000;

new Vue({
	el: '#app',
	components: {
		'loader': LoaderComponent
	},
	data: {
		step: 'one',
		isProcessing: false,
		isProcessing2: false,
		isProcessing3: false,
		isProcessing4: false,
		isProcessing5: false,
		repositories: [],
		message: undefined,
		data: undefined,
		setting: {
			asisText: 'Expected',
			tobeText: 'Observed',
			asisType: 'repository',
			tobeType: 'repository',
			asisRepository: undefined,			
			tobeRepository: undefined,			
			asisActivity: undefined,			
			tobeActivity: undefined,			
			asisActivities: {},			
			tobeActivities: {},	
//			mapActivities: {}
			mapActivities: []
		}
	},
	ready: function(){
		this.isProcessing = true;
		var repositoriesURI = apiURI.repository.repositories
						+ jsonData.workspaceId
						+ '/' + jsonData.datasetId
						+ '/' + jsonData.sdt
						+ '/' + jsonData.edt;
		var jobQueueURI = apiURI.job.queue;

		
		console.log("[STEP1-1] request : " + repositoriesURI);
		this.$http.get(repositoriesURI).then(function(response) {
			repositoriesRes = response;
			repositoriesResStatus = repositoriesRes.data.status;
			
			if (repositoriesResStatus == 'FINISHED') {
				this.isProcessing = false;
				this.initializeRepositories(repositoriesRes.data.response);
			} else if (repositoriesResStatus == 'FAILED') {
				this.isProcessing = false;
				console.log("error");
			} else if (repositoriesResStatus == 'RUNNING' || repositoriesResStatus == 'QUEUE') {
				var repositoriesQueueChecker = window.setInterval(_.bind(function() {
					console.log("[STEP1-2] get Repositories's Queue request : " + jobQueueURI
							+ repositoriesRes.data.request.jobId);
					this.$http.get(jobQueueURI + repositoriesRes.data.request).then(function(response) {
						var repositoriesQueueRes = response;
						var repositoriesQueueResStatus = repositoriesQueueRes.data.jobQueueStatus;
						
						if (repositoriesQueueResStatus == 'RUNNING') {
							
						} else if (repositoriesQueueResStatus == 'FAILED') {
							window.clearInterval(repositoriesQueueChecker);
							console.log('FAILED')
						} else if (repositoriesQueueResStatus == 'FINISHED') {
							window.clearInterval(repositoriesQueueChecker);
							console.log("[STEP1-3] get finished summary view request : " + summaryViewURI);
							this.$http.get(repositoriesURI).then(function(response) {
								console.log(response);
								var repositoriesRes = response;
								var repositoriesResStatus = repositoriesRes.data.status;
								if (repositoriesResStatus == 'FINISHED') {
									this.initializeRepositories(repositoriesRes.data.response);
								} else {
									console.log("error")
								}
							}, function(response) {
								this.isProcessing = false;
							});
						}
					}, function(response) {
						this.isProcessing = false;
					})
				}, this), intervalDuration);
			}
		}, function() {
			this.isProcessing = false;
		});
		
		
		
//		this.$http.get(APIURI).then(function (response) {
//			if(response.data.status == 'FINISHED'){
//				this.initializeRepositories(response.data.response);
//				this.isProcessing = false;
//			}
//			else if(response.data.status == 'FAILED' || response.data.status == 'UNKNOWN'){
//				// if status is not FINISHED or RUNNING
//				this.isProcessing = false;		
//		    }
//			else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUED'){
//				var intervalId = window.setInterval(_.bind(function(){
//					console.log('this is check every '+INTERVALDURATION+'ms');
//					this.$http.get(JOBQUEUEURI).then(function(response2){
//						// success callback
//						var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.jobId}));
//						var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.jobId}));
//					  
//						if(isJobInQueue || isJobInRunning){
//							console.log('current status is QUEUE/RUNNING');
//						}
//						else{
//							console.log('current status is FINISHED, request POST');
//							this.$http.get(APIURI).then(function (response3){
//								if(response3.data.status == 'FINISHED'){
//									window.clearInterval(intervalId)
//									this.initializeRepositories(response3.data.response);
//									this.isProcessing = false;
//								}
//							}, function(response3){
//								this.isProcessing = false;
//							});
//					  }
//				  }, function(response2){
//					  // error callback
//					  this.isProcessing = false;
//				  })	  
//				}, this), INTERVALDURATION);	
//			}	        
//	    }, function (response) {	
//	    	// error callback
//	    	this.isProcessing = false;
//	    	alert('cannot get data');
//	    });
		
	},
	methods: {
		initializeRepositories: function(response){
			// bind this to set timeout http://stackoverflow.com/posts/8800171/revisions
			var extension = '.brepo';
			response.forEach(function(val, key){
				if(val.id.indexOf(extension)!=-1){	// if contains .brepo extension
					val.id = val.id.substring(0, val.id.length - extension.length);						
				}
				val.workspaceId = jsonData.workspaceId;
				val.datasetId = jsonData.datasetId;					
			});
			
			window.setTimeout(_.bind(function(){
				// set data on vm
				this.$set('repositories', response);
				this.isProcessing = false;					
			}, this), DELAY)
		},
		initializeAsisActivities: function(response){
			this.$set('setting.asisActivities', response.activities);					
			var keys = _.keys(this.setting.asisActivities);
			this.asisActivity = keys[0];
			console.log('this.asisActivity', this.asisActivity);
		},
		initializeTobeActivities: function(response){
			this.$set('setting.tobeActivities', response.activities);
			var keys = _.keys(this.setting.tobeActivities);
			this.tobeActivity = keys[0];
			console.log('this.tobeActivity', this.tobeActivity);
		},
		initializeProcessModel: function(response){
			this.$set('data', response);
			this.isProcessing5 = true;
			var options = {
				is3D: false,
		        rankdir: "TB",
		        arctype: 'bundle',
		        isLogReplay: false,
		        type: 'heuristic',
		        multiple: true,
		        renderer: 'dagred3'
			};
			
			var asisNodeDifferences = response.asisModel.nodeDifferences ; 
			var asisArcDifferences = response.asisModel.arcDifferences ;
			var tobeNodeDifferences = response.tobeModel.nodeDifferences ; 
			var tobeArcDifferences = response.tobeModel.arcDifferences ;
			
			var asisModel = response.asisModel.model;
			var tobeModel = response.tobeModel.model;
			
			for(var i in asisNodeDifferences){
				if(asisNodeDifferences[i] == 1){
					asisModel.nodes[i].difference = true;
				}
			}
			for(var i in tobeNodeDifferences){
				if(tobeNodeDifferences[i] == 1){
					tobeModel.nodes[i].difference = true;
				}
			}
			
			for(var i in asisArcDifferences){
				if(asisArcDifferences[i] == 1){
					asisModel.arcs[i].difference = true;
				}
			}
			for(var i in tobeArcDifferences){
				if(tobeArcDifferences[i] == 1){
					tobeModel.arcs[i].difference = true;
				}
			}				
			
			var traceLevelKPI = {
				data: [],
				columns: [
				    { data: 'index', title:'#' },
				    { data: 'key', title:'Trace' },
				    { data: 'value', title:'Value' }
				]
			};
			var count = 0;
			for(var i in response.traceTPIs){
				count++;
				traceLevelKPI.data.push({ index:count, key:i, value: response.traceTPIs[i] });
			}
			
			var activityLevelKPI = {
				data: [],
				columns: [
				    { data: 'index', title:'#' },
				    { data: 'key', title:'Activity' },
				    { data: 'value', title:'Value' }
				]
			};
			var count = 0;
			for(var i in response.activityTPIs){
				count++;
				activityLevelKPI.data.push({ index:count, key:i, value: response.activityTPIs[i] });
			}
			
    		window.setTimeout(_.bind(function(){
    			var processModel = new ProcessModel('#expected-processmodel .processmodel-content', response.asisModel.model.nodes, response.asisModel.model.arcs, options);
    			processModel.drawProcessModel();
    			
    			var processModel2 = new ProcessModel('#observed-processmodel .processmodel-content', response.tobeModel.model.nodes, response.tobeModel.model.arcs, options);
    			processModel2.drawProcessModel();
    			this.isProcessing5 = false;
    			
    			$('#trace-level-tpi').DataTable({
    				paging: true,
                    filter: true,
                    sort: true,
                    info: false,
                    autoWidth: false,
    			    data: traceLevelKPI.data,
    			    columns: traceLevelKPI.columns
    			});
    			
    			$('#activity-level-tpi').DataTable({
    				paging: true,
                    filter: true,
                    sort: true,
                    info: false,
                    autoWidth: false,
    			    data: activityLevelKPI.data,
    			    columns: activityLevelKPI.columns
    			});
    			
    		}, this), DELAY);
		},
		openFirstModal: function(){
			this.step = 'one';
		},
		openSecondModal: function(){
			if(_.isUndefined(this.setting.asisRepository) || _.isUndefined(this.setting.tobeRepository)){
				console.log('please select repo');
				this.message = 'Please select the repository';				
			}
			else{
				console.log('repo is already selected');
				this.step = 'two';
				this.message = undefined;				
				
				this.setting.asisActivities = [];			
				this.setting.tobeActivities = [];	
				console.log('second modal is opened', this.setting);
				
				this.isProcessing2 = true;
				var ASISAPIURI = apiURI.repository.view+ jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt;
				var TOBEAPIURI = apiURI.repository.view+ jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt;
				var JOBQUEUEURI = apiURI.job.queue;
				
				this.$http.get(ASISAPIURI).then(function(response) {
					var asisRes = response;
					var asisResStatus = response.data.status;
					if (asisResStatus == 'FINISHED') {
						this.initializeAsisActivities(asisRes.data.response);
						this.isProcessing2 = false;
					} else if (asisResStatus == 'FAILED' || asisResStatus == 'UNKNOWN') {
						this.isProcessing2= false;
						console.log("FAILED")
					} else if (asisResStatus == 'RUNNING' || asisResStatus == 'QUEUE') {
						var asisQueueChecker = window.setinterval(_.bind(function() {
							this.$http.get(JOBQUEUEURI + asisRes.data.request.jobId).then(function(response) {
								var asisQueueRes = response;
								var asisQueueResStatus = response.data.jobQueueStatus;
								if (asisQueueResStatus == 'RUNNING') {
									
								} else if (asisQueueResStatus == 'FAILED') {
									window.clearInterval(asisQueueChecker);
									console.log("FAILED");
								} else if (asisQueueResStatus == 'FINISHED') {
									window.clearInterval(asisQueueChecker);
									this.$http.get(ASISAPIURI).then(function(response) {
										var asisRes = response;
										var asisResStatus = response.data.status;
										if(asisResStatus == 'FINISHED'){
											this.initializeAsisActivities(asisRes.data.response);	
										} else {
											alert("error")
										}
									}, function(response) {
										this.isProcessing2 = false;
									})
								}
							}, function(response) {
								this.isProcessing2 = false;
							});
						}, this), intervalDuration);
					}
				}, function() {
					this.isProcessing2 = false;
				})
				
				
				this.$http.get(TOBEAPIURI).then(function(response) {
					var tobeRes = response;
					var tobeResStatus = response.data.status;
					if (tobeResStatus == 'FINISHED') {
						this.initializeTobeActivities(tobeRes.data.response);
						this.isProcessing2 = false;
					} else if (tobeResStatus == 'FAILED' || tobeResStatus == 'UNKNOWN') {
						this.isProcessing2= false;
						console.log("FAILED")
					} else if (tobeResStatus == 'RUNNING' || tobeResStatus == 'QUEUE') {
						var tobeQueueChecker = window.setinterval(_.bind(function() {
							this.$http.get(JOBQUEUEURI + tobeRes.data.request.jobId).then(function(response) {
								var tobeQueueRes = response;
								var tobeQueueResStatus = response.data.jobQueueStatus;
								if (tobeQueueResStatus == 'RUNNING') {
									
								} else if (tobeQueueResStatus == 'FAILED') {
									window.clearInterval(tobeQueueChecker);
									console.log("FAILED");
								} else if (tobeQueueResStatus == 'FINISHED') {
									window.clearInterval(tobeQueueChecker);
									this.$http.get(TOBEAPIURI).then(function(response) {
										var tobeRes = response;
										var tobeResStatus = response.data.status;
										if(tobeResStatus == 'FINISHED'){
											this.initializeTobeActivities(tobeRes.data.response);	
										} else {
											alert("error")
										}
									}, function(response) {
										this.isProcessing2 = false;
									})
								}
							}, function(response) {
								this.isProcessing2 = false;
							});
						}, this), intervalDuration);
					}
				}, function() {
					this.isProcessing2 = false;
				})
			}
//		},
				
				
				
//				this.$http.get(ASISAPIURI).then(function (response) {
//					if(response.data.status == 'FINISHED'){
//						this.initializeAsisActivities(response.data.response);
//						this.isProcessing2 = false;
//					}
//					else if(response.data.status == 'FAILED' || response.data.status == 'UNKNOWN'){
//						// if status is not FINISHED or RUNNING
//						this.isProcessing2 = false;				
//				    }
//					else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUED'){
//						var intervalId = window.setInterval(_.bind(function(){
//							console.log('this is check every '+INTERVALDURATION+'ms');
//							this.$http.get(JOBQUEUEURI).then(function(response2){
//								// success callback
//								var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.jobId}));
//								var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.jobId}));
//							  
//								if(isJobInQueue || isJobInRunning){
//									console.log('current status is QUEUE/RUNNING');
//								}
//								else{
//									console.log('current status is FINISHED, request POST');
//									this.$http.get(ASISAPIURI).then(function (response3){
//										if(response3.data.status == 'FINISHED'){
//											window.clearInterval(intervalId)
//											this.initializeAsisActivities(response3.data.response);
//											this.isProcessing2 = false;
//										}
//									}, function(response3){
//										this.isProcessing2 = false;
//									});
//							  }
//						  }, function(response2){
//							  // error callback
//							  this.isProcessing2 = false;
//						  })	  
//						}, this), INTERVALDURATION);	
//					}					
//				}, function (response) {	
//					// error callback
//					this.isProcessing2 = false;
//					alert('cannot get data');
//				});
//				
//				this.isProcessing3 = true;
//				this.$http.get(TOBEAPIURI).then(function (response) {
//					if(response.data.status == 'FINISHED'){
//						this.initializeAsisActivities(response.data.response);
//						this.isProcessing3 = false;
//					}
//					else if(response.data.status == 'FAILED' || response.data.status == 'UNKNOWN'){
//						// if status is not FINISHED or RUNNING
//						this.isProcessing3 = false;				
//				    }
//					else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUED'){
//						var intervalId = window.setInterval(_.bind(function(){
//							console.log('this is check every '+INTERVALDURATION+'ms');
//							this.$http.get(JOBQUEUEURI).then(function(response2){
//								// success callback
//								var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.jobId}));
//								var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.jobId}));
//							  
//								if(isJobInQueue || isJobInRunning){
//									console.log('current status is QUEUE/RUNNING');
//								}
//								else{
//									console.log('current status is FINISHED, request POST');
//									this.$http.get(TOBEAPIURI).then(function (response3){
//										if(response3.data.status == 'FINISHED'){
//											window.clearInterval(intervalId)
//											this.initializeAsisActivities(response3.data.response);
//											this.isProcessing3 = false;
//										}
//									}, function(response3){
//										this.isProcessing3 = false;
//									});
//							  }
//						  }, function(response2){
//							  // error callback
//							  this.isProcessing3 = false;
//						  })	  
//						}, this), INTERVALDURATION);	
//					}
//				}, function (response) {	
//					// error callback
//					this.isProcessing3 = false;
//					alert('cannot get data');
//				});				
//			}			
		},
		mapActivity: function(asisActivity, tobeActivity){
			var map = _.find(this.setting.mapActivities, {asis: asisActivity});
			if(_.isUndefined(map)){
				this.setting.mapActivities.push({asis:asisActivity, tobe:tobeActivity});				
			}
			else{
				map.tobe = tobeActivity;
			}
			console.log(this.setting.mapActivities);
		},
		removeMapActivity: function(index){
			this.setting.mapActivities.splice(index,1);
		},
		submitDelta: function(){
			this.step = 'three';
			
			// if empty then initialize at least one map
//			if(_.isEmpty(this.setting.mapActivities)){
//				this.setting.mapActivities.push({asis:this.asisActivity, tobe:this.tobeActivity});
//			}
//			console.log(this.setting.mapActivities);
			
			this.isProcessing4 = true;
			var args = {
				repositoryURI: '/bab/workspaces/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+this.setting.asisRepository+'/'+this.setting.asisRepository,
				compareToURI: '/bab/workspaces/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+this.setting.tobeRepository+'/'+this.setting.tobeRepository,
//				repositoryURI: '/bab/workspaces/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.sdt+'/'+jsonData.edt,
//				compareToURI: '/bab/workspaces/'+jsonData.workspaceId+'/'+jsonData.datasetId+'/'+jsonData.sdt+'/'+jsonData.edt
			}
			
			
			if(!_.isEmpty(this.setting.mapActivities)){
				args.map = {};
				this.setting.mapActivities.forEach(function(val, key){
					args.map[val.tobe] = val.asis
				});
			}			
			var APIURI = apiURI.analysis.delta + jsonData.workspaceId + '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt;
			var JOBQUEUEURI = apiURI.job.queue;
			var intervalDuration = 3000;
			
			this.$http.post(APIURI, args).then(function(response) {
				var deltaRes = response;
				var deltaResStatus = response.data.status;
				if (deltaResStatus == 'FINISHED') {
					this.isProcessing4 = false;
					this.initializeProcessModel(deltaRes.data.response);
				} else if (deltaResStatus == 'FAILED' || deltaResStatus == 'UNKNOWN') {
					this.isProcessing4=false;
				} else if (deltaResStatus == 'RUNNING' || deltaResStatus == 'QUEUED') {
					var deltaQueueChecker = window.setInterval(_.bind(function() {
						this.$http.get(JOBQUEUEURI + deltaRes.data.request.jobId).then(function(response) {
							var deltaQueueRes = response;
							var deltaQueueResStatus = response.data.status;
							if (deltaQueueResStatus == 'RUNNING') {
								
							} else if (deltaQueueResStatus == 'FAILED') {
								this.isProcessing4=false;
								window.clearInterval(deltaQueueChecker);
								console.log("failed")
							} else if (deltaQueueResStatus == 'FINISHED') {
								window.clearInterval(deltaQueueChecker);
								this.$http.post(APIURI, args).then(function(response) {
									var deltaRes = response;
									var deltaResStatus = response.data.status;
									if(deltaResStatus == 'FINISHED'){
										this.initializeProcessModel(deltaRes.data.response);	
									} else {
										alert("error")
									}
								}, function(response) {
									this.isProcessing4=false;
									console.log(response);
								})
							}
							
						}, function(response) {
							this.isProcessing4=false;
						})
					}, this), intervalDuration);
				}
			})
			
			
			
			
			
//			this.$http.post(APIURI, args).then(function (response) {
//				if(response.data.status == 'FINISHED'){
//					window.setTimeout(_.bind(function(){
//						this.initializeProcessModel(response.data.response);
//						this.isProcessing4 = false;						
//					}, this), DELAY);
//				}
//				else if(response.data.status == 'FAILED' || response.data.status == 'UNKNOWN'){
//					// if status is not FINISHED or RUNNING
//					this.isProcessing4 = false;				
//			    }
//				else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUED'){
//					var intervalId = window.setInterval(_.bind(function(){
//						console.log('this is check every '+INTERVALDURATION+'ms');
//						this.$http.get(JOBQUEUEURI).then(function(response2){
//							// success callback
//							var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.jobId}));
//							var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.jobId}));
//						  
//							if(isJobInQueue || isJobInRunning){
//								console.log('current status is QUEUE/RUNNING');
//							}
//							else{
//								console.log('current status is FINISHED, request POST');
//								this.$http.post(APIURI, args).then(function (response3){
//									if(response3.data.status == 'FINISHED'){
//										window.clearInterval(intervalId)
//										this.initializeProcessModel(response3.data.response);
//										this.isProcessing4 = false;
//									}
//								}, function(response3){
//									this.isProcessing4 = false;
//								});
//						  }
//					  }, function(response2){
//						  // error callback
//						  this.isProcessing4 = false;
//					  })	  
//					}, this), INTERVALDURATION);	
//				}				
//			}, function (response) {	
//				// error callback
//				this.isProcessing4 = false;
//				alert('cannot get data');
//			});				
		},
	}
})