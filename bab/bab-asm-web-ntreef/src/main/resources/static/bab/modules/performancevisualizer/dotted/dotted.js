/*
 * author @taufiknuradi (taufik.nur.adi@gmail.com)
 * */

new Vue({
	el: '#app', components:{
		loader:LoaderComponent
	},
	data:{
		currentURL: document.URL,
		isBirdView:false,
		isTranspose:false,
		isRelative:false,
		topLabelLogical:true,
		isProcessing: false,
		svgHeight:600,
		svgWidth:815, //default 900
		jobQueueURI: "",
		intervalDuration:0
	},
	ready: function(){
		var self = this;
		var graphArea = d3.select("#graphArea");
		
		this.isProcessing = true;
		this.defaultDotted();
		this.sliderHandler();
		
		this.jobQueueURI = apiURI.job.queue;
		this.intervalDuration = 3000;
		
		/* Inspired from http://jsfiddle.net/bMyGY/1/ */
		/*$('#singleEventColors').on('click','span', function(event){
			var colorOptions = $(this).attr('color-options');
			$('#colorOptions_'+colorOptions).toggle();
		});*/
		
		
		/*var dotted = d3.select("#dotted g");
		dotted.call(d3.behavior.zoom().scaleExtent([1,4]).on("zoom", dotted.attr("transform","translate("+d3.event.translate+")scale("+d3.event.scale+")")));
		
		function zoomed(){
			dotted.attr("transform","translate("+d3.event.translate+")scale("+d3.event.scale+")");
		}*/
	},
	methods: {
		sliderHandler: function(event){
			var self = this;
			var hscale = document.getElementById("hscale");
			var vscale = document.getElementById("vscale");
			var hzoom = document.getElementById("hzoom");
			var vzoom = document.getElementById("vzoom");
			var maxHorizontal = 24400;
			var maxVertical = 21400;
			
			
			noUiSlider.create(hscale,{
				start:0,
				range:{
					'min':0,
					'max':maxHorizontal
				}
			});
			
			noUiSlider.create(vscale,{
				start:0,
				range:{
					'min':0,
					'max':maxVertical
				}
			});
			
			noUiSlider.create(hslider,{
				start:0,
				range:{
					'min':0,
					'max':maxHorizontal
				}
			});
			
			noUiSlider.create(vslider,{
				start:0,
				orientation:'vertical',
				range:{
					'min':0,
					'max':maxVertical
				}
			});
			
			
			vscale.noUiSlider.on("update",function(){
				var dotted = d3.select("#dotted");
				var dottedLeftLabel = d3.select("#dottedLeftLabel");
				var rect = d3.selectAll("#dotted g rect");
				var localSvgHeight = self.svgHeight + parseInt(vscale.noUiSlider.get());				
				
				dotted.attr("viewBox","0 0 "+self.svgWidth+" "+localSvgHeight);
				dottedLeftLabel.attr("viewBox","0 0 85 "+localSvgHeight);
			});
			
			hscale.noUiSlider.on("update",function(){
				var dotted = d3.select("#dotted");
				var dottedTopLabel = d3.select("#dottedTopLabel");
				var rect = d3.selectAll("#dotted g rect");
				var localSvgWidth = self.svgWidth + parseInt(hscale.noUiSlider.get());
				
				//document.getElementById("sliderValue").innerHTML = self.svgWidth+" "+hscale.noUiSlider.get()+" "+localSvgWidth;
				dotted.attr("viewBox","0 0 "+localSvgWidth+" "+self.svgHeight);
				dottedTopLabel.attr("viewBox","0 0 "+localSvgWidth+" 40");
				rect.attr("width",localSvgWidth);
			});
			
			vslider.noUiSlider.on("update",function(){
				var gAllCircle = d3.selectAll("#dotted g#allCircle");
				var gAllLeftLabel = d3.selectAll("g#leftLabel");
				var posVertical = -1*vslider.noUiSlider.get();
				
				//document.getElementById("sliderValue").innerHTML = posVertical;
				gAllLeftLabel.attr("transform","translate(0,"+posVertical+")");
				gAllCircle.attr("transform","translate(0,"+posVertical+")");
			});
			
			hslider.noUiSlider.on("update",function(){
				var gAllCircle = d3.selectAll("#dotted g#allCircle");
				//var gAllLeftLabel = d3.selectAll("#dotted g#leftLabel");
				var gAllTopLogical = d3.selectAll("g#topLabelLogical");
				var posHorizontal = -1*hslider.noUiSlider.get();
				
				//document.getElementById("sliderValue").innerHTML = posHorizontal;
				//gAllLeftLabel.attr("transform","translate("+posHorizontal+",0)");
				gAllTopLogical.attr("transform","translate("+posHorizontal+",0)");
				gAllCircle.attr("transform","translate("+posHorizontal+",0)");				
			});
			
		},		
		dotInfo: function(event){
			//Credit : http://stackoverflow.com/questions/20269384/how-do-you-remove-a-handler-using-a-d3-js-selector
			d3.selectAll("#dotted g circle").on("mouseover",null).on("mouseout",null);
			d3.selectAll("#dotted g circle")
			.on("mouseover",function(){
				
				if(self.topLabelLogical){
					var dataTime = d3.select(this).attr("data-time");
					//var dataTime = d3.select(this).attr("data-time-nl");
				}else{
					var dataTime = d3.select(this).attr("data-time-nl");
				}
				
				var dataComponent = d3.select(this).attr("data-component");
				var dataColor = d3.select(this).attr("data-color");
				
				d3.select("#currentTime").html(dataTime);
				d3.select("#currentComponent").html(dataComponent);
				d3.select("#currentColor").html(dataColor);
			})
			.on("mouseout",function(){
				d3.select("#currentTime").html("Current TIME");
				d3.select("#currentComponent").html("Current COMPONENT");
				d3.select("#currentColor").html("Current COLOR");
			});
		},
		changeDotColor: function(event){
			var cssEventColorsName = "cec";
			var id = event.target.id;
			var name = event.target.name;
			var value = event.target.value;
			var valueLength = value.length;
			var colorIndex = value.substring(valueLength-(valueLength-4),valueLength); 
			
			//Change the dotted color
			$("circle[data-color='"+name+"']").attr("class",value);
			
			//Change the dotted legend color (Single Event Colors legend).
			$("#"+id).attr("class",cssEventColorsName+colorIndex);
		},
		defaultDotted: function(event){
			var graphArea = d3.select("#graphArea");
			console.log(this.currentURL+'/Default/'+colorIndex);
			this.$http.get({url:this.currentURL+'/Default/'+colorIndex , method: 'GET'}).then(function(response){
				graphArea.html(response.data);
				this.dotInfo();
				this.isProcessing = false;
			}, function (response){
				this.isProcessing = false;
			});
//			var dotURI = this.currentURL+'/Default/'+colorIndex;
//			this.$http.get(dotURI).then(function (response) {
//				var dotRes = response;
//				var dotResStatus = dotRes.data.status;
//				console.log(dotRes)
//				console.log(dotResStatus)
//				if (dotResStatus == 'FINISHED') {
//					graphArea.html(dotRes.data);
//					this.dotInfo();
//					this.isProcessing = false;
//				} else if (dotResStatus == 'FAILED') {
//					this.isProcessing = false;
//					console.log('failed')
//				} else if (dotResStatus == 'RUNNING' || dotResStatus == 'QUEUE') {
//					var dotQueueChecker = window.setInterval(_.bind(function() {
//						this.$http.get(this.jobQueueURI + dotRes.data.request.jobId)
//							.then(function(response) {
//							var dotQueueRes = response;
//							var dotQueueResStatus = dotQueueRes.data.jobQueueStatus;
//							
//							if (dotQueueResStatus == 'RUNNING') {
//							} else if (dotQueueResStatus == 'FAILED') {
//								window.clearInterval(dotQueueChecker);
//								this.isProcessing = false;
//							} else if (dotQueueResStatus == 'FINISHED') {
//								window.clearInterval(dotQueueChecker);
//								this.$http.get(dotURI).then(function (response) {
//									var dotRes = response;
//									var dotResStatus = dotRes.data.status;
//									if (dotRes == 'FINISHED') {
//										graphArea.html(dotRes.data);
//										this.dotInfo();
//										this.isProcessing = false;
//									} else
//										console.log('FAILED')
//								}, function(response) {
//									this.isProcessing = false;
//									console.log('FAILED')
//								});
//							}
//						}, function(response) {
//							this.isProcessing = false;
//							console.log('FAILED')
//						})
//					}, this), this.intervalDuration)
//				} 
//				
//			}, function(response) {
//				this.isProcessing = false;
//				console.log('FAILED')
//			});
		},
		birdView: function(event){
			var dotted = d3.select("#dotted");
			var rect = d3.selectAll("#dotted g rect");
			var topLabel = d3.selectAll("#dottedTopLabel");
			var leftLabel = d3.selectAll("#dottedLeftLabel");
			var bvButton = d3.select("#birdview");
			
			if(!this.isBirdView){
				var gAllCircle = d3.selectAll("#dotted g#allCircle");
				var gAllLeftLabel = d3.selectAll("g#leftLabel");
				var gAllTopLogical = d3.selectAll("g#topLabelLogical");
				gAllCircle.attr("transform","translate(0,0)");
				gAllLeftLabel.attr("transform","translate(0,0)");
				gAllTopLogical.attr("transform","translate(0,0)");
				$("#vslider").hide();
				$("#hslider").hide();
				$("#dottedLeftLabel").hide();
				this.isBirdView = true;				
				if(this.isTranspose){
					dotted.attr("viewBox","0 0 24000 600");
					topLabel.attr("viewBox","0 0 24000 40");
					leftLabel.attr("viewBox","0 0 24000 600");
					rect.attr("width","24000");
				}else{
					dotted.attr("viewBox","0 0 815 21000");
					topLabel.attr("viewBox","0 0 815 21000");
					leftLabel.attr("viewBox","0 0 85 21000");
				}
				bvButton.attr("class","ui active button");
			}else{
				$("#vslider").show();
				$("#hslider").show();
				$("#dottedLeftLabel").show();
				this.isBirdView = false;
				dotted.attr("viewBox","0 0 815 600");
				topLabel.attr("viewBox","0 0 815 40");
				leftLabel.attr("viewBox","0 0 85 600");
				bvButton.attr("class","ui button");
			}				
		},
		transposeDotted:function(event){
			var graphArea = d3.select("#graphArea");
			var trButton = d3.select("#transpose");
			
			if(this.isTranspose){
				this.isTranspose = false;
				this.isProcessing = true;
				
				this.defaultDotted();
				trButton.attr("class","ui button");
			}else{
				this.isTranspose = true;
				this.isProcessing = true;
				
				this.$http.get({url: this.currentURL+'/Transpose/'+colorIndex, method: 'GET'}).then(function(response){
					graphArea.html(response.data);
					this.dotInfo();
					this.isProcessing = false;
				}, function (response){
					this.isProcessing = false;
				});
				trButton.attr("class","ui active button");
			}
		},		
		logicalTime: function(event){
			if(this.isTranspose){
				/* Becarefull !!!
				 * TopLabel in the traspose mode means LeftLabel 
				 * */
				
				var tll = d3.select("#leftLabel");
				var tlnl = d3.select("#leftLabelNonLogical");
				var ltButton = d3.select("#logicaltime");
				
				if(self.topLabelLogical){
					this.topLabelLogical = false;
					tlnl.attr("style",null);
					tll.attr("style","visibility:hidden");
					ltButton.attr("class","ui active button");
				}else{
					self.topLabelLogical = true;
					tlnl.attr("style","visibility:hidden");
					tll.attr("style",null);
					ltButton.attr("class","ui button");
				}				
			}else{
				var tll = d3.select("#topLabelLogical");
				var tlnl = d3.select("#topLabelNonLogical");
				var ltButton = d3.select("#logicaltime");
				
				if(self.topLabelLogical){
					self.topLabelLogical = false;
					tlnl.attr("style",null);
					tll.attr("style","visibility:hidden");
					ltButton.attr("class","ui active button");
				}else{
					self.topLabelLogical = true;
					tlnl.attr("style","visibility:hidden");
					tll.attr("style",null);
					ltButton.attr("class","ui button");
				}
			}
		},
		relativeDotted: function(event){
			var graphArea = d3.select("#graphArea");
			var rlButton = d3.select("#relative");
			
			if(this.isRelative){
				this.isRelative = false;
				this.isProcessing = true;
				
				this.defaultDotted();
				/*this.$http.get({url:this.currentURL+'/Default/'+colorIndex , method: 'GET'}).then(function(response){
					graphArea.html(response.data);
					this.dotInfo();
					this.isProcessing = false;
				}, function (response){
					this.isProcessing = false;
				});*/
				
				rlButton.attr("class","ui button");	
			}else{
				this.isRelative = true;
				this.isProcessing = true;
				
				this.$http.get({url: this.currentURL+'/Relative/'+colorIndex, method: 'GET'}).then(function(response){
					graphArea.html(response.data);
					this.dotInfo();
					this.isProcessing = false;
				}, function (response){
					this.isProcessing = false;
				});
				
				rlButton.attr("class","ui active button");
			}
		}
	}
})