/*
 * author @superpikar (dzulfikar.adiputra@gmail.com)
 * */

// parse json data from view with id #json-data
//var jsonData = JSON.parse(document.getElementById('json-data').innerHTML).response;
console.log('this is json data', jsonData);

// vue initialization
new Vue({
	el: '#app',
	components: {
		'fileupload': FileUploadComponent, 
		'loader': LoaderComponent
	},
	data: {
		isProcessing: false,
		datasets: [],
		repositories: []
	},
	ready: function(){
		this.isProcessing = true;
		var URI = apiURI.repository.datasets + jsonData.workspaceId;
		this.$http.get(URI).then(function (response) {
	        // set data on vm
			response.data.response.forEach(function(val, key){
				val.workspaceId = jsonData.workspaceId;
//				val.id = val.id.substring(0, val.id.length - 4);
				val.active = false;
			});
	        this.$set('datasets', response.data.response);
	        this.isProcessing = false;
	        
	    }, function (response) {	
	    	// error callback
	    	alert('cannot get data');
	    	this.isProcessing = false;
	    });
		
		$('.show-popup').popup();
	},
	methods: {
		showMessage: function(message){
			alert('message: '+message);
		},
		openDataset: function(dataset){
			console.log(dataset);
			this.isProcessing= true;
			
			// set all dataset inactive
			this.datasets.forEach(function(val, key){
				val.active = false;
			});
			
			// set current dataset inactive
			dataset.active = true;
			var arrString = dataset.uri.split("/");
			var URI = apiURI.repository.repositories + arrString[6]+'/'+arrString[7] 
			
			this.$http.get(URI).then(function (response) {
				// bind this to set timeout http://stackoverflow.com/posts/8800171/revisions
				var extension = '.brepo';
				response.data.response.forEach(function(val, key){
					if(val.id.indexOf(extension)!=-1){	// if contains .brepo extension
						val.id = val.id.substring(0, val.id.length - extension.length);						
					}
					val.workspaceId = jsonData.workspaceId;
					val.datasetId = dataset.id;					
				});
				
				window.setTimeout(_.bind(function(){
					// set data on vm
					this.$set('repositories', response.data.response);
					this.isProcessing = false;					
				}, this),500)
		        
		    }, function (response) {	
		    	// error callback
		    	this.isProcessing = false;
		    	alert('cannot get data');
		    });
		}
	}
});
