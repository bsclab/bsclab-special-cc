Tokens = (function(){
  function Tokens(tokens){
	this.tokens = tokens || [];
  }

  Tokens.prototype = {
	  initialize: function(tokens, options){
	    this.tokens = [];
	    var count = 1;
	    for(var key in tokens){    
	      this.tokens.push(new Tokens(tokens[key]));
	      count++;
	    }
	  },
	  findArc: function(source, target){
	    return _.findWhere(this.arcs, {source: source, target: target});
	  },
	  add: function(token){
	    this.tokens.push(token);
	  },
	  get: function(id){
	    return _.findWhere(this.tokens, {id: id});
	  },
	  getAll: function(){
	    return this.tokens;
	  },
	  calculateTokensByState: function(state){
		var tokens = _.filter(this.tokens, {state: state});
		var counts = _.map(tokens, function(val){
			if(val.tokensGroup.length==0){
				return 1;
			}
			else{
				return val.tokensGroup.length;
			}
		})
		var count = _.sum(counts);
		return count;
	  },
  };

  return Tokens;
})();