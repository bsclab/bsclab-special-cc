Nodee = (function(){
  function setPosX(pos, maxPos){
    return pos - (maxPos/2);
  }

  function setPosY(pos, maxPos){
    return (pos - (maxPos/2)) * -1;
  }

  function Nodee(options){
	_.extend(this, options);
    this.type = _.isUndefined(options.type)?'heuristic': options.type;
    this.isLogReplay = _.isUndefined(options.isLogReplay)? false: options.isLogReplay;
    this.label = options.label;
    this.id = _.snakeCase(options.label);
//    this.id = this.label.trim().replace(/\s+/g,'_').toLowerCase().replace('(','_').replace(')','_');
    this.duration = options.duration;
    this.frequency = options.frequency;
    this.svgId = options.svgId;
    this.nodeDOM = undefined;
    this.boxMesh = undefined;
    this.boxBorderMesh = undefined;
    this.difference = _.isUndefined(options.difference)? false: options.difference;		// only true and false;
//    this.size = {
//      width: 0,
//      height: 0,
//      depth: 0
//    };
//    this.size2 = {
//      width: 0,
//      height: 0,
//      depth: 0
//    };
//    this.position = {
//      x: 0,
//      y: 0,
//      z: 0
//    };
  }

  Nodee.prototype = {
//    setPosition: function(x,y,z, maxPosX, maxPosY){
//      this.position = {
//        x: maxPosX===0? x: setPosX(x, maxPosX),
//        y: maxPosY===0? y: setPosY(y, maxPosY),
//        z: z
//      };
//    },
//    setSize: function(width, height, depth){
//      this.size = {
//        width: width-5,
//        height: height-5,
//        depth: depth
//      };
//      this.size2 = {
//        width: width,
//        height: height,
//        depth: depth-5
//      };
//    },
//    createBox: function(scene, material){
//      this.boxMesh = ThreejsHelper.createBoxGeometry(scene, this.size.width, this.size.height, this.size.depth, this.position.x, this.position.y, this.position.z, material);
//    },
//    createBoxBorder: function(scene, material){
//      this.boxBorderMesh = ThreejsHelper.createBoxGeometry(scene, this.size2.width, this.size2.height, this.size2.depth, this.position.x, this.position.y, this.position.z, material);
//    },
//    createLabel: function(scene, text, fontSize, color){
//      var pos = {
//        x: this.position.x ,
//        y: this.position.y ,
//        z: Math.round(this.size.depth/2) +1,
//      };
//      this.meshLabel = ThreejsHelper.createCanvasLabel(scene, text, fontSize, pos.x, pos.y, pos.z, this.size.width, this.size.height, color);
//      // this.meshLabel = ThreejsHelper.createText(scene, text, fontSize, pos.x, pos.y, pos.z, color);
//    },
  };

  return Nodee;
})();
