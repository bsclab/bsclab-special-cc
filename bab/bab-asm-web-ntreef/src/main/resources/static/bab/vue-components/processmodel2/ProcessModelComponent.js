/* 
 * this is process model component based on dagre d3
 * author @superpikar
 * */

/*
 * credit : how to extend vue instance http://stackoverflow.com/questions/31709462/how-to-extend-a-vuejs-with-a-method
 * */

var ProcessModelComponent = Vue.extend({
	data: function () {
		return {
			processModel: undefined,
			summaryView: {
				waiting: false
			},
			pmData: {
				waiting: false,
				options: undefined,
				data: {
					cases: undefined,
					numberOfNodes: 0,
					numberOfArcs: 0,
					numberOfCases: 0,
					numberOfEvents: 0,
					startTime: 0,
					endTime: 0,
					searchNode: '',
					nodeFound: 0
				}
			}
		};
	},
	ready: function () {
		//	  console.log('process model is ready');
		this.loadSummaryView();
		this.loadProcessModel();
	},
	methods: {
		loadSummaryView: function () {
			var summaryViewURI = apiURI.repository.view + jsonData.workspaceId
				+ '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt;
			var intervalDuration = 3000;

			this.summaryView.waiting = true;
			//			console.log("[STEP1-1] getView request : " + summaryViewURI);
			this.$http.get(summaryViewURI).then(function (response) {
				//				console.log(response);
				var summaryViewRes = response;
				var summaryViewResStatus = summaryViewRes.data.status;

				if (summaryViewResStatus == 'FINISHED') {
					this.initializeInfo(summaryViewRes.data.response);
					this.summaryView.waiting = false;
				} else if (summaryViewResStatus == 'FAILED') {
					// if status is not FINISHED or RUNNING
					this.summaryView.waiting = false;
					alert("error");
				} else if (summaryViewResStatus == 'RUNNING' || summaryViewResStatus == 'QUEUE') {
					var summaryViewQueueChecker = window.setInterval(_.bind(function () {
						//						console.log("[STEP1-2] get View's Queue request : " + jobQueueURI + summaryViewRes.data.request.jobId);
						this.$http.get(jobQueueURI + summaryViewRes.data.request.jobId).then(function (response) {
							//							console.log(response);
							var summaryViewQueueRes = response;
							var summaryViewQueueResStatus = summaryViewQueueRes.data.jobQueueStatus;
							// success callback	
							if (summaryViewQueueResStatus == 'RUNNING') {
							} else if (summaryViewQueueResStatus == 'FAILED') {
								window.clearInterval(summaryViewQueueChecker);
								alert("FAILED : " + jobQueueURI + summaryViewQueueRes.data.request.jobId);
							} else if (summaryViewQueueResStatus == 'FINISHED') {
								window.clearInterval(summaryViewQueueChecker);
								//								console.log("[STEP1-3] get finished summary view request : " + summaryViewURI);
								this.$http.get(summaryViewURI).then(function (response) {
									//									console.log(response);
									var summaryViewRes = response;
									var summaryViewResStatus = summaryViewRes.data.status;
									if (summaryViewResStatus == 'FINISHED') {
										window.clearInterval(summaryViewQueueChecker)
										this.initializeInfo(summaryViewRes.data.response);
										this.summaryView.waiting = false;
									} else {
										alert("error")
									}
								}, function (response3) {
									this.summaryView.waiting = false;
								});
							}
						}, function (response) {
							// error callback
							this.summaryView.waiting = false;
						})
					}, this), intervalDuration);
				}
			}, function () {
				this.summaryView.waiting = false;
			});
		},
		// methods for process model
		loadProcessModel: function () {
			var intervalDuration = 3000;
			var huristicURI = this.pmData.URI;
			this.pmData.waiting = true;
//			console.log("[STEP2-1] reqeust : " + huristicURI);
			this.$http.get(huristicURI).then(function (response) {
//				console.log(response)
				var huristicRes = response;
				var huristicResStatus = huristicRes.data.status;
				if (response.data.status == 'FINISHED') {
					this.initializeProcessModel(huristicRes.data.response);
					this.pmData.waiting = false;
				} else if (response.data.status == 'FAILED') {
					// if status is not FINISHED or RUNNING
					this.pmData.waiting = false;
					alert("failed");
				} else if (huristicResStatus == 'RUNNING' || huristicResStatus == 'QUEUE') {
					// if status is running then check for each 3000 second
					var huristicQueueChecker = window.setInterval(_.bind(function () {
//						console.log('this is check every '+intervalDuration+'ms');
//						console.log("[STEP2-2] request : " + apiURI.job.queue);
						this.$http.get(apiURI.job.queue + huristicRes.data.request.jobId).then(function (response) {
							// success callback
							var huristicQueueRes = response;
							var huristicQueueStatus = huristicQueueRes.data.jobQueueStatus;
							
//							var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, { jobId: response.data.request.jobId }));
//							var isJobInRunning = !_.isUndefined(_.find(response2.data.running, { jobId: response.data.request.jobId }));

							if (huristicQueueStatus == 'RUNNING') {
							} else if (huristicQueueStatus == 'FAILED') {
								window.clearInterval(huristicQueueChecker);
								this.pmData.waiting = false;
							} else if (huristicQueueStatus == 'FINISHED') {
								window.clearInterval(huristicQueueChecker)
//								console.log("[STEP2-3] request : " + huristicURI)
								this.$http.get(huristicURI).then(function (response) {
//									console.log(response);
									var huristicRes = response;
									var huristicResStatus = huristicRes.data.status;
									// error callback
									if (huristicResStatus == 'FINISHED') {
										this.initializeProcessModel(huristicRes.data.response);
										this.pmData.waiting = false;
									} else {
										alert("error");
									}
								}, function (response) {
									this.pmData.waiting = false;
								});
							}
						}, function (response) {
							// error callback
							this.pmData.waiting = false;
						})

					}, this), intervalDuration);
				}
			}, function (response) {
				// error callback
				alert('error get process model!');
				this.pmData.waiting = false;
			});
		},
		initializeInfo: function (response) {
			this.pmData.data.numberOfCases = response.noOfCases;
			this.pmData.data.numberOfEvents = response.noOfEvents;
			this.pmData.data.startTime = response.timestampStart;
			this.pmData.data.endTime = response.timestampEnd;
			this.pmData.data.cases = _.map(response.cases, function (val, key) {
				return { id: key, value: val };
			});
		},
		initializeProcessModel: function (response) {
			var start = new Date();
			var nodes;
			var arcs;

			switch (this.pmData.options.type) {
				case 'timegap':
					arcs = response.transitions;
					nodes = response.nodes;
					break;
				case 'heuristic':
					arcs = response.arcs;
					nodes = response.nodes;
					break;
				default:
					arcs = response.arcs;
					nodes = response.nodes;
					break;
			}

			//  	  console.log(_.keys(nodes).length, 'nodes', _.keys(arcs).length, 'arcs');
			this.pmData.data.numberOfNodes = _.keys(nodes).length;
			this.pmData.data.numberOfArcs = _.keys(arcs).length;

			this.processModel = new ProcessModel('.processmodel-content', nodes, arcs, this.pmData.options);
			this.processModel.drawProcessModel();
			this.pmData.data.nodeFound = this.processModel.nodes.nodeFound;

			var endTime = new Date();
			var duration = endTime - start;
			//	  console.log('duration '+duration+'ms');
		},
		scaleToFit: function () {
			this.processModel.graphScaleToFit();
		},
		scaleToActual: function () {
			this.processModel.graphScaleToActual();
		},
		changeAlgorithm: function () {

		},
		decreaseFontSize: function () {
			//	  console.log('decrease font size', ev);
		},
		increaseFontSize: function () {
			//	  console.log('increase font size', ev);
		},
		changeArcType: function (ev) {
			this.processModel.changeArcType(ev.target.value);
		},
		changeArcDirection: function (ev) {
			this.processModel.changeArcDirection(ev.target.value);
		},
		searchNode: function () {
			this.processModel.searchNode(this.pmData.data.searchNode);
			this.pmData.data.nodeFound = this.processModel.nodes.nodeFound;
		},
		alertProcessModel: function () {
			alert('this is process model component')
		},
		alertLogReplay: function () {
			alert('log replay is not implemented')
		},
		exportAsImage: function () {
			this.processModel.exportAsImage();
		}
	}
});

