/*
 * @description this is loader component used to show loader based on condition isactive
 *
 * @example : <loader label="Please wait" :isactive="isProcessing"></loader>
 *
 * @props label String label of the loader
 * @props color String color of the loader
 * @props size String size of loader "mini/small/medium/large/huge"
 * @props isactive Boolean state to show the loader. If true then laoder will be shown
 * @dependency Semantic-UI CSS, vuejs
 * @author http://twitter.com/superpikar
 * */
var LoaderComponent = Vue.extend({
  props: {
  	label: { default: 'Loading' },
  	color: { default: 'inverted' },
  	size: { default: 'medium' },
  	isactive: { default: false }
  },
  template:
	'<div class="ui dimmer" v-bind:class="[size, color, { active: isactive }]">' +
	    '<div class="ui text loader">{{label}}</div>' +
	  '</div>'
  ,
  ready: function(){
  }
})
