LogReplay = (function(){
	var timeDivider = 1000;
	var start;
	var startOriginal;
	var complete;
	var completeOriginal;
	var realtime = 0;
	var duration;
	var partitions = [];
	var timeline;
	var svgTokens;
//	this.state = 'initial'; 	// initial, play, pause
	
	var timelineSlider;
	var speedSlider;
	
	var processModel;
	var partitions = new Partitions();
	var tokens = new Tokens();
	var uiData = {};
	var attributes = [];
	var timelineChartData = {};
	var timelineChart;
	
	var countTokens = 0;
	var countTokensGroup = 0;
	var tokenTemplate = Handlebars.compile($('#tokentooltip-template').html());
	
	function updateSlider(args){
		realtime = Math.round(timeline.time()) + start;
		timelineSlider.slider('value', realtime);
		updateUiTime(realtime);
		checkNextPartition(realtime);		
	}
	
	function updateUiTime(realtime){
		uiData.time = realtime * timeDivider;
	}
	
	function checkNextPartition(realtime){
//		console.log(realtime, partitions.currentPartition.nextPartitionStart, realtime > partitions.currentPartition.nextPartitionStart)
		if(realtime >= partitions.currentPartition.nextPartitionStart && !partitions.currentPartition.isLast){
			// to prevent multiple initialization because of timeline slider moving very fast.
			partitions.setCurrentPartition(partitions.currentPartition.part + 1);
			if(partitions.currentPartition.state == 'NA'){
//	 			console.log('initialize new partition bro!');
				initializePartition(partitions.currentPartition);
			}
		}
	}
	
	function initializePartition(partition){
		/*
		 * arrString = [ '', 'bab', 'workspaces', 'workspaceId', 'datasetId', 'repoHash', 'part' ]
		 * */
		uiData.waiting = true; 
		uiData.waitingLabel = 'Loading .. initialize tokens animation'; 
		window.setTimeout(function(){
			partition.state = 'DL';
			console.log(partition.url);
//			console.log(partition.url);
			$.ajax({
				url: partition.url, 
				success: function(data){
					var partitionOffset = partition.start - start; 
//					console.log('startMaster', start, 'duration', duration);
//					console.log('part', partition.part, 'startMaster', start, 'startPartition', partition.start, 'offset', partitionOffset);
					
					pause();
					partition.state = 'IN';
					
					var localTimeline = new TimelineLite();
					var localTokens = [];
					timeline.add(localTimeline, partitionOffset);
					data.response.listOfEvent.forEach(function(val, key){
						var tokenStart = val.start/timeDivider;
						var tokenComplete = val.complete/timeDivider;
						
						var arc = processModel.arcs.findArc(val.source, val.target);
						
						if(arc){
							countTokens++;
							var token = new Token({
								startOriginal: val.start,
								completeOriginal: val.complete,
								start: val.start/timeDivider,
								complete: val.complete/timeDivider,
								source: val.source,
								target: val.target,
								caseId: val.caseId,
								id: countTokens,
								path: arc.path,
								svgTokens: svgTokens,
								tokenTemplate: tokenTemplate
							});
							localTokens.push(token);
//					 console.log(token.id, 'startPartition', partitionStart, 'start', token.start, 'offset', tokenOffset);					 
//					  token.setSelector();
//					 var tokenOffset = token.start - partitionStart;
//					 var tokenDuration = token.complete - token.start;
//					 var tokenTween = TweenLite.to(token.id, tokenDuration);
//					 tokenTween.eventCallback('onUpdate', token.walking, ['{self}', token]);
//					 localTimeline.add(tokenTween, tokenOffset);
						}
						else{
//					 console.log('token path not found'); 
						}
					});
					
					/*
					 * group by the same start and complete time  
					 * credit : http://stackoverflow.com/questions/25550624/group-by-multiple-values-underscore-js-but-keep-the-keys-and-values
					 * */ 
					localTokens = _.groupBy(localTokens, function(val){
						return val.start+'_'+val.complete+'_'+val.soruce+'_'+val.target;
					});
					
					var count = 0;
					var localTokensLength = _.keys(localTokens).length;
					for(var i in localTokens){
						var token;
						if(localTokens[i].length == 1){
							token = localTokens[i][0]; 
						}
						else if(localTokens[i].length > 1){
							countTokensGroup++;					  
							token = new Token({
								startOriginal: localTokens[i][0].startOriginal,
								completeOriginal: localTokens[i][0].completeOriginal,
								start: localTokens[i][0].start,
								complete: localTokens[i][0].complete,
								source: localTokens[i][0].source,
								target: localTokens[i][0].target,
								caseId: 'group ('+localTokens[i].length+' tokens)',
								id: 'group'+countTokensGroup,
								path: localTokens[i][0].path,
								svgTokens: svgTokens,
								tokensGroup: localTokens[i],
								tokenTemplate: tokenTemplate 
							});
							
							token.tokensGroup.forEach(function(val, key){
								val.path = undefined;	// remove reference to the path 
							});
							
						}
						token.uiData = uiData;
						token.createToken();
						var tokenOffset = token.start - partition.start;
						var tokenDuration = token.complete - token.start;
						var tokenTween = TweenLite.to(token.id, tokenDuration);
						tokenTween.eventCallback('onUpdate', token.walking, ['{self}', token]);
						localTimeline.add(tokenTween, tokenOffset);					  
						tokens.add(token);
						
						if(_.isUndefined(timelineChartData[token.startOriginal])){
							timelineChartData[token.startOriginal] = token.tokensGroupLength;
						}
						else {
							timelineChartData[token.startOriginal] += token.tokensGroupLength; 
						}
						
						if(count == localTokensLength-1){
							timelineChartData[token.startOriginal+1] = 0;
						}
						count++;
					}
					delete timelineChartData[completeOriginal];
					timelineChartData[completeOriginal] = 0;
					var data = _.map(timelineChartData, function(val, key){
						return [new Date(parseInt(key)), val];
					});
//					console.log('---',timelineChartData);
//					console.log('---',data);
					timelineChart.updateOptions({'file': data});
					
					play();
					uiData.waiting = false;
					partition.state = 'RD';
				},
			});
		}, 1000);
	}
	
	function appendTokensSvgVue(){
		// append <g class="tokens"></g> into process model
		var svgSelector = '.processmodel-content svg .output';
		var intval = window.setInterval(function(){
			if($(svgSelector).length==1){
//				console.log('svg exist');
				window.clearInterval(intval);
//				d3.select('svg g.output').append('g').attr('class', 'tokens');

//				$('.tokens-container').appendTo(svgSelector + ' .tokens');				
				$('.tokens-container').children().appendTo(svgSelector);
//				$('.tokens').appendTo(svgSelector);
				
				initializePartition(partitions.currentPartition);			
			}
			else{
//				console.log('svg not exist');				
			}
		}, 1000)
	}
	
	function appendSvgTokens(){
		var svgSelector = '.processmodel-content svg .output';
		svgTokens = d3.select(svgSelector).append('g').attr('class', 'tokens');
		partitions.setCurrentPartition(0);
		initializePartition(partitions.currentPartition);			
	}
	
	function hideTokensCaseId(isHide=true){
		var prefix = 'style-tokens';
		d3.selectAll("[id^="+prefix+"]").remove(); 	// remove all css for tokens
		if(isHide){
			var cssString = "g.tokens text{ display:none; }"
			d3.select('head').append('style').attr('id', prefix).text(cssString);			
		}
	}
	
	function renderTokensCssClass(isRender = true, key = 'caseId'){	// set default value if undefined		
		var prefix = 'style-attributes-';
		
		d3.selectAll("[id^="+prefix+"]").remove(); 	// remove all css for tokens
		if(isRender){
			var attribute = _.find(attributes, {key: key});
			if(attribute){
				
				var randomSeed = 'osem';
				var cssString = '';
				var colors = randomColor({
					count: attribute.items.length,
					seed: randomSeed
				});
				attribute.items.forEach(function(val2, key2){
					cssString+= '.'+attribute.key+'_'+val2+' circle{'+
						'fill:'+ colors[key2]+';'+
					'}';
					
					cssString+= 'div.'+attribute.key+'_'+val2+'{'+
						'background:'+ colors[key2]+'!important;'+
					'}';					
				})
				d3.select('head').append('style').attr('id', prefix+attribute.key).text(cssString);			
			}			
		}		
	}
	
	function play(){
		timeline.play();
	  	uiData.state = 'play';
//	  	console.log('state change to play');
	  	timelineSlider.slider('enable');
	  	speedSlider.slider('enable');
	}
	
	function pause(){
		timeline.pause();
		uiData.state = 'pause';
		timelineSlider.slider('disable');
		speedSlider.slider('disable');
//		console.log('state change to pause');		
	}
	
	function LogReplay(starttime, completetime, lrdata, processmodel){
		var self = this;
//		console.log(starttime, completetime, lrdata);
		startOriginal = starttime;
		completeOriginal = completetime;
		start = starttime/timeDivider;
		complete = completetime/timeDivider;
		duration = complete - start;
//		console.log('time :', start, complete, duration);
		uiData = lrdata.data;
		var count = 0;
		for(var i in lrdata.partitions){
			var nextPartitionStart = -1;
			var isLast = false;
			if(i < lrdata.partitions.length - 1){
				nextPartitionStart = lrdata.partitions[count+1].start / timeDivider;
			}
			else{
				isLast = true;
			}
			var partition = new Partition({
				start: lrdata.partitions[i].start/timeDivider,
				complete: lrdata.partitions[i].start/timeDivider,
				part: lrdata.partitions[i].part,
				nextPartitionStart: nextPartitionStart,
				isLast: isLast,
				url: lrdata.partitions[i].url
			});
			partitions.add(partition);
			count++;
		}
		
		attributes = lrdata.attributes;
//		console.log('attributes', attributes);
		
		processModel = processmodel;
//		console.log(processModel);
		
//		console.log(partitions, tokens, uiData)
		timeline = new TimelineLite();
		
		var newTween = TweenLite.to('.timeline-slider', duration, {
			width: '100%',
		});
		newTween.eventCallback('onUpdate', updateSlider, ["{self}"]);
		
		timeline.add(newTween);
		timeline.timeScale(uiData.speed);
		timeline.pause();
		
		timelineSlider = $( "#timeline-slider" ).slider({
            range: false,
            min: start,
            max: complete,
            range: 'min',
            values: start,
            start: function(event, ui){
            	timeline.pause();
//            	uiData.state = 'pause';
            },
            stop: function(event, ui){
            	updateUiTime(ui.value);
    			timeline.time(ui.value - start);
//    			uiData.state = 'play';
    			timeline.play();
            },
            slide: function( event, ui ) {
//               uiData.state = 'slides';
               updateUiTime(parseInt(ui.value));
    		   timeline.time(ui.value - start);
    		   checkNextPartition(ui.value);
            }
        })
        .slider("pips", {
        	step: Math.round(duration/3),
        	rest: "label",
			formatLabel: function(val){
				return moment(val*timeDivider).format('YYYY-MM-DD') + '<br/>' + moment(val).format('HH:mm');
			}
		});
//		console.log(timelineSlider)
		
		var max = 10000; 
		var min = 1;
		speedSlider = $( "#speed-slider" ).slider({
			range: false,
			range: 'min',
			min: min,
			max: max,
			values: 1,
			slide: function( event, ui ) {
				self.changeSpeed(ui.value);
			}
		})
		.slider("pips", {
			step: Math.round((max - min)/2),
			rest: "label",
			suffix: " x",
			formatLabel: function(val){
				return numeral(val).format('0,0') + this.suffix;
			}
		});
		
		appendSvgTokens();
		renderTokensCssClass();
		hideTokensCaseId();
		
		timelineChartData[startOriginal] = 0;
		timelineChartData[completeOriginal] = 0;
		var data = _.map(timelineChartData, function(val, key){
			return [new Date(parseInt(key)), val];
		});
//		console.log('timelineChartData', timelineChartData);
//		console.log('data', data);
		timelineChart = new Dygraph(document.getElementById("timeline-chart"), 
			data, 
			{
				drawXGrid: false,
				drawYGrid: false,
				axes : 
					{ y : 
						{ 
							drawAxis: false 
						} 
					}
			}
        );
	}
	
	LogReplay.prototype = {
	  play: play,
	  pause: pause,
	  hideTokensCaseId: hideTokensCaseId,
	  renderTokensCssClass: renderTokensCssClass,
	  changeSpeed: function(theSpeed){
		  timeline.timeScale(theSpeed);
		  uiData.speed = theSpeed;
	  },
	  getSpeed: function(){
		  return timeline.timeScale(); 
	  },
	  getTime: function(){
		  return (timeline.time()+start) * divider;  
	  },
	  getState: function(){
		  return state;
	  },
	  calculateTokensByState: function(state){
		  return tokens.calculateTokensByState(state);
	  }
	}
	
	return LogReplay;
})();