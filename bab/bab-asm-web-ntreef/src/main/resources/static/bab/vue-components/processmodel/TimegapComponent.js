/* 
 * this is process model component based on dagre d3
 * author @superpikar
 * */
var TimegapComponent = ProcessModelComponent.extend({
	data: function() {
		return {
			timegapURI: "",
			jobQueueURI: "",
			intervalDuration: ""
		};
	},
	ready: function () {
		var self = this;
		//	  console.log('TimegapComponent is ready');
		setTimeout(function () {
			$('.pointing.menu .item').tab();
		}, 1000)

		var self = this;
		var status = "";

		this.timegapURI = apiURI.analysis.timegap + '/' + jsonData.workspaceId
			+ '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt;
		this.jobQueueURI = apiURI.job.queue;
		this.intervalDuration = 3000;

		self.isProcessing = true;
		
		/*
		 * waiting for making base file(csv from sqoop and summary data)
		 */
		var readyFlagChecker = window.setInterval(_.bind(function() {
			if (this.readyFlag == false) {
				
			} else if (this.readyFlag == true){
				window.clearInterval(readyFlagChecker);
				this.setTimegap();
			}
		}, this), this.intervalDuration)
		
		
	},
	methods: {
		setTimegap : function() {

			this.$http.get(this.timegapURI).then(function (response) {

				var timegapRes = response;
				var timegapResStatus = timegapRes.data.status;
				if (timegapResStatus == 'FINISHED') {
					this.isProcessing = false;
					drawTimegap(this, timegapRes.data.response);
				} else if (status == "FAILED") {
					this.isProcessing = false;
					console.log("FAILED")
				} else if (status == "RUNNING" || status == "QUEUED") {
					var timegapQueueChecker = window.setInterval(_.bind(function () {
						this.$http.get(this.jobQueueURI + timegapRes.data.request.jobId)
							.then(function (response) {
								var timegapQueueRes = response;
								var timegapQueueResStatus = timegapQueueRes.data.status;
								if (timegapQueueResStatus == 'RUNNING') {

								} else if (timegapQueueResStatus == 'FAILED') {
									window.clearInterval(timegapQueueChecker);
									console.log('FAILED');
								} else if (timegapQueueResStatus == 'FINISHED') {
									window.clearInterval(timegapQueueChecker);
									this.$http.get(this.timegapURI).then(function (response) {
										this.isProcessing = false;
										var timegapRes = response;
										var timegapResStatus = timegapRes.data.status;
										if (timegapResStatus == 'FINISHED') {
											drawTimegap(this, timegapRes.data.response);
											this.isProcessing = false;
										} else {
											console.log("FAILED")
										}
									}, function (response) {
										this.isProcessing = false;
									});
								}

							});
					}, this), this.intervalDuration);
				}
			});
		},
		overallBtn: function (e) {
			$('.overallDetail')
				.sidebar('setting', 'transition', 'overlay')
				.sidebar('toggle')
				.sidebar('attach events', '.bottomslide', 'show');
		}
	}
});


function drawTimegap(self, responseData) {
	self.arrayData = _.values(responseData.transitions);

	summaryTable = $('#SummaryTable').DataTable({

		data: self.arrayData,
		columns: [
			{ "data": 'source' },
			{ "data": 'target' },
			{ "data": 'meanSuccessive' },
			{ "data": 'stdDevSuccessive' },
			{ "data": 'noOfSuccessiveCases' }
		]
	});

	$.each(responseData.nodes, function (i, obj) {
		$('#fromSelect').append($('<option>').text(i).attr('value', i));
		$('#toSelect').append($('<option>').text(i).attr('value', i));
	});

	var skipSlider = document.getElementById('skipstep');
	var sliderText = $('#sliderText');

	//<!-- http://refreshless.com/nouislider/ -->
	noUiSlider.create(skipSlider, {
		start: 0,
		range: {
			'min': responseData.minGap,
			'max': responseData.maxGap
		},
		orientation: "vertical"
		//				tooltips: true
		//				pips: {
		//				mode: 'positions',
		//				values: [0, 100]
		//			}
	});

	var value = "";
	skipSlider.noUiSlider.on('update', function (values, handle) {

		value = values[handle];
		var absValue = Math.abs(value);
		var seconds = (absValue / 1000) % 60;
		var minutes = (absValue / (1000 * 60)) % 60;
		var hours = (absValue / (1000 * 60 * 60)) % 24;
		var days = absValue / (1000 * 60 * 60 * 24);
		var timeText;

		var day = "";
		if (days >= 1) {
			day = Math.round(days) + "D ";
		}

		var hr = (String(Math.round(hours)).length < 2) ? String("0" + Math.round(hours)) : String(Math.round(hours));
		var mnt = (String(Math.round(minutes)).length < 2) ? String("0" + Math.round(minutes)) : String(Math.round(minutes));
		var sec = (String(Math.round(seconds)).length < 2) ? String("0" + Math.round(seconds)) : String(Math.round(seconds));

		if (value < 0) {
			timeText = '- ' + day + hr + ":" + mnt + ":" + sec;
		} else {
			timeText = day + hr + ":" + mnt + ":" + sec;
		}
		sliderText.text(timeText);

	});

	skipSlider.noUiSlider.on('end', function () {

		var arcFilter = value;
		var model = responseData;

		var arcs = {};
		var arc_cluster_candidates = {};
		var nodes = {};
		var nodesAll = model.nodes;
		for (var arc in model.transitions) {
			var a = model.transitions[arc];
			if (a.meanRaw >= arcFilter) {
				arcs[arc] = a;
				if (!(a.source in nodes)) nodes[a.source] = nodesAll[a.source];
				if (!(a.target in nodes)) nodes[a.target] = nodesAll[a.target];

			} else {
				var s = a.meanRaw;
				if (!(s in arc_cluster_candidates)) arc_cluster_candidates[s] = {};
				arc_cluster_candidates[s][arc] = a;
			}
		}
		// Cluster arcs
		var arc_clusters = {};
		var arc_cluster_index = 0;
		var map_arc_clusters = {};
		for (var c in arc_cluster_candidates) {
			var i = 0;
			var s = "";
			for (var d in arc_cluster_candidates[c]) {
				i++;
				s = d;
			}
			if (i > 1) {
				arc_clusters[c] = arc_cluster_candidates[c];
				arc_cluster_index++;
			}
		}

		//		console.log(self.pmData.options.type);
		//		console.log(arcs);
		self.processModel.update(nodes, arcs, self.pmData.options);
		self.processModel.drawProcessModel();
	});
}