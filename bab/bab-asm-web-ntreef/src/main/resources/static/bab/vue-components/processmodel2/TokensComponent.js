var TokensComponent = Vue.extend({
	props: ['items'],
	components: {
		'token': TokenComponent
	},
	template: 
		'<g class="tokens">'+
			'<token v-for="item in items" :item="item"></token>'+
		'</g>'
	,
});