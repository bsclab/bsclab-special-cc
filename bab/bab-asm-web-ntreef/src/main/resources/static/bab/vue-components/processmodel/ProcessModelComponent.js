/* 
 * this is process model component based on dagre d3
 * author @superpikar
 * */
var DELAY = 500;
var INTERVALDURATION = 3000;

var ProcessModelComponent = Vue.extend({
  template: '#processmodel-template',
  components: {
    'loader': LoaderComponent
  },
  props: ['pmData', 'lrData', 'apiURI', 'theid'],
  data: function(){
	  return {
		  isProcessing: false,
		  summaryView: {
	    	waiting: false,
	    	data: {
	    		noOfCases: 0,
	    		noOfEvents: 0,
	    		noOfActivities: 0	    		
	    	}
	      },
	      data: {
	    	searchNode: '',
	    	nodeFound: 0
	      },
	      readyFlag : false
	  };
  },
  ready: function(){
	  this.loadSummaryView();
//	  this.loadProcessModel();
  },
  methods: {
	loadSummaryView: function(){
		var summaryViewURI = apiURI.repository.view + jsonData.workspaceId
								+ '/' + jsonData.datasetId + '/' + jsonData.sdt + '/' + jsonData.edt;
		var intervalDuration = 3000;
		var jobQueueURI = apiURI.job.queue;
		
	
		this.summaryView.waiting = true;
		//			console.log("[STEP1-1] getView request : " + summaryViewURI);
		this.$http.get(summaryViewURI).then(function (response) {
			//				console.log(response);
			var summaryViewRes = response;
			var summaryViewResStatus = summaryViewRes.data.status;
	
			if (summaryViewResStatus == 'FINISHED') {
				this.$set('summaryView.data', summaryViewRes.data.response);
				this.summaryView.waiting = false;
				this.loadProcessModel();
			} else if (summaryViewResStatus == 'FAILED') {
				// if status is not FINISHED or RUNNING
				this.summaryView.waiting = false;
				alert("error");
			} else if (summaryViewResStatus == 'RUNNING' || summaryViewResStatus == 'QUEUE') {
				var summaryViewQueueChecker = window.setInterval(_.bind(function () {
					this.$http.get(jobQueueURI + summaryViewRes.data.request.jobId).then(function (response) {
						//							console.log(response);
						var summaryViewQueueRes = response;
						var summaryViewQueueResStatus = summaryViewQueueRes.data.jobQueueStatus;
						// success callback	
						if (summaryViewQueueResStatus == 'RUNNING') {
						} else if (summaryViewQueueResStatus == 'FAILED') {
							window.clearInterval(summaryViewQueueChecker);
							alert("FAILED : " + jobQueueURI + summaryViewQueueRes.data.request.jobId);
						} else if (summaryViewQueueResStatus == 'FINISHED') {
							window.clearInterval(summaryViewQueueChecker);
							this.$http.get(summaryViewURI).then(function (response) {
								var summaryViewRes = response;
								var summaryViewResStatus = summaryViewRes.data.status;
								if (summaryViewResStatus == 'FINISHED') {
									window.clearInterval(summaryViewQueueChecker)
									this.$set('summaryView.data', summaryViewRes.data.response);
									this.summaryView.waiting = false;
									this.loadProcessModel();
								} else {
									alert("error")
								}
							}, function (response3) {
								this.summaryView.waiting = false;
							});
						}
					}, function (response) {
						// error callback
						this.summaryView.waiting = false;
					})
				}, this), intervalDuration);
			}
		}, function () {
			this.summaryView.waiting = false;
		});
//		this.summaryView.waiting = true;
//		this.$http.get(this.apiURI.repositoryView).then(function (response) {
//			if(response.data.status == 'FINISHED'){
//				this.$set('summaryView.data', response.data.response);
//				this.summaryView.waiting = false;
//			}
//			else if(response.data.status == 'FAILED' || response.data.status == 'UNKNOWN'){
//				// if status is not FINISHED or RUNNING
//				this.summaryView.waiting = false;				
//		    }
//			else if(response.data.status == 'RUNNING' || response.data.status == 'QUEUED'){
//				var intervalId = window.setInterval(_.bind(function(){
//					this.$http.get(apiURI.job.queue).then(function(response2){
//						// success callback
//						var isJobInQueue = !_.isUndefined(_.find(response2.data.queue, {jobId: response.data.request.jobId}));
//						var isJobInRunning = !_.isUndefined(_.find(response2.data.running, {jobId: response.data.request.jobId}));
//					  
//						if(isJobInQueue || isJobInRunning){
//						}
//						else{
//							this.$http.get(this.apiURI.repositoryView).then(function (response3){
//								if(response3.data.status == 'FINISHED'){
//									window.clearInterval(intervalId)
//									this.$set('summaryView.data', response.data.response);
//									this.summaryView.waiting = false;	
//								}
//							}, function(response3){
//								this.summaryView.waiting = false;
//							});
//					  }
//				  }, function(response2){
//					  // error callback
//					  this.summaryView.waiting = false;
//				  })	  
//				}, this), INTERVALDURATION);
//			}
//		}, function(response){
//	    	// error callback
//		   alert('error get summary view!');
//		   this.summaryView.waiting = false;
//	    });
	},
    // methods for process model
	loadProcessModel: function() {
		
		this.summaryView.waiting = false;
		
		var pmURI = this.apiURI.processModel;
		var jobQueueURI = apiURI.job.queue;
		var intervalDuration = 3000;
		
		this.$http.get(pmURI).then(function (response) {
			var pmRes = response;
			var pmResStatus = pmRes.data.status;
			if (pmResStatus == 'FINISHED') {
				this.initializeProcessModel(pmRes.data.response);
				this.readyFlag = true;
			} else if (pmResStatus == 'FAILED') {
				this.summaryView.waiting = false;
				console.log('FAILED');
			} else if (pmResStatus == 'RUNNING' || pmResStatus == 'QUEUED') {
				var pmQueueChecker = window.setInterval(_.bind(function() {
					this.$http.get(jobQueueURI + pmRes.data.request.jobId)
					.then(function(response) {
						var pmQueueRes = response;
						var pmQueueResStatus = pmQueueRes.data.jobQueueStatus;
						if (pmQueueResStatus == 'RUNNING') {
						} else if (pmQueueResStatus == 'FAILED') {
							this.summaryView.waiting = false;
							window.clearInterval(pmQueueChecker);
							console.log('FAILED')
						} else if (pmQueueResStatus == 'FINISHED') {
							window.clearInterval(pmQueueChecker);
							this.$http.get(pmURI).then(function(response) {
								var pmRes = response;
								var pmResStatus = pmRes.data.status;
								if (pmResStatus == 'FINISHED') {
									this.initializeProcessModel(pmRes.data.response);
									this.readyFlag = true;
								} else if (pmResStatus == 'FAILED') {
									this.summaryView.waiting = false;
									console.log('FAILED');
								}
							});
						}
					});
				}, this), intervalDuration);
			}
			
		});

	},
	initializeProcessModel: function(theResponse){
	  var start = new Date();
  	  var nodes = [];
  	  for (var n in theResponse.nodes) {
//  		  console.log(n);
  		  nodes[nodes.length] = theResponse.nodes[n];
  	  }
  	  theResponse.nodes = nodes;
  	  var arcs = [];
  	  for (var n in theResponse.arcs) {
  		  arcs[arcs.length] = theResponse.arcs[n];
  	  }
  	  theResponse.arcs = arcs;
  	  
  	  var arcs;
  	  
  	  switch (this.pmData.options.type) {
  	  	case 'timegap':
  	  		arcs = theResponse.transitions;
  	  		break;
  	  	case 'heuristic':
  	  		arcs = theResponse.arcs;
  	  		break;
	  		default:
	  			arcs = theResponse.arcs;
	  			break;
  	  }
  	  
//  	  console.log(_.keys(theResponse.nodes).length, 'nodes', arcs.length, 'arcs');
  	  
  	  var maxNodes = 1000;
  	  if(this.pmData.options.renderer == 'dagred3'){
  		  if(_.keys(theResponse.nodes).length < maxNodes){
	    		  this.processModel = new ProcessModel('#'+this.theid, theResponse.nodes, arcs, this.pmData.options);
	    		  this.processModel.drawProcessModel();
	    	  }
	    	  else{
	    		  this.processModel = new ProcessModelVisjs('#'+this.theid, theResponse.nodes, arcs, this.pmData.options);
	    		  this.processModel.drawProcessModel();
	    	  }  
  	  }
  	  else if(this.pmData.options.renderer == 'visjs'){
  		  this.processModel = new ProcessModelVisjs('#'+this.theid, theResponse.nodes, arcs, this.pmData.options);
  		  this.processModel.drawProcessModel();
  	  }
  	  else if(this.renderer== 'graphviz'){
  		  this.processModel = new ProcessModelGraphviz('#'+this.theid, theResponse.nodes, arcs, this.pmData.options);
  		  this.processModel.drawProcessModel();
  	  }
  	  var end = new Date();
        var duration = end - start;
//        console.log('duration '+duration+'ms');
        this.summaryView.waiting = false;
	},
    scaleToFit: function () {
      this.processModel.graphScaleToFit();
    },
    scaleToActual: function () {
      this.processModel.graphScaleToActual();
    },
    changeAlgorithm: function(){

    },
    decreaseFontSize: function(){
//      console.log('decrease font size', ev);
    },
    increaseFontSize: function(){
//      console.log('increase font size', ev);
    },
    changeArcType: function(ev){
//      console.log('change arc type', ev.target.value);
      this.processModel.changeArcType(ev.target.value);
    },
    changeArcDirection: function(ev){
//      console.log('change arc direction', ev.target.value);
      this.processModel.changeArcDirection(ev.target.value);
    },
    searchNode: function() {
	    this.processModel.searchNode(this.data.searchNode);
	    this.data.nodeFound = this.processModel.nodes.nodeFound;
	},
    alertProcessModel: function(){
    	alert('this is process model component')
    },
    alertLogReplay: function(){
    	alert('log replay is not implemented')
    }
  }
});
