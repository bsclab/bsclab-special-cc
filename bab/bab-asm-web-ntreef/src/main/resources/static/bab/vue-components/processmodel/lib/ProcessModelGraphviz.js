// credit : https://github.com/cpettitt/dagre-d3/issues/202
SVGElement.prototype.getTransformToElement = SVGElement.prototype.getTransformToElement || function(elem) {
    return elem.getScreenCTM().inverse().multiply(this.getScreenCTM());
};

// Write your package code here!
ProcessModelGraphviz = (function(){
  function ProcessModelGraphviz(containerEl, nodes, arcs, options){
    var self = this;
    
    this.containerEl = containerEl;
    this.nodes = new Nodees();
    this.nodes.initialize(nodes, options);
    this.arcs = new Arcs();
    this.arcs.initialize(arcs, options);
    
    this.g = new dagreD3.graphlib.Graph()
		    .setGraph({})
		    .setDefaultEdgeLabel(function() { return {}; });
    
    this.options = _.extend({
      nodesep: 70,
      ranksep: 50,
      rankdir: "TB",
      marginx: 20,
      marginy: 20,
      isLogReplay: false
    }, options);
    
    this.nodeTemplate = Handlebars.compile($('#node-template').html());
    this.arcTemplate = Handlebars.compile($('#arc-template').html());
    this.arcHoverTemplate = Handlebars.compile($('#archovertooltip-template').html());
    this.nodeHoverTemplate = Handlebars.compile($('#nodehovertooltip-template').html());
    this.nodeClickTemplate = Handlebars.compile($('#nodeclicktooltip-template').html());
    
    if(options.isLogReplay){
	    this.size = {
	      height: $('body').height() - $('.top.fixed.menu').height() - $('#processmodel-control').height() - $('#logreplay-control').height() - 150,
	      width: $(containerEl).width()
	    }
	}
	else{
	    this.size = {
	      height: $('body').height() - $('.top.fixed.menu').height() - $('#processmodel-control').height() - 150,
	      width: $(containerEl).width()
	    }
	}
    $(containerEl).height(this.size.height);	// set height of container el
}

  ProcessModelGraphviz.prototype = {
    update: function(nodes, arcs, options){
      
    },
    drawProcessModel: function() {
    	var graphString = 'digraph g {';
    	
    	this.arcs.getAll().forEach(function(val, key){
    		graphString += _.snakeCase(val.sourceId)+' -> '+_.snakeCase(val.targetId)+';';
    	});
    	graphString += '}';
    	console.log(graphString);
    	var graph = Viz(graphString, { 
    		format: 'svg',
    		engine: 'dot'
    	});
    	console.log(graph);
    },
    draw3DProcessModel: function(){
      // draw 3d here
    },
    setSVGSize: function(width, height){
      
    },
    graphScaleToFit: function(){
      
    },
    graphScaleToActual: function(){
      
    },
    updateGraph: function(){
      
    },
    changeArcDirection: function(direction){
      
    },
    changeArcType: function(type){
      
    }
  };

  return ProcessModelGraphviz;
})();
