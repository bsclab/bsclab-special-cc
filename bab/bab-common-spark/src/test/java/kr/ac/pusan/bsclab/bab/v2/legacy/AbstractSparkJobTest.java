package kr.ac.pusan.bsclab.bab.v2.legacy;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceRequest;
import kr.ac.pusan.bsclab.bab.v2.core.services.ServiceResponse;
import kr.ac.pusan.bsclab.bab.ws.api.SparkLocalExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.Arc;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.Node;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Element;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Event;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Gateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.SequenceFlow;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Task;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJob;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.model.RawResource;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public abstract class AbstractSparkJobTest {

	protected static ObjectMapper mapper;
	protected static ObjectMapper mapperPretty;
	protected static SparkLocalExecutor executor;

	public static void cleanup(String uri) {
		File f = new File(uri);
		if (f.exists()) {
			if (f.isDirectory()) {
				try {
					FileUtils.deleteDirectory(f);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				f.delete();
			}
		}
	}
	
	public static String getTestResourceUri(String path) {
//		File f = new File("src/test/resources" + path);
//		String uri = f.getAbsolutePath().replaceAll("\\\\", "/");
		return path;
	}
	
	public static String getTestResour2ceSparkUri(String path) {
		return "file:///" + getTestResourceUri(path);
	}
	
	public static ObjectMapper getMapper() {
		if (null == mapper) {
			mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		}
		return mapper;
	}
	
	public static SparkLocalExecutor executor() {
		if (null == executor) {
			executor = new SparkLocalExecutor();
			String hdfsUri = new File("").getAbsolutePath().replaceAll("\\\\", "/") + "/src/test/resources/";
			executor.setHdfsURI(hdfsUri);
			executor.startup("local");
			executor.setAppName("DEBUG");
		}
		return executor;
	}
	
	public static void debug(Object o) {
		if (mapperPretty == null) {
			mapperPretty = new ObjectMapper();
			mapperPretty.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapperPretty.enable(SerializationFeature.INDENT_OUTPUT);
		}
		System.out.println("=====");
		if (null == o) {
			System.out.println("null");
			return;
		}
		try {
			System.out.println(mapperPretty.writeValueAsString(o));
		} catch (Exception e) {
			System.out.println(o);
		}
	}

	public static void saveTextFile(String uri, String txt) {
		executor().getFileUtil().saveAsTextFile(executor(), uri, txt);	
	}
	
	public static String loadTextFile(String uri) {
		return executor().getFileUtil().loadTextFile(executor(), uri);	
	}
	
	
	public static <I extends ServiceRequest, O extends ServiceResponse> O testLegacyBabJob(IJob job, I config, Class<O> responseClass) {
		O response = null;
		try {
			IJobResult result = job.run(getMapper().writeValueAsString(config), new RawResource("", null, config.getUri()), executor());
			String responseJson = result.getResponse();
			response = getMapper().readValue(responseJson, responseClass);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@Before 
	public void before() {
		File file = new File("..\\bab\\lib\\hadoop");
		String path = file.getAbsolutePath();
		System.setProperty("HADOOP_HOME", path);
	}

	//public static final String EOL = ";\r\n";
	public static final String EOL = "";
	
	public String vizJs(String dot) {
		StringBuilder html = new StringBuilder();
		html.append("<!DOCTYPE html><html><head><title>Viz.js</title><script src=\"../shared/viz.js\"></script></head><body><script>var graph = '");
		html.append(dot);
		html.append("';var image = Viz(graph);document.body.innerHTML += image;</script></body></html>");
		return html.toString();
	}
	
	public String visualizeHNet(HeuristicModel hnet) {
		StringBuilder dot = new StringBuilder();
		dot.append("digraph G { rankdir=LR;");
		dot.append(EOL);
		Map<String, String> nodes = new LinkedHashMap<String, String>();
		for (Node n : hnet.getNodes().values()) {
			if (!nodes.containsKey(n.getLabel())) {
				nodes.put(n.getLabel(), "node" + nodes.size());				
			}
			dot.append(nodes.get(n.getLabel()));
			dot.append("[shape=\"rect\",label=\"");
			dot.append(n.getLabel());
			dot.append("\\n");
			dot.append(n.getFrequency().getAbsolute());
			dot.append("\"];");
			dot.append(EOL);
		}
		for (Arc a : hnet.getArcs().values()) {
			dot.append(nodes.get(a.getSource()));
			dot.append("->");
			dot.append(nodes.get(a.getTarget()));
			dot.append(";");
			dot.append(EOL);
		}
		dot.append("}");
		return vizJs(dot.toString());
	}
	
	public String visualizeBPMN(Model bpmn) {
		StringBuilder dot = new StringBuilder();
		dot.append("digraph G { rankdir=LR;");
		dot.append(EOL);
		for (Element e : bpmn.getProcess().getElements()) {
			if (e instanceof SequenceFlow) {
				dot.append(((SequenceFlow) e).getSourceRef().replaceAll("-", ""));
				dot.append("->");
				dot.append(((SequenceFlow) e).getTargetRef().replaceAll("-", ""));
				dot.append("[label=\"");
				dot.append(e.getId());
				dot.append("\"]");
				dot.append(";");
				dot.append(EOL);
			} else if (e instanceof Task) {
				dot.append(((Task) e).getId().replaceAll("-", ""));
				dot.append("[shape=\"rect\",label=\"");
				dot.append(e.getName());
				dot.append(e.getId());
				dot.append("\"];");
				dot.append(EOL);
			} else if (e instanceof Event) {
				dot.append(((Event) e).getId().replaceAll("-", ""));
				dot.append("[shape=\"circle\",label=\"");
				dot.append(e.getName());
				dot.append(e.getId());
				dot.append("\"];");
				dot.append(EOL);
			} else if (e instanceof Gateway) {
				dot.append(((Gateway) e).getId().replaceAll("-", ""));
				dot.append("[shape=\"diamond\",label=\"");
				dot.append(e.getName());
				dot.append(e.getId());
				dot.append("\"];");
				dot.append(EOL);
			}
		}
		dot.append("}");
		return vizJs(dot.toString());
	}
}
