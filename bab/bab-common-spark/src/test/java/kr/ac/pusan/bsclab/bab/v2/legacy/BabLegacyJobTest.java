package kr.ac.pusan.bsclab.bab.v2.legacy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import org.junit.experimental.categories.Category;

import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.Result;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.AssociationRuleMiner;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.SparkFpGrowthArMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.config.AssociationRuleMinerConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.SparkDottedChartAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.config.DottedChartAnalysisJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models.DottedChartModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dl.DeltaAnalysisJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dl.DeltaAnalysisResult;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dl.SparkDeltaAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SparkSocialNetworkAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config.SocialNetworkAnalysisConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.SocialNetworkAnalysis;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.SparkTimeGapAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.TimeGapAnalysisJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.TimeGapAnalysisJobModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.HeatMapModel;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.SparkTaskMatrixAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.TaskMatrixConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.fm.FuzzyMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.fm.FuzzyModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.fm.SparkFuzzyMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicBpmnMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.LogReplayConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.LogReplayModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.SparkLogReplayJob;
//import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob2;
//import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.SparkHNetToBPMNMinerJob;
//import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.TextResult;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ls.LogSummary;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ls.LogSummaryJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ls.SparkLogSummaryJob;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.PerformanceChartAnalysis;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.PerformanceChartAnalysisJobConfiguration;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.SparkPerformanceChartAnalysisJob;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BabLegacyJobTest extends AbstractSparkJobTest {

	public static final String DATASET_URI = getTestResourceUri("/test/dataset");
	public static final String REPOSITORY_URI = getTestResourceUri("/test/dataset_DEFAULT");
	public static final String DEFINED_URI = new File("").getAbsolutePath().replaceAll("\\\\", "/") + "/src/test/resources/defined/dataset_DEFAULT";	
	public static final String OBSERVED_URI = new File("").getAbsolutePath().replaceAll("\\\\", "/") + "/src/test/resources/test/dataset_DEFAULT";
	public static final String LINE_BREAK = System.lineSeparator();
	public static long TEST_STARTED;
		
	public static void setDefaultConfig(Configuration config) {
		config.setUri(REPOSITORY_URI);
		config.setRepositoryType("hdfs");
		config.setRepositoryURI(REPOSITORY_URI);

		config.setRepositoryType("mysql");
		config.setRepositoryURI("default/default/1");
		config.setRepositoryDbString("jdbc:mysql://localhost:3306/smartfactory_test");
		config.setRepositoryDbUsername("bab");
		config.setRepositoryDbPassword("bab");

//		config.setRepositoryType("presto2");
//		config.setRepositoryURI("workspace0/dataset1/2");
//		config.setRepositoryDbString("jdbc:presto://premaster.dev.iochord.co.kr:8080/hive/bab");
//		config.setRepositoryDbUsername("test");
//		config.setRepositoryDbPassword("");
	}
	
	// 1st Test the output of algorithm using default settings
	public static Result testRunJobDefault(String outputExtension, IJob job, Configuration config, Result expectedResult) {
		try {
			String jobName = job.getClass().getSimpleName();
			String outputFile = REPOSITORY_URI + outputExtension;
			cleanup(outputFile);
			Result observedResult = testLegacyBabJob(job, config, expectedResult.getClass());
			debug(jobName + " (" + outputExtension + ")");
			debug(config);
			debug(expectedResult);
			debug(observedResult);
			debug(outputFile);
			assertTrue(jobName + " RUN DEFAULT TEST", observedResult != null);
			return observedResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static <T extends Result> T loadFromFile(String file, T def, Class<T> resClass) {
		T result = null;
		try {
			String json = loadTextFile(file);
			result = getMapper().readValue(json, resClass);
		} catch (Exception e) {
			e.printStackTrace();
			return def;
		}
		return result;
	}

	// 2nd Test compare the result of algorithm using default settings with expected result 
	public static void testCompareResult(String outputExtension, IJob job, Result expectedResult, Result observedResult) {
		try {
			String jobName = job.getClass().getSimpleName();
			String expectedJson = getMapper().writeValueAsString(expectedResult);
			String observedJson = getMapper().writeValueAsString(observedResult);
			assertTrue(jobName + " RESULT DEFAULT TEST", expectedResult != null && observedResult != null &&  expectedJson.equals(observedJson));
		} catch (Exception e) {
		}
	}
	
	@Test
	public void a00_Test_Start() {
		TEST_STARTED = System.currentTimeMillis();
		assertEquals(0, 0);
	}
	
	@Test
	public void z99_Test_Complete() {
		debug("ALL TEST COMPLETED IN " + (System.currentTimeMillis() - TEST_STARTED) + " ms");
		assertEquals(0, 0);
	}

	/*/
		!!! TEST ALL AVAILABLE JOBS AFTER REPOSITORY CREATION !!!
	//*/

	/*/
	@Test
	@Category(SparkDeltaAnalysisJob.class)
	public void test06_Analysis_07_01_Delta() {
		String outputExtension = ".dlans";
		IJob job = new SparkDeltaAnalysisJob();
		DeltaAnalysisJobConfiguration config = new DeltaAnalysisJobConfiguration();
		setDefaultConfig(config);
		config.setCompareToURI(config.getRepositoryURI());
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new DeltaAnalysisResult(), DeltaAnalysisResult.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	//*/
	//*/
	@Test
	@Category(SparkLogSummaryJob.class)
	public void test04_Repository_01_01_LogSummary() {
		String outputExtension = ".blsum";
		IJob job = new SparkLogSummaryJob();
		LogSummaryJobConfiguration config = new LogSummaryJobConfiguration();
		setDefaultConfig(config);
		config.setRepositoryTab(true);
		config.setTimelineTab(true);
		config.setActivityTab(true);
		config.setOriginatorTab(true);
		config.setResourceTab(true);
		config.setCaseTab(true);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new LogSummary(), LogSummary.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	//*/
	@Test
	@Category(SparkHeuristicMinerJob.class)
	public void test05_Modelling_01_01_Heuristic() {
		String outputExtension = ".hmodel";
		IJob job = new SparkHeuristicMinerJob();
		Configuration config = new HeuristicMinerJobConfiguration();
		setDefaultConfig(config);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new HeuristicModel(), HeuristicModel.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkHeuristicMinerJob.class)
	public void test05_Modelling_01_02_Heuristic() {
		String outputExtension = ".hmodel";
		IJob job = new SparkHeuristicMinerJob();
		HeuristicMinerJobConfiguration config = new HeuristicMinerJobConfiguration();
		setDefaultConfig(config);
		config.setUri(getTestResourceUri("/test/dataset_DEFAULT_02"));
		config.getThreshold().setDependency(0.1);
		Result expectedResult = loadFromFile(getTestResourceUri("/defined/dataset_DEFAULT_02") + outputExtension, new HeuristicModel(), HeuristicModel.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}

	@Test
	@Category(SparkFuzzyMinerJob.class)
	public void test05_Modelling_03_01_Fuzzy() {
		String outputExtension = ".fmodel";
		IJob job = new SparkFuzzyMinerJob();
		Configuration config = new FuzzyMinerJobConfiguration();
		setDefaultConfig(config);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new FuzzyModel(), FuzzyModel.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}

	@Test
	@Category(SparkLogReplayJob.class)
	public void test05_Modelling_04_01_LogReplay() {
		String outputExtension = ".lrmodel";
		IJob job = new SparkLogReplayJob();
		Configuration config = new LogReplayConfiguration();
		setDefaultConfig(config);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new LogReplayModel(), LogReplayModel.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkHeuristicBpmnMinerJob.class)
	public void test05_Modelling_02_01_BPMN() {
		String outputExtension = ".hbmodel";	
		IJob job = new SparkHeuristicBpmnMinerJob();
		HeuristicMinerJobConfiguration config = new HeuristicMinerJobConfiguration();
		setDefaultConfig(config);
		config.setPositiveObservation(1);
		config.getThreshold().setDependency(0.001);
		Model expectedResult = loadFromFile(DEFINED_URI + outputExtension, new Model(), Model.class);
		Model observedResult = (Model) testRunJobDefault(outputExtension, job, config, expectedResult);
		saveTextFile(REPOSITORY_URI + ".obs.bpmn.html", visualizeBPMN(observedResult));
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
//
//	@Test
//	@Category(SparkHeuristicMinerJob2.class)
//	public void test05_Modelling_05_01_Heuristic2() {
//		String outputExtension = ".hmodel2";
//		IJob job = new SparkHeuristicMinerJob2();
//		Configuration config = new HeuristicMinerJobConfiguration();
//		setDefaultConfig(config);
//		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new HeuristicModel(), HeuristicModel.class);
//		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
//		testCompareResult(outputExtension, job, expectedResult, observedResult);
//	}
//
//	@Test
//	@Category(SparkHNetToBPMNMinerJob.class)
//	public void test05_Modelling_06_01_HNetToBPMN() {
//		String outputExtension = ".xbpmn";
//		IJob job = new SparkHNetToBPMNMinerJob();
//		Configuration config = new HeuristicMinerJobConfiguration();
//		setDefaultConfig(config);
//		Result expectedResult = new TextResult(); 
//		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
//		testCompareResult(outputExtension, job, expectedResult, observedResult);
//	}	
	
	@Test
	@Category(SparkFpGrowthArMinerJob.class)
	public void test06_Analysis_01_01_AssociationRule() {
		String outputExtension = ".arans";
		IJob job = new SparkFpGrowthArMinerJob();
		Configuration config = new AssociationRuleMinerConfiguration();
		setDefaultConfig(config);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new AssociationRuleMiner(), AssociationRuleMiner.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}

	@Test
	@Category(SparkSocialNetworkAnalysisJob.class)
	public void test06_Analysis_02_01_SocialNetworkHandover() {
		String outputExtension = ".snans";
		IJob job = new SparkSocialNetworkAnalysisJob();
		SocialNetworkAnalysisConfiguration config = new SocialNetworkAnalysisConfiguration();
		setDefaultConfig(config);
		config.getMethod().setAlgorithm("handoverofwork");
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new SocialNetworkAnalysis(), SocialNetworkAnalysis.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		((SocialNetworkAnalysis) expectedResult).setId("");
		((SocialNetworkAnalysis) observedResult).setId("");
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}

	@Test
	@Category(SparkSocialNetworkAnalysisJob.class)
	public void test06_Analysis_02_02_SocialNetworkSubcontract() {
		String outputExtension = ".snans";
		IJob job = new SparkSocialNetworkAnalysisJob();
		SocialNetworkAnalysisConfiguration config = new SocialNetworkAnalysisConfiguration();
		setDefaultConfig(config);
		config.setUri(getTestResourceUri("/test/dataset_DEFAULT_02"));
		config.getMethod().setAlgorithm("subcontracting");
		Result expectedResult = loadFromFile(DEFINED_URI + "_02" + outputExtension, new SocialNetworkAnalysis(), SocialNetworkAnalysis.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		((SocialNetworkAnalysis) expectedResult).setId("");
		((SocialNetworkAnalysis) observedResult).setId("");
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkSocialNetworkAnalysisJob.class)
	public void test06_Analysis_02_03_SocialNetworkWorkingTogether() {
		String outputExtension = ".snans";
		IJob job = new SparkSocialNetworkAnalysisJob();
		SocialNetworkAnalysisConfiguration config = new SocialNetworkAnalysisConfiguration();
		setDefaultConfig(config);
		config.setUri(getTestResourceUri("/test/dataset_DEFAULT_03"));
		config.getMethod().setAlgorithm("workingtogether");
		Result expectedResult = loadFromFile(DEFINED_URI + "_03" + outputExtension, new SocialNetworkAnalysis(), SocialNetworkAnalysis.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		((SocialNetworkAnalysis) expectedResult).setId("");
		((SocialNetworkAnalysis) observedResult).setId("");
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkSocialNetworkAnalysisJob.class)
	public void test06_Analysis_02_04_SocialNetworkSimilarTask() {
		String outputExtension = ".snans";
		IJob job = new SparkSocialNetworkAnalysisJob();
		SocialNetworkAnalysisConfiguration config = new SocialNetworkAnalysisConfiguration();
		setDefaultConfig(config);
		config.setUri(getTestResourceUri("/test/dataset_DEFAULT_04"));
		config.getMethod().setAlgorithm("similartask");
		Result expectedResult = loadFromFile(DEFINED_URI + "_04" + outputExtension, new SocialNetworkAnalysis(), SocialNetworkAnalysis.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		((SocialNetworkAnalysis) expectedResult).setId("");
		((SocialNetworkAnalysis) observedResult).setId("");
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkSocialNetworkAnalysisJob.class)
	public void test06_Analysis_02_05_SocialNetworkReassignment() {
		String outputExtension = ".snans";
		IJob job = new SparkSocialNetworkAnalysisJob();
		SocialNetworkAnalysisConfiguration config = new SocialNetworkAnalysisConfiguration();
		setDefaultConfig(config);
		config.setUri(getTestResourceUri("/test/dataset_DEFAULT_05"));
		config.getMethod().setAlgorithm("reassignment");
		Result expectedResult = loadFromFile(DEFINED_URI + "_05" + outputExtension, new SocialNetworkAnalysis(), SocialNetworkAnalysis.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		((SocialNetworkAnalysis) expectedResult).setId("");
		((SocialNetworkAnalysis) observedResult).setId("");
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkTaskMatrixAnalysisJob.class)
	public void test06_Analysis_03_01_TaskMatrix() {
		String outputExtension = ".tmans";
		IJob job = new SparkTaskMatrixAnalysisJob();
		Configuration config = new TaskMatrixConfiguration();
		setDefaultConfig(config);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new HeatMapModel(), HeatMapModel.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkDottedChartAnalysisJob.class)
	public void test06_Analysis_04_01_DottedChart() {
		String outputExtension = ".dcans";
		IJob job = new SparkDottedChartAnalysisJob();
		Configuration config = new DottedChartAnalysisJobConfiguration();
		setDefaultConfig(config);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new DottedChartModel(), DottedChartModel.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkPerformanceChartAnalysisJob.class)
	public void test06_Analysis_05_01_PerformanceChart() {
		String outputExtension = ".pfans";
		IJob job = new SparkPerformanceChartAnalysisJob();
		Configuration config = new PerformanceChartAnalysisJobConfiguration();
		setDefaultConfig(config);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new PerformanceChartAnalysis(), PerformanceChartAnalysis.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkTimeGapAnalysisJob.class)
	public void test06_Analysis_06_01_Timegap() {
		String outputExtension = ".tgans";
		IJob job = new SparkTimeGapAnalysisJob();
		Configuration config = new TimeGapAnalysisJobConfiguration();
		setDefaultConfig(config);
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new TimeGapAnalysisJobModel(), TimeGapAnalysisJobModel.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	
	@Test
	@Category(SparkDeltaAnalysisJob.class)
	public void test06_Analysis_07_01_Delta() {
		String outputExtension = ".dlans";
		IJob job = new SparkDeltaAnalysisJob();
		DeltaAnalysisJobConfiguration config = new DeltaAnalysisJobConfiguration();
		setDefaultConfig(config);
		config.setCompareToURI(config.getRepositoryURI());
		Result expectedResult = loadFromFile(DEFINED_URI + outputExtension, new DeltaAnalysisResult(), DeltaAnalysisResult.class);
		Result observedResult = testRunJobDefault(outputExtension, job, config, expectedResult);
		testCompareResult(outputExtension, job, expectedResult, observedResult);
	}
	//*/
	

//	@Test
//	@Category(SparkHeuristicBpmnMinerJob.class)
//	public void test05_Modelling_02_01_BPMN() {
//		String outputExtension = ".hbmodel";	
//		IJob job = new SparkHeuristicBpmnMinerJob();
//		HeuristicMinerJobConfiguration config = new HeuristicMinerJobConfiguration();
//		setDefaultConfig(config);
//		config.setPositiveObservation(1);
//		config.getThreshold().setDependency(0.001);
//		Model expectedResult = loadFromFile(DEFINED_URI + outputExtension, new Model(), Model.class);
//		Model observedResult = (Model) testRunJobDefault(outputExtension, job, config, expectedResult);
//		saveTextFile(REPOSITORY_URI + ".obs.bpmn.html", visualizeBPMN(observedResult));
//		testCompareResult(outputExtension, job, expectedResult, observedResult);
////		try {
////			String x = getMapper().writeValueAsString(expectedResult);
////			Model y = getMapper().readValue(x, Model.class);
////			String z = getMapper().writeValueAsString(y);
////			Model a = getMapper().readValue(z, Model.class);
////			getMapper().writeValueAsString(a);
////		} catch (Exception e) {
////			e.printStackTrace();
////		}
//		//saveTextFile(REPOSITORY_URI + ".exp.bpmn.html", visualizeBPMN(expectedResult));
//	}
//	
	/*/ Spark CSV Dataset Generation
	@Test
	@Category(SparkCsvImportJob.class)
	public void test00_Repository_01_01_CsvDatasetGeneration() {
		String csv = "CASE_ID|ACTIVITY|TIMESTAMP|ORIGINATOR|RESOURCE" + LINE_BREAK;
		csv += "CASE01|A|2001-01-01 00:00:00|X|I" + LINE_BREAK;
		csv += "CASE01|B|2001-01-02 00:00:00|Y|J" + LINE_BREAK;
		csv += "CASE01|C|2001-01-03 00:00:00|Z|K" + LINE_BREAK;
		csv += "CASE02|A|2001-02-01 00:00:00|X|I" + LINE_BREAK;
		csv += "CASE02|B|2001-02-02 00:00:00|Y|J" + LINE_BREAK;
		csv += "CASE02|C|2001-02-03 00:00:00|Z|K" + LINE_BREAK;
		String outputFile = DATASET_URI + ".csv";
		cleanup(outputFile);
		debug(outputFile);
		saveTextFile(outputFile, csv);
		String loadedCsv = loadTextFile(outputFile);
		debug(csv);
		debug(loadedCsv);
		assertTrue(TEST_NAME + "CSV Dataset Generation", loadedCsv.equals(csv));
	}
	//*/
	
	/*/ Dummy Intermediate Repository Generation
	@Test
	@Category(SparkCsvImportJob.class)
	public void test00_Repository_02_01_JsonDatasetGeneration() {
		StringBuilder rawlog = new StringBuilder();
		List<Map<String, String>> RL = new ArrayList<Map<String, String>>();
		Map<String, String> nr;
		nr = new LinkedHashMap<String, String>();
		nr.put("CASE"		, "CASE001");
		nr.put("ACTIVITY"	, "A");
		nr.put("TYPE"		, "complete");
		nr.put("TIME"		, "2001-01-01 00:00:00");
		nr.put("OWNER"		, "X");
		nr.put("MACHINE"	, "I");
		RL.add(nr);
		nr = new LinkedHashMap<String, String>();
		nr.put("CASE"		, "CASE001");
		nr.put("ACTIVITY"	, "B");
		nr.put("TYPE"		, "complete");
		nr.put("TIME"		, "2001-01-02 00:00:00");
		nr.put("OWNER"		, "Y");
		nr.put("MACHINE"	, "J");
		RL.add(nr);
		nr = new LinkedHashMap<String, String>();
		nr.put("CASE"		, "CASE001");
		nr.put("ACTIVITY"	, "C");
		nr.put("TYPE"		, "complete");
		nr.put("TIME"		, "2001-01-03 00:00:00");
		nr.put("OWNER"		, "Z");
		nr.put("MACHINE"	, "K");
		RL.add(nr);
		nr = new LinkedHashMap<String, String>();
		nr.put("CASE"		, "CASE002");
		nr.put("ACTIVITY"	, "A");
		nr.put("TYPE"		, "complete");
		nr.put("TIME"		, "2001-01-01 00:00:00");
		nr.put("OWNER"		, "X");
		nr.put("MACHINE"	, "I");
		RL.add(nr);
		nr = new LinkedHashMap<String, String>();
		nr.put("CASE"		, "CASE002");
		nr.put("ACTIVITY"	, "B");
		nr.put("TYPE"		, "complete");
		nr.put("TIME"		, "2001-01-02 00:00:00");
		nr.put("OWNER"		, "Y");
		nr.put("MACHINE"	, "J");
		RL.add(nr);
		nr = new LinkedHashMap<String, String>();
		nr.put("CASE"		, "CASE002");
		nr.put("ACTIVITY"	, "C");
		nr.put("TYPE"		, "complete");
		nr.put("TIME"		, "2001-01-03 00:00:00");
		nr.put("OWNER"		, "Z");
		nr.put("MACHINE"	, "K");
		RL.add(nr);
		for (Map<String, String> r : RL) {
			try {
				rawlog.append(getMapper().writeValueAsString(r) + LINE_BREAK);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		String outputFile = DATASET_URI + ".irepo/part-0000";
		cleanup(outputFile);
		debug(outputFile);
		saveTextFile(outputFile, rawlog.toString());
		String loadedJson = loadTextFile(outputFile);
		String[] LS = loadedJson.split(LINE_BREAK);
		debug(loadedJson);
		assertEquals(TEST_NAME + "JSON Dataset Generation", RL.size(), LS.length);
	}
	//*/
	
	/*/ Spark HDFS Repository Import from CSV
	@Test
	@Category(AbstractSparkJobTest.class)
	public void test01_RepositoryImport_01_01_FromCSV() {
		IJob job = new SparkCsvImportJob();
		ImportJobConfiguration request = new ImportJobConfiguration();
		request.setRepositoryURI(rawDatasetUri);
		ImportJobResult result = testLegacyBabJob(job, request, ImportJobResult.class);
		debug(request);
		debug(result);
	}
	//*/
	
	/*/ Spark Mapping Job
	@Test
	@Category(SparkIntermediateMappingJob.class)
	public void test02_Repository_01_01_RepositoryMapping() {
		SparkIntermediateMappingJob job = new SparkIntermediateMappingJob();
		MappingJobConfiguration config = new MappingJobConfiguration();
		config.setRepositoryId("eventlog");
		config.getMapping().put(SparkIntermediateMappingJob.FIELD_CASE, new LinkedHashMap<String, String>());
		config.getMapping().get(SparkIntermediateMappingJob.FIELD_CASE).put("CASE", SparkIntermediateMappingJob.SCOPE_CASE);
		config.getMapping().put(SparkIntermediateMappingJob.FIELD_ACTIVITY, new LinkedHashMap<String, String>());
		config.getMapping().get(SparkIntermediateMappingJob.FIELD_ACTIVITY).put("ACTIVITY", SparkIntermediateMappingJob.SCOPE_EVENT);
		config.getMapping().put(SparkIntermediateMappingJob.FIELD_TYPE, new LinkedHashMap<String, String>());
		config.getMapping().get(SparkIntermediateMappingJob.FIELD_TYPE).put("TYPE", SparkIntermediateMappingJob.SCOPE_EVENT);
		config.getMapping().put(SparkIntermediateMappingJob.FIELD_TIMESTAMP, new LinkedHashMap<String, String>());
		config.getMapping().get(SparkIntermediateMappingJob.FIELD_TIMESTAMP).put("TIME", SparkIntermediateMappingJob.SCOPE_EVENT + SparkIntermediateMappingJob.DELIMETER_PARAM + "yyyy-MM-dd HH:mm:ss");
		config.getMapping().put(SparkIntermediateMappingJob.FIELD_ORIGINATOR, new LinkedHashMap<String, String>());
		config.getMapping().get(SparkIntermediateMappingJob.FIELD_ORIGINATOR).put("OWNER", SparkIntermediateMappingJob.SCOPE_EVENT);
		config.getMapping().put(SparkIntermediateMappingJob.FIELD_RESOURCE, new LinkedHashMap<String, String>());
		config.getMapping().get(SparkIntermediateMappingJob.FIELD_RESOURCE).put("MACHINE", SparkIntermediateMappingJob.SCOPE_EVENT);
		config.setName("DEFAULT");
		config.setRepositoryURI(DATASET_URI);
		cleanup(REPOSITORY_URI + ".trepo");
		cleanup(REPOSITORY_URI + ".brepo");
		BRepository result = testLegacyBabJob(job, config, BRepository.class);
		assertTrue(TEST_NAME + "Repository Mapping Job", result.getCases().size() > 0);
	}
	//*/
	
	/*/ Spark HDFS Repository Reader Test
	protected final Configuration dummyRepoConfigHdfs = new Configuration();
	protected String dummyRepoUriHdfs = "";
	protected SparkHdfsRepositoryReader readerHdfs = null;
	
	public SparkHdfsRepositoryReader getReaderHdfs() {
		if (dummyRepoUriHdfs != null && readerHdfs == null) {
			dummyRepoConfigHdfs.setRepositoryType("hdfs");
			dummyRepoConfigHdfs.setRepositoryDbString("");
			dummyRepoConfigHdfs.setRepositoryDbUsername("");
			dummyRepoConfigHdfs.setRepositoryDbPassword("");
			readerHdfs = new SparkHdfsRepositoryReader(executor(), dummyRepoUriHdfs, dummyRepoConfigHdfs);
		}
		return readerHdfs;
	}

	@Test
	@Category(SparkHdfsRepositoryReader.class)
	public void test03_ReaderHdfs_01_01_Creation() {
		assertTrue(getReaderHdfs() != null);
	}

	@Test
	@Category(SparkHdfsRepositoryReader.class)
	public void test03_ReaderHdfs_01_02_CaseMappingFunction() {
		PairFunction<String, String, ICase> f = getReaderHdfs().CASE_MAPPER_FUNCTION;
		assertTrue(f != null);
	}
	
	@Test
	@Category(SparkHdfsRepositoryReader.class)
	public void test03_ReaderHdfs_01_03_RepositoryDeserialization() {
		IRepository repository = getReaderHdfs().getRepository();
		assertTrue(repository != null);
	}
	//*/ 

	/*/ Spark MYSQL Repository Reader Test
	protected final Configuration dummyRepoConfigMysql = new Configuration();
	protected String dummyRepoUriMysql = "jdbc:mysql://192.168.178.45:3306/smartfactory_test";
	protected SparkMysqlRepositoryReader readerMysql = null;
	
	public SparkMysqlRepositoryReader getReaderMysql() {
		if (dummyRepoUriMysql != null && readerMysql == null) {
			dummyRepoConfigMysql.setRepositoryType("mysql");
			dummyRepoConfigMysql.setRepositoryDbString(dummyRepoUriMysql);
			dummyRepoConfigMysql.setRepositoryDbUsername("bab");
			dummyRepoConfigMysql.setRepositoryDbPassword("bab");
			readerMysql = new SparkMysqlRepositoryReader(executor(), dummyRepoUriMysql, dummyRepoConfigMysql);
		}
		return readerMysql;
	}
	
	@Test
	@Category(SparkMysqlRepositoryReader.class)
	public void test03_ReaderMysql_02_01_Creation() {
		assertTrue(getReaderMysql() != null);
	}

	@Test
	@Category(SparkMysqlRepositoryReader.class)
	public void test03_ReaderMysql_02_02_CaseIdsRetrieval() {
		PairFunction<String, String, ICase> f = getReaderMysql().CASE_MAPPER_FUNCTION;
		assertTrue(f != null);
	}
	
	@Test
	@Category(SparkMysqlRepositoryReader.class)
	public void test03_ReaderMysql_02_03_CaseMappingFunction() {
		PairFunction<String, String, ICase> f = getReaderMysql().CASE_MAPPER_FUNCTION;
		assertTrue(f != null);
	}

	@Test
	@Category(SparkMysqlRepositoryReader.class)
	public void test03_ReaderMysql_02_04_RepositoryDeserialization() {
		IRepository repository = getReaderMysql().getRepository();
		assertTrue(repository != null);
	}
	//*/ 
	
	/*/ Spark PRESTO Repository Reader Test
	protected final Configuration dummyRepoConfigPresto = new Configuration();
	protected String dummyRepoUriPresto = "jdbc:presto://premaster.dev.iochord.co.kr:8080/hive/bab";
	protected SparkPrestoRepositoryReader readerPresto = null;
	
	public SparkPrestoRepositoryReader getReaderPresto() {
		if (dummyRepoUriPresto != null && readerPresto == null) {
			dummyRepoConfigPresto.setRepositoryType("presto");
			dummyRepoConfigPresto.setRepositoryDbString("jdbc:presto://premaster.dev.iochord.co.kr:8080/hive/bab");
			dummyRepoConfigPresto.setRepositoryDbUsername("bab");
			dummyRepoConfigPresto.setRepositoryDbPassword("bab");
			readerPresto = new SparkPrestoRepositoryReader(executor(), dummyRepoUriPresto, dummyRepoConfigPresto);
		}
		return readerPresto;
	}
	
	@Test
	@Category(SparkPrestoRepositoryReader.class)
	public void test03_ReaderPresto_03_01_Creation() {
		assertTrue(getReaderPresto() != null);
	}

	@Test
	@Category(SparkMysqlRepositoryReader.class)
	public void test03_ReaderPresto_03_02_CaseIdsRetrieval() {
		PairFunction<String, String, ICase> f = getReaderPresto().CASE_MAPPER_FUNCTION;
		assertTrue(f != null);
	}
	
	@Test
	@Category(SparkPrestoRepositoryReader.class)
	public void test03_ReaderPresto_03_03_CaseMappingFunction() {
		PairFunction<String, String, ICase> f = getReaderPresto().CASE_MAPPER_FUNCTION;
		assertTrue(f != null);
	}
	
	@Test
	@Category(SparkPrestoRepositoryReader.class)
	public void test03_ReaderPresto_03_04_RepositoryDeserialization() {
		IRepository repository = getReaderPresto().getRepository();
		assertTrue(repository != null);
	}
	//*/ 
	
}