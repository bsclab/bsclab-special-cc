package kr.ac.pusan.bsclab.bab.v2.legacy;

import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import org.junit.experimental.categories.Category;

import kr.ac.pusan.bsclab.bab.v2.common.process.graph.models.Node;
import kr.ac.pusan.bsclab.bab.v2.common.views.NodeRenderer;
import kr.ac.pusan.bsclab.bab.v2.legacy.factories.NodeFactory;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BabLegacyVisualTest extends AbstractSparkJobTest {
	
	@Test
	@Category(Node.class)
	public void test01_ProcessGraph() {
		String outputExtension = ".hmodel";
		HeuristicModel p = BabLegacyJobTest.loadFromFile(BabLegacyJobTest.DEFINED_URI + outputExtension, new HeuristicModel(), HeuristicModel.class);
		Node n = NodeFactory.createFromHeuristicsModel(p);
		NodeRenderer r = new NodeRenderer();
		r.setEol(NodeRenderer.EOL_NONE);
		String html = vizJs(r.render(n));
		String file = BabLegacyJobTest.OBSERVED_URI + outputExtension + ".html";
		saveTextFile(file, html);
		r.setEol(NodeRenderer.EOL_NORMAL);
		System.out.println(r.render(n));
	}
}