/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;

public class HdfsUtil extends SparkFileUtil implements Serializable {

	protected static HdfsUtil instance;

	public static HdfsUtil getInstance() {
		if (instance == null) {
			instance = new HdfsUtil();
		}
		return instance;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean isFileExists(IExecutor se, String uri) {
		try {
			return (((ISparkExecutor) se).getHdfsFileSystem().exists(new Path(uri)));
		} catch (Exception e) {

		}
		return false;
	}

	@Override
	public void saveAsTextFile(IExecutor se, String uri, String data) {
		try {
			FSDataOutputStream out = ((ISparkExecutor) se).getHdfsFileSystem().create(new Path(uri));
			out.write(data.getBytes("UTF-8"));
			out.close();
		} catch (Exception e) {
			//System.out.println(e.getMessage());
		}
	}

	@Override
	public String loadTextFile(IExecutor se, String uri) {
		try {
			FSDataInputStream is = ((ISparkExecutor) se).getHdfsFileSystem().open(new Path(uri));
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			return sb.toString();
		} catch (Exception e) {

		}
		return null;
	}
}
