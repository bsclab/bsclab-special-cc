package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.util.LinkedHashMap;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;

public class Algorithm1ActivityMapping {
	public static final String STRING_DIVIDER = "|";
	public static final String STRING_START_ACTIVITY = "Start" + STRING_DIVIDER + "Artificial";
	public static final String STRING_END_ACTIVITY = "End" + STRING_DIVIDER + "Artificial";
	private final Map<String, Integer> activities;

	public Algorithm1ActivityMapping(IRepository repository) {
		activities = new LinkedHashMap<String, Integer>();
		activities.put(STRING_START_ACTIVITY, activities.size());
		for (String t : repository.getActivityTypes().keySet()) {
			for (String a : repository.getActivities().keySet()) {
				activities.put(a + STRING_DIVIDER + t, activities.size());
			}
		}
		activities.put(STRING_END_ACTIVITY, activities.size());
	}

	public Map<String, Integer> getActivities() {
		return activities;
	}
}
