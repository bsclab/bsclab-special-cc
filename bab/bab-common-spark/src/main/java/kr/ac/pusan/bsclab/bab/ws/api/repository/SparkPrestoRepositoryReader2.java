/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
// import org.apache.spark.storage.StorageLevel;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.SparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.map.MappingJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.map.SparkIntermediateMappingJob;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;
import kr.ac.pusan.bsclab.bab.ws.controller.DateUtil;
import kr.ac.pusan.bsclab.bab.ws.model.BCase;
import kr.ac.pusan.bsclab.bab.ws.model.BEvent;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;

public class SparkPrestoRepositoryReader2 implements ISparkRepositoryReader,
		IFilteredRepositoryReader<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>>, Serializable {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private ISparkExecutor executor;
	private IRepository repository;
	private JavaPairRDD<String, ICase> casesRDD;
	private Map<String, ICase> cases;
	private String repositoryURI;
	private String repositoryId;
	private List<IRepositoryFilter<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>>> filters;
	private Configuration config;

	public SparkPrestoRepositoryReader2(IExecutor se, String repositoryURI, Configuration config) {
		this.executor = (ISparkExecutor) se;
		try {
			// repositoryURI is
			// workspaceId/datasetId/resourceDataHash/configurationHash
			// change to this :
			// workspaceId/datasetId/resourceDataHash/resourceDataHash
			this.repositoryURI = repositoryURI;
			this.repositoryId = config.getFilterId().trim().length() > 0 ? "/" + config.getFilterId()
					: repositoryURI.split("/")[repositoryURI.split("/").length - 2];
			this.config = config;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IRepository getRepository() {
		try {
			if (repository == null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String repositoryFile = executor.getHdfsURI(repositoryURI + "/repository.brepo");
				if (!executor.getFileUtil().isFileExists(executor, repositoryFile)) {
					SparkIntermediateMappingJob mappingJob = new SparkIntermediateMappingJob();
					MappingJobConfiguration mapConfig = new MappingJobConfiguration();
					mapConfig.setRepositoryURI(repositoryURI);
					mapConfig.setName(repositoryFile);
					mapConfig.setDescription(repositoryFile);
					JavaPairRDD<String, BCase> casesRDD = getCasesRDD()
							.mapToPair(new PairFunction<Tuple2<String, ICase>, String, BCase>() {

								private static final long serialVersionUID = 1L;

								@Override
								public Tuple2<String, BCase> call(Tuple2<String, ICase> t) throws Exception {
									return new Tuple2<String, BCase>(t._1(), (BCase) t._2());
								}
							});
					BRepository repository = mappingJob.generateRepositorySummary(casesRDD, mapConfig, repositoryURI);
					String repositoryJson = mapper.writeValueAsString(repository);
					executor.getFileUtil().saveAsTextFile(executor, repositoryFile, repositoryJson);
					this.repository = repository;
				} else {
					String repo = executor.getFileUtil().loadTextFile(executor, repositoryFile);
					this.repository = mapper.readValue(repo, BRepository.class);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return repository;
	}

	@Override
	public JavaPairRDD<String, ICase> getCasesRDD() {

		if (casesRDD == null) {
			JavaSparkContext jsc = executor.getContext();
			ArrayList<String> caseIds = getCaseIds();
			JavaRDD<String> caseStrRdd = jsc.parallelize(caseIds, 2);
			final String logId = "CAST(" + repositoryURI.split("/")[repositoryURI.split("/").length - 1]
					+ " AS BIGINT)";
			casesRDD = caseStrRdd.mapPartitionsToPair(new PairFlatMapFunction<Iterator<String>, String, ICase>() {

				private static final long serialVersionUID = 1L;
				Connection conn;

				@Override
				public Iterator<Tuple2<String, ICase>> call(Iterator<String> t) throws Exception {
					Map<String, Tuple2<String, ICase>> result = new LinkedHashMap<String, Tuple2<String, ICase>>();
					if (!t.hasNext()) {
						return result.values().iterator();
					}
					if (conn == null) {
						conn = DriverManager.getConnection(config.getRepositoryDbString(),
								config.getRepositoryDbUsername(), config.getRepositoryDbPassword());
					}
					// String sql =
					// config.getRepositorySqlSelectEventPerCase().replaceAll("\\?",
					// " CAST(" + caseId + " AS BIGINT)");

					// String sql =
					// config.getRepositorySqlSelectEventPerCase().replaceAll("\\?",
					// "'" + caseId + "'");
					StringBuilder in = new StringBuilder();
					in.append("'");
					while (t.hasNext()) {
						in.append(t.next());
						in.append("','");
					}
					in.append("-'");
					String sql = "SELECT * FROM bab_log_events WHERE event_case_id IN (" + in + ") AND event_log_id = "
							+ logId + " ORDER BY event_case_id, event_timestamp";
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);
					while (rs.next()) {
						String caseId = rs.getString("event_case_id");
						if (!result.containsKey(caseId)) {
							result.put(caseId, new Tuple2<String, ICase>(caseId, new BCase(caseId, caseId)));
						}
						ICase bcase = result.get(caseId)._2();
						long ts = 0;
						try {
							ts = DateUtil.getInstance().fromDateTimeString(rs.getString("event_timestamp"));
						} catch (Exception ex) {
							ts = -1;
						}
						if (ts >= 0) {
							BEvent bevent = new BEvent(rs.getString("event_id"), rs.getString("event_id"),
									rs.getString("event_activity"), rs.getString("event_type"),
									rs.getString("event_originator"), ts, rs.getString("event_resource"));
							bcase.getEvents().put(String.valueOf(bcase.getEvents().size()), bevent);
						}
					}
					rs.close();
					stmt.close();
					return result.values().iterator();
				}
			});
			// casesRDD = caseStrRdd.mapToPair(CASE_MAPPER_FUNCTION);
			applyFilters();
		}
		return casesRDD;
	}

	public ArrayList<String> getCaseIds() {
		ArrayList<String> caseIds = new ArrayList<String>();
		try {
			Connection conn = DriverManager.getConnection(config.getRepositoryDbString(),
					config.getRepositoryDbUsername(), config.getRepositoryDbPassword());
			String sql = config.getRepositorySqlSelectCases().replaceAll("\\?",
					"CAST(" + repositoryURI.split("/")[repositoryURI.split("/").length - 1] + " AS BIGINT)");
			;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				caseIds.add(rs.getString("case_id"));
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return caseIds;
	}

	public final PairFunction<String, String, ICase> CASE_MAPPER_FUNCTION = new PairFunction<String, String, ICase>() {
		/**
		 * Default serial version ID
		 */
		private static final long serialVersionUID = 1L;
		Connection conn;

		@Override
		public Tuple2<String, ICase> call(String caseId) throws Exception {
			if (conn == null) {
				conn = DriverManager.getConnection(config.getRepositoryDbString(), config.getRepositoryDbUsername(),
						config.getRepositoryDbPassword());
			}
			// String sql =
			// config.getRepositorySqlSelectEventPerCase().replaceAll("\\?",
			// " CAST(" + caseId + " AS BIGINT)");
			String sql = config.getRepositorySqlSelectEventPerCase().replaceAll("\\?", "'" + caseId + "'");
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			BCase bcase = new BCase(caseId, caseId);
			while (rs.next()) {
				long ts = 0;
				try {
					ts = DateUtil.getInstance().fromDateTimeString(rs.getString("event_timestamp"));
				} catch (Exception ex) {
					ts = -1;
				}
				if (ts >= 0) {
					BEvent bevent = new BEvent(rs.getString("event_id"), rs.getString("event_id"),
							rs.getString("event_activity"), rs.getString("event_type"),
							rs.getString("event_originator"), ts, rs.getString("event_resource"));
					bcase.getEvents().put(bevent.getId(), bevent);
				}
			}
			rs.close();
			stmt.close();
			return new Tuple2<String, ICase>(bcase.getId(), bcase);
		}

	};

	@Override
	public Map<String, ICase> getCases() {
		if (cases == null) {
			cases = getCasesRDD().collectAsMap();
		}
		return cases;
	}

	@Override
	public List<IRepositoryFilter<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>>> getFilters() {
		return filters;
	}

	@Override
	public void applyFilters() {
		if (getFilters() != null) {
			for (IRepositoryFilter<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>> filter : getFilters()) {
				casesRDD = filter.apply(casesRDD);
			}
		}
	}

}
