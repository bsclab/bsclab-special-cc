/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.dl;

import java.io.Serializable;
import java.util.*;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.stat.Statistics;
import org.apache.spark.mllib.stat.test.ChiSqTestResult;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.HmmModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.Arc;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicBpmnMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkLinearHeuristicMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.idnm.IdleTimeNetwork;
import kr.ac.pusan.bsclab.bab.ws.api.model.idnm.SparkEmptyIdleTimeRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.api.model.idnm.SparkIdleTimeNetworkMiner;
import kr.ac.pusan.bsclab.bab.ws.api.model.idnm.SparkIdleTimeRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Element;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Gateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.SequenceFlow;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;
import kr.ac.pusan.bsclab.bab.ws.model.RepositoryResource;

/**
 * Delta analysis job for comparing two process model <br>
 * <br>
 * Config class: {@link DeltaAnalysisJobConfiguration}<br>
 * Result class: {@link DeltaAnalysisResult}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkDeltaAnalysisJob extends DeltaAnalysisJob implements Serializable {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	@BabService(name = "AnalysisDeltaJob", title = "Delta Analysis", requestClass = DeltaAnalysisJobConfiguration.class, responseClass = DeltaAnalysisResult.class, legacyJobExtension = ".dlans")
	public IJobResult run(String json, IResource res, IExecutor se) {
		try {
			// JavaSparkContext sc = se.getContext();
			// FileSystem fs = se.getHdfsFileSystem();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			final DeltaAnalysisJobConfiguration config = mapper.readValue(json, DeltaAnalysisJobConfiguration.class);
			String outputURI = se.getContextUri(res.getUri());
			boolean isLogToLogComparison = config.isRepository() && config.isCompareToRepository();
			// boolean isModelToModelComparison = !config.isRepository()
			// && !config.isCompareToRepository();
			//
			// String[] tempStrings = config.getRepositoryURI().split("/");
			// String repUri = "";
			// for (int i = 0; i < tempStrings.length - 1; i++) {
			// repUri += tempStrings[i] + "/";
			// }
			// repUri += tempStrings[tempStrings.length - 2];
			// config.setRepositoryURI(repUri);

			// 1. Create Result Object
			DeltaAnalysisResult result = new DeltaAnalysisResult(config);

			System.err.println("ASIS" + config.getRepositoryURI());
			System.err.println("TOBE" + config.getCompareToURI());

			SparkRepositoryReader expectedReader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);
			SparkRepositoryReader observedReader = new SparkRepositoryReader(se, config.getCompareToURI(), config);

			// 2. Create Repository Reader
			SparkIdleTimeRepositoryReader asisReader = !config.isRepository()
					? new SparkEmptyIdleTimeRepositoryReader(expectedReader, se, config)
					: new SparkIdleTimeRepositoryReader(expectedReader, config);
			SparkIdleTimeRepositoryReader tobeReader = !config.isCompareToRepository()
					? new SparkEmptyIdleTimeRepositoryReader(observedReader, se, config)
					: ((config.getMap().size() > 0)
							? new SparkDeltaRepositoryReader2(observedReader, config.getMap(), config)
							: new SparkIdleTimeRepositoryReader(observedReader, config));

			IRepository asisRepository = asisReader.getRepository();
			IRepository tobeRepository = tobeReader.getRepository();
			JavaPairRDD<String, ICase> asisCasesRDD = asisReader.getCasesRDD();
			JavaPairRDD<String, ICase> tobeCasesRDD = tobeReader.getCasesRDD();

			result.getAsisModel().setNoOfCases(asisRepository.getNoOfCases());
			result.getAsisModel().setNoOfEvents(asisRepository.getNoOfEvents());
			result.getTobeModel().setNoOfCases(tobeRepository.getNoOfCases());
			result.getTobeModel().setNoOfEvents(tobeRepository.getNoOfEvents());

			// 3. Get Heuristic Model
			HeuristicModel asisModelRaw = new HeuristicModel();
			HeuristicModel tobeModelRaw = new HeuristicModel();
			kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model asisBpmnModelRaw = new kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model();
			kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model tobeBpmnModelRaw = new kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model();

			SparkRepositoryReader expectedReader2 = new SparkRepositoryReader(se, config.getRepositoryURI(), config);
			SparkRepositoryReader observedReader2 = new SparkRepositoryReader(se, config.getCompareToURI(), config);

			if (config.isRepository()) {
				SparkIdleTimeRepositoryReader asisReader2 = !config.isRepository()
						? new SparkEmptyIdleTimeRepositoryReader(expectedReader2, se, config)
						: new SparkIdleTimeRepositoryReader(expectedReader2, config);
				HeuristicMinerJobConfiguration asisModelConfig = new HeuristicMinerJobConfiguration();
				asisModelConfig.setRepositoryURI(config.getRepositoryURI());
				SparkLinearHeuristicMinerJob asisModelJob = new SparkLinearHeuristicMinerJob();
				asisModelJob.run(mapper.writeValueAsString(asisModelConfig),
						new RepositoryResource(config.getRepositoryURI(), asisReader2), se);
				asisModelRaw = asisModelJob.getModel();
			} else {
				if (config.getRepositoryExtension()
						.equalsIgnoreCase(DeltaAnalysisJobConfiguration.IS_HEURISTIC_MODEL)) {
					String huri = se.getContextUri(config.getRepositoryURI() + "." + config.getRepositoryExtension());
					String hmodelJson = se.getFileUtil().loadTextFile(se, huri);
					asisModelRaw = mapper.readValue(hmodelJson, HeuristicModel.class);
				} else if (config.getRepositoryExtension()
						.equalsIgnoreCase(DeltaAnalysisJobConfiguration.IS_HBPMN_MODEL)) {
					String huri = se.getContextUri(config.getRepositoryURI() + "." + config.getRepositoryExtension());
					String hmodelJson = se.getFileUtil().loadTextFile(se, huri);
					asisBpmnModelRaw = mapper.readValue(hmodelJson,
							kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model.class);
				}
			}

			if (config.isCompareToRepository()) {
				SparkIdleTimeRepositoryReader tobeReader2 = !config.isCompareToRepository()
						? new SparkEmptyIdleTimeRepositoryReader(observedReader2, se, config)
						: ((config.getMap().size() > 0)
								? new SparkDeltaRepositoryReader2(observedReader2, config.getMap(), config)
								: new SparkIdleTimeRepositoryReader(observedReader2, config));
				HeuristicMinerJobConfiguration tobeModelConfig = new HeuristicMinerJobConfiguration();
				tobeModelConfig.setRepositoryURI(config.getCompareToURI());
				SparkLinearHeuristicMinerJob tobeModelJob = new SparkLinearHeuristicMinerJob();
				tobeModelJob.run(mapper.writeValueAsString(tobeModelConfig),
						new RepositoryResource(config.getCompareToURI(), tobeReader2), se);
				tobeModelRaw = tobeModelJob.getModel();
			} else {
				if (config.getCompareToExtension().equalsIgnoreCase(DeltaAnalysisJobConfiguration.IS_HEURISTIC_MODEL)) {
					String huri = se.getContextUri(config.getCompareToURI() + "." + config.getCompareToExtension());
					String hmodelJson = se.getFileUtil().loadTextFile(se, huri);
					tobeModelRaw = mapper.readValue(hmodelJson, HeuristicModel.class);
				} else if (config.getCompareToExtension()
						.equalsIgnoreCase(DeltaAnalysisJobConfiguration.IS_HBPMN_MODEL)) {
					String huri = se.getContextUri(config.getCompareToURI() + "." + config.getCompareToExtension());
					String hmodelJson = se.getFileUtil().loadTextFile(se, huri);
					tobeBpmnModelRaw = mapper.readValue(hmodelJson,
							kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model.class);
				}
			}

			final HeuristicModel asisModel = asisModelRaw;
			final HeuristicModel tobeModel = tobeModelRaw;

			result.getAsisModel().setRepository((BRepository) asisRepository);
			result.getAsisModel().setModel(asisModel);
			result.getAsisModel().setNoOfNodes(asisModel.getNodes().size());
			result.getAsisModel().setNoOfArcs(asisModel.getArcs().size());
			result.getTobeModel().setRepository((BRepository) tobeRepository);
			result.getTobeModel().setModel(tobeModel);
			result.getTobeModel().setNoOfNodes(tobeModel.getNodes().size());
			result.getTobeModel().setNoOfArcs(tobeModel.getArcs().size());

			// Diff model heuristic
			for (String node : asisModel.getNodes().keySet()) {
				if (tobeModel.getNodes().containsKey(node)) {
					result.getAsisModel().getNodeDifferences().put(node, 0);
				} else {
					result.getAsisModel().getNodeDifferences().put(node, 1);
				}
			}
			for (String node : tobeModel.getNodes().keySet()) {
				if (asisModel.getNodes().containsKey(node)) {
					result.getTobeModel().getNodeDifferences().put(node, 0);
				} else {
					result.getTobeModel().getNodeDifferences().put(node, 1);
				}
			}
			for (String arc : asisModel.getArcs().keySet()) {
				if (tobeModel.getArcs().containsKey(arc)) {
					result.getAsisModel().getArcDifferences().put(arc, 0);
				} else {
					result.getAsisModel().getArcDifferences().put(arc, 1);
				}
			}
			for (String arc : tobeModel.getArcs().keySet()) {
				if (asisModel.getArcs().containsKey(arc)) {
					result.getTobeModel().getArcDifferences().put(arc, 0);
				} else {
					result.getTobeModel().getArcDifferences().put(arc, 1);
				}
			}

			// 3.1. Get BPMN Model
			kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model asisBpmnModel = config
					.getRepositoryExtension().equalsIgnoreCase(DeltaAnalysisJobConfiguration.IS_HBPMN_MODEL)
							? asisBpmnModelRaw : new SparkHeuristicBpmnMinerJob().getModel(asisModel);
			kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model tobeBpmnModel = config
					.getCompareToExtension().equalsIgnoreCase(DeltaAnalysisJobConfiguration.IS_HBPMN_MODEL)
							? tobeBpmnModelRaw : new SparkHeuristicBpmnMinerJob().getModel(tobeModel);

			// Diff model bpmn
			Map<String, Element> asisBpmnElements = new LinkedHashMap<String, Element>();
			Map<String, Element> tobeBpmnElements = new LinkedHashMap<String, Element>();
			for (Element e : asisBpmnModel.getProcess().getElements()) {
				asisBpmnElements.put(e.getId(), e);
				if (e instanceof SequenceFlow) {
					asisBpmnElements.put(((SequenceFlow) e).getSourceRef() + "TO" + ((SequenceFlow) e).getTargetRef(),
							e);
				} else if (e instanceof Gateway) {
					// StringBuilder sb = new StringBuilder();
					// for (String s : ((Gateway) e).getIncoming()) {
					// sb.append(s).append("+");
					// }
					// sb.append("TO");
					// for (String s : ((Gateway) e).getOutgoing()) {
					// sb.append(s).append("+");
					// }
					asisBpmnElements.put(e.getId(), e);
				} else {
					asisBpmnElements.put(e.getName(), e);
				}
			}
			for (Element e : tobeBpmnModel.getProcess().getElements()) {
				tobeBpmnElements.put(e.getId(), e);
				if (e instanceof SequenceFlow) {
					tobeBpmnElements.put(((SequenceFlow) e).getSourceRef() + "TO" + ((SequenceFlow) e).getTargetRef(),
							e);
				} else if (e instanceof Gateway) {

					tobeBpmnElements.put(e.getId(), e);
				} else {
					tobeBpmnElements.put(e.getName(), e);
				}
			}
			for (Element e : asisBpmnModel.getProcess().getElements()) {
				if (e instanceof SequenceFlow) {
					if ((!(asisBpmnElements.get(((SequenceFlow) e).getSourceRef()) instanceof Gateway))
							&& (!(asisBpmnElements.get(((SequenceFlow) e).getSourceRef()) instanceof Gateway))) {
						String arcName = ((SequenceFlow) e).getSourceRef() + "TO" + ((SequenceFlow) e).getTargetRef();
						if (tobeBpmnElements.containsKey(arcName)) {
							result.getAsisModel().getBpmnArcDifferences().put(e.getId(), 0);
						} else {
							result.getAsisModel().getBpmnArcDifferences().put(e.getId(), 1);
						}
					} else {
						// compare node to gateway arc
					}
				} else if (e instanceof Gateway) {
					// compare gateway to gateway
				} else {
					if (tobeBpmnElements.containsKey(e.getName())) {
						result.getAsisModel().getBpmnNodeDifferences().put(e.getName(), 0);
					} else {
						result.getAsisModel().getBpmnNodeDifferences().put(e.getName(), 1);
					}
				}
			}
			for (Element e : tobeBpmnModel.getProcess().getElements()) {
				if (e instanceof SequenceFlow) {
					if ((!(tobeBpmnElements.get(((SequenceFlow) e).getSourceRef()) instanceof Gateway))
							&& (!(tobeBpmnElements.get(((SequenceFlow) e).getSourceRef()) instanceof Gateway))) {
						String arcName = ((SequenceFlow) e).getSourceRef() + "TO" + ((SequenceFlow) e).getTargetRef();
						if (asisBpmnElements.containsKey(arcName)) {
							result.getAsisModel().getBpmnArcDifferences().put(e.getId(), 0);
						} else {
							result.getAsisModel().getBpmnArcDifferences().put(e.getId(), 1);
						}
					} else {
						// compare node to gateway arc
					}
				} else if (e instanceof Gateway) {
					// compare gateway to gateway
				} else {
					if (asisBpmnElements.containsKey(e.getName())) {
						result.getAsisModel().getBpmnNodeDifferences().put(e.getName(), 0);
					} else {
						result.getAsisModel().getBpmnNodeDifferences().put(e.getName(), 1);
					}
				}
			}

			// String k = "";
			// String v = "";
			// if (e instanceof Gateway) {
			// StringBuilder sb = new StringBuilder();
			// for (String s : ((Gateway) e).getIncoming()) {
			// sb.append(s).append("+");
			// }
			// k = sb.toString();
			// sb = new StringBuilder();
			// for (String s : ((Gateway) e).getOutgoing()) {
			// sb.append(s).append("+");
			// }
			// k += "TO" + sb.toString();
			// v = e.getName();
			// asisBpmnElements.put(k, v);
			// asisBpmnElements.put(e.getId(), v);
			// } else {
			// k = e.getName();
			// v = e.getName();
			// asisBpmnElements.put(k, v);
			// }
			// }
			result.getAsisModel().setBpmnModel(asisBpmnModel);
			result.getAsisModel().setBpmnModel(tobeBpmnModel);

			// 4. Get Fitness Evaluation
			final HmmModel asisHmmModel = new HmmModel();
			final HmmModel tobeHmmModel = new HmmModel();
			for (Arc a : asisModel.getArcs().values())
				asisHmmModel.enableTransition(a.getSource(), a.getTarget());
			for (Arc a : tobeModel.getArcs().values())
				tobeHmmModel.enableTransition(a.getSource(), a.getTarget());

			long asisCount = asisCasesRDD.count();
			long tobeCount = tobeCasesRDD.count();

			System.err.println("IQ 1: " + asisCount);
			System.err.println("IQ 2: " + tobeCount);

			if (asisCount > 0) {
				double asisToAsisFitness = asisCasesRDD.mapToDouble(new Replayer(asisHmmModel)).sum();
				double tobeToAsisFitness = asisCasesRDD.mapToDouble(new Replayer(tobeHmmModel)).sum();
				result.getAsisModel().setFitness(asisToAsisFitness / asisCount);
				result.getTobeModel().setCrossFitness(tobeToAsisFitness / asisCount);
			}

			if (tobeCount > 0) {
				double asisToTobeFitness = tobeCasesRDD.mapToDouble(new Replayer(asisHmmModel)).sum();
				double tobeToTobeFitness = tobeCasesRDD.mapToDouble(new Replayer(tobeHmmModel)).sum();
				result.getTobeModel().setFitness(tobeToTobeFitness / tobeCount);
				result.getAsisModel().setCrossFitness(asisToTobeFitness / tobeCount);
			}

			// ONLY FOR LOG TO LOG
			if (config.getCompareLogToLog() != 0 && isLogToLogComparison) {

				// 5. Idle Time Network Evaluation
				SparkIdleTimeNetworkMiner miner = new SparkIdleTimeNetworkMiner();
				IdleTimeNetwork asisIdn = miner.run(asisReader);
				IdleTimeNetwork tobeIdn = miner.run(tobeReader);
				result.getAsisModel().setIdnNetwork(asisIdn);
				result.getTobeModel().setIdnNetwork(tobeIdn);

				// 1. Temporal Tree-based Process Model

				// 2. Temporal Fitness

				final boolean asisNonInstantCompletion = asisRepository.getActivityTypes().containsKey("start")
						&& asisRepository.getActivityTypes().containsKey("complete");
				final boolean tobeNonInstantCompletion = tobeRepository.getActivityTypes().containsKey("start")
						&& tobeRepository.getActivityTypes().containsKey("complete");

				// Activity level
				JavaPairRDD<String, List<TEvent>> asisActivitiesRDD = asisCasesRDD
						.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, List<TEvent>>() {

							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public Iterator<Tuple2<String, List<TEvent>>> call(Tuple2<String, ICase> arg0)
									throws Exception {
								Map<String, List<TEvent>> tempResult = new LinkedHashMap<String, List<TEvent>>();
								Map<String, Stack<IEvent>> T = new LinkedHashMap<String, Stack<IEvent>>();
								for (IEvent e : arg0._2().getEvents().values()) {
									String a = e.getLabel();
									if (!T.containsKey(a))
										T.put(a, new Stack<IEvent>());
									if (!asisNonInstantCompletion) {
										if (!tempResult.containsKey(a))
											tempResult.put(a, new ArrayList<TEvent>());
										tempResult.get(a).add(new TEvent(a, e.getTimestamp(), e.getTimestamp()));
									} else {
										if (e.getType().equalsIgnoreCase("start")) {
											T.get(a).push(e);
										} else if (e.getType().equalsIgnoreCase("complete")) {
											if (T.get(a).size() > 0) {
												IEvent start = T.get(a).pop();
												IEvent complete = e;
												if (!tempResult.containsKey(a))
													tempResult.put(a, new ArrayList<TEvent>());
												tempResult.get(a).add(
														new TEvent(a, start.getTimestamp(), complete.getTimestamp()));
											}
										}
									}
								}
								List<Tuple2<String, List<TEvent>>> result = new ArrayList<Tuple2<String, List<TEvent>>>();
								for (String a : tempResult.keySet()) {
									result.add(new Tuple2<String, List<TEvent>>(a, tempResult.get(a)));
								}
								return result.iterator();
							}
						}).reduceByKey(new Function2<List<TEvent>, List<TEvent>, List<TEvent>>() {
							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public List<TEvent> call(List<TEvent> arg0, List<TEvent> arg1) throws Exception {
								TEvent[] a0 = arg0.toArray(new TEvent[0]);
								TEvent[] a1 = arg1.toArray(new TEvent[0]);
								int max = a0.length < a1.length ? a1.length : a0.length;
								List<TEvent> result = new ArrayList<TEvent>();
								if (a0.length < 1)
									return null;
								String a = a0[0].getName();
								for (int i = 0; i < max; i++) {
									long started = 0;
									long ended = 0;
									if (i < a0.length) {
										started += a0[i].getStart();
										ended += a0[i].getEnd();
									}
									if (i < a1.length) {
										started += a1[i].getStart();
										ended += a1[i].getEnd();
									}
									started /= 2;
									ended /= 2;
									result.add(new TEvent(a, started, ended));
								}
								return result;
							}
						});
				// .cache();

				JavaPairRDD<String, List<TEvent>> tobeActivitiesRDD = tobeCasesRDD
						.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, List<TEvent>>() {

							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public Iterator<Tuple2<String, List<TEvent>>> call(Tuple2<String, ICase> arg0)
									throws Exception {
								Map<String, List<TEvent>> tempResult = new LinkedHashMap<String, List<TEvent>>();
								Map<String, Stack<IEvent>> T = new LinkedHashMap<String, Stack<IEvent>>();
								for (IEvent e : arg0._2().getEvents().values()) {
									String a = e.getLabel();
									if (!T.containsKey(a))
										T.put(a, new Stack<IEvent>());
									if (!tobeNonInstantCompletion) {
										if (!tempResult.containsKey(a))
											tempResult.put(a, new ArrayList<TEvent>());
										tempResult.get(a).add(new TEvent(a, e.getTimestamp(), e.getTimestamp()));
									} else {
										if (e.getType().equalsIgnoreCase("start")) {
											T.get(a).push(e);
										} else if (e.getType().equalsIgnoreCase("complete")) {
											if (T.get(a).size() > 0) {
												IEvent start = T.get(a).pop();
												IEvent complete = e;
												if (!tempResult.containsKey(a))
													tempResult.put(a, new ArrayList<TEvent>());
												tempResult.get(a).add(
														new TEvent(a, start.getTimestamp(), complete.getTimestamp()));
											}
										}
									}
								}
								List<Tuple2<String, List<TEvent>>> result = new ArrayList<Tuple2<String, List<TEvent>>>();
								for (String a : tempResult.keySet()) {
									result.add(new Tuple2<String, List<TEvent>>(a, tempResult.get(a)));
								}
								return result.iterator();
							}
						}).reduceByKey(new Function2<List<TEvent>, List<TEvent>, List<TEvent>>() {
							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public List<TEvent> call(List<TEvent> arg0, List<TEvent> arg1) throws Exception {
								TEvent[] a0 = arg0.toArray(new TEvent[0]);
								TEvent[] a1 = arg1.toArray(new TEvent[0]);
								int max = a0.length < a1.length ? a1.length : a0.length;
								List<TEvent> result = new ArrayList<TEvent>();
								if (a0.length < 1)
									return null;
								String a = a0[0].getName();
								for (int i = 0; i < max; i++) {
									long started = 0;
									long ended = 0;
									if (i < a0.length) {
										started += a0[i].getStart();
										ended += a0[i].getEnd();
									}
									if (i < a1.length) {
										started += a1[i].getStart();
										ended += a1[i].getEnd();
									}
									started /= 2;
									ended /= 2;
									result.add(new TEvent(a, started, ended));
								}
								return result;
							}
						});
				// .cache();

				Map<String, Vector> activityLevelTPI = asisActivitiesRDD.join(tobeActivitiesRDD).groupByKey().mapToPair(
						new PairFunction<Tuple2<String, Iterable<Tuple2<List<TEvent>, List<TEvent>>>>, String, Vector>() {

							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public Tuple2<String, Vector> call(
									Tuple2<String, Iterable<Tuple2<List<TEvent>, List<TEvent>>>> arg0)
									throws Exception {
								double temporalDistance = 0;
								int activityOccurrenceL1 = 0;
								int activityOccurrenceL2 = 0;
								int successfulMapping = 0;
								double totalSpeedUp = 0;
								String activity = arg0._1();
								List<TEvent> L1 = new ArrayList<TEvent>();
								List<TEvent> L2 = new ArrayList<TEvent>();
								for (Tuple2<List<TEvent>, List<TEvent>> t : arg0._2()) {
									List<TEvent> L1A = t._1();
									if (L1.size() < L1A.size()) {
										int m = L1A.size() - L1.size();
										for (int i = 0; i < m; i++) {
											L1.add(new TEvent(activity, 0, 0));
										}
									}
									int j = 0;
									for (TEvent e : L1A) {
										TEvent s = L1.get(j);
										s.setStart(s.getStart() + e.getStart());
										s.setEnd(s.getEnd() + e.getEnd());
										s.setCount(s.getCount() + 1);
										j++;
									}
									List<TEvent> L2A = t._2();
									if (L2.size() < L2A.size()) {
										int m = L2A.size() - L2.size();
										for (int i = 0; i < m; i++) {
											L2.add(new TEvent(activity, 0, 0));
										}
									}
									j = 0;
									for (TEvent e : L2A) {
										TEvent s = L2.get(j);
										s.setStart(s.getStart() + e.getStart());
										s.setEnd(s.getEnd() + e.getEnd());
										s.setCount(s.getCount() + 1);
										j++;
									}
								}
								for (TEvent e : L1) {
									e.doAverage();
								}
								for (TEvent e : L2) {
									e.doAverage();
								}
								activityOccurrenceL1 = L1.size();
								activityOccurrenceL2 = L2.size();
								int min = activityOccurrenceL1 <= activityOccurrenceL2 ? activityOccurrenceL1
										: activityOccurrenceL2;
								for (int k = 0; k < min; k++) {
									TEvent E1 = L1.get(k);
									TEvent E2 = L2.get(k);
									if (E1.getName().equalsIgnoreCase(E2.getName())) {
										double[] td = getTemporalDistance(E1.getStart(), E1.getEnd(), E2.getStart(),
												E2.getEnd(), 1);
										temporalDistance += td[0];
										totalSpeedUp += td[1];
										successfulMapping++;
									}

								}
								double denom = activityOccurrenceL1 + activityOccurrenceL2 - successfulMapping;
								double tpi = temporalDistance / denom; // meninggalkan
																		// masalah
								double speedup = totalSpeedUp / denom;
								return new Tuple2<String, Vector>(arg0._1(),
										Vectors.dense(tpi, speedup, 1, temporalDistance, totalSpeedUp,
												activityOccurrenceL1, activityOccurrenceL2, successfulMapping));
							}
						}).collectAsMap();

				String[] sinting = asisActivitiesRDD.keys().collect().toArray(new String[0]);
				for (String s : sinting) {
					System.err.println("ASIS: " + s);
				}
				sinting = tobeActivitiesRDD.keys().collect().toArray(new String[0]);
				for (String s : sinting) {
					System.err.println("TOBE: " + s);
				}
				sinting = activityLevelTPI.keySet().toArray(new String[0]);
				for (String s : sinting) {
					System.err.println("JOIN: " + s);
				}

				double AL1T = asisRepository.getNoOfActivities();
				double AL2T = tobeRepository.getNoOfActivities();
				double AM = 0;
				double AtotalTPI = 0;
				double AtotalSpeedUp = 0;
				Map<String, Double> activityTPIs = new LinkedHashMap<String, Double>();
				for (String activityId : activityLevelTPI.keySet()) {
					double[] data = activityLevelTPI.get(activityId).toArray();
					double tpi = data[0];
					double speedup = data[1];
					double m = data[2];
					AtotalTPI += tpi;
					AtotalSpeedUp += speedup;
					AM += m;
					activityTPIs.put(activityId, tpi);
				}
				double Adenom = AL1T + AL2T - AM;
				result.setActivityTPIs(activityTPIs);
				result.setOverallActivityTPI(Adenom == 0 ? null : AtotalTPI / Adenom);
				result.setOverallActivitySpeedup(Adenom == 0 ? null : AtotalSpeedUp / Adenom);

				JavaPairRDD<String, List<Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>>> casePairs = asisCasesRDD
						.join(tobeCasesRDD).groupByKey().mapToPair(
								new PairFunction<Tuple2<String, Iterable<Tuple2<ICase, ICase>>>, String, List<Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>>>() {

									/**
									 * 
									 */
									private static final long serialVersionUID = 1L;

									@Override
									public Tuple2<String, List<Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>>> call(
											Tuple2<String, Iterable<Tuple2<ICase, ICase>>> arg0) throws Exception {
										String caseId = arg0._1();
										List<Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>> pairs = new ArrayList<Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>>();
										for (Tuple2<ICase, ICase> casePair : arg0._2()) {
											Map<String, List<TEvent>> L1 = new LinkedHashMap<String, List<TEvent>>();
											Map<String, List<TEvent>> L2 = new LinkedHashMap<String, List<TEvent>>();
											Map<String, Stack<IEvent>> T = new LinkedHashMap<String, Stack<IEvent>>();
											T.clear();
											for (IEvent e : casePair._1().getEvents().values()) {
												String a = e.getLabel();
												if (!T.containsKey(a))
													T.put(a, new Stack<IEvent>());
												if (!asisNonInstantCompletion) {
													if (!L1.containsKey(a))
														L1.put(a, new ArrayList<TEvent>());
													L1.get(a).add(new TEvent(a, e.getTimestamp(), e.getTimestamp()));
												} else {
													if (e.getType().equalsIgnoreCase("start")) {
														T.get(a).push(e);
													} else if (e.getType().equalsIgnoreCase("complete")) {
														if (T.get(a).size() > 0) {
															IEvent start = T.get(a).pop();
															IEvent complete = e;
															if (!L1.containsKey(a))
																L1.put(a, new ArrayList<TEvent>());
															L1.get(a).add(new TEvent(a, start.getTimestamp(),
																	complete.getTimestamp()));
														}
													}
												}
											}
											T.clear();
											for (IEvent e : casePair._2().getEvents().values()) {
												String a = e.getLabel();
												if (!T.containsKey(a))
													T.put(a, new Stack<IEvent>());
												if (!tobeNonInstantCompletion) {
													if (!L2.containsKey(a))
														L2.put(a, new ArrayList<TEvent>());
													L2.get(a).add(new TEvent(a, e.getTimestamp(), e.getTimestamp()));
												} else {
													if (e.getType().equalsIgnoreCase("start")) {
														T.get(a).push(e);
													} else if (e.getType().equalsIgnoreCase("complete")) {
														if (T.get(a).size() > 0) {
															IEvent start = T.get(a).pop();
															IEvent complete = e;
															if (!L2.containsKey(a))
																L2.put(a, new ArrayList<TEvent>());
															L2.get(a).add(new TEvent(a, start.getTimestamp(),
																	complete.getTimestamp()));
														}
													}
												}
											}
											pairs.add(new Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>(
													L1, L2));
										}
										return new Tuple2<String, List<Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>>>(
												caseId, pairs);
									}
								});

				Map<String, Vector> traceLevelTPI = casePairs.mapToPair(
						new PairFunction<Tuple2<String, List<Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>>>, String, Vector>() {

							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public Tuple2<String, Vector> call(
									Tuple2<String, List<Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>>>> arg0)
									throws Exception {
								double temporalDistance = 0;
								int activityOccurrenceL1 = 0;
								int activityOccurrenceL2 = 0;
								int successfulMapping = 0;
								double totalSpeedUp = 0;
								for (Tuple2<Map<String, List<TEvent>>, Map<String, List<TEvent>>> pairs : arg0._2()) {
									Map<String, List<TEvent>> L1 = pairs._1();
									Map<String, List<TEvent>> L2 = pairs._2();
									for (String a : L1.keySet()) {
										if (L2.containsKey(a)) {
											TEvent[] L1E = L1.get(a).toArray(new TEvent[0]);
											TEvent[] L2E = L2.get(a).toArray(new TEvent[0]);
											activityOccurrenceL1 += L1E.length;
											activityOccurrenceL2 += L2E.length;
											for (int i = 0; i < L1E.length; i++) {
												if (i >= L2E.length)
													break;
												TEvent E1 = L1E[i];
												TEvent E2 = L2E[i];
												if (E1.getName().equalsIgnoreCase(E2.getName())) {
													double[] td = getTemporalDistance(E1.getStart(), E1.getEnd(),
															E2.getStart(), E2.getEnd(), 1);
													temporalDistance += td[0];
													totalSpeedUp += td[1];
													successfulMapping++;
												}
											}
										}
									}
								}
								double denom = activityOccurrenceL1 + activityOccurrenceL2 - successfulMapping;
								double tpi = temporalDistance / denom; // meninggalkan
																		// masalah
								double speedup = totalSpeedUp / denom;
								return new Tuple2<String, Vector>(arg0._1(),
										Vectors.dense(tpi, speedup, 1, temporalDistance, totalSpeedUp,
												activityOccurrenceL1, activityOccurrenceL2, successfulMapping));
							}
						}).collectAsMap();

				double L1T = asisRepository.getNoOfCases();
				double L2T = tobeRepository.getNoOfCases();
				double M = 0;
				double totalTPI = 0;
				double totalSpeedUp = 0;
				Map<String, Double> traceTPIs = new LinkedHashMap<String, Double>();
				for (String traceId : traceLevelTPI.keySet()) {
					double[] data = traceLevelTPI.get(traceId).toArray();
					double tpi = data[0];
					double speedup = data[1];
					double m = data[2];
					totalTPI += tpi;
					totalSpeedUp += speedup;
					M += m;
					traceTPIs.put(traceId, tpi);
				}
				double denom = L1T + L2T - M;
				result.setTraceTPIs(traceTPIs);
				result.setOverallTraceTPI(denom == 0 ? null : totalTPI / denom);
				result.setOverallTraceSpeedup(denom == 0 ? null : totalSpeedUp / denom);

				// 5.4 Statistical Test
				Map<String, Integer> idnActWithLabels = new LinkedHashMap<String, Integer>();
				// for (String s : idnActs) {
				// for (String t : idnActs) {
				// idnActWithLabels.put(s + "||" + t, idnActWithLabels.size());
				// }
				// }
				for (String s : asisIdn.getLogicalTransitionTime().keySet()) {
					idnActWithLabels.put(s, idnActWithLabels.size());
				}

				double[] asisFeature = new double[idnActWithLabels.size()];
				double[] tobeFeature = new double[idnActWithLabels.size()];
				for (String s : idnActWithLabels.keySet()) {
					int i = idnActWithLabels.get(s);

					if (asisIdn.getLogicalTransitionTime().containsKey(s)) {
						int c = 0;
						Double[] d = asisIdn.getLogicalTransitionTime().get(s);
						for (int j = 0; j < d.length; j += 4) {
							c += d[j + 1];
						}
						asisFeature[i] = c;
					}

					if (tobeIdn.getLogicalTransitionTime().containsKey(s)) {
						int c = 0;
						Double[] d = tobeIdn.getLogicalTransitionTime().get(s);
						for (int j = 0; j < d.length; j += 4) {
							c += d[j + 1];
						}
						tobeFeature[i] = c;
					}
				}

				ChiSqTestResult hr = Statistics.chiSqTest(Vectors.dense(tobeFeature), Vectors.dense(asisFeature));
				result.setChiDegreeOfFreedom(hr.degreesOfFreedom());
				result.setChiPValue(hr.pValue());
				result.setChiStatistic(hr.statistic());
				result.setChiNullHypothesis(hr.nullHypothesis());
			}

			// 5. Block Extraction

			// final HeuristicModel asisModel = asisModelJob.getModel();
			// final HeuristicModel tobeModel = tobeModelJob.getModel();

			new BlockDetection(asisModel, result.getAsisModel()).run();
			new BlockDetection(tobeModel, result.getTobeModel()).run();

			// SparkRepositoryReader reader = new SparkRepositoryReader(se,
			// config.getRepositoryURI());
			// IRepository repository = reader.getRepository();

			RawJobResult jobResult = new RawJobResult("analysis.Delta", outputURI, outputURI,
					mapper.writeValueAsString(result));
			se.getFileUtil().saveAsTextFile(se, outputURI + ".dlans", jobResult.getResponse());
			return jobResult;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public double[] getTemporalDistance(long iL1Start, long iL1End, long iL2Start, long iL2End, long divider) {
		double temporalDistance = -2d;
		double temporalSpeedup = 0d;
		long direction = 1;
		iL1Start /= divider;
		iL1End /= divider;
		iL2Start /= divider;
		iL2End /= divider;
		long L1Start = iL1Start;
		long L1End = iL1End;
		long L2Start = iL2Start;
		long L2End = iL2End;
		long L1Duration = L1End - L1Start;
		long L2Duration = L2End - L2Start;

		if (L1Duration == L2Duration) {
			if (L2End < L1Start) { // Cond. 1
				temporalDistance = 0.5;
				temporalSpeedup = L2Duration + (L1Start - L2End);
			} else if (L2End == L1Start) { // Cond. 2
				temporalDistance = 0.33;
				temporalSpeedup = L2Duration;
			} else if (L2Start < L1Start) { // Cond. 3
				temporalDistance = 0.17;
				temporalSpeedup = L1Start - L2Start;
			} else if (L2Start == L1Start) {// Cond. 4
				temporalDistance = 0.0;
			} else if (L2Start > L1End) { // Cond. 7
				temporalDistance = -0.5;
				temporalSpeedup = -L2Duration - (L2Start - L1End);
			} else if (L2Start == L1End) { // Cond. 6
				temporalDistance = -0.33;
				temporalSpeedup = -L2Duration;
			} else { // Cond. 5
				temporalDistance = -0.17;
				temporalSpeedup = L1Start - L2Start;
			}
		} else if (L2Duration < L1Duration) {
			if (L1Start > L2End) { // Cond. 8
				temporalDistance = 1;
				temporalSpeedup = L1Duration + (L1Start - L2End);
			} else if (L1Start == L2End) { // Cond. 9
				temporalDistance = 0.9;
				temporalSpeedup = L1Duration;
			} else if (L1Start > L2Start) { // Cond. 10
				temporalDistance = 0.8;
				temporalSpeedup = L1End - L2End;
			} else if (L1Start == L2Start) {// Cond. 11
				temporalDistance = 0.7;
				temporalSpeedup = L1End - L2End;
			} else if (L1End == L2End) { // Cond. 13
				temporalDistance = 0.5;
			} else if (L1End < L2Start) { // Cond. 16
				temporalDistance = 0.2;
				temporalSpeedup = -L2Duration - (L2Start - L1End);
			} else if (L1End == L2Start) { // Cond. 15
				temporalDistance = 0.3;
				temporalSpeedup = -L2Duration;
			} else if (L1End < L2End) { // Cond. 14
				temporalDistance = 0.4;
				temporalSpeedup = -(L2End - L1End);
			} else { // Cond. 12
				temporalDistance = 0.6;
				temporalSpeedup = L1End - L2End;
			}
		} else {
			if (L2End < L1Start) { // Cond. 17
				temporalDistance = -0.2;
				temporalSpeedup = -(L1Duration + (L1Start - L2End));
			} else if (L1Start == L2End) { // Cond. 18
				temporalDistance = -0.3;
				temporalSpeedup = -L1Duration;
			} else if (L2End < L1End) { // Cond. 19
				temporalDistance = -0.4;
				temporalSpeedup = -(L1End - L2End);
			} else if (L1End == L2End) { // Cond. 20
				temporalDistance = -0.5;
			} else if (L1Start == L2Start) {// Cond. 22
				temporalDistance = -0.7;
				temporalSpeedup = -(L2End - L1End);
			} else if (L2Start > L1End) { // Cond. 25
				temporalDistance = -1;
				temporalSpeedup = -(L2Duration + (L2Start - L1End));
			} else if (L2Start == L1End) { // Cond. 24
				temporalDistance = -0.9;
				temporalSpeedup = -L2Duration;
			} else if (L1Start < L2Start) { // Cond. 23
				temporalDistance = -0.8;
				temporalSpeedup = -(L2End - L1End);
			} else { // Cond. 21
				temporalDistance = -0.6;
				temporalSpeedup = L1End - L2End;
			}
		}

		return new double[] { temporalDistance * direction, temporalSpeedup * direction };
	}

}
