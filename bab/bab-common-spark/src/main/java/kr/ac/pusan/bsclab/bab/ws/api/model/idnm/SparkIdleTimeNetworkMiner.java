/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.idnm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Variant of heuristic miner algorithm with considering idle time <br>
 * <br>
 * Config class: {@link IdleTimeNetworkConfiguration}<br>
 * Result class: {@link IdleTimeNetwork}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkIdleTimeNetworkMiner extends IdleTimeNetworkMiner {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	public IdleTimeNetwork run(SparkIdleTimeRepositoryReader reader) {
		reader.getClusteredCases();

		// 0. Configuration
		final double divider = 1000 * 60 * 60 * 24; // days

		// 1. Get Logical Start Time for Each Node and Duration for Each Node
		Map<String, Double[]> nodeLogicalStartTimes = reader.getCasesRDD()
				.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Vector>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Iterator<Tuple2<String, Vector>> call(Tuple2<String, ICase> arg0) throws Exception {
						Map<String, List<Double>> result = new HashMap<String, List<Double>>();
						for (IEvent e : arg0._2().getEvents().values()) {
							String name = e.getLabel() + "||" + e.getType();
							if (!result.containsKey(name))
								result.put(name, new ArrayList<Double>());
							double timestamp = (double) e.getTimestamp() / divider;
							// Every timestamp = [timestamp, 1(count),
							// 0(variance)]
							result.get(name).add(timestamp);
							result.get(name).add(1d);
							result.get(name).add(0d); // For Average
							result.get(name).add(0d); // For Variance
						}
						List<Tuple2<String, Vector>> startTimes = new ArrayList<Tuple2<String, Vector>>();
						for (String s : result.keySet()) {
							startTimes.add(new Tuple2<String, Vector>(s,
									Vectors.dense(ArrayUtils.toPrimitive(result.get(s).toArray(new Double[0])))));
						}
						return startTimes.iterator();
					}
				}).groupByKey().mapToPair(new PairFunction<Tuple2<String, Iterable<Vector>>, String, Vector>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, Vector> call(Tuple2<String, Iterable<Vector>> arg0) throws Exception {
						int size = 0;
						for (Vector v : arg0._2()) {
							if (v.size() > size)
								size = v.size();
						}
						double[] result = new double[size];
						for (Vector v : arg0._2()) {
							double[] va = v.toArray();
							for (int i = 0; i < va.length; i++) {
								result[i] += va[i];
							}
						}
						// Calculate Average
						for (int i = 0; i < result.length; i += 4) {
							result[i + 2] = (result[i + 1] == 0) ? 0 : result[i] / result[i + 1];
						}
						// Calculate Variance
						for (Vector v : arg0._2()) {
							double[] va = v.toArray();
							for (int i = 0; i < va.length; i += 4) {
								result[i + 3] += (result[i] - result[i + 2]) * (result[i] - result[i + 2]);
							}
						}
						for (int i = 0; i < result.length; i += 4) {
							result[i + 3] = (result[i + 1] == 0) ? 0 : result[i + 3] / result[i + 1];
						}
						return new Tuple2<String, Vector>(arg0._1(), Vectors.dense(result));
					}
				}).sortByKey().mapToPair(new PairFunction<Tuple2<String, Vector>, String, Double[]>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, Double[]> call(Tuple2<String, Vector> arg0) throws Exception {
						return new Tuple2<String, Double[]>(arg0._1(), ArrayUtils.toObject(arg0._2().toArray()));
					}
				}).collectAsMap();

		// 2. Get Logical Processing Time for Each Node and Duration for Each
		// Node
		Map<String, Double[]> nodeLogicalProcessingTimes = reader.getCasesRDD()
				.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Vector>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Iterator<Tuple2<String, Vector>> call(Tuple2<String, ICase> arg0) throws Exception {
						Map<String, List<Double>> result = new HashMap<String, List<Double>>();

						Map<Double, List<IEvent>> sortedEvent = new TreeMap<Double, List<IEvent>>();
						for (IEvent e : arg0._2().getEvents().values()) {
							double timestamp = e.getTimestamp() / divider;
							if (!sortedEvent.containsKey(timestamp))
								sortedEvent.put((double) timestamp, new ArrayList<IEvent>());
							sortedEvent.get(timestamp).add(e);
						}

						List<IEvent> pes = null;
						double pt = 0d;
						for (Double t : sortedEvent.keySet()) {
							if (pes == null) {
								pt = t;
								pes = sortedEvent.get(t);
								continue;
							}
							for (IEvent pe : pes) {
								for (IEvent e : sortedEvent.get(t)) {
									e.toString();
									String name = pe.getLabel() + "||" + pe.getType();
									if (!result.containsKey(name))
										result.put(name, new ArrayList<Double>());
									result.get(name).add(t - pt);
									result.get(name).add(1d);
									result.get(name).add(0d); // For
																// Average
									result.get(name).add(0d); // For
																// Variance
								}
							}
							pt = t;
							pes = sortedEvent.get(t);
						}

						List<Tuple2<String, Vector>> startTimes = new ArrayList<Tuple2<String, Vector>>();
						for (String s : result.keySet()) {
							startTimes.add(new Tuple2<String, Vector>(s,
									Vectors.dense(ArrayUtils.toPrimitive(result.get(s).toArray(new Double[0])))));
						}
						return startTimes.iterator();
					}
				}).groupByKey().mapToPair(new PairFunction<Tuple2<String, Iterable<Vector>>, String, Vector>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, Vector> call(Tuple2<String, Iterable<Vector>> arg0) throws Exception {
						int size = 0;
						for (Vector v : arg0._2()) {
							if (v.size() > size)
								size = v.size();
						}
						double[] result = new double[size];
						for (Vector v : arg0._2()) {
							double[] va = v.toArray();
							for (int i = 0; i < va.length; i++) {
								result[i] += va[i];
							}
						}
						// Calculate Average
						for (int i = 0; i < result.length; i += 4) {
							result[i + 2] = (result[i + 1] == 0) ? 0 : result[i] / result[i + 1];
						}
						// Calculate Variance
						for (Vector v : arg0._2()) {
							double[] va = v.toArray();
							for (int i = 0; i < va.length; i += 4) {
								result[i + 3] += (result[i] - result[i + 2]) * (result[i] - result[i + 2]);
							}
						}
						for (int i = 0; i < result.length; i += 4) {
							result[i + 3] = (result[i + 1] == 0) ? 0 : result[i + 3] / result[i + 1];
						}
						return new Tuple2<String, Vector>(arg0._1(), Vectors.dense(result));
					}
				}).sortByKey().mapToPair(new PairFunction<Tuple2<String, Vector>, String, Double[]>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, Double[]> call(Tuple2<String, Vector> arg0) throws Exception {
						return new Tuple2<String, Double[]>(arg0._1(), ArrayUtils.toObject(arg0._2().toArray()));
					}
				}).collectAsMap();

		// 3. Count Enabled Transition
		Map<String, Double[]> arcLogicalTransitionTimes = reader.getCasesRDD()
				.flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Vector>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Iterator<Tuple2<String, Vector>> call(Tuple2<String, ICase> arg0) throws Exception {
						Map<String, List<Double>> result = new HashMap<String, List<Double>>();

						Map<Double, List<IEvent>> sortedEvent = new TreeMap<Double, List<IEvent>>();
						for (IEvent e : arg0._2().getEvents().values()) {
							double timestamp = e.getTimestamp();
							if (!sortedEvent.containsKey(timestamp))
								sortedEvent.put((double) timestamp, new ArrayList<IEvent>());
							sortedEvent.get(timestamp).add(e);
						}

						List<IEvent> pes = null;
						double pt = 0d;
						for (Double t : sortedEvent.keySet()) {
							if (pes == null) {
								pt = t;
								pes = sortedEvent.get(t);
								continue;
							}
							for (IEvent pe : pes) {
								for (IEvent e : sortedEvent.get(t)) {
									String name = pe.getLabel() + "||" + pe.getType() + "||" + e.getLabel() + "||"
											+ e.getType();
									if (!result.containsKey(name))
										result.put(name, new ArrayList<Double>());
									result.get(name).add(t - pt);
									result.get(name).add(1d);
									result.get(name).add(0d); // For
																// Average
									result.get(name).add(0d); // For
																// Variance
								}
							}
							pt = t;
							pes = sortedEvent.get(t);
						}

						List<Tuple2<String, Vector>> startTimes = new ArrayList<Tuple2<String, Vector>>();
						for (String s : result.keySet()) {
							startTimes.add(new Tuple2<String, Vector>(s,
									Vectors.dense(ArrayUtils.toPrimitive(result.get(s).toArray(new Double[0])))));
						}
						return startTimes.iterator();
					}
				}).groupByKey().mapToPair(new PairFunction<Tuple2<String, Iterable<Vector>>, String, Vector>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, Vector> call(Tuple2<String, Iterable<Vector>> arg0) throws Exception {
						int size = 0;
						for (Vector v : arg0._2()) {
							if (v.size() > size)
								size = v.size();
						}
						double[] result = new double[size];
						for (Vector v : arg0._2()) {
							double[] va = v.toArray();
							for (int i = 0; i < va.length; i++) {
								result[i] += va[i];
							}
						}
						// Calculate Average
						for (int i = 0; i < result.length; i += 4) {
							result[i + 2] = (result[i + 1] == 0) ? 0 : result[i] / result[i + 1];
						}
						// Calculate Variance
						for (Vector v : arg0._2()) {
							double[] va = v.toArray();
							for (int i = 0; i < va.length; i += 4) {
								result[i + 3] += (result[i] - result[i + 2]) * (result[i] - result[i + 2]);
							}
						}
						for (int i = 0; i < result.length; i += 4) {
							result[i + 3] = (result[i + 1] == 0) ? 0 : result[i + 3] / result[i + 1];
						}
						return new Tuple2<String, Vector>(arg0._1(), Vectors.dense(result));
					}
				}).sortByKey().mapToPair(new PairFunction<Tuple2<String, Vector>, String, Double[]>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, Double[]> call(Tuple2<String, Vector> arg0) throws Exception {
						return new Tuple2<String, Double[]>(arg0._1(), ArrayUtils.toObject(arg0._2().toArray()));
					}
				}).collectAsMap();

		Map<String, Map<Double, Double[]>> logicalTimeline = new TreeMap<String, Map<Double, Double[]>>();
		double logicalCoverageTime = 0d;
		for (String node : nodeLogicalStartTimes.keySet()) {
			Double[] dd = nodeLogicalStartTimes.get(node);
			Double[] dp = nodeLogicalProcessingTimes.containsKey(node) ? nodeLogicalProcessingTimes.get(node)
					: new Double[0];
			for (int i = 0; i < dd.length; i += 4) {
				double startTime = Math.floor(dd[i + 2]);
				double startTimeVariance = Math.floor(dd[i + 3]);
				double processTime = Math.floor((i + 2 < dp.length) ? dp[i + 2] : 0d);
				double processTimeVariance = Math.floor((i + 3 < dp.length) ? dp[i + 3] : 0d);
				if (!logicalTimeline.containsKey(node))
					logicalTimeline.put(node, new TreeMap<Double, Double[]>());
				logicalTimeline.get(node).put(startTime,
						new Double[] { dd[i + 1], startTime, startTimeVariance, processTime, processTimeVariance });
				double coverageTime = startTime + processTime + processTimeVariance;
				if (coverageTime > logicalCoverageTime)
					logicalCoverageTime = coverageTime;
			}
		}

		IdleTimeNetwork result = new IdleTimeNetwork();
		result.setLogicalStartTime(nodeLogicalStartTimes);
		result.setLogicalProcessingTime(nodeLogicalProcessingTimes);
		result.setLogicalTimeline(logicalTimeline);
		result.setLogicalCoverageTime(logicalCoverageTime);
		result.setLogicalTransitionTime(arcLogicalTransitionTimes);
		result.setCountCoverageTime(logicalTimeline.size());
		return result;
	}

	@Override
	public IJobResult run(String json, IResource res, IExecutor se) {
		try {
			// JavaSparkContext sc = se.getContext();
			// FileSystem fs = se.getHdfsFileSystem();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			IdleTimeNetworkConfiguration config = mapper.readValue(json, IdleTimeNetworkConfiguration.class);
			String outputURI = se.getContextUri(res.getUri());
			SparkRepositoryReader nativeReader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);
			SparkIdleTimeRepositoryReader reader = new SparkIdleTimeRepositoryReader(nativeReader, config);
			RawJobResult jobResult = new RawJobResult("model.IdleTimeNetwork", outputURI, outputURI,
					mapper.writeValueAsString(run(reader)));
			se.getFileUtil().saveAsTextFile(se, outputURI + ".idmodel", jobResult.getResponse());
			return jobResult;
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
		}
		return null;
	}

}
