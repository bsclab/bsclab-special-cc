package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.PairFunction;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import scala.Tuple2;

public class Algorithm4DependencyFilter implements PairFunction<Tuple2<Integer, Activity>, Integer, Activity> {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private final boolean useAllConnected;
	private final double relativeToBest;
	private final int positiveObservations;
	private final double dependencyThreshold;
	private final double dependencyThresholdL1Loop;
	private final double dependencyThresholdL2Loop;
	private final double dependencyDivisor;

	public Algorithm4DependencyFilter(HeuristicMinerJobConfiguration config) {
		positiveObservations = config.getPositiveObservation();
		dependencyThreshold = config.getThreshold().getDependency();
		dependencyThresholdL1Loop = config.getThreshold().getL1Loop();
		dependencyThresholdL2Loop = config.getThreshold().getL2Loop();
		dependencyDivisor = config.getThreshold().getDependencyDivisor();
		useAllConnected = config.getOption().isAllConnected();
		relativeToBest = config.getThreshold().getRelativeToBest();
	}

	// @Override
	// public Boolean call(Activity arg0) throws Exception {
	// double[] v = arg0.toArray();
	// int type = (int) v[Algorithm3DependencyMiner.INDEX_TYPE];
	// double freq = v[Algorithm3DependencyMiner.INDEX_FREQ];
	// double dep = v[Algorithm3DependencyMiner.INDEX_DEP];
	// boolean result = false;
	// switch (type) {
	// case Algorithm3DependencyMiner.TYPE_DEP:
	// result = freq >= positiveObservations && dep > dependencyThreshold;
	// break;
	// case Algorithm3DependencyMiner.TYPE_L1DEP:
	// result = freq >= positiveObservations && dep > dependencyThresholdL1Loop;
	// break;
	// case Algorithm3DependencyMiner.TYPE_L2DEP:
	// result = freq >= positiveObservations && dep > dependencyThresholdL2Loop;
	// break;
	// }
	// return result;
	// }
	//

	@Override
	public Tuple2<Integer, Activity> call(Tuple2<Integer, Activity> arg0) throws Exception {
		int ai = arg0._1();
		Activity a = arg0._2();
		Activity r = new Activity();
		r.setId(a.getId());
		// Check L1Loop Dependency
		Dependency aL1 = a.getL1();
		if (aL1 != null && aL1.getFrequency() >= positiveObservations
				&& aL1.getDependency() >= dependencyThresholdL1Loop) {
			aL1.setAccepted(true);
			r.setL1(aL1);
		}
		// Check Dependency
		int ia = 0, oa = 0;
		Dependency bestInput = null, bestOutput = null;
		for (Integer i : a.getInput().keySet()) {
			Dependency d = a.getInput().get(i);
			if (bestInput == null || d.getDependency() > bestInput.getDependency())
				bestInput = d;
			if (d.getFrequency() >= positiveObservations && d.getDependency() >= dependencyThreshold) {
				d.setAccepted(true);
				r.getInput().put(i, d);
				ia++;
			}
		}
		for (Integer i : a.getOutput().keySet()) {
			Dependency d = a.getOutput().get(i);
			if (bestOutput == null || d.getDependency() > bestOutput.getDependency())
				bestOutput = d;
			if (d.getFrequency() >= positiveObservations && d.getDependency() >= dependencyThreshold) {
				d.setAccepted(true);
				r.getOutput().put(i, d);
				oa++;
			}
		}
		// Check L2Dependency
		Dependency bestInputL2 = null, bestOutputL2 = null;
		for (Integer i : a.getInputL2().keySet()) {
			Dependency d = a.getInputL2().get(i);
			if (bestInputL2 == null || d.getDependency() > bestInputL2.getDependency())
				bestInputL2 = d;
			if (d.getFrequency() >= positiveObservations && d.getDependency() >= dependencyThresholdL2Loop) {
				d.setAccepted(true);
				r.getInputL2().put(i, d);
				ia++;
			}
		}
		for (Integer i : a.getOutputL2().keySet()) {
			Dependency d = a.getOutputL2().get(i);
			if (bestOutputL2 == null || d.getDependency() > bestOutputL2.getDependency())
				bestOutputL2 = d;
			if (d.getFrequency() >= positiveObservations && d.getDependency() >= dependencyThresholdL2Loop) {
				d.setAccepted(true);
				r.getOutputL2().put(i, d);
				oa++;
			}
		}
		if (ia == 0) {
			if (bestInput != null) {
				bestInput.setAccepted(true);
				r.getInput().put(bestInput.getRelated(), bestInput);
			} else if (bestInputL2 != null) {
				bestInputL2.setAccepted(true);
				r.getInput().put(bestInputL2.getRelated(), bestInputL2);
			}
		}
		if (oa == 0) {
			if (bestOutput != null) {
				bestOutput.setAccepted(true);
				r.getOutput().put(bestOutput.getRelated(), bestOutput);
			} else if (bestOutputL2 != null) {
				bestOutputL2.setAccepted(true);
				r.getOutput().put(bestOutputL2.getRelated(), bestOutputL2);
			}
		}
		return new Tuple2<Integer, Activity>(ai, r);
	}

	public JavaPairRDD<Integer, Activity> mine(JavaPairRDD<Integer, Activity> dependencies) {
		JavaPairRDD<Integer, Activity> result = dependencies.mapToPair(this).cache();
		return result;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public boolean isUseAllConnected() {
		return useAllConnected;
	}

	public double getRelativeToBest() {
		return relativeToBest;
	}

	public int getPositiveObservations() {
		return positiveObservations;
	}

	public double getDependencyThreshold() {
		return dependencyThreshold;
	}

	public double getDependencyThresholdL1Loop() {
		return dependencyThresholdL1Loop;
	}

	public double getDependencyThresholdL2Loop() {
		return dependencyThresholdL2Loop;
	}

	public double getDependencyDivisor() {
		return dependencyDivisor;
	}
}
