/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.subcontracting;

import java.util.List;
import org.apache.spark.api.java.JavaPairRDD;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;

public class Subcontracting_CCIDCM extends SubcontractingAlgorithm {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private final List<String> originatorNameList;
	private final JavaPairRDD<String, ICase> rdd;

	public Subcontracting_CCIDCM(final List<String> originatorNameList, final JavaPairRDD<String, ICase> rdd) {
		this.originatorNameList = originatorNameList;
		this.rdd = rdd;
	}

	// NOT IMPLEMENTED
	@Override
	public SocialMatrix calculate(final double beta, final int depth) {
		// double normalVal = 0.0d;
		// List<Tuple2<String, ICase>> cases = rdd.collect();
		rdd.collect();
		int originatorNum = originatorNameList.size();

		SocialMatrix D = new SocialMatrix(originatorNum, originatorNum, 0, originatorNameList);

		// for(Tuple2<String, ICase> t : cases){
		// ICase icase = t._2;
		// List<IEvent> events = new
		// ArrayList<IEvent>(icase.getEvents().values());
		//
		// if(events.size() < 3) continue;
		//
		// int minK = 0;
		// if(events.size() < depth)
		// minK = events.size();
		// else
		// minK = depth + 1;
		//
		// if(minK < 3) minK = 3;
		//
		// for(int k = 2; k < minK; k++){
		// normalVal += Math.pow(beta, k-2);
		// SocialMatrix m = new SocialMatrix(originatorNum, originatorNum,
		// 0.0d);
		//
		// for(int i=0; i<events.size() - k; i++){
		// try{
		// IEvent ent = events.get(i);
		// IEvent ent3 = events.get(i+k);
		//
		// if(ent.getOriginator() != null && ent3.getOriginator() != null){
		// if(ent.getOriginator().equalsIgnoreCase(ent3.getOriginator())){
		// for(int j= i + 1; j < i + k; j++){
		// IEvent ent2 = events.get(j);
		// if(ent2.getOriginator() == null) continue;
		// int row = D.findIndexBy(ent.getOriginator());
		// int col = D.findIndexBy(ent2.getOriginator());
		// m.set(row, col, 1);
		// }
		// }
		// }
		// }catch(IndexOutOfBoundsException ee){};
		// }
		//
		// for(int i=0; i<originatorNum; i++){
		// for(int j=0; j<originatorNum; j++){
		// D.set(i, j, D.get(i, j) + m.get(i, j) * Math.pow(beta, k - 2));
		// }
		// }
		// }
		// }
		//
		// for(int i=0; i<D.originatorSize(); i++){
		// for(int j=0; j<D.originatorSize(); j++){
		// D.set(i, j, D.get(i, j)/normalVal);
		// }
		// }

		return D;
	}

}
