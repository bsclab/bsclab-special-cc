/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.model.idnm;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.PairFunction;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import scala.Tuple2;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.repository.IRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ISparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IRepository;
import kr.ac.pusan.bsclab.bab.ws.model.BEvent;

public class SparkIdleTimeRepositoryReader implements IRepositoryReader, ISparkRepositoryReader, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JavaPairRDD<String, ICase> casesRDD;
	private Map<String, ICase> cases;
	private ISparkRepositoryReader nativeReader;

	public SparkIdleTimeRepositoryReader(ISparkRepositoryReader nativeReader, Configuration config) {
		try {
			this.nativeReader = nativeReader;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public IRepository getRepository() {
		return nativeReader.getRepository();
	}

	@Override
	public JavaPairRDD<String, ICase> getCasesRDD() {

		if (casesRDD == null) {
			casesRDD = nativeReader.getCasesRDD().mapToPair(new PairFunction<Tuple2<String, ICase>, String, ICase>() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				ObjectMapper mapper;

				@Override
				public Tuple2<String, ICase> call(Tuple2<String, ICase> caseJson) throws Exception {
					if (mapper == null) {
						mapper = new ObjectMapper();
						mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						SimpleModule module = new SimpleModule("EnhancedDatesModule",
								new Version(0, 0, 0, "0", "0", "0"));
						module.addDeserializer(IEvent.class, new JsonDeserializer<IEvent>() {

							@Override
							public IEvent deserialize(JsonParser jp, DeserializationContext dc)
									throws IOException, JsonProcessingException {
								return jp.readValueAs(BEvent.class);
							}
						});
						mapper.registerModule(module);
					}
					// BCase bcase = mapper.readValue(caseJson, BCase.class);
					ICase bcase = caseJson._2();

					if (bcase.getEvents().size() > 0) {
						IEvent[] events = bcase.getEvents().values().toArray(new IEvent[0]);
						Map<Long, List<IEvent>> sortedEvents = new TreeMap<Long, List<IEvent>>();
						for (IEvent e : events) {
							if (!sortedEvents.containsKey(e.getTimestamp()))
								sortedEvents.put(e.getTimestamp(), new ArrayList<IEvent>());
							sortedEvents.get(e.getTimestamp()).add(e);
						}
						Long[] timestamps = sortedEvents.keySet().toArray(new Long[0]);
						Long caseStart = timestamps[0];
						Long caseEnd = timestamps[timestamps.length - 1];
						bcase.getAttributes().put("caseStart", caseStart);
						bcase.getAttributes().put("caseEnd", caseEnd);
						bcase.getEvents().clear();
						for (Long t : sortedEvents.keySet()) {
							for (IEvent e : sortedEvents.get(t)) {
								String eid = String.valueOf(bcase.getEvents().size());
								((BEvent) e).setId(eid);
								// ((BEvent)
								// e).setTimestamp(e.getTimestamp() -
								// caseStart);
								bcase.getEvents().put(eid, e);
							}
						}
						IEvent se = events[0];
						IEvent ee = events[events.length - 1];
						BEvent ase = new BEvent("-1", se.getUri(), "Start", "complete", se.getOriginator(),
								se.getTimestamp(), se.getResource());
						BEvent aee = new BEvent(String.valueOf(events.length), ee.getUri(), "End", "complete",
								ee.getOriginator(), ee.getTimestamp(), ee.getResource());
						bcase.getEvents().clear();
						if (se.getLabel().compareToIgnoreCase("start") != 0) {
							bcase.getEvents().put(ase.getId(), ase);
						}
						for (IEvent e : events) {
							bcase.getEvents().put(e.getId(), e);
						}
						if (ee.getLabel().compareToIgnoreCase("end") != 0) {
							bcase.getEvents().put(aee.getId(), aee);
						}
					}
					return new Tuple2<String, ICase>(bcase.getId(), bcase);
				}

			});
		}
		return casesRDD;
	}

	@Override
	public Map<String, ICase> getCases() {
		if (cases == null) {
			cases = getCasesRDD().collectAsMap();
		}
		return cases;
	}

	public Map<String, ICase> getClusteredCases() {

		System.err.println(getCasesRDD()
				// 1. Map to Pair -> Key: Trace Signature, Value: ICase
				.mapToPair(new PairFunction<Tuple2<String, ICase>, String, ICase>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, ICase> call(Tuple2<String, ICase> arg0) throws Exception {
						Map<Long, Set<String>> traces = new TreeMap<Long, Set<String>>();
						for (IEvent e : arg0._2().getEvents().values()) {
							if (!traces.containsKey(e.getTimestamp()))
								traces.put(e.getTimestamp(), new TreeSet<String>());
							traces.get(e.getTimestamp()).add(e.getLabel() + " (" + e.getType() + ")");
						}
						List<Set<String>> signature = new ArrayList<Set<String>>();
						for (Set<String> s : traces.values())
							signature.add(s);
						return new Tuple2<String, ICase>(DigestUtils.md5Hex(signature.toString()), arg0._2());
					}
				})
				// 2. Group by Key
				.groupByKey().keys().collect());

		return null;
	}

}
