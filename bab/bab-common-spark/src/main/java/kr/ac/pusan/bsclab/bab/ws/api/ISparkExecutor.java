/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api;

import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ISparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.controller.SparkFileUtil;

/**
 * Abstraction for spark executor
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public interface ISparkExecutor extends IExecutor {

	/**
	 * Executor context
	 * 
	 * @return executor context
	 */
	public JavaSparkContext getContext();

	/**
	 * HDFS instance
	 * 
	 * @return HDFS instance
	 */
	public FileSystem getHdfsFileSystem();

	/**
	 * Get HDFS URI (Relative to BAB Home)
	 * 
	 * @param uri
	 *            file/directory URI
	 * @return HDFS URI relative to bab
	 */
	public String getHdfsURI(String uri);

	/**
	 * Get executor configuration
	 * 
	 * @return executor configuration
	 */
	public SparkConf getConfiguration();

	public Class<? extends ISparkRepositoryReader> getReader();

	public SparkFileUtil getSparkFileUtil();
}
