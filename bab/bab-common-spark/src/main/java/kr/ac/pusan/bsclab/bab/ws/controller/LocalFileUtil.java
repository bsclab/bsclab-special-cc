/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;

public class LocalFileUtil extends SparkFileUtil implements Serializable {

	protected static LocalFileUtil instance;

	public static LocalFileUtil getInstance() {
		if (instance == null) {
			instance = new LocalFileUtil();
		}
		return instance;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public boolean isFileExists(IExecutor se, String uri) {
		try {
			File f = new File(uri);
			return f.exists();
		} catch (Exception e) {

		}
		return false;
	}

	@Override
	public void saveAsTextFile(IExecutor se, String uri, String data) {
		try {
			File file = new File(uri);
			File parentDir = file.getParentFile();
			if (parentDir != null && !parentDir.exists()) {
				parentDir.mkdirs();
			}
			FileWriter fw = new FileWriter(uri);
			BufferedWriter out = new BufferedWriter(fw);
			out.write(data);
			out.close();
		} catch (Exception e) {
			//System.out.println(e.getMessage());
		}
	}

	@Override
	public String loadTextFile(IExecutor se, String uri) {
		try {
			FileReader fileReader = new FileReader(uri);
			BufferedReader br = new BufferedReader(fileReader);
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line + System.lineSeparator());
			}
			br.close();
			return sb.toString();
		} catch (Exception e) {

		}
		return null;
	}
}
