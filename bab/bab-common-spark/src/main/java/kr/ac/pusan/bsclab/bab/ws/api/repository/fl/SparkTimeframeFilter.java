package kr.ac.pusan.bsclab.bab.ws.api.repository.fl;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import kr.ac.pusan.bsclab.bab.ws.api.SparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import scala.Tuple2;

public class SparkTimeframeFilter extends TimeFrameFilter<JavaPairRDD<String, ICase>, JavaPairRDD<String, ICase>>
		implements SparkFilter, Function<Tuple2<String, ICase>, Boolean> {
	private static final long serialVersionUID = 1L;

	@Override
	public JavaPairRDD<String, ICase> apply(JavaPairRDD<String, ICase> i) {
		JavaPairRDD<String, ICase> result = i;
		if (getRule().equalsIgnoreCase(KEEP_CASES_TRIM)) {
			result = i.mapToPair(new SparkTimeframeFilterTrimCases(this));
		} else {
			result = i.filter(this);
		}
		return result;
	}

	@Override
	public Boolean call(Tuple2<String, ICase> bcase) throws Exception {
		long caseStart = Long.MAX_VALUE;
		long caseEnd = Long.MIN_VALUE;
		boolean result = false;
		for (IEvent e : bcase._2().getEvents().values()) {
			if (e.getTimestamp() < caseStart) {
				caseStart = e.getTimestamp();
			}
			if (e.getTimestamp() > caseEnd) {
				caseEnd = e.getTimestamp();
			}
		}
		if (getRule().equalsIgnoreCase(KEEP_CASES_CONTAINED) && caseStart >= getMin() && caseEnd <= getMax()) {
			result = true;
		} else if (getRule().equalsIgnoreCase(KEEP_CASES_INTERSECT) && (caseStart >= getMin() || caseEnd <= getMax())) {
			result = true;
		} else if (getRule().equalsIgnoreCase(KEEP_CASES_STARTED) && caseStart >= getMin()) {
			result = true;
		} else if (getRule().equalsIgnoreCase(KEEP_CASES_COMPLETED) && caseEnd <= getMax()) {
			result = true;
		}
		return result;
	}

	@Override
	public JavaPairRDD<String, ICase> applyFilter(JavaPairRDD<String, ICase> casesRDD) {
		return apply(casesRDD);
	}

}
