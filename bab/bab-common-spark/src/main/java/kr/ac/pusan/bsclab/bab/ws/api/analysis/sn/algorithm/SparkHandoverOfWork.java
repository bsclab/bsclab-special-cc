/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.HandOverAlgorithm;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_CCCDCM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_CCCDIM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_CCIDCM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_CCIDIM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_ICCDCM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_ICCDIM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_ICIDCM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover.Handover_ICIDIM;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.config.SocialNetworkAnalysisConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.Arc;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.Node;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.response.SocialNetworkAnalysis;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import org.apache.spark.api.java.JavaPairRDD;

public class SparkHandoverOfWork extends SparkSocialNetworkAlgorithm {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public SocialNetworkAnalysis run(SocialNetworkAnalysisConfiguration config, SocialNetworkAnalysis analysis,
			IExecutor se) {

		// JavaSparkContext sc = ((ISparkExecutor) se).getContext();

		SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);
		final boolean CONSIDER_CAUSALITY = config.getMethod().isCasuality();
		final boolean CONSIDER_MULTIPLE_TRANSFER = config.getMethod().isMultipleTransfer();
		final double beta = config.getMethod().getDirectSubcontract().getBeta();
		final int depth = config.getMethod().getDirectSubcontract().getDepth();
		final boolean CONSIDER_DIRECT_SUBCONTRACT = (depth < 1) ? true : false;

		/* <Originator Name, Occurrences Count of Originator> */
		Map<String, Double> originatorNameFrequencyPerCaseMap = getMapOfOriginatorNameFrequencyPerCase(reader,
				config.getStates());
		List<String> originatorNameList = new ArrayList<String>(originatorNameFrequencyPerCaseMap.keySet());
		Collections.sort(originatorNameList);

		/* Make Nodes */
		Map<String, Node> nodes = makeNodes(originatorNameList, originatorNameFrequencyPerCaseMap);
		analysis.setNodes(nodes);
		analysis.setOriginators(new ArrayList<String>(reader.getRepository().getOriginators().keySet()));

		/* Ready to run HandoverOfWork Algorithm */
		JavaPairRDD<String, ICase> rdd = reader.getCasesRDD();
		HandOverAlgorithm handOverofWorkAlgorithm = null;

		// CCCDCM
		if (CONSIDER_CAUSALITY && CONSIDER_DIRECT_SUBCONTRACT && CONSIDER_MULTIPLE_TRANSFER) {
			// implementing..
			handOverofWorkAlgorithm = new Handover_CCCDCM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("CCCDCM");
		}
		// CCIDCM
		else if (CONSIDER_CAUSALITY && !CONSIDER_DIRECT_SUBCONTRACT && CONSIDER_MULTIPLE_TRANSFER) {
			// implementing..
			handOverofWorkAlgorithm = new Handover_CCIDCM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("CCIDCM");
		}
		// CCCDIM
		else if (CONSIDER_CAUSALITY && CONSIDER_DIRECT_SUBCONTRACT && !CONSIDER_MULTIPLE_TRANSFER) {
			// implementing..
			handOverofWorkAlgorithm = new Handover_CCCDIM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("CCCDIM");
		}
		// CCIDIM
		else if (CONSIDER_CAUSALITY && !CONSIDER_DIRECT_SUBCONTRACT && !CONSIDER_MULTIPLE_TRANSFER) {
			// implementing..
			handOverofWorkAlgorithm = new Handover_CCIDIM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("CCIDIM");
		}
		// ICCDCM
		else if (!CONSIDER_CAUSALITY && CONSIDER_DIRECT_SUBCONTRACT && CONSIDER_MULTIPLE_TRANSFER) {
			handOverofWorkAlgorithm = new Handover_ICCDCM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("ICCDCM");
		}
		// ICIDCM
		else if (!CONSIDER_CAUSALITY && !CONSIDER_DIRECT_SUBCONTRACT && CONSIDER_MULTIPLE_TRANSFER) {
			handOverofWorkAlgorithm = new Handover_ICIDCM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("ICIDCM");
		}
		// ICCDIM
		else if (!CONSIDER_CAUSALITY && CONSIDER_DIRECT_SUBCONTRACT && !CONSIDER_MULTIPLE_TRANSFER) {
			handOverofWorkAlgorithm = new Handover_ICCDIM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("ICCDIM");
		}
		// ICIDIM
		else if (!CONSIDER_CAUSALITY && !CONSIDER_DIRECT_SUBCONTRACT && !CONSIDER_MULTIPLE_TRANSFER) {
			handOverofWorkAlgorithm = new Handover_ICIDIM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("ICIDIM");
		}

		if (handOverofWorkAlgorithm == null) {
			handOverofWorkAlgorithm = new Handover_ICIDIM(originatorNameList, rdd);
			SparkSocialNetworkAlgorithm.printDebug("NULL -> ICIDIM");
			System.err.print("Causality : " + CONSIDER_CAUSALITY + ", Direct : " + CONSIDER_DIRECT_SUBCONTRACT
					+ ", Multiple : " + CONSIDER_MULTIPLE_TRANSFER);
		}
		SocialMatrix D = handOverofWorkAlgorithm.calculate(beta, depth);

		Map<String, Arc> arcs = makeArcs(D);
		analysis.setArcs(arcs);

		return analysis;
	}

}
