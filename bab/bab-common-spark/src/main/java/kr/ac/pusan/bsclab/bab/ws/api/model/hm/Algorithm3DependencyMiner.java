package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.mllib.linalg.Vector;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import scala.Tuple2;

public class Algorithm3DependencyMiner implements PairFlatMapFunction<Tuple2<String, Vector>, Integer, Activity>,
		Function2<Activity, Activity, Activity> {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	public static final int TYPE_DEP = 0;
	public static final int TYPE_L1DEP = 1;
	public static final int TYPE_L2DEP = 2;

	public static final int INDEX_A = 0;
	public static final int INDEX_B = 1;
	public static final int INDEX_TYPE = 2;
	public static final int INDEX_FREQ = 3;
	public static final int INDEX_DEP = 4;
	public static final int INDEX_FLAG = 5;

	private final int dependencyDivisor;

	public Algorithm3DependencyMiner(HeuristicMinerJobConfiguration config) {
		dependencyDivisor = config.getThreshold().getDependencyDivisor();
	}

	public double calculateDependency(double ab, double ba) {
		return (ab - ba) / (ab + ba + dependencyDivisor);
	}

	public double calculateL1Dependency(double count) {
		return count / (count + dependencyDivisor);
	}

	public double calculateL2Dependency(double ab, double ba) {
		return (ab + ba) / (ab + ba + dependencyDivisor);
	}

	@Override
	public Iterator<Tuple2<Integer, Activity>> call(Tuple2<String, Vector> arg0) throws Exception {
		String ab = arg0._1();
		double[] v = arg0._2().toArray();
		// double[] r = null; // a, b, type, freq, dependency
		boolean isRelation = ab.contains(Algorithm1ActivityMapping.STRING_DIVIDER);
		List<Tuple2<Integer, Activity>> result = new ArrayList<Tuple2<Integer, Activity>>();
		if (v == null)
			return result.iterator();
		Activity ao = null;
		if (isRelation) {
			String[] abs = ab.split("\\" + Algorithm1ActivityMapping.STRING_DIVIDER);
			int a = Integer.valueOf(abs[0]);
			int b = Integer.valueOf(abs[1]);
			double abCount = v[Algorithm2FrequencyMiner.RFINDEX_AB];
			double baCount = v[Algorithm2FrequencyMiner.RFINDEX_BA];
			double abDep = calculateDependency(abCount, baCount);
			double baDep = calculateDependency(baCount, abCount);
			ao = new Activity();
			ao.setId(a);
			if (baCount > 0)
				ao.getInput().put(b, new Dependency(b, baCount, baDep, false));
			if (abCount > 0)
				ao.getOutput().put(b, new Dependency(b, abCount, abDep, false));
			result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
			ao = new Activity();
			ao.setId(b);
			if (baCount > 0)
				ao.getOutput().put(a, new Dependency(a, baCount, baDep, false));
			if (abCount > 0)
				ao.getInput().put(a, new Dependency(a, abCount, abDep, false));
			result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));

			double abL2Count = v[Algorithm2FrequencyMiner.RFINDEX_ABL2];
			double baL2Count = v[Algorithm2FrequencyMiner.RFINDEX_BAL2];
			double abL2Dep = calculateL2Dependency(abL2Count, baL2Count);
			double baL2Dep = calculateL2Dependency(baL2Count, abL2Count);
			ao = new Activity();
			ao.setId(a);
			if (baL2Count > 0)
				ao.getInputL2().put(b, new Dependency(b, baL2Count, baL2Dep, false));
			if (abL2Count > 0)
				ao.getOutputL2().put(b, new Dependency(b, abL2Count, abL2Dep, false));
			result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
			ao = new Activity();
			ao.setId(b);
			if (baL2Count > 0)
				ao.getOutputL2().put(a, new Dependency(a, baL2Count, baL2Dep, false));
			if (abL2Count > 0)
				ao.getInputL2().put(a, new Dependency(a, abL2Count, abL2Dep, false));
			result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
		} else {
			int a = Integer.parseInt(ab);
			double aL1Count = v[Algorithm2FrequencyMiner.AFINDEX_L1];
			double aL1Dep = calculateL1Dependency(aL1Count);
			ao = new Activity();
			ao.setId(a);
			ao.setL1(new Dependency(a, aL1Count, aL1Dep, false));
			result.add(new Tuple2<Integer, Activity>(ao.getId(), ao));
		}
		return result.iterator();
	}

	@Override
	public Activity call(Activity arg0, Activity arg1) throws Exception {
		Activity r = new Activity();
		if (arg0 == null && arg1 != null) {
			arg0 = arg1;
			arg1 = null;
		}
		r.setId(arg0.getId());
		if (arg0.getL1() != null)
			r.setL1(arg0.getL1());
		r.getInput().putAll(arg0.getInput());
		r.getOutput().putAll(arg0.getOutput());
		r.getInputL2().putAll(arg0.getInputL2());
		r.getOutputL2().putAll(arg0.getOutputL2());
		if (arg1 != null) {
			if (arg1.getL1() != null)
				r.setL1(arg1.getL1());
			r.getInput().putAll(arg1.getInput());
			r.getOutput().putAll(arg1.getOutput());
			r.getInputL2().putAll(arg1.getInputL2());
			r.getOutputL2().putAll(arg1.getOutputL2());
		}
		return r;
	}

	public JavaPairRDD<Integer, Activity> mine(JavaPairRDD<String, Vector> frequencySet) {
		return frequencySet.flatMapToPair(this).reduceByKey(this).cache();
	}
}
