/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.TreeMap;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.ac.pusan.bsclab.bab.v2.core.services.IServiceExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.AbstractJob;
import kr.ac.pusan.bsclab.bab.ws.api.JobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.SparkDbExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.SparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.SparkLocalExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.SparkFpGrowthArMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.SparkDottedChartAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dl.SparkDeltaAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SparkSocialNetworkAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.SparkTimeGapAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.SparkTaskMatrixAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.fm.SparkFuzzyMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicBpmnMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob2;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.SparkLogReplayJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryViewDatasetJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryViewJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.SparkHNetToBPMNMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.fl.SparkFilteringJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.csv.brepo.SparkCsvImportJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ls.SparkLogSummaryJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.map.SparkIntermediateMappingJob;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.model.RawResource;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.SparkPerformanceChartAnalysisJob;

/**
 * 
 * BAB main class, this class will be included in bab.jar file as an entry point
 * class
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 * @since v2.0
 *
 */
public class BabBootstrap implements IServiceExecutor {
	private static String driverUri = null;
	protected final Map<String, Class<?>> jobClasses = new TreeMap<String, Class<?>>();
	
	/**
	 * BAB entry point, it will run as spark driver program or run as standalone
	 * program (not recommended except for debugging)
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		new BabBootstrap().run(args);
	}
	
	public Map<String, Class<?>> getJobClasses() {
		jobClasses.clear();
		jobClasses.put("RepositoryCsvImportJob", SparkCsvImportJob.class);
		jobClasses.put("RepositoryMappingJob", SparkIntermediateMappingJob.class);
		jobClasses.put("RepositoryFilteringJob", SparkFilteringJob.class);
		jobClasses.put("RepositoryViewJob", SparkRepositoryViewJob.class);
		jobClasses.put("RepositoryViewDatasetJob", SparkRepositoryViewDatasetJob.class);
		jobClasses.put("RepositoryLogSummaryJob", SparkLogSummaryJob.class);

		jobClasses.put("ModelHeuristicJob", SparkHeuristicMinerJob.class);
		jobClasses.put("ModelHeuristicBpmnJob", SparkHeuristicBpmnMinerJob.class);
		jobClasses.put("ModelFuzzyJob", SparkFuzzyMinerJob.class);
		jobClasses.put("ModelLogReplayJob", SparkLogReplayJob.class);
		jobClasses.put("AnalysisSocialNetworkJob", SparkSocialNetworkAnalysisJob.class);

		jobClasses.put("ModelHeuristicJob2", SparkHeuristicMinerJob2.class);
		jobClasses.put("ModelHNetToBPMNConvertJob", SparkHNetToBPMNMinerJob.class);

		jobClasses.put("AnalysisAssociationRuleJob", SparkFpGrowthArMinerJob.class);
		jobClasses.put("AnalysisTaskMatrixJob", SparkTaskMatrixAnalysisJob.class);
		jobClasses.put("AnalysisDottedChartJob", SparkDottedChartAnalysisJob.class);
		jobClasses.put("AnalysisPerformanceChartJob", SparkPerformanceChartAnalysisJob.class);

		jobClasses.put("AnalysisTimeGapJob", SparkTimeGapAnalysisJob.class);
		jobClasses.put("AnalysisDeltaJob", SparkDeltaAnalysisJob.class);
		return jobClasses;
	}

	@Override
	public String run(String[] args) throws Exception {
		return runImpl(args);
	}

	public String runImpl(String[] args) throws Exception {
		String response = null;
		long startup = System.currentTimeMillis();
		try {
			SparkExecutor.log(this).debug("STARTUP @ " + startup);

			ObjectMapper mapper = new ObjectMapper();
			//System.out.println(mapper.writeValueAsString(args));


			/*
			 * / disabled
			 * 
			 * 
			 * 
			 * 
			 * jobClasses.put("AnalysisKpiJob", SparkKpiAnalysis.class);
			 * jobClasses.put("RepositoryMxmlImportJob",
			 * SparkMxmlImportJob.class);
			 * jobClasses.put("RepositoryMxmlGzImportJob",
			 * SparkMxmlGzImportJob.class);
			 * 
			 * jobClasses.put("ModelIdleTimeNetworkMinerJob",
			 * SparkIdleTimeNetworkMiner.class);
			 * 
			 * jobClasses.put("YmKPIJob", SparkYmkpiJob.class); //
			 */

			// jobClasses.put("AnalysisKMeansClusteringJob",
			// KMeansClusteringJob.class);

			if (args.length < 5)
				throw new Exception("Args Invalid");
			driverUri = args[0];
			String hdfs = args[1];
			String spark = args[2];
			String jobClass = args[3];
			String jobPath = args[4];
			String userId = args[5];
			String runMode = args.length > 6 ? args[6] : "cluster";
			//System.out.println("Available Jobs: " + mapper.writeValueAsString(getJobClasses().keySet().toArray(new String[0])));
			if (!jobClass.equalsIgnoreCase("BabBatchJob") && !getJobClasses().containsKey(jobClass))
				throw new Exception("Job Class Invalid");

			SparkExecutor se = null;

			if (runMode.equalsIgnoreCase("cluster")) {
				se = new SparkExecutor();
			} else if (runMode.equalsIgnoreCase("db")) {
				se = new SparkDbExecutor();
			} else {
				se = new SparkLocalExecutor();
			}
			se.setHdfsURI(hdfs);
			se.startup(spark);

			// addJar(hdfs + "/bab/lib/flexjson-2.1.jar");
			// se.getContext().addJar(hdfs + "/bab/lib/bab-asm-ntreef-iq.jar");

			// */ Test Connection

			String requestPayload = se.getFileUtil().loadTextFile(se, se.getHdfsURI(jobPath));
			SparkExecutor.log(this).debug("MAIN CONFIGURATION: " + se.getHdfsURI(jobPath));
			SparkExecutor.log(this).debug("MAIN CONFIGURATION: " + requestPayload);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			JobConfiguration request = mapper.readValue(requestPayload, JobConfiguration.class);
			se.setAppName("BABv2 (" + userId + "): " + request.getJobId());
			SparkExecutor.setDebugId(request.getJobId());
			// se.setExecutorCore(1);
			// se.setExecutorMemory(24576);
			// se.setDriverMemory(24576);
			se.setExecutorMemory(2);
			se.setDriverMemory(2);
			// JavaSparkContext jsc = se.getContext();
			if (!jobClass.equalsIgnoreCase("BabBatchJob") && request.getJobs() == null
					|| request.getJobs().size() < 1) {
				request.getJobs().add(new String[] { jobClass, jobPath });
			}
			boolean isError = false;
			StringBuilder errors = new StringBuilder();
			int count = 0;
			for (String[] params : request.getJobs()) {
				try {
					jobClass = params[0];
					jobPath = params[1];
					count++;
					SparkExecutor.log(this).debug("LOADING JOB(" + count + "): " + jobClass + " " + jobPath);
					requestPayload = se.getFileUtil().loadTextFile(se, se.getHdfsURI(jobPath));
					request = mapper.readValue(requestPayload, JobConfiguration.class);
					SparkExecutor.log(this).debug("STARTING WITH CONFIGURATION: " + requestPayload);
					AbstractJob job = (AbstractJob) getJobClasses().get(jobClass).getConstructors()[0]
							.newInstance(new Object[0]);
					IJobResult result = job.run(request.getConfiguration(),
							new RawResource("resource.raw.Mxml", null, request.getPath()), se);
					response += result.getResponse();
					if (result == null || result.getResponse() == null) {
						SparkExecutor.log(this).debug("RESULT: FAILED");
						throw new Exception("NULL / Empty Result");
					}
					SparkExecutor.log(this).debug("RESULT: " + result.getResponse().length() + " Bytes");
				} catch (Exception e) {
					e.printStackTrace();
					errors.append(e.getMessage());
					isError = true;
				}
			}
			if (isError) {
				return null;
			}
			se.shutdown();
			long shutdown = System.currentTimeMillis();
			SparkExecutor.log(this).debug("SHUTDOWN @ " + shutdown);
			SparkExecutor.log(this).debug("DURATION @ " + (shutdown - startup) + " ms");
			report(driverUri);

		} catch (Exception e) {
			SparkExecutor.log(this).debug("ERROR: " + e.getMessage());
			long shutdown = System.currentTimeMillis();
			SparkExecutor.log(this).debug("SHUTDOWN @ " + shutdown);
			SparkExecutor.log(this).debug("DURATION @ " + (shutdown - startup) + " ms");
			report(driverUri + "/FAILED");
			return null;
		}
		return response;
	}

	/**
	 * Helper method for report progress remotely from spark master to BAB web
	 * service
	 * 
	 * @param driverUri
	 */
	public static void report(String driverUri) {
		try {
			URL url = new URL(driverUri);
			URLConnection connection = url.openConnection();
			InputStream in = connection.getInputStream();
			in.close();
		} catch (Exception e) {
			//System.out.println("ERROR:  " + e.getMessage());
			e.printStackTrace();
		}
	}

}
