//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn;

import java.io.*;
import java.util.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.HeuristicModel;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob2;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.config.HeuristicMinerJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.Arc;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.EndEvent;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.ExclusiveGateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Gateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.InclusiveGateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Model;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.ParallelGateway;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.SequenceFlow;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.StartEvent;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.model.Task;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Heuristic network to BPMN network <br>
 * <br>
 * Config class: {@link }<br>
 * Result class: {@link }
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkHNetToBPMNMinerJob extends HNetToBPMNMinerJob {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	@BabService(name = "ModelHNetToBPMNConvertJob", title = "HNet to BPMN Conversion Job", requestClass = HeuristicMinerJobConfiguration.class, responseClass = TextResult.class, legacyJobExtension = ".xbpmn")
	public IJobResult run(String json, IResource res, IExecutor se) {
		try {
			String outputURI = se.getContextUri(res.getUri());
			SparkHeuristicMinerJob2 hmjob = new SparkHeuristicMinerJob2();
			hmjob.run(json, res, se);
			HeuristicModel hm = hmjob.getModel();
			if (hm == null)
				return null;

			// Model bpmn = new Model();
			// bpmn.getProcess().setId(res.getURI());
			// bpmn.getProcess().setName(res.getURI());
			// bpmn.getProcess().getElements().add(new Task("task-A", "Task
			// A"));
			// bpmn.getProcess().getElements().add(new Task("task-B", "Task
			// B"));
			// bpmn.getProcess().getElements().add(new Task("task-C", "Task
			// C"));
			// bpmn.getProcess().getElements().add(new Task("task-D", "Task
			// D"));
			// bpmn.getProcess().getElements().add(new
			// StartEvent("startEvent-S", "Event S"));
			// bpmn.getProcess().getElements().add(new EndEvent("endEvent-E",
			// "Event E"));
			// bpmn.getProcess().getElements().add(new
			// ParallelGateway("gateway-1", "",
			// Gateway.DIRECTION_DIVERGING, new String[] {
			// "sequenceFlow-2"
			// }, new String[] {
			// "sequenceFlow-3", "sequenceFlow-4"
			// }));
			// bpmn.getProcess().getElements().add(new
			// ParallelGateway("gateway-2", "",
			// Gateway.DIRECTION_CONVERGING, new String[] {
			// "sequenceFlow-5", "sequenceFlow-6"
			// }, new String[] {
			// "sequenceFlow-7"
			// }));
			// bpmn.getProcess().getElements().add(new
			// SequenceFlow("sequenceFlow-1", "", "startEvent-S",
			// "task-A"));
			// bpmn.getProcess().getElements().add(new
			// SequenceFlow("sequenceFlow-2", "", "task-A",
			// "gateway-1"));
			// bpmn.getProcess().getElements().add(new
			// SequenceFlow("sequenceFlow-3", "", "gateway-1",
			// "task-B"));
			// bpmn.getProcess().getElements().add(new
			// SequenceFlow("sequenceFlow-4", "", "gateway-1",
			// "task-C"));
			// bpmn.getProcess().getElements().add(new
			// SequenceFlow("sequenceFlow-5", "", "task-B",
			// "gateway-2"));
			// bpmn.getProcess().getElements().add(new
			// SequenceFlow("sequenceFlow-6", "", "task-B",
			// "gateway-2"));
			// bpmn.getProcess().getElements().add(new
			// SequenceFlow("sequenceFlow-7", "", "gateway-2",
			// "task-D"));
			// bpmn.getProcess().getElements().add(new
			// SequenceFlow("sequenceFlow-8", "", "task-D",
			// "endEvent-E"));
			//

			Model bpmn = new Model();
			Map<String, Integer> freqs = new LinkedHashMap<String, Integer>();

			// 1. Turn node into task
			Map<String, Task> nmap = new LinkedHashMap<String, Task>();
			int n = 1;
			for (String node : hm.getNodes().keySet()) {
				String id = "node-" + n;
				Task t = new Task(id, node);
				bpmn.getProcess().getElements().add(t);
				nmap.put(node, t);
				int f = hm.getNodes().get(node).getFrequency().getAbsolute();
				freqs.put(id, f);
				n++;
			}
			// 2. Turn arc into sequence flow
			Map<String, SequenceFlow> smap = new LinkedHashMap<String, SequenceFlow>();
			int sq = 1;
			for (String arc : hm.getArcs().keySet()) {
				Arc a = hm.getArcs().get(arc);
				String id = "sequenceFlow-" + sq;
				Task source = nmap.get(a.getSource());
				Task target = nmap.get(a.getTarget());
				SequenceFlow flow = new SequenceFlow(id, "", source.getId(), target.getId());
				smap.put(id, flow);
				source.getOutgoing().add(id);
				target.getIncoming().add(id);
				int f = a.getFrequency().getAbsolute();
				freqs.put(id, f);
				sq++;
			}
			// 3. Create gateway
			int gwi = 1;
			for (Task t : nmap.values()) {
				String gid = "gateway-" + gwi;
				String sid = "sequenceFlow-" + sq;
				// Split
				if (t.getIncoming().size() == 1 && t.getOutgoing().size() > 1) {
					int nfreq = freqs.get(t.getId());
					List<Integer> afreqs = new ArrayList<Integer>();
					for (String aid : t.getOutgoing()) {
						afreqs.add(freqs.get(aid));
					}
					Gateway g = getGateway(gid, Gateway.DIRECTION_DIVERGING, nfreq, afreqs);
					for (String aid : t.getOutgoing()) {
						SequenceFlow iarc = smap.get(aid);
						iarc.setSourceRef(gid);
						g.getOutgoing().add(aid);
					}
					t.getOutgoing().clear();
					bpmn.getProcess().getElements().add(g);
					SequenceFlow nflow = new SequenceFlow(sid, "", t.getId(), gid);
					smap.put(sid, nflow);
					t.getOutgoing().add(sid);
					g.getIncoming().add(sid);
					sq++;
					gwi++;
					// Join
				} else if (t.getOutgoing().size() == 1 && t.getIncoming().size() > 1) {
					int nfreq = freqs.get(t.getId());
					List<Integer> afreqs = new ArrayList<Integer>();
					for (String aid : t.getIncoming()) {
						afreqs.add(freqs.get(aid));
					}
					Gateway g = getGateway(gid, Gateway.DIRECTION_CONVERGING, nfreq, afreqs);
					for (String aid : t.getIncoming()) {
						SequenceFlow iarc = smap.get(aid);
						iarc.setTargetRef(gid);
						g.getIncoming().add(aid);
					}
					t.getIncoming().clear();
					bpmn.getProcess().getElements().add(g);
					SequenceFlow nflow = new SequenceFlow(sid, "", gid, t.getId());
					smap.put(sid, nflow);
					t.getIncoming().add(sid);
					g.getOutgoing().add(sid);
					sq++;
					gwi++;
				}
			}
			// 4. Assign start (node with no incoming) / end event (node with no
			// outgoing)
			int sei = 1;
			int eei = 1;
			for (Task t : nmap.values()) {
				if (t.getIncoming().size() == 0) {
					StartEvent sev = new StartEvent("startEvent-" + sei, (sei == 1 ? "Start" : "Start " + sei));
					String id = "sequenceFlow-" + sq;
					bpmn.getProcess().getElements().add(sev);
					smap.put(id, new SequenceFlow(id, "", sev.getId(), t.getId()));
					t.getIncoming().add(id);
					sei++;
					sq++;
				}
				if (t.getOutgoing().size() == 0) {
					EndEvent eev = new EndEvent("endEvent-" + eei, (eei == 1 ? "End" : "End " + sei));
					String id = "sequenceFlow-" + sq;
					bpmn.getProcess().getElements().add(eev);
					smap.put(id, new SequenceFlow(id, "", t.getId(), eev.getId()));
					t.getOutgoing().add(id);
					eei++;
					sq++;
				}
			}
			// Register Flow
			for (SequenceFlow flow : smap.values()) {
				bpmn.getProcess().getElements().add(flow);
			}

			JAXBContext jaxbContext = JAXBContext.newInstance(Model.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter writer = new StringWriter();
			jaxbMarshaller.marshal(bpmn, writer);
			TextResult xml = new TextResult(
					writer.toString().replace("<definitions", "<definitions " + Model.XMLNS_STRING));

			// Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// StringReader reader = new StringReader("<?xml version=\"1.0\"
			// encoding=\"UTF-8\"?><definitions exporter=\"ProM.
			// http://www.promtools.org/prom6\"
			// exporterVersion=\"6.3\"><process
			// id=\"proc_437125793\"><startEvent
			// id=\"node_99597cd2-9d93-467d-b437-849186d24268\" name=\"fictive
			// start\"/><endEvent
			// id=\"node_38b33e70-81db-45d1-b00b-736657d18495\" name=\"fictive
			// end\"/><task
			// id=\"node_30ee667d-197b-4772-ab8b-680ea10c965b\" name=\"Test
			// Repair\"/><task
			// id=\"node_be2ff826-a64f-40c3-ba76-32de4be7dc7b\" name=\"Repair
			// (Complex)\"/><task
			// id=\"node_dbe3043c-604b-4808-a299-ab29f87be43e\" name=\"Archive
			// Repair\"/><task
			// id=\"node_ff93557d-5151-493c-911e-f5428e403508\" name=\"Repair
			// (Simple)\"/><task
			// id=\"node_836e51e5-43ca-4c0e-8ee6-be60df0fdab9\" name=\"Inform
			// User\"/><task
			// id=\"node_158f936b-3691-43fd-864b-f521068126fb\" name=\"Restart
			// Repair\"/><task
			// id=\"node_03d52f9f-654b-42dc-a88c-b4c8c915d0bd\" name=\"Analyze
			// Defect\"/><task
			// id=\"node_8f36adbc-4b2e-4a28-b09e-75b8ceadd427\"
			// name=\"Register\"/><exclusiveGateway
			// id=\"node_6b6f6d45-52f4-4ce5-af78-d93f5b791019\" name=\"XOR3\"
			// gatewayDirection=\"Converging\"><incoming>node_f3caf9b6-ce71-48e9-b0eb-61b9da2778da</incoming><incoming>node_bb683393-3d43-45a8-a542-2c2817dc8a2b</incoming><outgoing>node_31df3f01-5c8e-4057-a052-ce68bb628218</outgoing></exclusiveGateway><exclusiveGateway
			// id=\"node_f8baa85f-4305-49a0-ae08-7a037d3993de\" name=\"XOR8\"
			// gatewayDirection=\"Diverging\"><incoming>node_04befa34-369d-4897-b639-10e9cb559335</incoming><outgoing>node_c5e4d49b-0971-4359-b986-139df49dc6b2</outgoing><outgoing>node_9f5aedc4-8765-4c65-a02d-8dfd01fb8509</outgoing></exclusiveGateway><exclusiveGateway
			// id=\"node_65bbdb0c-58b7-4e91-8845-bea501ed195d\" name=\"XOR7\"
			// gatewayDirection=\"Diverging\"><incoming>node_1767d167-ae92-4fee-8895-132f4f28ad25</incoming><outgoing>node_5b6414b6-0157-4cf1-bf0b-9ade026ee7d4</outgoing><outgoing>node_bc29bf13-b5ee-454a-b75e-b801358f4e23</outgoing></exclusiveGateway><exclusiveGateway
			// id=\"node_2e74b677-ad4b-46c9-97f5-2299ce1858ba\" name=\"XOR4\"
			// gatewayDirection=\"Converging\"><incoming>node_6b008279-cd02-4cb9-8909-21061ed9721d</incoming><incoming>node_c5e4d49b-0971-4359-b986-139df49dc6b2</incoming><outgoing>node_15d522e1-44eb-4bb0-9cf7-c5fceac38a46</outgoing></exclusiveGateway><exclusiveGateway
			// id=\"node_873ba165-cb80-47cf-a64e-99542c26a40d\" name=\"XOR6\"
			// gatewayDirection=\"Diverging\"><incoming>node_5413d3f8-9a30-44b8-8ff1-c62a9bc2f88a</incoming><outgoing>node_6b008279-cd02-4cb9-8909-21061ed9721d</outgoing><outgoing>node_3edc62b5-8cfb-4503-918b-47234fb03c2d</outgoing></exclusiveGateway><exclusiveGateway
			// id=\"node_06c5af20-d38b-4341-8078-a9ea08c65534\" name=\"XOR2\"
			// gatewayDirection=\"Converging\"><incoming>node_9f5aedc4-8765-4c65-a02d-8dfd01fb8509</incoming><incoming>node_3edc62b5-8cfb-4503-918b-47234fb03c2d</incoming><outgoing>node_7875a7af-f479-4646-8b6d-70129e63dc8a</outgoing></exclusiveGateway><parallelGateway
			// id=\"node_0fc9fd2c-d249-4cb6-8888-0ad03d5cbc01\" name=\"AND2\"
			// gatewayDirection=\"Diverging\"><incoming>node_cd1739a7-a6d9-4523-9401-9c0ed4895dfd</incoming><outgoing>node_5413d3f8-9a30-44b8-8ff1-c62a9bc2f88a</outgoing><outgoing>node_2862535a-32eb-41f3-87bb-07ae4e05ee93</outgoing></parallelGateway><parallelGateway
			// id=\"node_85a4798e-f0ce-4c4e-a289-837c449819ed\" name=\"AND1\"
			// gatewayDirection=\"Converging\"><incoming>node_5b6414b6-0157-4cf1-bf0b-9ade026ee7d4</incoming><incoming>node_3c706e77-980a-40a3-aaa5-78e3eab9c806</incoming><outgoing>node_b43bf4f5-3dbc-463f-8b2b-9df103d66a3d</outgoing></parallelGateway><sequenceFlow
			// id=\"node_e359597c-72a7-414d-a937-9e0c44442285\" name=\"\"
			// sourceRef=\"node_dbe3043c-604b-4808-a299-ab29f87be43e\"
			// targetRef=\"node_38b33e70-81db-45d1-b00b-736657d18495\"/><sequenceFlow
			// id=\"node_15d522e1-44eb-4bb0-9cf7-c5fceac38a46\" name=\"\"
			// sourceRef=\"node_2e74b677-ad4b-46c9-97f5-2299ce1858ba\"
			// targetRef=\"node_ff93557d-5151-493c-911e-f5428e403508\"/><sequenceFlow
			// id=\"node_5413d3f8-9a30-44b8-8ff1-c62a9bc2f88a\" name=\"\"
			// sourceRef=\"node_0fc9fd2c-d249-4cb6-8888-0ad03d5cbc01\"
			// targetRef=\"node_873ba165-cb80-47cf-a64e-99542c26a40d\"/><sequenceFlow
			// id=\"node_cd1739a7-a6d9-4523-9401-9c0ed4895dfd\" name=\"\"
			// sourceRef=\"node_03d52f9f-654b-42dc-a88c-b4c8c915d0bd\"
			// targetRef=\"node_0fc9fd2c-d249-4cb6-8888-0ad03d5cbc01\"/><sequenceFlow
			// id=\"node_f37eb240-c1bd-4fb4-9b89-87da2314f4b3\" name=\"\"
			// sourceRef=\"node_99597cd2-9d93-467d-b437-849186d24268\"
			// targetRef=\"node_8f36adbc-4b2e-4a28-b09e-75b8ceadd427\"/><sequenceFlow
			// id=\"node_7875a7af-f479-4646-8b6d-70129e63dc8a\" name=\"\"
			// sourceRef=\"node_06c5af20-d38b-4341-8078-a9ea08c65534\"
			// targetRef=\"node_be2ff826-a64f-40c3-ba76-32de4be7dc7b\"/><sequenceFlow
			// id=\"node_3c706e77-980a-40a3-aaa5-78e3eab9c806\" name=\"\"
			// sourceRef=\"node_836e51e5-43ca-4c0e-8ee6-be60df0fdab9\"
			// targetRef=\"node_85a4798e-f0ce-4c4e-a289-837c449819ed\"/><sequenceFlow
			// id=\"node_6b008279-cd02-4cb9-8909-21061ed9721d\" name=\"\"
			// sourceRef=\"node_873ba165-cb80-47cf-a64e-99542c26a40d\"
			// targetRef=\"node_2e74b677-ad4b-46c9-97f5-2299ce1858ba\"/><sequenceFlow
			// id=\"node_188aaa57-198c-4876-8af2-c15d3f8daf00\" name=\"\"
			// sourceRef=\"node_8f36adbc-4b2e-4a28-b09e-75b8ceadd427\"
			// targetRef=\"node_03d52f9f-654b-42dc-a88c-b4c8c915d0bd\"/><sequenceFlow
			// id=\"node_3edc62b5-8cfb-4503-918b-47234fb03c2d\" name=\"\"
			// sourceRef=\"node_873ba165-cb80-47cf-a64e-99542c26a40d\"
			// targetRef=\"node_06c5af20-d38b-4341-8078-a9ea08c65534\"/><sequenceFlow
			// id=\"node_b43bf4f5-3dbc-463f-8b2b-9df103d66a3d\" name=\"\"
			// sourceRef=\"node_85a4798e-f0ce-4c4e-a289-837c449819ed\"
			// targetRef=\"node_dbe3043c-604b-4808-a299-ab29f87be43e\"/><sequenceFlow
			// id=\"node_04befa34-369d-4897-b639-10e9cb559335\" name=\"\"
			// sourceRef=\"node_158f936b-3691-43fd-864b-f521068126fb\"
			// targetRef=\"node_f8baa85f-4305-49a0-ae08-7a037d3993de\"/><sequenceFlow
			// id=\"node_bb683393-3d43-45a8-a542-2c2817dc8a2b\" name=\"\"
			// sourceRef=\"node_be2ff826-a64f-40c3-ba76-32de4be7dc7b\"
			// targetRef=\"node_6b6f6d45-52f4-4ce5-af78-d93f5b791019\"/><sequenceFlow
			// id=\"node_f3caf9b6-ce71-48e9-b0eb-61b9da2778da\" name=\"\"
			// sourceRef=\"node_ff93557d-5151-493c-911e-f5428e403508\"
			// targetRef=\"node_6b6f6d45-52f4-4ce5-af78-d93f5b791019\"/><sequenceFlow
			// id=\"node_c5e4d49b-0971-4359-b986-139df49dc6b2\" name=\"\"
			// sourceRef=\"node_f8baa85f-4305-49a0-ae08-7a037d3993de\"
			// targetRef=\"node_2e74b677-ad4b-46c9-97f5-2299ce1858ba\"/><sequenceFlow
			// id=\"node_1767d167-ae92-4fee-8895-132f4f28ad25\" name=\"\"
			// sourceRef=\"node_30ee667d-197b-4772-ab8b-680ea10c965b\"
			// targetRef=\"node_65bbdb0c-58b7-4e91-8845-bea501ed195d\"/><sequenceFlow
			// id=\"node_bc29bf13-b5ee-454a-b75e-b801358f4e23\" name=\"\"
			// sourceRef=\"node_65bbdb0c-58b7-4e91-8845-bea501ed195d\"
			// targetRef=\"node_158f936b-3691-43fd-864b-f521068126fb\"/><sequenceFlow
			// id=\"node_31df3f01-5c8e-4057-a052-ce68bb628218\" name=\"\"
			// sourceRef=\"node_6b6f6d45-52f4-4ce5-af78-d93f5b791019\"
			// targetRef=\"node_30ee667d-197b-4772-ab8b-680ea10c965b\"/><sequenceFlow
			// id=\"node_5b6414b6-0157-4cf1-bf0b-9ade026ee7d4\" name=\"\"
			// sourceRef=\"node_65bbdb0c-58b7-4e91-8845-bea501ed195d\"
			// targetRef=\"node_85a4798e-f0ce-4c4e-a289-837c449819ed\"/><sequenceFlow
			// id=\"node_9f5aedc4-8765-4c65-a02d-8dfd01fb8509\" name=\"\"
			// sourceRef=\"node_f8baa85f-4305-49a0-ae08-7a037d3993de\"
			// targetRef=\"node_06c5af20-d38b-4341-8078-a9ea08c65534\"/><sequenceFlow
			// id=\"node_2862535a-32eb-41f3-87bb-07ae4e05ee93\" name=\"\"
			// sourceRef=\"node_0fc9fd2c-d249-4cb6-8888-0ad03d5cbc01\"
			// targetRef=\"node_836e51e5-43ca-4c0e-8ee6-be60df0fdab9\"/></process></definitions>");
			// Model mod = (Model) jaxbUnmarshaller.unmarshal(reader);
			//
			// StringWriter writer2 = new StringWriter();
			// jaxbMarshaller.marshal(mod, writer2);
			//

			RawJobResult result = new RawJobResult("export.BPMN", outputURI, outputURI, xml.toString());
			se.getFileUtil().saveAsTextFile(se, outputURI + ".xbpmn", result.getResponse());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Gateway getGateway(String id, String direction, int nodeFreq, List<Integer> arcFreqs) {
		for (Integer arcFreq : arcFreqs) {
			if (arcFreq != nodeFreq) {
				int sumArcFreq = 0;
				for (Integer arcFreq2 : arcFreqs) {
					sumArcFreq += arcFreq2;
				}
				// It was XOR if sum of all arc freq = node freq
				if (sumArcFreq == nodeFreq) {
					return new ExclusiveGateway(id, "", direction);
					// Otherwise it was OR
				} else {
					return new InclusiveGateway(id, "", direction);
				}
			}
		}
		// By default is AND if all arc freq = node freq
		return new ParallelGateway(id, "", direction);
	}
}
