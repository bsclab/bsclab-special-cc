//
/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.repository;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.Configuration;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.repository.AbstractRepositoryJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.ImportJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Generate detailed log summary algorithm <br>
 * <br>
 * Config class: {@link LogSummaryJobConfiguration}<br>
 * Result class: {@link LogSummary}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkRepositoryViewDatasetJob extends AbstractRepositoryJob {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	@BabService(name = "RepositoryViewDatasetJob", title = "Repository Dataset View", requestClass = Configuration.class, responseClass = ImportJobResult.class, legacyJobExtension = ".mrepo")
	public IJobResult run(String json, IResource res, IExecutor se) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			Configuration config = mapper.readValue(json, Configuration.class);
			String outputURI = se.getContextUri(res.getUri());

			String jsonRes = se.getFileUtil().loadTextFile(se, se.getContextUri(config.getRepositoryURI() + ".mrepo"));

			RawJobResult result = new RawJobResult("repository.DatasetView", outputURI, outputURI,
					mapper.writeValueAsString(mapper.readValue(jsonRes, ImportJobResult.class)));
			se.getFileUtil().saveAsTextFile(se, outputURI + ".mrepo", result.getResponse());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
