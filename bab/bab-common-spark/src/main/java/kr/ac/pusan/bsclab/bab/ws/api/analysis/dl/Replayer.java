package kr.ac.pusan.bsclab.bab.ws.api.analysis.dl;

import java.io.Serializable;
import org.apache.spark.api.java.function.DoubleFunction;
import kr.ac.pusan.bsclab.bab.ws.api.model.HmmModel;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import scala.Tuple2;

public class Replayer implements DoubleFunction<Tuple2<String, ICase>>, Serializable {

  /**
   * Default serial version ID
   */
  private static final long serialVersionUID = 1L;
  private final HmmModel model;

  public Replayer(HmmModel model) {
    this.model = model;
  }

  @Override
  public double call(Tuple2<String, ICase> arg0) throws Exception {
    String prev = null;
    double result = 0;
    double i = 0;
    for (IEvent e : arg0._2().getEvents().values()) {
      String act = e.getLabel() + " (" + e.getType() + ")";
      if (prev == null) {
        prev = act;
        continue;
      }
      if (model.hasTransition(prev, act))
        result++;
      prev = act;
      i++;
    }
    return i == 0 ? 0 : result / i;
  }

}
