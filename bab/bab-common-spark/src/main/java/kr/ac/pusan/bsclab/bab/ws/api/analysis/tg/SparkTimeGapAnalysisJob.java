/*
 * 
 * Copyright © 2013-2015 Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.tg;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;

/**
 * Timegap analysis job <br>
 * <br>
 * Config class: {@link TimeGapAnalysisJobConfiguration}<br>
 * Result class: {@link TimeGapAnalysisJobModel}
 * 
 * @author Riska Asriana Sutrisnowati (asriana.riska@gmail.com)
 */
public class SparkTimeGapAnalysisJob extends TimeGapAnalysisJob {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	@BabService(name = "AnalysisTimeGapJob", title = "Time Gap Analysis", requestClass = TimeGapAnalysisJobConfiguration.class, responseClass = TimeGapAnalysisJobModel.class, legacyJobExtension = ".tgans")
	public IJobResult run(String json, IResource res, IExecutor se) {
		try {
			// JavaSparkContext sc = se.getContext();
			// FileSystem fs = se.getHdfsFileSystem();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			TimeGapAnalysisJobConfiguration config = mapper.readValue(json, TimeGapAnalysisJobConfiguration.class);

			ATimeGapAnalysisJob tgJob = null;
			IJobResult result = new RawJobResult("a", "a", "a", "{}");
			if (config.getCaseIds() == null) {
				System.err.println("no parameter");
				tgJob = new SparkTimeGapAnalysisJob1();
				result = tgJob.run(json, res, se);
			} else {
				System.err.println("with parameter");
				tgJob = new SparkTimeGapAnalysisJob2();
				result = tgJob.run(json, res, se);
			}

			// System.out.println(config.getSelected_events());

			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
