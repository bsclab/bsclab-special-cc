/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover;

import java.util.Iterator;
import java.util.List;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.UtilOperation;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import org.apache.spark.api.java.JavaPairRDD;
import scala.Tuple2;

public class Handover_ICCDIM extends HandOverAlgorithm {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private final List<String> originatorNameList;
	private final JavaPairRDD<String, ICase> rdd;

	public Handover_ICCDIM(final List<String> originatorNameList, final JavaPairRDD<String, ICase> rdd) {
		this.originatorNameList = originatorNameList;
		this.rdd = rdd;
	}

	@Override
	public SocialMatrix calculate(double beta, int depth) {
		List<Tuple2<String, ICase>> cases = rdd.collect();
		int originatorNum = originatorNameList.size();

		SocialMatrix D = new SocialMatrix(originatorNum, originatorNum, 0, originatorNameList);

		for (Tuple2<String, ICase> t : cases) {
			ICase icase = t._2();
			Iterator<IEvent> entIter = icase.getEvents().values().iterator();

			IEvent ent, ent2;
			SocialMatrix m = new SocialMatrix(originatorNum, originatorNum, 0);

			if (entIter.hasNext()) {
				ent = entIter.next();
				while (entIter.hasNext()) {
					int row, col;
					ent2 = entIter.next();
					if (ent.getOriginator() != null && ent2.getOriginator() != null) {
						row = D.findIndexBy(ent.getOriginator());
						col = D.findIndexBy(ent2.getOriginator());

						m.set(row, col, 1.0d);
					}
					ent = ent2;
				}
			}
			for (int i = 0; i < originatorNum; i++)
				for (int j = 0; j < originatorNum; j++)
					D.set(i, j, D.get(i, j) + m.get(i, j));
		}

		return UtilOperation.normalize(D, cases.size());
	}
}
