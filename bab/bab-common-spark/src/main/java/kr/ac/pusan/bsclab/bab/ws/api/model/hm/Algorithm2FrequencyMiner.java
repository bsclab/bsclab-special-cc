package kr.ac.pusan.bsclab.bab.ws.api.model.hm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.ArrayUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import scala.Tuple2;

public class Algorithm2FrequencyMiner
		implements PairFlatMapFunction<Tuple2<String, ICase>, String, Vector>, Function2<Vector, Vector, Vector> {
	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	public static final int AFINDEX_START = 0;
	public static final int AFINDEX_OCCUR = 1;
	public static final int AFINDEX_END = 2;
	public static final int AFINDEX_L1 = 3;
	public static final int RFINDEX_AB = 0;
	public static final int RFINDEX_BA = 1;
	public static final int RFINDEX_ABL2 = 2;
	public static final int RFINDEX_BAL2 = 3;

	private final Map<String, Integer> activities;

	public Algorithm2FrequencyMiner(Map<String, Integer> activities) {
		this.activities = activities;
	}

	@Override
	public Iterator<Tuple2<String, Vector>> call(Tuple2<String, ICase> arg0) throws Exception {
		// String traceId = arg0._1();
		ICase trace = arg0._2();
		// Previous activity 2
		int pa2 = -1;
		// Previous activity
		int pa = -1;

		// A => AAsStartFreq,AFreq,AAsEndFreq,AL1LoopFreq
		// A => AL1LDependency
		// A|B => AtoBFreq,BtoAFreq,AtoBL2LoopFreq,BtoAL2LoopFreq
		//

		Map<Integer, Double[]> eF = new LinkedHashMap<Integer, Double[]>();
		Map<String, Double[]> erF = new LinkedHashMap<String, Double[]>();
		Double[] aFreqs = null, rFreqs = null;
		int a = -1;
		for (IEvent e : trace.getEvents().values()) {
			String aStr = e.getLabel() + Algorithm1ActivityMapping.STRING_DIVIDER + e.getType();
			if (!activities.containsKey(aStr))
				continue;
			a = activities.get(aStr);
			if (!eF.containsKey(a))
				eF.put(a, ArrayUtils.toObject(new double[4]));
			aFreqs = eF.get(a);
			aFreqs[AFINDEX_OCCUR]++;
			if (pa == -1) {
				aFreqs[AFINDEX_START]++;
				pa = a;
				String r = activities.get(Algorithm1ActivityMapping.STRING_START_ACTIVITY)
						+ Algorithm1ActivityMapping.STRING_DIVIDER + a;
				if (!erF.containsKey(r))
					erF.put(r, ArrayUtils.toObject(new double[4]));
				rFreqs = erF.get(r);
				rFreqs[RFINDEX_AB]++;
				continue;
			}
			// L1Loop
			if (pa == a) {
				aFreqs[AFINDEX_L1]++;
				continue;
			} else {
				// A => B or B => A
				String r = (pa < a) ? pa + Algorithm1ActivityMapping.STRING_DIVIDER + a
						: a + Algorithm1ActivityMapping.STRING_DIVIDER + pa;
				int dr = (pa < a) ? RFINDEX_AB : RFINDEX_BA;
				if (!erF.containsKey(r))
					erF.put(r, ArrayUtils.toObject(new double[4]));
				rFreqs = erF.get(r);
				rFreqs[dr]++;
			}

			if (pa2 == -1) {
				pa2 = pa;
				pa = a;
				continue;
			}
			// L2Loop
			if (pa2 == a) {
				String r2 = (a < pa) ? a + Algorithm1ActivityMapping.STRING_DIVIDER + pa
						: pa + Algorithm1ActivityMapping.STRING_DIVIDER + a;
				int dr = (a < pa) ? RFINDEX_ABL2 : RFINDEX_BAL2;
				if (!erF.containsKey(r2))
					erF.put(r2, ArrayUtils.toObject(new double[4]));
				rFreqs = erF.get(r2);
				rFreqs[dr]++;
			}
			pa2 = pa;
			pa = a;
		}
		if (aFreqs != null) {
			String r = a + Algorithm1ActivityMapping.STRING_DIVIDER
					+ activities.get(Algorithm1ActivityMapping.STRING_END_ACTIVITY);
			if (!erF.containsKey(r))
				erF.put(r, ArrayUtils.toObject(new double[4]));
			rFreqs = erF.get(r);
			rFreqs[RFINDEX_AB]++;
			aFreqs[AFINDEX_END]++;
		}
		List<Tuple2<String, Vector>> result = new ArrayList<Tuple2<String, Vector>>();
		for (Integer e : eF.keySet()) {
			result.add(new Tuple2<String, Vector>(String.valueOf(e), Vectors.dense(ArrayUtils.toPrimitive(eF.get(e)))));
		}
		for (String r : erF.keySet()) {
			result.add(new Tuple2<String, Vector>(r, Vectors.dense(ArrayUtils.toPrimitive(erF.get(r)))));
		}
		return result.iterator();
	}

	@Override
	public Vector call(Vector arg0, Vector arg1) throws Exception {
		double[] a0 = arg0.toArray();
		double[] a1 = arg1.toArray();
		if (a0 == null && a1 != null)
			return Vectors.dense(a1);
		if (a1 == null)
			return Vectors.dense(a0);
		for (int i = 0; i < a0.length; i++) {
			a0[i] += a1[i];
		}
		return Vectors.dense(a0);
	}

	public JavaPairRDD<String, Vector> mine(JavaPairRDD<String, ICase> cases) {
		JavaPairRDD<String, Vector> result = cases.flatMapToPair(this).reduceByKey(this).cache();
		return result;
	}
}
