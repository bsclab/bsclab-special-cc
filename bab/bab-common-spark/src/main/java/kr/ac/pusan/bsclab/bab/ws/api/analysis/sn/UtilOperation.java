/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn;

import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;

public class UtilOperation {

	public static boolean isInCase(ICase icase, String originator1, String originator2) {
		boolean existOriginator1 = false;
		boolean existOriginator2 = false;
		for (IEvent ievent : icase.getEvents().values()) {
			if (originator1.equalsIgnoreCase(originator2) && ievent.getOriginator().equalsIgnoreCase(originator1)) {
				existOriginator1 = true;
				existOriginator2 = true;
				break;
			} else if (ievent.getOriginator().equalsIgnoreCase(originator1)) {
				existOriginator1 = true;
			} else if (ievent.getOriginator().equalsIgnoreCase(originator2)) {
				existOriginator2 = true;
			}
		}
		return existOriginator1 && existOriginator2;
	}

	public static SocialMatrix normalize(SocialMatrix D, double n) {
		for (int i = 0; i < D.originatorSize(); i++) {
			for (int j = 0; j < D.originatorSize(); j++) {
				D.set(i, j, D.get(i, j) / n);
			}
		}
		return D;
	}

}
