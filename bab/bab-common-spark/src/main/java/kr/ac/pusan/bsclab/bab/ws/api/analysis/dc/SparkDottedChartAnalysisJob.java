/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.dc;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.config.DottedChartAnalysisJobConfiguration;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models.Data;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.models.DottedChartModel;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;
// import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.Tuple2;

/**
 * Dotted chart analysis job <br>
 * <br>
 * Config class: {@link DottedChartAnalysisJobConfiguration}<br>
 * Result class: {@link DottedChartModel}
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkDottedChartAnalysisJob extends DottedChartAnalysisJob implements Serializable {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	@BabService(name = "AnalysisDottedChartJob", title = "Dotted Chart Analysis", requestClass = DottedChartAnalysisJobConfiguration.class, responseClass = DottedChartModel.class, legacyJobExtension = ".dcans")
	public IJobResult run(String json, IResource res, IExecutor se) {
		try {
			// JavaSparkContext sc = se.getContext();
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String outputURI = se.getContextUri(res.getUri());

			final DottedChartAnalysisJobConfiguration config = mapper.readValue(json,
					DottedChartAnalysisJobConfiguration.class);

			SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);

			DottedChartModel model = new DottedChartModel();

			List<Dot> dots = reader.getCasesRDD().flatMap(new FlatMapFunction<Tuple2<String, ICase>, Dot>() {

				/**
				 * Default serial version ID
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public Iterator<Dot> call(Tuple2<String, ICase> arg0) throws Exception {
					SimpleDateFormat f = new SimpleDateFormat(config.getTimeFormat());
					ICase c = arg0._2();
					List<Dot> dots = new ArrayList<Dot>();
					Long firstEvent = null;
					for (IEvent e : c.getEvents().values()) {
						long etime = f.parse(f.format(new Date(e.getTimestamp()))).getTime() / 1000;
						if (firstEvent == null || firstEvent > etime)
							firstEvent = etime;
						Dot d = new Dot();
						d.setX(etime);
						String component = c.getId();
						if (config.getComponent().equalsIgnoreCase("CASE")) {
							component = c.getId();
						} else if (config.getComponent().equalsIgnoreCase("ACTIVITY")) {
							component = e.getLabel();
						} else if (config.getComponent().equalsIgnoreCase("ORIGINATOR")) {
							component = e.getOriginator();
						} else if (config.getComponent().equalsIgnoreCase("RESOURCE")) {
							component = e.getResource();
						}
						d.setY(component);
						String color = e.getLabel();
						if (config.getColor().equalsIgnoreCase("CASE")) {
							color = c.getId();
						} else if (config.getColor().equalsIgnoreCase("ACTIVITY")) {
							color = e.getLabel();
						} else if (config.getColor().equalsIgnoreCase("ORIGINATOR")) {
							color = e.getOriginator();
						} else if (config.getColor().equalsIgnoreCase("RESOURCE")) {
							color = e.getResource();
						}
						d.setName(color);
						d.setType(e.getType());
						dots.add(d);
					}
					if (config.isTimeRelative()) {
						for (Dot d : dots) {
							d.setX(d.getX() - firstEvent);
						}
					}

					return dots.iterator();
				}

			}).collect();

			long timeMin = Long.MAX_VALUE;
			long timeMax = Long.MIN_VALUE;
			for (Dot d : dots) {
				long x = d.getX();
				// String y = d.getY();
				if (x < timeMin)
					timeMin = x;
				if (x > timeMax)
					timeMax = x;
				model.getLegend().put(d.getY(), 0);
			}

			long timeTick = (timeMax - timeMin) / config.getTimeSplit();
			long timeCurrent = timeMin;
			model.getTimes().put(timeCurrent, 0);
			for (int i = 0; i < config.getTimeSplit(); i++) {
				timeCurrent += timeTick;
				model.getTimes().put(timeCurrent, 0);
			}

			for (Dot d : dots) {
				long x = d.getX();
				String y = d.getY();
				if (!model.getData().containsKey(y))
					model.getData().put(y, new TreeMap<Long, Data>());
				String color = d.getName() + " (" + d.getType() + ")";
				model.getColors().put(color, 0);
				if (model.getData().get(y).containsKey(x)) {
					model.getData().get(y).get(x).setName(model.getData().get(y).get(x).getName() + "|" + color);
					model.getData().get(y).get(x).setType("meet");
				} else {
					model.getData().get(y).put(x, new Data(color, d.getType()));
				}
			}

			RawJobResult result = new RawJobResult("model.DottedChart", outputURI, outputURI,
					mapper.writeValueAsString(model));
			se.getFileUtil().saveAsTextFile(se, outputURI + ".dcans", result.getResponse());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
