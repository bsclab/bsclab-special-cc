/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.workingtogether;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.UtilOperation;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import scala.Tuple2;

/* Simultaneous Appearance Ratio */
public class WorkingTogether_SAR extends WorkingTogetherAlgorithm implements
		PairFlatMapFunction<Iterator<Tuple2<String, ICase>>, String, Double>, Function2<Double, Double, Double> {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private int originatorNameCount;
	private List<String> originatorNameList;

	public WorkingTogether_SAR(List<String> originatorNameList) {
		this.originatorNameList = originatorNameList;
		this.originatorNameCount = originatorNameList.size();
	}

	@Override
	public Iterator<Tuple2<String, Double>> call(Iterator<Tuple2<String, ICase>> arg0) throws Exception {

		List<Tuple2<String, Double>> list = new LinkedList<Tuple2<String, Double>>();

		while (arg0.hasNext()) {
			ICase icase = arg0.next()._2;
			for (int i = 0; i < originatorNameCount; i++) {
				String originator1 = originatorNameList.get(i);
				for (int j = 0; j < originatorNameCount; j++) {
					String originator2 = originatorNameList.get(j);
					if (UtilOperation.isInCase(icase, originator1, originator2))
						list.add(new Tuple2<String, Double>(originator1 + "->" + originator2, 1.0d));
				}
			}
		}

		return list.iterator();
	}

	@Override
	public Double call(Double arg0, Double arg1) throws Exception {
		return arg0 + arg1;
	}
}
