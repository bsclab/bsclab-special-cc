/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ISparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkMysqlRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.controller.DateUtil;
import kr.ac.pusan.bsclab.bab.ws.controller.FileUtil;
import kr.ac.pusan.bsclab.bab.ws.controller.LocalFileUtil;
import kr.ac.pusan.bsclab.bab.ws.controller.SparkFileUtil;

public class SparkDbExecutor extends SparkExecutor implements ISparkExecutor, Serializable {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private static SparkConf configuration;
	private static JavaSparkContext context;
	private static Configuration hdfsConfiguration;

	public SparkConf getConfiguration() {
		return configuration;
	}

	public void setAppName(String appName) {
		if (configuration == null)
			return;
		configuration.setAppName(appName);
	}

	public void setExecutorCore(int noOfCore) {
		if (configuration == null)
			return;
		configuration.set("spark.cores.max", String.valueOf(noOfCore));
	}

	public void setExecutorMemory(int memSizeInMb) {
		if (configuration == null)
			return;
		configuration.set("spark.executor.memory", memSizeInMb + "g");
	}

	public void setDriverMemory(int memSizeInMb) {
		if (configuration == null)
			return;
		configuration.set("spark.driver.memory", memSizeInMb + "g");
	}

	public void setHdfsURI(String hdfsURI) {
		if (hdfsConfiguration == null) {
			hdfsConfiguration = new Configuration();
		}
		hdfsConfiguration.set("fs.defaultFS", hdfsURI);
	}

	public String getHdfsURI() {
		if (hdfsConfiguration == null)
			return null;
		return hdfsConfiguration.get("fs.defaultFS");
	}

	public String getHdfsURI(String path) {
		return path;
		// if (hdfsConfiguration == null)
		// return null;
		// return hdfsConfiguration.get("fs.defaultFS") + path;
	}

	public Configuration getHdfsConfiguration() {
		return hdfsConfiguration;
	}

	public FileSystem getHdfsFileSystem() {
		try {
			FileSystem fs = FileSystem.get(hdfsConfiguration);
			return fs;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void startup(String sparkMasterURI) {
		if (configuration == null) {
			configuration = new SparkConf();
			if (!sparkMasterURI.equalsIgnoreCase("yarn")) {
				configuration.setMaster(sparkMasterURI);
			}
			// configuration.set("spark.rdd.compress", "true");
			// configuration.set("spark.akka.frameSize", "128");
			// configuration.set("spark.storage.memoryFraction", "0.2");
		}
	}

	@Override
	public JavaSparkContext getContext() {
		if (configuration != null && context == null) {
			context = new JavaSparkContext(configuration);
		}
		return context;
	}

	@Override
	public void shutdown() {
		if (context != null) {
			context.stop();
		}
	}

	@Override
	public String getContextUri(String rawUri) {
		return getHdfsURI(rawUri);
	}

	private DateUtil dateUtil;
	private FileUtil fileUtil;

	@Override
	public DateUtil getDateUtil() {
		if (dateUtil == null) {
			dateUtil = DateUtil.getInstance();
		}
		return dateUtil;
	}

	@Override
	public FileUtil getFileUtil() {
		if (fileUtil == null) {
			fileUtil = new LocalFileUtil();
		}
		return fileUtil;
	}

	@Override
	public SparkFileUtil getSparkFileUtil() {
		return (SparkFileUtil) getFileUtil();
	}

	private static String jobId = "UNKNOWN";

	public static void setDebugId(String jobId) {
		SparkDbExecutor.jobId = jobId;
	}

	@Override
	public Class<? extends ISparkRepositoryReader> getReader() {
		return SparkMysqlRepositoryReader.class;
	}
}
