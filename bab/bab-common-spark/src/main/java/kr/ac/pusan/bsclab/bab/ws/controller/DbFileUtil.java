/*
 * 
 * Copyright © 2013-2015 Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;

import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;
import scala.Tuple2;

public class DbFileUtil extends SparkFileUtil implements Serializable {

	protected static DbFileUtil instance;

	public static DbFileUtil getInstance() {
		if (instance == null) {
			instance = new DbFileUtil();
		}
		return instance;
	}

	private static final long serialVersionUID = 1L;

	protected static final FlatMapFunction<String, String> loadRddMapper = new FlatMapFunction<String, String>() {

		private static final long serialVersionUID = 1L;

		@Override
		public Iterator<String> call(String t) throws Exception {
			ArrayList<String> values = new ArrayList<String>();
			return values.iterator();
		}

	};

	protected static final FlatMapFunction<Iterator<String>, Boolean> saveRddMapper = new FlatMapFunction<Iterator<String>, Boolean>() {

		private static final long serialVersionUID = 1L;

		@Override
		public Iterator<Boolean> call(Iterator<String> t) throws Exception {
			return null;
		}

	};

	protected static final FlatMapFunction<Iterator<Tuple2<String, String>>, Boolean> saveRddPairMapper = new FlatMapFunction<Iterator<Tuple2<String, String>>, Boolean>() {

		private static final long serialVersionUID = 1L;

		@Override
		public Iterator<Boolean> call(Iterator<Tuple2<String, String>> t) throws Exception {
			return null;
		}

	};

	@Override
	public JavaRDD<String> loadRdd(IExecutor e, String uri) {
		ISparkExecutor se = (ISparkExecutor) e;
		ArrayList<String> filePart = new ArrayList<String>();
		return se.getContext().parallelize(filePart).flatMap(loadRddMapper);
	}

	public JavaRDD<String> saveRddAsTextFile(IExecutor e, String uri, JavaRDD<String> dataRdd) {
		dataRdd.mapPartitions(saveRddMapper).count();
		return dataRdd;
	}

	public JavaPairRDD<String, String> saveRddAsTextFile(IExecutor e, String uri, JavaPairRDD<String, String> dataRdd) {
		dataRdd.mapPartitions(saveRddPairMapper).count();
		return dataRdd;
	}

	@Override
	public boolean isFileExists(IExecutor se, String uri) {
		try {
			File f = new File(uri);
			return f.exists();
		} catch (Exception e) {

		}
		return false;
	}

	@Override
	public void saveAsTextFile(IExecutor se, String uri, String data) {
		try {
			File file = new File(uri);
			File parentDir = file.getParentFile();
			if (parentDir != null && !parentDir.exists()) {
				parentDir.mkdirs();
			}
			FileWriter fw = new FileWriter(uri);
			BufferedWriter out = new BufferedWriter(fw);
			out.write(data);
			out.close();
		} catch (Exception e) {
			//System.out.println(e.getMessage());
		}
	}

	@Override
	public String loadTextFile(IExecutor se, String uri) {
		try {
			FileReader fileReader = new FileReader(uri);
			BufferedReader br = new BufferedReader(fileReader);
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line + System.lineSeparator());
			}
			br.close();
			return sb.toString();
		} catch (Exception e) {

		}
		return null;
	}

}
