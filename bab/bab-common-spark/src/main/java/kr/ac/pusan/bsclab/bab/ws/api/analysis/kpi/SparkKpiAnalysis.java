package kr.ac.pusan.bsclab.bab.ws.api.analysis.kpi;

import java.util.Iterator;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.broadcast.Broadcast;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.kpi.models.Kpi;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.kpi.models.KpiParameters;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryReader;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import scala.Tuple2;

/**
 * Example job class for Algorithm Test
 *
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkKpiAnalysis extends KpiAnalysis {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Main method for running Algorithm Test
	 */
	@Override
	public IJobResult run(String jsoni, IResource res, IExecutor se) throws Exception {
		JavaSparkContext sc = ((ISparkExecutor) se).getContext();
		Broadcast<String> jsonb = sc.broadcast(jsoni);
		String json = jsonb.getValue();
		ObjectMapper mapper = new ObjectMapper();
		KpiParameters config = mapper.readValue(json, KpiParameters.class);
		String outputURI = se.getContextUri(res.getUri());

		SparkRepositoryReader reader = new SparkRepositoryReader(se, config.getRepositoryURI(), config);

		// 1. On activity analysis
		reader.getCasesRDD().flatMapToPair(new PairFlatMapFunction<Tuple2<String, ICase>, String, Kpi>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Iterator<Tuple2<String, Kpi>> call(Tuple2<String, ICase> bcase) throws Exception {

				return null;
			}

		});

		// 2. On transition analysis
		Kpi result = new Kpi();

		se.getFileUtil().saveAsTextFile(se, outputURI + ".kpimodel", mapper.writeValueAsString(result));
		return null;
	}

}
