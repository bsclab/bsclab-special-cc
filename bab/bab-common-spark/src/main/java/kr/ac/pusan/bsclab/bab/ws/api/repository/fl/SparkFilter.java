package kr.ac.pusan.bsclab.bab.ws.api.repository.fl;

import org.apache.spark.api.java.JavaPairRDD;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;

public interface SparkFilter {
	JavaPairRDD<String, ICase> applyFilter(JavaPairRDD<String, ICase> casesRDD);
}
