package kr.ac.pusan.bsclab.bab.ws.api.repository.map;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.DoubleFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabService;
import kr.ac.pusan.bsclab.bab.ws.api.IExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.ISparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.api.SparkExecutor;
import kr.ac.pusan.bsclab.bab.ws.base.controller.IJobResult;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import kr.ac.pusan.bsclab.bab.ws.base.model.IResource;
import kr.ac.pusan.bsclab.bab.ws.model.BCase;
import kr.ac.pusan.bsclab.bab.ws.model.BEvent;
import kr.ac.pusan.bsclab.bab.ws.model.BRepository;
import kr.ac.pusan.bsclab.bab.ws.model.RawJobResult;
import scala.Tuple2;

/**
 * Map immediate repository to BAB repository <br>
 * <br>
 * Config class: {@link MappingJobConfiguration}<br>
 * Result class: {@link BRepository}
 *
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 */
public class SparkIntermediateMappingJob extends IntermediateMappingJob {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	public static final String DELIMETER_PARAM = ",";
	public static final String DELIMETER_LABEL = "+";
	public static final String FIELD_CASE = "CASE";
	public static final String FIELD_ACTIVITY = "ACTIVITY";
	public static final String FIELD_TYPE = "TYPE";
	public static final String FIELD_ORIGINATOR = "ORIGINATOR";
	public static final String FIELD_TIMESTAMP = "TIMESTAMP";
	public static final String FIELD_RESOURCE = "RESOURCE";
	public static final String FIELD_ATTRIBUTE = "ATTRIBUTE";
	public static final String SCOPE_CASE = "CASE";
	public static final String SCOPE_EVENT = "EVENT";
	public static final String EVENT_TYPE_DEFAULT = "complete";
	public static final String CASE_ID_VOID = "---VOID---";
	public static final String DATE_DEFAULT_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

	public static String buildData(Map<String, String> builders, Map<String, String> eventData, String scope) {
		StringBuilder builder = new StringBuilder();
		for (String dimension : builders.keySet()) {
			String value = builders.get(dimension);
			if (value.compareTo(scope) == 0) {
				String state = (eventData.containsKey(dimension)) ? eventData.get(dimension) : "";
				if (builder.length() > 0)
					builder.append(DELIMETER_LABEL);
				builder.append(state);
			}
		}
		return builder.length() > 0 ? builder.toString() : null;
	}

	@Override
	@BabService(name = "RepositoryMappingJob", title = "Repository Mapping Job", requestClass = MappingJobConfiguration.class, responseClass = BRepository.class, legacyJobExtension = ".mrepo")
	public IJobResult run(String json, IResource res, IExecutor se) {
		RawJobResult result = null;
		try {
			// FileSystem fs = se.getHdfsFileSystem();
			ObjectMapper jsonMapper = new ObjectMapper();
			final MappingJobConfiguration config = jsonMapper.readValue(json, MappingJobConfiguration.class);
			final Map<String, SimpleDateFormat> eventTimestamps = new LinkedHashMap<String, SimpleDateFormat>();
			if (config.getMapping().containsKey(FIELD_TIMESTAMP)) {
				Map<String, String> timestamps = config.getMapping().get(FIELD_TIMESTAMP);
				for (String dimension : timestamps.keySet()) {
					String[] value = timestamps.get(dimension).split(DELIMETER_PARAM);
					if (value.length > 0 && value[0].compareTo(SCOPE_EVENT) == 0) {
						String timestampString = dimension + DELIMETER_PARAM
								+ (value.length > 2 ? value[2] : EVENT_TYPE_DEFAULT);
						SimpleDateFormat timestampFormatter = new SimpleDateFormat(
								value.length > 1 ? value[1] : DATE_DEFAULT_FORMAT);
						eventTimestamps.put(timestampString, timestampFormatter);
					}
				}
			}
			final boolean useTypeField = eventTimestamps.size() == 1;

			System.err.println(config.getFilter().keySet());

			if (eventTimestamps.size() > 0) {
				String datasetURI = se.getContextUri(config.getDatasetUri());
				String repositoryURI = se.getContextUri(config.getRepositoryURI());
				String outputURI = se.getContextUri(
						config.getRepositoryURI() + "_" + config.getName().replaceAll("[^A-Za-z0-9]", ""));

				// final ImportJobResult repositoryMetadata =
				// jsonMapper.readValue(new
				// InputStreamReader(fs.open(new Path(repositoryURI +
				// ".mrepo")), "UTF-8"),
				// ImportJobResult.class);
				JavaPairRDD<String, BCase> casesRDD = ((ISparkExecutor) se).getSparkFileUtil()
						.loadRdd(se, datasetURI + ".irepo").map(new Function<String, Map<String, String>>() {

							/**
							 * Default serial version ID
							 */
							private static final long serialVersionUID = 1L;
							ObjectMapper jsonMapper = new ObjectMapper();
							TypeFactory t = TypeFactory.defaultInstance();
							MapType classType = t.constructMapType(LinkedHashMap.class, String.class, String.class);

							@Override
							public Map<String, String> call(String dataJson) throws Exception {
								Map<String, String> eventData = jsonMapper.readValue(dataJson, classType);
								return eventData;
							}
						}).filter(new Function<Map<String, String>, Boolean>() {

							/**
							 * Default serial version ID
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public Boolean call(Map<String, String> eventData) throws Exception {
								for (String dimension : eventData.keySet()) {
									String state = eventData.get(dimension);
									if (config.getFilter().containsKey(dimension)
											&& !config.getFilter().get(dimension).containsKey(state)) {
										System.err.println(config.getFilter().keySet());
										return false;
									}
								}
								return true;
							}
						}).mapToPair(new PairFunction<Map<String, String>, String, BCase>() {

							/**
							 * Default serial version ID
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public Tuple2<String, BCase> call(Map<String, String> eventData) throws Exception {
								// public static final String FIELD_TYPE =
								// "TYPE";
								// public static final String
								// FIELD_TIMESTAMP = "TIMESTAMP";
								// public static final String
								// FIELD_ATTRIBUTE = "ATTRIBUTE";

								String caseId = config.getMapping().containsKey(FIELD_CASE)
										? buildData(config.getMapping().get(FIELD_CASE), eventData, SCOPE_CASE) : null;
								String activity = config.getMapping().containsKey(FIELD_ACTIVITY)
										? buildData(config.getMapping().get(FIELD_ACTIVITY), eventData, SCOPE_EVENT)
										: null;
								String type = config.getMapping().containsKey(FIELD_TYPE)
										? buildData(config.getMapping().get(FIELD_TYPE), eventData, SCOPE_EVENT) : null;
								String originator = config.getMapping().containsKey(FIELD_ORIGINATOR)
										? buildData(config.getMapping().get(FIELD_ORIGINATOR), eventData, SCOPE_EVENT)
										: null;
								String resource = config.getMapping().containsKey(FIELD_RESOURCE)
										? buildData(config.getMapping().get(FIELD_RESOURCE), eventData, SCOPE_EVENT)
										: null;
								BCase bcase = new BCase(caseId, caseId);
								if (type == null)
									type = EVENT_TYPE_DEFAULT;
								if (caseId != null && activity != null) {
									for (String timestampField : eventTimestamps.keySet()) {
										SimpleDateFormat formatter = eventTimestamps.get(timestampField);
										String[] timestampStrings = timestampField.split(DELIMETER_PARAM);
										type = useTypeField ? type : timestampStrings[1];
										if (eventData.containsKey(timestampStrings[0])) {
											try {
												long timestamp = formatter.parse(eventData.get(timestampStrings[0]))
														.getTime();
												BEvent bevent = new BEvent(String.valueOf(bcase.getEvents().size()),
														caseId, activity, type, originator, timestamp, resource);
												if (config.getMapping().containsKey(FIELD_ATTRIBUTE)) {
													Map<String, String> attributes = config.getMapping()
															.get(FIELD_ATTRIBUTE);
													for (String attribute : attributes.keySet()) {
														String value = attributes.get(attribute);
														attribute = attribute.replace(".", "");
														String state = (eventData.containsKey(attribute))
																? eventData.get(attribute) : "";
														if (value.compareTo(SCOPE_EVENT) == 0) {
															bevent.getAttributes().put(attribute, state);
														} else if (value.compareTo(SCOPE_CASE) == 0) {
															bcase.getAttributes().put(attribute, state);
														}
													}
												}
												bcase.getEvents().put(bevent.getId(), bevent);
											} catch (Exception ex) {
												SparkExecutor.log(this).debug(ex.getMessage());
											}
										}
									}
								}
								return new Tuple2<String, BCase>(bcase.getEvents().size() > 0 ? caseId : CASE_ID_VOID,
										bcase);
							}

						}).filter(new Function<Tuple2<String, BCase>, Boolean>() {
							/**
							 * Default serial version ID
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public Boolean call(Tuple2<String, BCase> arg0) throws Exception {
								return arg0._1().compareTo(CASE_ID_VOID) != 0;
							}
						}).reduceByKey(new Function2<BCase, BCase, BCase>() {
							/**
							 * Default serial version ID
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public BCase call(BCase arg0, BCase arg1) throws Exception {
								BCase bcase = null;
								if (arg0 != null) {
									bcase = new BCase(arg0.getId(), arg0.getUri());
									for (IEvent e : arg0.getEvents().values()) {
										((BEvent) e).setId(String.valueOf(bcase.getEvents().size()));
										bcase.getEvents().put(e.getId(), e);
									}
									bcase.getAttributes().putAll(arg0.getAttributes());
								}
								if (arg1 != null) {
									if (bcase == null)
										bcase = new BCase(arg1.getId(), arg1.getUri());
									for (IEvent e : arg1.getEvents().values()) {
										((BEvent) e).setId(String.valueOf(bcase.getEvents().size()));
										bcase.getEvents().put(e.getId(), e);
									}
									bcase.getAttributes().putAll(arg1.getAttributes());
								}
								return bcase;
							}

						}).mapToPair(new PairFunction<Tuple2<String, BCase>, String, BCase>() {
							/**
							 * Default serial version ID
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public Tuple2<String, BCase> call(Tuple2<String, BCase> arg0) throws Exception {
								BCase bcase = arg0._2();
								Map<Long, List<IEvent>> sorted = new TreeMap<Long, List<IEvent>>();
								for (IEvent e : bcase.getEvents().values()) {
									long timestamp = e.getTimestamp();
									if (!sorted.containsKey(timestamp))
										sorted.put(timestamp, new ArrayList<IEvent>());
									sorted.get(timestamp).add(e);
								}
								bcase.getEvents().clear();
								for (Long t : sorted.keySet()) {
									for (IEvent e : sorted.get(t)) {
										bcase.getEvents().put(String.valueOf(bcase.getEvents().size()), e);
									}
								}
								return new Tuple2<String, BCase>(arg0._1(), bcase);
							}
						});

				casesRDD.map(new Function<Tuple2<String, BCase>, String>() {
					/**
					 * Default serial version ID
					 */
					private static final long serialVersionUID = 1L;
					ObjectMapper jsonMapper = new ObjectMapper();

					@Override
					public String call(Tuple2<String, BCase> arg0) throws Exception {
						return jsonMapper.writeValueAsString(arg0._2());
					}
				}).saveAsTextFile(outputURI + ".trepo");
				BRepository repository = generateRepositorySummary(casesRDD, config, repositoryURI);

				String repositoryJson = jsonMapper.writeValueAsString(repository);
				// RawJobResult result = new RawJobResult("repository.Repository", outputURI, outputURI, repositoryJson);
				result = new RawJobResult("repository.Repository", outputURI, outputURI, repositoryJson);
				se.getFileUtil().saveAsTextFile(se, outputURI + ".brepo", result.getResponse());
				// return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return null;
		return result;
	}

	public BRepository generateRepositorySummary(JavaPairRDD<String, BCase> casesRDD, MappingJobConfiguration mapConfig,
			String repositoryURI) {

		if (casesRDD.count() < 1) {
			return null;
		}
		Function2<Integer, Integer, Integer> integerReducer = new Function2<Integer, Integer, Integer>() {
			/**
			 * Default serial version ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Integer call(Integer arg0, Integer arg1) throws Exception {
				return arg0 + arg1;
			}
		};

		Map<String, Integer> cases = casesRDD.mapToPair(new PairFunction<Tuple2<String, BCase>, String, Integer>() {
			/**
			 * Default serial version ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Tuple2<String, Integer> call(Tuple2<String, BCase> arg0) throws Exception {
				return new Tuple2<String, Integer>(arg0._1(), 1);
			}
		}).reduceByKey(integerReducer).collectAsMap();

		double eventCounts = casesRDD.mapToDouble(new DoubleFunction<Tuple2<String, BCase>>() {
			/**
			 * Default serial version ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public double call(Tuple2<String, BCase> arg0) throws Exception {
				return arg0._2().getEvents().size();
			}
		}).sum();
		Map<String, Integer> activities = casesRDD
				.flatMapToPair(new PairFlatMapFunction<Tuple2<String, BCase>, String, Integer>() {
					/**
					 * Default serial version ID
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Iterator<Tuple2<String, Integer>> call(Tuple2<String, BCase> arg0) throws Exception {
						Map<String, Integer> r = new LinkedHashMap<String, Integer>();
						for (IEvent e : arg0._2().getEvents().values()) {
							String k = e.getLabel();
							if (!r.containsKey(k))
								r.put(k, 0);
							r.put(k, r.get(k) + 1);
						}
						List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
						for (String k : r.keySet()) {
							Integer f = r.get(k);
							result.add(new Tuple2<String, Integer>(k, f));
						}
						return result.iterator();
					}
				}).reduceByKey(integerReducer).collectAsMap();
		Map<String, Integer> activityTypes = casesRDD
				.flatMapToPair(new PairFlatMapFunction<Tuple2<String, BCase>, String, Integer>() {
					/**
					 * Default serial version ID
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Iterator<Tuple2<String, Integer>> call(Tuple2<String, BCase> arg0) throws Exception {
						Map<String, Integer> r = new LinkedHashMap<String, Integer>();
						for (IEvent e : arg0._2().getEvents().values()) {
							String k = e.getType();
							if (!r.containsKey(k))
								r.put(k, 0);
							r.put(k, r.get(k) + 1);
						}
						List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
						for (String k : r.keySet()) {
							Integer f = r.get(k);
							result.add(new Tuple2<String, Integer>(k, f));
						}
						return result.iterator();
					}
				}).reduceByKey(integerReducer).collectAsMap();
		Map<String, Integer> originators = casesRDD
				.flatMapToPair(new PairFlatMapFunction<Tuple2<String, BCase>, String, Integer>() {
					/**
					 * Default serial version ID
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Iterator<Tuple2<String, Integer>> call(Tuple2<String, BCase> arg0) throws Exception {
						Map<String, Integer> r = new LinkedHashMap<String, Integer>();
						for (IEvent e : arg0._2().getEvents().values()) {
							String k = e.getOriginator();
							if (!r.containsKey(k))
								r.put(k, 0);
							r.put(k, r.get(k) + 1);
						}
						List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
						for (String k : r.keySet()) {
							Integer f = r.get(k);
							result.add(new Tuple2<String, Integer>(k, f));
						}
						return result.iterator();
					}
				}).reduceByKey(integerReducer).collectAsMap();
		Map<String, Integer> resources = casesRDD
				.flatMapToPair(new PairFlatMapFunction<Tuple2<String, BCase>, String, Integer>() {
					/**
					 * Default serial version ID
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Iterator<Tuple2<String, Integer>> call(Tuple2<String, BCase> arg0) throws Exception {
						Map<String, Integer> r = new LinkedHashMap<String, Integer>();
						for (IEvent e : arg0._2().getEvents().values()) {
							String k = e.getResource();
							if (!r.containsKey(k))
								r.put(k, 0);
							r.put(k, r.get(k) + 1);
						}
						List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
						for (String k : r.keySet()) {
							Integer f = r.get(k);
							result.add(new Tuple2<String, Integer>(k, f));
						}
						return result.iterator();
					}
				}).reduceByKey(integerReducer).collectAsMap();
		Map<String, Integer> attributes = casesRDD
				.flatMapToPair(new PairFlatMapFunction<Tuple2<String, BCase>, String, Integer>() {
					/**
					 * Default serial version ID
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public Iterator<Tuple2<String, Integer>> call(Tuple2<String, BCase> arg0) throws Exception {
						Map<String, Integer> r = new LinkedHashMap<String, Integer>();
						Map<String, Object> attributes = arg0._2().getAttributes();
						for (String attr : attributes.keySet()) {
							String k = "C|" + attr + "|" + attributes.get(attr);
							if (!r.containsKey(k))
								r.put(k, 0);
							r.put(k, r.get(k) + 1);
						}
						for (IEvent e : arg0._2().getEvents().values()) {
							attributes = e.getAttributes();
							for (String attr : attributes.keySet()) {
								String k = "E|" + attr + "|" + attributes.get(attr);
								if (!r.containsKey(k))
									r.put(k, 0);
								r.put(k, r.get(k) + 1);
							}
						}
						List<Tuple2<String, Integer>> result = new ArrayList<Tuple2<String, Integer>>();
						for (String k : r.keySet()) {
							Integer f = r.get(k);
							result.add(new Tuple2<String, Integer>(k, f));
						}
						return result.iterator();
					}

				}).reduceByKey(integerReducer).collectAsMap();

		Long startTime = casesRDD.map(new Function<Tuple2<String, BCase>, Long>() {
			/**
			 * Default serial version ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Long call(Tuple2<String, BCase> arg0) throws Exception {
				Long result = null;
				for (IEvent e : arg0._2().getEvents().values()) {
					if (result == null && e.getTimestamp() > 0) {
						result = e.getTimestamp();
					}
					if (e.getTimestamp() > 0 && result > e.getTimestamp())
						result = e.getTimestamp();
				}
				return result;
			}
		}).reduce(new Function2<Long, Long, Long>() {
			/**
			 * Default serial version ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Long call(Long arg0, Long arg1) throws Exception {
				return arg0 < arg1 ? arg0 : arg1;
			}
		});

		Long endTime = casesRDD.map(new Function<Tuple2<String, BCase>, Long>() {
			/**
			 * Default serial version ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Long call(Tuple2<String, BCase> arg0) throws Exception {
				Long result = null;
				for (IEvent e : arg0._2().getEvents().values()) {
					if (result == null && e.getTimestamp() > 0) {
						result = e.getTimestamp();
					}
					if (e.getTimestamp() > 0 && result < e.getTimestamp())
						result = e.getTimestamp();
				}
				return result;
			}
		}).reduce(new Function2<Long, Long, Long>() {
			/**
			 * Default serial version ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public Long call(Long arg0, Long arg1) throws Exception {
				return arg0 > arg1 ? arg0 : arg1;
			}
		});

		BRepository repository = new BRepository("BRepository@" + System.currentTimeMillis(), repositoryURI,
				mapConfig.getRepositoryURI(), System.currentTimeMillis(), cases.size(), (int) eventCounts,
				activities.size(), activityTypes.size(), originators.size(), resources.size());
		repository.setTimestampStart(startTime);
		repository.setTimestampEnd(endTime);
		toString();

		repository.setName(mapConfig.getName());
		repository.setDescription(mapConfig.getDescription());
		repository.setMapping(mapConfig.getMapping());

		repository.getCases().putAll(cases);
		repository.getActivities().putAll(activities);
		repository.getActivityTypes().putAll(activityTypes);
		repository.getOriginators().putAll(originators);
		repository.getResources().putAll(resources);

		for (String k : attributes.keySet()) {
			String[] keys = k.split("\\|", 3);
			String type = keys[0];
			String key = keys[1];
			String value = keys[2];
			if (keys.length > 2) {
				if (type.equalsIgnoreCase("C")) {
					if (!repository.getNoOfCaseAttributes().containsKey(key))
						repository.getNoOfCaseAttributes().put(key, 0);
					repository.getNoOfCaseAttributes().put(key, repository.getNoOfCaseAttributes().get(key) + 1);
					if (!repository.getCaseAttributes().containsKey(key))
						repository.getCaseAttributes().put(key, new TreeMap<String, Integer>());
					if (!repository.getCaseAttributes().get(key).containsKey(value))
						repository.getCaseAttributes().get(key).put(value, 0);
					repository.getCaseAttributes().get(key).put(value,
							repository.getCaseAttributes().get(key).get(value) + 1);
				} else {
					if (!repository.getNoOfEventAttributes().containsKey(key))
						repository.getNoOfEventAttributes().put(key, 0);
					repository.getNoOfEventAttributes().put(key, repository.getNoOfEventAttributes().get(key) + 1);
					if (!repository.getEventAttributes().containsKey(key))
						repository.getEventAttributes().put(key, new TreeMap<String, Integer>());
					if (!repository.getEventAttributes().get(key).containsKey(value))
						repository.getEventAttributes().get(key).put(value, 0);
					repository.getEventAttributes().get(key).put(value,
							repository.getEventAttributes().get(key).get(value) + 1);
				}
			}
		}
		return repository;
	}

}
