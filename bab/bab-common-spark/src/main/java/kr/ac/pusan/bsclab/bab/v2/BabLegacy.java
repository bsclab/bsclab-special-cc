/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/

package kr.ac.pusan.bsclab.bab.v2;

import java.util.Map;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabPackage;
import kr.ac.pusan.bsclab.bab.v2.core.annotations.BabPackageCategory;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.ar.SparkFpGrowthArMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dc.SparkDottedChartAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.dl.SparkDeltaAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SparkSocialNetworkAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tg.SparkTimeGapAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.tm.SparkTaskMatrixAnalysisJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.fm.SparkFuzzyMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicBpmnMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.model.hm.SparkHeuristicMinerJob2;
import kr.ac.pusan.bsclab.bab.ws.api.model.lr.SparkLogReplayJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryViewDatasetJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.SparkRepositoryViewJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ex.hmodel.bpmn.SparkHNetToBPMNMinerJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.fl.SparkFilteringJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.im.csv.brepo.SparkCsvImportJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.ls.SparkLogSummaryJob;
import kr.ac.pusan.bsclab.bab.ws.api.repository.map.SparkIntermediateMappingJob;
import kr.ac.pusan.re.ebiz.bab.ws.api.analysis.pc.SparkPerformanceChartAnalysisJob;

/**
 * 
 * BAB main class, this class will be included in bab.jar file as an entry point
 * class
 * 
 * @author Iq Reviessay Pulshashi (pulshashi@ideas.web.id)
 * @since v2.0
 *
 */
@BabPackage(name = "kr.ac.pusan.bsclab.bab.v2.legacy", title = "BAB Legacy Package", version = "2.3.0", download = "http://bsclab.pusan.ac.kr/apps/bab/v2/packages/bab-common-spark-2.3.0-SNAPSHOT.jar", jars = "bab-common-spark-2.3.0-SNAPSHOT.jar", mainClass = "kr.ac.pusan.bsclab.bab.v2.BabLegacy", categories = {
		BabPackageCategory.REPOSITORY, BabPackageCategory.DESCRIPTIVE, BabPackageCategory.VISUALIZATION, })
public class BabLegacy extends BabBootstrap {
	@Override
	public Map<String, Class<?>> getJobClasses() {
		Map<String, Class<?>> jc = super.getJobClasses();
		jobClasses.put("RepositoryCsvImportJob", SparkCsvImportJob.class);
		jobClasses.put("RepositoryMappingJob", SparkIntermediateMappingJob.class);
		jobClasses.put("RepositoryFilteringJob", SparkFilteringJob.class);
		jobClasses.put("RepositoryViewJob", SparkRepositoryViewJob.class);
		jobClasses.put("RepositoryViewDatasetJob", SparkRepositoryViewDatasetJob.class);
		jobClasses.put("RepositoryLogSummaryJob", SparkLogSummaryJob.class);

		jobClasses.put("ModelHeuristicJob", SparkHeuristicMinerJob.class);
		jobClasses.put("ModelHeuristicBpmnJob", SparkHeuristicBpmnMinerJob.class);
		jobClasses.put("ModelFuzzyJob", SparkFuzzyMinerJob.class);
		jobClasses.put("ModelLogReplayJob", SparkLogReplayJob.class);
		jobClasses.put("AnalysisSocialNetworkJob", SparkSocialNetworkAnalysisJob.class);

		jobClasses.put("ModelHeuristicJob2", SparkHeuristicMinerJob2.class);
		jobClasses.put("ModelHNetToBPMNConvertJob", SparkHNetToBPMNMinerJob.class);

		jobClasses.put("AnalysisAssociationRuleJob", SparkFpGrowthArMinerJob.class);
		jobClasses.put("AnalysisTaskMatrixJob", SparkTaskMatrixAnalysisJob.class);
		jobClasses.put("AnalysisDottedChartJob", SparkDottedChartAnalysisJob.class);
		jobClasses.put("AnalysisPerformanceChartJob", SparkPerformanceChartAnalysisJob.class);

		jobClasses.put("AnalysisTimeGapJob", SparkTimeGapAnalysisJob.class);
		jobClasses.put("AnalysisDeltaJob", SparkDeltaAnalysisJob.class);
		return jc;
	}
}
