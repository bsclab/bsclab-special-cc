/*
 * 
 * Copyright © 2013-2015 Choi Deokil (cdi1318@gmail.com)
 *
 * This file is part of Best Analytics of Big Data (BAB) Package.
 *
 * "BAB" is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * "BAB" is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/lgpl-3.0.html>.
 *
 */

package kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.algorithm.handover;

import java.util.ArrayList;
import java.util.List;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.SocialMatrix;
import kr.ac.pusan.bsclab.bab.ws.api.analysis.sn.UtilOperation;
import kr.ac.pusan.bsclab.bab.ws.base.model.ICase;
import kr.ac.pusan.bsclab.bab.ws.base.model.IEvent;
import org.apache.spark.api.java.JavaPairRDD;
import scala.Tuple2;

public class Handover_ICIDIM extends HandOverAlgorithm {

	/**
	 * Default serial version ID
	 */
	private static final long serialVersionUID = 1L;
	private final List<String> originatorNameList;
	private final JavaPairRDD<String, ICase> rdd;

	public Handover_ICIDIM(final List<String> originatorNameList, final JavaPairRDD<String, ICase> rdd) {
		this.originatorNameList = originatorNameList;
		this.rdd = rdd;
	}

	@Override
	public SocialMatrix calculate(double beta, int depth) {
		List<Tuple2<String, ICase>> cases = rdd.collect();
		int originatorNum = originatorNameList.size();

		double normalVal = 0;
		SocialMatrix D = new SocialMatrix(originatorNum, originatorNum, 0, originatorNameList);

		for (Tuple2<String, ICase> t : cases) {
			ICase icase = t._2();
			List<IEvent> events = new ArrayList<IEvent>(icase.getEvents().values());
			int minK = 0;
			if (events.size() < depth)
				minK = events.size();
			else
				minK = depth + 1;
			if (minK < 2)
				minK = 2;

			for (int k = 1; k < minK; k++) {
				normalVal += Math.pow(beta, k - 1);
				SocialMatrix m = new SocialMatrix(originatorNum, originatorNum, 0);
				for (int i = 0; i < events.size() - k; i++) {
					IEvent ent = null, ent2 = null;
					try {
						ent = events.get(i);
						ent2 = events.get(i + k);
					} catch (IndexOutOfBoundsException ee) {
					}
					;

					if (ent.getOriginator() == null || ent2.getOriginator() == null)
						continue;

					int row = D.findIndexBy(ent.getOriginator());
					int col = D.findIndexBy(ent2.getOriginator());
					m.set(row, col, 1);
				}
				for (int i = 0; i < originatorNum; i++)
					for (int j = 0; j < originatorNum; j++)
						D.set(i, j, D.get(i, j) + m.get(i, j) * Math.pow(beta, k - 1));
			}
		}

		return UtilOperation.normalize(D, normalVal);
	}
}
