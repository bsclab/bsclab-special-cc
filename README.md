/******************************************************************************
*      This file is part of Best Analytics of Big Data (BAB) v2.3 Package.     *
* ============================================================================ *
*                                                                              *
*                            @@@@@@        Best Analytics of Big Data (BAB) is *
*       @@@@@@@@@@@@@@@@@@@@@@@@@@@@@      free software; you can redistribute *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    it and/or modify it under the terms *
*  @@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   of  GNU General  Public License  as *
* @@@@@@@@   @@@@@@@@@   @@@@@@   @@@@@@   published  by   the  Free  Software *
* @@@@@@@@               @@@@@@   @@@@@@   Foundation; either version 3 of the *
*  @@@@@@@               @@@@@@   @@@@@@   License,  or (at  your option)  any *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@   later version.                      *
*  @@@@@@@   @@@@@@@@@   @@@@@@      @@@                                       *
*  @@@@@@@               @@@@@@   @@@@@@@  "BAB" is distributed in the hope it *
*  @@@@@@@               @@@@@@   @@@@@@@  will  be useful,  but  WIHTOUT  ANY *
*  @@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@  WARRANTY; without  even the implied *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@  warranty  of   MERCHANTABILITY   of *
*   @@@@@@                        @@@@@@@  FITNESS  FOR A PARTICULAR  PURPOSE. *
*   @@@@@@                        @@@@@@@  See   GNU  Lesser  General   Public *
*   @@@@@@   @@@@@@@@@@@@@@@@@@   @@@@@@@@ License for mode details.           *
*   @@@@@@                        @@@@@@@@                                     *
*   @@@@@@                        @@@@@@@  You  should receive  a copy of  the *
*    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@    GNU  General  Public License  along *
*      @@@@@@@@@@@@@@@@@@@@@@@@@@@         with  this  program.  If  not,  see *
*          @@@@@@@@@                       www.gnu.org/licenses/lgpl-3.0.html  *
*                                                                              *
* ============================================================================ *
*               Copyright (C) 2013-2016 BSCLab Members @ IEPNU                 *
* ---------------------------------------------------------------------------- *
*              Iq Reviessay Pulshashi  <pulshashi@ideas.web.id>                *
 ******************************************************************************/
 
# SETUP DEVELOPMENT ENVIRONMENT

## On Windows
1. Extract winutils.exe from bab/var/lib/hadoop/bin/winutils.zip into bab/var/lib/hadoop/bin/winutils.exe
2. Open bab/bab-common-spark/pom.xml, and find maven-surefire-plugin section and change the environmentVariables for hadoop home
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<environmentVariables>
						<HADOOP_HOME>Y:\0Cloud\Dropbox
							(Personal)\1Ideas\2013-IEPNU\2018\BABv3\app\babv3\bab\var\lib\hadoop</HADOOP_HOME>
					</environmentVariables>
					<systemPropertyVariables>
						<HADOOP_HOME>Y:\0Cloud\Dropbox
							(Personal)\1Ideas\2013-IEPNU\2018\BABv3\app\babv3\bab\var\lib\hadoop</HADOOP_HOME>
					</systemPropertyVariables>
					<!-- <skipTests>true</skipTests> -->
					<skipTests>true</skipTests>
					<testFailureIgnore>true</testFailureIgnore>
				</configuration>
			</plugin>
3. Open bab/bab-web-common/src/main/resources/application-devlocal.properties and change bab.homeDirectory
4. Create database 2018_bab and import db/bab-v2-log-mysql.sql
5. Create database user bab with password bab
6. Modify bab/bab-common/src/main/java/kr/ac/pusan/bsclab/bab/ws/api/Configuration.java as you please for database information
7. Run maven install from eclipse once, and make sure you have bab-[BUILD].jar in your bab/var/hdfs/bab/v2/packages
8. Run bab-web-common as Spring Boot App