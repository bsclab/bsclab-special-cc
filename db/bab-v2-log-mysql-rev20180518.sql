-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for 2018_bab
CREATE DATABASE IF NOT EXISTS `2018_bab` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `2018_bab`;

-- Dumping structure for table 2018_bab.bab_log
CREATE TABLE IF NOT EXISTS `bab_log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `log_workspace_id` varchar(255) NOT NULL DEFAULT 'default',
  `log_dataset_id` varchar(255) NOT NULL DEFAULT 'default',
  `log_title` varchar(255) NOT NULL DEFAULT 'Untitled',
  `log_author` varchar(255) NOT NULL DEFAULT 'User',
  `log_description` text,
  `log_metadata` longtext,
  `log_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table 2018_bab.bab_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `bab_log` DISABLE KEYS */;
INSERT INTO `bab_log` (`log_id`, `log_workspace_id`, `log_dataset_id`, `log_title`, `log_author`, `log_description`, `log_metadata`, `log_created`, `log_modified`) VALUES
	(1, 'default', 'default', 'TEST_LOG', 'USER', '', NULL, '2018-02-19 05:13:12', '2018-02-19 10:14:42');
/*!40000 ALTER TABLE `bab_log` ENABLE KEYS */;

-- Dumping structure for table 2018_bab.bab_log_cases
CREATE TABLE IF NOT EXISTS `bab_log_cases` (
  `case_log_id` int(11) unsigned NOT NULL,
  `case_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `case_title` varchar(255) NOT NULL DEFAULT '',
  `case_author` varchar(255) NOT NULL DEFAULT '',
  `case_description` text,
  `case_metadata` longtext,
  `case_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `case_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`case_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table 2018_bab.bab_log_cases: ~13 rows (approximately)
/*!40000 ALTER TABLE `bab_log_cases` DISABLE KEYS */;
INSERT INTO `bab_log_cases` (`case_log_id`, `case_id`, `case_title`, `case_author`, `case_description`, `case_metadata`, `case_created`, `case_modified`) VALUES
	(1, 1, 'CASE001', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 2, 'CASE002', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 3, 'CASE003', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 4, 'CASE004', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 5, 'CASE005', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 6, 'CASE006', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 7, 'CASE007', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 8, 'CASE008', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 9, 'CASE009', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 10, 'CASE010', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 11, 'CASE011', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 12, 'CASE012', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18'),
	(1, 13, 'CASE013', 'TEST', NULL, NULL, '2018-04-18 17:37:18', '2018-04-18 17:37:18');
/*!40000 ALTER TABLE `bab_log_cases` ENABLE KEYS */;

-- Dumping structure for table 2018_bab.bab_log_events
CREATE TABLE IF NOT EXISTS `bab_log_events` (
  `event_case_id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event_activity` varchar(255) NOT NULL DEFAULT '',
  `event_type` varchar(255) NOT NULL DEFAULT 'complete',
  `event_timestamp` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `event_originator` varchar(255) NOT NULL DEFAULT '',
  `event_resource` varchar(255) NOT NULL DEFAULT '',
  `event_metadata` longtext,
  `event_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `event_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- Dumping data for table 2018_bab.bab_log_events: ~80 rows (approximately)
/*!40000 ALTER TABLE `bab_log_events` DISABLE KEYS */;
INSERT INTO `bab_log_events` (`event_case_id`, `event_id`, `event_activity`, `event_type`, `event_timestamp`, `event_originator`, `event_resource`, `event_metadata`, `event_created`, `event_modified`) VALUES
	(1, 1, 'A', 'complete', '2001-01-01 00:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(1, 2, 'B', 'complete', '2001-01-02 00:00:00', 'X', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(1, 3, 'C', 'complete', '2001-01-03 00:00:00', 'Y', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(1, 4, 'D', 'complete', '2001-01-04 00:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(2, 5, 'A', 'complete', '2001-02-01 00:00:00', 'Z', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(2, 6, 'B', 'complete', '2001-02-02 00:00:00', 'W', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(2, 7, 'C', 'complete', '2001-02-03 00:00:00', 'X', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(2, 8, 'D', 'complete', '2001-02-04 00:00:00', 'Y', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(3, 9, 'A', 'complete', '2001-03-01 00:00:00', 'Y', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(3, 10, 'B', 'complete', '2001-03-02 00:00:00', 'Z', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(3, 11, 'C', 'complete', '2001-03-03 00:00:00', 'W', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(3, 12, 'D', 'complete', '2001-03-04 00:00:00', 'X', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(4, 13, 'A', 'complete', '2001-01-01 06:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(4, 14, 'C', 'complete', '2001-01-02 06:00:00', 'X', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(4, 15, 'B', 'complete', '2001-01-03 06:00:00', 'Y', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(4, 16, 'D', 'complete', '2001-01-04 06:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(5, 17, 'A', 'complete', '2001-02-01 06:00:00', 'Z', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(5, 18, 'C', 'complete', '2001-02-02 06:00:00', 'W', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(5, 19, 'B', 'complete', '2001-02-03 06:00:00', 'X', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(5, 20, 'D', 'complete', '2001-02-04 06:00:00', 'Y', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(6, 21, 'A', 'complete', '2001-03-01 06:00:00', 'Y', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(6, 22, 'C', 'complete', '2001-03-02 06:00:00', 'Z', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(6, 23, 'B', 'complete', '2001-03-03 06:00:00', 'W', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(6, 24, 'D', 'complete', '2001-03-04 06:00:00', 'X', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(7, 25, 'A', 'complete', '2001-04-01 06:00:00', 'X', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(7, 26, 'C', 'complete', '2001-04-02 06:00:00', 'Y', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(7, 27, 'B', 'complete', '2001-04-03 06:00:00', 'Z', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(7, 28, 'D', 'complete', '2001-04-04 06:00:00', 'W', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(8, 29, 'A', 'complete', '2001-01-01 12:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(8, 30, 'B', 'complete', '2001-01-02 12:00:00', 'X', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(8, 31, 'C', 'complete', '2001-01-03 12:00:00', 'Y', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(8, 32, 'E', 'complete', '2001-01-04 12:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(8, 33, 'F', 'complete', '2001-01-05 12:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(8, 34, 'B', 'complete', '2001-01-06 12:00:00', 'X', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(8, 35, 'C', 'complete', '2001-01-07 12:00:00', 'Y', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(8, 36, 'D', 'complete', '2001-01-08 12:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(9, 37, 'A', 'complete', '2001-02-01 12:00:00', 'Y', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(9, 38, 'B', 'complete', '2001-02-02 12:00:00', 'Z', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(9, 39, 'C', 'complete', '2001-02-03 12:00:00', 'W', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(9, 40, 'E', 'complete', '2001-02-04 12:00:00', 'X', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(9, 41, 'F', 'complete', '2001-02-05 12:00:00', 'Y', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(9, 42, 'B', 'complete', '2001-02-06 12:00:00', 'Z', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(9, 43, 'C', 'complete', '2001-02-07 12:00:00', 'W', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(9, 44, 'D', 'complete', '2001-02-08 12:00:00', 'X', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(10, 45, 'A', 'complete', '2001-01-01 18:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(10, 46, 'C', 'complete', '2001-01-02 18:00:00', 'Y', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(10, 47, 'B', 'complete', '2001-01-03 18:00:00', 'X', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(10, 48, 'E', 'complete', '2001-01-04 18:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(10, 49, 'F', 'complete', '2001-01-05 18:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(10, 50, 'B', 'complete', '2001-01-06 18:00:00', 'X', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(10, 51, 'C', 'complete', '2001-01-07 18:00:00', 'Y', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(10, 52, 'D', 'complete', '2001-01-08 18:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(11, 53, 'A', 'complete', '2001-02-01 18:00:00', 'Y', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(11, 54, 'C', 'complete', '2001-02-02 18:00:00', 'W', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(11, 55, 'B', 'complete', '2001-02-03 18:00:00', 'Z', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(11, 56, 'E', 'complete', '2001-02-04 18:00:00', 'X', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(11, 57, 'F', 'complete', '2001-02-05 18:00:00', 'Y', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(11, 58, 'B', 'complete', '2001-02-06 18:00:00', 'Z', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(11, 59, 'C', 'complete', '2001-02-07 18:00:00', 'W', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(11, 60, 'D', 'complete', '2001-02-08 18:00:00', 'X', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(12, 61, 'A', 'complete', '2001-01-01 21:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(12, 62, 'B', 'complete', '2001-01-02 21:00:00', 'X', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(12, 63, 'C', 'complete', '2001-01-03 21:00:00', 'Y', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(12, 64, 'E', 'complete', '2001-01-04 21:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(12, 65, 'F', 'complete', '2001-01-05 21:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(12, 66, 'C', 'complete', '2001-01-06 21:00:00', 'Y', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(12, 67, 'B', 'complete', '2001-01-07 21:00:00', 'X', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(12, 68, 'D', 'complete', '2001-01-08 21:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 69, 'A', 'complete', '2001-01-01 03:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 70, 'C', 'complete', '2001-01-02 03:00:00', 'X', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 71, 'B', 'complete', '2001-01-03 03:00:00', 'Y', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 72, 'E', 'complete', '2001-01-04 03:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 73, 'F', 'complete', '2001-01-05 03:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 74, 'B', 'complete', '2001-01-06 03:00:00', 'X', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 75, 'C', 'complete', '2001-01-07 03:00:00', 'Y', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 76, 'E', 'complete', '2001-01-08 03:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 77, 'F', 'complete', '2001-01-05 03:00:00', 'W', 'I', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 78, 'C', 'complete', '2001-01-06 03:00:00', 'Y', 'J', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 79, 'B', 'complete', '2001-01-07 03:00:00', 'X', 'K', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46'),
	(13, 80, 'D', 'complete', '2001-01-08 03:00:00', 'Z', 'L', NULL, '2018-04-18 17:46:46', '2018-04-18 17:46:46');
/*!40000 ALTER TABLE `bab_log_events` ENABLE KEYS */;

-- Dumping structure for table 2018_bab.bab_users
CREATE TABLE IF NOT EXISTS `bab_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_active` int(11) DEFAULT '1',
  `user_email` varchar(255) NOT NULL,
  `user_username` varchar(50) NOT NULL,
  `user_last_name` varchar(255) NOT NULL,
  `user_first_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table 2018_bab.bab_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `bab_users` DISABLE KEYS */;
INSERT INTO `bab_users` (`user_id`, `user_active`, `user_email`, `user_username`, `user_last_name`, `user_first_name`, `user_password`, `user_created`, `user_modified`) VALUES
	(2, 1, 'yabes.wirawan@gmail.com', 'yabesWirawan', 'Wirawan', 'Natanael Yabes', '$2a$10$UJcHdY2e/dxKTr6ZN6ax0.iOwEwNQqty1sUvYLQnj7xHY3Tsh/VE2', '2018-05-18 11:32:11', '2018-05-18 11:32:11'),
	(3, 1, 'administrator@gmail.com', 'administrator', 'Administrator', 'Administrator', '$2a$10$4iR.lQVXZ.cuuIDDa4LXlemJZzDsRkONXEYi1gOVvTPAzy.6m8Hhy', '2018-05-18 12:02:22', '2018-05-18 12:02:22');
/*!40000 ALTER TABLE `bab_users` ENABLE KEYS */;

-- Dumping structure for table 2018_bab.bab_user_has_roles
CREATE TABLE IF NOT EXISTS `bab_user_has_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
  CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `bab_users` (`user_id`),
  CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `bab_user_roles` (`role_id`),
  CONSTRAINT `FKqmsn677t7kpen9ko39r1bkra9` FOREIGN KEY (`role_id`) REFERENCES `bab_user_roles` (`role_id`),
  CONSTRAINT `FKsjgockk7q723qq25lxe78cssu` FOREIGN KEY (`user_id`) REFERENCES `bab_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table 2018_bab.bab_user_has_roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `bab_user_has_roles` DISABLE KEYS */;
INSERT INTO `bab_user_has_roles` (`user_id`, `role_id`) VALUES
	(2, 1),
	(3, 1);
/*!40000 ALTER TABLE `bab_user_has_roles` ENABLE KEYS */;

-- Dumping structure for table 2018_bab.bab_user_roles
CREATE TABLE IF NOT EXISTS `bab_user_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  `role_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `role_modified` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table 2018_bab.bab_user_roles: ~1 rows (approximately)
/*!40000 ALTER TABLE `bab_user_roles` DISABLE KEYS */;
INSERT INTO `bab_user_roles` (`role_id`, `role`, `role_created`, `role_modified`) VALUES
	(1, 'admin', '2018-05-17 16:11:18', '2018-05-17 16:11:18');
/*!40000 ALTER TABLE `bab_user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
